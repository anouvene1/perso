# TODOs

## Role de ce fichier
Il est de renseigner des TODO qui seraient trop long à expliquer en 
commentaires dans le code d'une part et, d'autre part, parce que certains ne 
sont pas spécifiques à une classe ou une méthode en particulier

## Améliorations nécessaires et bonnes pratiques

* best practices -- utiliser [mybatis migration](http://www.mybatis.org/migrations/) pour les modifications de structure de la base 
  de données plutôt qu'une gestion manuelle (évite les oublis et versionne 
  correctement les scripts puisque dans le projet)
* maven -- purger les dépendances inutiles (exemple: lucene)
* maven -- écrire les dépendances correctement. Le pom parent références les dépendances dont 
  les pom enfants disposent de façon à ne pas avoir à préciser la version des 
  dépendances dans les pom enfants
* java -- dans le module metier, tout est dans com.amilnote.project.metier.domain
  * repackager de la façon suivante:
    * com.amiltone.amilnote.metier.domain.entities
    * com.amiltone.amilnote.metier.dao
    * com.amiltone.amilnote.metier.pdf
    * com.amiltone.amilnote.metier.services
    * com.amiltone.amilnote.metier.tasks
    * com.amiltone.amilnote.metier.utils

* exploitabilité -- pas de **e.printstacktrace()**. Utiliser un logger à la place. inexploitable sans ça
* java -- pas de FQCN (Fully Qualified Class Name) sauf cas exceptionels (et en dehors des imports bien sûr)!
* java -- refactorer pour supprimer l'utilisation de l'API calendar et joda-time par 
  l'API java time fourni en standard par java 8
* best practices -- supprimer la partie "web" du module Amilnote-metier
* java -- utiliser .isEmpty() sur les collection splutot que .size() != 0 ou > 0, c'est fait pour!
* best practices -- mettre les choses au bon endroit et correctement pour respecter les standards 
  (DocumentService est une classe et se trouve avec les interfaces des services et n'a 
  pas d'interface! idem pour GenerateExcelService et MailService).
  
  **Respecter les standards améliore la maintenabilité!**
* best practices -- pas de classe de plus de 1000 lignes! c'est indigeste et in-maintenable! Il faut 
  spliter en plusieurs classes de rôle plus précis.
* java -- mettre les modifier dans le bon ordre (respect des standards) améliore la lisibilité
```
    @RequestMapping(value = {"Mission/update"},
        method = RequestMethod.POST,
        consumes = "application/json")
    public
    @ResponseBody
    String updateEvent(@RequestBody String pEvents) throws IOException {
```
* best practices/java -- respecter les standards sur les noms des variables, des méthdes et des classes!!!
```
public static String _generate(int length)
```
* best practices/java -- mettre les modifiers adaptés et éviter l'absence de modifier (car sans modifier, 
  c'est portée package et toute classe du même package que celle qui expose une 
  variable de portée package peut la modifier!!! c'est quasiment jamais utilisé 
  normalement. Donc, quand ça l'est, ça doit être documenté, explicité).
* best practices -- aérer le code. pas de paté comme ça:
```
    @Autowired
    private ModuleAddviseDAO moduleAddviseDAO;
    @Autowired
    private AddviseSessionDAO addviseSessionDAO;
    @Autowired
    private AddviseFormateurDAO addviseFormateurDAO;
    @Autowired
    private AddviseLinkSessionCollaborateurDAO addviseLinkSessionCollaborateurDAO;
    @Autowired
    private PDFBuilderAddvise pdfBuilderAddvise;
```
  Bien que valide (le compilateur ne râle pas), c'est illisible.


### Point sur les tests unitaires
* **FAIRE DES TESTS UNITAIRES**
* une assertion par test
* une méthode métier testée par méthode de test
* les méthodes de tests doivent être les plus simples possibles!
* inutile d'écrire dans la console, la sortie de junit suffit quand le test est bien écrit


### Bonnes pratiques web spring
* éviter les majuscules dans les url (attention lors de la livraison: va casser les bookmarks chez les utilisaturs qui en ont)
* pas de request.getParameter(name) dans les méthodes annotées en @RequestMapping. Utiliser plutot @RequestParam qui s'applique 
  sur les parametres et permet de typer plutot que tout passer par des Strings


### Point sur la javadoc
Elle ne sert à rien si elle se contente de reprendre la signature des méthodes. Il faut expliquer ce que fait la méthode et le rôle des paramètres!


### Point sur les logs
En terme d'exploitabilité (pouvoir dire ce qui se passe dans l'appli rien qu'en lisant les logs), 
il est inutile de sortir les stacktraces dans les logs.

Il faut donner des informations utiles dans les logs pour permettre de remonter à la méthode qui a produit l'erreur avec ses parametres d'entrés.

