package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderFacture;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.services.*;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.class)
public class AdminFactureControllerTest {


    @Mock
    private MissionService missionService;

    @Mock
    private DocumentService documentService;

    @Mock
    private CommandeService commandeService;

    @Mock
    private TypeFactureService typeFactureService;

    @Mock
    private ContactClientService contactClientService;

    @Mock
    private ClientService clientService;

    @Mock
    private ElementFactureService elementFactureService;

    @Mock
    private FactureArchiveeService factureArchiveeService;

    @Mock
    private TvaService tvaService;

    @Mock
    private PDFBuilderFacture pdfFacture;


    @Mock
    private MailService mailService;
    @Mock
    private FraisService fraisService;

    @Mock
    private EtatService etatService;

    @Mock
    private FactureService factureService;


    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private HttpSession httpSession;


    private RedirectAttributes redirectAttributes;

    @Mock
    private Model model;


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    /**
     * Test de la generation du csv
     * @throws Exception
     */
    @Test
    public void validationGenCSV() throws Exception {
    String codeTypeFacture="FFRAISCSV";

    List<Facture> listeFactures= new ArrayList<>();
    Facture facture=Mockito.mock(Facture.class);
    Client client=Mockito.mock(Client.class);
    Commande commande=Mockito.mock(Commande.class);

    Mockito.when(etatService.findUniqEntiteByProp(Etat.PROP_CODE, "VA")).thenReturn(new Etat("etat", Etat.ETAT_VALIDE_CODE));
    TypeFacture typeFacture=Mockito.mock(TypeFacture.class);
    Etat newEtat = etatService.findUniqEntiteByProp(Etat.PROP_CODE, "VA");
    Etat spyEtat = Mockito.spy(newEtat);


    Mockito.when(client.getId()).thenReturn(1L);
    Mockito.when(client.getContacts()).thenReturn(new ArrayList<>());
    Mockito.when(client.getAuxiliaire()).thenReturn("0TEST000");
    Mockito.when(client.getLibelleComptable()).thenReturn("Libelle comptable");

    Mockito.when(commande.getClient()).thenReturn(client);
    Mockito.when(typeFacture.getId()).thenReturn(1L);

    Mockito.when(typeFacture.getTypeFacture()).thenReturn("FRABLECSV");

    Mockito.when(facture.getId()).thenReturn(1);
    Mockito.when(facture.getQuantite()).thenReturn(1F);
    Mockito.when(facture.getPrix()).thenReturn(100F);
    Mockito.when(facture.getMontant()).thenReturn(10F);
    Mockito.when(facture.getContact()).thenReturn(1);
    Mockito.when(facture.getEtat()).thenReturn(spyEtat);
    Mockito.when(facture.getMontant()).thenReturn(1000F);
    Mockito.when(facture.getCommande()).thenReturn(commande);

    Mockito.when(facture.getPdf()).thenReturn(null);
    Mockito.when(facture.getDateFacture()).thenReturn(new Date("01/02/2018"));
    Mockito.when(facture.getMoisPrestations()).thenReturn(null);
    Mockito.when(facture.getRIBFacture()).thenReturn(null);
    Mockito.when(facture.getCommentaire()).thenReturn(null);
    Mockito.when(facture.getDateCreation()).thenReturn(null);
    Mockito.when(facture.getTypeFacture()).thenReturn(typeFacture);
    Mockito.when(facture.getIsAvoir()).thenReturn(false);


    DateTime dateVoulue=new DateTime(Calendar.getInstance());
    Mockito.when(factureService.findFacturablesByMonth(dateVoulue)).thenReturn((listeFactures));
    redirectAttributes=new RedirectAttributesModelMap();
    AdminFactureController adminFactureController=new AdminFactureController();
        adminFactureController.genererCSV("FRABLECSV","2018","01",httpServletRequest,
            model,httpSession,redirectAttributes);
        assertEquals("Generation du fichier csv",redirectAttributes.getFlashAttributes().get(AbstractController.FLASHATTRIBUTE_KEY_SUCCESS));
    }

}
