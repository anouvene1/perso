package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.OutilsInterneDAO;
import com.amilnote.project.metier.domain.dao.impl.OutilsInterneDAOImpl;
import com.amilnote.project.metier.domain.entities.OutilsInterne;
import com.amilnote.project.metier.domain.entities.json.OutilsInterneAsJson;
import com.amilnote.project.metier.domain.services.OutilsInterneService;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class OutilsInterneServiceTest {

    private List<OutilsInterne> getList(){
        List<OutilsInterne> lOisTest = new ArrayList<>();

        lOisTest.add(new OutilsInterne(1L,"BabManager", "BM", 1));
        lOisTest.add(new OutilsInterne(2L,"SIRH", "SIRH", 1));
        lOisTest.add(new OutilsInterne(3L,"CRM", "CRM", 1));
        lOisTest.add(new OutilsInterne(4L,"Note", "NT", 1));
        lOisTest.add(new OutilsInterne(5L,"Autres", "AUT", 1));

        return lOisTest;
    }
    
    @Test
    public void executeTest1() {

        List<OutilsInterne> lOisTest = getList();

        OutilsInterneDAOImpl oidi;
        oidi = Mockito.mock(OutilsInterneDAOImpl.class);
        Mockito.when(oidi.loadAll()).thenReturn(lOisTest);

        OutilsInterneService ois = new OutilsInterneServiceImpl(oidi);
        List<OutilsInterne> lOis = ois.getAllOutilsInterne();

        assertEquals(5, lOis.size());
    }

    @Test
    public void executeTest2() {
        List<OutilsInterne> lOisajTest = getList();

        OutilsInterneDAOImpl oidi;
        oidi = Mockito.mock(OutilsInterneDAOImpl.class);
        Mockito.when(oidi.loadAll()).thenReturn(lOisajTest);

        OutilsInterneService ois = new OutilsInterneServiceImpl(oidi);
        List<OutilsInterneAsJson> lOisaj = ois.getAllOutilsInterneAsJson();

        assertEquals(5, lOisaj.size());
    }
}
