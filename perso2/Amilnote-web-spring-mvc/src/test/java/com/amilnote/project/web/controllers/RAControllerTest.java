package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.itextpdf.text.DocumentException;
import org.apache.commons.mail.EmailException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.quartz.SchedulerException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;

@RunWith(MockitoJUnitRunner.class)
public class RAControllerTest {

    /* mocked depencencies */
    @Mock
    private MissionService missionService;
    @Mock
    private RapportActivitesService rapportActivitesService;
    @Mock
    private EtatService etatService;
    @Mock
    private PDFBuilderRA pdfBuilder;
    @Mock
    private FraisService fraisService;
    @Mock
    private LinkEvenementTimesheetService linkEvenenementTimesheetService;
    @Mock
    private FactureService factureService;
    @Mock
    private ElementFactureService elementFactureService;
    @Mock
    private LinkTypeFraisCaracService linkTypeFraisCaracService;
    @Mock
    private AmiltoneUtils utils;
    @Mock
    private Parametrage parametrage;

    @InjectMocks
    private RAController raController = Mockito.spy(new RAController());

    private Collaborator collab = new Collaborator();
    private Collaborator manager = new Collaborator();

    @Before
    public void setUp() {
        manager.setNom("nom Manager");
        manager.setPrenom("prenom manager");
        manager.setId(1L);

        collab.setNom("Nom Collab Tst");
        collab.setPrenom("Prénom Collab Tst");
        collab.setManager(manager);
        collab.setId(1L);

        Mockito.when(utils.getCurrentCollaborator()).thenReturn(collab);
    }

    @Test
    public void test_soumissionRASansFrais() throws
        EmailException, IOException, SchedulerException, NamingException, DocumentException, MessagingException {

        /* mocks */
        Mockito.when(rapportActivitesService.saveNewRACollaborator(any(), any(), any(), any(), anyBoolean()))
            .thenReturn("succes");

        // factures
        Facture facture = Mockito.mock(Facture.class);
        Mockito.when(facture.getEtat()).thenReturn(new Etat(Etat.ETAT_SOUMIS_CODE, "SO"));
        Mockito.when(factureService.findByMissionByMonth(any(), any()))
            .thenReturn(facture);

        // missions
        List<Mission> missions = new ArrayList<>();
        missions.add(new Mission());
        Mockito.when(missionService.findMissionsClientesByMonthForCollaborator(any(), any()))
            .thenReturn(missions);

        /* configuration du spy sur raController */
        Mockito.doReturn("le_dossier_pdfs_ras").when(raController).getDossierPDFRA();

        RedirectAttributes rAtts = new RedirectAttributesModelMap();
        raController.soumissionRA("2018-04-01", "non", rAtts);

        /* verifs */
        Mockito.verify(facture, Mockito.times(1)).setRaValide(1);
        Mockito.verify(factureService, Mockito.times(1)).createOrUpdateFacture(facture);

        Assert.assertEquals("Votre rapport d'activités pour le mois de 2018-04 est soumis sans les frais. " +
                "Pour une soumission complète, veuillez valider de nouveau votre Rapport d'activités. " +
                "<br>Vous recevrez très prochainement un mail de confirmation dans votre messagerie Amiltone.",
            rAtts.getFlashAttributes().get(AbstractController.FLASHATTRIBUTE_KEY_SUCCESS));
    }

}
