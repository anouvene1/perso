/*
 * ©Amiltone 2017
 */

package com.amilnote.project.jackson;

import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import com.amilnote.project.metier.domain.utils.JacksonJsonListConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by jnallet on 21/11/2016.
 */
public class ElementFactureJsonTest {

    @Test
    public void testJsonStringToPojo() throws IOException {
        String json = "[{\"libelle\":\"sdgdfgfgh\",\"quantite\":55.5,\"pu\":5.5,\"montant\":0.0,\"id\":50},{\"libelle\":\"gfjghjghj\",\"quantite\":54.25,\"pu\":54.5,\"montant\":0.0,\"id\":51}]";
//        String json =  "{\"libelle\":\"sdgdfgfgh\",\"quantite\":55.5,\"pu\":5.5,\"montant\":0.0,\"id\":50}";

//        ElementFactureAsJson elts = mapper.readValue(json, ElementFactureAsJson.class);
        List<ElementFactureAsJson> elts = JacksonJsonListConverter.fromJson(new TypeReference<List<ElementFactureAsJson>>() {
        }, json);

        Assert.assertNotNull(elts);
        Assert.assertTrue(elts.size() == 2);

        for (ElementFactureAsJson elt : elts) {
            Assert.assertTrue(StringUtils.isNotBlank(elt.getLibelleElement()));
        }


    }

}
