package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.services.AddviseService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;



import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AdminAddviseControlleurTest {
    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private Model model;

    @Mock
    private AddviseService addviseService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addNewModuleTest() throws Exception {
        String codeModule="M0";
        String nomModule="Test M0";
        Mockito.when(httpServletRequest.getParameter("codeModule")).thenReturn(null);
        Mockito.when(httpServletRequest.getParameter("nomModule")).thenReturn(null);

        Mockito.when(addviseService.addNewModule(codeModule,nomModule)).thenReturn(new Long(0));

    }

}
