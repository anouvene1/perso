package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderAvanceFrais;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.EtatAsJson;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.FraisService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.metier.domain.services.RapportActivitesService;
//import junit.framework.Assert;
import org.junit.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class AdminASFControllerTest {
    @Mock
    private FraisService fraisService;

    @Mock
    private EtatService etatService;

    @Mock
    private PDFBuilderAvanceFrais pdfBuilderFrais;

    @Mock
    private PDFBuilderRA pdfBuilder;
    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private HttpSession httpSession;

    @Mock
    private RapportActivitesService raService;

    private RedirectAttributes redirectAttributes;

    @Mock
    private Model model;

    private Collaborator collaborateur = new Collaborator();
    private Collaborator manager = new Collaborator();

    @Mock
    MissionService missionService;

    @Mock
    ServletContext servletContext;

    @InjectMocks
    AdminASFController adminASFController;


    @Before
    public void setUp() throws Exception {
        manager.setNom("nom Manager");
        manager.setPrenom("prenom manager");

        collaborateur.setNom("nom collab");
        collaborateur.setPrenom("prenom collab");
        collaborateur.setManager(manager);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test de l'idNoteFrais venant de l'URL(pathVariable) et requestParam=null
     * @throws Exception
     */
    @Test
    public void validationNoteDeFraisCas1() throws Exception {
        String action="VA";
        Mockito.when(httpServletRequest.getParameter("idNoteFrais")).thenReturn(null);

        Mockito.when(etatService.findUniqEntiteByProp(Etat.PROP_CODE, action)).thenReturn(new Etat("etat", Etat.ETAT_VALIDE_CODE));
        Etat newEtat = etatService.findUniqEntiteByProp(Etat.PROP_CODE, action);

        Etat spyEtat = Mockito.spy(newEtat);

        EtatAsJson etatJson = new EtatAsJson(newEtat);

        Frais frais = Mockito.mock(Frais.class);
        Mission mission = Mockito.mock(Mission.class);
        Mockito.when(mission.getId()).thenReturn(1L);
        Mockito.when(mission.getMission()).thenReturn("la mission test");
        Mockito.when(mission.getCollaborateur()).thenReturn(collaborateur);

        LinkEvenementTimesheet linkEvenementTimesheet = Mockito.mock(LinkEvenementTimesheet.class);
        Mockito.when(linkEvenementTimesheet.getMission()).thenReturn(mission);
        Mockito.when(linkEvenementTimesheet.getDate()).thenReturn(new Date());


        TypeFrais typeFrais=Mockito.mock(TypeFrais.class);
        Mockito.when(typeFrais.getCode()).thenReturn("ASF");

        Mockito.when(frais.getLinkEvenementTimesheet()).thenReturn(linkEvenementTimesheet);
        Mockito.when(frais.getTypeAvance()).thenReturn("PE");
        Mockito.when(frais.getMontant()).thenReturn(new BigDecimal("100.0"));
        Mockito.when(frais.getId()).thenReturn(1L);
        Mockito.when(frais.getCommentaire()).thenReturn(null);
        Mockito.when(frais.getNombreKm()).thenReturn(0);
        Mockito.when(frais.getLinkFraisTFC()).thenReturn(new ArrayList<>());
        Mockito.when(frais.getNational()).thenReturn(false);
        Mockito.when(frais.getJustifs()).thenReturn(new ArrayList<>());
        Mockito.when(frais.getFraisForfait()).thenReturn(null);
        Mockito.when(frais.getEtat()).thenReturn(spyEtat);
        Mockito.when(frais.getTypeFrais()).thenReturn(typeFrais);

        Mockito.stub(frais.toString()).toReturn("String representation of Frais!");
//        Mockito.when(frais.toString()).thenReturn("String representation of Frais!");

        //laisser comme ça, sinon bug mockito (?)
        //voir ici https://stackoverflow.com/questions/15554119/mockito-unfinishedstubbingexception
        FraisAsJson fraisAsJson = new FraisAsJson(frais);
        String fraisJsonString = fraisAsJson.toString();

        Mockito.when(frais.toJSon()).thenReturn(fraisAsJson);

        FraisAsJson fraisJson = frais.toJSon();
        fraisJson.setIdMission(frais.getLinkEvenementTimesheet().getMission().getId());
        fraisJson.setMission(frais.getLinkEvenementTimesheet().getMission().getMission());
        fraisJson.setDateEvenement(frais.getLinkEvenementTimesheet().getDate());
        fraisJson.setEtat(etatJson);
        fraisJson.setTypeAvance(frais.getTypeAvance());
        fraisJson.setMontant(frais.getMontant());

        Mockito.when(fraisService.findUniqEntiteByProp(Etat.PROP_CODE, action)).thenReturn(frais);

        Mockito.when(fraisService.findUniqEntiteByProp("id", 1L)).thenReturn(frais);
        Mockito.when(fraisService.createOrUpdateFrais(any(),any())).thenReturn(AdminASFController.SUCCES);

        //on se contrefiche des parametres
        Mockito.when(fraisService.actionValidationASF(any(), any(), any(), any())).thenReturn("Validation de l'avance de frais effectuée avec succès");

        redirectAttributes = new RedirectAttributesModelMap();

        adminASFController.validationNoteDeFrais(1L, action, 1L, httpSession, httpServletRequest, model, httpServletResponse, redirectAttributes, "");

        Assert.assertEquals("Validation de l'avance de frais effectuée avec succès",
            redirectAttributes.getFlashAttributes().get(AbstractController.FLASHATTRIBUTE_KEY_SUCCESS));

    }

}
