package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.utils.Encryption;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

//TODO refactorer cette classe en respectant les bonnes pratiques des tests unitaires
class CollaborateurTest {
    //Pour executer des tests JUnit 5 sous IntelliJ: Utilisez Ctrl + Shift + F10

    @org.junit.jupiter.api.Test
    void setPasswordAndEncode() {
        boolean noErr = true;
        //Test mot de passe de longeur 1-32:
        System.out.println("Test mot de passe de longeur 1-32: ...");
        boolean testPass = executeTest1();
        if(testPass){
            System.out.println("OK");
        }else{
            System.out.println("ERROR!");
            noErr = false;
        }
        assertTrue(testPass);

        //Test hash ne correspond au mot de passe du collaborateur:
        System.out.println("Test hash ne correspond au mot de passe du collaborateur: ...");
        testPass = executeTest2();
        if(testPass){
            System.out.println("OK");
        }else{
            System.out.println("ERROR!");
            noErr = false;
        }
        assertTrue(testPass);

        //Test hash mal formaté:
        System.out.println("Test hash mal formaté: ...");
        testPass = executeTest3();
        if(testPass){
            System.out.println("OK");
        }else{
            System.out.println("ERROR!");
            noErr = false;
        }
        assertTrue(testPass);

        if(noErr){
            System.out.println("Test passé: Tout est OK");
        }else{
            System.out.println("Test échoué: Il y a des erreurs");
        }
    }

    private boolean executeTest1() {
        boolean passing = true;
        Random r = new Random();
        for(int i = r.nextInt(4) +1; i<=32; i += r.nextInt(5) +1) {
            if(passing) {
                String newMdp_test = com.amilnote.project.web.utils.AmiltoneUtils._generate(i);
                Collaborator collaborateur = new Collaborator();
                try {
                    String hashedNewMDP_test = Encryption.encryptSHA(newMdp_test);
                    collaborateur.setPasswordAndEncode(hashedNewMDP_test);
                    passing = simulateFrontRequest(hashedNewMDP_test, collaborateur);
                } catch (NoSuchAlgorithmException e) {
                    fail("Une erreur pendant le test #1 de setPasswordAndEncode a été trouvé: " + e.getMessage());
                    return false;
                }
                System.out.println(" "+i+": OK");
            }else{
                System.out.println(" "+i+": ERROR!");
                break;
            }
        }
        return passing;
    }

    private boolean executeTest2() {
        Collaborator collaborateur = new Collaborator();
        String real_pass = "";
        String tried_pass = "";
        do{ //Assurer que les 2 mdp génerées ne sont pas identiques
            real_pass = com.amilnote.project.web.utils.AmiltoneUtils._generate(11);
            tried_pass = com.amilnote.project.web.utils.AmiltoneUtils._generate(11);
        }while(real_pass.equals(tried_pass));
        try {
            String real_pass_hash = Encryption.encryptSHA(real_pass);
            String tried_pass_hash = Encryption.encryptSHA(tried_pass);
            collaborateur.setPasswordAndEncode(real_pass_hash);
            return !simulateFrontRequest(tried_pass_hash, collaborateur);
        } catch (NoSuchAlgorithmException e) {
            fail("Une erreur pendant le test #2 de setPasswordAndEncode a été trouvé: " + e.getMessage());
            return false;
        }
    }

    private boolean executeTest3() {
        String newMdp_test = com.amilnote.project.web.utils.AmiltoneUtils._generate(11);
        Collaborator collaborateur = new Collaborator();
        Random r = new Random();
        try {
            String hashedNewMDP_test = Encryption.encryptSHA(newMdp_test);
            collaborateur.setPasswordAndEncode(hashedNewMDP_test);

            //Erreur de taille de hach
            String badHash = hashedNewMDP_test.substring((r.nextInt(hashedNewMDP_test.length()-2) +1)/2);
            System.out.print(" Taille trop petit ("+badHash.length()+"): ");
            boolean res = simulateFrontRequest(badHash, collaborateur);
            if(res){
                System.out.println("ERROR!");
                return false;
            }else{
                System.out.println("OK");
            }
            badHash = hashedNewMDP_test.concat(badHash);
            System.out.print(" Taille trop grand ("+badHash.length()+"): ");
            res = simulateFrontRequest(badHash, collaborateur);
            if(res){
                System.out.println("ERROR!");
                return false;
            }else{
                System.out.println("OK");
            }

            //Erreur ce n'est pas un hach
            badHash = com.amilnote.project.web.utils.AmiltoneUtils._generate(128);
            System.out.print(" Erreur de format: ");
            res = simulateFrontRequest(badHash, collaborateur);
            if(res){
                System.out.println("ERROR!");
                return false;
            }else{
                System.out.println("OK");
            }
            //Chaine vide
            badHash = "";
            System.out.print(" Chaîne vide: ");
            res = simulateFrontRequest(badHash, collaborateur);
            if(res){
                System.out.println("ERROR!");
                return false;
            }else{
                System.out.println("OK");
            }
        }catch(NoSuchAlgorithmException e){
            fail("Une erreur pendant le test #3 de setPasswordAndEncode a été trouvé: " + e.getMessage());
            return false;
        }
        return true;
    }

    boolean simulateFrontRequest(String hash, Collaborator collaborateur) throws NoSuchAlgorithmException {
        String secret = "";
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder(secret, 185000, 512);
        return pbkdf2PasswordEncoder.matches(hash, collaborateur.getPassword());
    }

}
