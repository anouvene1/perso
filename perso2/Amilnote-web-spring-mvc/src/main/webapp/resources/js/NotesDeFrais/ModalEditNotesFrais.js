/*
 * ©Amiltone 2017
 */

function ModalEditNotesFrais($pElem, pFraisData, pcurrentFrais) {
    var that = this;
    this.$that = $pElem;
    this.$selectMission = that.$that.find("#idSelectMission");
    this.$datePicker = that.$that.find("#idDatePicker");
    this.fraisData = pFraisData;

    this.onlyWorkdedDay;
    this.onlyWorkdedDayDate = [];

    if (null != pcurrentFrais) {
        this.date = pcurrentFrais.date;
    }

    this.init = function () {
        that.buildSelectMission();

        that.show();
    }


    this.show = function () {
        this.$that.modal("show");
    }

    //Construction de la liste déroulante
    this.buildSelectMission = function () {
        // Initialisation
        that.$selectMission.empty();
        that.$selectMission.append($("<option>"));
        $.each(that.fraisData, function (key, val) {

            var $input = $("<option>").attr({'value': val.id});
            $input.html(val.mission);
            that.$selectMission.append($input);

        });
        that.$selectMission.change(function () {
            var currentMissionId = $(this).val();
            if (currentMissionId != "") {
                var dataCurrentMission = $.grep(that.fraisData, function (val) {
                    return ( val.id == currentMissionId );
                });
                if (null != dataCurrentMission) {
                    that.buildDatePicker(dataCurrentMission);
                }
            }
        });
    }
    //Construction de la liste déroulante
    this.buildDatePicker = function (mission) {

        that.onlyWorkdedDay = $.grep(mission[0].listLinkEvenementTimesheetAsJson, function (event) {
            if (null != event.absence)
                return false;
            var day = moment(event.date).day();
            if (day == "Sunday")
                return false;

            return true;
        });

        var firstDay = moment().startOf("Month").toDate();
        var lastDay = moment().endOf("Month").toDate();
        //création du calendrier
        that.$datePicker.datepicker({
            minDate: firstDay,
            maxDate: lastDay,
            dateFormat: 'dd - M - yy',
            constrainInput: true,
            option: {required: true},
            beforeShowDay: function (date) {

                var tmpDate, tmpBool = false;
                var currentDate = new moment(date);

                $.each(that.onlyWorkdedDay, function (key, val) {

                    tmpDate = new moment(val.date);
                    tmpBool = currentDate.isSame(tmpDate, "day");

                    if (tmpBool) {
                        return false;// Break
                    }

                });

                if (tmpBool) {
                    return [true]
                }
                else {
                    return [false];
                }
            },

        });
    }

}
