/*
 * ©Amiltone 2017
 */

function ModalEdit($pModalEdit, pWithSaturday) {

    var that = this;
    this.$that = $pModalEdit;

    this.WITHSATURDAY = pWithSaturday;
    this.WITHFERIE = "";

    this.listMission = null;
    this.startPeriod = "";
    this.endPeriod = "";

    this.$gifLoading = that.$that.find("#idGifLoading");
    this.$backDrop = null;
    this.backDropIndex = null;

    // Mission select
    this.$selectMission = that.$that.find("#modalEditMissionSelect");

    // A chaque changement sur la liste on appelle activeOrNotSubmitButton()
    that.$selectMission.change(function () {
        that.updateCheckedWithJson({
            "mission": {
                "mission": that.get.missionSelectValueFullName()
            }
        });
        that.activeOrNotSubmitButton();

        that.showLoading(false);
    });

    // Absence select
    this.$selectAbsence = that.$that.find("#modalEditAbsenceSelect");
    that.$selectAbsence.change(function () {
        that.updateCheckedWithJson({
            "absence": {
                "typeAbsence": {
                    "typeAbsence": that.get.absenceSelectValueFullName()
                },
                "etat": {
                    "code": "RE"
                }
            }
        });
        that.activeOrNotSubmitButton();
    });

    this.idSubtitle = '#modalEditSubTitle';
    this.$subtitle = that.$that.find(that.idSubtitle);

    this.idBody = that.idBody = "#modalEditBody";
    this.$body = that.$that.find(that.idBody);

    this.idTableEvent = '.modalWithScrolling';

    this.$tableEvent = that.$that.find(that.idTableEvent);
    this.idTable = "idTable";
    this.$table = "";


    this.idMessageUtilisateur = '#idMessageUtilisateur';
    this.$messageUtilisateur = that.$that.find(that.idMessageUtilisateur);

    this.idChecBoxForAllSelection = 'idChecBoxForAllSelection';

    //Ne sera accessible que après la création du tableau où un checkbox avec cet idée est inséré dans l'entête.
    this.$idChecBoxForAllSelection = "";

    // Create or update missions/absences button
    this.$submitButton = that.$that.find('#btnUpdateSelectionModal');

    // Delete missions/absences button
    this.$delSelectionButton = that.$that.find("#btnDelSelectionModal");
    that.$delSelectionButton.click(function () {
        that.delEvents();
    });

    this.allEvents = {};
    this.saveAllEvents = {};
    this.currentAbsenceId = null;
    this.formatDateForTabEvents = 'DD/MM/YYYY HH:00';

    this.listCheckModalFunction = [];

    this.$navTabs = that.$that.find(".nav-tabs");

    // Quand click sur un des tabs, afficher la div correspondante
    that.$navTabs.find('a[data-toggle="tab"]').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // Quand affichage d'un tab
    that.$navTabs.find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        that.get.$table().bootstrapTable("load", extend(that.saveAllEvents));
        that.customInitForTab(that.get.hrefCurrentTab());
    });

    this.classEditOk = "success";
    this.classEditNotOk = "danger";

    /**
     * Fonction appelée pour afficher la fenêtre modale
     */
    this.init = function (pAllEvents, pStart, pEnd, pListMissionDisponible, pListTypeAbsence, isRangeOfDays, pAbsenceId, pWithFerie) {
        that.initFenetre();

        that.WITHFERIE = pWithFerie;
        that.listMission = pListMissionDisponible;

        // Init calendar
        var currentViewMonth = $('#calendar').fullCalendar('getView').intervalStart.month();

        //Si l'utilisateur n'a pas séléctionné une plage de jours
        if (!isRangeOfDays) {
            //La personne a cliqué sur un event
            var lAllEvents = pAllEvents;

            if (pAbsenceId == null) {
                //Récupère le premier lien dans la liste de Tabs et affiche le tab correspondant à 'href'
                that.$navTabs.find('a:first').tab('show');
            } else { //Cet event est une absence
                //Récupère le deuxième lien dans la liste de Tabs et affiche le tab correspondant à 'href'
                that.currentAbsenceId = pAbsenceId;
                that.$navTabs.find('a[href="#modalEditTabEditAbsence"]').tab('show');
            }
        }

        that.allEvents = pAllEvents;
        that.startPeriod = pStart;
        that.endPeriod = pEnd;

        that.buildSelectMission(that.$selectMission, that.listMission);
        that.buildSelectAbsence(that.$selectAbsence, pListTypeAbsence);

        that.check.listCheckFunc.forAbsence = [
            that.check.func.countEventsEditOk,
            that.check.func.selectAbsenceNotEmpty,
            that.check.func.nbJoursFerie,
            that.check.func.nbJoursAbsence,
            that.check.func.nbFraisInSelection,
            that.check.func.editAbsence
        ];

        that.check.listCheckFunc.forMission = [
            that.check.func.countEventsEditOk,
            that.check.func.selectMissionNotEmpty,
            that.check.func.nbJoursFerie,
            that.check.func.nbJoursAbsence,
        ];

        that.check.listCheckFunc.forDelEvents = [
            that.check.func.countEventsEditOk,
            that.check.func.nbJoursAbsence,
            that.check.func.nbEventsEmpty
        ];

        // Il faut appeler cette fonction avant la construction de la table des événements à modifier car cette fonction
        // fait appel à SelectMission.change ou SelectAbsence.change. Ces 2 dernières fonctions réinitialisent la
        // table des événements à modifier avec leur élément sélectionné, ce qui écraseraient la table créée par that.buildTabEvents.
        that.customInitForTab(that.get.hrefCurrentTab());

        that.buildTabPeriod(that.idSubtitle, pStart, pEnd);

        that.buildTabEvents(that.allEvents);

        that.activeOrNotSubmitButton();

        that.saveAllEvents = extend(that.allEvents);

        that.$that.modal({backdrop: 'static'});
        that.$backDrop = $(".modal-backdrop.fade.in");
        that.backDropIndex = that.$backDrop.css("z-index");

    };

    this.initFenetre = function () {

        that.$tableEvent.empty();

        that.$subtitle.empty();
        that.$submitButton.prop('disabled', true);
        that.$delSelectionButton.prop('disabled', false);
        that.$messageUtilisateur.hide();
        that.listCheckModalFunction = [];
        that.currentAbsenceId = null;
        that.check.listCheckFunc.forAbsence = [];
        that.check.listCheckFunc.forMission = [];
        that.check.listCheckFunc.forDelEvents = [];
    };

    /**
     * Initialise la fenêtre en fonction de pIdTab qui correspond à l'id du tab
     * Par défaut initialise pour l'onglet "mission"
     */
    this.customInitForTab = function (pIdTab) {
        //On initialise pour le tab absence
        if (pIdTab == "#modalEditTabEditAbsence") {

            //On ajoute à la liste les fonctions de tests pour absences
            that.listCheckModalFunction = that.check.listCheckFunc.forAbsence;

            //Change la fonction de soumission "quand click"
            that.$submitButton.unbind();
            that.$submitButton.click(function () {
                that.submitModalAbsence();
            });
            that.$that.find(".btnUpdateSelectionModal").text("Créer l'absence");
            that.activeOrNotSubmitButton();
            //Recharger le tableau des événements avec l'absence sélectionnée
            that.$selectAbsence.change();
            //On initialise pour le tab par défaut "mission"
        } else {

            //On ajoute à la liste les fonctions de tests pour mission
            that.listCheckModalFunction = that.check.listCheckFunc.forMission;

            //Change la fonction de soumission "quand click"
            that.$submitButton.unbind();
            that.$submitButton.click(function () {
                that.submitModalMission();
            });
            that.$that.find(".btnUpdateSelectionModal").text("Enregistrer");
            that.activeOrNotSubmitButton();
            //Recharger le tableau des événements avec la mission sélectionnée
            that.$selectMission.change();
        }
    };

    //Construction du tableau récapitulatif
    this.buildTabEvents = function (pListEvents) {
        that.$tableEvent.empty();
        var $table = $('<table>').prop('id', that.idTable);
        that.$tableEvent.append($table);

        pListEvents.sort(function (event1, event2) {
            if (moment(event1.start).isBefore(moment(event2.start))) {
                return -1;
            } else {
                return 1;
            }
        });

        $table.bootstrapTable({
            data: pListEvents,
            cache: false,
            striped: true,
            rowStyle: that.rowFormatter,
            height: 200,
            columns: [{
                field: 'start',
                title: 'Début',
                align: 'center',
                valign: 'middle',
                formatter: formatter.date,
            }, {
                field: 'end',
                title: 'Fin',
                align: 'center',
                valign: 'middle',
                formatter: formatter.date
            }, {
                field: 'mission',
                title: 'Mission',
                align: 'center',
                valign: 'middle',
                formatter: formatter.mission
            }, {
                field: 'absence',
                title: 'Absence',
                align: 'center',
                valign: 'middle',
                formatter: formatter.typeAbsence
            }, {
                field: 'absence',
                title: 'État',
                align: 'center',
                valign: 'middle',
                formatter: formatter.etat
            }, {
                field: 'ferie',
                title: 'Férié',
                align: 'center',
                valign: 'middle'
            }, {
                field: "nbFrais",
                title: 'Frais',
                align: 'center',
                valign: 'middle',
            }, {
                field: "colSort",
                title: '',
                align: 'center',
                valign: 'middle',
                visible: false,
                order: "asc"
            }]

        });

        that.get.$table().on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table', function (e, row) {
            that.activeOrNotSubmitButton();
        });
    };

    this.rowFormatter = function (row, index) {
        if (null != row.absence && row.absence.etat.code != "RE" && row.absence.etat.code != "BR" && row.absence.etat.code != "CO" && row.absence.etat.code != "AN") {

            row["etatModif"] = that.classEditNotOk;
            return {classes: that.classEditNotOk}

        } else if (null != row.absence && row.absence.etat.code == "BR" && row.absence.etat.code != "CO" && null == that.currentAbsenceId) {

            row["etatModif"] = that.classEditNotOk;
            return {classes: that.classEditNotOk}

        } else if (null != row.ferie && that.WITHFERIE == false) {

            row["etatModif"] = that.classEditNotOk;
            return {classes: that.classEditNotOk}

        } else {

            row["etatModif"] = that.classEditOk;
            return {classes: that.classEditOk}

        }
    };

    //Construction du tableau récapitulatif
    this.buildTabPeriod = function (pIdSubtitle, pStart, pEnd) {
        var lTxtPeriode = formatDateForUser(pStart) + " - " + formatDateForUser(pEnd);
        that.$subtitle.html(lTxtPeriode);
    };

    //Construction de la liste déroulante des missions
    this.buildSelectMission = function ($list, pList) {
        // Initialisation
        $list.empty();
        var $select = $('<select>').addClass("selectpicker");
        var $input = $("<option>").attr({'value': ""});
        $input.html(" - ");
        $select.append($input);

        $.each(pList, function (key, val) {
            var lDateDebut = moment(val.dateDebut).subtract(moment().utcOffset()-dstInFrance(moment()), 'minutes').format('DD/MM/YYYY');
            var lDateFin = moment(val.dateFin).subtract(moment().utcOffset()-dstInFrance(moment()), 'minutes').format('DD/MM/YYYY');
            var $input = $("<option>").attr({'value': val.id, 'data-subtext': lDateDebut + ' - ' + lDateFin});
            $input.html(val.mission);
            $select.append($input);
        });
        $list.append($select);
        $select.selectpicker();
        $select.attr({"data-width": "80%"});
    };

    //Construction de la liste déroulante
    this.buildSelectAbsence = function ($list, pList) {
        // Initialisation
        $list.empty();
        var $select = $('<select>').addClass("selectpicker");
        var $input = $("<option>").attr({'value': ""});
        $input.html(" - ");
        $select.append($input);

        $.each(pList, function (key, val) {
            var $input = $("<option>").attr({'value': val.id});
            $input.html(val.typeAbsence);
            $select.append($input);
        });
        $list.append($select);
        $select.selectpicker();
        $list.attr({"data-width": "80%"});
    };

    /**
     * Met à jour les cases cochées du tableau d'evenements avec le json passé en parametre
     */
    this.updateCheckedWithJson = function (json) {

        var allEvents = that.get.allEvents();
        $.each(allEvents, function (key, val) {
            if (val.etatModif == that.classEditOk) {
                $.extend(val, json);
            }
        });
    };

    this.showLoading = function (bool) {
        if (bool) {
            that.get.backdropIndex(9999);
            $("body").css("cursor", "progress");
            that.$gifLoading.show();
        } else {
            $("body").css("cursor", "default");
            that.$gifLoading.hide();
            that.get.backdropIndex(that.backDropIndex);
        }
    };

    /**
     * Ajoute les messages
     */
    this.addErrorToMessageUtilisateur = function (pListErrors) {
        var lListErrors = [];
        that.$messageUtilisateur.empty();
        var $lUl = $('<ul>').addClass('list-group');

        $.each(pListErrors, function (key, err) {
            var classLevel;
            if (err.level == 1) {
                classLevel = 'list-group-item-danger';
            } else if (err.level == 2) {
                classLevel = 'list-group-item-warning';
            }
            var $lIl = $('<li>').addClass('list-group-item ' + classLevel).html(err.message);
            $lUl.append($lIl);
        });
        that.$messageUtilisateur.append($lUl);
    };

    /**
     * Accesseurs sur la fenêtre modale
     */
    this.get = {
        eventsEditOK: function () {
            //Retourne toutes les lignes, même celle avec un checkbox disable(bug)
            var allEvents = that.get.allEvents();
            //Filtre les cases retournées pour enlever celles qui sont disable.
            //La fonction est commune avec checkbox formatter dans le parametrage de la colonne state.
            var lEventsEditOk = $.grep(allEvents, function (row) {
                return ( row.etatModif == that.classEditOk );
            });
            return lEventsEditOk;
        },
        allEvents: function () {
            var lData = that.get.$table().bootstrapTable('getData');
            return lData;
        },

        missionSelectValue: function () {
            return that.$selectMission.find('select').val();
        },
        missionSelectValueFullName: function () {
            return that.$selectMission.find('.bootstrap-select button span.filter-option').html();
        },

        absenceSelectValue: function () {
            return that.$selectAbsence.find('select').val();
        },
        absenceSelectValueFullName: function () {
            return that.$selectAbsence.find('.bootstrap-select button span.filter-option').html();
        },
        hrefCurrentTab: function () {
            return that.$navTabs.find('li.active a').attr('href');
        },
        $table: function () {
            return that.$body.find("#" + that.idTable);
        },
        backdropIndex: function (val) {
            if (null == that.$backDrop)
                return false;
            if (null != val)
                that.$backDrop.css("z-index", val);
            else
                return that.$backDrop.css("z-index");
        }

    };

    /**
     * - Appel that.check.checkModal()
     * - Si that.checkModal = true
     *        - activer bouton de validation de la fenêtre
     * - Sinon
     *        - Désactiver bouton de validation de la fenêtre
     */
    this.activeOrNotSubmitButton = function () {
        if (that.check.checkModal(that.listCheckModalFunction)) {
            that.$submitButton.prop('disabled', false);
        } else {
            that.$submitButton.prop('disabled', true);
        }
    }

    /**
     * Namespace pour les outils de tests de la fenêtre modales
     */
    this.check = {

        /**
         * - Fonctions de tests de la fenêtre modal
         *        - Soit il y a une erreur et on retourne l'erreur avec son message et son niveau
         *                - 2 = warning, 1 = alert
         *        - Soit on retourne TRUE
         */
        func: {

            countEventsEditOk: function () {
                var lEventsEditOk = that.get.eventsEditOK();
                if (lEventsEditOk.length == 0) {
                    return that.check.getMessage(1, "Aucun événement de votre sélection n'est modifiable.");
                } else {
                    return true;

                }
            },
            selectMissionNotEmpty: function () {
                var lSelectVal = that.get.missionSelectValue();
                var lListMission = that.listMission;
                if (lListMission.length == 0) {
                    return that.check.getMessage(1, "Aucune mission disponible.");
                } else if (lSelectVal == null || lSelectVal == "") {
                    return that.check.getMessage(1, "Une mission doit être choisie.");
                } else {
                    return true;
                }
            },
            selectAbsenceNotEmpty: function () {
                var lSelectVal = that.get.absenceSelectValue();
                if (lSelectVal == null || lSelectVal == "") {
                    return that.check.getMessage(1, "Une absence doit être choisie.");
                } else {
                    return true;
                }
            },
            nbJoursFerie: function () {
                var lAllEventFerie = $.grep(that.allEvents, function (event) {
                    return (event.ferie != null);
                });
                if (lAllEventFerie.length != 0 && that.WITHFERIE == false) {
                    return that.check.getMessage(2, "Il y a des jours fériés dans la sélection. Un paramètre est disponible pour pouvoir les modifier.");
                } else {
                    return true;
                }
            },
            nbFraisInSelection: function () {
                var lnbFraisTotal = 0;
                var lEventsEditOk = that.get.eventsEditOK();
                $.each(lEventsEditOk, function (key, val) {
                    if (null != val.nbFrais && val.nbFrais > 0) {
                        lnbFraisTotal += val.nbFrais;
                    }
                });
                if (lnbFraisTotal > 0) {
                    return that.check.getMessage(2, "Il y a " + lnbFraisTotal + " frais associés aux missions de votre séléction, la création d'une absence cette même demi-journée supprimera les frais liés à cette demi-journée.");
                } else {
                    return true;
                }
            },
            nbJoursAbsence: function () {
                var lEventsWithAbsence = $.grep(that.allEvents, function (event) {
                    return ( null != event.absence
                    && event.absence.etat.code != "RE"
                    && event.absence.etat.code != "BR"
                        && event.absence.etat.code != "AN"
                        && !(event.absence.etat.code == "CO" &&document.getElementById("btn-sendRA").getAttribute("disabled")!='disabled'));
                });
                if (lEventsWithAbsence.length != 0 && lEventsWithAbsence.length == that.allEvents.length) {
                    return that.check.getMessage(1, "Toutes vos demi-journées sont des absences  soumises ou validées. Aucune édition n'est possible.");
                }
                if (lEventsWithAbsence.length != 0) {
                    return that.check.getMessage(2, "Il y a des absences soumises ou validées dans la sélection : aucune édition ou suppression n'est possible pour cette ou ces demi-journée(s). ");
                } else {
                    return true;
                }
            },
            nbEventsEmpty: function () {
                var nbEventsEmpty = $.grep(that.get.eventsEditOK(), function (event) {
                    return (event.mission == null && event.absence == null );
                });
                if (nbEventsEmpty.length > 0) {
                    return that.check.getMessage(1, "Vous ne pouvez pas supprimer des demi-journées vides.");
                } else {
                    return true;
                }
            },
            editAbsence: function () {
                var listAbs = $.grep(that.allEvents, function (event) {
                    return ( null != event.absence
                    && (event.absence.etat.code == "BR"
                    || event.absence.etat.code == "CO"));
                });
                var msg = "Vous pouvez modifier des absences avec l'etat \"Brouillon\" en cliquant directement dessus";
                if (listAbs.length > 0 && null == that.currentAbsenceId && listAbs.length == that.allEvents.length) {
                    return that.check.getMessage(1, msg);
                }
                else if (listAbs.length > 0 && null == that.currentAbsenceId) {
                    return that.check.getMessage(2, msg);
                } else {
                    return true;
                }
            }

        },
        /**
         * Doit contenir des tableaux de fonctions check
         * - c'est tableaux peuvent etre donnés en paramètre a checkmodal() pour les executés
         */
        listCheckFunc: {},


        /**
         * - Vide la liste des messages d'erreurs
         * - Pour chaque function de test présente dans pListCheckModalFunction
         *        - récupérer le résultat
         *        - Si = faux : stocker le message d'erreur
         * - SI il y a des messages d'erreurs :
         *        - Afficher les messages utilisateurs
         *        - SI il y a que des warnings : retourner true
         *        - SINON retourner Faux
         * - SINON
         *        - Cacher la div d'erreur et retourner TRUE
         */
        checkModal: function (pListCheckModalFunction) {
            var lCheck = true;
            var listErrors = [];
            that.$messageUtilisateur.empty();
            //Pour chaque fonction de test dans that.listCheckModalFunction
            $.each(pListCheckModalFunction, function (key, func) {
                //Si la fonction return le tableau avec un message d'erreur en plus
                error = func(); // on stock le resultat
                if (error != true) {
                    listErrors.push(error);
                }
            });

            // il y a aucune erreur
            if (listErrors.length == 0) {
                that.$messageUtilisateur.hide();
                return true;
                // si il y a eu des erreurs lors des tests
            } else if (listErrors.length != 0) {
                // on affiches les erreurs
                that.addErrorToMessageUtilisateur(listErrors);
                that.$messageUtilisateur.show();
                // On récupère les erreurs bloquantes (level = 1)
                var listErrorsWithAlert = $.grep(listErrors, function (error) {
                    return (error.level == 1)
                });
                // il y a eu des erreurs bloquantes lors des tests ( level = 1 )
                if (listErrorsWithAlert.length != 0) {
                    return false;
                } else { // il y a que des warnings
                    return true;
                }
            }
        },
        getMessage: function (level, message) {
            return {"level": level, "message": message};
        }
    };

    // Create or update mission(s)
    this.submitModalMission = function () {
        if (that.check.checkModal(that.listCheckModalFunction)) {
            var lAllEventsEditOK = that.get.eventsEditOK();

            if (lAllEventsEditOK.length > 0) {
                var lMissionSelectValue = that.get.missionSelectValue();

                var jsonData = {
                    "id": lMissionSelectValue,
                    "listLinkEvenementTimesheetAsJson": []
                };

                $.each(lAllEventsEditOK, function (key, val) {
                    jsonData.listLinkEvenementTimesheetAsJson.push({
                        "id": val.id,
                        "start": moment(val.start).utc().utcOffset(-(moment().utcOffset()-dstInFrance(moment())), 'minutes'),
                        "end": moment(val.end).utc().utcOffset(-(moment().utcOffset()-dstInFrance(moment())), 'minutes')
                    });
                });

                // Update treatment with AJAX
                updateSelection(jsonData, 'Rapport-Activites/mesTimesheets/Event/Mission/update');
            }
        }
    };

    // Create or update absence(s)
    this.submitModalAbsence = function () {
        if (that.check.checkModal(that.listCheckModalFunction)) {
            var lAllEventsEditOK = that.get.eventsEditOK();

            if (lAllEventsEditOK.length > 0) {
                var lAbsenceSelectValue = that.get.absenceSelectValue();

                var jsonData = {
                    "id": that.currentAbsenceId,
                    "dateDebut": that.startPeriod,
                    "dateFin": that.endPeriod,
                    "typeAbsence": {
                        "id": lAbsenceSelectValue
                    },
                    "listLinkEvenementTimesheetAsJson": [],
                };

                var today = moment();

                $.each(lAllEventsEditOK, function (key, val) {
                    jsonData.listLinkEvenementTimesheetAsJson.push({
                        "id": val.id,
                        "start": moment(val.start).utc().utcOffset(-(moment().utcOffset()-dstInFrance(today)), 'minutes'),
                        "end": moment(val.end).utc().utcOffset(-(moment().utcOffset()-dstInFrance(today)), 'minutes')
                    });
                });

                jsonData.dateDebut = moment(jsonData.listLinkEvenementTimesheetAsJson[0].start).local()
                    .utcOffset(today.utcOffset()-dstInFrance(today), 'minutes');
                jsonData.dateFin = moment(jsonData.listLinkEvenementTimesheetAsJson[jsonData.listLinkEvenementTimesheetAsJson.length-1].end).local()
                    .utcOffset(today.utcOffset()-dstInFrance(today), 'minutes');

                // Update treatment with AJAX
                updateSelection(jsonData, 'Rapport-Activites/mesTimesheets/Event/Absence/update');
            }
        }
    };

    // Delete missions or absences
    this.delEvents = function () {
        if (that.check.checkModal(that.check.listCheckFunc.forDelEvents)) {
            var lAllEventsEditOK = that.get.eventsEditOK();

            if (lAllEventsEditOK.length > 0) {
                var jsonData = [];

                $.each(lAllEventsEditOK, function (key, val) {
                    jsonData.push({"id": val.id});
                });

                // Delete treatment with AJAX
                updateSelection(jsonData, 'Rapport-Activites/mesTimesheets/Event/Absence/supprime');
            }
        }
    }
} //Fin de la modal

function getInputCheckBox(pName, pVal, pIsCheck, pDisabled) {

    var lChecked = ( pIsCheck ? "checked" : "" );
    var lDisabled = ( pDisabled ? "disabled" : "" );
    var input = "";
    if (!lDisabled) {
        // Cet elements est destiné a être inséré dans un tableau $.jsonToTable()
        //Seuls les elements écrits en dures fonctionnent avec cette technique
        var input = "<input  type='checkbox'  value='" + pVal + "' " + lChecked + ">";
    } else {
        var input = "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
    }

    return input;
}

function getCheckBoxForAllSelection(pId) {
    var $input = $('<input>').attr({
        type: 'checkbox',
        id: pId
    });
    //  Bidouille pour récupèrerer l'input créé au format string
    return $input.clone().wrap('<p>').parent().html();

}

function addIdToListEvents(pListEvents) {
    var lListEvents;
    lListEvents = $.each(pListEvents, function (key, val) {

        val["colSort"] = moment(val['start']).format();

        if (val._id == null)
            val['_id'] = "tmp" + key;
    });
    return lListEvents;
}