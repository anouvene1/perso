$(function() {
    $('#clientContactRows').on('click', 'a.contactAction', function(event) {
        event.preventDefault();

        let $this = $(this);
        let clientContact = lstContacts[$this.data('contactindex')];
        delete clientContact.client.contacts; // Remove the client "contacts" attribute
        let clientHref = $this.closest('td').prev('td.contactEditLink').data('url');

        $.ajax({
            url: 'Administration/clientContact',
            type: 'PUT',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(clientContact),
            async: false,
            success: function(res) {
                let clientContactSuccess = res;

                if(clientContactSuccess.enabled) {
                    $this.html('Désactiver');

                    if($this.closest('td').prev('td.contactEditLink').find('a').length > 0) {
                        $this.closest('td').prev('td').find('a.contactEdit').css('display', 'block');
                    } else {
                        $this.closest('td').prev('td.contactEditLink').html('<a class="contactEdit" href="' + clientHref + '"> &Eacute;diter</a>');
                    }
                } else {
                    $this.html('Activer');
                    $this.closest('td').prev('td').find('a.contactEdit').css('display', 'none');
                }

                clientContact.enabled = clientContactSuccess.enabled;

                // To prevent bug bootstrap table
                $('[data-toggle="table"]').bootstrapTable('resetView');
                $(".fixed-table-container").css({"height":"auto", "padding-bottom": "0"});
            },
            error: function (xhr, status, error) {
                console.log('Erreur de traiement serveur: ', error);
            }
        });
    });
});