/*
 * ©Amiltone 2017
 */
//AMNT-871
function generateUuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

class Mission {
    constructor() {
        this.colllaborator = idCollaborator;
        this.missionName = document.getElementById('nomMission').value;
        this.startDate = document.getElementById('dateDebut').value;
        this.endDate = document.getElementById('dateFin').value;
        this.description = document.getElementById('description').value;
        this.weeklyDuration = document.getElementById('duree').value;
        this.missionType = document.getElementById('listTypesMission').value;
        this.agency = null;
        this.customer = null;
        this.tjm = null;
        this.customerManager = null;
        this.following = null;
        this.missionManager = null;
        this.internalTools = null;
        this.uuid = generateUuid();

        if(this.missionType == 5){
            this.customer = document.getElementById('listClients').value;
            this.customerManager = document.getElementById('listContactsClient').value;
            this.tjm = document.getElementById('tjm').value;
            this.following = document.getElementById('suivi').value ;
            this.missionManager = document.getElementById('listeManagers').value;
        }

        if(this.missionType == 2){
            this.agency = document.getElementById('agence').value;
        }
        if(this.missionType == 9){
            this.internalTools =  document.getElementById('outilsInterne').value;
        }
    }
}

class Forfait {
    constructor(attachMission, code, amount, label, frequency){
        this.attachedMission = attachMission;
        this.code = code;
        this.amount = amount;
        this.label = label;
        this.frequency = frequency;
    }
}


//display a forfait table
function createForfaitTable(forfaitTable){

    var cells = document.querySelector(".forfaitTable");
    cells.innerHTML = "";
    for(let i = 0 ; i < forfaitTable.length ; i++){
        document.getElementsByClassName('.remove' + i).id = i ;
        cells.innerHTML +=
            "<tr class='remove"+ i +"'><td style='text-align: center; width: 16.38%;'>"+ forfaitTable[i][0] + "</td>" +
            "<td  style='text-align: center; width: 28.85%; '>"+ forfaitTable[i][1] + "</td>" +
            "<td  style='text-align: center; width: 23.7%;'>"+ forfaitTable[i][2] + "</td>" +
            "<td  style='text-align: center; width: 31.05%;'> <a  onclick='removeForfait(document.getElementsByClassName(\".remove"+i+"\").id)'> <span class='glyphicon glyphicon-remove 1'></span> </a> </td> </tr>";
        document.getElementsByClassName('.remove').id = i ;
    }
}

function getSelectedForfait(sel) {
    var selectedForfaitOption = $('#idListForfaits').find('option:selected');

    if ($('#idListForfaits').val().indexOf('LIB') !== -1) {

        $('#libelle').attr("value", selectedForfaitOption.attr("name"));

        $('#libelleForfaitLibre').attr('style', 'display: block');
        $('#montantForfait').attr('style', 'display: none');
    }
    else {
        $('#libelleForfaitLibre').attr('style', 'display: none');
        $('#montantForfait').attr('style', 'display: block');
    }
}

function nomMissionValid() {
    if (document.getElementById('idMission').value == "") {
        if (!nomMissionExist(document.getElementById('nomMission').value)) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

$("#idMontant").attr('required', '');

function remplaceVirgule(input) {
    input.value = input.value.replace(',', '.');
}

function testForm(form) {
    var $modalWarning = $("#modalWarning");

    var nomMission = document.getElementById('nomMission').value;
    if(nomMission.indexOf("/") > -1) {
        $modalWarning.find(".modal-body").empty();
        $modalWarning.find(".modal-body").append("Nom de mission invalide : les '/' ne sont pas autorisés.");
        $modalWarning.modal({
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }

    var valid = nomMissionValid();
    if (!valid) {
        $modalWarning.find(".modal-body").empty();
        $modalWarning.find(".modal-body").append("Une mission portant le même nom existe déjà pour ce collaborateur. \nVeuillez choisir un nom de mission unique.");
        $modalWarning.modal({
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }
    else {
        if ((form.listTypesMission.value == 5) && (form.listClients.value == "")) {

            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Vous devez sélectionner un client.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            return false;
        }
        if (form.listTypesMission.value == 5 && $('#tjm').val() <= 0) {
            $('#tjm').focus();
            return false;
        }
    }
}

$(function() {
    var URLWITHForfait = urlForfait;
    var URLEDITMISSION = urlEditMission;
    var addForfaitWithUrl = urlAddForfait;
    var WITHFORFAIT = pWithForfaitFromController;
    var editMission = pEditMission;
    var idClientSelected = document.getElementById("listClients").value;

    if (editMission) {
        var idMission = document.getElementById("idMission").value;
    } else {
        var idMission = null;
    }

    if(idMission == null) {
        $('#idWithForfait').change(function (e) {
            storeValues(document.formMission);
            if ((idClientSelected != "undefined") && (idClientSelected != "")) {
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + (WITHFORFAIT ? "true" : "true") + "/" + idClientSelected);
            } else
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + (WITHFORFAIT ? "true" : "true") + "/" + 0);
        });

        $("#listClients").change(function (e) {
            if (document.getElementById("listClients").value != "") {
                storeValues(document.formMission);
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + document.getElementById("idWithForfait").checked + "/" + document.getElementById("listClients").value);
            }
        });

    } else {
        $("#listClients").change(function (e) {
            if (document.getElementById("listClients").value != "") {
                storeValues(document.formMission);
                $(location).attr('href', URLEDITMISSION + "/" + idMission + "/getContacts/" + document.getElementById("listClients").value);
            }
        });
    }

    var tmpForfaitCode;
    var tempForfaitMontant;
    var libelle;

    $("#buttonSubmitForfait").click(function (e) {
        tmpForfaitCode = document.getElementById('idListForfaits').value;
        tempForfaitMontant = document.getElementById('idMontant').value;
        libelle = $("#libelle").val();
        var percent = '%';
        var frequency;
        if(tmpForfaitCode === "TEL" || tmpForfaitCode === "LIB_TCM"){
            frequency = "Mensuel";
            if(tmpForfaitCode === "LIB_TCM"){
                tempForfaitMontant="50% abonnement TCL"
            }
        } else if(tmpForfaitCode === "LIB"){
            frequency = "Ponctuel";
            tempForfaitMontant = "-"
        } else {
            frequency = "Journalier";
        }
        // symbole pourcentage encodé car caractère mal interprété dans les url
        var percentEncoded = "%25";

        if (libelle === "") {
            libelle = "0";
        }

        if (libelle.indexOf(percent) !== -1) {
            libelle = libelle.replace(/\%/g, percentEncoded);
        }

        var forfait = [tmpForfaitCode, frequency, tempForfaitMontant, libelle];

        forfaitTable.push(forfait);
        createForfaitTable(forfaitTable);
    });

    // Envoie de la mission
    $("#buttonSubmiMission").click(function (e) {
        // storeValues(document.formMission);

        let missionFlatRates = [];
        $('.forfaitTable').find('tr').each(function() {
            missionFlatRates.push({
                code: $(this).find('td:eq(0)'),
                montant: $(this).find('td:eq(2)')
            });
        });

        console.log(missionFlatRates);

        let missionAsJson = {
            mission: $('#nomMission').val(),
            dateDebut: $('#dateDebut').val(),
            dateFin: $('#dateFin').val(),
            description: $('#description').val(),
            heuresHebdo: $('#duree').val(),
            typeMission: null,
            agence: $('#listTypesMission').val() === 2 ? $('#agence') : null,
            client: $('#listTypesMission').val() === 5 ? $('#listClients').value : null,
            tjm: $('#listTypesMission').val() === 5 ? ('#tjm').val() : null,
            responsableClient: $('#listTypesMission').val() === 5 ? $('#listContactsClient').val() : null,
            suivi: $('#listTypesMission').val() === 5 ? ('#suivi').val() : null,
            manager: $('#listTypesMission').val() === 5 ? ('#listeManagers').val() : null,
            outilsInterne: $('#listTypesMission').val() === 9 ? $('#agence') : null
        };

        console.log(missionAsJson);

        $.ajax({
            url: 'Administration/mission-flat-rate/' + idCollaborator,
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            //data: JSON.stringify(missionAsJson),
            data: {
                'missionAsJson': JSON.stringify(missionAsJson),
                'missionFlatRates': JSON.stringify(missionFlatRates)
            },
            async: false,
            success: function(data, textStatus, xhr) {
                console.log(textStatus);
                if(textStatus === 'success'){
                    console.log(xhr.status, data);
                }
            },
            error: function (xhr, status, error) {
                console.log('Erreur de traiement serveur: ', error);
            }
        });
    });

    $("#suppr").click(function (e) {
        storeValues(document.formMission);
    });

    var today = new Date();
    var expiry = new Date(today.getTime() + 60 * 1000); // plus 1 min

    // on set les valeurs enregistrées dans le cookie
    if (nomMission = getCookie("nomMission"))
        document.formMission.nomMission.value = nomMission;
    if (dateDebut = getCookie("dateDebut"))
        document.formMission.dateDebut.value = dateDebut;
    if (dateFin = getCookie("dateFin"))
        document.formMission.dateFin.value = dateFin;
    if (listTypesMission = getCookie("listTypesMission"))
        document.getElementById('listTypesMission').value = listTypesMission;
    if (outilsInterne = getCookie("outilsInterne"))
        document.getElementById('outilsInterne').value = outilsInterne;
    if (description = getCookie("description"))
        document.getElementById('description').value = description;
    if (duree = getCookie("duree"))
        duree = duree;
    if (listClients = getCookie("listClients"))
        document.getElementById('listClients').value = listClients;
    if (listContactsClient = getCookie("listContactsClient"))
        document.getElementById('listContactsClient').value = listContactsClient;
    if (suivi = getCookie("suivi"))
        document.getElementById('suivi').value = suivi;
    if (tjm = getCookie("tjm"))
        document.getElementById('tjm').value = tjm;
    /*
     SHA AMNOTE-196 05/01/2017
     restitution des cookies des chechboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        var perimetre = getCookie("perimetreMission-" + e.value);
        if (perimetre) {
            $("#perimetreMission_"+e.value).prop("checked", perimetre == "true");
        }
    });

    // On affiche les infos pour la mission cliente et formation
    loadTypeMission();

    // Date picker calendar
    $(".datepicker, #ui-datepicker-div").datepicker({
        showOn: "both",
        dateFormat: 'yy-mm-dd'
    }).on("click", function() {
        $(".ui-datepicker-calendar").css("display", "block");
    });
});

function setCookie(name, value) {
    var today = new Date();
    var expiry = new Date(today.getTime() + 60 * 1000); // plus 1 min
    document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
}

function deleteCookie(name) {
    var today = new Date();
    var expired = new Date(today.getTime() - 24 * 3600 * 1000); // less 24 hours
    document.cookie = name + "=null; path=/; expires=" + expired.toGMTString();
}

function clearCookies() {
    /*
     SHA AMNOTE-196 05/01/2017
     suppression des cookies des chechboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        deleteCookie("perimetreMission-" + e.value);
    });

    deleteCookie("nomMission");
    deleteCookie("dateDebut");
    deleteCookie("dateFin");
    deleteCookie("listTypesMission");
    deleteCookie("outilsInterne");
    deleteCookie("description");
    deleteCookie("duree");
    deleteCookie("listClients");
    deleteCookie("listContactsClient");
    deleteCookie("suivi");
    deleteCookie("tjm");
}

function storeValues(form) {
    /*
     SHA AMNOTE-196 05/01/2017
     création des cookies des checkboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        setCookie("perimetreMission-" + e.value, e.checked);
    });

    setCookie("nomMission", form.nomMission.value);
    setCookie("dateDebut", form.dateDebut.value);
    setCookie("dateFin", form.dateFin.value);
    setCookie("listTypesMission", document.getElementById('listTypesMission').value);
    setCookie("outilsInterne", document.getElementById('outilsInterne').value);
    setCookie("description", document.getElementById('description').value);
    setCookie("duree", document.getElementById("duree").value);
    setCookie("listClients", document.getElementById("listClients").value);
    setCookie("listContactsClient", document.getElementById("listContactsClient").value);
    setCookie("suivi", document.getElementById("suivi").value);
    setCookie("tjm", form.tjm.value);
    return true;
}

function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function loadTypeMission() {
    var idMission = $("#listTypesMission").val();
    checkPerimetresByDefault(idMission);

    /*Ids type mission:
     * 1=Travaux internes
     * 2=ActivitéCommerciale
     * 4=Formation
     * 5=Mission cliente
     * 6=Web Factory
     * 7=Mobile Factory
     * 8=Data Factory
     * 9=Soft Factory
     */

    var divClient = $("#divClient");
    var divOutilsInterne = $("#divOutilsInterne");
    var inputActiviteCommerciale = $('#perimetreMission_3');
    var fieldsMissionCliente = $(".form-group.missionCliente");
    var fieldsOutilsInterne = $(".form-group.fieldsOutilsInterne");
    var divAgence = $("#choixAgence");

    switch(idMission) {
        case '2':
            divAgence.removeAttr('hidden');
            divAgence.show();

            divOutilsInterne.attr('hidden', 'true');
            divClient.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '4':
            divClient.removeAttr('hidden'); //on restore la visibilité
            fieldsMissionCliente.show();
            inputActiviteCommerciale.prop("checked", false);
            inputActiviteCommerciale.parent().hide();
            fieldsMissionCliente.hide(); //tous les champs propres aux missions clientes sont cachés

            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '5':
            divClient.removeAttr('hidden'); //on restaure la visibilité
            fieldsMissionCliente.show();
            inputActiviteCommerciale.parent().show();

            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '9':
            divOutilsInterne.removeAttr('hidden');
            fieldsOutilsInterne.show();

            divClient.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            break;

        default:
            divClient.attr('hidden', 'true');
            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            document.getElementById('listClients').value = null;
            document.getElementById('listContactsClient').value = null;
            document.getElementById('suivi').value = null;
            document.getElementById('tjm').value = 0;
            break;
    }
}
function changeTypeMission() {
    $(".check-box.perimetreMission").prop("checked", false);
    loadTypeMission();
}

function checkPerimetresByDefault(idMission) {
    // On coche les cases masquées des périmètres de mission selon le type de mission
    switch (idMission) {
        case "1": //Travaux internes
        case "6": //idem pour Web Factory
        case "7": //idem pour Mobile Factory
        case "8": //idem pour Data Factory
        case "9": //idem pour Soft Factory
            $('#perimetreMission_1').prop("checked", true);
            break;
        case "2":
            $('#perimetreMission_3').prop("checked", true);
            break;
        default: //Cases à cocher non masquées pour Mission Cliente et Formations
            break;
    }
}
function removeForfait(forfaitId) {

    forfaitTable.splice(forfaitId, 1);
    createForfaitTable(forfaitTable);
}