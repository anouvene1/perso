<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Gestion d'AmilNote</li>
</ol>

<div class="center-block ">
    <div class="panel-primary col-md-4">
        <div class="panel-heading text-center">Édition de la tarification (TVA/Kilométrique)</div>
        <div class="panel-body">
            <form:form method="post" action="${administration}${saveTVA}"
                       modelattribute="TVAForm" class="form-horizontal">
                <c:forEach items="${listTva}" var="TVA" varStatus="status">
                    <div class="input-group">
                        <c:if test="${TVA.code == 'KM' }">
                            <span class="input-group-addon">${TVA.tva}</span> <input
                            type="text" class="form-control"
                            aria-label="Amount (to the nearest dollar)"
                            name="listTVA[${status.index}].montant" value="${TVA.montant}"
                            data-validation="number"
                            data-validation-allowing="range[0.00;200.0],float"/> <span
                            class="input-group-addon">€</span>
                        </c:if>
                        <c:if test="${TVA.code != 'KM' }">
                            <span class="input-group-addon">${TVA.tva}</span> <input
                            type="text" class="form-control"
                            aria-label="Amount (to the nearest dollar)"
                            name="listTVA[${status.index}].montant" value="${TVA.montant}"
                            data-validation="number"
                            data-validation-allowing="range[0.00;200.0],float"/> <span
                            class="input-group-addon">%</span>
                        </c:if>
                    </div>
                    <!-- CHAMP ID TVA -->
                    <input type="hidden" name="listTVA[${status.index}].id"
                           value="${TVA.id}"/>
                    <!-- CHAMP CODE TVA -->
                    <input type="hidden" name="listTVA[${status.index}].code"
                           value="${TVA.code}"/>

                </c:forEach>
                <div class="form-group">
                    <!-- Colonne vide pour aligner le bouton aux champs de saisies -->
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <input class="btn btn-success" type="submit" value="Sauvegarder">
                    </div>
                </div>

            </form:form>
        </div>
        <div class="panel-heading text-center">Gestion des commandes</div>
        <div class="panel-body">
            <form:form method="post" action="${administration}${saveBudget}"
                       class="form-horizontal">

                <div class="input-group">
                    <span class="input-group-addon">${budget.libelle}</span>
                    <input id="budget"
                           type="text" class="form-control"
                           name="budget" value="${budget.valeur}"/>
                    <span class="input-group-addon">€</span>
                </div>

                <input type="hidden" name="id" value="${budget.id}">

                <div class="form-group">
                    <!-- Colonne vide pour aligner le bouton aux champs de saisies -->
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <input class="btn btn-success" type="submit" value="Sauvegarder">
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>


<div class="center-block ">
    <div class="panel-primary col-md-4">
        <div class="panel-heading text-center">Édition de la TVA par
            frais
        </div>
        <div class="panel-body">
            <form:form method="post"
                       action="${administration}${saveTVAForTypeFrais}"
                       class="form-horizontal" modelattribute="TVAForm">
                <c:forEach items="${listTypeFrais}" var="fraisTVA"
                           varStatus="status">
                    <div class="form-group">
                        <label class="col-md-4 control-label">${fraisTVA.typeFrais}</label>
                        <div class="col-md-6">
                            <select name="listTypeFrais[${status.index}].tva.id"
                                    class="form-control">
                                <c:forEach items="${listTva}" var="typeTVA">

                                    <option value="${typeTVA.id}"
                                        <c:if test="${ typeTVA.tva == fraisTVA.tva.tva}">
                                            <c:out value="selected=\"selected\""/>
                                        </c:if>>
                                            ${typeTVA.tva}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <input type="hidden" name="listTypeFrais[${status.index}].id"
                               value="${fraisTVA.id}"/>
                        <!-- CHAMP CODE TVA -->
                        <input type="hidden" name="listTypeFrais[${status.index}].code"
                               value="${fraisTVA.code}"/>
                    </div>
                </c:forEach>
                <div class="form-group">
                    <!-- Colonne vide pour aligner le bouton aux champs de saisies -->
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <input class="btn btn-success" type="submit" value="Sauvegarder" style="margin-bottom:100px;">
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<br/>
<br/>
<!-- TEMPLATE AUTRE PANEL -->
<!-- <div class="center-block ">
<div class="panel-primary">
<div class="panel-heading text-center">Autre panel</div>
<div class="panel-body">
<p>Ceci est un autre panel</p>
</div>
</div>
</div> -->
