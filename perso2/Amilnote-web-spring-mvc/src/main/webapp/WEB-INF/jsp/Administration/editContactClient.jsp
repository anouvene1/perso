<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript">
    function checkboxResp() {
        var respClient = document.getElementById("idresp_client");
        var respFacturation = document.getElementById("idresp_facturation");
        var $modalWarning = $("#modalWarning");

        if ((!respClient.checked) && (!respFacturation.checked)) {
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Au moins une case doit être cochée.\nVeuillez choisir un rôle pour ce contact.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            return false;
        }
        else
            return true;
    }
</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${administration}${ clients }">Clients</a></li>
    <li><a
        href="${administration}${editClient}/${ client.id }">Édition
        du client</a></li>
    <c:choose>
        <c:when test="${ !empty contactClient.id }">
            <li class="active">Édition du contact</li>
        </c:when>
        <c:otherwise>
            <li class="active">Création d'un nouveau contact</li>
        </c:otherwise>
    </c:choose>
</ol>


<div id="idProfilContainer">
    <form:form class="form-horizontal" method="POST"
               action="${administration}/${saveContactClient}" commandName="contactClient" name="formcontactClient"
               onsubmit="return checkboxResp()">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center">

                <c:choose>
                    <c:when test="${empty contactClient.id }">
                        Création d'un nouveau contact <br> pour le client ${client.nom_societe}
                    </c:when>
                    <c:otherwise>
                        Modification du contact :   ${ contactClient.nom }  ${ contactClient.prenom }
                    </c:otherwise>
                </c:choose>

            </legend>
            <form:hidden id="idContactClient" path="id" value="${ id }"/>
            <form:hidden path="client.id"/>
            <c:if test="${not empty error}">
                <div class="errorblock">
                        ${error}
                </div>
            </c:if>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idNom" id="nom">Nom<span style="color:red">*</span></label>
                <div class="col-md-4">

                    <form:input path="nom" cssClass="form-control input-md"
                                data-validation-length="2-50" data-validation="length" id="nomContact"
                                name="nomContact"/>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idPrenom">Prénom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idPrenom" path="prenom" type="text"
                                placeholder="" class="form-control input-md"
                                data-validation-length="2-50" data-validation="length"/>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idEmail">Email<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idEmail" path="mail" type="email"
                                placeholder="" class="form-control input-md"
                                data-validation="custom"
                                data-validation-regexp="[a-zA-Z0-9_\.\+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+$"
                                data-validation-help="Vous devez entrer une adresse mail correcte"/>
                </div>
            </div>

            <!-- Telephone-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idTel">Téléphone</label>
                <div class="col-md-4">
                    <form:input id="idTel" path="telephone" type="tel"
                                class="form-control input-md" data-validation="custom"
                                data-validation-regexp="^0[0-9]{9,13}$|^$"
                                data-validation-help="Veuillez entrer un numéro de téléphone valide en 14 chiffres."/>
                </div>
            </div>

            <!-- Poste (resp client ou resp facturation) -->
            <div class="form-group">
                <label class="col-md-4 control-label">Responsable client</label>
                <div class="col-md-4">
                    <form:checkbox class="check-box CheckBoxClass" path="respClient" id="idresp_client"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Responsable facturation</label>
                <div class="col-md-4">
                    <form:checkbox class="check-box CheckBoxClass" path="respFacturation" id="idresp_facturation"/>
                </div>
            </div>

            <!-- Button (Double) -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-8">

                    <a class="btn btn-danger"
                       href="${administration}${editClient}/${ client.id }"
                       role="button">Retour</a>
                    <input type="submit" id="buttonSubmit"
                           value="Valider Contact" class="btn btn-success"/>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>
