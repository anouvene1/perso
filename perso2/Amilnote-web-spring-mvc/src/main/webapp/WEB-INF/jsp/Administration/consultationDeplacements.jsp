<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript"></script>
<!-- Création d'une variable avec la date now() -->
<jsp:useBean id="today" class="java.util.Date"/>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Consultation des demandes de déplacement</li>
</ol>

<script type="text/javascript" src='resources/js/modalCommentaire.js'></script>
<script type="text/javascript" src='resources/js/Administration/validationDeplacements.js'></script>

<div id="idLargeTableContainer">
    <p class="messageSucces">${messageSucces}</p>
    <p class="messageErreur">${messageErreur}</p>

    <h3>Gestion des demandes de déplacement</h3>
    <a href="${administration}${consultationDeplacements}/null" id="lienFiltre">Toutes</a>
    <a href="${administration}${consultationDeplacements}/VA" id="lienFiltre">Validées</a>
    <a href="${administration}${consultationDeplacements}/SO" id="lienFiltre">Soumises</a>
    <a href="${administration}${consultationDeplacements}/RE" id="lienFiltre">Refusées</a>

    <FORM id="formFiltre" method="GET">
        <div class="form-group" style="margin-top:27px">
            <input name="monthYearExtract" id="monthExtract" class="monthPicker form-control" placeholder="MM/AAAA" autocomplete="off">
            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </FORM>

    <!-- Tableau avec la liste des deplacements -->
    <div id="mini-scrolltab" style="height: calc(100% - 375px);">
        <table
            data-toggle="table"
            data-height="520"
            data-search="true"
            data-striped="true"
            data-pagination="true"
            data-page-size=50
            data-page-list="[10, 25, 50, 100, ${listDD.size()}]"
            data-pagination-v-align="top"
            data-pagination-h-align="right"
            data-pagination-detail-h-align="left"
            id="idBootstrapTable"
            data-toolbar="#toolbar"
            data-maintain-selected="true"
            data-cookie="true"
            data-cookie-id-table="saveIdConsultDepl">
            <!-- En tête du tableau et configuration de bootstrapTable -->
            <thead>
            <tr>
                <th data-field="state" data-checkbox="true" data-formatter="stateFormatter"></th>
                <th data-field="id" data-visible="false">id</th>
                <th data-field="code" data-visible="false">code</th>
                <th class="col-md-3" data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur
                </th>
                <th class="col-md-1" data-field="mission" data-sortable="true" data-align="center">Mission</th>
                <th class="col-md-1" data-field="debut" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                    data-align="center">Début
                </th>
                <th class="col-md-1" data-field="fin" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                    data-align="center">Fin
                </th>
                <th class="col-md-0" data-field="nbJours" data-sortable="true" data-align="center" data-valign="middle">
                    Durée
                </th>
                <th class="col-md-1" data-field="lieu" data-sortable="true" data-align="center">Lieu</th>
                <th class="col-md-0" data-field="etat" data-sortable="true" data-align="center">État</th>
                <th class="col-md-2" data-field="motif" data-sortable="true" data-align="center">Motif</th>
                <th class="col-md-0" data-align="center">Visualiser</th>
                <th class="col-md-0" data-align="center">Annuler</th>
                <th class="col-md-0" data-field="valider" data-align="center" data-valign="middle">Valider</th>
                <th class="col-md-0" data-field="refuser" data-align="center" data-valign="middle">Refuser</th>
            </tr>
            </thead>
            <!-- Corps du tableau -->
            <tbody>
            <c:forEach items="${listDD}" var="demandeDeplacement">
                <tr>
                    <td></td> <!-- checkbox -->
                    <td>${demandeDeplacement.id}</td>
                    <td>${demandeDeplacement.etat.code}</td>
                    <td>${demandeDeplacement.collaborateur.nom} ${demandeDeplacement.collaborateur.prenom}</td>
                    <td>${demandeDeplacement.mission.mission}</td>
                    <td><fmt:formatDate value="${demandeDeplacement.dateDebut}" pattern="dd/MM/yyyy"/></td>
                    <td><fmt:formatDate value="${demandeDeplacement.dateFin}" pattern="dd/MM/yyyy"/></td>
                    <td>${demandeDeplacement.nbJours}</td>
                    <td>${demandeDeplacement.lieu}</td>
                    <td>${demandeDeplacement.etat.etat}</td>
                    <td>${demandeDeplacement.motif}</td>
                    <td>
                        <c:choose>
                            <c:when test="${ demandeDeplacement.pdfDeplacement!='null' }">
                                <a href="${administration}${validationDeplacements}/${demandeDeplacement.id}${download}"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="glyphicon glyphicon-eye-close"></span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:if test="${demandeDeplacement.etat.code != 'VA'}">
                            <a class="openModalCommentaire" data-title="Annulation du Déplacement"
                               data-href="${administration}${annulationDeplacements}/${demandeDeplacement.id}">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                            </a>
                        </c:if>
                    </td>
                    <!-- Bouton pour accepter le deplacement -->
                    <td>
                        <c:choose>
                            <c:when test="${demandeDeplacement.etat.code == 'SO'}">
                                <a href="${administration}${validationDeplacements}/${demandeDeplacement.id}${VA}">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                -
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <!-- Bouton pour refuser le deplacement -->
                    <td>
                        <c:choose>
                            <c:when test="${demandeDeplacement.etat.code == 'SO'}">
                                <a class="openModalCommentaire" data-title="Refus du Déplacement"
                                   data-href="${administration}${validationDeplacements}/${demandeDeplacement.id}${RE}">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                -
                            </c:otherwise>
                        </c:choose>
                    </td>

                </tr>
            </c:forEach>
            </tbody>
        </table>

        <!-- boutons de validation multiple -->
        <div class="navbar-form navbar-left">
            <input
                class="btn btn-default btnActionSelection"
                value="Valider la sélection"
                data-action="${administration}${validationsMultiplesDeplacements}${VA}"
            >
            <input
                class="btn btn-default btnActionSelection"
                value="Refuser la sélection"
                data-action="${administration}${validationsMultiplesDeplacements}${RE}"
            >
        </div>
    </div>
</div>
