<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="tab-pane" id="tabFMainSS">
    <br/>
    <div class="col-md-12 text-right" style="font-size: xx-large; float:top;">
        <div class="col-md-6 text-left"></div>
        <div class="col-md-6 text-right">${moisActuel} ${anneeActuel}<br/></div>
    </div>


    <div class="col-md-12 text-center">
        <form class="form-inline" method="post" action="${administration}${factures}/factures#tabFMainSS">
            <div class="form-group">
                <label for="monthExtractFMainSS" id="lblMonthPickerFMainSS">Date voulue : </label>
                <input class="form-control monthPicker" name="monthYearExtract" id="monthExtractFMainSS">
            </div>
            <button class="btn btn-primary" type="submit" name="action"
                    id="btnMonthPickerFMainSS" value="">Valider</button>
        </form>
    </div>
    <br/><br/><br/>

    <div class="col-md-12" id=toolbarBoutonsFacturesALaMainSS">
        <%-- [CLO][AMNOTE 308] Tableau des factures créées à la main - REFONTE - à REPRENDRE ULTERIEUREMENT --%>
        <legend class="text-center">
            <h3>Factures créées à la main pour sous-traitants</h3>
            <p class="headerLabel">Factures sur le modèle des facturables, à créer à la main (avenants, facture
                manquante, etc.)</p>
            <br/>
        </legend>
        <fieldset>
            <div style="margin-bottom: 8px;">
                <label class="col-md-6 control-label text-right" for="listeSoustraitants"
                       style="font-size: larger; margin-top: 5px;"> Choisir un consultant </label>
                <div class="col-md-6 sautLigne">
                    <form:form class="form-horizontal" method="GET" id="listeSoustraitants"
                               modelAttribute="listeSoustraitants">
                        <div class="col-md-6 text-left">
                            <input class="form-control" list="filtreSoustraitantMain"
                                   onChange="loadMission(this, true, 'btnCreateSubcontractor', 'Subcontractor')"/>
                            <datalist style="display:none" name="listeSoustraitants"
                                      path=""
                                      id="filtreSoustraitantMain"
                                      class="form-control"
                                      itemLabel="listeSoustraitants">
                                <c:forEach items="${listeSoustraitants}" var="collaborateur">
                                    <option value="${collaborateur.id} ${collaborateur.nom} ${collaborateur.prenom}"
                                            data-id-manager="${collaborateur.manager.id}"></option>
                                </c:forEach>
                            </datalist>
                        </div>
                    </form:form>
                </div>
                <br/>
                <br/>

                <form:form id="listMissionsClientesDuMoisVouluSS" class="form-horizontal" method="POST"
                           action="${administration}${creationFactureVierge}${FMAIN}/${dateVoulueRefonte}"
                           modelAttribute="listMissionsClientesDuMoisVoulu">
                    <label class="col-md-6 control-label text-right" for="listMissionsClientesDuMoisVouluSubcontractor"
                           style="font-size: larger; margin-top: 5px;">
                        Créer une nouvelle facture à la main pour la mission cliente :
                    </label>
                    <div class="col-md-6 sautLigne">

                        <div class="col-md-6 text-left">
                            <form:select name="listMissionsClientesDuMoisVoulu" class="form-control" path=""
                                         id="listMissionsClientesDuMoisVouluSubcontractor"
                                         placeholder="Sélectionner une mission cliente"
                                         itemLabel="listMissionsClientesDuMoisVouluSS"
                                         onChange="loadCommandes(this, 'btnCreateSubcontractor', 'Subcontractor')">
                                <option value="" disabled selected hidden>Choisir une mission</option>
                                <c:forEach items="${listMissionsClientesDuMoisVouluSS}" var="mission">
                                    <form:option value="${mission.id}">${mission.mission}</form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <!-- Orders -->
                    <label class="col-md-6 control-label text-right" for="listCommandesDuMoisVouluMissionSubcontractor"
                           style="font-size: larger; margin-top: 5px;">
                        Sélectionnez la commande correspondante :
                    </label>
                    <div class="col-md-6">
                        <div class="col-md-6 text-left">
                            <form:select name="listCommandesDuMoisVouluMission" class="form-control" path=""
                                         id="listCommandesDuMoisVouluMissionSubcontractor"
                                         placeholder="Sélectionner la commande associée"
                                         itemLabel="listCommandesDuMoisVouluMission" disabled="true">
                                <c:forEach items="${listCommandesDuMoisVouluMission}" var="commande">
                                    <form:option value="${commande.id}">
                                        ${commande.numero} ${debugmode ? ' -- '.concat(commande) : ''}
                                    </form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                        <div class="col-md-6">
                            <button id="btnCreateSubcontractor" class="btn btn-info" type="submit" disabled="true">
                                Créer
                            </button>
                        </div>
                    </div>

                </form:form>

            </div>


            <!--SBE_AMNOTE-183 POSSIBILITE DE SOUMETTRE LES FACTURES SEULEMENT POUR LES DRH -->
            <sec:authorize access="hasAnyAuthority('DRH')">
                <c:set var="verifSoumissionMain" scope="session" value="0"/>
                <c:forEach items="${listFacturesALaMainSS}" var="itemMain">
                    <c:if test="${itemMain.etat.id != 2 && itemMain.etat.id != 10}">
                        <c:set var="verifSoumissionMain" scope="session" value="1"/>
                    </c:if>
                </c:forEach>
                <c:choose>
                    <c:when test="${verifSoumissionMain==1}">
                        <c:if test="${not empty listFacturesALaMainSS}">
                            <c:set var="nbsFacturesMainSansNum" scope="session" value="0"/>
                            <c:forEach items="${listFacturesALaMainSS}" var="factureMain">
                                <c:if test="${factureMain.numFacture==0}">
                                    <c:set var="nbsFacturesMainSansNum" scope="session" value="1"/>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${nbsFacturesMainSansNum==0}">
                                    <a class="btn btn-danger" onclick="soumissionFactures(3,'sous_traitants')">Soumettre
                                        l'ordre
                                        de facturation à la main </a>
                                </c:when>
                                <c:otherwise>
                                    <a class="btn btn-info"
                                       onclick="openModalNumerotation(3, 'sous_traitants')">Numéroter les
                                        factures à la main </a>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${not empty listFacturesALaMainSS}">
                            <a class="btn btn-default" disabled="true"
                               onclick="soumissionFactures(3,'sous_traitants')">Soumettre
                                l'ordre de facturation à la main </a>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </sec:authorize>

            <!--SBE_AMNOTE-183 POSSIBILITE DE VALIDER LES FACTURES SEULEMENT POUR LA DIRECTION -->
            <sec:authorize access="hasAnyAuthority('DIR')">
                <c:if test="${not empty listFacturesALaMainSS}">
                    <c:set var="verifExistSoumissionMain" scope="session" value="0"/>
                    <c:forEach items="${listFacturesALaMainSS}" var="itemMain">
                        <c:if test="${itemMain.etat.id == 2}">
                            <c:set var="verifExistSoumissionMain" scope="session" value="1"/>
                        </c:if>
                    </c:forEach>

                    <!--affichage du boutton valider si il existe des facture soumises-->
                    <c:choose>
                        <c:when test="${verifExistSoumissionMain==1}">
                            <a class="btn btn-success" onclick="createExcels(3,'sous_traitants')">Valider
                                l'ordre de facturation
                                à la main </a>
                            <a class="btn btn-danger" onclick="refusFactures(3,'sous_traitants')">Refuser
                                l'ordre de facturation
                                à la main </a>
                        </c:when>
                    </c:choose>
                </c:if>
            </sec:authorize>

            <c:set var="verifListeFraisVide" scope="session" value="0"/>
            <c:forEach items="${listFacturesALaMainSS}" var="itemFrais">
                <c:if test="${itemFrais.etat.id != 10}">
                    <c:set var="verifListeFraisVide" scope="session" value="1"/>
                </c:if>
            </c:forEach>
            <c:choose>
                <%-- SBE_AMNOTE-175 Si la liste n'est pas vide, le bouton est clicable, sinon non --%>
                <c:when test="${verifListeFraisVide==1}">
                    <a href="${administration}/factures/genererExcel/sous_traitants/${FMAIN}/${dateVoulueRefonte}"
                       id="exportExcelFacturesALaMain"
                       class="exportExcelFacturesALaMain btn btn-primary"> Exporter </a>

                    <a href="${administration}/factures/csv/${FMAINSSTCSV}/${dateVoulueRefonte}"
                       id="exportFMainCSV"
                       class="btn btn-primary"> Export comptable </a>

                    <a href="${administration}/factures/SaveAllFacturesPDF/sous_traitants/${FMAIN}/${dateVoulueRefonte}"
                       id="pdfFacturesFrais"
                       class="btn btn-warning"> Télécharger PDF complet </a>
                </c:when>

                <c:otherwise>
                    <a id="margeEnBasBoutton" class="exportExcelFacturesALaMain btn btn-default"
                       disabled="true">
                        Exporter </a>
                </c:otherwise>

            </c:choose>
            <br/>
            <br/>
            <table class="table"
                   data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbarFacture"
                   data-pagination="true"
                   data-page-size=20
                   data-page-list="[10, 20, 30, 40,50]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-pagination-detail-h-align="left"
                   data-show-multi-sort="true"
                   data-sort-priority='[{"sortName": "nom","sortOrder":"asc"}{"sortName": "id", "sortOrder":"asc"}]'
                   id="idBootstrapTable4"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditCollabSansFacture">
                <thead>
                <tr>
                    <th data-field="id" data-sortable="true" data-align="center">N°</th>
                    <th data-field="nom" data-sortable="true" data-align="center">Client</th>
                    <th data-field="prenom" data-sortable="true" data-align="center">Collaborateur</th>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                    <th data-field="nbjours" data-sortable="true" data-align="center">Quantité</th>
                    <th data-field="tjm" data-sortable="true" data-align="center">P.U.</th>
                    <th data-field="total" data-sortable="true" data-align="center">Total</th>
                    <th data-field="editer" data-sortable="true" data-align="center">Éditer</th>
                    <th data-field="Supprimer" data-sortable="true" data-align="center">Supprimer</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${listFacturesALaMainSS}" var="facture">
                    <tr>
                        <td><c:out value="${facture.numFacture}"/></td>
                        <td><c:out value="${facture.mission.client.nom_societe}"/></td>
                        <td><c:out value="${facture.mission.collaborateur}"/></td>
                        <td><c:out value="${facture.mission.mission}"/></td>
                        <c:choose>
                            <c:when test="${facture.commande!=null}">
                                <td><c:out value="${facture.commande.numero}"/></td>
                            </c:when>
                            <c:otherwise>
                                <td>Suivant proposition commerciale</td>
                            </c:otherwise>
                        </c:choose>
                        <td><c:out value="${mapFactureFMainSS[facture.id].first}"/></td>
                        <td><c:out value="${facture.prix}"/></td>
                        <td><c:out value="${mapFactureFMainSS[facture.id].second}"/></td>
                        <c:choose>
                            <c:when test="${facture.etat.id == 2}">
                                <td><a href="${administration}${afficher}/${facture.id}"
                                       disabled="true" target="_blank">
                                    <a disabled="true"></a>
                                    <span class="glyphicon glyphicon-edit" disabled="true"></span>
                                </a></td>

                                <td><span class="glyphicon glyphicon-ban-circle"></span></td>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    <a href="${administration}${editFacture}/<c:out value="${facture.id}"/>">
                                        <span class="glyphicon glyphicon-edit "></span>
                                    </a>
                                </td>
                                <td>
                                    <a href="${administration}${deleteFacture}/<c:out value="${facture.id}"/>"
                                       data-confirm="Etes-vous certain de vouloir supprimer la facture ${facture.numFacture} ?">
                                        <span class="glyphicon glyphicon-remove "></span>
                                    </a>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </fieldset>
    </div>

</div>
