<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>


<%--<div>HELLO</div>--%>
<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${administration}${gestionCommandes}">Commandes</a></li>
    <li class="active">Création de la commande</li>
</ol>


<div id="idProfilContainer">
    <form:form class="form-horizontal" method="POST"
               action="${administration}${gestionCommandes}"
               commandName="commande">
        <%--onsubmit="testForm(this)"--%>
        <fieldset>

            <div class="form-group">
                <label class="col-md-4 control-label" for="collaborateur">Collaborateur</label>
                <div class="col-md-4">
                    <form:select name="collaborateur" class="form-control" path="collaborateur.nom"
                                 items="${listCollab}" itemLabel="collaborateur" itemValue="collaborateur">
                    </form:select>
                </div>
            </div>

            <div>FEKLUGEZFLIUGEFIMEZGF</div>

                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="nom">Nom de la commande</label>
                    <div class="col-md-4">
                        <form:input path="nom" cssClass="form-control input-md"
                                    data-validation-length="2-50" data-validation="length" id="nomCommande" name ="nomCommande"/>
                    </div>
                </div>--%>

                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="numero">Numero de commande</label>
                    <div class="col-md-4">
                        <form:input id="numero" path="numero" type="text"
                                    placeholder="numero de la commande client" class="form-control input-md"
                        />
                    </div>
                        &lt;%&ndash;value = "${commande.numero}"&ndash;%&gt;
                </div>--%>

            <!-- Text input-->
                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="idDateDebut">Date
                        de début</label>
                    <div class="col-md-4">
                        <form:input type="text"
                                    path="dateDebut"
                                    class="form-control input-md datepicker"
                                    id = "dateDebut"
                                    name = "dateDebut"/>

                    </div>
                </div>--%>

                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="idDateFin">Date
                        de fin</label>
                    <div class="col-md-4">

                        <form:input type="text"
                                    path="dateFin"
                                    class="form-control input-md datepicker"
                                    id= "dateFin"
                                    name = "dateFin"/>
                    </div>
                </div>--%>


                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="commentaire">Commentaire</label>
                    <div class="col-md-4">
                        <form:input id="commentaire" path="commentaire" type="text" name="commentaire"
                                    placeholder="Commentaires" class="form-control input-md"
                                    value = "${commande.commentaire}"
                        />
                    </div>
                </div>--%>

                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="typeCommande">Type de commande</label>
                    <div class="col-md-4">
                        <form:select name="typeCommande" class="form-control" path="typeCommande.id"
                                     items="${listTypesCommandes}" itemLabel="typeCommande" itemValue="id">
                        </form:select>
                    </div>
                </div>--%>


            <!-- Button (Double) -->
                <%--<div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <a class="btn btn-danger"
                           href="${administration}${editCollaborateur}/${ collaborateur.id }${ editMission }/${ mission.id }${commandes}"
                           role="button">Retour</a> <input type="submit" id="button1id"
                                                           value="Valider" class="btn btn-success" />
                </div>--%>
                    </div>

        </fieldset>
    </form:form>
</div>
