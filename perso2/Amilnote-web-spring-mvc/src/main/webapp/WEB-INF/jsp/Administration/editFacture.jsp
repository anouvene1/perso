<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%--
  ~ ©Amiltone 2017
  --%>

<script>
    $(function () {
        loadTotal();

        $(document).on('click', '.datepicker, .ui-datepicker-prev, .ui-datepicker-next',
            function() {
            $('.ui-datepicker-calendar').css('display', 'block');
        });
    });
</script>


<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<script type="text/javascript" src='resources/js/Administration/editFacture.js'></script>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${administration}${factures}/null">Factures</a></li>
    <li class="active">Édition de la facture</li>
</ol>


<div id="idProfilContainer">
    <form:form id="editionFacture" class="form-horizontal" method="POST"
               action=""
               modelAttribute="facture">
        <fieldset>
            <legend align="center">
            <c:choose>
                <c:when test="${facture.isAvoir == true}">
                    <c:choose>
                        <c:when test="${facture.id != null && facture.id != 0}">
                            <h3 id="idlabelTitre">Édition de l'avoir</h3>
                        </c:when>
                        <c:otherwise>
                            <h3 id="idlabelTitre">Création d'un avoir</h3>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${facture.id != null && facture.id != 0}">
                            <h3 id="idlabelTitre">Édition de la facture</h3>
                        </c:when>
                        <c:otherwise>
                            <h3 id="idlabelTitre">Création d'une facture</h3>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>


            <form:hidden id="id" path="id" value="${ facture.id }"/>
            <form:hidden id="idMission" name="idMission" path="" value="${ facture.mission.id }"/>

                <c:choose>
                    <c:when test="${commande != null}">
                        <form:hidden id="idCommande" name="idCommande" path="" value="${ commande.id }"/>
                    </c:when>
                </c:choose>
            <form:hidden id="idTypeFacture" name="idTypeFacture" path="" value="${typeFacture}"/>
            </legend>

            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="idIsAvoir">Cocher en cas d'avoir : </label>
                <div class="col-md-2">
                    <input id="idIsAvoir"
                           path=""
                           type="checkbox"
                           name="idIsAvoir"
                           onchange="changeAvoir()"
                            <c:choose>
                                <c:when test="${facture.isAvoir == true}">
                                    checked
                                </c:when>
                            </c:choose>
                    />
                </div>
            </div>
            <%--[EJA][AMNOTE 294] Ajout d'un champ 'N° de facture'--%>
            <div class="form-group">
                <c:choose>
                    <c:when test="${facture.isAvoir == true}">
                        <label class="col-md-4 col-md-offset-4 control-label" for="numFacture" id="labelNumFacture">N° d'avoir : </label>
                    </c:when>
                    <c:otherwise>
                        <label class="col-md-4 col-md-offset-4 control-label" for="numFacture" id="labelNumFacture">N° de facture : </label>
                    </c:otherwise>
                </c:choose>
                <div class="col-md-2">
                    <form:input id="numFacture"
                                path=""
                                type="text"
                                name="numFacture"
                                class="form-control input-md"
                                value="${facture.numFacture}"
                    />
                </div>
            </div>

                <%--[JNA][AMNOTE 160] Date de facturation --%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="dateFacturation">Lyon, le : </label>
                <div class="col-md-4">
                    <form:input id="dateFacturation"
                                path=""
                                type="text"
                                name="dateFacturation"
                                class="form-control input-md datepicker"
                                placeholder="Date de facturation YYYY-MM-DD"
                                autocomplete="off"
                                pattern="^[0-9]{4}-[0-9]{2}-[0-9]{2}$"
                                TITLE="Le format de date n'est pas valide (YYYY-MM-DD)"
                                value="${ facture.dateFacture }"
                    />
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="client">Client : </label>
                <div class="col-md-4">
                    <c:choose>
                        <c:when test="${facture.commande!=null}">
                            <form:input id="client" path="" type="text" name="client"
                                        placeholder="Client" class="form-control input-md"
                                        value="${facture.commande.client.nom_societe}"
                                        readOnly="true"
                            />
                        </c:when>
                        <c:otherwise>
                            <form:input id="client" path="" type="text" name="client"
                                        placeholder="Client" class="form-control input-md"
                                        value="${facture.mission.client.nom_societe}"
                                        readOnly="true"
                            />
                        </c:otherwise>

                    </c:choose>
                </div>
            </div>

                <%--[JNA][AMNOTE 142] Liste des contacts pour le client de la facture--%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="listeContact">A l'attention de : </label>
                <div class="col-md-4">
                    <form:select name="listeContact" class="form-control" path="mission.client.id" id="listeContact"
                                 placeholder="Sélectionner un contact" itemLabel="listeContact">
                        <form:option value="0">Aucun</form:option>
                        <c:forEach items="${listeContact}" var="contact"><%-- les contacts du client --%>
                            <option
                                value="${contact.id}"
                                ${(contact.id eq idContact) ? "selected" : "" }>
                                    ${contact.nom} ${contact.prenom}
                            </option>
                        </c:forEach>

                    </form:select>
                </div>
            </div>

                <%--[JNA][AMNOTE 142] Adresse de facturation si différente de l'adresse du client --%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="adresse">Adresse de facturation : </label>
                <div class="col-md-4">
                    <form:input id="adresseFacturation" path="" type="text" name="adresseFacturation"
                                placeholder="N° et rue, CP et ville" class="form-control input-md"
                                pattern="^[^,]+, ?[0-9]{5}[^,]+$"
                                data-validation-length="1-500" data-validation="length"
                                TITLE="n'oubliez pas la virgule entre l'adresse et le code postal"
                                value="${facture.adresseFacturation}"
                    />
                </div>
            </div>

            <br><br>
                <%--[JNA][AMNOTE 142] N° TVA Amiltone --%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-4 control-label" for="adresse">Notre N° TVA Intrac : </label>
                <div class="col-md-4">
                    <form:input id="adresse" path="mission.client.id" type="text" name="adresse"
                                placeholder="Notre N° TVA Intrac" class="form-control input-md"
                                value="FR23538949108"
                                readOnly="true"
                    />
                </div>
            </div>

                <%--[JNA][AMNOTE 142] N° SIRET Amiltone--%>
            <div class="form-group">
                <label class="col-md-1 control-label" for="adresse">N° Siret : </label>
                <div class="col-md-3">
                    <form:input id="adresse" path="mission.client.id" type="text" name="adresse"
                                placeholder="N° Siret" class="form-control input-md"
                                value="53894910800020"
                                readOnly="true"
                    />

                </div>

                    <%--[JNA][AMNOTE 142] N° TVA Client--%>
                <label class="col-md-4 control-label" for="adresse">Votre N° TVA Intrac : </label>
                <div class="col-md-4">
                    <form:input id="adresse" path="commande.client.tva" type="text" name="adresse"
                                placeholder="Votre N° TVA Intrac" class="form-control input-md"
                                value="${tvaIntrac}"
                                readOnly="true"
                    />

                </div>
            </div>

            <br><br>

            <%-- [CLO AMNOTE-308] Refonte : Champ pour la mission, ne peut pas être modifié --%>

            <div class="form-group">
                <label class="col-md-1 control-label" for="commentaire">Ordre de mission : </label>
                <div class="col-md-3">
                    <form:input id="mission" path="mission.mission" type="text" name="mission"
                                placeholder="Ordre de misison" class="form-control input-md"
                                value="${facture.mission.mission}"
                                readOnly="true"
                    />
                </div>
                
                <label class="col-md-3 control-label" for="adresse">Commande N° : </label>
                <div class="col-md-4" align="center">
                    <c:choose>
                        <c:when test="${commande != null}">
                            <form:input id="numero" path="commande.numero" type="text" name="numero"
                                        placeholder="Numéro commande" class="form-control input-md"
                                        value="${commande.numero}"
                                        readOnly="true"
                            />
                            <c:if test="${commande.justificatif != null}">
                                <a href="${administration}${download}/${commande.id}" target="_blank">
						            <span class="glyphicon glyphicon-eye-open" title="Visualiser"
                                    style="font-size:1.3em">&nbsp;${commande.justificatif}</span>
                                </a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <label class="control-label" style="color: red">Suivant proposition commerciale : </label>
                        </c:otherwise>
                    </c:choose>


                </div>
            </div>

            <br><br>

                <%--[JNA][AMNOTE 142] Titre du tableau de la facture--%>
            <div class="form-group">
                <c:if test="${typeFacture != 2}">
                    <label class="col-md-4 control-label">Quantité</label>
                    <label class="col-md-4 control-label">P.U.</label>
                    <label class="col-md-3 control-label">Montant € HT</label>
                </c:if>
            </div>

            <br>

                <%--[JNA][AMNOTE 142] Num commande de la facture--%>
            <div class="form-group">
                <c:if test="${typeFacture != 2}">
                    <div class="col-md-4">
                        <form:input id="nbJours" path="" type="number" name="quantite"
                                    placeholder="NB jours" class="form-control input-md"
                                    value="${facture.quantite}"
                                    onchange="loadMontant()"
                                    step="any"
                        />
                    </div>
                    <div class="col-md-4">
                        <c:choose>
                            <c:when test="${facture.prix != 0}">
                                <form:input id="tjm" path="" type="number" name="prix"
                                            placeholder="TJM" class="form-control input-md"
                                            value="${facture.prix}"
                                            onchange="loadMontant()"
                                            step="any"
                                />
                            </c:when>
                            <c:otherwise>
                                <form:input id="tjm" path="" type="number" name="prix"
                                            placeholder="TJM" class="form-control input-md"
                                            value="${tauxJournalierMoyen}"
                                            onchange="loadMontant()"
                                            step="any"
                                />
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-3">
                        <form:input id="montant" path="" type="number" name="montant"
                                    placeholder="Montant" class="form-control input-md montant"
                                    value="${facture.montant}"
                                    readOnly="true"
                                    step="any"
                        />
                    </div>
                </c:if>

            </div>

                <%--[JNA][AMNOTE 142] Commentaire de la facture--%>
            <div class="form-group">
                <label class="col-md-1 control-label" for="commentaire">Commentaire :</label>
                <c:if test="${typeFacture != 2}">
                <div class="col-md-3">
					<textarea id="commentaire" path="commentaire" name="commentaire"
                              placeholder="Commentaire" class="form-control input-md">${facture.commentaire}</textarea>
                </div>
                </c:if>
                <c:if test="${typeFacture == 2}">
                    <div class="col-md-3">
					<textarea id="commentaire" path="commentaire" name="commentaire"
                              placeholder="Commentaire" class="form-control input-md">Frais ${collaborateur}</textarea>
                    </div>
                </c:if>
            </div>

            <br>
                <%--[JNA][AMNOTE 142] Bouton frais--%>
            <div class="form-group ajoutPrestations">
                <label class="col-md-2 control-label">Prestation ou frais du mois de :</label>
                <div class="col-md-3 date form_time">
                    <form:input id="moisPrestations"
                                path=""
                                type="text"
                                name="moisPrestations"
                                class="form-control input-md datepicker"
                                placeholder="Date de prestation"
                                autocomplete="off"
                                value="${ facture.moisPrestations }
"
                    />
                </div>
                <label class="col-md-2 control-label">Ajouter une prestation :</label>
                <input type="button" id="button1id"
                       value="Ajouter" class="btn btn-success" onclick="loadFrais()"/>
            </div>

                <%--[JNA][AMNOTE 142] Div contenant tous les frais, rempli par le JS et/ou par frais existant --%>
            <div id="frais" class="form-group">
                <c:forEach items="${listeFrais}" var="frais"
                           varStatus="myIndex"><%-- On affiche les frais de la facture --%>

                    <div id="${frais.idElementFacture}" class="form-group ligneFrais"><label
                        class="col-md-1 control-label" for="libelle${myIndex.index + 1}">Libellé :</label>
                        <div class="col-md-3">
                            <form:input id="libelleFraisSave${frais.idElementFacture}" path="" type="text"
                                        name="libelleElementFacture"
                                        placeholder="Libellé" class="form-control input-md"
                                        value="${frais.libelleElement}"
                                        required="true"
                            />
                        </div>
                        <div class="col-md-3">
                            <form:input id="quantiteFraisSave${frais.idElementFacture}" path="" type="number"
                                        name="quantiteElementFacture"
                                        placeholder="Quantité" class="form-control input-md"
                                        value="${frais.quantiteElement}"
                                        onchange="loadMontantFraisSave('${frais.idElementFacture}')"
                                        step="any"
                            />
                        </div>
                        <div class="col-md-3">
                            <form:input id="puFraisSave${frais.idElementFacture}" path="" type="number"
                                        name="puElementFacture"
                                        placeholder="Prix unitaire" class="form-control input-md"
                                        value="${frais.prixElement}"
                                        onchange="loadMontantFraisSave('${frais.idElementFacture}')"
                                        step="any"
                            />
                        </div>
                        <div class="col-md-1">
                            <form:input id="montantFraisSave${frais.idElementFacture}" path="" type="number"
                                        name="montantElementFacture"
                                        placeholder="Montant" class="form-control input-md montant"
                                        value="${frais.montantElement}"
                                        onchange="loadTotal()"
                                        step="any"
                                        readOnly="true"
                            />
                        </div>
                        <form:hidden name="idElementFacture" path="" value="${frais.idElementFacture}"/>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-default"><span
                                onclick="deletInputFrais(${frais.idElementFacture},${tva})"
                                class="glyphicon glyphicon-remove text-danger" style="font-size: 20px;"></span>
                            </button>
                        </div>
                    </div>
                </c:forEach>
            </div>

            <br><br>
            <div class="form-group">
                <div class="row">
                    <div class="row">

                        <label class="col-md-1 control-label">RIB :</label>


                        <div class="col-md-5">
                            <form:select name="ribFacture" class="form-control" path="" id="ribFacture"
                                         placeholder="Sélectionner un moyen de payement" itemLabel="conditionPaiement">
                                <c:forEach items="${listeRIB}" var="rib">
                                    <c:choose>
                                        <c:when test="${rib == selectedRib}">
                                            <form:option value="${rib}" selected="selected">${rib}</form:option>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option value="${rib}">${rib}</form:option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                    <br>
                    <div class="row">

                        <label class="col-md-1 control-label">TVA :</label>
                        <div class="col-md-5">
                            <form:select name="ChoixTVA" onchange="choixdeTva()" class="form-control" path="" id="TVA"
                                         placeholder="Sélectionner une TVA" itemLabel="TVA" >
                                <c:forEach items="${listeTVA}" var="tva">
                                    <c:choose>
                                        <c:when test="${tva.getId() == selectedTva}">
                                            <form:option value="${tva.getId()}" id="tva_${tva.getId()}" name="${tva.getMontant()}" selected="selected">${tva.getTva()}</form:option>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option value="${tva.getId()}" id="tva_${tva.getId()}" name="${tva.getMontant()}">${tva.getTva()}</form:option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </form:select>
                        </div>

                    </div>
                    <input id ="hiddenTva" name="hiddenTva" hidden="true" value="${selectedTva}"/>

                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-4 col-md-offset-6 control-label" for="ht">Total H.T :</label>

                        <div class="col-md-2">
                            <form:input id="ht" path="" type="text" name="ht"
                                        placeholder="Adresse" class="form-control input-md"
                                        value="${facture.montant}"
                                        readOnly="true"
                            />
                        </div>
                    </div>

                <%--[JNA][AMNOTE 142] Montant TVA--%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-6 control-label" for="tva" id="tauxtva">T.V.A (${tva}) :</label>

                <div class="col-md-2">
                    <form:input id="tva" path="" type="text" name="tva"
                                placeholder="Adresse" class="form-control input-md"
                                value="${facture.montant*tva}"
                                readOnly="true"
                    />
                </div>
            </div>


                <%-- [JNA][AMNOTE 142] Montant Total TTC --%>
            <div class="form-group">
                <label class="col-md-4 col-md-offset-6 control-label" for="total">Total T.T.C :</label>

                <div class="col-md-2">
                    <form:input id="total" path="" type="text" name="total"
                                placeholder="Adresse" class="form-control input-md"
                                value="${facture.montant + facture.montant*(tva)}"
                                readOnly="true"
                    />
                </div>
            </div>
                </div>
            </div>

            <!-- Button (Double) -->
            <br><br>
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <input type="hidden" name="listeJsonElementsFacture" id="listeJsonElementsFacture" value="">

                    <input type="submit" value="Retour" class="btn btn-danger" onclick="canceledit()" />
                                                    <button type="button"
                                                            value="Valider" class="btn btn-success" onclick="checksubmit('adresseFacturation','dateFacturation')">Valider</button>
                    <c:choose>
                        <c:when test="${facture.isAvoir == true}">
                            <a class="btn btn-info"
                               onclick="visuPdfFacture()" target="_blank" id="btnVisualiser"
                            >Visualisez votre avoir</a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-info"
                               onclick="visuPdfFacture()" target="_blank" id="btnVisualiser"
                            >Visualisez votre facture</a>
                        </c:otherwise>
                    </c:choose>

                </div>
            </div>

        </fieldset>
    </form:form>

    <script type="text/javascript">
        function checksubmit(inputAdresse, inputDate) {
            var inputAdressea = document.getElementById(inputAdresse).value
            var inputDatea = document.getElementById(inputDate).value
                if(inputAdressea.match("^[^,]+, ?[0-9]{5}[^,]+$") && inputDatea.match("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")){
                saveFacture()
            }
            return false
        }
    </script>

    <script type="text/javascript">
        function canceledit() {
            var form = document.getElementById("editionFacture");
            form.setAttribute("action", "Administration/factures/null");
            form.setAttribute("target", "");
        }
    </script>
</div>

