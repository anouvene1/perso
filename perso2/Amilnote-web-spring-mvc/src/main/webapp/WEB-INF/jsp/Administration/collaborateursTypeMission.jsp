<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%-- ~ ©Amiltone 2017 --%>
<script type="text/javascript" src="resources/js/Administration/collaborateursTypeMission.js"></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <c:choose>
        <c:when test="${typeTravailleur=='sous_traitant'}">
            <li><a href="${administration}${travailleurs}?type=idTableContainerSS">Consultants</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="${administration}${travailleurs}?type=idTableContainer">Consultants</a></li>
        </c:otherwise>
    </c:choose>
    <li class="active">Type de mission</li>
</ol>

<div id="idTableContainer">
    <c:choose>
        <c:when test="${typeTravailleur=='sous_traitant'}">
            <h3>Mission en cours pour chaque sous traitant</h3>
        </c:when>
        <c:otherwise>
            <h3>Mission en cours pour chaque collaborateur</h3>
        </c:otherwise>
    </c:choose>
    <form id="formFiltre" method="GET">
        <div>
            <input name="monthYearExtract"
                   id="monthExtract"
                   class="monthPicker form-control"
                   placeholder="MM/AAAA"
                   autocomplete="off"
                   value="${monthYearExtract}"
            >
            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </form>
    <div id="scrolltab">
        <table class="table"
               data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbar"
               data-pagination="true"
               data-page-size=100
               data-page-list="[10, 25, 50, 100, ${listeCollaborateursTypeMission.size()}]"
               data-pagination-v-align="top"
               data-pagination-h-align="right"
               data-pagination-detail-h-align="left"
               id="idBootstrapTable"
               data-cookie="true"
               data-cookie-id-table="saveIdCollab">
            <thead>
            <tr>
                <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                <th class="col-md-1" data-field="mission" data-sortable="true" data-align="center">Mission</th>
                <th class="col-md-1" data-field="typeMission" data-sortable="true" data-align="center">Type de Mission
                </th>
                <th class="col-md-1" data-field="dateDebut" data-sortable="true" data-align="center">Date de début</th>
                <th class="col-md-1" data-field="dateFin" data-sortable="true" data-align="center">Date de Fin</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listeCollaborateursTypeMission}" var="collaborateurTypeMission">
                <c:forEach items="${collaborateurTypeMission.listMission}" var="laMission">
                    <tr>
                        <td>${collaborateurTypeMission.collab.nom}</td>
                        <td>${collaborateurTypeMission.collab.prenom}</td>
                        <td>${laMission.mission}</td>
                        <td>${laMission.typeMission.typeMission}</td>
                        <td>${(laMission.dateDebut).toString().split(" ")[0]}</td>
                        <td>${(laMission.dateFin).toString().split(" ")[0]}</td>
                    </tr>
                </c:forEach>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
