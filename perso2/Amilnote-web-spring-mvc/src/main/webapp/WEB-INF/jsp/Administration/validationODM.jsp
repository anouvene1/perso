<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript" src='resources/js/Administration/validationODM.js'></script>
<script type="text/javascript" src='resources/js/librairie.js'></script>

<script>
    //SBE AMNOTE-461 permets l'ouverture d'un pdf en module
    (function(a){a.twModalPDF=function(b){
        defaults={title:"PDF de l'ordre de mission",message:"Impossible d’ouvrir le document PDF",closeButton:true,scrollable:false};
        var b=a.extend({},defaults,b);
        var c=(b.scrollable===true)?'style="max-height: 1020px;overflow-y: auto;"':"";
        html='<div class="modal fade" id="oModalPDF">';html+='<div class="modal-dialog modal-lg">';
        html+='<div class="modal-content">';html+='<div class="modal-header">';
        html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";
        html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";
        html+='<div class="modal-footer">';html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>';html+="</div>";
        html+="</div>";
        html+="</div>";html+="</div>";a("body").prepend(html);
    }})(jQuery);

    $(function(){
        $('.voir-pdf').on('mouseover',function(){
            var sUrl = $(this).attr('href');
            var sTitre = $(this).attr('title');
            var sIframe = '<object type="application/pdf" data="'+sUrl+'" width="100%" height="500">Aucun support</object>';
            $.twModalPDF({
                title:sTitre,
                message: sIframe,
                closeButton:true,
                scrollable:false
            });

            return false;
        });

    })

</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Validation des ordres de mission</li>
</ol>

<div id="idTableContainer">
    <h3>Validation des ordres de mission</h3>
    <div id="scrolltab">
        <table
            data-toggle="table"
            data-height="520"
            data-search="true"
            data-striped="true"
            data-toolbar="#toolbar"
            data-pagination="true"
            data-page-size=10
            data-page-list="[10, 25, 50, 100, ${listMission.size()}]"
            data-pagination-v-align="top"
            data-pagination-h-align="right"
            data-pagination-detail-h-align="left"
            id="idBootstrapTable"
            data-toolbar=":submit"
            data-id-field="id"
            data-maintain-selected="true"
            data-cookie="true"
            data-cookie-id-table="saveIdODM">
            <!-- En tête du tableau et configuration de bootstrapTable -->

            <!-- <th data-field="state" data-checkbox="true"></th> -->
            <thead style="text-align:center">
            <tr>
                <th data-field="state" data-checkbox="true"></th>
                <th data-field="id" data-visible="false">id</th>
                <th data-field="prenom" data-sortable="true" data-align="center">Collaborateur</th>
                <th data-field="mission" data-sortable="true" data-align="center" data-valign="middle">Mission</th>
                <th data-field="visualisation" data-align="center" data-valign="middle">Visualisation</th>
                <th data-field="valider" data-align="center" data-valign="middle">Valider</th>
                <th data-field="refuser" data-align="center" data-valign="middle">Refuser</th>
            </tr>
            </thead>

            <!-- Corps du tableau -->
            <tbody>
            <!-- Boucle sur la liste des deplacements pour afficher chaque ligne -->

            <c:forEach items="${listMission}" var="mission" varStatus="loop">
                <tr>
                    <td></td>
                    <td>${mission.id}</td>
                    <td>${mission.collaborateur}</td>
                    <td>${mission.mission}</td>

                    <!--bouton oeil-->
                    <td>
                        <c:choose>
                            <c:when test="${!empty mission.pdfODM}">
                                <a href="${administration}${validationODM}/${mission.id}${download}" class="voir-pdf" title="Ordre De Mission : ${mission.mission}" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="glyphicon glyphicon-eye-close"></span>
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <!-- Bouton pour accepter le deplacement -->
                    <td>
                        <a href="${administration}${validationODM}/${mission.id}${SO}" onclick="clickAndDisable(this);">
                            <span class="glyphicon glyphicon-ok"></span>
                        </a>
                    </td>
                    <!-- Bouton pour refuser le deplacement -->
                    <td>
                        <a href="${administration}${validationODM}/${mission.id}${RE}" onclick="clickAndDisable(this);">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <div class="navbar-form navbar-left">
            <input
                class="btn btn-default btnActionSelection"
                value="Valider la sélection"
                data-action="${administration}${validationsMultiplesODM}${SO}"
            >
            <input
                class="btn btn-default btnActionSelection"
                value="Refuser la sélection"
                data-action="${administration}${validationsMultiplesODM}${RE}"
            >
        </div>
    </div>
</div>
