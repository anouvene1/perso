<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>
<script type="text/javascript" src='resources/js/modalCommentaire.js'></script>
<script type="text/javascript" src='resources/js/Administration/validationASF.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Notes de frais</li>
</ol>

<div id="idTableContainer">
    <h3>Liste des demandes d'avance sur frais</h3>

    <a href="${administration}${noteDeFrais}/null" id="lienFiltre1">Toutes</a>
    <a href="${administration}${noteDeFrais}/SO" id="lienFiltre2">Soumises</a>
    <a href="${administration}${noteDeFrais}/VA" id="lienFiltre3">Validées</a>
    <a href="${administration}${noteDeFrais}/SD" id="lienFiltre4">Soldées</a>

    <form id="formFiltre" method="GET">
        <div class="form-group" style="margin-top:27px">
            <input name="monthYearExtract"
                   id="monthExtract"
                   class="monthPicker
                   form-control"
                   placeholder="MM/AAAA"
                   autocomplete="off">
            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </form>
    <div id="mini-scrolltab" style="height: calc(100% - 375px);">
        <table
            data-toggle="table"
            data-height="520"
            data-search="true"
            data-striped="true"
            data-pagination="true"
            data-page-size=50
            data-page-list="[10, 25, 50, 100, ${listeNoteDeFrais.size()}]"
            data-pagination-v-align="top"
            data-pagination-h-align="right"
            data-pagination-detail-h-align="left"
            id="idBootstrapTable"
            data-toolbar="#toolbar"
            data-maintain-selected="true"
            data-cookie="true"
            data-cookie-id-table="saveIdNoteDeFrais">

            <thead>
            <tr>
                <th data-field="state" data-align="center" data-checkbox="true" data-formatter="stateFormatter"></th>
                <th data-field="id" data-visible="false">id</th>
                <th data-field="code" data-visible="false">code</th>
                <th class="col-md-1" data-field="type" data-sortable="true" data-align="center">Type</th>
                <th class="col-md-3" data-field="nom" data-sortable="true" data-align="center">Collaborateur</th>
                <th class="col-md-1" data-field="date" data-sortable="true" data-align="center"
                    data-sorter="ddMMyyyyCustomSorter" data-sort-order="desc">Date
                </th>
                <th class="col-md-4" data-field="motif" data-sortable="true" data-align="center">Motif</th>
                <th class="col-md-1" data-field="montant" data-sortable="true" data-align="center">Montant</th>
                <th class="col-md-0" data-field="etat" data-sortable="true" data-align="center">État</th>
                <th class="col-md-0" data-field="visualisation" data-align="center" data-valign="middle">Visualisation
                </th>
                <th class="col-md-0" data-align="center">Solder</th>
                <th class="col-md-0" data-field="valider" data-align="center" data-valign="middle">Valider</th>
                <th class="col-md-0" data-field="refuser" data-align="center" data-valign="middle">Refuser</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listeNoteDeFrais}" var="noteDeFrais">
                <tr>
                    <td></td> <!-- checkbox -->
                    <td>${noteDeFrais.id}</td>
                    <td>${noteDeFrais.etat.code}</td>
                    <td>
                        <c:if test="${noteDeFrais.typeAvance == 'PE'}">Permanente</c:if>
                        <c:if test="${noteDeFrais.typeAvance == 'PO'}">Ponctuelle</c:if>
                    </td>
                    <td>${noteDeFrais.collab}</td>
                    <td><fmt:formatDate value="${noteDeFrais.dateEvenement}" pattern="dd/MM/YYYY"/></td>
                    <td>${noteDeFrais.commentaire}</td>
                    <td>${noteDeFrais.montant} €</td>
                    <td>${noteDeFrais.etat.etat}</td>

                    <!--bouton oeil-->
                    <td>
                        <c:choose>
                            <c:when test="${ noteDeFrais.pdfAvanceFrais!='null' }">
                                <a href="${administration}${validNoteDeFrais}/${noteDeFrais.id}${download}"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="glyphicon glyphicon-eye-close"></span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:if
                            test="${noteDeFrais.typeAvance == 'PE' && noteDeFrais.etat.code =='VA' && noteDeFrais.dateSolde == null}">
                            <a data-title="Solde Avance permanente" title="Solder l'avance" href=""
                               onclick="return openModalDateSolde('${noteDeFrais.id}')">
                                Solder <span class="glyphicon glyphicon-share-alt"></span>
                            </a>
                        </c:if>
                        <c:if
                            test="${(noteDeFrais.typeAvance == 'PO') || (noteDeFrais.typeAvance == 'PE' && noteDeFrais.etat.code !='VA')}">
                            -
                        </c:if>
                    </td>
                    <!-- Bouton pour accepter l'avance -->
                    <td>
                        <c:choose>
                            <c:when test="${ noteDeFrais.etat.code!='SO' }">
                                -
                            </c:when>
                            <c:otherwise>
                                <a data-title="Validation Demande Avance"
                                   href="${administration}${validNoteDeFrais}/${noteDeFrais.id}${VA}">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <!-- Bouton pour refuser l'avance -->
                    <td>
                        <c:choose>
                            <c:when test="${ noteDeFrais.etat.code!='SO' }">
                                -
                            </c:when>
                            <c:otherwise>
                                <a
                                    class="openModalCommentaire"
                                    data-title="Refus de l'avance de frais"
                                    data-href="${administration}${validNoteDeFrais}/${noteDeFrais.id}${RE}"
                                >
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>

                <div class="modal fade" id="warning-missing-parameter" tabindex="-1"
                     role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">Date de solde</h3>
                            </div>
                            <div class="modal-body" id="warning-delete-Collaborateur-body">
                                <h4>Veuillez saisir la date de solde de l'avance permanente.</h4>
                                <br>
                                <div class="form-group">
                                    <form name="solderASFPE" id="solderASFPE" method="POST"
                                          action="${administration}${validNoteDeFrais}/${noteDeFrais.id}/SD">
                                        <label class="col-md-4 control-label" for="dateSolde">Date de solde :</label>
                                        <input id="dateSolde" name="dateSolde" required="required"
                                               data-validation-format="yyyy-mm-dd"
                                               data-validation="date"
                                               data-toggle="validator" type="text"
                                               class="form-control input-md datepicker" value="" autocomplete="off"/>
										<span id="msgDateManquante" style="color:red;
										 display:none">La date de solde est obligatoire !</span>

                                        <div class="modal-footer">
                                            <button type="submit" id="delete-Action" onclick="return validateForm();"
                                                    class="btn btn-success dismiss" data-dismiss="modal">Valider
                                            </button>
                                            <button type="button" class="btn btn-danger dismiss"
                                                    onclick="document.getElementById('msgDateManquante').style.display='none'"
                                                    data-dismiss="modal">Annuler
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            ;
                        </div>
                    </div>
                </div>
            </c:forEach>
            </tbody>
        </table>

        <!-- boutons de validation multiple -->
        <div class="navbar-form navbar-left">
            <input
                class="btn btn-default btnActionSelection"
                value="Valider la sélection"
                data-action="${administration}${validMultipleNoteDeFrais}${VA}"
            >
            <input
                class="btn btn-default btnActionSelection"
                value="Refuser la sélection"
                data-action="${administration}${validMultipleNoteDeFrais}${RE}"
            >
        </div>
    </div>

    <div class="modal fade" id="warning-delete-Action" tabindex="-1"
         role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Refus d'une demande d'avance de frais</h3>
                </div>
                <div class="modal-body" id="warning-delete-Collaborateur-body">
                    <h4>Êtes-vous sûr de vouloir refuser cette demande d'avance de frais ?</h4>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" id="delete-Action"
                            class="btn btn-success dismiss" data-dismiss="modal">Oui
                    </button>
                    <button type="button" class="btn btn-danger dismiss"
                            data-dismiss="modal">Non
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>