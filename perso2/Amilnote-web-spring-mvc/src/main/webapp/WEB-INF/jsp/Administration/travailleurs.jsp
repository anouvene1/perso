<%--
  Created by IntelliJ IDEA.
  User: akhuoy
  Date: 16/02/2018
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="resources/css/travailleurs.css">
<script src="resources/js/Administration/travailleurs.js"></script>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Consultants</li>
</ol>

<div id="menuTravailleur">
    <ul class="nav nav-pills">
        <c:choose>
            <c:when test="${navActive=='idTableContainer'}">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="pill" href="${administration}${collaborateurs}#idTableContainer">
                    Gestion des collaborateurs</a>
                </li>
                <li class="nav-item"><a class="nav-link" data-toggle="pill" href="${administration}${travailleurs}#idTableContainerSS">
                    Gestion des sous-traitants</a>
                </li>
            </c:when>
            <c:otherwise>
                <li class="nav-item"><a class="nav-link" data-toggle="pill" href="${administration}${collaborateurs}#idTableContainer">
                    Gestion des collaborateurs</a>
                </li>
                <li class="nav-item active"><a class="nav-link" data-toggle="pill" href="${administration}${travailleurs}#idTableContainerSS">
                    Gestion des sous-traitants</a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>
</div>

<div class="tab-content">
    <c:choose>
    <c:when test="${navActive=='idTableContainer'}">
    <div id="idTableContainer" class="tab-pane active">
        </c:when>
        <c:otherwise>
        <div id="idTableContainer" class="tab-pane">
            </c:otherwise>
            </c:choose>
            <h3>Liste des collaborateurs</h3>

            <div id="toolbar">
                <a href="${administration}${editTravailleur}/collaborateur"
                   class="btn btn-success">Nouveau collaborateur</a>
            </div>

            <div id="toolbar2">
                <a href="${administration}${collaborateurTypeMission}/collaborateurs"
                   class="btn btn-success">Type de mission par collaborateur</a>
                <a href="${administration}/exportConsultants/collab" class="btn btn-info"
                   title="Télécharge un tableau Excel contenant les collaborateurs">
                    Export collaborateurs
                </a>
                <form style="display: inline-block;" method="post" action="/csv/fraiscollaborateurs">
                    <input class="monthPicker hasDatePicker" name="monthYearExtract" id="monthExtractFMain">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" id="btnMonthPickerFMain" value="" >Export des frais</button>
                </form>
            </div>

            <div id="checkboxDiv">
                <label>Afficher les collaborateurs désactivés: </label>
                <c:choose>
                    <c:when test="${isChecked}">
                        <input type="checkbox" id="idDisabledCollab" onclick='handleClick(this, "travailleurs", "?type=idTableContainer");' checked>
                    </c:when>
                    <c:otherwise>
                        <input type="checkbox" id="idDisabledCollab" onclick='handleClick(this, "travailleurs", "?type=idTableContainer");'>
                    </c:otherwise>
                </c:choose>
            </div>

            <!-- data-height: Si on met juste 0: les colonnes ne sont pas fixes ==>..%: rempli toute la div et garde les colones fixes-->
            <div id="scrolltab">
                <table class="table"
                       data-toggle="table"
                       data-height="520"
                       data-search="true"
                       data-striped="true"
                       data-toolbar="#toolbar"
                       data-pagination="true"
                       data-page-size=100
                       data-page-list="[10, 25, 50, 100, ${listeCollaborateurs.size()}]"
                       data-pagination-v-align="top"
                       data-pagination-h-align="right"
                       data-pagination-detail-h-align="left"
                       id="idBootstrapTable"
                       data-cookie="true"
                       data-cookie-id-table="saveIdCollab">
                    <thead>
                    <tr>
                        <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                        <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                        <th class="col-md-1" data-field="mail" data-sortable="true" data-align="center">Mail</th>
                        <th class="col-md-1" data-field="droit" data-sortable="true" data-align="center">Droit</th>
                        <th class="col-md-1" data-field="state" data-sortable="true" data-align="center">État</th>
                        <th class="col-md-1" data-align="center">Édition</th>
                        <th class="col-md-1" data-align="center">Désactiver</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                        <tr>
                            <td>${collaborateur.nom}</td>
                            <td>${collaborateur.prenom}</td>
                            <td>${collaborateur.mail}</td>
                            <td>${collaborateur.statut.statut}</td>
                            <td class="etat">
                                <c:choose>
                                    <c:when test="${collaborateur.enabled}">
                                        Activé
                                    </c:when>
                                    <c:otherwise>
                                        Désactivé
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="edition">
                                <c:choose>
                                    <c:when test="${collaborateur.enabled}">
                                        <a href="${administration}${editTravailleur}/collaborateur/${collaborateur.id}/null">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="desactivation">
                                <c:choose>
                                    <c:when test="${collaborateur.enabled}">
                                        <a data-title="Désactivation"
                                           title="Désactiver le collaborateur"
                                           href=""
                                           onclick="return openModalDateSortie(
                                                   'warning-delete-Action',
                                                   '${collaborateur.id}',
                                                   'idCollab',
                                                    'disabledCollabForm',
                                                   'idTableContainer')">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a class="reactivatedLink"
                                           title="Réactiver le collaborateur"
                                           href=""
                                           data-typeTravailleur="collaborateur"
                                           data-warning="warning-delete-Action",
                                           data-idTravailleur="${collaborateur.id}"
                                           data-idInputTravailleur="idCollab"
                                           data-formId="disabledCollabForm"
                                           data-idtable="idTableContainer"
                                        >
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                    <div class="modal fade" id="warning-delete-Action" tabindex="-1"
                         role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title">Désactivation d'un collaborateur</h3>
                                </div>
                                <div class="modal-body" id="warning-delete-Collaborateur-body">
                                    <h4>Êtes-vous sûr de vouloir désactiver ce collaborateur ?</h4>
                                    <h5>Attention : Cette action bloquera l'accès à l'application pour ce
                                        collaborateur.</h5>
                                    <br>
                                    <form name="disabledCollabForm" id="disabledCollabForm" method="POST"
                                          action="Administration/enableDisableWorkers/${collaborateur.id}?type=idTableContainer">
                                        <label class="col-md-4 control-label" for="dteSortie">Date de sortie :</label>
                                        <input id="dteSortie" name="dteSortie" required="required"
                                               data-validation-format="yyyy-mm-dd"
                                               data-validation="date"
                                               data-toggle="validator" type="text" class="form-control input-md datepicker"
                                               value="" autocomplete="off"/>
                                        <span id="msgDateManquante" style="color:red; display:none">La date de sortie est obligatoire !</span>
                                        <div class="modal-footer">
                                            <button type="submit" id="delete-Action" onclick="return validateFormDelete('disabledCollabForm', 'msgDateManquante');"
                                                    class="btn btn-success dismiss" data-dismiss="modal">Oui
                                            </button>
                                            <button type="button" class="btn btn-danger dismiss"
                                                    data-dismiss="modal"
                                                    onclick="document.getElementById('msgDateManquante').style.display='none'">Non
                                            </button>
                                        </div>
                                        <input type="hidden" id="idCollab" name="idCollab" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    </tbody>
                </table>
            </div>
        </div>

        <c:choose>
        <c:when test="${navActive=='idTableContainer'}">
        <div id="idTableContainerSS" class="tab-pane">
            </c:when>
            <c:otherwise>
            <div id="idTableContainerSS" class="tab-pane active">
                </c:otherwise>
                </c:choose>

                <h3>Liste des sous-traitants</h3>

                <div id="toolbarSS">
                    <a href="${administration}${editTravailleur}/sous_traitant"
                       class="btn btn-success">Nouveau sous-traitant</a>
                </div>

                <div id="toolbarSS2">
                    <a href="${administration}/typeMission/sous_traitant"
                       class="btn btn-success">Type de mission par sous-traitant</a>
                    <a href="${administration}/exportConsultants/stt" class="btn btn-info"
                       title="Télécharge un tableau Excel contenant les sous-traitants">
                        Export sous-traitants
                    </a>
                </div>

                <div id="checkboxDivSS">
                    <label>Afficher les sous-traitants désactivés : </label>
                    <c:choose>
                        <c:when test="${isChecked}">
                            <input type="checkbox" onclick='handleClick(this, "travailleurs", "?type=idTableContainerSS");' checked>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" onclick='handleClick(this, "travailleurs", "?type=idTableContainerSS");'>
                        </c:otherwise>
                    </c:choose>
                </div>

                <!-- data-height: Si on met juste 0: les colonnes ne sont pas fixes ==>..%: rempli toute la div et garde les colones fixes-->
                <div id="scrolltabSS">
                    <table class="table"
                           data-toggle="table"
                           data-height="520"
                           data-search="true"
                           data-striped="true"
                           data-toolbar="#toolbarSS"
                           data-pagination="true"
                           data-page-size=100
                           data-page-list="[10, 25, 50, 100, ${listeSousTraitant.size()}]"
                           data-pagination-v-align="top"
                           data-pagination-h-align="right"
                           data-pagination-detail-h-align="left"
                           id="idBootstrapTableSS"
                           data-cookie="true"
                           data-cookie-id-table="saveIdStt">
                        <thead>
                            <tr>
                                <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                                <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                                <th class="col-md-1" data-field="raisonSociale" data-sortable="true" data-align="center">Raison Sociale</th>
                                <th class="col-md-1" data-field="mail" data-sortable="true" data-align="center">Mail</th>
                                <th class="col-md-1" data-field="droit" data-sortable="true" data-align="center">Droit</th>
                                <th class="col-md-1" data-field="state" data-sortable="true" data-align="center">État</th>
                                <th class="col-md-1" data-align="center">Édition</th>
                                <th class="col-md-1" data-align="center">Désactiver</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listeSousTraitant}" var="sousTraitant">
                            <tr>
                                <td>${sousTraitant.nom}</td>
                                <td>${sousTraitant.prenom}</td>
                                <td>${sousTraitant.raisonSociale.raisonSociale}</td>
                                <td>${sousTraitant.mail}</td>
                                <td>${sousTraitant.statut.statut}</td>
                                <td class="etat">
                                    <c:choose>
                                        <c:when test="${sousTraitant.enabled}">
                                            Activé
                                        </c:when>
                                        <c:otherwise>
                                            Désactivé
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="edition">
                                    <c:choose>
                                        <c:when test="${sousTraitant.enabled}">
                                            <a href="${administration}${editTravailleur}/sous_traitant/${sousTraitant.id}/null">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="desactivation">
                                    <c:choose>
                                        <c:when test="${sousTraitant.enabled}">
                                            <a data-title="Désactivation"
                                               title="Désactiver le sous-traitant" href=""
                                               onclick="return openModalDateSortie(
                                                       'warning-delete-ActionSS',
                                                       '${sousTraitant.id}',
                                                       'idCollabSS',
                                                       'disabledCollabFormSS',
                                                       'idTableContainerSS')">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a class="reactivatedLink"
                                               title="Réactiver le sous-traitant"
                                               href=""
                                               data-typeTravailleur="sous_traitant"
                                               data-warning="warning-delete-ActionSS",
                                               data-idTravailleur="${sousTraitant.id}"
                                               data-idInputTravailleur="idCollabSS"
                                               data-formId="disabledCollabFormSS"
                                               data-idtable="idTableContainerSS"
                                            >
                                                <span class="glyphicon glyphicon-ok"></span>
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            </c:forEach>
                            <div class="modal fade" id="warning-delete-ActionSS" tabindex="-1"
                                 role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title">Désactivation d'un sous-traitant</h3>
                                        </div>
                                        <div class="modal-body" id="warning-delete-Collaborateur-bodySS">
                                            <h4>Êtes-vous sûr de vouloir désactiver ce sous-traitant ?</h4>
                                            <h5>Attention : Cette action bloquera l'accès à l'application pour ce
                                                sous-traitant.</h5>
                                            <br>
                                            <form name="disabledCollabForm" id="disabledCollabFormSS" method="POST"
                                                  action="Administration${enableDisableWorkers}/${sousTraitant.id}?type=idTableContainerSS">
                                                <label class="col-md-4 control-label" for="dteSortie">Date de sortie :</label>
                                                <input id="dteSortieSS" name="dteSortie" required="required"
                                                       data-validation-format="yyyy-mm-dd"
                                                       data-validation="date"
                                                       data-toggle="validator" type="text" class="form-control input-md datepicker"
                                                       value="" autocomplete="off"/>
                                                <span id="msgDateManquanteSS" style="color:red; display:none">La date de sortie est obligatoire !</span>
                                                <div class="modal-footer">
                                                    <button type="submit" id="delete-ActionSS" onclick="return validateFormDelete('disabledCollabFormSS', 'msgDateManquanteSS');"
                                                            class="btn btn-success dismiss" data-dismiss="modal">Oui
                                                    </button>
                                                    <button type="button" class="btn btn-danger dismiss"
                                                            data-dismiss="modal"
                                                            onclick="document.getElementById('msgDateManquanteSS').style.display='none'">Non
                                                    </button>
                                                </div>
                                                <input type="hidden" id="idCollabSS" name="idCollabSS" value="">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>