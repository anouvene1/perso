<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>
<link rel="stylesheet" href="resources/css/factures.css">
<script type="text/javascript" src='resources/js/Administration/factures.js'></script>
<script type="text/javascript" src="resources/js/Administration/confirm.js"></script>

<c:set var="debugmode" value="${param.debug ne null}"/>

<!-- Création d'une variable avec la date now() -->
<jsp:useBean id="today" class="java.util.Date"/>

<!-- [AMA][AMN 576] les span servent pour la redirection (récupèrent le hash du type de facture traité) -->
<span id="hash"
      style="display:none">${hash}</span> <!-- récupéré via la méthode saveFacture() du controller en cas de création/édition -->
<span id="hashRet"
      style="display:none">${hashRetour}</span> <!-- récupéré via la méthode consultationFactures du controller en cas d'annulation (bouton Retour de l'édition) -->

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<section id="msgErreurExport" class="alert alert-danger" role="alert" hidden="hidden">
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p>Un problème est survenu lors de l'export du fichier.</p>
</section>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Gestion des factures</li>
</ol>

<div id="menuFacture">
    <ul class="nav nav-pills col-md-12" id="mesOnglets">
        <li class="nav-item col-md-2 text-center">
            <a class="nav-link" href="#tabRAnonValide" data-toggle="tab" style="font-weight:bold ;font-size: small">RA
                non valides</a>
        </li>
        <li class="nav-item col-md-2 text-center">
            <a class="nav-link" href="#tabFfrais" data-toggle="tab" style="font-weight:bold ;font-size: small">Facture
                de frais</a>
        </li>
        <li class="dropdown col-md-2 text-center">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="font-weight:bold ;font-size: small">Factures
                collaborateurs
                <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#tabFacturable" data-toggle="tab">Facturables</a></li>
                <li><a href="#tabFMain" data-toggle="tab">Factures créées à la main</a></li>
            </ul>
        </li>
        <li class="dropdown col-md-2 text-center">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="font-weight:bold; font-size: small">Factures
                sous-traitants
                <span class="caret"></span></a>
            <ul class="dropdown-menu">

                <li><a href="#tabFacturableSS" data-toggle="tab">Facturables</a></li>
                <li><a href="#tabFMainSS" data-toggle="tab">Factures créées à la main</a></li>

            </ul>
        </li>
        <li class="pull-right">
            <form action="Administration/downloadFactureRA" method="post">
                <input name="monthYearExtract" id="monthYearExtract" class="monthPicker col-offset-1"
                       style="margin: 15px; text-align: center">
                <button class="btn btn-action purpleHaze" type="submit">Génération RA</button>
            </form>
        </li>
    </ul>
</div>

<div id="idTableContainer" style="margin-bottom: 40px;">
    <div class="tab-content clearfix">

        <jsp:include page="invoice/collaboratorCost.jsp"/>
        <jsp:include page="invoice/collaboratorInvoice.jsp"/>
        <jsp:include page="invoice/collaboratorCreateInvoice.jsp"/>
        <jsp:include page="invoice/subcontractorInvoice.jsp"/>
        <jsp:include page="invoice/subcontractorCreateInvoice.jsp"/>
        <jsp:include page="invoice/invalideActivityReport.jsp"/>

        <br/><br/>
    </div>
</div>

<!-- Modal de génération du numéro factures-->
<div
        id="modalGenerationNumero"
        class="modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${administration}${numeroterFactures}/${dateVoulueRefonte}">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Numéroter les factures: </label><br>
                        <input required name="numFacture" data-validation="number" data-validation-length="2-20"
                               class="form-control" data-validation-allowing="int" id="numFacture">
                    </div>
                </div>
                <input name="typeFacture2" class="form-control" type="hidden" id="typeFacture2" value="">
                <input name="typeDeTravailleur2" class="form-control" type="hidden" id="typeDeTravailleur2" value="">
                <div class="modal-footer">
                    <button id="idConfirmGenerationNUmero" type="submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal de confirmation de soumission des factures -->
<div
        id="soumissionFactures"
        class="modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${administration}${soumettreFactures}/${dateVoulueRefonte}">
                <div class="modal-header" style="text-align:center" ;>
                    <h4>Soumettre les factures du mois</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Êtes-vous sûr de vouloir soumettre les factures de ce mois ? Elles ne seront plus
                            éditables.</label><br>
                    </div>
                </div>
                <input name="typeFacture3" class="form-control" type="hidden" id="typeFacture3" value="">
                <input name="typeDeTravailleur3" class="form-control" type="hidden" id="typeDeTravailleur3" value="">
                <div class="modal-footer">
                    <button id="idConfirmSoumissionFactures" type="submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal de validation des factures -->
<div
        id="modalValidationFacture"
        class="modal fade"
        tabindex="-1"
        role="dialog"
        aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${administration}${validerFactures}/${dateVoulueRefonte}">
                <div class="modal-header" style="text-align:center" ;>
                    <h4>Archiver les factures du mois</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Êtes-vous sûr de vouloir archiver les factures de ce mois ? Elles ne seront plus
                            éditables. </label><br>
                    </div>
                </div>
                <input name="typeFacture" class="form-control" type="hidden" id="typeFacture" value="">
                <input name="typeDeTravailleur" class="form-control" type="hidden" id="typeDeTravailleur" value="">
                <div class="modal-footer">
                    <button id="idConfirmCreateExcels" type="submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal de refus des factures -->
<div id="refusFactures"
     class="modal fade"
     tabindex="-1"
     role="dialog"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${administration}${refuserFactures}/${dateVoulueRefonte}">
                <div class="modal-header" style="text:center" ;>
                    <h4>Refuser les factures du mois</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Commentaire : </label><br>
                        <textarea ROWS="10" COLS="80" name="commentaireRefus" id="commentaireRefus" value=""></textarea>
                    </div>
                </div>
                <input name="typeFacture4" class="form-control" type="hidden" id="typeFacture4" value="">
                <input name="typeDeTravailleur4" class="form-control" type="hidden" id="typeDeTravailleur4" value="">
                <div class="modal-footer">
                    <label class="text-danger">Êtes-vous sûr de vouloir refuser la soumission des factures de ce mois
                        ? </label><br>
                    <button id="idRefusSoumissionFactures" type="submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modale de forçage des factures pour les missions à RA non soumis -->
<div id="forcerFactures"
     class="modal fade"
     tabindex="-1"
     role="dialog"
     aria-hidden="true"
     align="center">
    <div class="modal-dialog-forcerFactures">
        <div class="modal-content">
            <form action="${administration}${forcerFactures}/${dateVoulueRefonte}">
                <div class="modal-header" style="text:center" ;>
                    <h4>Forcer les factures du mois pour lesquelles le collaborateur n'a pas rempli son RA</h4>
                </div>
                <div class="modal-body">
                    <table class="table"
                           data-height="520"
                           data-toggle="table"
                           data-search="true"
                           data-striped="true"
                           data-toolbar="#toolbarCollabSansCommande"
                           data-pagination="true"
                           data-page-size=20
                           data-page-list="[10, 20, 30, 40,50]"
                           data-pagination-v-align="top"
                           data-pagination-h-align="left"
                           data-pagination-detail-h-align="left"
                           data-show-multi-sort="true"
                           data-sort-priority='[{"sortName": "nom","sortOrder":"asc"}{"sortName": "id", "sortOrder":"asc"}]'
                           data-cookie="true"
                           id="idBootstrapTable6"
                           data-cookie-id-table="saveIdEditCollabSansFacture">
                        <thead>
                        <tr>
                            <th data-field="collaborateur" data-sortable="true" data-align="center"
                                style="width: 30px;"> Collaborateur
                            </th>
                            <th data-field="client" data-sortable="true" data-align="center"> Client</th>
                            <th data-field="mission" data-sortable="true" data-align="center"> Mission</th>
                            <th data-field="commande" data-sortable="true" data-align="center"> Commande</th>
                            <th data-field="nombre_de_jours" data-sortable="false" data-align="center"> Nombre de
                                jours
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach items="${listFacturesRANonSoumis}" var="facture">
                            <tr>
                                <td><c:out value="${facture.mission.collaborateur}"/></td>
                                <c:choose>
                                    <c:when test="${facture.commande!=null}">
                                        <td><c:out value="${facture.commande.client.nom_societe}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><c:out value="${facture.mission.client.nom_societe}"/></td>
                                    </c:otherwise>
                                </c:choose>
                                <td><c:out value="${facture.mission.mission}"/></td>
                                <c:choose>
                                    <c:when test="${facture.commande!=null}">
                                        <td><c:out value="${facture.commande.numero}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><span class="manqueCommande"> Suivant proposition commerciale</span></td>
                                    </c:otherwise>
                                </c:choose>
                                <td>
                                    <input type="text" id="quantiteFactureForcee${facture.id}"
                                           name="quantiteFactureForcee${facture.id}"
                                           placeholder="Insérer le nombre de jours travaillés"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="idForcerFactures" type="submit" class="btn btn-success dismiss"> Valider</button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal"> Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>
