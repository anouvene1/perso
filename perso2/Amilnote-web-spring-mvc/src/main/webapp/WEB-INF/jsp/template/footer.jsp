<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  ~ ©Amiltone 2017
  --%>

<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate var="year" value="${now}" pattern="yyyy"/>
<footer class="bluck">Site optimis&eacute; pour Firefox et Chrome - Copyright &copy; Amiltone ${year}</footer>
