<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <title>Réinitialisation du mot de passe</title>
        <link rel="icon" type="image/x-icon"
              href="resources/images/favicon.ico"/>
        <link rel="stylesheet" type="text/CSS" href="resources/css/menu-navigation.css"/>

        <link rel="stylesheet" type="text/CSS"
              href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/CSS" href="resources/css/custom.css"/>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script type="text/javascript" src='resources/js/Login/login.js'></script>
    </head>

    <body onload="readCookieLoginSaisi()">

        <jsp:include page="template/header-login.jsp"/>

        <div id="loginForm" class="panel panel-default">
            <form method='POST' class="form-horizontal" action="resetMdpOublie">

                <!-- Logo -->
                <div class="form-group">
                    <div class="col-sm-12">
                        <a id="amilnote" title='retour accueil application amilnote' href="${welcome}">
                            <img alt="Accueil application amilnote" title="Accueil application amilnote"
                                 src="resources/images/amilnote-logo.png"/>
                        </a>
                    </div>
                </div>

                <!-- Email -->
                <div class="form-group">
                    <Label for="mail" class="col-sm-3 col-md-3 control-label">E-mail <i class="obligated">*</i></Label>
                    <div class="col-sm-8 col-md-7">
                        <input id="mail" class="form-control" type="email" placeholder="Entrez votre adresse mail" required
                               name='mail' pattern="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|" autofocus/><br>
                        <c:if test="${chgtmdpenvoye}">
                            <span class="text text-success">${newMdp}</span>
                        </c:if>
                        <c:if test="${!chgtmdpenvoye}">
                            <span class="text text-danger">${errorNewMdp}</span>
                        </c:if>
                    </div>
                </div>

                <!-- Submit -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-8 col-md-7">
                        <input id="submit" name="submit" type="submit" value="Réinitialiser" class="btn btn-default purpleHaze"/>
                    </div>
                </div>

                <!-- Forgotten password -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-md-offset-3 col-sm-8 col-md-7 text-right">
                        <a href="${welcome}" title="Page d'accueil">Retour</a>
                    </div>
                </div>

            </form>
        </div>

        <jsp:include page="template/footer.jsp"/>
    </body>

</html>
