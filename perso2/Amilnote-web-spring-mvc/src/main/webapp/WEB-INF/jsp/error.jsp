<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Connexion to Amilnote</title>
    <link rel="icon" type="image/x-icon"
          href="${contxtPath}/resources/images/favicon.ico"/>
    <link rel="stylesheet" type="text/CSS" href="${contextPath}/resources/css/menu-navigation.css"/>

    <link rel="stylesheet" type="text/CSS"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/CSS" href="${contextPath}/resources/css/custom.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<body>

<article id="header">

    <nav class="navbar navbar-default bluck" role="navigation">
        <section class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="${contextPath}/welcome">
                    <img src="${contextPath}/resources/images/amilnote-logo-header.png"/>
                </a>
            </div>
        </section>
    </nav>
</article>

<c:if test="${ not empty titre }">
    <div class="errorblock">
        <h4> ${titre} </h4>
    </div>
    <br>
</c:if>

<div class="errorblock">
    ${name}<br>
    ${message}
</div>


<div class="errorblock">
    <p>Une erreur est survenue. Si l'erreur persiste, merci de contacter le support à l'adresse suivante :
        support_appli@amiltone.com<br>

</div>
<br>


<a href="${contextPath}/welcome">
    <button type="button" class="btn btn-default">Retourner à la page d'accueil</button>
</a>


<jsp:include page="template/footer.jsp"/>
</body>

</html>
