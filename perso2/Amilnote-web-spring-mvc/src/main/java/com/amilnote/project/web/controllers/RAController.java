/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.EmailException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.*;

/**
 * Controller des Rapports d'activités
 *
 * @author LSouai
 */
@Controller
@RequestMapping("/Rapport-Activites")
public class RAController extends AbstractController {

    /* SERVICES */
    @Autowired
    private MissionService missionService;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private PDFBuilderRA pdfBuilder;

    @Autowired
    private FraisService fraisService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenenementTimesheetService;

    @Autowired
    private FactureService factureService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private LinkTypeFraisCaracService linkTypeFraisCaracService;

    @Autowired
    private AmiltoneUtils utils;

    @Autowired
    private FileService fileService;

    @Autowired
    private MailService mailService;

    private static final String RA_FILE_STARTNAME = "RA_";


    /**
     * redirection vers la page "Mes Timesheets"
     *
     * @param request the request
     * @param model   the model
     * @return String string
     */
    @RequestMapping(value = "mesTimeSheets", method = {RequestMethod.GET})
    public String mesTimeSheets(HttpServletRequest request, Model model) {
        init(request, model);

        Collaborator collaborateur = utils.getCurrentCollaborator();
        boolean lWithSaturday;
        String lMoisChoisi;

        if (model.containsAttribute("withSaturdayUserChoice")) {
            lWithSaturday = (boolean) model.asMap().get("withSaturdayUserChoice");
        } else {
            int lNbSaturday = linkEvenenementTimesheetService.getCountSaturdaysSinceHistoMaxTimesheet(collaborateur);
            lWithSaturday = (lNbSaturday > 0 ? true : false);
        }
        if (model.containsAttribute("moisChoisi")) {
            lMoisChoisi = (String) model.asMap().get("moisChoisi");
        } else {
            lMoisChoisi = "null";
        }
        String lServletPath = request.getServletPath().substring(1);
        request.setAttribute("origRequestURL", lServletPath);

        model.addAttribute("periodHistoMaxTimeSheetAsMonth", Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR);
        model.addAttribute("withSaturday", lWithSaturday);
        model.addAttribute("moisChoisis", lMoisChoisi);
        return NAV_TIMESHEETS;
    }

    /**
     * Redirection vers le mapping mes timesheets avec un le paramètre
     * "afficherSamedi" en plus
     *
     * @param request            the request
     * @param model              the model
     * @param redirectAttributes the redirect attributes
     * @param withSaturday       Précise s'il faut l'affichage des samedis
     * @param moisChoisi         the mois choisi
     * @return String string
     */
    @RequestMapping(value = "mesTimeSheetsWithSaturday/{withSaturday}/{moisChoisi}", method = RequestMethod.GET)
    public String mesTimeSheetsWithSaturday(HttpServletRequest request, Model model, final RedirectAttributes redirectAttributes, @PathVariable("withSaturday") String withSaturday,
                                            @PathVariable("moisChoisi") String moisChoisi) {
        init(request, model);
        redirectAttributes.addFlashAttribute("withSaturdayUserChoice", withSaturday.equals("true"));
        redirectAttributes.addFlashAttribute("moisChoisi", moisChoisi);
        return Constantes.REDIRECT + NAV_TIMESHEETS;
    }

    /**
     * Renvoie la liste des missions
     *
     * @param pMois  the p mois
     * @param pAnnee the p annee
     * @return missions
     */
    @RequestMapping(value = {"mesTimeSheets/getMissions"}, method = RequestMethod.POST)
    @ResponseBody
    public String getMissions(@RequestParam("mois") int pMois, @RequestParam("annee") int pAnnee) {

        return missionService.findAllMissionsNotFinishForCollaboratorAsJson(utils.getCurrentCollaborator(), pMois, pAnnee);
    }

    /**
     * redirection vers la page "Mes notes de frais" recuperation de la liste
     * des missions liées au collaborateur récupération des forfaits liés a
     * chaque mission
     *
     * @param request the request
     * @param model   the model
     * @param message the message
     * @return String string
     * @throws NamingException         the naming exception
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"mesNotesDeFrais"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String mesNotesDeFrais(HttpServletRequest request, Model model, @RequestParam(value = "message", required = false) String message) throws NamingException, JsonProcessingException {
        init(request, model);

        return NAME_NAV_WELCOME;
    }

    /**
     * ajout d'un nouveau frais depuis la page mesNotesDeFrais
     *
     * @param request      the request
     * @param model        the model
     * @param pNomMission  the p nom mission
     * @param pIdEvenement the p id evenement
     * @param pTypeFrais   the p type frais
     * @param pKm          the p km
     * @param pMontant     the p montant
     * @param pCommentaire the p commentaire
     * @param proofFile      the proof file
     * @param pRedir       the p redir
     * @return String string
     */
    @RequestMapping(value = "/saveNewFrais", method = RequestMethod.POST)
    public String saveNewFrais(HttpServletRequest request, Model model, @RequestParam("mission") String pNomMission, @RequestParam("evenement") Long pIdEvenement,
                               @RequestParam("typeFrais") Long pTypeFrais, @RequestParam(value = "km", required = false) Double pKm, @RequestParam("montant") BigDecimal pMontant,
                               @RequestParam(value = "commentaire", defaultValue = "") String pCommentaire, @RequestParam(value = "justif", required = false) MultipartFile proofFile, RedirectAttributes pRedir) {
        init(request, model);
        // recuperation de la mission, du LinkEvenementTimesheet et du
        // LinkTypeFraisCarac liés au nouveau frais
        LinkEvenementTimesheet pLinkEvtTS = linkEvenenementTimesheetService.findById(pIdEvenement);
        LinkTypeFraisCarac pLinkTFraisCarac = linkTypeFraisCaracService.findById(pTypeFrais);

        // récupération de "arembourser" et du blob de l'image si présente
        List<Object> listToPrepareFrais = utils.prepareDataFrais(pNomMission, pMontant, proofFile, null, pTypeFrais);

        // création du frais
        String message;
        try {
            fraisService.saveNewFrais(proofFile);
            message = "<h4>Enregistrement effectué !</h4> <p>Votre frais a été enregistré avec succes.</p>";
        } catch (Exception e) {
            logger.error(e.getMessage());
            message = e.getMessage();
        }

        // si le message contient "succes" il faudra l'afficher en vert
        if (message.contains("succes")) {
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, message);
            // sinon c'est une erreur
        } else {
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, message);
        }
        // redirection vers la page de frais
        return Constantes.REDIRECT+ NAV_RA_NOTESDEFRAIS;
    }

    /**
     * suppression du/des frais sélectionnés
     *
     * @param pListIdToDelete the p list id to delete
     * @return response entity
     * @throws IOException             the io exception
     */
    @RequestMapping(value = "deleteFrais", method = RequestMethod.POST)
    public ResponseEntity<String> deleteFrais(@RequestBody String pListIdToDelete) throws IOException {

        List<Long> lListIdFraisToDelete = new ArrayList<>();

        // récupération des données stockés dans le tableau json
        ObjectMapper lMapper = new ObjectMapper();
        JsonNode lRootNode = lMapper.readTree(pListIdToDelete);

        // parcours des données récupérées et stockage dans une liste de long
        for (Iterator<JsonNode> lIdNode = lRootNode.elements(); lIdNode.hasNext(); ) {
            lListIdFraisToDelete.add(Long.parseLong(lIdNode.next().path("id").asText()));
        }
        // suppression des frais correspondant à la liste
        String lMessage = fraisService.deleteFrais(lListIdFraisToDelete);

        // constitution du paramètre à retourner
        HttpHeaders lHeaders = new HttpHeaders();
        lHeaders.add("Content-Type", "text/html; charset=utf-8");
        ResponseEntity<String> lEntity = new ResponseEntity<>(lMessage, lHeaders, HttpStatus.OK);
        return lEntity;
    }

    /**
     * modification d'un frais sélectionné
     *
     * @param pIdFrais     the p id frais
     * @param pNomMission  the p nom mission
     * @param pIdEvenement the p id evenement
     * @param pTypeFrais   the p type frais
     * @param pKm          the p km
     * @param pMontant     the p montant
     * @param pCommentaire the p commentaire
     * @param pJustif      the p justif
     * @param pRedir       the p redir
     * @return String string
     */
    @RequestMapping(value = "modifyFrais", method = RequestMethod.POST)
    public String modifyFrais(@RequestParam("frais") Long pIdFrais, @RequestParam("mission") String pNomMission, @RequestParam(value = "evenement", required = false) Long pIdEvenement,
                              @RequestParam("typeFrais") Long pTypeFrais, @RequestParam(value = "km", required = false) Double pKm, @RequestParam(value = "montant", required = false) BigDecimal pMontant,
                              @RequestParam(value = "commentaire", defaultValue = "") String pCommentaire, @RequestParam(value = "justif", required = false) MultipartFile pJustif, RedirectAttributes pRedir) {

        // recuperation de la mission, du LinkEvenementTimesheet et du
        // LinkTypeFraisCarac liés au nouveau frais
        LinkEvenementTimesheet pLinkEvtTS = linkEvenenementTimesheetService.findById(pIdEvenement);
        LinkTypeFraisCarac pLinkTFraisCarac = linkTypeFraisCaracService.findById(pTypeFrais);

        // récupération de "arembourser" et du blob de l'image si présente
        List<Object> listToPrepareFrais = utils.prepareDataFrais(pNomMission, pMontant, pJustif, null, pTypeFrais);

        // modification du frais
        String message = fraisService.modifyFrais(pIdFrais, pMontant, pLinkTFraisCarac, pLinkEvtTS, new BigDecimal(listToPrepareFrais.get(0).toString()), pCommentaire, pKm,
            (String) listToPrepareFrais.get(1));

        // si le message contient "succes" il faudra l'afficher en vert
        if (message.contains("succès")) {
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, message);
            // sinon c'est une erreur
        } else {
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, message);
        }

        // redirection vers la page de frais
        return Constantes.REDIRECT + NAV_RA_NOTESDEFRAIS;
    }

    /**
     * Gets image frais.
     *
     * @param pIdFrais the p id frais
     * @param response the response
     * @throws IOException             the io exception
     */
    //TODO utiliser pathVariable pour idFrais (et en Long) pour simplifier et utiliser vraiment SpringMVC! modifier aussi l'appel depuis la vue pour ne plus faire du POST (cette méthode est un GET!!)
    //TODO supprimer cette méthode: semble ne pas etre utilisée (à confirmer)
    @RequestMapping(value = "getImageFrais", method = RequestMethod.POST)
    public void getImageFrais(@RequestBody String pIdFrais, HttpServletResponse response) throws IOException {
        // récupération des données stockés dans le tableau json
        ObjectMapper lMapper = new ObjectMapper();
        JsonNode lRootNode = lMapper.readTree(pIdFrais);
        logger.debug(lRootNode.asText());

        // ecriture de la rÃ©ponse selon le format de l'inputStream
        // si c'est un pdf
        if (fraisService.getPictureForFrais(Long.parseLong(lRootNode.asText())).getFirst().equals("application/pdf")) {
            response.getWriter()
                .print("data:application/pdf;base64," + Base64.getEncoder().encode(IOUtils.toByteArray(fraisService.getPictureForFrais(Long.parseLong(lRootNode.asText())).getSecond())));
            // si c'est un jpeg
        } else {
            response.getWriter()
                    .print("data:image/png;base64," + Base64.getEncoder().encode(IOUtils.toByteArray(fraisService.getPictureForFrais(Long.parseLong(lRootNode.asText())).getSecond())));
        }

    }

    /**
     * création du PDF de rapport d'activités pour le mois courant et envoi par
     * mail
     *
     * @param pDateRAVoulue      the p date ra voulue
     * @param pAvecFrais         the p avec frais
     * @param redirectAttributes the redirect attributes
     * @return ModelAndView string
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     * @throws NamingException    the naming exception
     */
    @RequestMapping(value = "soumissionRA/{dateRAVoulue}/{avecFrais}", method = RequestMethod.GET)
    public String soumissionRA(
            @PathVariable("dateRAVoulue") String pDateRAVoulue,
            @PathVariable("avecFrais") String pAvecFrais,
            RedirectAttributes redirectAttributes)
            throws IOException, SchedulerException, EmailException, MessagingException, DocumentException, NamingException {

        boolean avecFrais = false;
        if ("oui".equals(pAvecFrais)) {
            avecFrais = true;
        } else if ("non".equals(pAvecFrais)) {
            avecFrais = false;
        }

        Collaborator collaborateur = utils.getCurrentCollaborator();
        DateTime dateRAVoulue = new DateTime(pDateRAVoulue);
        List<RapportActivites> listRA = rapportActivitesService.getExistingRAForMonth(collaborateur, dateRAVoulue);
        String reponse;

        Etat etatSoumis = etatService.getEtatByCode(Etat.ETAT_SOUMIS_CODE);
        Etat etatValide = etatService.getEtatByCode(Etat.ETAT_VALIDE_CODE);

        //Si on n'a aucun résultat. Cela signifie que... tu vivras ta vie. Sans aucun soucis ! Hakuna matata (dsl)
        //si pAvecFrais="oui" alors on soumet le RA tout de suite
        //si pAvecFrais="non" alors on met à l'état BR (brouillon)

		/* --------Création de l'excel de facturation----------------- */
        //retourfacture = rapportActivitesService.createFacture(collaborateur,dateRAVoulue); //retourne la liste des noms des fichiers excel si une/des factures a/ont été créées

		/* --------Soumission du RA----------------- */
        if (avecFrais) {
            // Si il existe déjà un ra qui a été soumis avec les frais (donc qui
            // est à l'état soumis ou validé
            if (!listRA.isEmpty()) {
                for (RapportActivites ra : listRA) {
                    if (ra.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE) || ra.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)) {
                        redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Un rapport d'activité a déjà été soumis pour ce mois ci. Vous ne pouvez plus l'éditer");
                        return Constantes.REDIRECT + NAV_TIMESHEETS;
                    }
                }
            }
            String nomFichier = pdfBuilder.createAndSavePDF(collaborateur, dateRAVoulue, "collab");
            reponse = rapportActivitesService.saveNewRACollaborator(new Date(), nomFichier, collaborateur, dateRAVoulue, true);

            // [CLO AMNOTE-308] REFONTE - mise à jour facture correspondante
            List<Mission> missions = missionService.findMissionsClientesByMonthForCollaborator(collaborateur, dateRAVoulue);

            String apresSoumissionFacture = "";

            for (Mission mission : missions) {
                // Refactoring nécessaire pour mettre le tout dans une seule méthode
                Float nbJours = rapportActivitesService.getNbJoursTravailles(collaborateur, mission, dateRAVoulue);
                Facture facture = factureService.findByMissionByMonth(mission, dateRAVoulue);
                if (facture != null) {
                    if (facture.getEtat() != etatSoumis && facture.getEtat() != etatValide) {
                        facture.setRaValide(1);
                        // On peut modifier la facture correspondant au RA seulement si elle n'a pas encore été numeroté
                        if (facture.getNumFacture() == 0) {
                            facture.setQuantite(nbJours);
                            facture.setMontant(facture.getQuantite() * facture.getPrix());
                        }
                        factureService.createOrUpdateFacture(facture);
                    } else {
                        Float quantite = facture.getQuantite();
                        if (!quantite.equals(nbJours)) {
                            apresSoumissionFacture = "mauvaiseQuantite";
                        } else if (!"mauvaiseQuantite".equals(apresSoumissionFacture)) {
                            // on ne fini la boucle des mission avec "bonneQuantite" que s'il n'y pas eu de "mauvaiseQuantite"
                            // sur l'une des factures mais que l'une des factures au moins a été soumise
                            apresSoumissionFacture = "bonneQuantite";
                        }
                    }
                }
            }
            // fin refonte

            if (reponse.contains("succes")) {
                //String repertoire = Parametrage.getContext("dossierPDFRA");
                String repertoire = getDossierPDFRA();
                nomFichier = RA_FILE_STARTNAME + collaborateur.getId() + "-" + collaborateur.getNom() + "_" + DateTimeFormat.forPattern(Constantes.DATE_FORMAT_MMYYYY).print(dateRAVoulue);

                // on converti le RA en ZIP car il sera effacé dans la méthode de MailService après l'envoie des mails
                fileService.convertFileToZip(nomFichier + Constantes.EXTENSION_FILE_PDF, Constantes.CONTEXT_FOLDER_RA);

                mailService.sendMailActivityReportSubmit(collaborateur, repertoire, nomFichier, dateRAVoulue, avecFrais, apresSoumissionFacture);

                // si une version SOCIAL (soumis sans frais) existe lors de la validation finale du RA, on le supprime
                File raSocial = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + nomFichier + Constantes.RA_SOCIAL_ZIP_EXTENSION);
                if (raSocial.exists())
                {
                    if (!listRA.isEmpty())
                    {
                        for (RapportActivites ra : listRA)
                        {
                            rapportActivitesService.changePdf(ra, nomFichier + Constantes.EXTENSION_FILE_PDF);
                        }
                    }
                    fileService.deleteFile(raSocial);
                }

                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS,
                    "Votre rapport d'activités pour le mois de "
                        + dateRAVoulue.toString(Constantes.DATE_FORMAT_MMMM_YYYY)
                        + " est en cours de traitement,  aucune modification n'est possible pour ce mois. <br>Vous recevrez très prochainement un e-mail de confirmation dans votre messagerie Amiltone.");

            } else {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR,
                    "Votre rapport d'activités pour le mois de " + dateRAVoulue.toString(Constantes.DATE_FORMAT_MMMM_YYYY) + " n'a pas pu être soumis. Erreur dans la création du PDF.");
            }
            return Constantes.REDIRECT + NAV_TIMESHEETS;

        } else {
            if (!listRA.isEmpty()) {
                for (RapportActivites ra : listRA) {
                    //Si il existe déjà un ra qui a été soumis sans les frais (donc qui est à l'éta brouillon) on ne peut pas le resoumettre
                    if (ra.getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE)) {
                        redirectAttributes.addFlashAttribute(
                            FLASHATTRIBUTE_KEY_ERROR, "Un rapport d'activité a déjà été soumis sans les frais pour ce mois ci");
                        return Constantes.REDIRECT + NAV_TIMESHEETS;
                    }
                }
            }
            String nomFichier = pdfBuilder.createAndSavePDF(collaborateur, dateRAVoulue, "social");
            reponse = rapportActivitesService.saveNewRACollaborator(new Date(), nomFichier, collaborateur, dateRAVoulue, false);

            // [CLO AMNOTE-308] REFONTE - mise à jour facture correspondante
            List<Mission> missions = missionService.findMissionsClientesByMonthForCollaborator(collaborateur, dateRAVoulue);

            String apresSoumissionFacture = "";

            for (Mission mission : missions) {
                Facture facture = factureService.findByMissionByMonth(mission, dateRAVoulue);
                Float nbJours = rapportActivitesService.getNbJoursTravailles(collaborateur, mission, dateRAVoulue);
                if (facture != null) {
                    if (facture.getEtat() != etatSoumis && facture.getEtat() != etatValide) {
                        facture.setRaValide(1);
                        // On peut modifier la facture correspondant au RA seulement si elle n'a pas encore été numeroté
                        if (facture.getNumFacture() == 0) {
                            facture.setQuantite(nbJours);
                            // Calcul du montant total de la facture en prenant en compte les éléments de frais éventuels
                            Float montantTotal = facture.getQuantite() * facture.getPrix();
                            List<ElementFacture> listeElementsFacture = elementFactureService.findByFacture(facture);
                            for (ElementFacture element : listeElementsFacture) {
                                Float elementMontant = element.getMontantElement();
                                montantTotal += elementMontant;
                            }
                            facture.setMontant(montantTotal);
                        }
                        factureService.createOrUpdateFacture(facture);
                    } else {
                        Float quantite = facture.getQuantite();
                        if (quantite != nbJours) {
                            apresSoumissionFacture = "mauvaiseQuantite";
                        } else if (!"mauvaiseQuantite".equals(apresSoumissionFacture)) {
                            // on ne fini la boucle des mission avec "bonneQuantite" que s'il n'y pas eu de "mauvaiseQuantite"
                            // sur l'une des factures mais que l'une des factures au moins a été soumise
                            apresSoumissionFacture = "bonneQuantite";
                        }
                    }
                }
            }
            // fin refonte

            if (reponse.contains("succes")) {
                String repertoire = getDossierPDFRA();
                nomFichier = RA_FILE_STARTNAME + collaborateur.getId() + "-" + collaborateur.getNom() + "_" + DateTimeFormat.forPattern("MMyyyy").print(dateRAVoulue);
                mailService.sendMailActivityReportSubmit(collaborateur, repertoire, nomFichier, dateRAVoulue, avecFrais, apresSoumissionFacture);

                //vérifie si un RA final existe deja (si c'est le cas, il a été annulé puis soumis sans frais)
                // si une version SOCIAL (soumis sans frais) existe lors de la validation finale du RA, on le supprime
                File raFinal = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + nomFichier + Constantes.EXTENSION_FILE_ZIP);
                if (raFinal.exists())
                {
                    fileService.deleteFile(raFinal);
                }
                else{
                    raFinal = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + nomFichier + Constantes.EXTENSION_FILE_PDF);
                    if (raFinal.exists()) {
                        fileService.deleteFile(raFinal);
                    }
                }

                // une fois le mail avec le RA envoyé, on peut convertir le fichier PDF en .ZIP puis l'effacer
                fileService.convertFileToZip(nomFichier.concat(Constantes.RA_SOCIAL_PDF_EXTENSION), Constantes.CONTEXT_FOLDER_RA);
                fileService.deleteFile(nomFichier.concat(Constantes.RA_SOCIAL_PDF_EXTENSION), Constantes.CONTEXT_FOLDER_RA);

                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS,
                    "Votre rapport d'activités pour le mois de "
                        + dateRAVoulue.toString(
                        Constantes.DATE_FORMAT_MMMM_YYYY)
                        + " est soumis sans les frais. Pour une soumission complète, veuillez valider de nouveau votre Rapport d'activités. <br>Vous recevrez très prochainement un e-mail de confirmation dans votre messagerie Amiltone.");
            } else {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR,
                    "Votre rapport d'activités pour le mois de " + dateRAVoulue.toString(Constantes.DATE_FORMAT_MMMM_YYYY) + " n'a pas pu être soumis. Erreur dans la création du PDF.");
            }
            return Constantes.REDIRECT + NAV_TIMESHEETS;
        }

    }


    /**
     * wrap de la fonction statique pour permettre de tester avec Mockito en passant par un Spy
     * Obligation de visibilité package-private pour cette méthode
     * @return location du dossier des PDF RA
     * @throws NamingException si la clé n'existe pas dans le contexte
     */
    String getDossierPDFRA() throws NamingException {
        return Parametrage.getContext("dossierPDFRA");
    }

    /**
     * Test si le timesheet pour le mois donnée en paramètre est editable
     *
     * @param dateTimesheet the date timesheet
     * @return bool false si on peut l'éditer, true sinon
     */
    @RequestMapping(value = "timesheetIsEditable/{dateTimesheet}", method = RequestMethod.GET)
    public
    @ResponseBody
    boolean timesheetIsEditable(@PathVariable("dateTimesheet") String dateTimesheet) {

        Collaborator collaborateur = utils.getCurrentCollaborator();

        boolean existingRA = true;

        List<RapportActivites> listRA = rapportActivitesService.getExistingRAForMonth(collaborateur, new DateTime(dateTimesheet));

        //Si il n'y a pas de RA pour le mois donné ou si le RA est au statut Brouillon, on peut l'éditer.
        if (listRA.size() == 0 || Etat.ETAT_ANNULE.equals(listRA.get(0).getEtat().getCode()) || Etat.ETAT_REFUSE.equals(listRA.get(0).getEtat().getCode())) {
                existingRA = false;
        }

        // Retourne false si il n'y a pas de RA ou le RA est au statut BR.
        // Retourne vrai sinon
        // Pourquoi l'avoir fait comme ça et pas l'inverse ?????
        return existingRA;
    }

    /**
     * Test si le timesheet pour le mois donnée en paramètre existe
     *
     * @param dateTimesheet the date timesheet
     * @return String string
     */
    @RequestMapping(value = "timesheetIsPresent/{dateTimesheet}", method = RequestMethod.GET)
    public
    @ResponseBody
    String timesheetIsPresent(@PathVariable("dateTimesheet") String dateTimesheet) {

        Collaborator collaborateur = utils.getCurrentCollaborator();

        String existingRA;

        List<RapportActivites> listRA = rapportActivitesService.getExistingRAForMonth(collaborateur, new DateTime(dateTimesheet));

        // Si il n'y a pas de RA pour le mois donné
        if (listRA.size() == 0 || Etat.ETAT_ANNULE.equals(listRA.get(0).getEtat().getCode()) || Etat.ETAT_REFUSE.equals(listRA.get(0).getEtat().getCode())) {
            existingRA = "none";
        }
        // Si il y a un RA
        else {
            existingRA = listRA.get(0).getEtat().getCode();
        }

        return existingRA;
    }

    /**
     * Visualisation ra response entity.
     *
     * @param pDateRAVoulue      the p date ra voulue
     * @return the response entity
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     * @throws NamingException    the naming exception
     */
    @RequestMapping(value = "visualisationRA/{dateRAVoulue}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> visualisationRA(@PathVariable("dateRAVoulue") String pDateRAVoulue)
        throws
        IOException, SchedulerException, EmailException, MessagingException, DocumentException, NamingException {

        Collaborator collaborateur = utils.getCurrentCollaborator();
        DateTime dateRAVoulue = new DateTime(pDateRAVoulue);
        byte[] contents = new byte[]{0};

        String repertoire = Parametrage.getContext("dossierPDFRA");
        String nomFichier;
        nomFichier = pdfBuilder.createPDF(collaborateur, dateRAVoulue, "collab");
        File fic = new File(repertoire + nomFichier);

        if (fic.exists() && fic.isFile()) {
            Path path = fic.toPath();
            contents = Files.readAllBytes(path);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentLength(fic.length());
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + nomFichier);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        // si le fichier visualisé est deja en ZIP, on supprime la version PDF
        if (fileService.fileWasZipped(fic.getPath())) fileService.deleteFile(fic);

        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }

    /**
     * Retourne 'true' si les jours passés en paramètre sont des jours avant le
     * démarrage du collaborateur Retourne 'false' sinon
     *
     * @param lastJourVide the last jour vide
     * @return String string
     */
    @RequestMapping(value = "/beforeJourDemarrage/{lastJourVide}", method = RequestMethod.POST)
    public
    @ResponseBody
    String beforeJourDemarrage(@PathVariable("lastJourVide") String lastJourVide) {
        String beforeDemarrage;
        Collaborator collaborateur = utils.getCurrentCollaborator();

        // récupération de la date de départ de la première mission
        List<MissionAsJson> listMissions = missionService.findAllMissionsCollaborator(collaborateur, Mission.PROP_DATEDEBUT);

        // On récupère la première mission qui ne soit pas la mission Action
        // commercial
        // On ne tiens pas compte de la mission "ActionCommercial" créée par
        // défaut dont la date de début ne correspond pas à la date d'entrée du
        // collab.
        MissionAsJson mission = null;
        for (MissionAsJson missionTemp : listMissions) {
            if (!missionTemp.getMission().equals("Action Commercial")) {
                mission = missionTemp;
                break;
            }
        }

        if (mission != null) {
            // On récupère la date de début de la première mission du
            // collaborateur (=date d'entrée dans la société)
            Date dateDebCollab = mission.getDateDebut();

            // On transforme la chaine de caractère en entrée en date (c'est un
            // string TimeStamp)
            Timestamp dateJourVide = new Timestamp(Long.parseLong(lastJourVide));

            // Si la date du jour vide passé en paramètre est avant la date de
            // commencement du collaborateur alors on retourne true
            // Sinon on retourne false
            if (dateJourVide.before(dateDebCollab)) {
                beforeDemarrage = "true";
            } else {
                beforeDemarrage = "false";
            }
        } else {
            beforeDemarrage = "ActionCommercial";
        }

        return beforeDemarrage;
    }
}
