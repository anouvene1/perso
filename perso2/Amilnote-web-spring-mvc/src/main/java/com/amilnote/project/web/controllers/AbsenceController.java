/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Controller des Rapports d'activités
 *
 * @author LSouai
 */

/**
 * Controller des Rapports d'activités
 *
 * @author LSouai
 */
@Controller
@RequestMapping("/Absences")
public class AbsenceController extends AbstractController {

    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private MailService mailService;

    @Autowired
    private AmiltoneUtils utils;

    private static final String JSON_PRODUCES = "application/json; charset=utf-8";
    /**
     * redirection vers la page "Mes Absence"
     *
     * @param model   the model
     * @return String string
     */
    @GetMapping("/mesAbsences")
    public String mesAbsences(Model model) {
        init(model);

        return NAV_ABSENCE;
    }

    /**
     * Renvoie la liste des Types absences
     *
     * @return String (list AbsenceAsJson)
     */
    @PostMapping(value = "**/getAllTypeAbsence", produces = JSON_PRODUCES)
    @ResponseBody
    public String getAllTypeAbsence() {
        return absenceService.getAllTypeAbsenceAsJson();
    }

    /**
     * Renvoie la liste des états possibles pour une absence
     *
     * @return String (list EtatAsJson)
     */
    @PostMapping(value = "*/getEtatsAbsence", produces = JSON_PRODUCES)
    @ResponseBody
    public String getEtatsAbsence() {
        return etatService.getEtatDomaineAsJson(Etat.ETAT_LISTE_ABENCE);
    }

    /**
     * Build demande absence byte array output stream.
     *
     * @return the byte array output stream
     * @throws DocumentException the document exception
     * @throws IOException       the io exception
     */
    public ByteArrayOutputStream buildDemandeAbsence() throws DocumentException, IOException {
        PdfReader pdfTemplate;
        pdfTemplate = new PdfReader("/Pdf/testTemplateDemandeAbsence.pdf");

        ByteArrayOutputStream fileOutputStream = new ByteArrayOutputStream();

        PdfStamper stamper = new PdfStamper(pdfTemplate, fileOutputStream);

        stamper.close();

        return fileOutputStream;
    }

    /**
     * Renvoie la liste des états possible pour une absence
     *
     * @return String (list AbsenceAsJson)
     */
    @GetMapping(value = "/getAbsenceBrouillon", produces = JSON_PRODUCES)
    @ResponseBody
    public String getAbsenceBrouillon()  {
        return absenceService.getAbsencesByCollaboratorAndEtatBrouillonAsJson(utils.getCurrentCollaborator());
    }

    /**
     * Met à jour les états des absences après leur demande de validation (de brouillon à soumis)
     *
     * @param listId ensemble des id des absences concernées
     * @return un message d'erreur éventuel
     */
    @PostMapping("/updateAbsenceBrouillon")
    @ResponseBody
    public String updateAbsenceBrouillon(@RequestParam(value = "listId") String listId, @RequestParam(value = "comment") String comment) {
        return absenceService.updateAbsencesFromBRToSO(listId, comment);
    }

    /**
     * Permet l'envoi de mail en asynchrone aux responsables pour leur notifier une demande d'absences
     *
     * @param listId  ensemble des id des absences concernées
     * @param comment commentaire à la demande des absences
     * @throws NamingException    the naming exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws IOException        the io exception
     */
    @PostMapping("/sendMailAbsenceSubmission")
    @ResponseBody
    public void sendMailAbsence(@RequestParam(value = "listId") String listId,
                                       @RequestParam(value = "comment") String comment) throws NamingException, EmailException, MessagingException, IOException {
        Collaborator collaborateur = utils.getCurrentCollaborator();
        mailService.envoiMailDemandeAbsences(collaborateur, listId, comment);
    }
}
