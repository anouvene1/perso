/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.utils.AbsencesForm;
import com.amilnote.project.metier.domain.utils.enumerations.MonthsOfYear;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller de la gestion des Absences
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminAbsenceController extends AbstractController {

    private static final String ERROR_ACTIONINCONNUE = "error.actionInconnue";
    private static final String REDIRECT = "redirect:/";
    private static final String VALUE_NULL = "null";
    private static final String SEPARATOR_SLASH = "/";

    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private AmiltoneUtils amiltoneUtils;

    // ---- VALIDATION DES ABSENCES ----//

    /**
     * Recupère toutes les absences soumises et affiche la page de validation des absences
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Strig Représente le nom de la vue "validation absences"
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "validationsAbsences", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationsAbsences(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);
        String mois = request.getParameter("mois");

        List<AbsenceAsJson> listAbsSO = absenceService.getAllAbsencesEtatSO();
        List<AbsenceAsJson> listTemp = new ArrayList<>();
        if (mois != null && !mois.isEmpty()) {
            for (AbsenceAsJson absence : listAbsSO) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(absence.getDateDebut());
                String moisDebut = Integer.toString(calendar.get(Calendar.MONTH));

                calendar.setTime(absence.getDateFin());
                String moisFin = Integer.toString(calendar.get(Calendar.MONTH));

                if (moisDebut.equals(mois) || moisFin.equals(mois) || "-1".equals(mois)) {
                    listTemp.add(absence);
                }
            }
            listAbsSO = listTemp;
        }

        model.addAttribute("listAbsSO", listAbsSO);

        return NAV_ADMIN + NAV_ADMIN_CONSULTATIONABSENCES + "/null";
    }

    /**
     * Validate, reject a given absence and send an email to the collaborator and DRH
     *
     * @param request            {@link HttpServletRequest}
     * @param model              {@link Model}
     * @param absenceId          {@link Long}
     * @param action             {@link String} should have value "Valide" or "Refuse"
     * @param redirectAttributes {@link RedirectAttributes}
     * @param comment            {@link String}
     * @param refererUri         {@link String} url for redirection
     *                           format : /Administration/consultationAbsences?param=stuff
     *                           e.g. via get : ?refererUri=/Administration/consultationAbsences?param=stuff
     *                           e.g. via post : refererUri:/Administration/consultationAbsences?param=stuff
     * @return Redirect to validationsAbsences
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsAbsences/{idAbsence}/{action}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationsAbsences(HttpServletRequest request,
                                      Model model,
                                      @PathVariable("idAbsence") Long absenceId,
                                      @PathVariable("action") String action,
                                      @RequestParam(value = "refererUri", required = false) String refererUri,
                                      RedirectAttributes redirectAttributes,
                                      String comment) throws Exception {
        init(model);

        if (null != absenceId) {
            Collaborator currentCollaborator = amiltoneUtils.getCurrentCollaborator();
            String returnMessage = absenceService.actionValidationAbsence(absenceId, action, comment, currentCollaborator);

            if (!"".equals(returnMessage)) {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, returnMessage);
            } else {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get(ERROR_ACTIONINCONNUE, action));
            }
        }
        return resolveRedir(request, refererUri);
    }

    /**
     * Redirects toward the right page (if you are on refused absences it must returns you to refused absence)
     *
     * @param request    the Request
     * @param refererUri the landing page's url (this parameter is set up in the JavaScript page)
     * @return the landing page's url
     */
    private String resolveRedir(HttpServletRequest request, String refererUri) {

        String defaultRedir = REDIRECT + NAV_ADMIN + NAV_ADMIN_CONSULTATIONABSENCES + SEPARATOR_SLASH + VALUE_NULL;

        // The null parameter is used when the user is currently on the "all absences" page and it differs from the other
        // page codes: "RE" / "SO" / "VA"
        if (refererUri != null && !refererUri.substring(refererUri.length() - 4).equals(VALUE_NULL)) {
            try {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_CONSULTATIONABSENCES + SEPARATOR_SLASH + refererUri.substring(refererUri.length() - 2);
            } catch (Exception e) {
                return defaultRedir;
            }
        }

        return defaultRedir;
    }


    /**
     * Validate or reject a list of absences
     *
     * @param request            {@link HttpServletRequest}
     * @param action             {@link String} should be equal to "Valide" or "Refuse"
     * @param refererUri         {@link String} refererUri
     * @param redirectAttributes {@link RedirectAttributes}
     * @param absencesForm       {@link AbsencesForm} contains the list of absence and the comment
     * @return redirect to validationsAbsences
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsMultiplesAbsences/{action}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationsMultiplesAbsences(
            HttpServletRequest request,
            @PathVariable("action") String action,
            @RequestParam(value = "refererUri", required = false) String refererUri,
            RedirectAttributes redirectAttributes,
            @ModelAttribute("absencesForm") AbsencesForm absencesForm) throws Exception {

        if (!CollectionUtils.isEmpty(absencesForm.getListAbsences())) {
            try {
                List<Long> absenceIds = absencesForm.getListAbsences().stream().map(AbsenceAsJson::getId).collect(Collectors.toList());
                Collaborator currentCollaborator = amiltoneUtils.getCurrentCollaborator();
                String returnMessage = absenceService.actionValidationAbsenceList(absenceIds, action, absencesForm.getCommentaire(), currentCollaborator);
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, returnMessage);
            } catch (Exception e) {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get(ERROR_ACTIONINCONNUE, action));
            }
        } else {
            redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément séléctionné");
        }

        return resolveRedir(request, refererUri);
    }

    /**
     * Display absences by state and/or month/year
     *
     * @param request the request
     * @param model   the model
     * @param state   the state filter
     * @return string
     */
    @RequestMapping(value = "consultationAbsences/{filtre}", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationsAbsences(HttpServletRequest request, Model model, @PathVariable("filtre") String state) {
        init(request, model);

        String month = request.getParameter("mois");
        String year = request.getParameter("annee");
        String monthYear = request.getParameter("monthYearExtract");
        String monthInLetters = null;

        if (monthYear != null && !monthYear.isEmpty()) {
            month = monthYear.substring(0, 2);
            year = monthYear.substring(monthYear.length() - 4);
            monthInLetters = MonthsOfYear.getMonth(Integer.parseInt(month)).name();
        } else {
            if (!"SO".equals(state)) {
                Calendar calendar = Calendar.getInstance();
                month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                year = String.valueOf(calendar.get(Calendar.YEAR));
                monthInLetters = MonthsOfYear.getMonth(Integer.parseInt(month)).name();
            }
        }

        List<Absence> listAbs = absenceService.findAbsencesByStateMonthAndYearOrderByDate(state, month, year);
        List<Absence> listAbsences = absenceService.findDemiJourDebutFinAbsence(listAbs);

        model.addAttribute("moisDuFiltre", monthInLetters);
        model.addAttribute("anneeDuFiltre", year);
        model.addAttribute("listAbsences", listAbsences);
        model.addAttribute("monthYearExtract", monthYear);

        return NAV_ADMIN + NAV_ADMIN_CONSULTATIONABSENCES;

    }

    /**
     * Annulation absences string.
     *
     * @param request            {@link HttpServletRequest}
     * @param model              {@link Model}
     * @param idAbsence          {@link Long}
     * @param redirectAttributes {@link RedirectAttributes}
     * @param comment            {@link String}
     * @param refererUri         {@link String}
     * @return redirect to validationsAbsences
     * @throws Exception the exception
     */
    @RequestMapping(value = "annulationAbsences/{idAbsence}", method = {RequestMethod.GET, RequestMethod.POST})
    public String annulationAbsences(HttpServletRequest request,
                                     Model model,
                                     @PathVariable("idAbsence") Long idAbsence,
                                     @RequestParam(value = "refererUri", required = false) String refererUri,
                                     RedirectAttributes redirectAttributes,
                                     String comment) throws Exception {
        init(model);

        if (null != idAbsence) {
            Collaborator currentCollaborator = amiltoneUtils.getCurrentCollaborator();
            String returnMessage = absenceService.actionValidationAbsence(idAbsence, Etat.ETAT_ANNULE, comment, currentCollaborator);

            if (!"".equals(returnMessage)) {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, returnMessage);
            } else {
                redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get(ERROR_ACTIONINCONNUE, Etat.ETAT_ANNULE));
            }
        }

        return resolveRedir(request, refererUri);
    }


    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


}
