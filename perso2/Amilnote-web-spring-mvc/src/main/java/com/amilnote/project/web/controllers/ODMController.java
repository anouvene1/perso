/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderODM;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Odm controller.
 */
@Controller
@RequestMapping("/mesOrdresDeMission")
public class ODMController extends AbstractController {

    @Autowired
    private MissionService missionService;

    @Autowired
    private MailService mailService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private PDFBuilderODM pdfBuilderODM;

    @Autowired
    private AmiltoneUtils utils;

    /**
     * Ordres de mission string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return the string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "ordresDeMission", method = {RequestMethod.GET, RequestMethod.POST})
    public String ordresDeMission(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        Collaborator collaborateur = utils.getCurrentCollaborator(collaboratorService);
        List<MissionAsJson> listMission = missionService.findAllMissionsCollaborator(collaborateur);
        List<MissionAsJson> tempList = new ArrayList<>();
        for (MissionAsJson mission : listMission) {
            if (mission.getEtat() != null && ((mission.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)) || (mission.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)))) {
                File pdfODM = missionService.getFileODMId(mission.getId());
                if (pdfODM != null) {
                    mission.setpdfODM(pdfODM.getPath());
                }
                tempList.add(mission);
            }
        }

        model.addAttribute("listMission", tempList);
        return NAV_ODM_ORDRESDEMISSION;
    }

    /**
     * Méthode permettant de valider l'ordre de mission coté direction (on rempli le champ de validation dans le pdf de l'odm)
     *
     * @param session     the session
     * @param request     the request
     * @param model       the model
     * @param ra          the ra
     * @param idMission   the id mission
     * @param action      the action
     * @param commentaire the commentaire
     * @return string
     * @throws Exception the exception
     */
    @RequestMapping(value = {"validationODM/{idMission}/{action}"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String validationOrdreDeMission(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                                           @PathVariable("idMission") Long idMission,
                                           @PathVariable("action") String action,
                                           String commentaire) throws Exception {

        init(request, model);
        if (null != idMission) {
            String msgRetour = missionService.updateEtatMission(idMission, action); //action = refusé ou action = validé
            if (!msgRetour.equals("")) {
                Mission mission = missionService.get(idMission);
                String retourPDF = pdfBuilderODM.createPDFODM(mission.getCollaborateur(), mission.toJson());
                if (!retourPDF.contains("Erreur")) {

                    mailService.sendMailMissionOrder(utils.getCurrentCollaborator(), mission, action, commentaire);

                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Action effectuéee avec succès");
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour du pdf");
                }

            } else {
                ra.addFlashAttribute("messageErreur", properties.get("error.actionInconnue", action));
            }
        }
        return "redirect:/" + NAV_ODM_ORDRESDEMISSION;
    }

    /**
     * Retourne le fichier PDF correspondant au déplacement souhaité
     *
     * @param idMission the id mission
     * @param request   the request
     * @param response  the response
     * @param model     the model
     * @param session   the session
     * @param ra        the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationODM/{idMission}/download", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void visualisationOrdreDeMission(@PathVariable("idMission") Long idMission, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                                     RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idMission) {

            //[ALV] [AMNOTE-224] Interdire de visualiser l'ordre de mission si on n'est pas le collaborateur concerné
            //Les rôles supérieurs aux collaborateurs ont toujours l'accès
            Collaborator collaborateur = utils.getCurrentCollaborator(collaboratorService);
            if (StatutCollaborateur.STATUT_COLLABORATEUR.equals(collaborateur.getStatut().getCode()) &&
                missionService.get(idMission).getCollaborateur() != collaborateur) {
                throw new AccessDeniedException("Les collaborateurs non concernés n'ont pas accès à ce fichier.");
            }

            //--- Récupération du fichier PDF
            File lFile = missionService.getFileODMId(idMission);

            //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
            httpResponseConstruct("pdf", response, lFile);
        }
    }
}
