package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by alosat on 26/06/2017.
 */
@Controller
@SessionAttributes
@RequestMapping("/Administration")
public class DisponibiliteCollaborateurController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(DisponibiliteCollaborateurController.class);

    @Autowired
    private AbsenceService abscenceService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private AmiltoneUtils amiltoneUtils;

    private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:ss";

    private static final String ABSENCE_SOUMISE = "soumise";

    private static final String ABSENCE_VALIDE = "valide";

    private static final String MINUIT = "00:00:00";

    private static final String MINUIT_ET_UNE_SECONDE = "00:00:01";

    private static final String HUIT_HEURES = "08:00:00";

    private static final String HEURE_FIN_1 = "11:59:58";

    private static final String HEURE_DEBUT_2 = "11:59:59";

    private static final String MIDI = "12:00:00";

    private static final String TREIZE_HEURES = "13:00:00";

    private static final String DIX_HUIT_HEURES = "18:00:00";

    private static final String HEURE_FIN_2 = "23:59:59";

    private static final String DEBUT_CALENDRIER = "2000/01/01";

    /**
     * Renvoie la page pour consulter la timeline des congés
     *
     * @param model   model
     * @param ra      ra
     * @param session session
     * @return jsp name
     * @throws ParseException ParseException
     */
    @RequestMapping(value = "consultationDisponibilites", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationDisponibilites(HttpSession session, Model model, RedirectAttributes ra) throws ParseException {
        init(model);

        return NAV_ADMIN + NAV_ADMIN_MODULE_DISPONIBILITE;
    }


    /**
     * ajoute au modele l'id du manager
     *
     * @param request request
     * @return idManager
     */
    @ModelAttribute("idManager")
    public String addIDManager(HttpServletRequest request) {
        // On récupère l'id du manager sélectionné
        String idManager = request.getParameter("filtreResponsable");

        return idManager;
    }


    /**
     * ajoute au modele  la liste des collaborateurs du responsable selectionné
     *
     * @param pairGetCollaborateurs pairGetCollaborateurs
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeCollaborateurs")
    public List addCollab(@ModelAttribute("pairGetCollaborateurs") Pair<List<CollaboratorAsJson>, List<CollaboratorAsJson>> pairGetCollaborateurs) {

        List<CollaboratorAsJson> listeCollaborateur = new ArrayList<>();
        listeCollaborateur.addAll(pairGetCollaborateurs.getLeft());
        listeCollaborateur.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listeCollaborateur;
    }


    /**
     * ajoute au modele l'id du collaborateur selectionné
     *
     * @param request request
     * @return idCollaborateur
     */
    @ModelAttribute("idCollaborateur")
    public String addIDCollaborateur(HttpServletRequest request) {
        // On récupère l'id du collaborateur sélectionné
        String idCollaborateur = null;
        try {
            idCollaborateur = request.getParameter("filtreCollaborateur");
        } catch (Exception e) {
            logger.error("[add collaborator id]", e);
        }
        return idCollaborateur;
    }


    /**
     * ajoute au modele la liste de tous les collaborateurs
     *
     * @param pairGetCollaborateurs pairGetCollaborateurs
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeCollaborateursFixe")
    /*public List addCollabSelect(@ModelAttribute("idManager")String idManager, @ModelAttribute("idCollaborateur") String idCollaborateur) {*/
    public List addCollabSelect(@ModelAttribute("pairGetCollaborateurs") Pair<List<CollaboratorAsJson>, List<CollaboratorAsJson>> pairGetCollaborateurs) {
        List<CollaboratorAsJson> listCollaborateurs = pairGetCollaborateurs.getRight();
        listCollaborateurs.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listCollaborateurs;
    }

    /**
     * ajoute au modele le liste des managers
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeManagers")
    public List addListManagers() {
        List<CollaboratorAsJson> listeManagers = collaboratorService.findAllManagersAsJson();
        listeManagers.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listeManagers;
    }

    /**
     * ajoute au modele le liste des directeurs
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeDirecteurs")
    public List addListDirecteurs() {
        List<CollaboratorAsJson> listeDirecteurs = collaboratorService.findAllDirectionAsJson();
        listeDirecteurs.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listeDirecteurs;
    }

    /**
     * ajoute au modele la liste des responsable
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeResponsables")
    public List addListResponsable() {
        List<CollaboratorAsJson> listeResponsables = collaboratorService.findAllResponsablesTechniqueAsJson();
        listeResponsables.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listeResponsables;
    }

    /**
     * ajoute au modele le liste des chefs de projet
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    @ModelAttribute("listeChefDeProjet")
    public List addListChefDeProjet() {
        List<CollaboratorAsJson> listeChefProjet = collaboratorService.findAllChefDeProjetAsJson();
        listeChefProjet.sort((CollaboratorAsJson collab1, CollaboratorAsJson collab2) -> collab1.getNom().trim().compareTo(collab2.getNom().trim()));
        return listeChefProjet;
    }

    /**
     * ajoute au modele la liste des absences soumises
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Absence}
     */
    @ModelAttribute("listeAbsencesSoumises")
    public List addListeAbsencesSoumises(@ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {
        List<Absence> listeAbsenceSoumises = null;
        try {
            listeAbsenceSoumises = getTriple(collaborateurs).getMiddle();
        } catch (Exception e) {
            logger.error("[add submitted absences]", e);
        }
        return listeAbsenceSoumises;
    }

    /**
     * ajoute au modele la liste des absences validées
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Absence}
     */
    @ModelAttribute("listeAbsencesValidees")
    public List addListeAbsencesValidees(@ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {
        List<Absence> listeAbsenceValidees = getTriple(collaborateurs).getRight();
        return listeAbsenceValidees;
    }

    /**
     * ajoute au modele la liste de missions
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Mission}
     */
    @ModelAttribute("listeMissions")
    public List addMission(@ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {
        List<Mission> missions = null;
        try {
            missions = getTriple(collaborateurs).getLeft();
            updateDatesForTimelineSchedulerForMissions(missions);
        } catch (ParseException e) {
            logger.error("[update dates for missions] parse date error", e);
        }
        return missions;
    }

    /**
     * recupere le triplet missions, absence Validees , absence soumises
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a {@link Triple}
     */
    public Triple<List<Mission>, List<Absence>, List<Absence>> getTriple(
            @ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {

        List<Absence> absencesValidees = getListeAbsencesValidees(collaborateurs);
        List<Absence> absencesSoumises = getListeAbsencesSoumises(collaborateurs);
        List<Mission> missions = getMissions();

        // fusionne les elements du même type et qui ont un intervalle de date en commun
        try {
            absencesSoumises = mergeAbsences(absencesSoumises, collaborateurs);
            absencesValidees = mergeAbsences(absencesValidees, collaborateurs);
            // fusionne les elements du même type et qui ont des dates rapprochées

            absencesSoumises = mergeMultipleAbsences(absencesSoumises, collaborateurs);
            absencesValidees = mergeMultipleAbsences(absencesValidees, collaborateurs);


            updateDatesForTimelineSchedulerForAbsences(absencesSoumises);
            updateDatesForTimelineSchedulerForAbsences(absencesValidees);
        } catch (ParseException e) {
            logger.error("[update dates for absences] parse date error", e);
        }
        return cutMissionsForCollaborateursForAbsences(collaborateurs, missions, absencesSoumises, absencesValidees);

    }


    /**
     * recupere toutes les missions
     *
     * @return a list of {@link Mission}
     */
    public List<Mission> getMissions() {
        SimpleDateFormat simpleDateFormatEn = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        String pEndDate = null;
        String pStartDate = null;

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        pEndDate = formatDate(pEndDate, simpleDateFormatEn, calendar);

        //on enleve un mois au dernier mois
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, firstDayOfMonth);
        pStartDate = formatDate(pStartDate, simpleDateFormatEn, calendar);
        // on remplit les listes précédement remplies
        return missionService.findMissionBetweenTwoDates(pStartDate, pEndDate);
    }


    /**
     * recupere la liste des absences validees
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Absence}
     */
    public List<Absence> getListeAbsencesValidees(@ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {
        List<Absence> absencesValidees = new ArrayList<>();
        SimpleDateFormat simpleDateFormatEn = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        String pEndDate = null;
        String pStartDate = null;

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        pEndDate = formatDate(pEndDate, simpleDateFormatEn, calendar);

        //on enleve un mois au dernier mois
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, firstDayOfMonth);
        pStartDate = formatDate(pStartDate, simpleDateFormatEn, calendar);


        //recuperation des absences
        absencesValidees = abscenceService.getAbsencesValideesBetweenDate(pStartDate, pEndDate);
        //assignation des absences pour chaque collaborateur
        absencesValidees = mergeAbsences(absencesValidees, collaborateurs);
        try {
            updateDatesForTimelineSchedulerForAbsences(absencesValidees);
        } catch (ParseException e) {
            logger.error("[update dates for absences] list absences parse date error", e);
        }
        return mergeMultipleAbsences(absencesValidees, collaborateurs);
    }


    /**
     * recupere la liste des absences soumises
     *
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Absence}
     */
    public List<Absence> getListeAbsencesSoumises(@ModelAttribute("listeCollaborateurs") List<CollaboratorAsJson> collaborateurs) {
        List<Absence> absencesSoumises = new ArrayList<>();
        SimpleDateFormat simpleDateFormatEn = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        String pEndDate = null;
        String pStartDate = null;

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        pEndDate = formatDate(pEndDate, simpleDateFormatEn, calendar);

        //on enleve un mois au dernier mois
        calendar.add(Calendar.MONTH, -1);
        calendar.set(Calendar.DAY_OF_MONTH, firstDayOfMonth);

        pStartDate = formatDate(pStartDate, simpleDateFormatEn, calendar);

        //recuperation des absences
        absencesSoumises = abscenceService.getAbsencesSoumisesBetweenDate(pStartDate, pEndDate);
        //assignation des absences pour chaque collaborateur
        absencesSoumises = mergeAbsences(absencesSoumises, collaborateurs);

        return mergeMultipleAbsences(absencesSoumises, collaborateurs);
    }

    /**
     * Renvoie la liste des collaborateurs à afficher sur la timeline et la liste à mettre dans le select
     *
     * @param idManager       idManager
     * @param idCollaborateur idCollaborateur
     * @return new {@link Pair}
     */
    @ModelAttribute("pairGetCollaborateurs")
    /*private Pair<List<CollaborateurAsJson>, List<CollaborateurAsJson>> getCollaborateurs(@ModelAttribute("idManager")String idManager, @ModelAttribute("idcollaborateur") String idCollaborateur) {*/
    private Pair<List<CollaboratorAsJson>, List<CollaboratorAsJson>> getCollaborateurs(String idManager, String idCollaborateur) {

        List<CollaboratorAsJson> listCollaboratorAsJsons = null;//tous les collab
        List<CollaboratorAsJson> listCollaboratorAsJsonsForSelect;//collab dependant d un responsable
        // Si le collaborateur connecté est un ingénieur d'affaires ou un responsable technique
        // On renvoie la liste des collaborateurs sous sa charge
        if (isResponsable() || isDirecteur()) {

            listCollaboratorAsJsons = convertListCollaborateurToJsonList(amiltoneUtils.getCurrentCollaborator(collaboratorService).getCollaborateurs());
            listCollaboratorAsJsonsForSelect = listCollaboratorAsJsons;

            // Sinon on renvoie tous les collaborateurs
        } else {
            listCollaboratorAsJsons = collaboratorService.findAllOrderByNomAscAsJsonWithoutADMINAndDir();
            listCollaboratorAsJsonsForSelect = listCollaboratorAsJsons;

        }
//TODO fusionner les listes car elles renvoient toujours la meme chose
        return new ImmutablePair<>(listCollaboratorAsJsons, listCollaboratorAsJsonsForSelect);
    }

    /**
     * Renvoie true si le collaborateur connecté est un ingénieur d'affaires ou un responsable technique
     *
     * @return true or false
     */
    private boolean isResponsable() {
        return collaboratorService.isManager(amiltoneUtils.getCurrentCollaborator(collaboratorService));
    }

    /**
     * Renvoie true si le collaborateur connecté est un directeur
     *
     * @return true or false
     */
    private boolean isDirecteur() {
        return collaboratorService.isDirecteur(amiltoneUtils.getCurrentCollaborator(collaboratorService));
    }

    /**
     * Renvoie true si le collaborateur connecté est un CP
     *
     * @return true or false
     */
    private boolean isChefDeProjet() {
        return collaboratorService.isChefDeProjet(amiltoneUtils.getCurrentCollaborator(collaboratorService));
    }

    /**
     * Renvoie true si le collaborateur connecté est un responsable technique
     *
     * @returntrue or false
     */
    private boolean isResponsableTechnique() {
        return collaboratorService.isResponsableTechnique(amiltoneUtils.getCurrentCollaborator(collaboratorService));
    }


    /**
     * Renvoie le collaborateur choisi dans une liste
     *
     * @return true or false
     */
    private List<CollaboratorAsJson> convertListCollaborateurToJsonList(List<Collaborator> collaborateurs) {

        List<CollaboratorAsJson> collaboratorAsJsons = new ArrayList<>();

        for (Collaborator collaborateur : collaborateurs) {
            collaboratorAsJsons.add(collaborateur.toJson());
        }

        return collaboratorAsJsons;
    }

    /**
     * Découpe les missions des collaborateurs en fonction des absences
     *
     * @param collaborateurs   a list of {@link CollaboratorAsJson}
     * @param missions         a list of {@link Mission}
     * @param absencesSoumises a list of {@link Absence}
     * @param absencesValidees a list of {@link Absence}
     * @return new {@link MutableTriple}
     * @see Triple
     */
    private Triple<List<Mission>, List<Absence>, List<Absence>> cutMissionsForCollaborateursForAbsences(List<CollaboratorAsJson> collaborateurs, List<Mission> missions, List<Absence> absencesSoumises, List<Absence> absencesValidees) {

        List<Mission> resultsListMission = new ArrayList<>();
        List<Absence> resultsListAbsencesSoumises = new ArrayList<>();
        List<Absence> resultsListAbsencesValidees = new ArrayList<>();

        for (CollaboratorAsJson collaborateur : collaborateurs) {
            cutMissionsForOneCollaborateur(missions, absencesSoumises, absencesValidees, resultsListMission, resultsListAbsencesSoumises, resultsListAbsencesValidees, collaborateur);
        }

        return new MutableTriple<>(resultsListMission, resultsListAbsencesSoumises, resultsListAbsencesValidees);
    }

    /**
     * Découpe la mission d'un collaborateur en fonction de ses absences
     *
     * @param missions                    a list of {@link Mission}
     * @param absencesSoumises            a list of {@link Absence}
     * @param absencesValidees            a list of {@link Absence}
     * @param resultsListMission          a list of {@link Mission}
     * @param resultsListAbsencesSoumises a list of {@link Absence}
     * @param resultsListAbsencesValidees a list of {@link Absence}
     * @param collaborateur               a {@link CollaboratorAsJson}
     */
    private void cutMissionsForOneCollaborateur(List<Mission> missions,
                                                List<Absence> absencesSoumises,
                                                List<Absence> absencesValidees,
                                                List<Mission> resultsListMission,
                                                List<Absence> resultsListAbsencesSoumises,
                                                List<Absence> resultsListAbsencesValidees,
                                                CollaboratorAsJson collaborateur) {

        List<Mission> missionsForCollaborateur = new ArrayList<>();
        List<Absence> absencesSoumisesForCollaborateur = new ArrayList<>();
        List<Absence> absencesValideesForCollaborateur = new ArrayList<>();
        List<Mission> finalMissionList = new ArrayList<>();
        // pour chaque missions si le collaborateur et celui qui est associé à la mission on l ajoute à la liste
        for (Mission mission : missions) {
            if (collaborateur.getId().equals(mission.getCollaborateur().getId())) {
                missionsForCollaborateur.add(mission);
            }
        }
        // trie les missions du collaborateur par date de début
        sortMissions(missionsForCollaborateur);
        // reccupère toutes les absences soumises du collaborateur
        for (Absence absence : absencesSoumises) {
            if (collaborateur.getId().equals(absence.getCollaborateur().getId())) {
                absencesSoumisesForCollaborateur.add(absence);
            }
        }
        // trie les absences par date de debut
        sortAbsences(absencesSoumisesForCollaborateur);
        // reccupere toutes les absences validees du collaborateur
        for (Absence absence : absencesValidees) {
            if (collaborateur.getId().equals(absence.getCollaborateur().getId())) {
                absencesValideesForCollaborateur.add(absence);
            }
        }
        // trie les absences par date de debut
        sortAbsences(absencesValideesForCollaborateur);
        // map contenant les deux listes d'absences
        Map<String, Absence> allAbsencesForCollaborateur = addAbsencesToMap(absencesSoumisesForCollaborateur, absencesValideesForCollaborateur);
        // on decoupe les missions et les missions
        addCutMissionsToList(missionsForCollaborateur, absencesSoumisesForCollaborateur, absencesValideesForCollaborateur, finalMissionList, allAbsencesForCollaborateur);
        // on reccupere tout
        resultsListAbsencesSoumises.addAll(absencesSoumisesForCollaborateur);
        resultsListAbsencesValidees.addAll(absencesValideesForCollaborateur);
        resultsListMission.addAll(finalMissionList);
        missionsForCollaborateur.clear();
        finalMissionList.clear();
        absencesSoumisesForCollaborateur.clear();
        absencesValideesForCollaborateur.clear();
        allAbsencesForCollaborateur.clear();
    }

    /**
     * Rajoute les missions coupées pour les absences
     *
     * @param missionsForCollaborateur         a list of {@link Mission}
     * @param absencesSoumisesForCollaborateur a list of {@link Absence}
     * @param absencesValideesForCollaborateur a list of {@link Absence}
     * @param finalMissionList                 a list of {@link Mission}
     * @param allAbsencesForCollaborateur      a {@link Map}
     */
    private void addCutMissionsToList(List<Mission> missionsForCollaborateur, List<Absence> absencesSoumisesForCollaborateur, List<Absence> absencesValideesForCollaborateur, List<Mission> finalMissionList, Map<String, Absence> allAbsencesForCollaborateur) {
        if (!allAbsencesForCollaborateur.isEmpty()) {
            List<Map.Entry<String, Absence>> sortedAbsences = sortAllAbsencesByDateDebut(allAbsencesForCollaborateur);

            List<Map.Entry<String, Absence>> withoutDuplicatedAbsencesForCollaborateur = deleteDuplicatedAbsences(sortedAbsences);
            // on decoupe les missions en fonction des dates d'absences
            cutMissionForCollaborateurOnAllAbsences(missionsForCollaborateur, finalMissionList, withoutDuplicatedAbsencesForCollaborateur);
            // on clean les listes d'absences du collaborateur avant de passer au collaborateur suivant
            absencesSoumisesForCollaborateur.clear();
            absencesValideesForCollaborateur.clear();

            for (Map.Entry<String, Absence> absenceEntry : withoutDuplicatedAbsencesForCollaborateur) {
                if (absenceEntry.getKey().contains(ABSENCE_SOUMISE)) {
                    absencesSoumisesForCollaborateur.add(absenceEntry.getValue());
                } else {
                    absencesValideesForCollaborateur.add(absenceEntry.getValue());
                }
            }
        } else {
            finalMissionList.addAll(missionsForCollaborateur);
        }
    }

    /**
     * decoupe les missions pour toutes les absences
     *
     * @param missionsForCollaborateur                  a list of {@link Mission}
     * @param finalMissionList                          a list of {@link Mission}
     * @param withoutDuplicatedAbsencesForCollaborateur a {@link List}
     */
    private void cutMissionForCollaborateurOnAllAbsences(List<Mission> missionsForCollaborateur, List<Mission> finalMissionList, List<Map.Entry<String, Absence>> withoutDuplicatedAbsencesForCollaborateur) {
        for (Mission mission : missionsForCollaborateur) {
            for (Map.Entry<String, Absence> absenceEntry : withoutDuplicatedAbsencesForCollaborateur) {
                if (mission.getDateFin().after(absenceEntry.getValue().getDateDebut()) && mission.getDateDebut().before(absenceEntry.getValue().getDateDebut())) {
                    // On fait une copie de la mission à laquelle on attribu des dates de fin et de début et on l ajoute à la liste
                    Mission clonedMission = (Mission) SerializationUtils.clone(mission);
                    mission.setDateDebut(absenceEntry.getValue().getDateFin());
                    clonedMission.setDateFin(absenceEntry.getValue().getDateDebut());
                    finalMissionList.add(clonedMission);
                }
            }
            finalMissionList.add(mission);
        }
    }

    /**
     * supprime les Absences dupliquées et des soumises dans la mesure ou une validées est sur le même intervalle
     *
     * @param sortedAbsences a {@link List}
     * @return a {@link List}
     */
    private List<Map.Entry<String, Absence>> deleteDuplicatedAbsences(List<Map.Entry<String, Absence>> sortedAbsences) {
        List<Map.Entry<String, Absence>> listeDeMap = new ArrayList<>();

        for (Map.Entry<String, Absence> entry : sortedAbsences) {
            int size = listeDeMap.size();
            if (!listeDeMap.isEmpty()) {
                int index = size - 1;
                String lastKey = listeDeMap.get(index).getKey();
                Absence lastInsertedAbsence = listeDeMap.get(index).getValue();
                if (lastKey.contains(ABSENCE_SOUMISE) && entry.getKey().contains(ABSENCE_VALIDE)) {
                    if (lastInsertedAbsence.getDateFin().before(entry.getValue().getDateDebut()) &&
                            lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) ||
                            lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) &&
                                    lastInsertedAbsence.getDateFin().after(entry.getValue().getDateDebut())) {
                        listeDeMap.remove(index);
                        listeDeMap.add(entry);
                    } else {
                        listeDeMap.add(entry);
                    }
                } else if (lastKey.contains(ABSENCE_VALIDE) && entry.getKey().contains(ABSENCE_SOUMISE)) {
                    if (lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) &&
                            lastInsertedAbsence.getDateFin().before(entry.getValue().getDateDebut())) {
                        listeDeMap.add(entry);
                    }
                } else {
                    listeDeMap.add(entry);
                }
            } else {
                listeDeMap.add(entry);
            }
        }
        return listeDeMap;
    }

    /**
     * ajoute l absences à la map
     *
     * @param absencesSoumisesForCollaborateur a list of {@link Absence}
     * @param absencesValideesForCollaborateur a list of {@link Absence}
     * @return a {@link Map}
     */
    private Map<String, Absence> addAbsencesToMap(List<Absence> absencesSoumisesForCollaborateur, List<Absence> absencesValideesForCollaborateur) {
        Map<String, Absence> allAbsencesForCollaborateur = new HashMap<>();
        // ajout des absences soumises à la map
        if (!absencesSoumisesForCollaborateur.isEmpty()) {
            for (int i = 0; i < absencesSoumisesForCollaborateur.size(); i++) {
                Absence absence = absencesSoumisesForCollaborateur.get(i);
                allAbsencesForCollaborateur.put(ABSENCE_SOUMISE + i, absence);
            }
        }
        // ajout des absences validees à la map
        if (!absencesValideesForCollaborateur.isEmpty()) {
            for (int i = 0; i < absencesValideesForCollaborateur.size(); i++) {
                Absence absence = absencesValideesForCollaborateur.get(i);
                allAbsencesForCollaborateur.put(ABSENCE_VALIDE + i, absence);
            }
        }
        // retourne la map contenant toutes les absences
        return allAbsencesForCollaborateur;
    }

    /**
     * Trie une List de Map des absences
     *
     * @param unsortedMap a {@link Map}
     * @return a {@link List}
     */
    private static List<Map.Entry<String, Absence>> sortAllAbsencesByDateDebut(Map<String, Absence> unsortedMap) {

        // Conversion de la Map en List
        List<Map.Entry<String, Absence>> listeDeMap = new LinkedList<>(unsortedMap.entrySet());

        // On trie la liste avec Collections.sort() et on donne un Comparator custom
        Collections.sort(listeDeMap, new Comparator<Map.Entry<String, Absence>>() {
            public int compare(Map.Entry<String, Absence> absence1,
                               Map.Entry<String, Absence> absence2) {
                return (absence1.getValue().getDateDebut()).compareTo(absence2.getValue().getDateDebut());
            }
        });

        return listeDeMap;
    }

    /**
     * Met à jour les date de toutes les absences pour la timeline
     *
     * @param absences a list of {@link Mission}
     * @throws ParseException parseException
     */
    private void updateDatesForTimelineSchedulerForAbsences(List<Absence> absences) throws ParseException {
        for (Absence absence : absences) {
            updateDatesForTimelineScheduler(absence);
        }
    }

    /**
     * Met à jour les dates de toutes les missions pour la timeline
     *
     * @param missions a list of {@link Mission}
     * @throws ParseException parseException
     */
    private void updateDatesForTimelineSchedulerForMissions(List<Mission> missions) throws ParseException {
        for (Mission mission : missions) {
            updateDatesForTimelineScheduler(mission);
        }
    }

    /**
     * Met à jour les dates pour l'affichage d'une absence
     *
     * @param absence a list of {@link Absence}
     * @throws ParseException parseException
     */
    private void updateDatesForTimelineScheduler(Absence absence) throws ParseException {

        String startDate;
        String endDate;

        Date dateDebut;
        Date dateFin;

        startDate = absence.getDateDebut().toString();
        endDate = absence.getDateFin().toString();

        dateDebut = convertStringToTimestampForDateDebutAbsence(startDate);
        dateFin = convertStringToTimestampForDateFinAbsence(endDate);

        absence.setDateDebut(dateDebut);
        absence.setDateFin(dateFin);

    }

    /**
     * Met à jour les dates pour l'affichage d'une mission
     *
     * @param mission a {@link Mission}
     * @throws ParseException parseException
     */
    private void updateDatesForTimelineScheduler(Mission mission) throws ParseException {

        String startDate;
        String endDate;

        Date dateDebut;
        Date dateFin;

        startDate = mission.getDateDebut().toString();
        endDate = mission.getDateFin().toString();

        dateDebut = convertStringToTimestampForDateDebutMission(startDate);
        dateFin = convertStringToTimestampForDateFinMission(endDate);

        // Cas ou l'absence se termine en même temps que la fin de la mission
        if (dateDebut.before(dateFin)) {
            mission.setDateDebut(dateDebut);
            mission.setDateFin(dateFin);
        } else {
            mission.setDateDebut(dateDebut);
            mission.setDateFin(dateDebut);
        }

    }

    /**
     * Renvoie la date en chaîne de caractères
     *
     * @param pDate            date
     * @param simpleDateFormat dateformat
     * @param calendar         calendar
     * @return formatted date
     */
    private String formatDate(String pDate, SimpleDateFormat simpleDateFormat, Calendar calendar) {

        if (pDate == null) {
            pDate = simpleDateFormat.format(calendar.getTime());
            String year = pDate.substring(0, 4);
            String month = pDate.substring(5, 7);
            String day = pDate.substring(8, 10);

            return day + "/" + month + "/" + year;

        } else {
            String year = pDate.substring(6, 10);
            String month = pDate.substring(3, 5);
            String day = pDate.substring(0, 2);

            return year + "-" + month + "-" + day;

        }

    }

    /**
     * Renvoie un timestamp de la chaîne de caractères passée en paramètre avec le temps à la seconde près pour la date de début de misssion
     *
     * @param date date
     * @return date
     * @throws ParseException parseException
     */
    private Date convertStringToTimestampForDateDebutMission(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        String time;
        String hour = date.substring(11, 19);

        // Problème de confusion d'heure entre 00:00:00 et 12:00:00
        // Les missions commencent à minuit ou à midi si une absence est incluse dans la matinée
        switch (hour) {
            // Si l'heure de la mission 00:00:00 ou 00:00:01
            case MINUIT:
            case MINUIT_ET_UNE_SECONDE:
                time = MINUIT;
                break;
            // Si l'heure de la mission est 11:59:58 ou 11:59:59 ou 12:00:00
            case HEURE_FIN_1:
            case HEURE_DEBUT_2:
            case MIDI:
                time = HEURE_DEBUT_2;
                break;
            // Si l'heure de la mission est 23:59:59
            case HEURE_FIN_2:
                // on ajoute une seconde pour passer au jour suivant
                date = addDay(date);
                time = MINUIT;
                break;
            // Si un cas n'est pas géré, on évite l'exception
            // Cependant un bug d'affichage pourrait probablement apparaitre
            default:
                time = date.substring(11, 19);
                break;
        }

        // Concaténation de chaînes de caractères
        stringBuilder.append(date.substring(0, 10));
        stringBuilder.append(" " + time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Renvoie un timestamp de la chaîne de caractères passée en paramètre avec le temps à la seconde près pour la date de fin de misssion
     *
     * @param date date
     * @return date
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateFinMission(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        String time;
        String hour = date.substring(11, 19);

        // Les missions se terminent à midi ou à minuit
        switch (hour) {
            // Si l'heure de la mission 00:00:00 ou 00:00:01
            case MINUIT:
            case MINUIT_ET_UNE_SECONDE:
                time = MINUIT;
                break;
            // Si l'heure de la mission est 11:59:59 ou 12:00:00
            case HEURE_DEBUT_2:
            case MIDI:
                time = HEURE_FIN_1;
                break;
            // Si l'heure de la mission est 23:59:59
            case HEURE_FIN_2:
                // on ajoute une seconde pour passer au jour suivant
                date = addDay(date);
                time = MINUIT;
                break;
            default:
                time = date.substring(11, 19);
                break;
        }

        stringBuilder.append(date.substring(0, 10));
        stringBuilder.append(" " + time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Renvoie un timestamp de la chaîne de caractères passée en paramètre avec le temps à la seconde près pour la date de début d'absence
     *
     * @param date date
     * @return date
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateDebutAbsence(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        // variable time pour set le nouveau temps qui sera pris en compte pour la timeline
        String time;
        String hour = date.substring(11, 19);

        // Les absences commencent soit à 00:00:01 ou à 11:59:59
        switch (hour) {
            // Si l'heure de l'absence est 08:00:00
            case HUIT_HEURES:
                time = MINUIT_ET_UNE_SECONDE;
                break;
            // Si l'heure de l'absence est à 12:00:00 ou 13:00:00
            case MIDI:
            case TREIZE_HEURES:
                time = HEURE_DEBUT_2;
                break;
            // Si l'heure de l'absence est à 18:00:00
            case DIX_HUIT_HEURES:
                time = HEURE_FIN_2;
                break;
            default:
                time = date.substring(11, 19);
                break;
        }

        stringBuilder.append(date.substring(0, 11));
        stringBuilder.append(time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Renvoie un timestamp de la chaîne de caractères passée en paramètre avec le temps à la seconde près pour la date de fin d'absence
     *
     * @param date date
     * @return date
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateFinAbsence(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        // variable time pour set le nouveau temps qui sera pris en compte pour la timeline
        String time;
        String hour = date.substring(11, 19);

        // Les absences commencent se terminent soit 11:59:58 ou à 23:59:59
        switch (hour) {
            // Si l'heure de l'absence est 08:00:00
            case HUIT_HEURES:
                time = MINUIT_ET_UNE_SECONDE;
                break;
            // Si l'heure de l'absence est à 11:59:59 ou 12:00:00 ou 13:00:00
            case HEURE_DEBUT_2:
            case MIDI:
            case TREIZE_HEURES:
                time = HEURE_FIN_1;
                break;
            // Si l'heure de l'absence est à 18:00:00
            case DIX_HUIT_HEURES:
                time = HEURE_FIN_2;
                break;
            default:
                time = date.substring(11, 19);
                break;
        }

        stringBuilder.append(date.substring(0, 11));
        stringBuilder.append(time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Renvoie une chaîne de caractères en ajoutant une seconde à la date
     *
     * @param date date
     * @return dateTime.toString()
     * @throws ParseException ParseException
     */
    private String addDay(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
        Date stringToDate = simpleDateFormat.parse(date);
        DateTime dateTime = new DateTime(stringToDate);
        // on ajoute une seconde pour passer au jour suivant
        // l'heure étant à 23:59:59
        dateTime = dateTime.plusSeconds(1);
        return dateTime.toString();
    }

    /**
     * Fusionne les absences si elles sont superposées dans le temps
     *
     * @param absences       a list of {@link Absence}
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return mergedAbsences a list of {@link Absence}
     */
    private List<Absence> mergeAbsences(List<Absence> absences, List<CollaboratorAsJson> collaborateurs) {

        List<Absence> absencesForCollaborateur = new ArrayList<>();
        List<Absence> mergedAbsences = new ArrayList<>();
        //on rempli une liste avec les absences du collaborateur
        for (CollaboratorAsJson collaborateur : collaborateurs) {
            // si la liste n'est pas vide on la clear
            if (!absencesForCollaborateur.isEmpty()) {
                absencesForCollaborateur.clear();
            }
            // ensuite on ajoute toutes les absences du collaborateur
            if (absencesForCollaborateur.isEmpty()) {
                for (Absence absence : absences) {
                    if (absence.getCollaborateur().getId() == collaborateur.getId()) {
                        absencesForCollaborateur.add(absence);
                    }
                }
                // on tri les absences du collaboratur par date de debut
                sortAbsences(absencesForCollaborateur);
                //on passe par la methode qui gere la fusion des absences
                for (Absence absence : absencesForCollaborateur) {
                    addMergedAbsencesToMergedList(mergedAbsences, absence);
                }
            }
        }

        return mergedAbsences;
    }

    /**
     * Fusionne les absences qui se suivent dans le temps
     *
     * @param absences       a list of {@link Absence}
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Absence}
     */
    private List<Absence> mergeMultipleAbsences(List<Absence> absences, List<CollaboratorAsJson> collaborateurs) {

        List<Absence> resultsAbsences = new ArrayList<>();

        for (CollaboratorAsJson collaborateur : collaborateurs) {

            List<Absence> absencesForCollaborateur = new ArrayList<>();

            mergeMultipleAbsencesForOneCollaborateur(absences, resultsAbsences, collaborateur, absencesForCollaborateur);

            if (!absencesForCollaborateur.isEmpty()) {
                absencesForCollaborateur.clear();
            }

        }
        return resultsAbsences;
    }

    /**
     * Fusionne deux absences si les dates de fin et de début se suivent
     *
     * @param absences                 a list of {@link Absence}
     * @param resultsAbsences          a list of {@link Absence}
     * @param collaborateur            a {@link CollaboratorAsJson}
     * @param absencesForCollaborateur a list of {@link Absence}
     */
    private void mergeMultipleAbsencesForOneCollaborateur(List<Absence> absences, List<Absence> resultsAbsences, CollaboratorAsJson collaborateur, List<Absence> absencesForCollaborateur) {
        if (absencesForCollaborateur.isEmpty()) {

            for (Absence absence : absences) {
                if (absence.getCollaborateur().getId() == collaborateur.getId()) {
                    absencesForCollaborateur.add(absence);
                }
            }
            sortAbsences(absencesForCollaborateur);

            for (Absence absence : absencesForCollaborateur) {
                addMergeToList(resultsAbsences, absence);
            }
        }
    }

    /**
     * Ajoute les absences à la liste d'absences après traitement
     *
     * @param resultsAbsences a list of {@link Absence}
     * @param absence         a {@link Absence}
     */
    private void addMergeToList(List<Absence> resultsAbsences, Absence absence) {
        if (!resultsAbsences.isEmpty()) {
            int index = resultsAbsences.size() - 1;
            Absence lastInsertedAbsence = resultsAbsences.get(index);

            if (lastInsertedAbsence.getCollaborateur().getId().equals(absence.getCollaborateur().getId())) {
                if (checkDates(lastInsertedAbsence, absence)) {
                    absence.setDateDebut(lastInsertedAbsence.getDateDebut());
                    resultsAbsences.remove(index);
                    resultsAbsences.add(absence);
                } else {
                    resultsAbsences.add(absence);
                }
            } else {
                resultsAbsences.add(absence);
            }
        } else {
            resultsAbsences.add(absence);
        }
    }

    /**
     * Fusionne les missions qui se suivent dans le temps
     *
     * @param missions       a list of {@link Mission}
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Mission}
     */
    private List<Mission> mergeMultipleMissions(List<Mission> missions, List<CollaboratorAsJson> collaborateurs) {

        List<Mission> resultsMissions = new ArrayList<>();

        for (CollaboratorAsJson collaborateur : collaborateurs) {
            List<Mission> missionsForCollaborateur = new ArrayList<>();
            mergeMultipleMissionsForOneCollaborateur(missions, resultsMissions, collaborateur, missionsForCollaborateur);
            if (!missionsForCollaborateur.isEmpty()) {
                missionsForCollaborateur.clear();
            }

        }
        return resultsMissions;
    }

    /**
     * Fusionne deux missions si les dates de fin et de début se suivent
     *
     * @param missions                 a list of {@link Mission}
     * @param resultsMissions          a list of {@link Mission}
     * @param collaborateur            a {@link CollaboratorAsJson}
     * @param missionsForCollaborateur a list of {@link Mission}
     */
    private void mergeMultipleMissionsForOneCollaborateur(List<Mission> missions, List<Mission> resultsMissions, CollaboratorAsJson collaborateur, List<Mission> missionsForCollaborateur) {
        if (missionsForCollaborateur.isEmpty()) {

            for (Mission mission : missions) {
                if (collaborateur.getId().equals(mission.getCollaborateur().getId())) {
                    missionsForCollaborateur.add(mission);
                }
            }

            sortMissions(missionsForCollaborateur);

            for (Mission mission : missionsForCollaborateur) {
                addMergeToList(resultsMissions, mission);
            }
        }
    }

    /**
     * Ajoute les missions à la liste de missions après traitement
     *
     * @param resultsMissions a list of {@link Mission}
     * @param mission         a {@link Mission}
     */
    private void addMergeToList(List<Mission> resultsMissions, Mission mission) {
        if (!resultsMissions.isEmpty()) {
            int index = resultsMissions.size() - 1;
            Mission lastInsertedMission = resultsMissions.get(index);

            if (lastInsertedMission.getCollaborateur().getId().equals(mission.getCollaborateur().getId())) {
                if (checkDates(lastInsertedMission, mission)) {
                    mission.setDateDebut(lastInsertedMission.getDateDebut());
                    resultsMissions.remove(index);
                    resultsMissions.add(mission);
                } else {
                    resultsMissions.add(mission);
                }
            } else {
                resultsMissions.add(mission);
            }
        } else {
            resultsMissions.add(mission);
        }
    }

    /**
     * Ajoute les absences fusionnées à la liste d'absences finale
     *
     * @param mergedAbsences a list of {@link Absence}
     * @param absence        a {@link Absence}
     */
    private void addMergedAbsencesToMergedList(List<Absence> mergedAbsences, Absence absence) {
        // Si la taille de la liste fusionnée est inférieure à 0, on ajoute l'absence par défaut
        // Si la taille est supérieure à 0, on compare la dernière absence insérée avec l'absence passée en paramètre
        if (!mergedAbsences.isEmpty()) {
            int size = mergedAbsences.size();
            int index = size - 1;
            Absence lastInsertedAbsence = mergedAbsences.get(index);
            // Si les absences concernent le même collaborateur, dans ce cas on peut vérifier les dates et les fusionner si besoin
            if (lastInsertedAbsence.getCollaborateur().getId() == absence.getCollaborateur().getId()) {
                // 1er cas, si l'absence insérée commence avant la seconde absence et qu'elle se termine avant,
                // on set la date de début de la deuxième absence avec celle de l'absence insérée dans la liste, on supprime la 1ère de la liste et on ajoute la seconde
                if (lastInsertedAbsence.getDateDebut().before(absence.getDateDebut()) && lastInsertedAbsence.getDateFin().after(absence.getDateDebut()) && lastInsertedAbsence.getDateFin().before(absence.getDateFin())) {
                    absence.setDateDebut(lastInsertedAbsence.getDateDebut());
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // 2ème cas, si l'absence insérée commence après la seconde absence et si sa date de fin se termine après la date de fin de la seconde et si sa date de début est avant la date de fin de la seconde
                    // on set la date de fin de la deuxième absence avec celle de l'absence insérée dans la liste, on supprime la 1ère de la liste et on ajoute la seconde
                } else if (lastInsertedAbsence.getDateDebut().after(absence.getDateDebut()) && lastInsertedAbsence.getDateFin().after(absence.getDateFin()) && lastInsertedAbsence.getDateDebut().before(absence.getDateFin())) {
                    absence.setDateFin(lastInsertedAbsence.getDateFin());
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // 3ème cas, si l'absence insérée est comprise entre la seconde absence
                    // on supprime la 1ère de la liste et on ajoute la seconde
                } else if (lastInsertedAbsence.getDateDebut().after(absence.getDateDebut()) && lastInsertedAbsence.getDateFin().before(absence.getDateFin())) {
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // 4ème cas, si l'absence insérée n'est pas dans le même intervalle que la seconde
                    // on ajoute la seconde
                    // 5ème cas, inverse du 3ème cas, on ne fait rien
                } else if (lastInsertedAbsence.getDateDebut().before(absence.getDateDebut()) && lastInsertedAbsence.getDateFin().before(absence.getDateDebut())) {
                    mergedAbsences.add(absence);
                }
            } else {
                mergedAbsences.add(absence);
            }
        } else {
            mergedAbsences.add(absence);
        }
    }

    /**
     * Vérifie si les dates de fin et de début de deux absences se suivent
     *
     * @param lastAbsence      a {@link Absence}
     * @param absenceToCompare a {@link Absence}
     * @return true or false
     */
    private boolean checkDates(Absence lastAbsence, Absence absenceToCompare) {

        DateTime dateFin = new DateTime(lastAbsence.getDateFin());
        DateTime dateDebut = new DateTime(absenceToCompare.getDateDebut());

        if (dateFin.getDayOfMonth() == dateDebut.getDayOfMonth() && dateFin.getMonthOfYear() == dateDebut.getMonthOfYear()) {
            return true;
            // Sachant que l'absence finit à 18:00:00, en ajoutant 14 heures on doit retomber sur le jour suivant à 08:00:00
        } else if (dateFin.plusHours(14).getDayOfMonth() == dateDebut.getDayOfMonth() && dateFin.plusHours(14).getMonthOfYear() == dateDebut.getMonthOfYear()) {
            DateTime dateModif = new DateTime(dateFin.plusHours(14));
            return dateDebut.getHourOfDay() - dateModif.getHourOfDay() == 0;
        } else {
            return false;
        }
    }

    /**
     * Vérifie si les dates de fin et de début de deux missions se suivent
     *
     * @param lastInsertedMission a {@link Mission}
     * @param mission             a {@link Mission}
     * @return true or false
     */
    private boolean checkDates(Mission lastInsertedMission, Mission mission) {

        DateTime dateFin = new DateTime(lastInsertedMission.getDateFin());
        DateTime dateDebut = new DateTime(mission.getDateDebut());

        // Avec une boucle, on vérifie sur un écart de 20 jours si deux missions se suivent et ainsi les rattacher en une seule et même mission
        for (int i = 0; i <= 20; i++) {
            if (dateFin.plusDays(i).getDayOfMonth() == dateDebut.getDayOfMonth() && dateFin.plusDays(i).getMonthOfYear() == dateDebut.getMonthOfYear()) {
                DateTime dateModif = new DateTime(dateFin.plusDays(i));
                if (dateDebut.getHourOfDay() - dateModif.getHourOfDay() == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Permet de trier la liste des absences pour un collaborateur en fonction des dates de début
     *
     * @param absencesForCollaborateur a list of {@link Absence}
     */
    private void sortAbsences(List<Absence> absencesForCollaborateur) {
        Collections.sort(absencesForCollaborateur, Comparator.comparing(Absence::getDateDebut));
    }

    /**
     * @param missions       a list of {@link Mission}
     * @param collaborateurs a list of {@link CollaboratorAsJson}
     * @return a list of {@link Mission}
     */
    private List<Mission> mergeMissions(List<Mission> missions, List<CollaboratorAsJson> collaborateurs) {

        List<Mission> missionsForCollaborateur = new ArrayList<>();
        List<Mission> mergedMissions = new ArrayList<>();

        for (CollaboratorAsJson collaborateur : collaborateurs) {

            if (!missionsForCollaborateur.isEmpty()) {
                missionsForCollaborateur.clear();
            }

            if (missionsForCollaborateur.isEmpty()) {
                for (Mission mission : missions) {
                    if (mission.getCollaborateur().getId() == collaborateur.getId()) {
                        missionsForCollaborateur.add(mission);
                    }
                }

                sortMissions(missionsForCollaborateur);

                for (Mission mission : missionsForCollaborateur) {
                    addMergedMissionsToMergedList(mergedMissions, mission);
                }
            }
        }
        return mergedMissions;
    }

    /**
     * Ajoute les missions fusionnées à la liste de missions finale
     *
     * @param mergedMissions a list of {@link Mission}
     * @param mission        a {@link Mission}
     */
    private void addMergedMissionsToMergedList(List<Mission> mergedMissions, Mission mission) {
        int size = mergedMissions.size();
        int index = size - 1;
        // Même principe que addMergedAbsencesToMergedList
        if (!mergedMissions.isEmpty()) {
            // ci dessous les conditions de prise en compte des objets afin de selectionner ceux qui vont nous interessés ou non
            Mission lastInsertedMission = mergedMissions.get(index);
            if (lastInsertedMission.getCollaborateur().getId() == mission.getCollaborateur().getId()) {
                if (lastInsertedMission.getDateDebut().before(mission.getDateDebut()) && lastInsertedMission.getDateFin().after(mission.getDateDebut()) && lastInsertedMission.getDateFin().before(mission.getDateFin())) {
                    mission.setDateDebut(lastInsertedMission.getDateDebut());
                    mergedMissions.remove(index);
                    mergedMissions.add(mission);
                } else if (lastInsertedMission.getDateDebut().after(mission.getDateDebut()) && lastInsertedMission.getDateFin().after(mission.getDateFin()) && lastInsertedMission.getDateDebut().before(mission.getDateFin())) {
                    mission.setDateFin(lastInsertedMission.getDateFin());
                    mergedMissions.remove(index);
                    mergedMissions.add(mission);
                } else if (lastInsertedMission.getDateDebut().after(mission.getDateDebut()) && lastInsertedMission.getDateFin().before(mission.getDateFin())) {
                    mergedMissions.remove(index);
                    mergedMissions.add(mission);
                } else if (lastInsertedMission.getDateDebut().before(mission.getDateDebut()) && lastInsertedMission.getDateFin().before(mission.getDateDebut())) {
                    mergedMissions.add(mission);
                }
            } else {
                mergedMissions.add(mission);
            }
        } else {
            mergedMissions.add(mission);
        }
    }

    /**
     * Permet de trier une liste de missions en fonction des dates de début de mission
     *
     * @param missionsForCollaborateur a list of {@link Mission}
     */
    private void sortMissions(List<Mission> missionsForCollaborateur) {
        Collections.sort(missionsForCollaborateur, (Mission mission1, Mission mission2) -> {
            return mission1.getDateDebut().compareTo(mission2.getDateDebut());
        });
    }


    /**
     * @param annee annee
     * @return a list of {@link Date}
     */
    private List<Date> getHolidays(int annee) {
        List<Date> datesFeries = new ArrayList<>();

        // Jour de l'an
        GregorianCalendar jourAn = new GregorianCalendar(annee, 0, 1);
        datesFeries.add(jourAn.getTime());

        // Lundi de pacques
        GregorianCalendar pacques = calculLundiPacques(annee);
        datesFeries.add(pacques.getTime());

        // Fete du travail
        GregorianCalendar premierMai = new GregorianCalendar(annee, 4, 1);
        datesFeries.add(premierMai.getTime());

        // 8 mai
        GregorianCalendar huitMai = new GregorianCalendar(annee, 4, 8);
        datesFeries.add(huitMai.getTime());

        // Ascension (= pâques + 38 jours)
        GregorianCalendar ascension = new GregorianCalendar(annee,
                pacques.get(GregorianCalendar.MONTH),
                pacques.get(GregorianCalendar.DAY_OF_MONTH));
        ascension.add(GregorianCalendar.DAY_OF_MONTH, 38);
        datesFeries.add(ascension.getTime());

        // Fête Nationale
        GregorianCalendar quatorzeJuillet = new GregorianCalendar(annee, 6, 14);
        datesFeries.add(quatorzeJuillet.getTime());

        // Assomption
        GregorianCalendar assomption = new GregorianCalendar(annee, 7, 15);
        datesFeries.add(assomption.getTime());

        // La Toussaint
        GregorianCalendar toussaint = new GregorianCalendar(annee, 10, 1);
        datesFeries.add(toussaint.getTime());

        // L'Armistice
        GregorianCalendar armistice = new GregorianCalendar(annee, 10, 11);
        datesFeries.add(armistice.getTime());

        // Noël
        GregorianCalendar noel = new GregorianCalendar(annee, 11, 25);
        datesFeries.add(noel.getTime());

        return datesFeries;
    }

    /**
     * Calcul lundi pacques gregorian calendar.
     *
     * @param annee the annee
     * @return the gregorian calendar
     */
    private GregorianCalendar calculLundiPacques(int annee) {
        int a = annee / 100;
        int b = annee % 100;
        int c = (3 * (a + 25)) / 4;
        int d = (3 * (a + 25)) % 4;
        int e = (8 * (a + 11)) / 25;
        int f = (5 * a + b) % 19;
        int g = (19 * f + c - e) % 30;
        int h = (f + 11 * g) / 319;
        int j = (60 * (5 - d) + b) / 4;
        int k = (60 * (5 - d) + b) % 4;
        int m = (2 * j - k - g + h) % 7;
        int n = (g - h + m + 114) / 31;
        int p = (g - h + m + 114) % 31;
        int jour = p + 1;
        int mois = n;

        GregorianCalendar date = new GregorianCalendar(annee, mois - 1, jour);
        date.add(GregorianCalendar.DAY_OF_MONTH, 1);
        return date;
    }
}
