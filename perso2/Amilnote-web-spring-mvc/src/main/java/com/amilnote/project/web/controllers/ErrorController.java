/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The type Error controller.
 */
@ControllerAdvice
public class ErrorController extends AbstractController {

    /**
     * The Error page.
     */
    private static final String ERROR_PAGE = "error";
    private static final String ENVIRONMENT_PROD = "env.prod";
    private static Logger logger = LogManager.getLogger(ErrorController.class);

    /***
     * Recupère toutes les erreurs de l'application
     *
     * @param e        the e
     * @param response the response
     * @param request  the request
     * @return model and view
     * @throws NamingException the exception
     */
    @ExceptionHandler(value = Exception.class)
    public ModelAndView exception(Exception e, HttpServletResponse response, WebRequest request) throws NamingException {

        response.setStatus(900);

        String name = "Erreur";

        logger.error(name +
                " / " + e.getClass().getSimpleName() +
                " / " + e.toString() + e.getCause() +
                " / " + request.getContextPath());

        ModelAndView mav;

        if (!Boolean.valueOf(Parametrage.getContext(ENVIRONMENT_PROD))) {
            mav = this.getModelAndView(name,
                    e.getClass().getSimpleName(),
                    e.toString() + e.getCause(),
                    request.getContextPath());
        } else {
            mav = this.getModelAndView(name,
                    "",
                    "",
                    request.getContextPath());
        }

        return mav;
    }


    /**
     * No handler found Exception
     *
     * @param e       the e
     * @param request the request
     * @return a redirection to the home page
     */
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ModelAndView exception(NoHandlerFoundException e, WebRequest request) {

        logger.error("Page introuvable" +
                " / " + e.getClass().getSimpleName() +
                " / " + e.toString() + e.getCause() +
                " / " + request.getContextPath());

        return new ModelAndView(Constantes.REDIRECT);
    }

    /**
     * Exception model and view.
     *
     * @param e        the e
     * @param response the response
     * @param request  the request
     * @return the model and view
     * @throws NamingException NamingException
     */
    @ExceptionHandler(value = EmailException.class)
    public ModelAndView exception(EmailException e, HttpServletResponse response, WebRequest request) throws NamingException {

        response.setStatus(903);

        String name = "Erreur lors de l'envoi du mail";

        logger.error(name +
                " / " + e.getClass().getSimpleName() +
                " / " + e.toString() + e.getCause() +
                " / " + request.getContextPath());
        ModelAndView mav;

        if (!Boolean.valueOf(Parametrage.getContext(ENVIRONMENT_PROD))) {
            mav = this.getModelAndView(name,
                    e.getClass().getSimpleName(),
                    e.toString() + e.getCause(),
                    request.getContextPath());
        } else {
            mav = this.getModelAndView(name,
                    "",
                    "",
                    request.getContextPath());
        }

        return mav;
    }

    /**
     * Exception model and view.
     *
     * @param e        the e
     * @param response the response
     * @param request  the request
     * @return the model and view
     * @throws IOException     the io exception
     * @throws NamingException NamingException
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    public ModelAndView exception(AccessDeniedException e, HttpServletResponse response, WebRequest request) throws IOException, NamingException {

        response.setStatus(904);

        String name = "Vous n'avez pas l'autorisation d'accéder à cette page";

        logger.error(name +
                " / " + e.getClass().getSimpleName() +
                " / " + e.toString() + e.getCause() +
                " / " + request.getContextPath());

        ModelAndView mav;

        if (!Boolean.valueOf(Parametrage.getContext(ENVIRONMENT_PROD))) {
            mav = this.getModelAndView(name,
                    e.getClass().getSimpleName(),
                    e.toString() + e.getCause(),
                    request.getContextPath());
        } else {
            mav = this.getModelAndView(name,
                    "",
                    "",
                    request.getContextPath());
        }

        return mav;
    }

    private ModelAndView getModelAndView(String titre, String name, String message, String contextPath) {
        ModelAndView mav = new ModelAndView(ERROR_PAGE);

        mav.addObject("titre", titre);
        mav.addObject("name", name);
        mav.addObject("message", message);
        mav.addObject("contextPath", contextPath);

        return mav;
    }


}

