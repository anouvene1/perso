package com.amilnote.project.web.controllers;


import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderODM;
import com.amilnote.project.metier.domain.services.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public abstract class AbstractTravailleurController extends AbstractController {

    protected static final String PROP_MISSION_TO_EDIT = "currentEditMission";
    protected static final String REDIRECT = "redirect:/";
    protected static final String NULL = "/null";
    protected static final String WITH_DISABLED_ATTRIBUT = "withDisabled";
    protected static final String LIST_COLLABORATOR_ATTRIBUT = "listeCollaborateurs";

    @Autowired
    protected ClientService clientService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    protected ContactClientService contactClientService;

    @Autowired
    protected MissionService missionService;

    @Autowired
    protected PosteService postService;

    @Autowired
    protected LinkMissionForfaitService linkMissionforfaitService;

    @Autowired
    protected TypeMissionService typeMissionService;


    @Autowired
    protected TypeFraisService typeFraisService;

    @Autowired
    protected PDFBuilderODM pdfBuilderODM;

    @Autowired
    protected EtatService etatService;


    abstract AbstractTravailleurService getService();


    /**
     * Liste des tous les collab
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @GetMapping("travailleurs")
    public String listeTravailleur(HttpSession session, HttpServletRequest request, Model model) throws JsonProcessingException {
        init(request, model);

        List<CollaboratorAsJson> listTempAllCollab;
        boolean lWithDisabled = false;
        if (model.containsAttribute(WITH_DISABLED_ATTRIBUT)) {
            lWithDisabled = (boolean) model.asMap().get(WITH_DISABLED_ATTRIBUT);
        }

        if (lWithDisabled) {
            listTempAllCollab = collaboratorService.findAllOrderWithDisabledByNomAscAsJson();
            model.addAttribute(LIST_COLLABORATOR_ATTRIBUT, listTempAllCollab);
            model.addAttribute("isChecked", lWithDisabled);
        } else {
            listTempAllCollab = collaboratorService.findAllOrderByNomAscAsJson();
            model.addAttribute(LIST_COLLABORATOR_ATTRIBUT, listTempAllCollab);
        }

        List<CollaboratorAsJson> listCollabWithDisable = new ArrayList<>();
        List<CollaboratorAsJson> listSttWithDisable = new ArrayList<>();

        for (CollaboratorAsJson collab : listTempAllCollab) {
            if (collab.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
                listSttWithDisable.add(collab);
            else
                listCollabWithDisable.add(collab);
        }
        model.addAttribute(LIST_COLLABORATOR_ATTRIBUT, listCollabWithDisable);
        model.addAttribute("listeSousTraitant", listSttWithDisable);

        //verif de l'endroit où l'on arrive
        String temp = request.getParameter("type");
        String navActive;
        if (temp != null)
            navActive = temp;
        else
            navActive = "idTableContainer";
        model.addAttribute("navActive", navActive);


        return NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS;
    }

    /**
     * Renvoi vers la méthode listant les collab avec le paramère "withDisabled" à true pour afficher aussi les collab désactivés
     *
     * @param model        the model
     * @param session      the session
     * @param withDisabled : paramètre envoyé quand on coche la case "Afficher les collab désactivés"
     * @param ra           the ra
     * @param request      request
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @GetMapping("travailleurs/{withDisabled}")
    public String listeTravailleursWithDisabled(Model model, HttpSession session, @PathVariable(WITH_DISABLED_ATTRIBUT) String withDisabled, HttpServletRequest request, RedirectAttributes ra) throws JsonProcessingException {
        init(request, model);

        ra.addFlashAttribute(WITH_DISABLED_ATTRIBUT, "true".equals(withDisabled));
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS + "?type=" + request.getParameter("type");
    }

    protected boolean moisIsOkMission(String mois, MissionAsJson miss) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(miss.getDateDebut());
        String moisMission = Integer.toString(cal.get(Calendar.MONTH));

        return moisMission.equals(mois) || "-1".equals(mois);
    }

    protected boolean anneeIsOkMission(String annee, MissionAsJson miss) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(miss.getDateDebut());
        String anneeMission = Integer.toString(cal.get(Calendar.YEAR));

        return anneeMission.equals(annee) || "-1".equals(annee);
    }

}
