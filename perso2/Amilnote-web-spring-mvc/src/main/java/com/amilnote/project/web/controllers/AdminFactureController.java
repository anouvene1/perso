/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.*;
import com.amilnote.project.metier.domain.pdf.PDFBuilderFacture;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.*;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import com.amilnote.project.metier.domain.utils.enumerations.TypeMissionEnum;
import com.amilnote.project.web.DTO.CommandeDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.itextpdf.text.DocumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.amilnote.project.metier.domain.entities.Collaborator.COLLABORATORS_FR_LOWER_CASE;
import static com.amilnote.project.metier.domain.entities.Collaborator.SUBCONTRACTORS_FR_SNAKE_CASE;
import static com.amilnote.project.metier.domain.entities.TypeFacture.TYPEFACTURE_FACTURABLE;
import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.*;

/**
 * Controller de la gestion des Factures
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminFactureController extends AbstractController {

    private static Logger logger = LogManager.getLogger(AdminFactureController.class);

    public static final String MUST_REVALIDATE = "must-revalidate, post-check=0, pre-check=0";
    public static final int SUPPORTING_LENGTH = 14;

    final private String ETAT_SOUMIS_CODE = Etat.ETAT_SOUMIS_CODE;
    final private String ETAT_GENERE = Etat.ETAT_GENERE;
    final private String ETAT_EDITE = Etat.ETAT_EDITE;
    private ResponseEntity<byte[]> responseEntity;

    @Autowired
    private PDFBuilderRA pdfBuilderRA;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private DocumentExcelService documentExcelService;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private FactureService factureService;

    @Autowired
    private TypeFactureService typeFactureService;

    @Autowired
    private ContactClientService contactClientService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private FactureArchiveeService factureArchiveeService;

    @Autowired
    private TvaService tvaService;

    @Autowired
    private PDFBuilderFacture pdfBuilderFacture;

    @Autowired
    private EtatService etatService;

    @Autowired
    private MailService mailService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private FileService fileService;

    @Autowired
    private DocumentCSVService documentCSVService;

    /**
     * Page de gestion des factures : mise en place des données à transmettre à la JSP pour afficher la page
     *
     * @param moisVoulu     the mois voulu
     * @param request       the request
     * @param model         the model
     * @param session       the session
     * @param idMission     idMission
     * @param idTypeFacture idTypeFacture
     * @return string
     */
    @RequestMapping(value = "factures/{moisVoulu}", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationFactures(
            @PathVariable("moisVoulu") String moisVoulu,
            @RequestParam(value = "idTypeFacture", required = false) Integer idTypeFacture,
            @RequestParam(value = "idMission", required = false) Long idMission,
            HttpServletRequest request, Model model, HttpSession session) {

        init(request, model);
        List<CollaboratorAsJson> listeConsultants = collaboratorService.findAllWithMissionOrderByNomAscAsJson();
        List<CollaboratorAsJson> listeCollaborateurs = new ArrayList<>();
        List<CollaboratorAsJson> listeSoustraitants = new ArrayList<>();

        for (CollaboratorAsJson collaboratorAsJson : listeConsultants) {
            if (collaboratorAsJson.getStatut().getId() != 7) {
                listeCollaborateurs.add(collaboratorAsJson);
            } else {
                listeSoustraitants.add(collaboratorAsJson);
            }
        }

        Calendar calendar = Calendar.getInstance();
        String date = request.getParameter(MONTH_YEAR_EXTRACT);
        if (!moisVoulu.equals("null")) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Integer.parseInt(date.split(SLASH)[0]) - 1);
            calendar.set(Calendar.YEAR, Integer.parseInt(date.split(SLASH)[1]));
        }

        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        DateTime dateVoulue = new DateTime(calendar.getTime());

        String yearOfSearch = Integer.toString(calendar.get(Calendar.YEAR));
        String monthOfSearch = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        // date affichée en haut de la page de gestion des factures du mois
        model.addAttribute("moisActuel", monthOfSearch.toUpperCase());
        model.addAttribute("anneeActuel", yearOfSearch.toUpperCase());

        model.addAttribute("dateVoulue", dateVoulue.toString(Constantes.STANDARD_DATE_FORMAT));

        // Listes en session pour générer les excels aux boutons (voir si on recalcule ou si on transmet autrement)
        session.setAttribute("dateVoulue", dateVoulue);

        DateTime dateVoulueRefonte = new DateTime();
        String year = null;
        String month = null;
        if (date != null) {
            year = date.substring(date.length() - 4);
            month = date.substring(0, 2);
            dateVoulueRefonte = extractPickedDate(month, year);
        }

        List<Facture> listFacturables = new ArrayList<>();
        List<Facture> listFacturablesSS = new ArrayList<>();
        List<Facture> listTemporaireFacturables = new ArrayList<>();

        List<Facture> listFacturesRANonSoumis = new ArrayList<>();

        List<Mission> listMissionsClientesDuMoisVoulu = new ArrayList<>();
        List<Mission> listMissionsClientesDuMoisVouluSS = new ArrayList<>();
        List<Mission> listMissionsClientesDuMoisVouluConsultants = new ArrayList<>();
        List<Mission> listMissionsClientesDuMoisVouluTemporaire = new ArrayList<>();
        List<Mission> listMissionsWithoutInvoiceCollaborator = new ArrayList<>();
        List<Mission> listMissionsWithoutInvoiceSubcontractor = new ArrayList<>();

        List<Commande> listCommandesDuMoisVouluMission = new ArrayList<>();

        List<Facture> listFacturesFrais = new ArrayList<>();

        List<Facture> listFacturesALaMain = new ArrayList<>();
        List<Facture> listFacturesALaMainSS = new ArrayList<>();
        List<Facture> listFacturesALaMainTemporaire = new ArrayList<>();

        try {
            String monthYearExtract = request.getParameter(MONTH_YEAR_EXTRACT);
            DateTime[] startEndSearchDateTimes = firstAndLastDatesOfMonth(
                    monthYearExtract != null ? Integer.parseInt(monthYearExtract.split(SLASH)[1]) : null,
                    monthYearExtract != null ? Integer.parseInt(monthYearExtract.split(SLASH)[0]) : null
            );

            listTemporaireFacturables = factureService.findUnarchivedInvoicesByTypeAndValidRa(
                    startEndSearchDateTimes[0].toDate(),
                    startEndSearchDateTimes[1].toDate(),
                    typeFactureService.findByCode(TYPEFACTURE_FACTURABLE),
                    1, 2, -1 // 1 correspond à RA valide, 2 à RA non valide mais facture forcée, et -1 RA annulé
            );

            for (Facture facture : listTemporaireFacturables) {
                if (facture.getMission().getCollaborateur().getStatut().getId() != 7 && facture.getMission().getCollaborateur().isEnabled()) {
                    listFacturables.add(facture);
                } else {
                    listFacturablesSS.add(facture);
                }
            }
            listFacturesRANonSoumis = factureService.findFacturesRANonSoumisByMonth(dateVoulueRefonte);
            listMissionsClientesDuMoisVouluTemporaire = missionService.findMissionsClientesByMonth(dateVoulueRefonte);
            for (Mission mission : listMissionsClientesDuMoisVouluTemporaire) {
                if (mission.getCollaborateur().getStatut().getId() != 7) {
                    listMissionsClientesDuMoisVoulu.add(mission);
                } else {
                    listMissionsClientesDuMoisVouluSS.add(mission);
                }
                listMissionsClientesDuMoisVouluConsultants.add(mission);
            }
            listFacturesFrais = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
            listFacturesALaMainTemporaire = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
            for (Facture facture : listFacturesALaMainTemporaire) {
                if (facture.getMission().getCollaborateur().getStatut().getId() != 7) {
                    listFacturesALaMain.add(facture);
                } else {
                    listFacturesALaMainSS.add(facture);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        DateTime[] boundsOfMonth = Utils.firstAndLastDatesOfMonth(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1);
        Date lastDayofMonth = boundsOfMonth[1].toDate();

        // ajout des missions du mois précédent à la liste des missions consultants
        try {
            List<Mission> lMissionsMoisPrecedentConsultants = missionService.findMissionsClientesByMonth(dateVoulueRefonte.minusMonths(1));
            Set<Mission> setMissionsConsultants = new LinkedHashSet<>(listMissionsClientesDuMoisVouluConsultants);
            setMissionsConsultants.addAll(lMissionsMoisPrecedentConsultants);
            listMissionsClientesDuMoisVouluConsultants = new ArrayList<>(setMissionsConsultants);
            listMissionsClientesDuMoisVouluConsultants.sort(Comparator.comparing(Mission::getMission));
            listMissionsWithoutInvoiceCollaborator = missionService
                    .findMissionWithoutInvoice(TypeMissionEnum.CLIENT.toString(), StatutCollaborateur.STATUT_COLLABORATEUR, lastDayofMonth);
            listMissionsWithoutInvoiceSubcontractor = missionService
                    .findMissionWithoutInvoice(TypeMissionEnum.CLIENT.toString(), StatutCollaborateur.STATUT_SOUS_TRAITANT, lastDayofMonth);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        for (Mission mission : listMissionsClientesDuMoisVouluConsultants) {
            for (Commande commande : mission.getCommandes()) {
                listCommandesDuMoisVouluMission.add(commande);
            }
        }

        // [FIX] AMNT-619 : permet de visualiser les missions du mois précédent le mois selectionné
        // à la liste des missions collaborateurs [Factures Collaborateurs/Factures crée à la main]
        try {
            List<Mission> lMissionsMoisPrecedentCollab = missionService.findMissionsClientesByMonth(dateVoulueRefonte.minusMonths(1));
            Set<Mission> setMissionsCollab = new LinkedHashSet<>(listMissionsClientesDuMoisVoulu);
            setMissionsCollab.addAll(lMissionsMoisPrecedentCollab);
            listMissionsClientesDuMoisVoulu = new ArrayList<>(setMissionsCollab);
            listMissionsClientesDuMoisVoulu.sort(Comparator.comparing(Mission::getMission));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        Boolean forcerFactureOK = false;
        DateTime dateLimiteRANonSoumis = new DateTime();
        dateLimiteRANonSoumis = dateLimiteRANonSoumis
                .withMonthOfYear(dateVoulueRefonte.getMonthOfYear())
                .withDayOfMonth(20)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0);
        if (dateLimiteRANonSoumis.isBeforeNow() && !listFacturesRANonSoumis.isEmpty()) {
            forcerFactureOK = true;
        }

        // Calcul des montants des factures de frais
        HashMap<Integer, Pair<Float, Float>> montantsParFactures = new HashMap<>();
        for (Facture facture : listFacturesFrais) {
            montantsParFactures.put(facture.getId(), factureService.getMontantHT(facture));
        }

        //Collaborator
        // Calcul des quantités et montants des facturables si frais
        HashMap<Integer, Pair<Float, Float>> mapFactureQuantiteMontant = new HashMap<>();
        for (Facture facture : listFacturables) {
            mapFactureQuantiteMontant.put(facture.getId(), factureService.getQuantiteMontantAvecFrais(facture));
        }

        // Calcul uniquement des elements pour collaborateur
        HashMap<Integer, Float> mapFactureElementMontant = new HashMap<>();
        for (Facture facture : listFacturables) {
            mapFactureElementMontant.put(facture.getId(), factureService.getMontantFrais(facture));
        }

        // Calcul des quantités et montants des facturables si frais FMAIN
        HashMap<Integer, Pair<Float, Float>> mapFactureQuantiteMontantFMain = new HashMap<>();
        for (Facture facture : listFacturesALaMain) {
            mapFactureQuantiteMontantFMain.put(facture.getId(), factureService.getQuantiteMontantAvecFrais(facture));
        }

        //Sous-traitant
        // Calcul des quantités et montants des facturables si frais pour sous-traitant
        HashMap<Integer, Pair<Float, Float>> mapFactureQuantiteMontantSS = new HashMap<>();
        for (Facture facture : listFacturablesSS) {
            mapFactureQuantiteMontantSS.put(facture.getId(), factureService.getQuantiteMontantAvecFrais(facture));
        }

        // Calcul uniquement des elements pour sous-traitant
        HashMap<Integer, Float> mapFactureElementMontantSS = new HashMap<>();
        for (Facture facture : listFacturablesSS) {
            mapFactureElementMontantSS.put(facture.getId(), factureService.getMontantFrais(facture));
        }

        HashMap<Integer, Pair<Float, Float>> mapFactureFMainSS = new HashMap<>();
        for (Facture facture : listFacturesALaMainSS) {
            mapFactureFMainSS.put(facture.getId(), factureService.getQuantiteMontantAvecFrais(facture));
        }

        String redirect = factureService.findTypeFactureById(idTypeFacture, idMission);
        // Sort clients by alphabetical order
        listFacturables = factureService.sortInvoicesByClient(listFacturables);
        listFacturablesSS = factureService.sortInvoicesByClient(listFacturablesSS);

        model.addAttribute("dateVoulueRefonte", dateVoulueRefonte.toString("MM/yyyy"));
        model.addAttribute("listFacturables", listFacturables);
        model.addAttribute("listFacturablesSS", listFacturablesSS);
        model.addAttribute("listFacturesRANonSoumis", listFacturesRANonSoumis);
        model.addAttribute("listeFactureFrais", listFacturesFrais);
        model.addAttribute("listFacturesALaMain", listFacturesALaMain);
        model.addAttribute("listFacturesALaMainSS", listFacturesALaMainSS);
        model.addAttribute("listMissionsClientesDuMoisVoulu", listMissionsClientesDuMoisVoulu);
        model.addAttribute("listMissionsClientesDuMoisVouluSS", listMissionsClientesDuMoisVouluSS);
        model.addAttribute("listMissionsClientesDuMoisVouluConsultants", listMissionsClientesDuMoisVouluConsultants);
        model.addAttribute("listMissionsWithoutInvoiceCollaborator", listMissionsWithoutInvoiceCollaborator);
        model.addAttribute("listMissionsWithoutInvoiceSubcontractor", listMissionsWithoutInvoiceSubcontractor);
        model.addAttribute("listCommandesDuMoisVouluMission", listCommandesDuMoisVouluMission);
        model.addAttribute("listeConsultants", listeConsultants);
        model.addAttribute("listeCollaborateurs", listeCollaborateurs);
        model.addAttribute("listeSoustraitants", listeSoustraitants);
        model.addAttribute("forcerFactureOK", forcerFactureOK);
        model.addAttribute("montantsParFactures", montantsParFactures);
        model.addAttribute("mapFactureQuantiteMontant", mapFactureQuantiteMontant);
        model.addAttribute("mapFactureElementMontant", mapFactureElementMontant);
        model.addAttribute("mapFactureQuantiteMontantFMain", mapFactureQuantiteMontantFMain);
        model.addAttribute("mapFactureQuantiteMontantSS", mapFactureQuantiteMontantSS);
        model.addAttribute("mapFactureElementMontantSS", mapFactureElementMontantSS);
        model.addAttribute("mapFactureFMainSS", mapFactureFMainSS);

        // renvoie le hash à la page des factures, pour la redirection
        model.addAttribute("hashRetour", redirect);

        return NAV_ADMIN + NAV_ADMIN_FACTURES;
    }

    /**
     * Extraire le list des missions d'un collaborateur select
     *
     * @param request  la requete http
     * @param idCollab id du collab précédemment sélectionné
     * @param prev     booléen pour sélectionné les missions du mois précédent
     * @return a list of {@link MissionAsJson}
     */
    @RequestMapping(value = "loadMissionCourantes/{idCollab}/{prev}", method = {RequestMethod.POST, RequestMethod.GET})
    public
    @ResponseBody
    List<MissionAsJson> loadMission(HttpServletRequest request, @PathVariable("idCollab") long idCollab, @PathVariable("prev") boolean prev) {
        List<Mission> listeMissions = new ArrayList<Mission>();
        Collaborator collaborateur = collaboratorService.get(idCollab);
        listeMissions = collaborateur.getMissions();
        List<MissionAsJson> missionsJson = new ArrayList<MissionAsJson>();
        Calendar cal = Calendar.getInstance();
        if (prev) cal.add(Calendar.MONTH, -2);
        Date result = cal.getTime();
        for (Mission maMission : listeMissions) {
            if (maMission.getTypeMission().getCode().equals("MI") && maMission.getDateFin().compareTo(result) >= 0) {
                missionsJson.add(maMission.toJson());
            }

        }
        return missionsJson;
    }

    /**
     * Extraire le list des commandes d'une mission select
     *
     * @param request   la requete http
     * @param idMission id de la mission précédemment sélectionné
     * @return a list of {@link CommandeDTO}
     */
    @RequestMapping(value = "loadCommandesDeMission/{idMission}", method = {RequestMethod.GET})
    @ResponseBody
    public List<CommandeDTO> loadCommande(HttpServletRequest request, @PathVariable("idMission") long idMission) {
        List<Commande> listeCommandes = new ArrayList<Commande>();
        listeCommandes = commandeService.findByIdMission(missionService.get(idMission));

        // la Serialization ne fonctionne pas pour l'objet CommandeAsJson comme pour MissionAsJson (voir methode AdminFactureController#loadMission()
        // creation d'un objet DTO qui sera renvoyé en Json grace au modelmApper

        List<CommandeDTO> commandesDTO = new ArrayList<CommandeDTO>();

        ModelMapper modelMapper = new ModelMapper();

        for (Commande maCommande : listeCommandes) {
            CommandeDTO commandeJson = modelMapper.map(maCommande, CommandeDTO.class);
            commandesDTO.add(commandeJson);
        }

        return commandesDTO;
    }

    /**
     * Méthode servant à numéroter les factures qui sont dans l'ordre de facturation du type voulu à partir d'un
     * numéro choisi
     *
     * @param monthOfSearch     le mois voulu
     * @param yearOfSearch      l'année voulue
     * @param idTypeFacture     the type facture
     * @param numFacture        the num facture
     * @param request           the request
     * @param model             the model
     * @param session           the session
     * @param ra                the ra
     * @param typeDeTravailleur typeDeTravailleur
     * @return string
     * @throws IOException                the io exception
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     */
    @RequestMapping(value = "numeroterFactures/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String numeroterFactures(
            @RequestParam(value = "typeFacture2", required = false) int idTypeFacture,
            @RequestParam(value = "numFacture", required = false) int numFacture,
            @RequestParam(value = "typeDeTravailleur2", required = false) String typeDeTravailleur,
            @PathVariable(value = "lAnnee") String yearOfSearch,
            @PathVariable(value = "leMois") String monthOfSearch,
            HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra)
            throws IOException, EncryptedDocumentException, InvalidFormatException, ParseException, NamingException {

        init(request, model);
        boolean validNumFacture = factureService.assignNumbersToInvoices(idTypeFacture, numFacture, typeDeTravailleur, yearOfSearch, monthOfSearch);

        if (!validNumFacture) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Le numéro de facture est inférieur ou égal au plus grand numéro de facture de la liste");
        }

        return concatenateObjectsToString(REDIRECT, NAV_ADMIN, NAV_ADMIN_FACTURES, NULL);
    }

    /**
     * Methode servant à soumettre l'ordre de facturation du type voulu en créant un récapitulatif au format Excel et
     * en l'envoyant par mail à la direction
     *
     * @param leMois            le mois voulu
     * @param lAnnee            l'année voulue
     * @param idTypeFacture     the type facture
     * @param request           the request
     * @param model             the model
     * @param session           the session
     * @param ra                the ra
     * @param typeDeTravailleur typeDeTravailleur
     * @return string
     * @throws IOException                the io exception
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     * @throws Exception                  the exception
     */
    @RequestMapping(value = "soumettreFactures/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String soumettreFactures(
            @RequestParam(value = "typeFacture3", required = false) int idTypeFacture,
            @RequestParam(value = "typeDeTravailleur3", required = false) String typeDeTravailleur,
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "leMois") String leMois,
            HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra)
            throws IOException, EncryptedDocumentException, InvalidFormatException, ParseException, NamingException, Exception {
        init(request, model);

        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);

        //initialisation de la liste des factures
        String codeType = "";
        List<Facture> listeFacturesTemp = new ArrayList<>();
        List<Facture> listeFactures = new ArrayList<>();
        List<Facture> listeFacturesSoumises = new ArrayList<>();
        TypeFacture typeFacture = typeFactureService.findById(idTypeFacture);

        if (typeDeTravailleur.equals(COLLABORATORS_FR_LOWER_CASE)) codeType = StatutCollaborateur.STATUT_COLLABORATEUR;
        else {
            if (typeDeTravailleur.equals(SUBCONTRACTORS_FR_SNAKE_CASE))
                codeType = StatutCollaborateur.STATUT_SOUS_TRAITANT;
        }

        try {
            switch (typeFacture.getCode()) {
                case TypeFacture.TYPEFACTURE_FACTURABLE:
                    listeFacturesTemp = factureService.findFacturablesByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_FRAIS:
                    listeFacturesTemp = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN:
                    listeFacturesTemp = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (!codeType.equals("")) {
            if (!typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                for (Facture fact : listeFacturesTemp) {
                    if (codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && fact.getMission().getCollaborateur().getStatut().getCode().equals(codeType)) {
                        listeFactures.add(fact);
                    } else {
                        if (!codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && !fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                            listeFactures.add(fact);
                        }
                    }
                }
            } else {
                listeFactures = listeFacturesTemp;
            }
        }

        //on passe toutes les factures à l'état soumis
        Etat etatSoumis = etatService.getEtatByCode(ETAT_SOUMIS_CODE);
        Etat etatGenere = etatService.getEtatByCode(ETAT_GENERE);
        Etat etatEdite = etatService.getEtatByCode(ETAT_EDITE);
        for (Facture facture : listeFactures) {
            //SBE_AMNOTE_183 Si l'état à la base n'est pas soumis
            if (facture.getEtat().equals(etatGenere) || facture.getEtat().equals(etatEdite)) {
                listeFacturesSoumises.add(facture);
                facture.setEtat(etatSoumis);
                factureService.createOrUpdateFacture(facture); // On sauvegarde la facture
            }
        }

        //création de l'excel de récap des factures soumises
        String cheminXls = "/temporaire" + "/demandeDeSoumission";

        //On regarde le numero de l'odf à laquelle on demande la soumission
        String chemin = Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT);
        SimpleDateFormat mois = new SimpleDateFormat(Constantes.DATE_FORMAT_MMMM);
        SimpleDateFormat annee = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY);

        Date date = dateVoulueRefonte.toDate();

        chemin = chemin + SLASH + annee.format(date);
        chemin = chemin + SLASH + mois.format(date);
        File directory2 = new File(chemin);
        String[] sousDossiers2 = directory2.list();
        int numODF;
        if (sousDossiers2 == null) {
            numODF = 1;
        } else {
            numODF = sousDossiers2.length + 1;
        }
        String lODF = "ODF" + numODF;

        String lExcelDeLaFacture;
        // on créé l'Excel de l'ODF avec la liste des factures générées en PDF
        if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURABLE)) {
            lExcelDeLaFacture = documentExcelService.createExcelRecapFacturesMois(listeFacturesSoumises, dateVoulueRefonte, cheminXls, lODF, typeFacture.getCode());
        } else if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
            lExcelDeLaFacture = documentExcelService.createExcelRecapFacturesFraisMois(listeFacturesSoumises, dateVoulueRefonte, cheminXls, lODF);
        } else {
            lExcelDeLaFacture = documentExcelService.createExcelRecapFacturesMois(listeFacturesSoumises, dateVoulueRefonte, cheminXls, lODF, typeFacture.getCode());
        }
        mailService.envoiMailFacturesSoumises(lExcelDeLaFacture, typeFacture.getId().intValue());
        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Les factures ont bien été soumises et un mail a été envoyé à la direction.");

        //On supprime l'excel temporaire après l'avoir envoyé par mail
        String cheminASupprimer = Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT) + "temporaire" + "/demandeDeSoumission" + SLASH + lExcelDeLaFacture + Constantes.EXTENSION_FILE_XLS;
        File directorySup = new File(cheminASupprimer);
        if (directorySup.getParentFile().exists()) {
            directorySup.delete();
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }

    /**
     * Méthode servant à la direction à refuser l'ordre de facturation qui a été soumis et de prévenir par mail le
     * service DRH
     *
     * @param leMois            le mois voulu
     * @param lAnnee            l'année voulue
     * @param idTypeFacture     the type facture
     * @param commentaireRefus  the commentaire refus
     * @param request           the request
     * @param model             the model
     * @param session           the session
     * @param ra                the ra
     * @param typeDeTravailleur typeDeTravailleur
     * @return string
     * @throws IOException                the io exception
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     * @throws Exception                  the exception
     */
    @RequestMapping(value = "refuserFactures/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String refuserFactures(
            @RequestParam(value = "typeFacture4", required = false) int idTypeFacture,
            @RequestParam(value = "typeDeTravailleur4", required = false) String typeDeTravailleur,
            @RequestParam(value = "commentaireRefus", required = false) String commentaireRefus,
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "leMois") String leMois,
            HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra)
            throws IOException, EncryptedDocumentException, InvalidFormatException, ParseException, NamingException, Exception {

        init(request, model);

        String codeType = "";
        List<Facture> listeFacturesTemp = new ArrayList<>();

        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);

        //on passe toutes les factures à l'état soumis
        Etat etatSoumis = etatService.getEtatByCode("SO");
        Etat etatGenere = etatService.getEtatByCode("GN");

        //SBE_AMNOTE_183 fonction qui refuse la soumission des factures par les DRH
        List<Facture> listeFactures = new ArrayList<>();
        TypeFacture typeFacture = typeFactureService.findById(idTypeFacture);

        if (typeDeTravailleur.equals(COLLABORATORS_FR_LOWER_CASE)) codeType = StatutCollaborateur.STATUT_COLLABORATEUR;
        else {
            if (typeDeTravailleur.equals(SUBCONTRACTORS_FR_SNAKE_CASE))
                codeType = StatutCollaborateur.STATUT_SOUS_TRAITANT;
        }

        try {
            switch (typeFacture.getCode()) {
                case TypeFacture.TYPEFACTURE_FACTURABLE:
                    listeFacturesTemp = factureService.findFacturablesByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_FRAIS:
                    listeFacturesTemp = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN:
                    listeFacturesTemp = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (!codeType.equals("")) {
            if (!typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                for (Facture fact : listeFacturesTemp) {
                    if (codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && fact.getMission().getCollaborateur().getStatut().getCode().equals(codeType)) {
                        listeFactures.add(fact);
                    } else {
                        if (!codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && !fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                            listeFactures.add(fact);
                        }
                    }
                }
            } else {
                listeFactures = listeFacturesTemp;
            }
        }

        for (Facture facture : listeFactures) {
            if (facture.getEtat().equals(etatSoumis)) {
                facture.setEtat(etatGenere);
                factureService.createOrUpdateFacture(facture); // On sauvegarde la facture
            }
        }

        //SBE_AMNOTE-183 envoi du mail de refus
        mailService.envoiMailFacturesConfirmeesOuRefusees(typeFacture.getId().intValue(), false, commentaireRefus);
        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Les factures soumises ont bien été refusées et un mail a été envoyé aux DRH.");
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }

    /**
     * Endpoint for validate facturation order, prevent DRH service, archiving invoices in table ami_facture_archivee,
     * empty the table ami_facture if production environment, and create a directory on server caontainning
     * all invoices as pdf files ordered by customer, and generate an excel summury
     *
     * @param invoiceTypeId      the id of the invoice type
     * @param collaboratorStatus the status of the collaborator
     * @param yearOfSearch       all invoices which dates's year equals this year of search
     * @param monthOfSearch      all invoices which dates's month equals this year of search
     * @param request            the incoming http servlet request
     * @param model              the incoming model
     * @param redirectAttributes Attributes that will be include in the redirection
     * @return the link redirection url as string
     * @throws IOException            local/network file/folder read/write issues
     * @throws InvalidFormatException some values's formats are invalid
     * @throws ParseException         parsing exceptions
     * @throws DocumentException      issues related to document(pdf, excel, csv, etc.) read/write operations
     * @throws NamingException        wrong namings
     */
    @RequestMapping(value = "validerFactures/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validateInvoices(
            @RequestParam(value = "typeFacture", required = false) long invoiceTypeId,
            @RequestParam(value = "typeDeTravailleur", required = false) String collaboratorStatus,
            @PathVariable(value = "lAnnee") String yearOfSearch,
            @PathVariable(value = "leMois") String monthOfSearch,
            HttpServletRequest request, Model model, RedirectAttributes redirectAttributes)
            throws IOException, InvalidFormatException, ParseException, DocumentException, NamingException {
        init(request, model);

        if (factureService.validateInvoices(monthOfSearch, yearOfSearch, invoiceTypeId, collaboratorStatus))
            redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Les factures ont bien été générées.");
        else
            redirectAttributes.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucune facture n'a été créée. Elles sont déjà générées.");

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }

    /**
     * Méthode qui permet de forcer les factures en entrant le nombre de jours travaillés estimés pour les factures
     * dont le RA n'a pas été soumis en fin de mois
     * Les factures ainsi remplies passent dans l'ordre de facturation et leur attribut ra_valide vaut 2 pour signifier
     * qu'elles ont été forcées
     *
     * @param lAnnee  l'année voulue
     * @param leMois  le mois voulu
     * @param session la session http
     * @param request la requête http
     * @param model   le modèle http
     * @param ra      les attributs de redirection
     * @return le string de redirection vers la page de gestion des factures du mois
     * @throws IOException l'IO exception
     */
    @RequestMapping(value = "/forcerFactures/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String forcerFactures(
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "leMois") String leMois,
            HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
            throws IOException {
        init(request, model);

        DateTime dateVoulue = extractPickedDate(leMois, lAnnee);

        List<Facture> facturesAForcer = factureService.findFacturesRANonSoumisByMonth(dateVoulue);

        for (Facture facture : facturesAForcer) {
            int id = facture.getId();
            String paramName = "quantiteFactureForcee" + id;

            String paramValue = request.getParameter(paramName);

            if (paramValue != null && !paramValue.equals("")) {
                // on met les factures à ra_valide = 2 pour signifier que la facture a été forcée.
                facture.setRaValide(2);
                facture.setQuantite(Float.valueOf(paramValue));

                facture.setMontant(facture.getQuantite() * facture.getPrix());

                factureService.createOrUpdateFacture(facture);
            }
        }

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }


    /**
     * Méthode permettant de transmettre les données de la facture à éditer à la JSP pour avoir la page d'édition de
     * la facture voulue
     *
     * @param id         the id
     * @param idCommande the id commande
     * @param session    the session
     * @param request    the request
     * @param model      the model
     * @param ra         the ra
     * @return Redirection vers la page d'édition d'une facture
     * @throws JsonProcessingException the json processing exception
     * @throws ParseException          the parse exception
     */
    @RequestMapping(value = "editFacture/{idFacture}", method = {RequestMethod.GET, RequestMethod.POST})
    public String editFacture(@PathVariable("idFacture") int id,
                              @RequestParam(value = "listeCommandes", required = false) Long idCommande,
                              HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException, ParseException {

        init(model);

        List<String> listeRIB = new ArrayList<>();
        Facture facture = null;
        Commande commande = null;
        Mission mission = null;
        Collaborator collaborateur = null;
        Float tauxJournalierMoyen = 0f;

        TypeFacture typeFacture = typeFactureService.findById(new Long(1));
        List<ElementFactureAsJson> lListElementsJson = new ArrayList<>();
        if (id == -1) {
            commande = commandeService.findById(idCommande);
            facture = new Facture();
            mission = commande.getMission();
            // On affiche les différents RIB

        } else {
            facture = factureService.findById(id);
            commande = facture.getCommande();
            typeFacture = facture.getTypeFacture();
            mission = facture.getMission();
            collaborateur = mission.getCollaborateur();

            List<ElementFacture> listeElementFacture = elementFactureService.findByFacture(facture);

            for (ElementFacture elem : listeElementFacture) {
                ElementFactureAsJson elemJson = new ElementFactureAsJson(elem);
                lListElementsJson.add(elemJson);
            }
        }

        Client client;
        if (commande != null) {
            client = facture.getCommande().getClient();
        } else {
            client = facture.getMission().getClient();
        }

        String tvaIntrac = client.getTva();

        long selectedTva = 1;

        if(clientService.isClientIntracNumberFR(client)){
            facture.setTVA(tvaService.getTVAByCode(TVA.TAUX_NORMAL));
        } else {
            facture.setTVA(tvaService.getTVAByCode(TVA.TAUX_ZERO));
        }

        if (facture.getTVA() != null) {
            selectedTva = facture.getTVA().getId();
        }
        else {
            facture.setTVA(tvaService.getTVAByCode(TVA.TAUX_NORMAL));
        }

        String selectedRib = "";
        if (facture.getRIBFacture() != null) {
            selectedRib = facture.getRIBFacture();
        } else {
            facture.setRIBFacture("");
        }

        if (facture.getRIBFacture().equals("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX")) {
            selectedRib = "BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX";
            listeRIB.add(selectedRib);
            listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
            listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");
        } else if (facture.getRIBFacture().equals("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878")) {
            selectedRib = "CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878";
            listeRIB.add(selectedRib);
            listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
            listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");
        } else if (facture.getRIBFacture().equals("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP")) {
            selectedRib = "CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP";
            listeRIB.add(selectedRib);
            listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
            listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
        } else {
            listeRIB.add("");
            listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
            listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
            listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");
        }

        //[JNA][AMNOTE 160] Les dates sont récupérées sous format yyyy-mm-dd 00:00:00 donc on retire les 00:00:00
        SimpleDateFormat sdf = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);
        if (facture.getMoisPrestations() != null) {
            facture.setMoisPrestations(java.sql.Date.valueOf(sdf.format(facture.getMoisPrestations())));
        }
        if (facture.getDateFacture() != null) {
            facture.setDateFacture(java.sql.Date.valueOf(sdf.format(facture.getDateFacture())));
        }

        //recuperation de la TVA
        TVAAsJson tvaJson;
        List<TVA> listeTVA = tvaService.findAll(null);
        if (facture.getTVA() != null)
            tvaJson = facture.getTVA().toJson();
        else
            tvaJson = listeTVA.get(0).toJson();

        Float montantTVA = tvaJson.getMontant() / 100;

        if (facture.getAdresseFacturation() == null) {
            if (client.getAdresseFacturation() != null) {
                facture.setAdresseFacturation(client.getAdresseFacturation());
            } else {
                facture.setAdresseFacturation("Adresse facturation, 00000 Ville");
            }
        }

        // We get billing managers in customer's contacts
        List<ContactClientAsJson> listContactsJson = contactClientService.findAllBillingManagersByCustomer(client)
                .stream().map(ContactClient::toJson).collect(Collectors.toList());

        long idContact = 0;
        // We're looking if there are several billing managers among the customer's contacts.
        // If not, we're taking either the one of the invoice or the order depending on whether the value has been changed or not
        if(listContactsJson.size() > 1)
        {
            // if the invoice doesn't have a billing manager, we take the default order's contact
            if (facture.getContact() > 0 )
            {
                idContact = facture.getContact();
            }
            else {
                idContact = commande.getIdResponsableFacturation();
            }
        }
        else if(1 == listContactsJson.size()) {
            // If only one match then it's the right one
            idContact = listContactsJson.get(0).getId();
        }

        if (commande != null && commande.getJustificatif() != null) {
            // Pour chaque commande on renomme le justificatif pour l'afficher dans la vue
            if ((commande.getJustificatif().length() >= SUPPORTING_LENGTH && StringUtils.isNumeric(commande.getJustificatif().substring(0, 12)))) {
                commande.setJustificatif(commande.getJustificatif().substring(SUPPORTING_LENGTH));
            } else {
                commande.setJustificatif(commande.getJustificatif());
            }
        }

        if (facture.getPrix() == 0f) {
            tauxJournalierMoyen = mission.getTjm();
        }

        // FIN [CLO AMNOTE-308] Refonte
        model.addAttribute("selectedRib", selectedRib);
        model.addAttribute("selectedTva", selectedTva);
        model.addAttribute("client", client);
        model.addAttribute("tvaIntrac", tvaIntrac);
        model.addAttribute("typeFacture", typeFacture.getId());
        model.addAttribute("listeRIB", listeRIB);
        model.addAttribute("idContact", idContact);
        model.addAttribute("listeTVA", listeTVA);
        model.addAttribute("tva", montantTVA);
        model.addAttribute("facture", facture);
        model.addAttribute("listeContact", listContactsJson);
        model.addAttribute("listeFrais", lListElementsJson);
        model.addAttribute("commande", commande);
        model.addAttribute("collaborateur", collaborateur);
        model.addAttribute("tauxJournalierMoyen", tauxJournalierMoyen);
        // model.addAttribute("adresseFacturation", adresse)

        return NAV_ADMIN + NAV_ADMIN_EDITFACTURE;
    }

    /**
     * Méthode permettant de visualiser le PDF de la facture voulue en extrayant ses données
     *
     * @param id       the id
     * @param request  the request
     * @param response the response
     * @param model    the model
     * @param session  the session
     * @param ra       the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "afficher/{idFacture}", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void afficherPdfFacture(@PathVariable("idFacture") int id, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                            RedirectAttributes ra) throws Exception {
        init(request, model);

        FactureArchivee facture = factureArchiveeService.findById(id);

        // [JNA][AMNOTE 158] On crée le chemin de stockage de la facture temporaire
        String nomFichier = "Fac_" + facture.getNumero() + Constantes.EXTENSION_FILE_PDF;
        String chemin = facture.getCheminPDF();
        File directory = new File(chemin);


        if (nomFichier != "Erreur fichier") {

            //--- Récupération du fichier
            File lFile = new File(facture.getCheminPDF());

            // --- Vérification de l'existance du fichier
            if (!lFile.exists()) {
                throw new Exception("Fichier '" + lFile.getPath() + "'" + ": introuvable");
            }

            //[JNA][AMNOTE 158] Ouverture du fichier dans un nouvel onglet
            response.setHeader("Location:", lFile.getPath());

            //--- Copie du fichier dans la réponse HTTP
            try (FileInputStream fileInputStream = new FileInputStream(lFile)) {
                FileCopyUtils.copy(fileInputStream, response.getOutputStream());
            } catch (IOException e) {
                throw e;
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Problème de récupération du fichier");
        }
    }

//SBE_AMNOTE_154_17/11/2016

    /**
     * Méthode appelée dans la page des factures archivées pour les transmettre à la JPS via le modèle et ainsi
     * afficher la pages des factures archivées avec un tableau par ODF pour le mois voulu
     *
     * @param moisVoulu the mois voulu
     * @param session   the session
     * @param request   the request
     * @param model     the model
     * @param ra        the ra
     * @return Redirection vers la page d'édition d'une facture
     * @throws JsonProcessingException the json processing exception
     * @throws ParseException          the parse exception
     */
    @RequestMapping(value = "facturesValidees/{moisVoulu}", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationFacturesArchivees(@PathVariable("moisVoulu") String moisVoulu, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException, ParseException {

        init(request, model);
        HashMap<String, List<FactureArchivee>> mapOdfFacture = new HashMap<>();
        HashMap<String, Integer> mapOdfTypeFacture = new HashMap<>();
        List<String> listODF = new ArrayList<>();

        List<FactureArchivee> listFacturesArchiveesduMois;

        // CLO - 25/04/2017 - AMNOTE-281 : On va changer --> faire directement la liste des factures du mois via une requête CRITERIA restreinte grâce au mois de la date d'archivage, enregistrée quand la facture est archivée par les patrons

        // obtention et gestion du mois sélectionné
        java.sql.Date pDateDEBUTTEST; // date de début du mois
        java.sql.Date pDateFINTEST; // date de fin du mois
        Calendar pCalDEBUT;
        Calendar pCalFIN;

        String leMoisSelectionne, lAnneeSelectionnee;
        SimpleDateFormat annee = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY);
        if (!moisVoulu.equals("null")) {
            //Récupération de l'action (année ou mois) et de la date choisie pour extract facturation
            String action = request.getParameter("action");
            String pDate = request.getParameter(MONTH_YEAR_EXTRACT);
            pCalDEBUT = Calendar.getInstance();
            pCalFIN = Calendar.getInstance();
            if (pDate != null && !pDate.isEmpty()) {
                Integer pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
                Integer pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
                pCalDEBUT.set(pYear, pMonth, 1);

                pCalFIN.set(pYear, pMonth + 1, 1);
            }
            lAnneeSelectionnee = Integer.valueOf(pDate.substring(pDate.length() - 4)).toString();
            leMoisSelectionne = pCalDEBUT.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        } else {
            pCalDEBUT = Calendar.getInstance();
            pCalDEBUT.set(pCalDEBUT.get(Calendar.YEAR), pCalDEBUT.get(Calendar.MONTH), 1);
            pCalFIN = Calendar.getInstance();
            pCalFIN.set(pCalDEBUT.get(Calendar.YEAR), pCalDEBUT.get(Calendar.MONTH) + 1, 1);
            Date today = new Date();
            lAnneeSelectionnee = annee.format(today);
            leMoisSelectionne = pCalDEBUT.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        }

        pCalDEBUT.add(Calendar.DATE, -1);
        pCalFIN.add(Calendar.DATE, -1);
        pDateDEBUTTEST = new java.sql.Date(pCalDEBUT.getTime().getTime());
        pDateFINTEST = new java.sql.Date(pCalFIN.getTime().getTime());

        // Création de la liste des factures archivées correspondant au mois choisi par requête CRITERIA basé sur des dates de la classe sql.Date
        listFacturesArchiveesduMois = factureArchiveeService.findByStartEndDates(pDateDEBUTTEST, pDateFINTEST);

        String odfFacture = "";
        boolean existODF;

        //on recupere tout les ODF existant du mois concernes
        for (FactureArchivee fact : listFacturesArchiveesduMois) {
            existODF = false;
            // Sous linux, le numéro d'ODF est en antépénultième (avant-avant dernier) position du chemin (sur windows, il est en 2ème position)
            String[] pathParts = fact.getCheminPDF().split(SLASH);
            for (String part : pathParts) {
                if (part.contains("ODF")) {
                    odfFacture = part;
                }
            }
            for (String odf : listODF) {
                if (odfFacture.equals(odf)) {
                    existODF = true;
                }
            }
            if (existODF == false) {
                listODF.add(odfFacture);
            }
        }

        Integer idTypeFacture = 1;
        List<FactureArchivee> listTempFacture;
        //pour chaque odf, on rempli la map
        for (String odf : listODF) {
            listTempFacture = new ArrayList<>();

            for (FactureArchivee fact : listFacturesArchiveesduMois) {
                // Sous linux, le numéro d'ODF est en antépénultième (avant-avant dernier) position du chemin (sur windows, il est en 2ème position)
                String[] pathParts = fact.getCheminPDF().split(SLASH);
                for (String part : pathParts) {
                    if (part.contains("ODF")) {
                        odfFacture = part;
                    }
                }
                if (odfFacture.equals(odf)) {
                    listTempFacture.add(fact);
                    idTypeFacture = fact.getTypeFacture();
                }
            }

            //Ajouter dans la map
            mapOdfFacture.put(odf, listTempFacture);
            mapOdfTypeFacture.put(odf, idTypeFacture);
        }

        model.addAttribute("mapOdfFacture", mapOdfFacture);
        model.addAttribute("mapOdfTypeFacture", mapOdfTypeFacture);
        model.addAttribute("leMois", WordUtils.capitalize(leMoisSelectionne));
        model.addAttribute("lAnnee", lAnneeSelectionnee);

        return NAV_ADMIN + NAV_ADMIN_FACTURES_VALIDEES;
    }

    /**
     * Méthode permettant de visualiser la facture en cours d'édition au format PDF en extrayant les donénes entrées
     * dans la page d'édition
     *
     * @param listeJsonElementsFacture the liste json elements facture
     * @param idTypeFacture            the type facture
     * @param idCommande               the id commande
     * @param idContact                the id contact
     * @param moisPrestations          the mois prestations
     * @param dateFacturation          the date facturation
     * @param quantite                 the quantite
     * @param prix                     the prix
     * @param montant                  the montant
     * @param client                   the client
     * @param adresse                  the adresse
     * @param adresseFacturation       the adresse facturation
     * @param ribFacture               the rib facture
     * @param commentaire              the commentaire
     * @param request                  the request
     * @param response                 the response
     * @param model                    the model
     * @param ra                       the ra
     * @param factureJson              the facture json
     * @param isAvoir                  the avoir
     * @param idtva                    the hidden
     * @param idMission                idMission
     * @param numFacture               numFacture
     * @throws Exception the exception
     */
    @RequestMapping(value = "visualisez", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void visualisationPdfFacture(@RequestParam(value = "listeJsonElementsFacture", required = false) String listeJsonElementsFacture,
                                 @RequestParam(value = "idTypeFacture", required = false) Integer idTypeFacture,
                                 @RequestParam(value = "idMission") Long idMission,
                                 @RequestParam(value = "idCommande", required = false) Long idCommande,
                                 @RequestParam(value = "mission.client.id", required = false) int idContact,
                                 @RequestParam(value = "moisPrestations", required = false) Date moisPrestations,
                                 @RequestParam(value = "dateFacturation", required = false) Date dateFacturation,
                                 @RequestParam(value = "quantite", required = false) Float quantite,
                                 @RequestParam(value = "prix", required = false) Float prix,
                                 @RequestParam(value = "montant", required = false) Float montant,
                                 @RequestParam(value = "client", required = false) String client,
                                 @RequestParam(value = "adresse", required = false) String adresse,
                                 @RequestParam(value = "adresseFacturation", required = false) String adresseFacturation,
                                 @RequestParam(value = "ribFacture", required = false) String ribFacture,
                                 @RequestParam(value = "commentaire", required = false) String commentaire,
                                 @RequestParam(value = "numFacture", required = false) String numFacture,
                                 @RequestParam(value = "idIsAvoir", required = false) boolean isAvoir,
                                 @RequestParam(value = "hiddenTva", required = false) int idtva,
                                 HttpServletRequest request, HttpServletResponse response, Model model,
                                 RedirectAttributes ra, FactureAsJson factureJson) throws Exception {
        init(request, model);
        //[JNA][AMNOTE 168] Récupération des élémentsFacture (qui arrivent sous forme de String Json puis on parse)
        List<ElementFactureAsJson> elementFactureAsJson = JacksonJsonListConverter.fromJson(new TypeReference<List<ElementFactureAsJson>>() {
        }, listeJsonElementsFacture);
        // On récupère la facture
        Facture facture = factureService.findById(factureJson.getId());
        // On récupère la commande lié à la facture
        Commande commande = null;
        Mission mission = missionService.get(idMission);
        TypeFacture typeFacture = typeFactureService.findById(new Long(idTypeFacture));
        TVA tva = tvaService.get(new Long(idtva));
        if (facture == null) {
            // On crée une nouvelle facture
            facture = new Facture();
            // On lui assigne le codeTypeFacture = 1 car création d'une facture vierge
            facture.setTypeFacture(typeFacture);
            // On récupère la commande à laquelle on a lié notre nouvelle facture
            commande = commandeService.findById(idCommande);
            facture.setMission(mission);
            facture.setCommande(commande);
        } else { // Si on est dans le cas d'édition
            // On récupère la commande déjà liée à la facture
            if (facture.getCommande() != null) {
                commande = commandeService.findById(facture.getCommande().getId());
            }
        }

        // On récupère la mission de la commande
        mission = missionService.get(facture.getMission().getId());

        try {
            //[JNA][AMNOTE 168] On rempli la facture avec les paramètres reçus
            facture = remplissageFacture(facture,
                    typeFacture,
                    idContact,
                    moisPrestations,
                    dateFacturation,
                    quantite,
                    prix,
                    montant,
                    client,
                    adresse,
                    adresseFacturation,
                    ribFacture,
                    commentaire,
                    numFacture,
                    isAvoir,
                    tva);
        } catch (IOException e) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, e.getMessage());
        }

        // On crée le PDF a visualiser
        // [JNA][AMNOTE 158] On crée le chemin de stockage de la facture temporaire
        String chemin = Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT);
        chemin = chemin + "/temporaire";
        File directory = new File(chemin);
        directory.mkdirs();

        //[JNA][AMNOTe 168] Si la liste des elementFacture n'est pas vide les les sauvegarde un par un
        List<ElementFacture> elements = new ArrayList<>();
        if (!elementFactureAsJson.isEmpty()) {
            for (ElementFactureAsJson elementFactureJson : elementFactureAsJson) {
                ElementFacture elem = new ElementFacture(elementFactureJson);
                elements.add(elem);
            }
        }

        Client clientFacture;
        if (commande != null) {
            clientFacture = commande.getClient();
        } else {
            clientFacture = mission.getClient();
        }

        String nomFichier = "Fac_" + facture.getNumFacture() + Constantes.EXTENSION_FILE_PDF;

        nomFichier = pdfBuilderFacture.createPdfFacture(mission, facture, elements, facture.getContact(), true, null, new DateTime(dateFacturation), clientFacture);

        if (nomFichier != "Erreur fichier") {

            //--- Récupération du fichier
            File lFile = new File(Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT) + "temporaire" + SLASH + nomFichier);

            // --- Vérification de l'existance du fichier
            if (!lFile.exists()) {
                throw new Exception("Fichier '" + lFile.getPath() + "'" + ": introuvable");
            }

            //[JNA][AMNOTE 158] Ouverture du fichier dans un nouvel onglet
            response.setHeader("Location:", lFile.getPath());

            //--- Copie du fichier dans la réponse HTTP
            try (FileInputStream fileInputStream = new FileInputStream(lFile)) {
                FileCopyUtils.copy(fileInputStream, response.getOutputStream());
            } catch (IOException e) {
                logger.error("Erreur lors de la visualisation du pdf : " + e.getMessage());
                throw e;
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Problème de récupération du fichier");
        }
    }

    /**
     * Remplissage la facture en cours d'édition
     *
     * @param facture            Facture
     * @param typeFacture        TypeFacture
     * @param idContact          int
     * @param moisPrestations    Date
     * @param dateFacturation    Date
     * @param quantite           Float
     * @param prix               Float
     * @param montant            Float
     * @param client             String
     * @param adresse            String
     * @param adresseFacturation String
     * @param ribFacture         String
     * @param commentaire        String
     * @param isAvoir            Boolean
     * @param tva                TVA
     * @throws IOException    the io exception
     * @throws ParseException the parse exception
     */
    private Facture remplissageFacture(Facture facture,
                                       TypeFacture typeFacture,
                                       int idContact,
                                       Date moisPrestations,
                                       Date dateFacturation,
                                       Float quantite,
                                       Float prix,
                                       Float montant,
                                       String client,
                                       String adresse,
                                       String adresseFacturation,
                                       String ribFacture,
                                       String commentaire,
                                       String numFacture,
                                       boolean isAvoir,
                                       TVA tva) throws IOException, ParseException {

        if (moisPrestations != null) {
            facture.setMoisPrestations(moisPrestations); // Récupération du mois des prestations
        } else {
            Date today = new Date();
            facture.setMoisPrestations(today);
        }

        if (dateFacturation != null) {
            facture.setDateFacture(dateFacturation); // Récupération de la date de facturation de la facture
        } else {
            facture.setDateFacture(null);
        }

        if (idContact != 0) { // S'il y a un contact de sélectionné
            facture.setContact(idContact); // On la prend pour la facture
        } else { // Sinon on utilise celle du client
            facture.setContact(0);
        }

        facture.setTypeFacture(typeFacture);

        if (StringUtils.isNotBlank(adresseFacturation)) { // Si l'adresse de facturation n'est pas vide
            facture.setAdresseFacturation(adresseFacturation); // On la prend pour la facture
        } else { // Sinon on utilise celle du client
            facture.setAdresseFacturation("");
        }

        if (StringUtils.isNotBlank(client)) { // Si le client n'est pas vide
            Client clientFacture = clientService.findByNom(client); // On récupère le client via son nom
            facture.setIdClient(clientFacture.getId().intValue()); // On met l'id du client dans la facture
        } else { // Sinon on le met à 0
            facture.setIdClient(0);
        }

        if (montant != null && 0.0f != montant) { // On récupère le montant s'il n'est pas vide
            facture.setMontant(montant);
        } else {// Sinon on le met à 0
            facture.setMontant(0);
        }

        if (prix != null && 0.0f != prix) { // On récupère le prix s'il n'est pas vide
            facture.setPrix(prix);
        } else {// Sinon on le met à 0
            facture.setPrix(0);
        }

        if (tva != null) { // On récupère la tva mamen
            facture.setTVA(tva);
        } else {// Sinon on le met à 20%
            facture.setTVA(tvaService.get(new Long(1)));
        }

        if (quantite != null && 0.0f != quantite) { // On récupère la quantité si elle n'est pas vide
            facture.setQuantite(quantite);
        } else {// Sinon on le met à 0
            facture.setQuantite(0);
        }

        if (StringUtils.isNotBlank(ribFacture)) { // On récupère la date de facturation si elle n'est pas vide
            facture.setRIBFacture(ribFacture);
        } else {// Sinon on le met à 0
            facture.setRIBFacture(null);
        }

        if (StringUtils.isNotBlank(commentaire)) { // On récupère lle commentaire
            facture.setCommentaire(commentaire);
            /*if (facture.getCommande() != null) {
                facture.getCommande().setCommentaire(commentaire);
            }
        } else {// Sinon on le met à 0
            facture.setCommentaire(null);*/
        } else {// Sinon on le met à 0
            facture.setCommentaire(null);
        }

        if (StringUtils.isNotBlank(numFacture)) {
            if (!factureService.checkExistsNumFacture(Integer.parseInt(numFacture), facture.getId())) {
                facture.setNumFacture(Integer.valueOf(numFacture));
            }
        }

        facture.setIsAvoir(isAvoir);
        return facture;
    }

    /**
     * Méthode permettant de sauvegarder les modifications apportées à ne facture dans sa page d'édition et qui renvoie
     * vers la page de gestion des factures du mois
     *
     * @param numFacture               the num facture
     * @param idTypeFacture            the type facture
     * @param idCommande               the id commande
     * @param idContact                the id contact
     * @param listeJsonElementsFacture the liste json elements facture
     * @param moisPrestations          the mois prestations
     * @param dateFacturation          the date facturation
     * @param quantite                 the quantite
     * @param prix                     the prix
     * @param montant                  the montant
     * @param client                   the client
     * @param adresse                  the adresse
     * @param adresseFacturation       the adresse facturation
     * @param ribFacture               the rib facture
     * @param commentaire              the commentaire
     * @param request                  the request
     * @param model                    the model
     * @param factureJson              the facture json
     * @param ra                       the ra
     * @param isAvoir                  the avoir
     * @param idMission                idMission
     * @param idtva                    idTva
     * @return the string
     * @throws IOException    the io exception
     * @throws ParseException the parse exception
     */
    @RequestMapping(value = {"saveFacture", "saveFacture/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String saveFacture(@RequestParam(value = "idTypeFacture", required = false) Integer idTypeFacture,
                              @RequestParam(value = "idMission", required = false) Long idMission,
                              @RequestParam(value = "idCommande", required = false) Long idCommande,
                              @RequestParam(value = "mission.client.id", required = false) int idContact,
                              @RequestParam(value = "listeJsonElementsFacture", required = false) String listeJsonElementsFacture,
                              @RequestParam(value = "moisPrestations", required = false) Date moisPrestations,
                              @RequestParam(value = "dateFacturation", required = false) Date dateFacturation,
                              @RequestParam(value = "quantite", required = false) Float quantite,
                              @RequestParam(value = "prix", required = false) Float prix,
                              @RequestParam(value = "montant", required = false) Float montant,
                              @RequestParam(value = "client", required = false) String client,
                              @RequestParam(value = "adresse", required = false) String adresse,
                              @RequestParam(value = "adresseFacturation", required = false) String adresseFacturation,
                              @RequestParam(value = "ribFacture", required = false) String ribFacture,
                              @RequestParam(value = "commentaire", required = false) String commentaire,
                              @RequestParam(value = "numFacture", required = false) String numFacture,
                              @RequestParam(value = "idIsAvoir", required = false) boolean isAvoir,
                              @RequestParam(value = "hiddenTva", required = false) int idtva,
                              HttpServletRequest request, Model model, FactureAsJson factureJson,
                              RedirectAttributes ra) throws IOException, ParseException {
        init(model);
        //[JNA][AMNOTE 168] Récupération des élémentsFacture (qui arrivent sous forme de String Json puis on parse)
        List<ElementFactureAsJson> elementFactureAsJson = JacksonJsonListConverter.fromJson(new TypeReference<List<ElementFactureAsJson>>() {
        }, listeJsonElementsFacture);
        //[JNA][ANMOTE 168] On récupère la facture
        Facture facture = factureService.findById(factureJson.getId()); // Récupération de la facture
        Commande commande = null;
        Client c = clientService.findByNom(client);
        TypeFacture typeFacture = typeFactureService.findById(new Long(idTypeFacture));
        Mission mission = missionService.get(idMission);

        TVA tva = tvaService.get(new Long(idtva));

        //[JNA][AMNOTE 168] Si la facture est null on est dans le cas d'une création
        if (facture == null) {
            // On crée une nouvelle facture
            facture = new Facture();
            // On lui assigne le codeTypeFacture = 1 car création d'une facture vierge
            facture.setTypeFacture(typeFacture);
            // On récupère la commande à laquelle on a lié notre nouvelle facture
            commande = commandeService.findById(idCommande);
            facture.setMission(mission);
            facture.setCommande(commande);
        } else { // Si on est dans le cas d'édition
            // On récupère la commande déjà liée à la facture
            if (facture.getCommande() != null) {
                commande = commandeService.findById(facture.getCommande().getId());
            }
        }

        //[AMA][AMNOTE 576] on récupère le #hash relatif à l'URL de la facture traitée
        String redir = factureService.findTypeFactureById(idTypeFacture, idMission);

        try {
            //[JNA][AMNOTE 168] On rempli la facture avec les paramètres reçus
            facture = remplissageFacture(facture,
                    typeFacture,
                    idContact,
                    moisPrestations,
                    dateFacturation,
                    quantite,
                    prix,
                    montant,
                    client,
                    adresse,
                    adresseFacturation,
                    ribFacture,
                    commentaire,
                    numFacture,
                    isAvoir,
                    tva);
        } catch (IOException e) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, e.getMessage());
        }

        if (factureJson.getId() != 0) { // si la facture a un ID on est en mode édition
            //[JNA][AMNOTE 168] On récupère un Etat d'édition
            Etat etatEdition = etatService.getEtatByCode("ED");
            //[JNA][AMNOTE 168] On l'assigne à la facture
            facture.setEtat(etatEdition);
        } else { // Sinon on est en mode création
            //[JNA][AMNOTE 168] On récupère un Etat d'édition
            Etat etatCreation = etatService.getEtatByCode("GN");
            //[JNA][AMNOTE 168] On l'assigne à la facture
            facture.setEtat(etatCreation);
        }
        //[JNA][AMNOTE 168] On sauvegarde la facture
        int id = 0;
        // Ajout d'un try catch pour faire remonter les erreurs au controller
        try {
            // [CLO AMNOTE-308] Refonte
            factureService.createOrUpdateFacture(facture);
            if (commande != null) {
                /*commande.setCommentaire(facture.getCommentaire());*/
                commandeService.createOrUpdateCommande(commande, facture.getMission());
            }

        } catch (Exception e) {
            ra.addFlashAttribute("hash", redir);
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, e.getMessage());
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
        }

        // [JNA][AMNOTE 168] On récupère l'ID qui vient d'être généré par la requête pour lier les elementFacture à la facture par l'id de la facture
        factureJson.setId(id);

        //[JNA][AMNOTE 168] Si la liste des elementFacture n'est pas vide les les sauvegarde un par un
        if (!elementFactureAsJson.isEmpty()) {
            for (ElementFactureAsJson elementFacture : elementFactureAsJson) {
                elementFactureService.createOrUpdateElementFacture(elementFacture, facture);
            }
        }

        ra.addFlashAttribute("hash", redir);

        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Création/Modification effectuée avec succès");

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }

    //[JNA][AMNOTE 142] Supression de frais à partir de l'édition de la facture

    /**
     * Delete frais string.
     *
     * @param request the request
     * @param idFrais the id frais
     * @return string
     */
    @RequestMapping(value = "deleteFrais/{idFrais}", method = {RequestMethod.POST, RequestMethod.GET})
    public
    @ResponseBody
    String deleteFrais(HttpServletRequest request, @PathVariable("idFrais") int idFrais) {
        ElementFacture element = elementFactureService.findById(idFrais);
        String retour = elementFactureService.deleteElementFacture(element);
        return retour;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Download a single pdf of all archived invoices (factures archivées) of a given month and year
     *
     * @param yearOfSearch  the year corresponding to the invoice year
     * @param monthOfSearch the month corresponding to the invoice month
     * @param request       http request
     * @param response      http reponse
     * @param model         model
     * @param session       session
     * @param ra            redirect atribute
     * @return a response wich include the result pdf of all archived invoices of a given month and year
     * @throws Exception NamingException, IOException or DocumentException related to pdf read/write problems
     */
    @RequestMapping(value = "saveFactureArchivee/{yearOfSearch}/{monthOfSearch}", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<byte[]> saveFactureArchivee(
            @PathVariable("yearOfSearch") Integer yearOfSearch, @PathVariable("monthOfSearch") String monthOfSearch, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, RedirectAttributes ra
    ) throws Exception {

        init(request, model);

        // [AMNOTE-652] Création d'un PDF de toutes les factures archivées, selon un moi de recherche
        String resultFileDestinationPath = factureArchiveeService.mergeArchivedInvoicesPdfFiles(yearOfSearch, monthOfSearch);

        // lire le fichier pdf résultat et l'inclure dans la réponse
        byte[] contents = fileService.readFileByPath(resultFileDestinationPath + EXTENSION_FILE_PDF);

        // Setter les headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(FileFormatEnum.PDF.toString()));
        headers.setContentDispositionFormData(
                concatenateObjectsToString(monthOfSearch, UNDERSCORE, yearOfSearch, EXTENSION_FILE_PDF),
                concatenateObjectsToString(monthOfSearch, UNDERSCORE, yearOfSearch, EXTENSION_FILE_PDF)
        );
        headers.setCacheControl(MUST_REVALIDATE);

        responseEntity = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);

        // Après avoir inclus le fichier pdf généré incluant toutes les factures dans la réponse on va le supprimer du disque
        fileService.deleteFile(new File(resultFileDestinationPath + EXTENSION_FILE_PDF));

        return responseEntity;
    }

    /**
     * Création et édition d'une facture vierge
     *
     * @param idMission       l'ide de mission
     * @param lAnnee          l'année voulue
     * @param leMois          le mois voulu
     * @param session         the session
     * @param request         the request
     * @param model           the model
     * @param ra              the ra
     * @param codeTypeFacture codeTypeFacture
     * @param numCommande     numCommande
     * @return Redirection vers la page d'édition d'une facture
     * @throws JsonProcessingException the json processing exception
     * @throws ParseException          the parse exception
     */
    @RequestMapping(value = "creationFactureVierge/{codeTypeFacture}/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public String creationFactureFrais(
            @RequestParam(value = "listMissionsClientesDuMoisVoulu", required = false) Long idMission,
            @RequestParam(value = "listCommandesDuMoisVouluMission", required = false) Long numCommande,
            @PathVariable(value = "leMois") String leMois,
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "codeTypeFacture") String codeTypeFacture,
            HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException, ParseException {

        init(request, model);
        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);

        SimpleDateFormat sdf = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);
        List<String> listeRIB = new ArrayList<>();

        TypeFacture typeFacture = typeFactureService.findByCode(codeTypeFacture);

        // [CLO AMNOTE-308] REFONTE
        if (idMission != 0) {
            Mission mission = missionService.get(idMission);

            Float tauxJournalierMoyen = 0f;
            if (!typeFacture.TYPEFACTURE_FACTURE_FRAIS.equals(codeTypeFacture) && mission.getTjm() != null) {
                tauxJournalierMoyen = mission.getTjm();
            }

            Commande commande = null;


            if (numCommande != null && numCommande != 0) {
                commande = commandeService.get(numCommande);
            } else {
                List<Commande> commandes = commandeService.findByIdMissionByMonth(mission, dateVoulueRefonte);

                if (!commandes.isEmpty()) {
                    commande = commandes.get(0);
                } else {
                    commandes = commandeService.findByIdMission(mission);
                    commande = commandes.get(0);
                }
            }

            Facture facture = new Facture();
            facture.setMission(mission);

            DateTime dateFacture;

            if (codeTypeFacture.equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                dateFacture = new DateTime();
            } else {
                dateFacture = dateVoulueRefonte.dayOfMonth().withMaximumValue();
                dateFacture = factureService.getMostRecentWorkingDay(dateFacture);
            }

            facture.setDateFacture(java.sql.Date.valueOf(sdf.format(dateFacture.toDate())));

            String selectedRib = "";

            long selectedTva = tvaService.updateTVAIntracRate(commande, facture);

            if (commande != null) {
                facture.setCommande(commande);
            }

            Client client;
            if (commande != null) {
                client = commande.getClient();
            } else {
                client = mission.getClient();
            }


            if (client.getRibClient() != null) {
                selectedRib = client.getRibClient();
            }

            // On affiche les différent RIB
            listeRIB.add("");
            listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
            listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
            listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");
            // On récupère la TVA
            List<TVA> listeTVA = tvaService.findAll(null);
            TVAAsJson tvaJson = listeTVA.get(0).toJson();
            Float montantTVA = tvaJson.getMontant() / 100;
            // On récupère le client de la commande

            if (client.getAdresseFacturation() == null) {
                client.setAdresseFacturation(client.getAdresse().replace("\n", " "));
            } else {
                client.setAdresseFacturation(client.getAdresseFacturation().replace("\n", " "));
            }

            if (facture.getCommande() != null && facture.getCommande().getClient() != null && facture.getCommande().getClient().getAdresseFacturation() != null) {
                facture.setAdresseFacturation(facture.getCommande().getClient().getAdresseFacturation());
            } else {
                if (client.getAdresseFacturation() != null) {
                    facture.setAdresseFacturation(client.getAdresseFacturation());
                }
            }

            // [AMNT-598]On vérifie d'abord s'il y a un Resp Facturation attribué à la commande (nouvelle colonne table "Commande" en BDD)
            // s'il n'y en a pas on utilise l'ancienne méthode qui selectionne le dernier responsable facturation du client
            Long idContact = 0l;
            if (commande.getIdResponsableFacturation() != null) {
                idContact = commande.getIdResponsableFacturation();
            } else {
                idContact = Long.valueOf(clientService.getResponsableFacturation(client));
            }

            List<ContactClientAsJson> listeContactJson = contactClientService.findAllContactsForClient(client);

            if (facture.getCommentaire() == null && facture.getMission() != null) {
                if (!codeTypeFacture.equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                    if (commande != null && commande.getCommentaire() != null) {
                        facture.setCommentaire(commande.getCommentaire());
                    } else {
                        facture.setCommentaire("");
                    }
                } else {
                    facture.setCommentaire("Frais - " + facture.getMission().getCollaborateur().getPrenom() + " " + facture.getMission().getCollaborateur().getNom() + " : ");
                }
            }

            /* facture.setAdresseFacturation(client.getAdresseFacturation());*/
            model.addAttribute("selectedRib", selectedRib);
            model.addAttribute("selectedTva", selectedTva);
            model.addAttribute("mission", mission);
            model.addAttribute("typeFacture", typeFacture.getId());
            model.addAttribute("listeRIB", listeRIB);
            model.addAttribute("listeTVA", listeTVA);
            model.addAttribute("facture", facture);
            model.addAttribute("idContact", idContact);
            model.addAttribute("commande", commande);
            model.addAttribute("tva", montantTVA);
            model.addAttribute("listeContact", listeContactJson);
            model.addAttribute("adresseFacturation", client.getAdresseFacturation());
            model.addAttribute("tauxJournalierMoyen", tauxJournalierMoyen);
        }

        return NAV_ADMIN + NAV_ADMIN_EDITFACTURE;

    }

// [OZE] [AMNOTE] 25-11-2016  Method pour effacer une facture

    /**
     * Delete facture string.
     *
     * @param id      the id
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page de gestion des factures
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "deleteFacture/{idFacture}", method = {RequestMethod.GET, RequestMethod.POST})
    public String deleteFacture(@PathVariable("idFacture") int id, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
            throws JsonProcessingException {
        init(request, model);

        if (id != 0) {
            Facture tmpFacture = factureService.findById(id);

            if (tmpFacture != null) {

                List<ElementFacture> listeElem = elementFactureService.findByFacture(tmpFacture); // On récupère les éléments de factures si il y en a
                listeElem.forEach(e -> elementFactureService.deleteElementFacture(e)); // On les supprime avant pour eviter les conflits d'intégrité référentiel
                String retour = factureService.deleteFacture(tmpFacture);  // On supprime la facture ensuite

                if (retour.contains("succès")) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "La suppression de la facture s'est bien effectuée.");
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "La suppression de la facture  s'est mal effectuée.");
                }
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "L'id de la commande est null");
        }

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_FACTURES + NULL;
    }

    //[SBE] Méthode pour télecharger toutes les factures en un seul PDF

    /**
     * download factures
     *
     * @param leMois             le mois voulu
     * @param lAnnee             l'année voulue
     * @param typeDeTravailleurs type de trvailleurs
     * @param codeTypeFacture    the type facture
     * @param request            the request
     * @param response           the response
     * @param model              the model
     * @param session            the session
     * @return response entity
     * @throws FileNotFoundException the file not found exception
     * @throws IOException           the io exception
     * @throws NamingException       the naming exception
     * @throws MessagingException    the messaging exception
     * @throws DocumentException     the document exception
     * @throws SchedulerException    the scheduler exception
     */
    @RequestMapping(value = "/factures/SaveAllFacturesPDF/{typeDeTravailleurs}/{codeTypeFacture}/{leMois}/{lAnnee}", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<byte[]> SaveAllFacturesPDF(
            @PathVariable("codeTypeFacture") String codeTypeFacture,
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "leMois") String leMois,
            @PathVariable(value = "typeDeTravailleurs") String typeDeTravailleurs,
            HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session)
            throws FileNotFoundException, IOException, NamingException, MessagingException, DocumentException, SchedulerException {

        init(request, model);

        String codeType = "";
        List<Facture> listeFacturesFactices = new ArrayList<>();

        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);
        Etat etatArchive = etatService.getEtatByCode("AR");
        List<Facture> listeFacturesTemp = new ArrayList<>();
        List<Facture> listeFactures = new ArrayList<>();
        boolean fraisOuPas = false;

        TypeFacture typeFacture = typeFactureService.findByCode(codeTypeFacture);

        if (typeDeTravailleurs.equals(COLLABORATORS_FR_LOWER_CASE)) codeType = StatutCollaborateur.STATUT_COLLABORATEUR;
        else {
            if (typeDeTravailleurs.equals(SUBCONTRACTORS_FR_SNAKE_CASE))
                codeType = StatutCollaborateur.STATUT_SOUS_TRAITANT;
        }

        try {
            switch (typeFacture.getCode()) {
                case TypeFacture.TYPEFACTURE_FACTURABLE:
                    DateTime[] startEndSearchDateTimes = firstAndLastDatesOfMonth(
                            Integer.parseInt(lAnnee),
                            Integer.parseInt(leMois)
                    );
                    listeFacturesFactices = factureService.findUnarchivedInvoicesByTypeAndValidRa(
                            startEndSearchDateTimes[0].toDate(),
                            startEndSearchDateTimes[1].toDate(),
                            typeFacture,
                            -1,1,2 // AR cancelled (-1), validated (1) or invalided (2)
                            );
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_FRAIS:
                    listeFacturesFactices = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN:
                    listeFacturesFactices = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (!codeType.equals("") && !typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
            for (Facture fact : listeFacturesFactices) {
                if (fact.getAdresseFacturation() == null) {
                    Client client = fact.getCommande().getClient();
                    if (client.getAdresseFacturation() != null) {
                        fact.setAdresseFacturation(client.getAdresseFacturation());
                    } else {
                        fact.setAdresseFacturation("Adresse facturation, 00000 Ville");
                    }
                }
                if (codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && fact.getMission().getCollaborateur().getStatut().getCode().equals(codeType)) {
                    listeFactures.add(fact);
                } else {
                    if (!codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && !fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                        listeFactures.add(fact);
                    }
                }
            }
        } else {
            listeFactures = listeFacturesFactices;
        }

        //A quoi cela sert ? NE fonctionne pas
        for (Facture fact : listeFacturesTemp) {
            int idEtat = fact.getEtat().getId();
            if (idEtat != etatArchive.getId()) {
                Facture fac = factureService.findById(fact.getId());
                listeFactures.add(fac);
            }

        }
        // Tri de la liste par numéro de facture
        Collections.sort(listeFactures);

        String chemin = Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT) + "temporaire/" + "ToutesLesFacturesActuelles/";
        String titre = null;
        listeFactures = factureService.sortInvoicesByClient(listeFactures);

        //SBE_AMNOTE-194 Création du pdf complet de toutes les factures de la liste
        try {
            titre = pdfBuilderFacture.createBigPdfFacture(listeFactures, fraisOuPas, dateVoulueRefonte)[1];
        } catch (Exception e) {
            logger.error("[pdf invoice] create big pdf error", e);
        }
        chemin = chemin + titre;

        byte[] contents = new byte[]{0};
        HttpHeaders headers = new HttpHeaders();
        File file = new File(chemin);

        if (file.exists() && file.isFile()) {
            Path path = file.toPath();
            contents = Files.readAllBytes(path);
        }

        headers.setContentType(MediaType.parseMediaType(FileFormatEnum.PDF.toString()));

        //Définition du nom de fichier en fonction de la date et du type d'extract
        headers.setContentDispositionFormData(titre, titre);
        headers.setCacheControl(MUST_REVALIDATE);
        responseEntity = new ResponseEntity<>(contents, headers, HttpStatus.OK);

        return responseEntity;
    }

    //TODO Un controller régissant la gestion des CSV a été créé à cet effet

    /**
     * [AMNOTE-463] Créer et exporter le fichier CSV pour CEGID au clic du bouton
     *
     * @param codeTypeFacture codeTypeFacture
     * @param lAnnee          lAnnee
     * @param leMois          leMois
     * @param request         request
     * @param model           model
     * @param session         session
     * @param rat             rat
     * @return CSV file
     * @throws IOException            IOException
     * @throws InvalidFormatException InvalidFormatException
     * @throws ParseException         ParseException
     * @throws NamingException        NamingException
     */
    @RequestMapping(value = "/factures/csv/{codeTypeFacture}/{leMois}/{lAnnee}", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<byte[]> genererCSV(@PathVariable(value = "codeTypeFacture") String codeTypeFacture,
                                             @PathVariable(value = "lAnnee") String lAnnee,
                                             @PathVariable(value = "leMois") String leMois,
                                             HttpServletRequest request, Model model, HttpSession session, RedirectAttributes rat)
            throws IOException, InvalidFormatException, ParseException, NamingException {

        init(request, model);
        logger.debug("genererCSV");
        logger.debug("type de facture= " + codeTypeFacture);

        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);
        List<Facture> listeFacturesTemp = new ArrayList<>();
        List<Facture> listeFactures = new ArrayList<>();
        List<Frais> listeFraisCollab = new ArrayList<>();
        String file = "";
        Etat etatArchivee = etatService.getEtatByCode("AR");
        try {
            switch (codeTypeFacture) {
                case TypeFacture.TYPEFACTURE_FACTURABLE_CSV:
                    listeFacturesTemp = factureService.findFacturablesByMonth(dateVoulueRefonte);
                    for (Facture fact : listeFacturesTemp) {
                        if (!fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
                            listeFactures.add(fact);
                    }
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_FRAIS_CSV:
                    listeFactures = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
                    break;

                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_CSV:
                    listeFacturesTemp = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    for (Facture fact : listeFacturesTemp) {
                        if (!fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
                            listeFactures.add(fact);
                    }
                    break;

                case TypeFacture.TYPEFACTURE_FACTURABLE_SST_CSV:
                    listeFacturesTemp = factureService.findFacturablesByMonth(dateVoulueRefonte);
                    for (Facture fact : listeFacturesTemp) {
                        if (fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
                            listeFactures.add(fact);
                    }
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_SST_CSV:
                    listeFacturesTemp = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    for (Facture fact : listeFacturesTemp) {
                        if (fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
                            listeFactures.add(fact);
                    }
                    break;
                default:
                    listeFactures = factureService.findFacturesRANonSoumisByMonth(dateVoulueRefonte);
                    break;
            }

            file = getFile(codeTypeFacture, dateVoulueRefonte, etatArchivee, listeFactures);

            logger.debug("file : " + file);
            responseEntity = downloadFile(file, "csv");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return responseEntity;
    }

    /**
     * [AMNOTE-308] Refonte - Créer et exporter les excels de factures en même temps au clic du bouton
     *
     * @param leMois             le mois voulu
     * @param lAnnee             l'année voulue
     * @param typeDeTravailleurs type de trvailleurs
     * @param request            HttpServletRequest
     * @param model              Model
     * @param session            HttpSession
     * @param rat                RedirectAttributes
     * @param codeTypeFacture    codeTypeFacture
     * @return le fichier généré (csv)
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    @RequestMapping(value = "/factures/genererExcel/{typeDeTravailleurs}/{codeTypeFacture}/{leMois}/{lAnnee}", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<byte[]> genererExcel(
            @PathVariable(value = "codeTypeFacture") String codeTypeFacture,
            @PathVariable(value = "lAnnee") String lAnnee,
            @PathVariable(value = "leMois") String leMois,
            @PathVariable(value = "typeDeTravailleurs") String typeDeTravailleurs,
            HttpServletRequest request, Model model, HttpSession session, RedirectAttributes rat)
            throws IOException, InvalidFormatException, ParseException, NamingException {
        init(model);
        logger.debug("genererExcel:" + codeTypeFacture);
        logger.debug("type de facture= " + codeTypeFacture);
        DateTime dateVoulueRefonte = extractPickedDate(leMois, lAnnee);
        Etat etatArchivee = etatService.getEtatByCode("AR");

        String file;
        String codeType = "";
        List<Facture> listeFacturesTemp = new ArrayList<>();
        List<Facture> listeFactures = new ArrayList<>();

        if (typeDeTravailleurs.equals(COLLABORATORS_FR_LOWER_CASE)) {
            codeType = StatutCollaborateur.STATUT_COLLABORATEUR;
        } else {
            if (typeDeTravailleurs.equals(SUBCONTRACTORS_FR_SNAKE_CASE))
                codeType = StatutCollaborateur.STATUT_SOUS_TRAITANT;
        }

        try {
            switch (codeTypeFacture) {
                case TypeFacture.TYPEFACTURE_FACTURABLE_SST:
                case TypeFacture.TYPEFACTURE_FACTURABLE:
                    listeFacturesTemp = factureService.findFacturablesByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_FRAIS:
                    listeFacturesTemp = factureService.findFacturesFraisByMonth(dateVoulueRefonte);
                    break;
                case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN:
                    listeFacturesTemp = factureService.findFacturesALaMainByMonth(dateVoulueRefonte);
                    break;
                default:
                    listeFacturesTemp = factureService.findFacturesRANonSoumisByMonth(dateVoulueRefonte);
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (!codeType.equals("") && !codeTypeFacture.equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
            for (Facture fact : listeFacturesTemp) {
                if (codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && fact.getMission().getCollaborateur().getStatut().getCode().equals(codeType)) {
                    listeFactures.add(fact);
                } else {
                    if (!codeType.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT) && !fact.getMission().getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                        listeFactures.add(fact);
                    }
                }
            }
        } else {
            listeFactures = listeFacturesTemp;
        }
        file = getFile(codeTypeFacture, dateVoulueRefonte, etatArchivee, listeFactures);

        responseEntity = downloadFile(file, "xls");
        return responseEntity;
    }

    /**
     * [AMNOTE-463]
     * methode de creation d'un fichier recap excel/fichier plat CSV pour logiciel CEGID
     *
     * @param codeTypeFacture
     * @param dateVoulueRefonte
     * @param etatArchivee
     * @param listeFactures
     * @return a String representing the file itself
     * @throws InvalidFormatException
     * @throws IOException
     * @throws ParseException
     * @throws NamingException
     */
    private String getFile(String codeTypeFacture, DateTime dateVoulueRefonte, Etat etatArchivee, List<Facture> listeFactures)
            throws InvalidFormatException, IOException, ParseException, NamingException {

        List<Facture> listeFacturesActuelle = new ArrayList<>();
        String file = "";
        for (Facture facFrais : listeFactures) {
            if (facFrais.getEtat().getId() != etatArchivee.getId()) {
                listeFacturesActuelle.add(facFrais);
            }
        }
        logger.debug("type de facture: " + codeTypeFacture);
        switch (codeTypeFacture) {

            case TypeFacture.TYPEFACTURE_FACTURABLE_CSV:

            case TypeFacture.TYPEFACTURE_FACTURABLE_SST_CSV:

            case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_CSV:

            case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_SST_CSV:

            case TypeFacture.TYPEFACTURE_FACTURE_FRAIS_CSV:

                file = documentCSVService.createCSVFileFacture(listeFacturesActuelle, dateVoulueRefonte, codeTypeFacture, null);
                logger.debug("creation du fichier= " + file);
                break;

            case TypeFacture.TYPEFACTURE_FACTURABLE:
            case TypeFacture.TYPEFACTURE_FACTURABLE_SST:
            case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN:
                file = documentExcelService.createExcelRecapFacturesMois(listeFacturesActuelle, dateVoulueRefonte,
                        null, null, codeTypeFacture);
                break;

            case TypeFacture.TYPEFACTURE_FACTURE_FRAIS:
                file = documentExcelService.createExcelRecapFacturesFraisMois(listeFacturesActuelle, dateVoulueRefonte,
                        null, null);
                break;

            default:
                file = documentExcelService.createExcelRecapRANonSoumis(listeFacturesActuelle, dateVoulueRefonte,
                        null, null);
                break;
        }
        logger.debug("file= " + file);
        return file;
    }

    /**
     * [AMNOTE-308] Refonte : download facture/csv
     *
     * @param file        the file
     * @param typeFichier typeFichier
     * @return Le fichier excel qui doit se télécharger
     * @throws NamingException        the naming exception
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     */
    public ResponseEntity<byte[]> downloadFile(String file, String typeFichier) throws IOException,
            InvalidFormatException, ParseException, NamingException {

        byte[] contents = new byte[]{0};
        HttpHeaders headers = new HttpHeaders();

        File fFile = new File(file);

        if (fFile.exists()) {
            Path path = fFile.toPath();
            contents = Files.readAllBytes(path);

            headers.setContentType(MediaType.parseMediaType("application/" + typeFichier));

            headers.setContentDispositionFormData(fFile.getName(), fFile.getName());
            headers.setCacheControl(MUST_REVALIDATE);
        }
        return new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/creerFactureFromMission", method = {RequestMethod.POST, RequestMethod.GET})
    public String creerFactureTestFromMission(HttpServletRequest request, Model model, HttpSession session, RedirectAttributes rat) throws NamingException, ParseException, InvalidFormatException, IOException {
        init(request, model);

        DateTime dateVoulue = new DateTime();
        List<Mission> missions = missionService.findMissionsClientesByMonth(dateVoulue);

        for (Mission mission : missions) {
            Facture facture = factureService.createFactureFromMission(mission, null);
            if (facture != null) {
                factureService.saveOrUpdate(facture);
            }
        }

        return NAV_ADMIN + NAV_ADMIN_FACTURES;
    }

    @RequestMapping(value = "/downloadFactureRA", method = {RequestMethod.POST, RequestMethod.GET})
    public ResponseEntity<byte[]> downloadCollaborateurMissionCliente(HttpServletRequest request, Model model, HttpSession session, RedirectAttributes rat) throws Exception {
        init(request, model);
        byte[] contents = new byte[]{0};
        // reccup date
        Calendar calendar = Calendar.getInstance();
        String pDate = request.getParameter(MONTH_YEAR_EXTRACT);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, Integer.parseInt(pDate.split(SLASH)[0]) - 1);
        calendar.set(Calendar.YEAR, Integer.parseInt(pDate.split(SLASH)[1]));
        DateTime dateTime = new DateTime(calendar.getTime());

        // création du chemin
        Date today = new Date();
        String nomFichier;
        SimpleDateFormat full = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm);

        // Nom du fichier?
        nomFichier = "RA_Facturation_" + full.format(today) + Constantes.EXTENSION_FILE_PDF;


        // reccupération de la liste des RA
        List<RapportActivites> rapportActivites = rapportActivitesService.getRACollaboratorMissionClienteForMonth(dateTime);
        if (!rapportActivites.isEmpty()) {
            String pathFile = pdfBuilderRA.createSeveralPdfRA(rapportActivites, nomFichier, "FA");
            File file = new File(pathFile);
            if (file.exists() && file.isFile()) {
                Path path = file.toPath();
                contents = Files.readAllBytes(path);

                fileService.deleteFile(file);
            }
            rat.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Fichier généré");
        } else {
            rat.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément concerné");
        }

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.parseMediaType(FileFormatEnum.PDF.toString()));
        headers.setCacheControl("must-revalidate");
        headers.setContentDispositionFormData(nomFichier, nomFichier);

        ResponseEntity<byte[]> reponse = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return reponse;
    }

    /**
     * Method for retrieving a pdf of of all invoices whose date is in a given month, and whose state is part of a given set of states
     *
     * @param yearOfSearch       the year of the month of search
     * @param monthOfSearch      the month of search
     * @param codesEtatsFactures set of invoices states (etats de factures), validated, etc.
     * @param request            HttpServletRequest Extends the ServletRequest interface to provide request information for HTTP servlets.
     * @param response           HttpServletResponse Extends the ServletResponse interface to provide HTTP-specific functionality in sending a response. For example, it has methods to access HTTP headers and cookies.
     * @param model              interface that defines a holder for model attributes. Primarily designed for adding attributes to the model. Allows for accessing the overall model as a java.util.Map.
     * @param session            HttpSession Provides a way to identify a user across more than one page request or visit to a Web site and to store information about that user.
     * @return a http response with a pdf file (included in the response body a byte array) of all invoices (factures) whose dates is in a given month, and whose state is part of a given set of states
     */
    @RequestMapping(value = "/factures/saveAllFacturesPDFByMonthAndState/{monthOfSearch}/{yearOfSearch}/{codesEtatsFactures}", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<byte[]> saveAllFacturesPDFByMonthAndState(
            @PathVariable(value = "yearOfSearch") String yearOfSearch,
            @PathVariable(value = "monthOfSearch") String monthOfSearch,
            @PathVariable(value = "codesEtatsFactures") String[] codesEtatsFactures,
            HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) {

        init(request, model);
        DateTime dateOfSearch = extractPickedDate(monthOfSearch, yearOfSearch);

        // Récuperer toutes les factures répondant aux critères
        List<Facture> facturesResult = factureService.findFactureByMonthAndState(dateOfSearch, Facture.PROP_NUMFACTURE, codesEtatsFactures);

        try {
            if (!facturesResult.isEmpty()) {

                // création du pdf à travers le service PDFBuilder, puis récupération du chemin du dossier contenant le fichier ainsi que le nom du fichier
                String[] pdfFilePathAndName = pdfBuilderFacture.createBigPdfFacture(facturesResult, true, dateOfSearch);

                // Création et définition des en têtes http de la réponse
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.parseMediaType(FileFormatEnum.PDF.toString()));
                headers.setContentDispositionFormData(pdfFilePathAndName[1], pdfFilePathAndName[1]);
                headers.setCacheControl(MUST_REVALIDATE);

                // On va lire le fichier pdf pour récuperer un tableau d'octets (bytes[]) puis l'inclure dans le corps de la réponse (responseEntity)
                responseEntity = new ResponseEntity(fileService.readFileByPath(pdfFilePathAndName[0] + pdfFilePathAndName[1]), headers, HttpStatus.OK);
            } else {
                // Ici si la liste de factures est vide, c'est à dire aucune facture correspondant aux critères on retourne une reponse http 204, No Content
                responseEntity = new ResponseEntity(HttpStatus.NO_CONTENT);
            }

        } catch (Exception e) {
            // En cas d'erreur serveur on renvoie une réponse code 500, et on inclu le message de l'exception dans le corps de la requête
            responseEntity = new ResponseEntity(e.getMessage().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
            //TODO Créer une classe MonException héritant de runtimeException que l'on va throw et qui va gérer l'envoi d'une réponse http avec un code http correspondant à la situation, ?utiliser @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        }
        return responseEntity;
    }
}