/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.metier.domain.services.RapportActivitesService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.web.utils.AmiltoneUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Authentication controller
 */
@Controller
public class LoginController extends AbstractController {

    private static final String ERROR = "error";
    private static final String ERROR_AUTHENTICATION = "L'authentification a échoué.";
    private static final String ERROR_LOCKED_ACCOUNT = "Compte bloqué (nombre de tentatives erronnées trop importantes).<br/> Veuillez patienter 3 min.";
    private static final String ERROR_ACCESS_DENIED = "Accès Refusé.<br/> Veuillez contacter votre administrateur à l'adresse suivante: support_appli@amiltone.com";

    /**
     * The Collab.
     */
    private Collaborator collab = new Collaborator();

    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private AmiltoneUtils utils;

    /**
     * Redirection toward the login page or the home page if the user already possesses a running session
     * /pagenotfound URL allows to handle the ServletNotFound Exeption or page not found exceptions
     * For more information, consider checking: {@link ErrorController#exception(NoHandlerFoundException, WebRequest)}
     *
     * @param request  the request
     * @param model    the model
     * @param response the response
     * @return String string
     */
    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String login(HttpServletRequest request, Model model, HttpServletResponse response) {

        init(request, model);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            // If the user already possesses a valid session
            return "welcome";
        } else {
            response.setStatus(901);
            return "login";
        }
    }

    /**
     * Redirection after a successful login or a direct /welcome url
     *
     * @param request the request
     * @param model   the model
     * @return String string
     */
    @RequestMapping(value = "/welcome", method = {RequestMethod.GET, RequestMethod.POST})
    public String welcome(HttpServletRequest request, Model model) {
        init(model);

        collab = utils.getCurrentCollaborator();

        reinitAttemptsCollab(collab);

        String dateRASoumis = "-";

        // First and last name of the current logged user
        model.addAttribute("prenomNomCollab", collab.getPrenom() + " " + collab.getNom());

        if (null != rapportActivitesService.getDateDernierRASoumis(collab)) {
            dateRASoumis = utils.sdf.format((rapportActivitesService.getDateDernierRASoumis(collab)));
        }
        model.addAttribute("dateDernierRA", dateRASoumis);

        String dateAbsSoumis = "-";
        if (null != absenceService.getDateDernierAbsSoumis(collab)) {
            dateAbsSoumis = utils.sdf.format((absenceService.getDateDernierAbsSoumis(collab)));
        }
        model.addAttribute("dateDerniereAbs", dateAbsSoumis);

        model.addAttribute("nbMissionAValider", missionService.getNbMissionAttenteVal(collab));

        model.addAttribute("nbAbsAttente", absenceService.getNbAbsenceAttenteVal(collab));

        model.addAttribute("nbAbsBrouillon", absenceService.getNbAbsenceBrouillonVal(collab));

        boolean adminOrDRH = request.isUserInRole(StatutCollaborateur.STATUT_DRH) || request.isUserInRole(StatutCollaborateur.STATUT_ADMIN);
        if (adminOrDRH) {
            model.addAttribute("nbAbsAValider", absenceService.getNbAbsenceAttenteVal());
            model.addAttribute("nbDOMAValider", missionService.getNbMissionAttenteVal());
        }

        return "welcome";
    }

    private void reinitAttemptsCollab(Collaborator currentCollaborateur) {
        currentCollaborateur.setNbAttempts(0);
        collaboratorService.saveOrUpdate(currentCollaborateur);
    }

    /**
     * Redirection in case of a failed attempt during the logging process
     *
     * @param request the request
     * @param model   the model
     * @return String string
     */
    @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
    public String loginfailed(HttpServletRequest request, Model model) {
        init(request, model);
        // A login ending with .fr and a login ending with .com
        Cookie cookie = recupCookieMail(request.getCookies());

        if (cookie != null) {
            String testMail = testTypeMail(cookie);
            if ("fr".equals(testMail) || "com".equals(testMail)) {
                testLockedAccount(model, collab);
            } else {
                model.addAttribute(ERROR, ERROR_AUTHENTICATION);
            }
        } else {
            model.addAttribute(ERROR, ERROR_AUTHENTICATION);
        }
        return "login";
    }

    /**
     * Retrieves the email address inside the cookies
     *
     * @param cookies the cookie list
     * @return the corresponding cookie
     */
    private Cookie recupCookieMail(Cookie[] cookies) {

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("loginSaisi")) {
                return cookie;
            }
        }
        return null;
    }

    /**
     * Tests whether the collaborator has an email ending with ".fr" or ".com"
     *
     * @param cookie the cookies containing the email address
     * @return the email's ending
     */
    private String testTypeMail(Cookie cookie) {
        collab = collaboratorService.findByMail(cookie.getValue() + Constantes.AMILTONE_EMAIL_DOMAIN_FR);
        if (collab != null) {
            return "fr";
        }

        collab = collaboratorService.findByMail(cookie.getValue() + Constantes.AMILTONE_EMAIL_DOMAIN_COM);
        if (collab != null) {
            return "com";
        }

        return "";
    }

    /**
     * Tests if the collaborator passed as parameter has a locked account or not
     *
     * @param collab the collaborator
     */
    private void testLockedAccount(Model model, Collaborator collab) {
        int nbAttempts = collab.getNbAttempts();

        if (collab.getAccountNonLocked() && collab.isEnabled()) {
            if (collab.getNbAttempts() >= 4) {
                // Locks the account after 4 unsuccessful attempts
                collab.setNbAttempts(5);
                collab.setAccountNonLocked(false);
                collaboratorService.saveOrUpdate(collab);
                model.addAttribute(ERROR, ERROR_LOCKED_ACCOUNT);
            } else {
                nbAttempts++;
                collab.setNbAttempts(nbAttempts);
                collaboratorService.saveOrUpdate(collab);
                model.addAttribute(ERROR, ERROR_AUTHENTICATION);
            }
        } else if (!collab.isEnabled()) {
            model.addAttribute(ERROR, ERROR_ACCESS_DENIED);
        } else {
            model.addAttribute(ERROR, ERROR_LOCKED_ACCOUNT);
        }
    }

    /**
     * Disconnects the user and redirects him toward the login page
     *
     * @param request the request
     * @param model   the model
     * @return a string
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, Model model) {
        init(request, model);

        return "logout";
    }

    /**
     * Redirection in case of a denied access to a specific page
     *
     * @param request the request
     * @param model   the model
     * @param pRedir  the p redir
     * @return string
     */
    @RequestMapping(value = "/accessdenied", method = {RequestMethod.GET, RequestMethod.POST})
    public String accessDenied(HttpServletRequest request, Model model, RedirectAttributes pRedir) {
        init(request, model);
        pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get("error.accesNonAutorise"));
        return Constantes.REDIRECT + NAV_WELCOME;
    }

    /**
     * Shows error string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return the string
     */
    @RequestMapping(value = "/showError")
    public String showError(HttpServletRequest request, Model model, HttpSession session) {
        init(request, model);

        return ERROR;
    }
}
