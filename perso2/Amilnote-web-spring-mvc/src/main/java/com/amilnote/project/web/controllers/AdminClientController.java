/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.amilnote.project.metier.domain.entities.json.ContactClientAsJson;
import com.amilnote.project.metier.domain.services.ClientService;
import com.amilnote.project.metier.domain.services.ContactClientService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller de la gestion des Clients
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminClientController extends AbstractController {

    private static Logger logger = LogManager.getLogger(AdminClientController.class);

    private static final String PROP_CLIENTTOEDIT = "currentEditClient";

    private static final String PROP_CONTACTCLIENTTOEDIT = "currentEditContactClient";

    @Autowired
    private ClientService clientService;

    @Autowired
    private ContactClientService contactClientService;

    //		Gestion des Clients		//

    /**
     * Récupère la liste de tous les clients pour l'afficher
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "clients", method = {RequestMethod.GET, RequestMethod.POST})
    public String clients(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        session.removeAttribute(PROP_CLIENTTOEDIT);
        session.setAttribute(NAME_NAV_ADMIN_COLLABORATEURS, NAV_ADMIN_COLLABORATEURS);

        List<ClientAsJson> listClients = clientService.findAllOrderByNomAscAsJson();
        model.addAttribute("listClients", listClients);

        return NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /**
     * Edition d'un client existant
     *
     * @param id       the id
     * @param session  the session
     * @param request  the request
     * @param model    the model
     * @param response the response
     * @param ra       the ra
     * @return string
     * @throws Exception the exception
     */
    @RequestMapping(value = "editClient/{idClient}", method = {RequestMethod.GET})
    public String editClient(@PathVariable("idClient") Long id, HttpSession session, HttpServletRequest request, Model model,
                             HttpServletResponse response, RedirectAttributes ra) throws Exception {
        init(request, model);

        if (id != null) {

            Client tmpClient = clientService.get(id);
            if (tmpClient != null) {
                ClientAsJson clientAsJson = tmpClient.toJson();

                session.setAttribute(PROP_CLIENTTOEDIT, clientAsJson);
                session.removeAttribute(PROP_CONTACTCLIENTTOEDIT);

                List<ContactClientAsJson> listContacts = contactClientService.findAllContactsForClient(tmpClient);
                model.addAttribute("listContacts", listContacts);
                model.addAttribute("listContactsAsJson", new ObjectMapper().writeValueAsString(listContacts));

                if (clientAsJson.getAdresse().length() < 2) {
                    if (clientAsJson.getAdresse_facturation().length() > 1) {
                        clientAsJson.setAdresse(clientAsJson.getAdresse_facturation());
                    } else {
                        clientAsJson.setAdresse("Adresse siege, 00000 Ville");
                    }
                }
                model.addAttribute("adresseClientSiege", clientAsJson.getAdresse());
                if (clientAsJson.getAdresse_facturation() == null) {
                    if (clientAsJson.getAdresse().length() > 1) {
                        clientAsJson.setAdresse_facturation(clientAsJson.getAdresse());
                    } else {
                        clientAsJson.setAdresse_facturation("Adresse facturation, 00000 Ville");
                    }
                }

                List<String> listeRIB = new ArrayList<>();
                listeRIB.add("");
                listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
                listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
                listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");

                String selectedRIB = "";
                if (clientAsJson.getRibClient() != null) {
                    selectedRIB = clientAsJson.getRibClient();
                }

                model.addAttribute("client", clientAsJson);
                model.addAttribute("adresseClientFacturation", clientAsJson.getAdresse_facturation());
                model.addAttribute("auxiliaire", clientAsJson.getAuxiliaire());
                model.addAttribute("libelle_comptable", clientAsJson.getLibelleComptable());
                model.addAttribute("listeRIB", listeRIB);
                model.addAttribute("selectedRIB", selectedRIB);

                return NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT;
            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /***
     * Méthode permettant l'ajout d'un nouveeau client
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Redirection vers une autre page
     * @throws Exception the exception
     */
    @RequestMapping(value = {"editClient", "editClient/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newClient(HttpServletRequest request, Model model, HttpSession session) throws Exception {
        init(request, model);

        session.removeAttribute(PROP_CONTACTCLIENTTOEDIT);

        List<String> listeRIB = new ArrayList<>();
        listeRIB.add("");
        listeRIB.add("BNP PARIBAS - IBAN : FR76 3000 4017 5300 0100 4892 401 - BIC : BNPAFRPPXXX");
        listeRIB.add("CRÉDIT AGRICOLE - IBAN : FR76 1780 6004 8962 2521 2558 812 - BIC : AGRIFRPP878");
        listeRIB.add("CIC - IBAN : FR76 1009 6185 0300 0838 3240 168 - BIC : CMCIFRPP");

        model.addAttribute("client", new ClientAsJson());
        model.addAttribute("listeRIB", listeRIB);

        return NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT;
    }

    /**
     * Sauvegarder un client
     *
     * @param request      the request
     * @param model        ERROR@param client                Client à sauvegarder
     * @param clientAsJson the client as json
     * @param ra           the ra
     * @return Redirection vers la page du listing des clients
     * @throws Exception the exception
     */
    @RequestMapping(value = "saveClient", method = {RequestMethod.POST})
    public String saveClient(HttpServletRequest request, Model model, ClientAsJson clientAsJson, RedirectAttributes ra)
            throws Exception {
        init(request, model);

        String adresse = request.getParameter("adresseClient");
        String adresseFacturation = request.getParameter("adresseFacturation");
        String ribClient = request.getParameter("ribClient");
        //AMNOTE-462 ajout des attributs auxiliaires et libelle_comptable
        String auxiliaire = request.getParameter("auxiliaire");
        String libelleComptable = request.getParameter("libelle_comptable");

        if (!adresse.isEmpty()) {
            clientAsJson.setAdresse(adresse);
        } else {
            if (!adresseFacturation.isEmpty()) {
                clientAsJson.setAdresse(adresseFacturation);
            } else {
                clientAsJson.setAdresse("Adresse siege, 0000 Ville");
            }
        }


        if (!adresseFacturation.isEmpty()) {
            clientAsJson.setAdresse_facturation(adresseFacturation);
        } else {
            if (!clientAsJson.getAdresse().isEmpty()) {
                clientAsJson.setAdresse_facturation(clientAsJson.getAdresse());
            } else {
                clientAsJson.setAdresse_facturation("Adresse facturation, 00000 Ville");
            }
        }

        if (ribClient != null) {
            clientAsJson.setRibClient(ribClient);
        } else {
            clientAsJson.setRibClient("");
        }

        // enregistrement de l'auxiliaire
        if (!auxiliaire.isEmpty()) {
            clientAsJson.setAuxiliaire(auxiliaire);
        } else {
            clientAsJson.setAuxiliaire("");
        }

        //enregistrement du libelle comptable
        if (!libelleComptable.isEmpty()) {
            clientAsJson.setLibelleComptable(libelleComptable);
        } else {
            clientAsJson.setLibelleComptable("");
        }

        // --- Test si des données sont bien récupérées
        if (null != clientAsJson) {
            if (clientAsJson.getId() == null) { // On regarde si le nom existe que dans le cas d'une création!
                if (clientExist(request, clientAsJson.getNom_societe())) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Un client portant le même nom existe déjà.");
                    return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + clientAsJson.getId();
                }
            }
            String res = clientService.updateOrCreateClient(clientAsJson);
            //Dans le cas d'une création bien passé, on créé la mission spécial ActionCommercial
            if (res.contains("succès")) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, res);
            } else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, res);
            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /**
     * Client exist boolean.
     *
     * @param request   the request
     * @param nomClient the nom client
     * @return the boolean
     */
    @RequestMapping(value = "/clientExist/{nomClient}", method = RequestMethod.POST)
    public
    @ResponseBody
    Boolean clientExist(HttpServletRequest request, @PathVariable("nomClient") String nomClient) {

        Client client = clientService.findByNom(nomClient);

        //Si il existe un client dans la BDD avec le même nom alors on renvoi true
        //Sinon on renvoi false
        if (client != null) {
            return true;
        } else {
            return false;
        }
    }

    /***
     * Supprimer un Client existant, on supprime aussi les contacts
     *
     * @param id      ID du Client à supprimer
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page NAV_ADMIN + NAV_ADMIN_CLIENTS
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "deleteClient/{idClient}", method = {RequestMethod.GET, RequestMethod.POST})
    public String deleteClient(@PathVariable("idClient") Long id, HttpServletRequest request, Model model, RedirectAttributes ra)
            throws JsonProcessingException {
        init(request, model);

        if (id != null) {

            Client tmpClient = clientService.get(id);
            if (tmpClient != null) {
                String retourDelete = "";

                retourDelete = clientService.deleteClient(tmpClient);
                if (retourDelete.contains("succès")) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retourDelete);
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, retourDelete);
                }
            } else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Client introuvable");
            }
        }

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /***
     * Méthode permettant l'ajout ou l'édition d'un contact client en fonction de la
     * présence d'un id
     *
     * @param session         Permet d'accèder aux attributs de session
     * @param idContactClient Id du contact à modifier
     * @param idClient        Id correspondant au client en cours de modification
     * @param request         the request
     * @param model           the model
     * @param ra              the ra
     * @return Redirection vers une autre page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"editClient/{idClient}/editContactClient/{idContactClient}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String editContactClient(HttpSession session, @PathVariable("idContactClient") Long idContactClient, @PathVariable("idClient") Long idClient,
                                    HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException {

        init(request, model);
        ClientAsJson clientAsJson = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);
        // Pour editer un contact, il faut qu'un client ait été
        // sélectionné est sauvegardé en session
        if (null != clientAsJson) {

            if (null != idContactClient) {
                ContactClient contact = contactClientService.findById(idContactClient);

                if (null != contact && contact.getClient().getId().equals(clientAsJson.getId())) {

                    String lServletPath = request.getServletPath().substring(1);
                    request.setAttribute("origRequestURL", lServletPath);

                    ContactClientAsJson tmpContactClientAsJson = contact.toJson();
                    if (contact.getTelephone() != null)
                        tmpContactClientAsJson.setTelephone(contact.getTelephone());

                    // Sauvegarde en session du contact
                    session.setAttribute(PROP_CONTACTCLIENTTOEDIT, tmpContactClientAsJson);

                    model.addAttribute("contactClient", tmpContactClientAsJson);
                    model.addAttribute("client", clientAsJson);

                    return NAV_ADMIN + NAV_ADMIN_EDIT_CONTACTCLIENT;
                } else {
                    return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
                }
            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /***
     * Méthode permettant l'ajout d'un nouveau contact client
     *
     * @param request the request
     * @param model   the model
     * @param session Permet d'accèder aux attributs de session
     * @param ra      Permet de cacher les paramètres de l'URL de redirection
     * @return Redirection vers une autre page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"editClient/{idClient}/editContactClient"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newContactClient(HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra) throws JsonProcessingException {

        init(request, model);

        String lServletPath = request.getServletPath().substring(1);
        request.setAttribute("origRequestURL", lServletPath);

        ContactClientAsJson contactClient = new ContactClientAsJson();

        ClientAsJson client = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);
        if (null != client) {
            model.addAttribute("contactClient", contactClient);
            model.addAttribute("client", client);

            return NAV_ADMIN + NAV_ADMIN_EDIT_CONTACTCLIENT;
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
    }

    /***
     * Méthode permettant de sauvegarder un nouveau contact ou mettre à jour
     * un contact existant.
     *
     * @param session Permet d'accèder aux attributs de session
     * @param request the request
     * @param model   ERROR@param contactClient                contactClient à sauvegarder
     * @param contact the contact
     * @param ra      Permet de cacher les paramètres de l'URL de redirection
     * @return Redirection vers une autre page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"saveContactClient", "saveContactClient/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String saveContactClient(HttpSession session, HttpServletRequest request, Model model, ContactClientAsJson contact, RedirectAttributes ra) {
        init(request, model);

        ResponseEntity<ContactClientAsJson> responseEntity;

        ClientAsJson client = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);
        if (null != client) {

            if (contact.getId() == null) {
                if (contactExist(request, contact.getMail())) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Un contact avec ce mail est déjà enregistré.");
                    return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + client.getId() + NAV_ADMIN_EDIT_CONTACTCLIENT + (contact.getId() != null ? "/" + contact.getId() : "");
                }
            }

            try {
                responseEntity = contactClientService.createOrUpdtateContact(contact, client);

                if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "1");
                }
            } catch (Exception e) {
                logger.error("[admin contact] save contact", e);
            }
            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + client.getId();
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la création du contact");
            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
        }
    }

    @RequestMapping(value = {"addDefaultContactClient", "addDefaultContactClient/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String addDefaultContactClient(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) {

        ResponseEntity<ContactClientAsJson> responseEntity;

        ClientAsJson client = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);
        Client c = new Client(client.getNom_societe(), client.getAdresse(), client.getAdresse_facturation(), client.getTva(), client.getContacts());
        if (client != null) {

            ContactClient contact = new ContactClient(
                    "Service comptabilité",
                    "",
                    "",
                    "",
                    c,
                    false,
                    true,
                    true);
            try {
                responseEntity = contactClientService.createOrUpdtateContact(contact.toJson(), client);

                if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "1");
                }
            } catch (Exception e) {
                logger.error("[create update client] client id : {}", client.getId(), e);
            }

            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + client.getId();
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la création du contact");
            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CLIENTS;
        }
    }


    /**
     * tester si un contact existe déjà dans la BDD pour le client en cours d'édition (on compare grace au mail)
     *
     * @param request     the request
     * @param mailContact the mail contact
     * @return boolean
     */
    @RequestMapping(value = "/contactExist/{mailContact}", method = RequestMethod.POST)
    public
    @ResponseBody
    Boolean contactExist(HttpServletRequest request, @PathVariable("mailContact") String mailContact) {
        HttpSession session = request.getSession();
        //On récupère le contact entrain d'être modifié
        ClientAsJson clientAsJson = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);

        Client client = clientService.findByNom(clientAsJson.getNom_societe());

        ContactClient contact = contactClientService.findByMailForClient(mailContact, client);

        //Si il existe un contact dans la BDD avec le même mail, pour le même client alors on renvoi true
        //Sinon on renvoi false
        if (contact != null && contact.getClient().getId() == client.getId()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Supprimer un contact
     *
     * @param id      ID du contact à supprimer
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page du client en cours d'édition
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "deleteContactClient/{idContactClient}", method = {RequestMethod.GET, RequestMethod.POST})
    public String deleteContactClient(@PathVariable("idContactClient") Long id, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
            throws JsonProcessingException {
        init(request, model);

        ClientAsJson clientJson = (ClientAsJson) session.getAttribute(PROP_CLIENTTOEDIT);
        if (null != id) {
            if (null != clientJson) {
                ContactClient tmpContact = contactClientService.findById(id);
                if (null != tmpContact) {
                    String retour = contactClientService.deleteContactClient(tmpContact);
                    if (retour.contains("succès")) {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retour);
                        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + clientJson.getId();
                    } else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, retour);
                        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_EDIT_CLIENT + "/" + clientJson.getId();
                    }
                }
            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Launch client request by given the client contact id
     * Change the enabled boolean field
     * And finally return the updated contactClientAsJson by http
     * @param contactClientAsJson ClientContact object in JSON format to update
     * @return ResponseEntity The HTTP server response
     */
    @RequestMapping(value="clientContact", method = RequestMethod.PUT, produces = Constantes.APPLICATION_JSON )
    public ResponseEntity<ContactClientAsJson> editClientContact(@RequestBody ContactClientAsJson contactClientAsJson) {

        return contactClientService.createOrUpdtateContact(
                contactClientAsJson,
                clientService.get(contactClientAsJson.getClient().getId()).toJson()
        );
    }
}