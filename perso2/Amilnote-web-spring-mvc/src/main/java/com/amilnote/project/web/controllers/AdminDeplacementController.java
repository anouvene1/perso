/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;
import com.amilnote.project.metier.domain.services.DemandeDeplacementService;
import com.amilnote.project.metier.domain.services.FileService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.DeplacementsForm;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Controller des demandes de déplacements
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminDeplacementController extends AbstractController {

    @Autowired
    private DemandeDeplacementService deplacementService;

    @Autowired
    private FileService fileService;

    // ---- GESTION DES DEPLACEMENTS ----//

    /**
     * Recupère toutes les demandes de deplacements et affiche la page de validation des deplacements
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Strig Représente le nom de la vue "validationDeplacements"
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "validationDeplacements", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationDeplacements(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);
        String mois = request.getParameter("mois");
        List<DemandeDeplacementAsJson> listDeplacementSO = deplacementService.getAllDemandeDeplacementEtatSO();
        List<DemandeDeplacementAsJson> listTemp = new ArrayList<DemandeDeplacementAsJson>();
        if (mois != null && !mois.isEmpty()) {
            for (DemandeDeplacementAsJson depl : listDeplacementSO) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(depl.getDateDebut());
                String moisDebut = Integer.toString(calendar.get(Calendar.MONTH));

                calendar.setTime(depl.getDateFin());
                String moisFin = Integer.toString(calendar.get(Calendar.MONTH));

                if (moisDebut.equals(mois) || moisFin.equals(mois) || "-1".equals(mois)) {
                    listTemp.add(depl);
                }
            }
            listDeplacementSO = listTemp;
        }

        model.addAttribute("listDeplacementASJSON", listDeplacementSO);
        return NAV_ADMIN + NAV_ADMIN_DEPLACEMENTS;
    }

    /**
     * Méthode permettant de valider une demande de déplacement
     *
     * @param request       the request
     * @param model         the model
     * @param session       the session
     * @param idDeplacement the id deplacement
     * @param action        the action
     * @param ra            the ra
     * @param commentaire   the commentaire
     * @return string
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationDeplacements/{idDeplacement}/{action}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationDeplacement(HttpServletRequest request, Model model, HttpSession session, @PathVariable("idDeplacement") Long idDeplacement,
                                        @PathVariable("action") String action,
                                        RedirectAttributes ra,
                                        String commentaire) throws Exception {
        init(request, model);
        if (commentaire != null) {
            commentaire = new String(commentaire.getBytes(), Charset.forName("UTF-8"));
        }
        else {
            commentaire = "";
        }
        if (null != idDeplacement) {
            String msgRetour = deplacementService.actionValidationDeplacement(idDeplacement, action, commentaire);
            if (!msgRetour.equals("")) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, msgRetour);
            } else {
                ra.addFlashAttribute("messageErreur", properties.get("error.actionInconnue", action));
            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CONSULTATION_DEPLACEMENTS + "/null";
    }

    /**
     * Validations multiples deplacements string.
     *
     * @param request          the request
     * @param model            the model
     * @param session          the session
     * @param action           the action
     * @param ra               the ra
     * @param deplacementsForm the deplacements form
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsMultiplesDeplacements/{action}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationsMultiplesDeplacements(
        HttpServletRequest request,
        Model model,
        HttpSession session,
        @PathVariable("action") String action,
        RedirectAttributes ra,
        @ModelAttribute("deplacementsForm") DeplacementsForm deplacementsForm

    ) throws Exception {

        String msgRetour = "";

        if (deplacementsForm.getListDeplacements() != null) {
            for (DemandeDeplacementAsJson tmpDeplacementAsJson : deplacementsForm.getListDeplacements()) {
                if (deplacementsForm.getCommentaire() != null) {
                    msgRetour = deplacementService.actionValidationDeplacement(tmpDeplacementAsJson.getId(), action, deplacementsForm.getCommentaire());
                }
                else {
                    msgRetour = deplacementService.actionValidationDeplacement(tmpDeplacementAsJson.getId(), action, "");
                }

                if (msgRetour.equals("")) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get("error.actionInconnue", action));
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, msgRetour);
                }
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément séléctionné");
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CONSULTATION_DEPLACEMENTS + "/null";
    }


    /**
     * Annulation deplacement string.
     *
     * @param request       the request
     * @param model         the model
     * @param session       the session
     * @param idDeplacement the id deplacement
     * @param ra            the ra
     * @param commentaire   the commentaire
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = "annulationDeplacements/{idDeplacement}", method = {RequestMethod.GET, RequestMethod.POST})
    public String annulationDeplacement(HttpServletRequest request,
                                        Model model,
                                        HttpSession session,
                                        @PathVariable("idDeplacement") Long idDeplacement,
                                        RedirectAttributes ra,
                                        String commentaire) throws Exception {
        init(request, model);

        if (null != idDeplacement) {
            String msgRetour = deplacementService.actionValidationDeplacement(idDeplacement, Etat.ETAT_ANNULE, commentaire);
            if (!msgRetour.equals("")) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, msgRetour);
            } else {
                ra.addFlashAttribute("messageErreur", properties.get("error.actionInconnue", Etat.ETAT_ANNULE));
            }
        }

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CONSULTATION_DEPLACEMENTS + "/null";
    }


    /**
     * Consultation de l'ensemble des demandes de déplacement quelques soit leurs état et leurs date avec un filtre sur le mois et sur l'état
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @param pFiltre the p filtre
     * @return Page de consultation des deplacements
     * @throws JsonProcessingException the json processing exception
     * @throws NamingException         the naming exception
     */
    @RequestMapping(value = "consultationDeplacements/{pFiltre}", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationDeplacements(HttpServletRequest request, Model model, HttpSession session, @PathVariable("pFiltre") String pFiltre) throws JsonProcessingException, NamingException {
        init(request, model);
        String mois = request.getParameter("mois");
        String annee = request.getParameter("annee");
        String pDate = request.getParameter("monthYearExtract");
        Calendar pCal = Calendar.getInstance();
        if (pDate != null && !pDate.isEmpty()) {
            Integer pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
            Integer pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
            mois = String.valueOf(pMonth);
            annee = String.valueOf(pYear);
        }

        List<DemandeDeplacementAsJson> listDD = deplacementService.getAllDemandeDeplacementAsJson();
        List<DemandeDeplacementAsJson> listTemp = new ArrayList<>();

        for (DemandeDeplacementAsJson dd : listDD) {
            if ("null".equals(pFiltre) || dd.getEtat().getCode().equals(pFiltre)) {
                if ((mois == null || mois.isEmpty() || moisIsOk(mois, dd)) &&
                    (annee == null || annee.isEmpty() || anneeIsOk(annee, dd))) {
                    associatePdfIfMonthOk(dd);
                    listTemp.add(dd);
                }
            }
        }

        listDD = listTemp;

        model.addAttribute("listDD", listDD);
        model.addAttribute("repertoireDD", Parametrage.getContext("dossierPDFDEPLACEMENT"));

        return NAV_ADMIN + NAV_ADMIN_CONSULTATION_DEPLACEMENTS;
    }

    private boolean moisIsOk(String mois, DemandeDeplacementAsJson dd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dd.getDateDebut());
        String moisDebut = Integer.toString(calendar.get(Calendar.MONTH));

        calendar.setTime(dd.getDateFin());
        String moisFin = Integer.toString(calendar.get(Calendar.MONTH));

        return moisDebut.equals(mois) || moisFin.equals(mois) || "-1".equals(mois);
    }

    private boolean anneeIsOk(String annee, DemandeDeplacementAsJson dd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dd.getDateDebut());
        String anneeDebut = Integer.toString(calendar.get(Calendar.YEAR));

        calendar.setTime(dd.getDateFin());
        String anneeFin = Integer.toString(calendar.get(Calendar.YEAR));

        return anneeDebut.equals(annee) || anneeFin.equals(annee) || "-1".equals(annee);
    }

    private void associatePdfIfMonthOk(DemandeDeplacementAsJson dd) {
        File lFileDeplacement = deplacementService.getFileDeplacementbyId(dd.getId());
        if (lFileDeplacement != null) {
            if (fileService.fileWasZipped(lFileDeplacement))
            dd.setPdfDeplacement(lFileDeplacement.getPath());
        } else {
            dd.setPdfDeplacement("null");
        }
    }

    /**
     * Retourne le fichier PDF correspondant au déplacement souhaité
     *
     * @param idDeplacement ID du RA à visualiser
     * @param request       the request
     * @param response      the response
     * @param model         the model
     * @param session       the session
     * @param ra            the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationDeplacements/{idDeplacement}/download", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void visualisationDeplacement(@PathVariable("idDeplacement") Long idDeplacement, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                                  RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idDeplacement) {

            //--- Récupération du fichier PDF lié au RA
            File lFileDD = deplacementService.getFileDeplacementbyId(idDeplacement);
            if (lFileDD != null) {
                if (!lFileDD.exists() && fileService.fileWasZipped(lFileDD)){
                    lFileDD = fileService.unZipFile(new File(lFileDD.getPath().substring(0, (lFileDD.getPath()).lastIndexOf(".")) + Constantes.EXTENSION_FILE_ZIP));
                }
                String nomFichier = lFileDD.getPath();
                String newNomFichier = nomFichier.replace("_SOCIAL", "");
                lFileDD = new File(newNomFichier);

                //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
                httpResponseConstruct("pdf", response, lFileDD);

                if (fileService.fileWasZipped(lFileDD)) fileService.deleteFile(lFileDD);
            }
        }
    }


    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

}
