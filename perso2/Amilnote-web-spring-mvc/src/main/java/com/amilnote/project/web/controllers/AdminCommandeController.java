/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.entities.json.ContactClientAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.Utils;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.amilnote.project.metier.domain.utils.Constantes.DATE_FORMAT_YYYY_MM_DD;
import static com.amilnote.project.metier.domain.utils.Constantes.REDIRECT;

/**
 * Controller de la gestion des commandes
 * Created by azicaro on 13/09/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminCommandeController extends AbstractController {

    private static final String PROP_MISSIONTOEDIT = "currentEditMission";
    private static final String FILTER_ATTRIBUTE = "filtre";
    private static final String WISHED_DATE_ATTRIBUTE = "dateVoulue";
    private static final String COMMANDE_ATTRIBUTE = "commande";
    private static final String VIREMENT_ATTRIBUTE = "virement";
    private static final String BANKCHECK_ATTRIBUTE = "chèque";

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private MoyensPaiementService paiementService;

    @Autowired
    private FactureService factureService;

    @Autowired
    private DocumentExcelService documentExcelService;

    @Autowired
    private AmiltoneUtils utils;

    @Autowired
    private FileService fileService;


    // ---- GESTION DES COMMANDES ----//


    /**
     * Liste toutes les commandes
     *
     * @param filtre  the filtre
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page d'édition d'une commande
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "gestionCommandes/{filtre}", method = {RequestMethod.GET, RequestMethod.POST})
    public String gestionCommandes(@PathVariable(FILTER_ATTRIBUTE) String filtre, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException {

        init(request, model);
        model.addAttribute(FILTER_ATTRIBUTE, filtre);
        String filtreChoisi = filtre;
        // [JNA][AMNOTE 53] ajout du filtre par mois
        //Récupération du filtre sur le mois
        String pDate = request.getParameter("monthYearExtract");

        List<Commande> listeCommandes = null;

        listeCommandes = getCommandes(filtreChoisi, pDate);

        model.addAttribute(FILTER_ATTRIBUTE, filtre);
        model.addAttribute(WISHED_DATE_ATTRIBUTE, pDate);
        model.addAttribute("listeCommandes", listeCommandes);
        model.addAttribute("monthYearExtract", pDate);

        session.setAttribute(FILTER_ATTRIBUTE, filtre);
        session.setAttribute(WISHED_DATE_ATTRIBUTE, pDate);

        return NAV_ADMIN + NAV_ADMIN_GESTION_COMMANDES;
    }

    /**
     * Récupère la liste des commandes à afficher
     * @param filtre
     * @param pDate
     * @return
     */
    private List<Commande> getCommandes(String filtre, String pDate) {
        String date = pDate;
        List<Commande> listeCommandes = null;
        if (null != date) {
            listeCommandes = commandeService.findCommandesForMonth(pDate, filtre);
        } else if (filtre.equals("CO") && null == date){
            listeCommandes = commandeService.findStartedCommandes();
        } else if (filtre.equals("TE") && null == date){
            listeCommandes = commandeService.findStoppedCommandes();
        }
        return listeCommandes;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * New commande string.
     *
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return the string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"newCommande"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newCommande(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException {

        init(request, model);

        List<CollaboratorAsJson> listCollab = collaboratorService.findAllOrderByNomAscAsJson();
        model.addAttribute("listCollab", listCollab);
        List<CommandeAsJson> listCommandes = commandeService.findAllOrderByNomAscAsJson();
        model.addAttribute("listCommandes", listCommandes);

        //SHA AMNOTE-206 Liste de clients pour la facture
        model.addAttribute("listeClients", clientService.findAll(null));

        return NAV_ADMIN + NAV_ADMIN_NEWCOMMANDE;
    }

    /**
     * Supprimer une commande
     *
     * @param id      ID de la commande à supprimer
     * @param etat    String
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page de la mission en cours d'édition
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "deleteComm/{etat}/{idCommande}", method = {RequestMethod.GET, RequestMethod.POST})
    public String deleteCommande(@PathVariable("idCommande") Long id, @PathVariable("etat") String etat, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
        throws JsonProcessingException {
        init(request, model);
        if (null != id) {
            Commande tmpCommande = commandeService.findById(id);

            if (null != tmpCommande) {
                String retour = commandeService.deleteCommande(tmpCommande);
                if (retour.contains("succès")) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retour);
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, retour);
                }
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "L'id de la commande est null");
        }

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTION_COMMANDES + "/" + etat;
    }

    /**
     * Ajouter une nouvelle commande
     *
     * @param id      the id
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page d'édition d'une commande
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "editCommande/{idCommande}", method = {RequestMethod.GET, RequestMethod.POST})
    public String editCommande(@PathVariable("idCommande") Long id, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException {
        init(request, model);
        List<CollaboratorAsJson> listCollabWithDisable = collaboratorService.findAllOrderByNomAscAsJsonWithoutADMIN();
        model.addAttribute("listeCollaborateurs", listCollabWithDisable);
        List<MoyensPaiement> paiements = paiementService.findAll(null);
        List<String> conditionPaiement = new ArrayList<>();

        // création d'une commande
        if (id == -1) {
            CommandeAsJson newCommandeJson = new CommandeAsJson();
            model.addAttribute(COMMANDE_ATTRIBUTE, newCommandeJson);
            conditionPaiement.add(VIREMENT_ATTRIBUTE);
            conditionPaiement.add(BANKCHECK_ATTRIBUTE);
        } else { // modification d'une commande
            // On récupère la commande
            Commande commande = commandeService.findById(id);

            if (commande.getJustificatif() != null) {
                commande.setJustificatif(commande.getJustificatif());
            }
            CommandeAsJson commandeJson = commande.toJson();

            //[JNA][AMNOTE 160] On initialise la liste des condition en fonction de ce qui est sauvegardé dans la commande

            if (commandeJson.getConditionPaiement() == null
                    || commandeJson.getConditionPaiement().equals(VIREMENT_ATTRIBUTE)
                    || commandeJson.getConditionPaiement().equals(BANKCHECK_ATTRIBUTE)) {
                conditionPaiement.add(VIREMENT_ATTRIBUTE);
                conditionPaiement.add(BANKCHECK_ATTRIBUTE);
            }

            Collaborator collab = commande.getMission().getCollaborateur();
            // On récupère la liste des missions du collab de cette commande
            List<MissionAsJson> listeMissionsCollab = missionService.findAllMissionsCollaborator(commande.getMission().getCollaborateur());

            Mission selectedMission = commande.getMission();

            String commentaire = commande.getCommentaire();

            //recuperation du responsable facturation
            DateTime dateDebut = new DateTime(commande.getDateDebut());
            DateTime dateFin = new DateTime(commande.getDateFin());
            List<Facture> factures = factureService.findByMissionBetweenDates(commande.getMission(), dateDebut, dateFin);

            long selectedIdResponsable=0;
            if (commande.getIdResponsableFacturation() != null) {
                selectedIdResponsable = commande.getIdResponsableFacturation();
            } else if(!factures.isEmpty()) {
                selectedIdResponsable= factures.get(0).getContact();
            }

            //recuperation de la liste des responsables
            List<ContactClientAsJson> listContacts = new ArrayList<>();
            List<ContactClient> listContactsTemp = commande.getClient().getContacts();
            for(ContactClient cc: listContactsTemp) {
                if(cc.isRespFacturation()) listContacts.add(cc.toJson());
            }
            //On ajoute ensuite le service compta
            for(ContactClient cc: listContactsTemp) {
                if (!cc.isRespFacturation() && !cc.isRespClient()) listContacts.add(cc.toJson());
            }

            model.addAttribute("selectedidRespo", selectedIdResponsable);
            model.addAttribute("listeResponsablesFact", listContacts);
            model.addAttribute("selectedMission", selectedMission);
            model.addAttribute("commentaire", commentaire);
            model.addAttribute("listeMissionsCollab", listeMissionsCollab);
            model.addAttribute(COMMANDE_ATTRIBUTE, commandeJson);
            model.addAttribute("collab", collab.toJson());
        }


        model.addAttribute("conditionPaiementCommande", conditionPaiement);
        model.addAttribute("listePaiements", paiements);
        //SHA AMNOTE-206 Liste de clients pour la facture
        model.addAttribute("listeClients", clientService.findAllOrderByNomAsc());

        return NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE;

    }


    /**
     * Sauvegarde une mission
     *
     * @param fichier     the file
     * @param request      the request
     * @param model        the model
     * @param commandeJson the commande json
     * @param ra           the ra
     * @param respFact     respFact
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = {"saveCommande", "saveCommande/"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String saveCommande(@RequestParam(value = "fichier", required = false) MultipartFile fichier,  @RequestParam(value = "TheValueIdResp", required = false) String respFact,
                               HttpServletRequest request, Model model, CommandeAsJson commandeJson,
                               RedirectAttributes ra) throws Exception {
        init(request, model);

        if (commandeJson.getClient() == null || commandeJson.getClient().getId() == 0) {
            commandeJson.setClient(commandeJson.getMission().getClient());
        }

        //[JNA][AMNOTE 53] Sauvegarde d'une commande

        if (commandeJson.getMission().getId() == 0 || commandeJson.getMission().getCollaborateur().getId() == 0) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Sélectionnez un collaborateur et une mission.");
            if (request.getParameter("duplicatedCommandeId") != null) {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE + "/" + request.getParameter("duplicatedCommandeId");
            }
            else if (commandeJson.getId() == null) {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + "/" + -1;
            } else {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + (commandeJson.getId() != null ? "/" + commandeJson.getId() : "");
            }
        }

        Commande tCommande = null;
        if (commandeJson.getId() != null) {
            tCommande = commandeService.findById(commandeJson.getId());
            commandeJson.setEtat(tCommande.getEtat());
        } else {
            Etat etatCommande = etatService.getEtatByCode("CO");
            commandeJson.setEtat(etatCommande);
        }
        // On récupère la mission choisi pour la commande
        Mission mission = missionService.get(commandeJson.getMission().getId());
        // On la converti en Json
        MissionAsJson missionJson = mission.toJson();

        // On vérifie que les dates de la commande sont bien comprises dans les dates de la mission si elles sont renseignées
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);

        Date finMission = mission.getDateFin();
        if (commandeJson.getDateDebut() != null && commandeJson.getDateFin() != null) {
            if (commandeJson.getDateDebut().after(commandeJson.getDateFin())) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "La date de début doit être inférieure à la date de fin.");
                if (request.getParameter("duplicatedCommandeId") != null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE + "/" + request.getParameter("duplicatedCommandeId");
                }
                else if (commandeJson.getId() == null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + "/" + -1;
                } else {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + (commandeJson.getId() != null ? "/" + commandeJson.getId() : "");
                }
            }

            if (format.parse(format.format(commandeJson.getDateFin())).after(finMission)) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Problème de concordence entre les dates de la commande et celles de la mission.");
                if (request.getParameter("duplicatedCommandeId") != null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE + "/" + request.getParameter("duplicatedCommandeId");
                }
                else if (commandeJson.getId() == null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + "/" + -1;
                } else {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + (commandeJson.getId() != null ? "/" + commandeJson.getId() : "");
                }
            }
        }

        //[JNA][AMNOTE 142] Ajout d'un justificatif à la commande
        String filename = null;
        // ajouter la pièce justificative.
        if (fichier != null && !fichier.isEmpty()) {
            // par défaut la description du justificatif est le nom de la pièce justificative.
            // --- Récupération de l'extension du fichier
            String extensionFichier = fichier.getOriginalFilename();
            extensionFichier = extensionFichier.substring(extensionFichier.lastIndexOf('.'));
            // On teste si le fichier est bien un Pdf
            if (!extensionFichier.contains("pdf")) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Le justificatif doit être un PDF.");
                if (request.getParameter("duplicatedCommandeId") != null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE + "/" + request.getParameter("duplicatedCommandeId");
                }
                else if (commandeJson.getId() == null) {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + "/" + -1;
                } else {
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE + (commandeJson.getId() != null ? "/" + commandeJson.getId() : "");
                }
            }

            // --- Ecriture du fichier justificatif sur le serveur
            if (!fichier.isEmpty()) {
                // Récupération du nom du fichier avec son extension
                filename = fichier.getOriginalFilename();
                // Le nom fichier est un NB de millis + le filename
                String nomDuFichierASauvegarder = DateTime.now().getMillis() + "_" + filename;
                // On met à jour le justificatif de la commande avec le nom du fichier
                commandeJson.setJustificatif(nomDuFichierASauvegarder);

                String chemin = Parametrage.getContext("dossierJustificatifsCommandes") + nomDuFichierASauvegarder;
                FileFormatEnum[] formats = {FileFormatEnum.EXT_PDF, FileFormatEnum.PDF};
                fileService.uploadFile(fichier, chemin, UploadFile.MAX_SIZE_JUSTIF_COMMAND, formats); //throw exception if failed
            } else {
                throw new Exception("Le fichier à upload est vide");
            }

        } else {
            if (tCommande != null) {
                commandeJson.setJustificatif(tCommande.getJustificatif());
            }
        }

        //initialisation de l'ID du responsable facturation
        int responsableFacturation = 0;
        if (respFact != null && !respFact.equals("")) {
            // [AMNT-598] enregistrement du responsable facturation dans la commande (colonne BDD créee pour ce ticket)
            commandeJson.setIdResponsableFacturation(Long.parseLong(respFact));
            responsableFacturation = Integer.parseInt(respFact);
        } else {
            commandeJson.setIdResponsableFacturation((long) (responsableFacturation));
        }

        // On met à jour la BD
        String res = commandeService.createOrUpdateCommande(commandeJson, missionJson);
        Commande commande = commandeService.findById(commandeJson.getId());

        DateTime dateDebut = new DateTime(commande.getDateDebut());
        DateTime dateFin = new DateTime(commande.getDateFin());


        List<Facture> factures = factureService.findByMissionBetweenDates(mission, dateDebut, dateFin);
        for (Facture facture : factures) {
            facture.setCommande(commande);
            facture.setContact(responsableFacturation);
            facture.setCommentaire(commande.getCommentaire());
            facture.setAdresseFacturation(commande.getClient().getAdresseFacturation());
            facture.setIdClient(commande.getClient().getId().intValue());
            factureService.createOrUpdateFacture(facture);
        }
        // fin refonte

        if (res.equals("ok")) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Création/Modification effectuée avec succès");
            model.addAttribute(PROP_MISSIONTOEDIT, missionJson);
        }

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTION_COMMANDES + "/CO";
    }

    //[JNA][AMNOTE 142] Ajout d'un justificatif à la commande : téléchargement

    /**
     * Retourne le fichier correspondant au justificatif souhaité
     *
     * @param idCommande nom du fichier a récupérer
     * @param request    the request
     * @param response   the response
     * @param model      the model
     * @param session    the session
     * @param ra         the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "download/{idCommande}", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void downloadFichier(@PathVariable("idCommande") Long idCommande, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                         RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID de la commande
        if (null != idCommande) {
            // Récupération de la commande concernée
            Commande commande = commandeService.findById(idCommande);

            //--- Récupération du fichier
            File lFile = new File(Parametrage.getContext("dossierJustificatifsCommandes") + commande.getJustificatif());

            // --- Vérification de l'existance du fichier
            if (!lFile.exists()) {
                throw new Exception("Fichier '" + lFile.getPath() + "'" + ": introuvable");
            }

            httpResponseConstruct("pdf", response, lFile);
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Problème de récupération du fichier");

        }
    }


    /**
     * Commande exist boolean.
     *
     * @param request    the request
     * @param idCommande the id commande
     * @return the boolean
     */
    @PostMapping("/commandeExist/{idCommande}")
    public
    @ResponseBody
    Boolean commandeExist(HttpServletRequest request, @PathVariable("idCommande") Long idCommande) {
        HttpSession session = request.getSession();
        //On récupère la mission entrain d'être modifié
        MissionAsJson missionAsJson = (MissionAsJson) session.getAttribute(PROP_MISSIONTOEDIT);
        Mission mission = missionService.get(missionAsJson.getId());
        Commande commande = commandeService.findById(idCommande);

        //Si il existe une commande dans la BDD avec le même nom pour la même mission alors on renvoi true
        //Si non on renvoi false
        if (commande != null && commande.getMission().getId().equals(mission.getId())) {
            return true;
        }
        return false;

    }


    /**
     * Load mission list.
     *
     * @param request  the request
     * @param idCollab the id collab
     * @return the list
     */
//[JNA][AMNOTE 53] Récupération dynamique des missions en fonction d'un collab pour remplir la liste déroulante de missions
    @RequestMapping(value = "loadMission/{idCollab}", method = {RequestMethod.POST, RequestMethod.GET})
    public
    @ResponseBody
    List<MissionAsJson> loadMission(HttpServletRequest request, @PathVariable("idCollab") long idCollab) {
        Collaborator collaborateur = collaboratorService.get(idCollab);
        List<Mission> listeMissions = collaborateur.getMissions();
        List<MissionAsJson> missionsJson = new ArrayList<>();
        for (Mission maMission : listeMissions) {
            if (maMission.getTypeMission().getCode().equals("MI")) {
                missionsJson.add(maMission.toJson());
            }

        }
        return missionsJson;
    }

    /**
     * Change client int.
     *
     * @param request   the request
     * @param idMission the id mission
     * @return the int
     */
//SHA AMNOTE-206 Ajout liste des clients pour la facture
    @RequestMapping(value = "changeClient/{idMission}", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public int changeClient(HttpServletRequest request, @PathVariable("idMission") long idMission) {
        Long idClient;
        List<Commande> lCommande = commandeService.findAll(null);

        try {
            idClient = missionService.get(idMission).getClient().getId();
        } catch (Exception e) {
            idClient = (long) 0;
        }

        for (Commande cmdTmp : lCommande) {
            if (cmdTmp.getMission().getId().equals(idMission)) {
                idClient = cmdTmp.getClient().getId();
            }
        }

        return idClient.intValue();
    }

    /**
     * Change resp fact.
     *
     * @param request   the request
     * @param idClient the id idClient
     * @return the int
     */
    @RequestMapping(value = "changeRespFacturation/{idClient}", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public List<ContactClientAsJson> changeRespFacturation(HttpServletRequest request, @PathVariable("idClient") Long idClient) {
        List<ContactClientAsJson> lRespFact = new ArrayList<>();
        Client leClient = clientService.get(idClient);

        //On ajoute en premier les responsables facturations
        for(ContactClient cc: leClient.getContacts()) {
            if(cc.isRespFacturation()) lRespFact.add(cc.toJson());
        }

        //On ajoute ensuite le service compta
        for(ContactClient cc: leClient.getContacts()) {
            if(!cc.isRespFacturation()  && !cc.isRespClient()) lRespFact.add(cc.toJson());
        }

        return lRespFact;
    }

    /**
     * Stoper une commande
     *
     * @param id      ID de la commande à supprimer
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers la page des commandes
     * @throws IOException the io exception
     */
    @RequestMapping(value = "stopCommande/{idCommande}", method = {RequestMethod.GET, RequestMethod.POST})
    public String stopCommande(@PathVariable("idCommande") Long id, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
        throws IOException {
        init(request, model);

        if (null != id) {
            Commande tmpCommande = commandeService.findById(id);
            Mission tmpMission = missionService.get(tmpCommande.getMission().getId());
            if (null != tmpCommande) {
                Etat etat = etatService.getEtatByCode("TE");
                tmpCommande.setEtat(etat);
                commandeService.createOrUpdateCommande(tmpCommande, tmpMission);
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "La commande " + tmpCommande.getNumero() + " a bien été arretée");
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "L'id de la commande est null");
        }

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTION_COMMANDES + "/CO";
    }

    /**
     * Méthode qui permet de créer un commande à partir d'un commande arrêtée
     *
     * @param id        the id
     * @param session   the session
     * @param request   the request
     * @param model     the model
     * @param ra        the redirection attributes
     * @return      the redirection string
     * @throws IOException the IO exception
     */
    @RequestMapping(value = "duplicateStoppedCommande/{idCommande}", method = {RequestMethod.GET, RequestMethod.POST})
    public String duplicateStoppedCommande(@PathVariable("idCommande") Long id, HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra)
        throws IOException {
        init(request, model);

        List<CollaboratorAsJson> listCollabWithDisable = collaboratorService.findAllOrderByNomAscAsJsonWithoutADMINWithDisabled();
        model.addAttribute("listeCollaborateurs", listCollabWithDisable);

        List<MoyensPaiement> paiements = paiementService.findAll(null);
        List<String> conditionPaiement = new ArrayList<>();

        Commande commande = commandeService.findById(id);

        CommandeAsJson newCommandeJson = new CommandeAsJson();

        if (commande.getJustificatif() != null) {
            newCommandeJson.setJustificatif(commande.getJustificatif());
            newCommandeJson.setMoyensPaiement(commande.getMoyensPaiement());
            newCommandeJson.setMontant(commande.getMontant());
        }

        if (commande.getConditionPaiement() == null
                || commande.getConditionPaiement().equals(VIREMENT_ATTRIBUTE)
                || commande.getConditionPaiement().equals(BANKCHECK_ATTRIBUTE)) {
            conditionPaiement.add(VIREMENT_ATTRIBUTE);
            conditionPaiement.add(BANKCHECK_ATTRIBUTE);
        }

        Collaborator collab = commande.getMission().getCollaborateur();
        // On récupère la liste des missions du collab de cette commande
        List<MissionAsJson> listeMissionsCollab = missionService.findAllMissionsCollaborator(commande.getMission().getCollaborateur());

        Mission selectedMission = commande.getMission();
        Client selectedClient = commande.getClient();
        String numeroCommande = commande.getNumero();
        String libelle = commande.getCommentaire();
        DateTime dateDebut = new DateTime(commande.getDateDebut());
        DateTime dateFin = new DateTime(commande.getDateFin());

        //recuperation du responsable facturation
        List<Facture> factures = factureService.findByMissionBetweenDates(commande.getMission(), dateDebut, dateFin);
        Long selectedIdResponsable=0l;

        //[AMNT-598] ajout de la nouvelle méthode pour récupérer un responsable facturation
        // ancienne méthode (via facture) conservée pour une transition progressive vers la table BDD COMMANDE_ATTRIBUTE
        if (commande.getIdResponsableFacturation() != null) { selectedIdResponsable = commande.getIdResponsableFacturation(); }
        else if(!factures.isEmpty()) { selectedIdResponsable= Long.valueOf(factures.get(0).getContact()); }

        //recuperation de la liste des responsables
        List<ContactClientAsJson> listContacts = new ArrayList<>();
        List<ContactClient> listContactsTemp = commande.getClient().getContacts();
        for(ContactClient cc: listContactsTemp) {
            if(cc.isRespFacturation()) listContacts.add(cc.toJson());
        }
        //On ajoute ensuite le service compta
        for(ContactClient cc: listContactsTemp) {
            if(!cc.isRespFacturation()  && !cc.isRespClient()) listContacts.add(cc.toJson());
        }



            model.addAttribute("selectedidRespo", selectedIdResponsable);
            model.addAttribute("listeResponsablesFact", listContacts);
            model.addAttribute("idCommande", id);
            model.addAttribute("duplicate", true);
            model.addAttribute("dateDebut", dateDebut.toString(DATE_FORMAT_YYYY_MM_DD));
            model.addAttribute("dateFin", dateFin.toString(DATE_FORMAT_YYYY_MM_DD));
            model.addAttribute("commentaire", libelle);
            model.addAttribute("selectedClient", selectedClient);
            model.addAttribute("numero", numeroCommande);
            model.addAttribute("selectedMission", selectedMission);
            model.addAttribute("listeMissionsCollab", listeMissionsCollab);
            model.addAttribute(COMMANDE_ATTRIBUTE, newCommandeJson);
            model.addAttribute("collab", collab.toJson());

            model.addAttribute("conditionPaiementCommande", conditionPaiement);
            model.addAttribute("listePaiements", paiements);
            //SHA AMNOTE-206 Liste de clients pour la facture
            model.addAttribute("listeClients", clientService.findAllOrderByNomAsc());

            return NAV_ADMIN + NAV_ADMIN_EDITCOMMANDE;

        }





    /**
     *  Créer et exporter l'excel des commandes en cours
     * @param request request
     * @param model model
     * @param redirectAttributes redirectAttributes
     * @param session session
     * @return excel file
     * @throws IOException IOException
     * @throws NamingException NamingException
     */
    @RequestMapping(value = "gestionCommandes/", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<byte[]> genererExcelCommandesRecap(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes, HttpSession session) throws IOException, NamingException {
        init(request, model);

        String filtre = (String) session.getAttribute(FILTER_ATTRIBUTE);
        String date = null;

        List<Commande> commandes;
        if (isDateVoulue(session)) {
            date = (String) session.getAttribute(WISHED_DATE_ATTRIBUTE);
            commandes = getCommandes(filtre, date);
            date = "01/" + date;
        } else {
            commandes = getCommandes(filtre, date);
            DateTime dateTime = new DateTime();
            date = utils.sdf.format(dateTime.toDate());
        }

        String fileName = null;

        if (filtre.equals("CO")) {
            if (isDateVoulue(session)) {
                fileName = "Recapitulatif_Commandes_En_Cours_" + Utils.moisStringToString(date.substring(3, 5)) + "_" + date.substring(6);
            } else {
                fileName = "Recapitulatif_Commandes_En_Cours_" + date.substring(6);
            }

        } else if (filtre.equals("TE")) {
            if (isDateVoulue(session)) {
                fileName = "Recapitulatif_Commandes_Terminees_" + Utils.moisStringToString(date.substring(3, 5)) + "_" + date.substring(6);
            } else {
                fileName = "Recapitulatif_Commandes_Terminees_" + date.substring(6);
            }

        }

        genererExcelCommandes(commandes, fileName, date);


        return downloadExcel(fileName);
    }

    /**
     * Renvoie un booleen si il y a une date passée en session
     * @param session
     * @return true or false
     */
    private boolean isDateVoulue(HttpSession session) {
        return session.getAttribute(WISHED_DATE_ATTRIBUTE) != null;
    }

    /**
     * Appel au service pour générer le fichier excel
     * @param commandes
     * @param fileName
     * @param date
     * @throws IOException
     * @throws NamingException
     */
    private void genererExcelCommandes(List<Commande> commandes, String fileName, String date) throws IOException, NamingException {
        documentExcelService.createExcelCommandes(commandes, fileName, date);
    }

    /**
     * Télécharge le fichier excel de récapitulatif des commandes
     *
     * @param file    the file
     * @return Le fichier excel qui doit se télécharger
     * @throws NamingException the naming exception
     * @throws IOException     the io exception
     */
    @GetMapping("gestionCommandes/downloadExcel/{file}")
    public ResponseEntity<byte[]> downloadExcel(String file) throws NamingException, IOException {
        byte[] contents = new byte[]{0};
        HttpHeaders headers = new HttpHeaders();

        File fileExcel = new File(Parametrage.getContext(Constantes.TEMP_FOLDER_FILES) + file + Constantes.EXTENSION_FILE_XLS);

        if (fileExcel.exists()) {
            Path path = fileExcel.toPath();
            contents = Files.readAllBytes(path);

            headers.setContentType(MediaType.parseMediaType("application/xls"));

            headers.setContentDispositionFormData(fileExcel.getName(), fileExcel.getName());
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        }
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }

}
