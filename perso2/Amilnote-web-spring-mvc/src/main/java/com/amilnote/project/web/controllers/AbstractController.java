/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.utils.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * The type Abstract controller.
 */

public abstract class AbstractController {

    /**
     * The constant FLASHATTRIBUTE_KEY_SUCCESS.
     */
    public static final String FLASHATTRIBUTE_KEY_SUCCESS = "actionSuccess";
    /**
     * The constant FLASHATTRIBUTE_KEY_ERROR.
     */
    protected static final String FLASHATTRIBUTE_KEY_ERROR = "actionError";
    /**
     * The constant HTTP_STATUS_200.
     */
    protected static final String HTTP_STATUS_200 = HttpStatus.OK.toString();
    /**
     * The constant NAME_NAV_LOGOUT.
     */
// nom des propriétés
    protected static final String NAME_NAV_LOGOUT = "logout";
    /**
     * The constant NAME_NAV_WELCOME.
     */
    protected static final String NAME_NAV_WELCOME = "welcome";
    /**
     * The constant NAME_NAV_GESTIONPROFIL.
     */
    protected static final String NAME_NAV_GESTIONPROFIL = "gestionProfil";
    /**
     * The constant NAME_NAV_GESTIONMAIL.
     */
    protected static final String NAME_NAV_GESTIONMAIL = "mail";
    /**
     * The constant NAME_NAV_PROFIL_ERRORMDP.
     */
    protected static final String NAME_NAV_PROFIL_ERRORMDP = "errorValidationMDP";
    /**
     * The constant NAME_NAV_PROFIL_MESSMDP.
     */
    protected static final String NAME_NAV_PROFIL_MESSMDP = "messageValidationMDP";
    /**
     * The constant NAME_NAV_REINITIALISE_MDP.
     */
    protected static final String NAME_NAV_REINITIALISE_MDP = "newMdp";
    /**
     * The constant NAME_NAV_ERREUR_MDP.
     */
    protected static final String NAME_NAV_ERREUR_MDP = "errorNewMdp";
    /**
     * The constant NAME_NAV_TEMP_REINITMDP.
     */
    protected static final String NAME_NAV_TEMP_REINITMDP = "chgtmdpenvoye";
    /**
     * The constant NAME_NAV_TIMESHEETS.
     */
    protected static final String NAME_NAV_TIMESHEETS = "mesTimeSheets";
    /**
     * The constant NAME_NAV_FRAIS.
     */
    protected static final String NAME_NAV_FRAIS = "mesNotesDeFrais";
    /**
     * The constant NAME_NAV_FRAIS_NOTESDEFRAIS.
     */
    protected static final String NAME_NAV_FRAIS_NOTESDEFRAIS = "frais";
    /**
     * The constant NAME_NAV_FRAIS_SOUMISSIONAVANCE.
     */
    protected static final String NAME_NAV_FRAIS_SOUMISSIONAVANCE = "soumettreAvance";
    /**
     * The constant NAME_NAV_FRAIS_ASSOCIATION_JUSTIFS.
     */
    protected static final String NAME_NAV_FRAIS_ASSOCIATION_JUSTIFS = "assocFraisJustifs";
    /**
     * The constant NAME_NAV_FRAIS_NEW_JUSTIF.
     */
    protected static final String NAME_NAV_FRAIS_NEW_JUSTIF = "newJustif";
    /**
     * The constant NAME_NAV_FRAIS_DELETE_JUSTIF.
     */
    protected static final String NAME_NAV_FRAIS_DELETE_JUSTIF = "deleteJustif";
    /**
     * The constant NAME_NAV_FRAIS_RETIRER_JUSTIF.
     */
    protected static final String NAME_NAV_FRAIS_RETIRER_JUSTIF = "retirerJustif";
    /**
     * The constant NAME_NAV_FRAIS_SAVE_JUSTIF.
     */
    protected static final String NAME_NAV_FRAIS_SAVE_JUSTIF = "saveJustif";
    /**
     * The constant NAME_NAV_ABSENCE.
     */
    protected static final String NAME_NAV_ABSENCE = "mesAbsences";
    /**
     * The constant NAME_NAV_TIMESHEETS_MISSION.
     */
    protected static final String NAME_NAV_TIMESHEETS_MISSION = "getMissions";
    /**
     * The constant NAME_NAV_TIMESHEETS_TYPEABSENCE.
     */
    protected static final String NAME_NAV_TIMESHEETS_TYPEABSENCE = "getTypeAbsence";
    /**
     * The constant NAME_NAV_SAVE_FRAIS.
     */
    protected static final String NAME_NAV_SAVE_FRAIS = "saveFrais";
    /**
     * The constant NAME_NAV_DELETE_FRAIS.
     */
    protected static final String NAME_NAV_DELETE_FRAIS = "deleteFrais";
    /**
     * The constant NAME_NAV_ADMIN_DELETEFACTURE.
     */
    protected static final String NAME_NAV_ADMIN_DELETEFACTURE = "deleteFacture";
    /**
     * The constant NAME_NAV_EDIT_FRAIS.
     */
    protected static final String NAME_NAV_EDIT_FRAIS = "editFrais";
    /**
     * The constant NAME_NAV_GETIMAGE_FRAIS.
     */
    protected static final String NAME_NAV_GETIMAGE_FRAIS = "getImageFrais";
    /**
     * The constant NAME_NAV_MAIL_ADMIN.
     */
    protected static final String NAME_NAV_MAIL_ADMIN = "mailAdministrateur";
    /**
     * The constant NAME_NAV_ADMIN.
     */
    protected static final String NAME_NAV_ADMIN = "administration";
    /**
     * The constant NAME_NAV_ADMIN_COLLABORATEURS.
     */
    protected static final String NAME_NAV_ADMIN_COLLABORATEURS = "collaborateurs";
    /**
     * The constant NAME_NAV_ADMIN_EDITCOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_EDITCOLLABORATEUR = "editCollaborateur";

    /**
     * The constant NAME_NAV_ADMIN_TRAVAILLEUR
     */
    protected static final String NAME_NAV_ADMIN_TRAVAILLEURS = "travailleurs";

    /**
     * The constant NAME_NAV_ADMIN_SOUSTRAITANT
     */
    protected static final String NAME_NAV_ADMIN_SOUSTRAITANTS = "sousTraitants";

    /**
     * The constant NAME_NAV_ADMIN_SOUS_TRAITANTS
     */
    protected static final String NAME_NAV_ADMIN_SOUS_TRAITANTS = "sous_traitants";

    /**
     * The constant  NAME_NAV_ADMIN_EDITSOUSTRAITANT
     */
    protected static final String NAME_NAV_ADMIN_EDITSOUSTRAITANT = "editSousTraitant";

    /**
     * The constant NAME_NAV_ADMIN_SAVECOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_SAVESOUSTRAITANT = "saveSousTraitant";
    /**
     * The constant NAME_NAV_ADMIN_DELETECOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_DELETESOUSTRAITANT = "deleteSousTraitant";

    /**
     * The constant NAME_NAV_ADMIN_SAVECOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_SAVECOLLABORATEUR = "saveCollaborateur";
    /**
     * The constant NAME_NAV_ADMIN_DELETECOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_DELETECOLLABORATEUR = "deleteCollaborateur";
    /**
     * The constant NAME_NAV_ADMIN_DELETECOLLABORATEUR.
     */
    protected static final String NAME_NAV_ADMIN_EDITTRAVAILLEUR = "editTravailleur";
    /**
     * The constant NAME_NAV_NAV_ADMIN_NEWTRAVAILLEUR.
     */
    protected static final String NAME_NAV_ADMIN_NEWTRAVAILLEUR = "newTravailleur";
    /**
     * The constant NAME_NAV_ADMIN_NOTEDEFRAIS.
     */
    protected static final String NAME_NAV_ADMIN_NOTEDEFRAIS = "noteDeFrais";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONNOTEDEFRAIS.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONNOTEDEFRAIS = "validNoteDeFrais";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSMULTIPLESASF.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSMULTIPLESASF = "validMultipleNoteDeFrais";
    /**
     * The constant NAME_NAV_ADMIN_REFUSNOTEDEFRAIS.
     */
    protected static final String NAME_NAV_ADMIN_REFUSNOTEDEFRAIS = "refusNoteDeFrais";
    /**
     * The constant NAME_NAV_ADMIN_EDITMISSION.
     */
    protected static final String NAME_NAV_ADMIN_EDITMISSION = "editMission";
    /**
     * The constant NAME_NAV_ADMIN_SAVEMISSION.
     */
    protected static final String NAME_NAV_ADMIN_SAVEMISSION = "saveMission";
    /**
     * The constant NAME_NAV_ADMIN_DELETEMISSION.
     */
    protected static final String NAME_NAV_ADMIN_DELETEMISSION = "deleteMission";
    /**
     * The constant NAME_NAV_ADMIN_EDITLINKFORFAIT.
     */
    protected static final String NAME_NAV_ADMIN_EDITLINKFORFAIT = "editForfait";
    /**
     * The constant NAME_NAV_ADMIN_SAVELINKFORFAIT.
     */
    protected static final String NAME_NAV_ADMIN_SAVELINKFORFAIT = "saveForfait";
    /**
     * The constant NAME_NAV_ADMIN_DELETELINKFORFAIT.
     */
    protected static final String NAME_NAV_ADMIN_DELETELINKFORFAIT = "deleteForfait";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSABSENCES.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSABSENCES = "validationsAbsences";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES = "validationsMultiplesAbsences";

    /**
     * The constant NAME_NAV_ADMIN_CREATION_PDF_MULTIPLES_RA.
     */
    protected static final String NAME_NAV_ADMIN_CREATION_PDF_MULTIPLES_RA = "creationPdfMultiplesRA";
    /**
     * The constant NAME_NAV_ADMIN_CONSULTATIONABSENCES.
     */
    protected static final String NAME_NAV_ADMIN_CONSULTATIONABSENCES = "consultationAbsences";
    /**
     * The constant NAME_NAV_ADMIN_ANNULATIONABSENCES.
     */
    protected static final String NAME_NAV_ADMIN_ANNULATIONABSENCES = "annulationAbsences";
    /**
     * The constant NAME_NAV_ADMIN_GESTIONAPPLICATION.
     */
    protected static final String NAME_NAV_ADMIN_GESTIONAPPLICATION = "gestionApplication";
    /**
     * The constant NAME_NAV_ADMIN_SAVETVA.
     */
    protected static final String NAME_NAV_ADMIN_SAVETVA = "saveTVA";
    /**
     * The constant NAME_NAV_ADMIN_SAVEBUDGET.
     */
    protected static final String NAME_NAV_ADMIN_SAVEBUDGET = "saveBudget";
    /**
     * The constant NAME_NAV_ADMIN_SAVETVAFORTYPEFRAIS.
     */
    protected static final String NAME_NAV_ADMIN_SAVETVAFORTYPEFRAIS = "saveTVAForTypeFrais";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSRA.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSRA = "validationsRA";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSMULTIPLESRA.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSMULTIPLESRA = "validationsMultiplesRA";
    /**
     * The constant NAME_NAV_ADMIN_CONSULTATIONRA.
     */
    protected static final String NAME_NAV_ADMIN_CONSULTATIONRA = "consultationRA";
    /**
     * The constant NAME_NAV_ADMIN_LISTESOUMISRA.
     */
    protected static final String NAME_NAV_ADMIN_LISTESOUMISRA = "listeSoumissionRA";
    /**
     * The constant NAME_NAV_ADMIN_VALIDE.
     */
    protected static final String NAME_NAV_ADMIN_VALIDE = "VA";
    /**
     * The constant NAME_NAV_ADMIN_EN_COURS.
     */
    protected static final String NAME_NAV_ADMIN_EN_COURS = "CO";
    /**
     * The constant NAME_NAV_ADMIN_TERMINEE.
     */
    protected static final String NAME_NAV_ADMIN_TERMINEE = "TE";
    /**
     * The constant NAME_NAV_ADMIN_REFUSE.
     */
    protected static final String NAME_NAV_ADMIN_REFUSE = "RE";
    /**
     * The constant NAME_NAV_ADMIN_SOUMIS.
     */
    protected static final String NAME_NAV_ADMIN_SOUMIS = "SO";
    /**
     * The constant NAME_NAV_ADMIN_ANNULE.
     */
    protected static final String NAME_NAV_ADMIN_ANNULE = "AN";
    /**
     * The constant NAME_NAV_ADMIN_DOWNLOAD.
     */
    protected static final String NAME_NAV_ADMIN_DOWNLOAD = "download";
    /**
     * The constant NAME_NAV_ADMIN_FACTURES_VALIDEES.
     */
    protected static final String NAME_NAV_ADMIN_FACTURES_VALIDEES = "facturesValidees";
    /**
     * The constant NAME_NAV_ADMIN_NUMEROTATION_FACTURES.
     */
    protected static final String NAME_NAV_ADMIN_NUMEROTATION_FACTURES = "numeroterFactures";
    /**
     * The constant NAME_NAV_ADMIN_SOUMISSIONS_FACTURES.
     */
    protected static final String NAME_NAV_ADMIN_SOUMISSIONS_FACTURES = "soumettreFactures";
    /**
     * The constant NAME_NAV_ADMIN_FACTURES_REFUSEES.
     */
    protected static final String NAME_NAV_ADMIN_FACTURES_REFUSEES = "refuserFactures";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONFACTURES_MOIS.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATION_FACTURES = "validerFactures";
    /**
     * The constant NAME_NAV_ADMIN_FORCER_FACTURES.
     */
    protected static final String NAME_NAV_ADMIN_FORCER_FACTURES = "forcerFactures";
    /**
     * The constant NAME_NAV_ADMIN_GESTION_TABLEURS.
     */
    protected static final String NAME_NAV_ADMIN_GESTION_TABLEURS = "gestionTableurs";
    /**
     * The constant NAME_NAV_ADMIN_TABLEURS.
     */
    protected static final String NAME_NAV_ADMIN_TABLEURS = "tableurExcel";
    /**
     * The constant NAME_NAV_ADMIN_DEPLACEMENTS.
     */
    protected static final String NAME_NAV_ADMIN_DEPLACEMENTS = "validationDeplacements";
    /**
     * The constant NAME_NAV_ADMIN_ANNULATION_DEPLACEMENTS.
     */
    protected static final String NAME_NAV_ADMIN_ANNULATION_DEPLACEMENTS = "annulationDeplacements";
    /**
     * The constant NAME_NAV_EDIT_DEPLACEMENT.
     */
    protected static final String NAME_NAV_EDIT_DEPLACEMENT = "editDeplacement";
    /**
     * The constant NAME_NAV_SAVE_DEPLACEMENT.
     */
    protected static final String NAME_NAV_SAVE_DEPLACEMENT = "saveDeplacement";
    /**
     * The constant NAME_NAV_ADMIN_CONSULTATION_DEPLACEMENTS.
     */
    protected static final String NAME_NAV_ADMIN_CONSULTATION_DEPLACEMENTS = "consultationDeplacements";
    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS = "validationsMultiplesDeplacements";
    /**
     * The constant NAME_NAV_ADMIN_EDIT_MISSIONCLIENTE.
     */
    protected static final String NAME_NAV_ADMIN_EDIT_MISSIONCLIENTE = "editMissionCliente";
    /**
     * The constant NAME_NAV_ADMIN_SAVE_MISSIONCLIENTE.
     */
    protected static final String NAME_NAV_ADMIN_SAVE_MISSIONCLIENTE = "saveMissionCliente";
    /**
     * The constant NAME_NAV_ADMIN_CLIENTS.
     */
    protected static final String NAME_NAV_ADMIN_CLIENTS = "clients";
    /**
     * The constant NAME_NAV_ADMIN_EDIT_CLIENT.
     */
    protected static final String NAME_NAV_ADMIN_EDIT_CLIENT = "editClient";

    /**
     * The constant NAME_NAV_ADMIN_SAVECLIENT.
     */
    protected static final String NAME_NAV_ADMIN_SAVECLIENT = "saveClient";

    /**
     * The constant NAME_NAV_ADMIN_DELETECLIENT.
     */
    protected static final String NAME_NAV_ADMIN_DELETECLIENT = "deleteClient";

    /**
     * The constant NAME_NAV_ADMIN_EDIT_CONTACTCLIENT.
     */
    protected static final String NAME_NAV_ADMIN_EDIT_CONTACTCLIENT = "editContactClient";

    /**
     * The constant NAME_NAV_ADMIN_SAVE_CONTACTCLIENT.
     */
    protected static final String NAME_NAV_ADMIN_SAVE_CONTACTCLIENT = "saveContactClient";

    /**
     * The constant NAME_NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT.
     */
    protected static final String NAME_NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT = "addDefaultContactClient";

    /**
     * The constant NAME_NAV_ADMIN_DELETE_CONTACTCLIENT.
     */
    protected static final String NAME_NAV_ADMIN_DELETE_CONTACTCLIENT = "deleteClient";

    /**
     * The constant NAME_NAV_ADMIN_VALID_ODM.
     */
    protected static final String NAME_NAV_ADMIN_VALID_ODM = "validationODM";

    /**
     * The constant NAME_NAV_ADMIN_VALIDATIONSMULTIPLES_ODM.
     */
    protected static final String NAME_NAV_ADMIN_VALIDATIONSMULTIPLES_ODM = "validationsMultiplesODM";

    /**
     * The constant NAME_NAV_ODM.
     */
    protected static final String NAME_NAV_ODM = "mesOrdresDeMission";

    /**
     * The constant NAME_NAV_ODM_ORDRESDEMISSION.
     */
    protected static final String NAME_NAV_ODM_ORDRESDEMISSION = "ordresDeMission";

    /**
     * The constant NAME_NAV_ADMIN_COLLABORATEURS_TYPE_MISSION.
     */
    protected static final String NAME_NAV_ADMIN_COLLABORATEURS_TYPE_MISSION = "collaborateurTypeMission";

    /**
     * The constant NAME_NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE.
     */
    protected static final String NAME_NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE = "collaborateurOutilsInterne";

    /**
     * The constant NAME_NAV_ADMIN_FACTURES.
     */
    protected static final String NAME_NAV_ADMIN_FACTURES = "factures";

    /**
     * The constant NAME_NAV_ADMIN_CREATIONFACTURES_MOIS.
     */
    protected static final String NAME_NAV_ADMIN_CREATIONFACTURES_MOIS = "creationFacturesMois";

    /**
     * The constant NAME_NAV_ADMIN_CREATIONFACTURE_VIERGE.
     */
    protected static final String NAME_NAV_ADMIN_CREATIONFACTURE_VIERGE = "creationFactureVierge";

    /**
     * The constant NAME_NAV_ADMIN_GET_CONSULTANT.
     */
    protected static final String NAME_NAV_ADMIN_GET_CONSULTANT = "getConsultant";

    /**
     * The constant NAME_NAV_ADMIN_EDITFACTURE.
     */
    protected static final String NAME_NAV_ADMIN_EDITFACTURE = "editFacture";

    /**
     * The constant NAME_NAV_ADMIN_SAVEFACTURE.
     */
    protected static final String NAME_NAV_ADMIN_SAVEFACTURE = "saveFacture";

    /**
     * The constant NAME_NAV_ADMIN_DELETEFRAIS.
     */
    protected static final String NAME_NAV_ADMIN_DELETEFRAIS = "deleteFrais";

    /**
     * The constant NAME_NAV_ADMIN_VISUALISEZ.
     */
    protected static final String NAME_NAV_ADMIN_VISUALISEZ = "visualisez";

    /**
     * The constant NAME_NAV_ADMIN_AFFICHERPDF.
     */
    protected static final String NAME_NAV_ADMIN_AFFICHERPDF = "afficher";

    /**
     * The constant NAME_NAV_ADMIN_SAVE_FACTURE_ARCHIVEE.
     */
    protected static final String NAME_NAV_ADMIN_SAVE_FACTURE_ARCHIVEE = "saveFactureArchivee";

    /**
     * The constant NAME_NAV_ADMIN_FACTURES_PRINT.
     */
    protected static final String NAME_NAV_ADMIN_FACTURES_PRINT = "print";

    /**
     * The constant NAME_NAV_ADMIN_EDITCOMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_EDITCOMMANDE = "editCommande";

    /**
     * The constant NAME_NAV_ADMIN_LOADMISSION.
     */
    protected static final String NAME_NAV_ADMIN_LOADMISSION = "loadMission";

    /**
     * The constant NAME_NAV_ADMIN_SAVECOMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_SAVECOMMANDE = "saveCommande";

    /**
     * The constant NAME_NAV_ADMIN_DELETECOMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_DELETECOMMANDE = "deleteCommande";

    /**
     * The constant NAME_NAV_ADMIN_DELETECOMM.
     */
    protected static final String NAME_NAV_ADMIN_DELETECOMM = "deleteComm";

    /**
     * The constant NAME_NAV_ADMIN_NEWCOMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_NEWCOMMANDE = "newCommande";

    /**
     * The constant NAME_NAV_ADMIN_GESTION_COMMANDES.
     */
    protected static final String NAME_NAV_ADMIN_GESTION_COMMANDES = "gestionCommandes";

    /**
     * The constant NAME_NAV_ADMIN_STOP_COMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_STOP_COMMANDE = "stopCommande";

    /**
     * The constant NAME_NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE.
     */
    protected static final String NAME_NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE = "duplicateStoppedCommande";

    /**
     * The constant NAME_NAV_ADMIN_MODULE_ADDVISE.
     */
    protected static final String NAME_NAV_ADMIN_MODULE_ADDVISE = "moduleAddvise";

    /**
     * The constant NAME_NAV_ADMIN_SUPPRESSION_MODULE_ADDVISE.
     */
    protected static final String NAME_NAV_ADMIN_SUPPRESSION_MODULE_ADDVISE = "suppressionModulesAddvise";

    /**
     * The constant NAME_NAV_ADMIN_EDIT_MODULE_ADDVISE.
     */
    protected static final String NAME_NAV_ADMIN_EDIT_MODULE_ADDVISE = "editModulesAddvise";

    /**
     * The constant NAME_NAV_ADMIN_SAVE_MODULE_ADDVISE.
     */
    protected static final String NAME_NAV_ADMIN_SAVE_MODULE_ADDVISE = "saveModulesAddvise";

    /**
     * The constant NAME_NAV_ADMIN_MODULE_DISPONIBILITE.
     */
    protected static final String NAME_NAV_ADMIN_MODULE_DISPONIBILITE = "consultationDisponibilites";


    /**
     * The constant NAME_NAV_EMAIL_EXIST.
     */
    protected static final String NAME_NAV_EMAIL_EXIST = "emailExist";

    /**
     * The constant NAV_LOGOUT.
     */
// chemin de navigation
    protected static final String NAV_LOGOUT = "logout";

    /**
     * The constant NAV_WELCOME.
     */
    protected static final String NAV_WELCOME = "welcome";

    /**
     * The constant NAV_GESTIONPROFIL.
     */
    protected static final String NAV_GESTIONPROFIL = "Profil/gestionProfil";

    /**
     * The constant NAV_GESTIONMAIL.
     */
    protected static final String NAV_GESTIONMAIL = "mail";

    /**
     * The constant NAV_TIMESHEETS.
     */
    protected static final String NAV_TIMESHEETS = "Rapport-Activites/mesTimeSheets";


    /**
     * The constant NAV_NOTEDEFRAIS.
     */
    protected static final String NAV_NOTEDEFRAIS = "mesNotesDeFrais";

    /**
     * The constant NAV_ABSENCE.
     */
    protected static final String NAV_ABSENCE = "Absences/mesAbsences";

    /**
     * The constant NAV_TIMESHEETS_MISSION.
     */
    protected static final String NAV_TIMESHEETS_MISSION = "Rapport-Activites/getMissions";

    protected static final String NAV_RA_NOTESDEFRAIS = "Rapport-Activites/mesNotesDeFrais";

    /**
     * The constant NAV_TIMESHEETS_TYPEABSENCE.
     */
    protected static final String NAV_TIMESHEETS_TYPEABSENCE = "Rapport-Activites/getTypeAbsence";

    /**
     * The constant NAV_FRAIS_NOTESDEFRAIS.
     */
    protected static final String NAV_FRAIS_NOTESDEFRAIS = "/frais";

    /**
     * The constant NAV_FRAIS_SOUMISSIONAVANCE.
     */
    protected static final String NAV_FRAIS_SOUMISSIONAVANCE = "/soumettreAvance";

    /**
     * The constant NAV_FRAIS_SAVE_FRAIS.
     */
    protected static final String NAV_FRAIS_SAVE_FRAIS = "/saveFrais";

    /**
     * The constant NAV_FRAIS_DELETE_FRAIS.
     */
    protected static final String NAV_FRAIS_DELETE_FRAIS = "/deleteFrais";

    /**
     * The constant NAV_FRAIS_RETIRER_JUSTIF.
     */
    protected static final String NAV_FRAIS_RETIRER_JUSTIF = "/retirerJustif";

    /**
     * The constant NAV_FRAIS_EDIT_FRAIS.
     */
    protected static final String NAV_FRAIS_EDIT_FRAIS = "/editFrais";

    /**
     * The constant NAV_ADMIN_CREATION_PDF_MULTIPLES_RA.
     */
    protected static final String NAV_ADMIN_CREATION_PDF_MULTIPLES_RA = "/creationPdfMultiplesRA";

    /**
     * The constant NAV_FRAIS_ASSOCIATION_JUSTIFS.
     */
    protected static final String NAV_FRAIS_ASSOCIATION_JUSTIFS = "/assocFraisJustifs";

    /**
     * The constant NAV_FRAIS_NEW_JUSTIF.
     */
    protected static final String NAV_FRAIS_NEW_JUSTIF = "/newJustif";

    /**
     * The constant NAV_FRAIS_DELETE_JUSTIF.
     */
    protected static final String NAV_FRAIS_DELETE_JUSTIF = "/deleteJustif";

    /**
     * The constant NAV_FRAIS_SAVE_JUSTIF.
     */
    protected static final String NAV_FRAIS_SAVE_JUSTIF = "/saveJustif";

    /**
     * The constant NAV_GETIMAGE_FRAIS.
     */
    protected static final String NAV_GETIMAGE_FRAIS = "Rapport-Activites/getImageFrais";

    /**
     * The constant NAV_MAIL_ADMIN.
     */
    protected static final String NAV_MAIL_ADMIN = "mailAdministrateur";

    /**
     * The constant NAV_ADMIN.
     */
    protected static final String NAV_ADMIN = "Administration";

    /**
     * The constant NAV_ADMIN_FACTURES_VALIDEES.
     */
    protected static final String NAV_ADMIN_FACTURES_VALIDEES = "/facturesValidees";

    /**
     * The constant NAV_ADMIN_COLLABORATEURS_TYPE_MISSION.
     */
    protected static final String NAV_ADMIN_COLLABORATEURS_TYPE_MISSION = "/typeMission";

    /**
     * The constant NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE.
     */
    protected static final String NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE = "/outilsInterne";

    /**
     * The constant NAV_ADMIN_COLLABORATEURS.
     */
    protected static final String NAV_ADMIN_COLLABORATEURS = "/collaborateurs";

    /**
     * The constant NAV_ADMIN_SOUSTRAITANTS.
     */
    protected static final String NAV_ADMIN_SOUSTRAITANTS = "/soustraitants";

    /**
     * The constant NAV_ADMIN_TRAVAILLEURS.
     */
    protected static final String NAV_ADMIN_TRAVAILLEURS = "/travailleurs";

    /**
     * The constant NAV_ADMIN_EDITCOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_EDITCOLLABORATEUR = "/editCollaborateur";


    /**
     * The constant NAV_ADMIN_EDITCOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_EDITSOUSTRAITANTS = "/editSousTraitant";


    /**
     * The constant NAV_ADMIN_SAVECOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_SAVESOUSTRAITANT = "/saveSousTraitant";

    /**
     * The constant NAV_ADMIN_DELETESOUSTRAITANT
     */
    protected static final String NAV_ADMIN_DELETESOUSTRAITANT = "/deleteSousTraitant";

    /**
     * The constant NAV_ADMIN_SAVECOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_SAVECOLLABORATEUR = "/saveCollaborateur";


    /**
     * The constant NAV_ADMIN_DELETECOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_DELETECOLLABORATEUR = "/deleteCollaborateur";

    /**
     * The constant NAV_ADMIN_DELETECOLLABORATEUR.
     */
    protected static final String NAV_ADMIN_EDITTRAVAILLEUR = "/editTravailleur";
    /**
     * The constant NAV_ADMIN_NEWTRAVAILLEUR.
     */
    protected static final String NAV_ADMIN_NEWTRAVAILLEUR = "/newTravailleur";

    /**
     * The constant NAV_ADMIN_NOTEDEFRAIS.
     */
    protected static final String NAV_ADMIN_NOTEDEFRAIS = "/noteDeFrais";

    /**
     * The constant NAV_ADMIN_VALIDATIONNOTEDEFRAIS.
     */
    protected static final String NAV_ADMIN_VALIDATIONNOTEDEFRAIS = "/validNoteDeFrais";

    /**
     * The constant NAV_ADMIN_REFUSNOTEDEFRAIS.
     */
    protected static final String NAV_ADMIN_REFUSNOTEDEFRAIS = "/refusNoteDeFrais";

    /**
     * The constant NAV_ADMIN_VALIDATIONSMULTIPLESASF.
     */
    protected static final String NAV_ADMIN_VALIDATIONSMULTIPLESASF = "/validMultipleNoteDeFrais";

    /**
     * The constant NAV_ADMIN_EDITMISSION.
     */
    protected static final String NAV_ADMIN_EDITMISSION = "/editMission";

    /**
     * The constant NAV_ADMIN_SAVEMISSION.
     */
    protected static final String NAV_ADMIN_SAVEMISSION = "/saveMission";

    /**
     * The constant NAV_ADMIN_DELETEMISSION.
     */
    protected static final String NAV_ADMIN_DELETEMISSION = "/deleteMission";

    /**
     * The constant NAV_ADMIN_EDITLINKFORFAIT.
     */
    protected static final String NAV_ADMIN_EDITLINKFORFAIT = "/editForfait";

    /**
     * The constant NAV_ADMIN_SAVELINKFORFAIT.
     */
    protected static final String NAV_ADMIN_SAVELINKFORFAIT = "/saveForfait";

    /**
     * The constant NAV_ADMIN_DELETELINKFORFAIT.
     */
    protected static final String NAV_ADMIN_DELETELINKFORFAIT = "/deleteForfait";

    /**
     * The constant NAV_ADMIN_VALIDATIONSABSENCES.
     */
    protected static final String NAV_ADMIN_VALIDATIONSABSENCES = "/validationsAbsences";

    /**
     * The constant NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES.
     */
    protected static final String NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES = "/validationsMultiplesAbsences";

    /**
     * The constant NAV_ADMIN_CONSULTATIONABSENCES.
     */
    protected static final String NAV_ADMIN_CONSULTATIONABSENCES = "/consultationAbsences";

    /**
     * The constant NAV_ADMIN_ANNULATIONABSENCES.
     */
    protected static final String NAV_ADMIN_ANNULATIONABSENCES = "/annulationAbsences";

    /**
     * The constant NAV_ADMIN_GESTIONAPPLICATION.
     */
    protected static final String NAV_ADMIN_GESTIONAPPLICATION = "/gestionApplication";

    /**
     * The constant NAV_ADMIN_SAVETVA.
     */
    protected static final String NAV_ADMIN_SAVETVA = "/saveTVA";

    /**
     * The constant NAV_ADMIN_SAVEBUDGET.
     */
    protected static final String NAV_ADMIN_SAVEBUDGET = "/saveBudget";

    /**
     * The constant NAV_ADMIN_SAVETVAFORTYPEFRAIS.
     */
    protected static final String NAV_ADMIN_SAVETVAFORTYPEFRAIS = "/saveTVAForTypeFrais";

    /**
     * The constant NAV_ADMIN_VALIDATIONSRA.
     */
    protected static final String NAV_ADMIN_VALIDATIONSRA = "/validationsRA";

    /**
     * The constant NAV_ADMIN_VALIDATIONSMULTIPLESRA.
     */
    protected static final String NAV_ADMIN_VALIDATIONSMULTIPLESRA = "/validationsMultiplesRA";
    /**
     * The constant NAV_ADMIN_CONSULTATIONRA.
     */
    protected static final String NAV_ADMIN_CONSULTATIONRA = "/consultationRA";

    /**
     * The constant NAV_ADMIN_LISTESOUMISRA.
     */
    protected static final String NAV_ADMIN_LISTESOUMISRA = "/listeSoumissionRA";

    /**
     * The constant NAV_ADMIN_VALIDE.
     */
    protected static final String NAV_ADMIN_VALIDE = "/VA";

    /**
     * The constant NAV_ADMIN_REFUSE.
     */
    protected static final String NAV_ADMIN_REFUSE = "/RE";

    /**
     * The constant NAV_ADMIN_ANNULE.
     */
    protected static final String NAV_ADMIN_ANNULE = "/AN";

    /**
     * The constant NAV_ADMIN_EN_COURS.
     */
    protected static final String NAV_ADMIN_EN_COURS = "/CO";

    /**
     * The constant NAV_ADMIN_TERMINEE.
     */
    protected static final String NAV_ADMIN_TERMINEE = "/TE";

    /**
     * The constant NAV_ADMIN_SOUMIS.
     */
    protected static final String NAV_ADMIN_SOUMIS = "/SO";

    /**
     * The constant NAV_ADMIN_DOWNLOAD.
     */
    protected static final String NAV_ADMIN_DOWNLOAD = "/download";

    /**
     * The constant NAV_ADMIN_GESTION_TABLEURS.
     */
    protected static final String NAV_ADMIN_GESTION_TABLEURS = "/gestionTableurs";

    /**
     * The constant NAV_ADMIN_TABLEURS.
     */
    protected static final String NAV_ADMIN_TABLEURS = "/tableurExcel";

    /**
     * The constant NAV_ADMIN_DEPLACEMENTS.
     */
    protected static final String NAV_ADMIN_DEPLACEMENTS = "/validationDeplacements";

    /**
     * The constant NAV_ADMIN_ANNULATION_DEPLACEMENTS.
     */
    protected static final String NAV_ADMIN_ANNULATION_DEPLACEMENTS = "/annulationDeplacements";

    /**
     * The constant NAV_ADMIN_CONSULTATION_DEPLACEMENTS.
     */
    protected static final String NAV_ADMIN_CONSULTATION_DEPLACEMENTS = "/consultationDeplacements";

    /**
     * The constant NAV_EDIT_DEPLACEMENT.
     */
    protected static final String NAV_EDIT_DEPLACEMENT = "Deplacements/editDeplacement";

    /**
     * The constant NAV_SAVE_DEPLACEMENT.
     */
    protected static final String NAV_SAVE_DEPLACEMENT = "Deplacements/saveDeplacement";

    /**
     * The constant NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS.
     */
    protected static final String NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS = "/validationsMultiplesDeplacements";

    /**
     * The constant NAV_ADMIN_EDIT_MISSIONCLIENTE.
     */
    protected static final String NAV_ADMIN_EDIT_MISSIONCLIENTE = "/editMissionCliente";

    /**
     * The constant NAV_ADMIN_SAVE_MISSIONCLIENTE.
     */
    protected static final String NAV_ADMIN_SAVE_MISSIONCLIENTE = "/saveMissionCliente";

    /**
     * The constant NAV_ADMIN_CLIENTS.
     */
    protected static final String NAV_ADMIN_CLIENTS = "/clients";

    /**
     * The constant NAV_ADMIN_SAVE_CLIENT.
     */
    protected static final String NAV_ADMIN_SAVE_CLIENT = "/saveClient";

    /**
     * The constant NAV_ADMIN_EDIT_CLIENT.
     */
    protected static final String NAV_ADMIN_EDIT_CLIENT = "/editClient";

    /**
     * The constant NAV_ADMIN_DELETE_CLIENT.
     */
    protected static final String NAV_ADMIN_DELETE_CLIENT = "/deleteClient";

    /**
     * The constant NAV_ADMIN_EDIT_CONTACTCLIENT.
     */
    protected static final String NAV_ADMIN_EDIT_CONTACTCLIENT = "/editContactClient";

    /**
     * The constant NAV_ADMIN_SAVE_CONTACTCLIENT.
     */
    protected static final String NAV_ADMIN_SAVE_CONTACTCLIENT = "/saveContactClient";

    /**
     * The constant NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT.
     */
    protected static final String NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT = "/addDefaultContactClient";

    /**
     * The constant NAV_ADMIN_DELETE_CONTACTCLIENT.
     */
    protected static final String NAV_ADMIN_DELETE_CONTACTCLIENT = "/deleteContactClient";

    /**
     * The constant NAV_ADMIN_VALID_ODM.
     */
    protected static final String NAV_ADMIN_VALID_ODM = "/validationODM";

    /**
     * The constant NAV_ADMIN_VALIDATIONSMULTIPLES_ODM.
     */
    protected static final String NAV_ADMIN_VALIDATIONSMULTIPLES_ODM = "/validationsMultiplesODM";

    /**
     * The constant NAV_ODM.
     */
    protected static final String NAV_ODM = "mesOrdresDeMission";

    /**
     * The constant NAV_ODM_ORDRESDEMISSION.
     */
    protected static final String NAV_ODM_ORDRESDEMISSION = "mesOrdresDeMission/ordresDeMission";

    /**
     * The constant NAV_ADMIN_FACTURES.
     */
    protected static final String NAV_ADMIN_FACTURES = "/factures";

    /**
     * The constant NAV_ADMIN_NUMEROTATION_FACTURES.
     */
    protected static final String NAV_ADMIN_NUMEROTATION_FACTURES = "/numeroterFactures";

    /**
     * The constant NAV_ADMIN_SOUMISSIONS_FACTURES.
     */
    protected static final String NAV_ADMIN_SOUMISSIONS_FACTURES = "/soumettreFactures";

    /**
     * The constant NAV_ADMIN_FACTURES_REFUSEES.
     */
    protected static final String NAV_ADMIN_FACTURES_REFUSEES = "/refuserFactures";

    /**
     * The constant NAV_ADMIN_VALIDATIONFACTURES_MOIS.
     */
    protected static final String NAV_ADMIN_VALIDATION_FACTURES = "/validerFactures";

    /**
     * The constant NAV_ADMIN_FORCER_FACTURES.
     */
    protected static final String NAV_ADMIN_FORCER_FACTURES = "/forcerFactures";

    /**
     * The constant NAV_ADMIN_CREATIONFACTURE_VIERGE.
     */
    protected static final String NAV_ADMIN_CREATIONFACTURE_VIERGE = "/creationFactureVierge";

    /**
     * The constant NAV_ADMIN_GET_CONSULTANT.
     */
    protected static final String NAV_ADMIN_GET_CONSULTANT = "/getConsultant";

    /**
     * The constant NAV_ADMIN_CREATIONFACTURES_MOIS.
     */
    protected static final String NAV_ADMIN_CREATIONFACTURES_MOIS = "/creationFacturesMois";

    /**
     * The constant NAV_ADMIN_EDITFACTURE.
     */
    protected static final String NAV_ADMIN_EDITFACTURE = "/editFacture";

    /**
     * The constant NAV_ADMIN_SAVEFACTURE.
     */
    protected static final String NAV_ADMIN_SAVEFACTURE = "/saveFacture";

    /**
     * The constant NAV_ADMIN_DELETEFRAIS.
     */
    protected static final String NAV_ADMIN_DELETEFRAIS = "/deleteFrais";

    /**
     * The constant NAV_ADMIN_DELETEFACTURE.
     */
    protected static final String NAV_ADMIN_DELETEFACTURE = "/deleteFacture";

    /**
     * The constant NAV_ADMIN_VISUALISEZ.
     */
    protected static final String NAV_ADMIN_VISUALISEZ = "/visualisez";

    /**
     * The constant NAV_ADMIN_AFFICHERPDF.
     */
    protected static final String NAV_ADMIN_AFFICHERPDF = "/afficher";

    /**
     * The constant NAV_ADMIN_SAVE_FACTURE_ARCHIVEE.
     */
    protected static final String NAV_ADMIN_SAVE_FACTURE_ARCHIVEE = "/saveFactureArchivee";

    /**
     * The constant NAV_ADMIN_FACTURES_PRINT.
     */
    protected static final String NAV_ADMIN_FACTURES_PRINT = "/print";

    /**
     * The constant NAV_ADMIN_EDITCOMMANDE.
     */
    protected static final String NAV_ADMIN_EDITCOMMANDE = "/editCommande";

    /**
     * The constant NAV_ADMIN_LOADMISSION.
     */
    protected static final String NAV_ADMIN_LOADMISSION = "/loadMission";

    /**
     * The constant NAV_ADMIN_SAVECOMMANDE.
     */
    protected static final String NAV_ADMIN_SAVECOMMANDE = "/saveCommande";

    /**
     * The constant NAV_ADMIN_DELETECOMMANDE.
     */
    protected static final String NAV_ADMIN_DELETECOMMANDE = "/deleteCommande";

    /**
     * The constant NAV_ADMIN_DELETECOMM.
     */
    protected static final String NAV_ADMIN_DELETECOMM = "/deleteComm";

    /**
     * The constant NAV_ADMIN_NEWCOMMANDE.
     */
    protected static final String NAV_ADMIN_NEWCOMMANDE = "/newCommande";

    /**
     * The constant NAV_ADMIN_GESTION_COMMANDES.
     */
    protected static final String NAV_ADMIN_GESTION_COMMANDES = "/gestionCommandes";

    /**
     * The constant NAV_ADMIN_STOP_COMMANDE.
     */
    protected static final String NAV_ADMIN_STOP_COMMANDE = "/stopCommande";

    /**
     * The constant NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE.
     */
    protected static final String NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE = "/duplicateStoppedCommande";

    /**
     * The constant NAV_TYPE_FACTURABLE
     */
    protected static final String NAV_TYPE_FACTURABLE = "/FRABLE";

    /**
     * The constant NAV_TYPE_FACTURABLE_CSV
     */
    protected static final String NAV_TYPE_FACTURABLE_CSV = "/FRABLECSV";

    /**
     * The constant NAV_TYPE_FACTURABLE_SST_CSV
     */
    protected static final String NAV_TYPE_FACTURABLE_SST_CSV = "/FRABLESSTCSV";

    /**
     * The constant NAV_TYPE_FACTURE_FRAIS
     */
    protected static final String NAV_TYPE_FACTURE_FRAIS = "/FFRAIS";


    /**
     * The constant NAV_TYPE_FACTURE_MAIN
     */
    protected static final String NAV_TYPE_FACTURE_MAIN = "/FMAIN";


    /**
     * The constant NAV_TYPE_FACTURE_FRAIS_CSV
     */
    protected static final String NAV_TYPE_FACTURE_FRAIS_CSV = "/FFRAISCSV";

    /**
     * The constant NAV_TYPE_FACTURE_FRAIS_COLLAB_CSV
     */
    protected static final String NAV_TYPE_FACTURE_FRAIS_COLLAB_CSV = "/FFRAISCOLLABCSV";


    /**
     * The constant NAV_TYPE_FACTURE_MAIN_CSV
     */
    protected static final String NAV_TYPE_FACTURE_MAIN_CSV = "/FMAINCSV";

    /**
     * The constant NAV_TYPE_FACTURE_MAIN_SST_CSV
     */
    protected static final String NAV_TYPE_FACTURE_MAIN_SST_CSV = "/FMAINSSTCSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURABLE
     */
    protected static final String NAME_NAV_TYPE_FACTURABLE = "FRABLE";

    /**
     * The constant NAME_NAV_TYPE_FACTURABLE
     */
    protected static final String NAME_NAV_TYPE_FACTURABLE_CSV = "FRABLECSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURABLE
     */
    protected static final String NAME_NAV_TYPE_FACTURABLE_SST_CSV = "FRABLESSTCSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_FRAIS
     */
    protected static final String NAME_NAV_TYPE_FACTURE_FRAIS = "FFRAIS";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_FRAIS
     */
    protected static final String NAME_NAV_TYPE_FACTURE_FRAIS_CSV = "FFRAISCSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_FRAIS_COLLAB
     */
    protected static final String NAME_NAV_TYPE_FACTURE_FRAIS_COLLAB = "FFRAISCOLLABCSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_MAIN
     */
    protected static final String NAME_NAV_TYPE_FACTURE_MAIN = "FMAIN";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_MAIN_CSV
     */
    protected static final String NAME_NAV_TYPE_FACTURE_MAIN_CSV = "FMAINCSV";

    /**
     * The constant NAME_NAV_TYPE_FACTURE_MAIN_CSV
     */
    protected static final String NAME_NAV_TYPE_FACTURE_MAIN_SST_CSV = "FMAINSSTCSV";


    protected static final String NAV_ADMIN_ADDVISE = "/addvise";
    protected static final String NAV_ADMIN_ADDVISE_HIST = "/addvise-hist";
    protected static final String NAV_ADMIN_SUPPRESSION_MODULE_ADDVISE = "/suppressionModulesAddvise";
    /**
     * The constant NAV_ADMIN_EDIT_MODULE_ADDVISE.
     */
    protected static final String NAV_ADMIN_EDIT_MODULE_ADDVISE = "/editModulesAddvise";
    /**
     * The constant NAV_ADMIN_SAVE_MODULE_ADDVISE.
     */
    protected static final String NAV_ADMIN_SAVE_MODULE_ADDVISE = "/saveModulesAddvise";
    protected static final String NAV_REPONSE_MAIL_ADDVISE = "addviseReponseMail";
    protected static final String NAV_MAIL_EXIST = "/emailExist";
    protected static Logger logger = LogManager.getLogger(AbstractController.class);


    /**
     * The constant NAV_ADMIN_MODULE_DISPONIBILITE.
     */
    protected static final String NAV_ADMIN_MODULE_DISPONIBILITE = "/consultationDisponibilites";


    /**
     * The Log.
     */
    protected Logger log = LogManager.getLogger(getClass());
    /**
     * The Properties.
     */
    @Autowired
    protected Properties properties;

    @Autowired
    private ServletContext servletContext;

    /**
     * chargement de la page selon la request et le model
     *
     * @param request the request
     * @param model   the model
     * @deprecated remplacer l'appel de cette methode par celle ne prenant que le model en
     * parametre et la supprimer quand elle ne sera plus utilisée
     */
    @Deprecated
    protected void init(HttpServletRequest request, Model model) {
        init(model);
    }

    /**
     * peuplement de la navigation dans le model
     *
     * @param model the model
     */

    protected void init(Model model) {

        // add contextPath au model
        model.addAttribute("contextPath", servletContext.getContextPath());

        // add nav entries
        // logout
        model.addAttribute(NAV_LOGOUT, NAV_LOGOUT);
        // page accueil
        model.addAttribute(NAME_NAV_WELCOME, NAV_WELCOME);
        // page du profil
        model.addAttribute(NAME_NAV_GESTIONPROFIL, NAV_GESTIONPROFIL);
        // page de reset de mdp si oublié
        model.addAttribute(NAME_NAV_GESTIONMAIL, NAV_GESTIONMAIL);
        // page timesheet
        model.addAttribute(NAME_NAV_TIMESHEETS, NAV_TIMESHEETS);
        // page notes de frais
        model.addAttribute(NAME_NAV_FRAIS, NAV_NOTEDEFRAIS);
        // page de soumission des avances de frais
        model.addAttribute(NAME_NAV_FRAIS_SOUMISSIONAVANCE, NAV_FRAIS_SOUMISSIONAVANCE);


        // page des absences
        model.addAttribute(NAME_NAV_ABSENCE, NAV_ABSENCE);

        model.addAttribute(NAME_NAV_FRAIS_NOTESDEFRAIS, NAV_FRAIS_NOTESDEFRAIS);

        model.addAttribute(NAME_NAV_ADMIN_FACTURES_VALIDEES, NAV_ADMIN_FACTURES_VALIDEES);

        model.addAttribute(NAME_NAV_FRAIS_NEW_JUSTIF, NAV_FRAIS_NEW_JUSTIF);

        model.addAttribute(NAME_NAV_FRAIS_DELETE_JUSTIF, NAV_FRAIS_DELETE_JUSTIF);

        model.addAttribute(NAME_NAV_FRAIS_RETIRER_JUSTIF, NAV_FRAIS_RETIRER_JUSTIF);

        model.addAttribute(NAME_NAV_FRAIS_SAVE_JUSTIF, NAV_FRAIS_SAVE_JUSTIF);

        model.addAttribute(NAME_NAV_FRAIS_ASSOCIATION_JUSTIFS, NAV_FRAIS_ASSOCIATION_JUSTIFS);
        // suppression d'un frais
        model.addAttribute(NAME_NAV_DELETE_FRAIS, NAV_FRAIS_DELETE_FRAIS);
        // affichage de la piece jointe
        model.addAttribute(NAME_NAV_GETIMAGE_FRAIS, NAV_GETIMAGE_FRAIS);
        // modification d'un frais
        model.addAttribute(NAME_NAV_EDIT_FRAIS, NAV_FRAIS_EDIT_FRAIS);
        // sauvegarde d'un nouveau frais
        model.addAttribute(NAME_NAV_SAVE_FRAIS, NAV_FRAIS_SAVE_FRAIS);
        // recuperation des mission
        model.addAttribute(NAME_NAV_TIMESHEETS_MISSION, NAV_TIMESHEETS_MISSION);
        // récuperation des types d'absences
        model.addAttribute(NAME_NAV_TIMESHEETS_TYPEABSENCE, NAV_TIMESHEETS_TYPEABSENCE);
        // envoi d'un mail à l'administrateur
        model.addAttribute(NAME_NAV_MAIL_ADMIN, NAV_MAIL_ADMIN);

        model.addAttribute(NAME_NAV_ADMIN, NAV_ADMIN);
        model.addAttribute(NAME_NAV_ADMIN_TRAVAILLEURS, NAV_ADMIN_TRAVAILLEURS);

        model.addAttribute(NAME_NAV_ADMIN_COLLABORATEURS, NAV_ADMIN_COLLABORATEURS);

        model.addAttribute(NAME_NAV_ADMIN_EDITCOLLABORATEUR, NAV_ADMIN_EDITCOLLABORATEUR);

        model.addAttribute(NAME_NAV_ADMIN_SAVECOLLABORATEUR, NAV_ADMIN_SAVECOLLABORATEUR);

        model.addAttribute(NAME_NAV_ADMIN_DELETECOLLABORATEUR, NAV_ADMIN_DELETECOLLABORATEUR);

        model.addAttribute(NAME_NAV_ADMIN_SOUSTRAITANTS, NAV_ADMIN_SOUSTRAITANTS);

        model.addAttribute(NAME_NAV_ADMIN_EDITSOUSTRAITANT, NAV_ADMIN_EDITSOUSTRAITANTS);

        model.addAttribute(NAME_NAV_ADMIN_SAVESOUSTRAITANT, NAV_ADMIN_SAVESOUSTRAITANT);

        model.addAttribute(NAME_NAV_ADMIN_DELETESOUSTRAITANT, NAV_ADMIN_DELETESOUSTRAITANT);

        model.addAttribute(NAME_NAV_ADMIN_NOTEDEFRAIS, NAV_ADMIN_NOTEDEFRAIS);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSMULTIPLESASF, NAV_ADMIN_VALIDATIONSMULTIPLESASF);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONNOTEDEFRAIS, NAV_ADMIN_VALIDATIONNOTEDEFRAIS);

        model.addAttribute(NAME_NAV_ADMIN_REFUSNOTEDEFRAIS, NAV_ADMIN_REFUSNOTEDEFRAIS);

        model.addAttribute(NAME_NAV_ADMIN_EDITMISSION, NAV_ADMIN_EDITMISSION);

        model.addAttribute(NAME_NAV_ADMIN_SAVEMISSION, NAV_ADMIN_SAVEMISSION);

        model.addAttribute(NAME_NAV_ADMIN_COLLABORATEURS_TYPE_MISSION, NAV_ADMIN_COLLABORATEURS_TYPE_MISSION);

        model.addAttribute(NAME_NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE, NAV_ADMIN_COLLABORATEURS_OUTILS_INTERNE);

        model.addAttribute(NAME_NAV_ADMIN_DELETEMISSION, NAV_ADMIN_DELETEMISSION);

        model.addAttribute(NAME_NAV_ADMIN_EDITLINKFORFAIT, NAV_ADMIN_EDITLINKFORFAIT);

        model.addAttribute(NAME_NAV_ADMIN_SAVELINKFORFAIT, NAV_ADMIN_SAVELINKFORFAIT);

        model.addAttribute(NAME_NAV_ADMIN_DELETELINKFORFAIT, NAV_ADMIN_DELETELINKFORFAIT);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSABSENCES, NAV_ADMIN_VALIDATIONSABSENCES);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES, NAV_ADMIN_VALIDATIONSMULTIPLESABSENCES);

        model.addAttribute(NAME_NAV_ADMIN_CONSULTATIONABSENCES, NAV_ADMIN_CONSULTATIONABSENCES);

        model.addAttribute(NAME_NAV_ADMIN_ANNULATIONABSENCES, NAV_ADMIN_ANNULATIONABSENCES);

        model.addAttribute(NAME_NAV_ADMIN_GESTIONAPPLICATION, NAV_ADMIN_GESTIONAPPLICATION);

        model.addAttribute(NAME_NAV_ADMIN_SAVETVAFORTYPEFRAIS, NAV_ADMIN_SAVETVAFORTYPEFRAIS);

        model.addAttribute(NAME_NAV_ADMIN_SAVETVA, NAV_ADMIN_SAVETVA);

        model.addAttribute(NAME_NAV_ADMIN_SAVEBUDGET, NAV_ADMIN_SAVEBUDGET);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSRA, NAV_ADMIN_VALIDATIONSRA);

        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSMULTIPLESRA, NAV_ADMIN_VALIDATIONSMULTIPLESRA);

        model.addAttribute(NAME_NAV_ADMIN_CONSULTATIONRA, NAV_ADMIN_CONSULTATIONRA);

        model.addAttribute(NAME_NAV_ADMIN_LISTESOUMISRA, NAV_ADMIN_LISTESOUMISRA);

        model.addAttribute(NAME_NAV_ADMIN_VALIDE, NAV_ADMIN_VALIDE);

        model.addAttribute(NAME_NAV_ADMIN_REFUSE, NAV_ADMIN_REFUSE);

        model.addAttribute(NAME_NAV_ADMIN_ANNULE, NAV_ADMIN_ANNULE);

        model.addAttribute(NAME_NAV_ADMIN_SOUMIS, NAV_ADMIN_SOUMIS);

        model.addAttribute(NAME_NAV_ADMIN_DOWNLOAD, NAV_ADMIN_DOWNLOAD);

        model.addAttribute(NAME_NAV_ADMIN_GESTION_TABLEURS, NAV_ADMIN_GESTION_TABLEURS);

        model.addAttribute(NAME_NAV_ADMIN_TABLEURS, NAV_ADMIN_TABLEURS);

        model.addAttribute(NAME_NAV_ADMIN_EDITTRAVAILLEUR, NAV_ADMIN_EDITTRAVAILLEUR);

        model.addAttribute(NAME_NAV_ADMIN_NEWTRAVAILLEUR, NAV_ADMIN_NEWTRAVAILLEUR);

        model.addAttribute(NAME_NAV_ADMIN_CREATION_PDF_MULTIPLES_RA, NAV_ADMIN_CREATION_PDF_MULTIPLES_RA);


        // page des commandes

        model.addAttribute(NAME_NAV_ADMIN_TERMINEE, NAV_ADMIN_TERMINEE);
        model.addAttribute(NAME_NAV_ADMIN_EN_COURS, NAV_ADMIN_EN_COURS);
        // page des deplacements
        model.addAttribute(NAME_NAV_ADMIN_DEPLACEMENTS, NAV_ADMIN_DEPLACEMENTS);
        model.addAttribute(NAME_NAV_ADMIN_ANNULATION_DEPLACEMENTS, NAV_ADMIN_ANNULATION_DEPLACEMENTS);
        model.addAttribute(NAME_NAV_EDIT_DEPLACEMENT, NAV_EDIT_DEPLACEMENT);
        model.addAttribute(NAME_NAV_SAVE_DEPLACEMENT, NAV_SAVE_DEPLACEMENT);
        model.addAttribute(NAME_NAV_ADMIN_CONSULTATION_DEPLACEMENTS, NAV_ADMIN_CONSULTATION_DEPLACEMENTS);
        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS, NAV_ADMIN_VALIDATIONSMULTIPLESDEPLACEMENTS);

        //Ordres de mission
        model.addAttribute(NAME_NAV_ADMIN_EDIT_MISSIONCLIENTE, NAV_ADMIN_EDIT_MISSIONCLIENTE);
        model.addAttribute(NAME_NAV_ADMIN_SAVE_MISSIONCLIENTE, NAV_ADMIN_SAVE_MISSIONCLIENTE);

        //Clients
        model.addAttribute(NAME_NAV_ADMIN_CLIENTS, NAV_ADMIN_CLIENTS);
        model.addAttribute(NAME_NAV_ADMIN_EDIT_CLIENT, NAV_ADMIN_EDIT_CLIENT);
        model.addAttribute(NAME_NAV_ADMIN_SAVECLIENT, NAV_ADMIN_SAVE_CLIENT);
        model.addAttribute(NAME_NAV_ADMIN_DELETECLIENT, NAV_ADMIN_DELETE_CLIENT);

        model.addAttribute(NAME_NAV_ADMIN_EDIT_CONTACTCLIENT, NAV_ADMIN_EDIT_CONTACTCLIENT);
        model.addAttribute(NAME_NAV_ADMIN_SAVE_CONTACTCLIENT, NAV_ADMIN_SAVE_CONTACTCLIENT);
        model.addAttribute(NAME_NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT, NAV_ADMIN_ADD_DEFAULTCONTACTCLIENT);
        model.addAttribute(NAME_NAV_ADMIN_DELETE_CONTACTCLIENT, NAV_ADMIN_DELETE_CONTACTCLIENT);

        model.addAttribute(NAME_NAV_ADMIN_VALID_ODM, NAV_ADMIN_VALID_ODM);
        model.addAttribute(NAME_NAV_ADMIN_VALIDATIONSMULTIPLES_ODM, NAV_ADMIN_VALIDATIONSMULTIPLES_ODM);
        model.addAttribute(NAME_NAV_ODM, NAV_ODM);
        model.addAttribute(NAME_NAV_ODM_ORDRESDEMISSION, NAV_ODM_ORDRESDEMISSION);

        //Factures
        model.addAttribute(NAME_NAV_ADMIN_FACTURES, NAV_ADMIN_FACTURES);
        model.addAttribute(NAME_NAV_ADMIN_SOUMISSIONS_FACTURES, NAV_ADMIN_SOUMISSIONS_FACTURES);
        model.addAttribute(NAME_NAV_ADMIN_FACTURES_REFUSEES, NAV_ADMIN_FACTURES_REFUSEES);
        model.addAttribute(NAME_NAV_ADMIN_VALIDATION_FACTURES, NAV_ADMIN_VALIDATION_FACTURES);
        model.addAttribute(NAME_NAV_ADMIN_NUMEROTATION_FACTURES, NAV_ADMIN_NUMEROTATION_FACTURES);
        model.addAttribute(NAME_NAV_ADMIN_FORCER_FACTURES, NAV_ADMIN_FORCER_FACTURES);
        model.addAttribute(NAME_NAV_ADMIN_CREATIONFACTURE_VIERGE, NAV_ADMIN_CREATIONFACTURE_VIERGE);
        model.addAttribute(NAME_NAV_ADMIN_GET_CONSULTANT, NAME_NAV_ADMIN_GET_CONSULTANT);
        model.addAttribute(NAME_NAV_ADMIN_CREATIONFACTURES_MOIS, NAV_ADMIN_CREATIONFACTURES_MOIS);
        model.addAttribute(NAME_NAV_ADMIN_EDITFACTURE, NAV_ADMIN_EDITFACTURE);
        model.addAttribute(NAME_NAV_ADMIN_SAVEFACTURE, NAV_ADMIN_SAVEFACTURE);
        model.addAttribute(NAME_NAV_ADMIN_DELETEFRAIS, NAV_ADMIN_DELETEFRAIS);
        model.addAttribute(NAME_NAV_ADMIN_VISUALISEZ, NAV_ADMIN_VISUALISEZ);
        model.addAttribute(NAME_NAV_ADMIN_AFFICHERPDF, NAV_ADMIN_AFFICHERPDF);
        model.addAttribute(NAME_NAV_ADMIN_SAVE_FACTURE_ARCHIVEE, NAV_ADMIN_SAVE_FACTURE_ARCHIVEE);
        model.addAttribute(NAME_NAV_ADMIN_DELETEFACTURE, NAV_ADMIN_DELETEFACTURE);
        model.addAttribute(NAME_NAV_ADMIN_FACTURES_PRINT, NAV_ADMIN_FACTURES_PRINT);


        model.addAttribute(NAME_NAV_ADMIN_EDITCOMMANDE, NAV_ADMIN_EDITCOMMANDE);
        model.addAttribute(NAME_NAV_ADMIN_LOADMISSION, NAV_ADMIN_LOADMISSION);
        model.addAttribute(NAME_NAV_ADMIN_LOADMISSION, NAV_ADMIN_LOADMISSION);
        model.addAttribute(NAME_NAV_ADMIN_DELETECOMMANDE, NAV_ADMIN_DELETECOMMANDE);
        model.addAttribute(NAME_NAV_ADMIN_DELETECOMM, NAV_ADMIN_DELETECOMM);
        model.addAttribute(NAME_NAV_ADMIN_SAVECOMMANDE, NAV_ADMIN_SAVECOMMANDE);
        model.addAttribute(NAME_NAV_ADMIN_NEWCOMMANDE, NAV_ADMIN_NEWCOMMANDE);
        model.addAttribute(NAME_NAV_ADMIN_GESTION_COMMANDES, NAV_ADMIN_GESTION_COMMANDES);
        model.addAttribute(NAME_NAV_ADMIN_STOP_COMMANDE, NAV_ADMIN_STOP_COMMANDE);
        model.addAttribute(NAME_NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE, NAV_ADMIN_DUPLICATE_STOPPED_COMMANDE);

        model.addAttribute(NAME_NAV_ADMIN_MODULE_ADDVISE, NAV_ADMIN_ADDVISE);
        model.addAttribute(NAME_NAV_ADMIN_SUPPRESSION_MODULE_ADDVISE, NAV_ADMIN_SUPPRESSION_MODULE_ADDVISE);
        model.addAttribute(NAME_NAV_ADMIN_EDIT_MODULE_ADDVISE, NAV_ADMIN_EDIT_MODULE_ADDVISE);
        model.addAttribute(NAME_NAV_ADMIN_SAVE_MODULE_ADDVISE, NAV_ADMIN_SAVE_MODULE_ADDVISE);

        model.addAttribute(NAME_NAV_TYPE_FACTURABLE, NAV_TYPE_FACTURABLE);
        model.addAttribute(NAME_NAV_TYPE_FACTURABLE_CSV, NAV_TYPE_FACTURABLE_CSV);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_FRAIS, NAV_TYPE_FACTURE_FRAIS);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_FRAIS_CSV, NAV_TYPE_FACTURE_FRAIS_CSV);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_FRAIS_COLLAB, NAV_TYPE_FACTURE_FRAIS_COLLAB_CSV);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_MAIN, NAV_TYPE_FACTURE_MAIN);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_MAIN_CSV, NAV_TYPE_FACTURE_MAIN_CSV);

        model.addAttribute(NAME_NAV_TYPE_FACTURABLE_SST_CSV, NAV_TYPE_FACTURABLE_SST_CSV);
        model.addAttribute(NAME_NAV_TYPE_FACTURE_MAIN_SST_CSV, NAV_TYPE_FACTURE_MAIN_SST_CSV);
        // Module dispo
        model.addAttribute(NAME_NAV_ADMIN_MODULE_DISPONIBILITE, NAV_ADMIN_MODULE_DISPONIBILITE);
        model.addAttribute(NAME_NAV_EMAIL_EXIST, NAV_MAIL_EXIST);
    }

    /**
     * convertisseur de listes en string
     *
     * @param strings the strings
     * @return string
     */
    protected String listToStringConverter(List<String> strings) {
        if (strings == null || strings.isEmpty()) {
            return null;
        }

        StringBuilder result = new StringBuilder();
        for (String s : strings) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(s);
        }
        return result.toString();
    }

    /**
     * Construit l'entête de la réponse Http en fonction du type de fichier passé en paramètre
     *
     * @param extension the extension
     * @param response  the response
     * @param lFile     the l file
     * @throws IOException the io exception
     */
    protected void httpResponseConstruct(String extension, HttpServletResponse response, File lFile) throws IOException {
        if (StringUtils.equals("jpg", extension) || StringUtils.equals("jpeg", extension) || StringUtils.equals("png", extension) || StringUtils.equals("bmp", extension)) {
            response.setContentType("image/" + extension);
        } else {
            response.setContentType("application/" + extension);
        }
        response.setContentLength((int)lFile.length());
        response.setHeader("Content-Disposition", "inline; filename=" + lFile.getName());

        //--- Copie du fichier dans la réponse HTTP
        try (FileInputStream fileInputStream = new FileInputStream(lFile)) {
            FileCopyUtils.copy(fileInputStream, response.getOutputStream());
        } catch (FileNotFoundException e) {
            throw e;
        }
    }

}
