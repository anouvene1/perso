/*
 * ©Amiltone 2017
 */

function Colors(pListColors) {
    var that = this;
    this.list = pListColors;
    this.currentListNum = -1;
    this.defaultColor = "#9C9C9C";
    this.addItemToColor = function (pName) {
        that.currentListNum++;
        if (that.currentListNum <= that.list.length - 1) {
            that.list[that.currentListNum].name = pName;
        }
    };
    this.addList = function (pList, itemName) { //"itemName" représente le nom de la propriété de l'objet Json "pList"
        $.each(pList, function (i, item) {
            that.addItemToColor(item[itemName]);
        });
    };
    this.getValue = function (pName) {
        if (pName == null) return that.defaultColor;
        var item = $.grep(that.list, function (e) {
            return e.name == pName
        });
        if (item[0] != null && item[0].hexa != null) {
            return item[0].hexa;
        } else {
            return that.defaultColor;
        }
    };
}

function getColorsList(listName) {
    var lists = {
        "absenceEtat": [{hexa: "#C2C1C2", name: "BR"},
            {hexa: "#2F57A3", name: "SO"},
            {hexa: "#293286", name: "VA"},
            {hexa: "#0B132B", name: "RE"},
            {hexa: "#3C3F47", name: "AN"}
        ],
        "mission": [{hexa: "#CE3089", name: ""},
            {hexa: "#6A4B7F", name: ""},
            {hexa: "#6d06ff", name: ""},
            {hexa: "#352640", name: ""},
            {hexa: "#e568c8", name: ""}
        ],	// nuance de violet
        "férié": "#990000",
        "absenceDefault": "#30C930",
        "eventSelection": "#4A4A4A",
        "vide": "#C3D9E0"
    };
    if (lists[listName] != null)
        return lists[listName];
    else
        return "nom inconnu";
}

// Récupère tous les types d'absence au format JSON
function getListTypeAbsence() {
    var lResListTypeAbsence = "";
    $.ajax({
        url: 'Absences/mesAbsences/getAllTypeAbsence',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lResListTypeAbsence = result;
        },
        error: function (xhr, status, error) {
            lResListTypeAbsence = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResListTypeAbsence;
}

//Récupère tous les états absence
function getEtatsAbsence() {
    var lResListEtatsAbsence = "";
    $.ajax({
        url: 'Absences/mesAbsences/getEtatsAbsence',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lResListEtatsAbsence = result;
        },
        error: function (xhr, status, error) {
            lResListEtatsAbsence = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResListEtatsAbsence;
}

// Pré-remplir la mission par défaut pour l'utilisateur connecté sur la plage choisie
function missionPreFill(pDateDebut, pDateFin, withSaturday) {
    var lResListTypeAbsence = "";
    $.ajax({
        url: 'Rapport-Activites/mesTimesheets/Event/Mission/preFill/' + pDateDebut + '/' + pDateFin + '/' + withSaturday,
        dataType: 'text',
        type: 'GET',
        async: false,
        success: function (result) {
            lResListTypeAbsence = result;
        },
        error: function (xhr, status, error) {
            lResListTypeAbsence = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResListTypeAbsence;
}

// Récupère tous les links evenements non terminés pour l'utilisateur connecté, au format Json
function getListEvents(pStart, pEnd) {
    var lRes = "";
    $.ajax({
        url: 'Rapport-Activites/mesTimesheets/Event/getEvents/' + pStart.format() + '/' + pEnd.format(),
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            lRes = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

// Récupère toutes les absences à l'état brouillon
function getAbsenceBrouillon() {
    var lRes = "";

    $.ajax({
        url: 'Absences/getAbsenceBrouillon',
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            lRes = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

/**
 * Validate absence(s) request treatment
 * Async refresh timesheet after absences validation
 * @param pListIdAbsence List of absences ID
 * @param comment Absence comment
 */
function updateAbsenceBrouillon(pListIdAbsence, comment) {
    // Error message init
    $('#messageUtilisateurErreur p').empty().hide();

    // Init the loader
    $(".fixed-table-loading").find("img").remove();
    $(".fixed-table-loading").hide();

    // Spinner
    var $loader = $("<img id=\"submitButtonLoadingGif\" src=\"resources/gif/load.gif\" alt=\"Charqement ...\"/>");

    $.ajax({
        url: 'Absences/updateAbsenceBrouillon',
        type: 'POST',
        data: {
            "listId": JSON.stringify(pListIdAbsence),
            "comment": comment
        },
        beforeSend: function() {
            // Load spinner
            $(".fixed-table-loading").append($loader);
            $(".fixed-table-loading").show();
        },
        success: function (result) {
            var errorMsg = result;

            if (errorMsg) {
                setMessageUtilisateur($('#messageUtilisateurErreur'), errorMsg);
            } else {
                $('#messageUtilisateurErreur').hide();

                // var messageUtilisateur = $('#messageUtilisateur');
                setMessageUtilisateur($('#messageUtilisateur'), "La demande de validation des absences a bien été prise en compte.");

                //Envoyer un mail de notification aux responsables
                envoiMailDemandeAbsences(pListIdAbsence, comment);
            }

            // To reload FullCalendar
            $('#calendar').fullCalendar('refetchEvents');

            // Close modal - Valid absences
            $('#modalValidAbsences').modal('hide');
        },
        error: function (xhr, status, error) {
            setMessageUtilisateur($('#messageUtilisateurErreur'), "Une erreur est survenue.");
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });
}

/**
 * Create, update, or delete missions, absences by AJAX
 *
 * @param jsonData Data request
 * @param url The URI ajax treatment
 */
function updateSelection(jsonData, url) {
    // Init the loader
    $(".fixed-table-loading").find("img").remove();
    $(".fixed-table-loading").hide();

    // Spinner
    var $loader = $("<img src=\"resources/gif/load.gif\" alt=\"Charqement ...\"/>");

    $.ajax({
        url: url,
        type: 'POST',
        contentType: 'application/json',
        dataType: 'text',
        data: JSON.stringify(jsonData),
        // Launch the spinner loader
        beforeSend: function() {
            $(".fixed-table-loading").append($loader);
            $(".fixed-table-loading").show();
        },
        success: function (data) {
            // Send a notification email to managers
            setMessageUtilisateur($('#messageUtilisateur'), data);

            // To reload FullCalendar
            $('#calendar').fullCalendar('refetchEvents');

            // Close modal
            $('#modalEdit').modal('hide');
        },
        error: function (error) {
            setMessageUtilisateur($('#messageUtilisateur'), error);
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });
}

// Envoi de mail asynchrone aux responsables après demande de validation des absences par le collaborateur
function envoiMailDemandeAbsences(listIdAbsence, comment) {
    $.ajax({
        url: 'Absences/sendMailAbsenceSubmission',
        type: 'POST',
        data: {
            "listId": JSON.stringify(listIdAbsence),
            "comment": comment
        }
    })
}

function delEvent(pListId) {
    var lRes = "";
    //var data =  $.toJSON( pListIdAbsence );
    $.ajax({
        url: 'Absences/updateAbsenceBrouillon',
        type: 'POST',
        data: {"listId": JSON.stringify(pListId)},
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            lRes = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

function getListEventsAjaxRequest(pStart, pEnd) {

    return {
        url: 'Rapport-Activites/mesTimesheets/Event/getEvents/' + pStart.format() + '/' + pEnd.format(),
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        async: false
    };
}

function getListHolidays(pStart, pEnd) {
    var lResList = "";
    $.ajax({
        url: 'Rapport-Activites/mesTimesheets/Event/getHolidays/' + pStart.format() + '/' + pEnd.format(),
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        async: false,
        success: function (result) {
            $.each(result, function (key, event) {
                event["title"] = "férié";
                event["ferie"] = "férié";
                event["nbFrais"] = null;

            });
            lResList = result;
        },
        error: function (xhr, status, error) {
            lResList = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResList;

}

function getTimeSheetIsEditable(date) {
    var res = "";
    $.ajax({
        url: 'Rapport-Activites/timesheetIsEditable/' + date.format(),
        type: 'GET',
        async: false,
        success: function (result) {
            res = result;
        },
        error: function (xhr, status, error) {
            setMessageUtilisateur($('#messageUtilisateur'), error);
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return res;

}

function getTimeSheetIsPresent(date) {
    var res = "";
    $.ajax({
        url: 'Rapport-Activites/timesheetIsPresent/' + date.format(),
        type: 'GET',
        async: false,
        success: function (result) {
            res = result;
        },
        error: function (xhr, status, error) {
            setMessageUtilisateur($('#messageUtilisateur'), error);
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return res;

}

function getListHolidaysAsAjaxRequest(pStart, pEnd) {

    return {
        url: 'Rapport-Activites/mesTimesheets/Event/getHolidays/' + pStart.format() + '/' + pEnd.format(),
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        async: false
    };

}
// Récupère tous les missions en cours pour l'utilisateur courant au format Json
function getListMissions(pMois, pAnnee) {
    if (pMois == undefined) {
        var date_jour = new Date();
        pMois = date_jour.getMonth() + 1;
    }

    if (pAnnee == undefined) {
        var date_jour = new Date();
        pAnnee = date_jour.getFullYear();
    }

    var lResListMissions = "";
    $.ajax({
        url: 'Rapport-Activites/mesTimeSheets/getMissions',
        datatype: 'json',
        type: 'POST',
        async: false,
        data: "mois=" + pMois + "&annee=" + pAnnee,
        success: function (result) {
            // ne pas oublier de parser le résultat car comme le contenttype n'est pas application/json,
            // le résultat en chaîne de caractère n'est pas parsé pour devenir un objet Javascript
            lResListMissions = JSON.parse(result);
        },
        error: function (xhr, status, error) {
            lResListMissions = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResListMissions;
}

function addCustomClassForEventPM(pEvent, pElement, pListFullCalendarEvent) {
    var lListFullCalendarEvent = pListFullCalendarEvent;
    var lEventStartAsMoment = moment(pEvent.start);
    var lHours = lEventStartAsMoment.hours();
    var lListEventsAMOnTheSameDays;

    // Si c'est un évenement de l'après midi
    if (lHours >= 12) {
        //On récupère les évenements qui sont le matin ET le mêm jour que l'event passé en paramètre
        lListEventsAMOnTheSameDays = $.grep(lListFullCalendarEvent, function (e) {
            var lTmpEventStartAsMoment = moment(e.start);

            return ( lTmpEventStartAsMoment.hours() < 11
            && lEventStartAsMoment.isSame(lTmpEventStartAsMoment, 'day') );
        });
        //SI il y en a pas, on assigne le class custom à l'élement en cours
        if (lListEventsAMOnTheSameDays.length == 0) {
            pElement.addClass('eventPMWithCustomCSS');
        }
    }
}

function findByItemNameNotNullInListEvents(pList, lItemName) {
    return $.grep(pList, function (key) {
        return (key[lItemName] != null);
    });
}

function formatDateForUser(date) {
    return moment(date).format('DD/MM/YYYY');
}

// Ajoute un parser à tableSorter
function addDateParser(formatDateForTableEvents) {
    var formatDate = formatDateForTableEvents;
    //Parser utiliser pour filtrer un champ date ( momentJs ) par ordre chronologique
    $.tablesorter.addParser({
        // set a unique id 
        id: 'sortDateParser',  //utiliser cet Id dans le headers pour appeler ce parser
        is: function (s) {
            // return false so this parser is not auto detected 
            return false;
        },
        format: function (s) {
            // format your data for normalization 
            var res = moment(s, formatDate).format('YYYYMMDDHH');
            return res;
        },
        // set type, either numeric or text 
        type: 'numeric'
    });
}

//-----------------------------------------------------------------------------//
// Affichage ou non de la fleche dans le bloc de droite
//-----------------------------------------------------------------------------//
function arrowAction(elem) {
    if ($(elem).attr("id") == "arrowLeft") {

        $("#arrowRight").css("display", "block");
        $(".nav-tabs").css("width", "100%");
        $(".tab-frais").css("width", "100%");
    } else {
        $("#arrowRight").css("display", "none");
        $(".nav-tabs").css("width", "70%");
        $(".tab-frais").css("width", "70%");
    }
}

//-----------------------------------------------------------------------------//
// Affichage de l'aide dans la fenêtre de gauche
//-----------------------------------------------------------------------------//
function showHelp() {
    //si l'aide n'est pas affiché
    if ($(".block-aide").is(":empty")) {
        $(".block-aide").append('<p>Sélectionnez la mission pour laquelle vous souhaitez enregistrer des frais dans la liste qui s\'affiche dans la fenêtre de gauche.</p>')
            .append('<p>Si votre ordre de mission comporte un forfait, merci de vérifier les valeurs indiquées dans le cadre bleu en haut à droite.</p>')
            .append('<p>Pour entrer des frais supplémentaires, cliquez sur le bouton "Nouvelle dépense".</p>')
            .append('<p>Chaque nouveau frais est envoyé à soumission et apparaît à l\'état "soumis". Une fois validé par votre manager, son état passera à "validé".</p>')
            .append('<p>Si vous constatez une erreur dans les forfaits ou les frais, ou l\'absence d\'une mission sur laquelle vous avez travaillé dans la liste des missions, contactez l\'administrateur du site.</p>');
        $(".block-aide").css("margin-bottom", "10%");
        //sinon on le cache
    } else {
        $(".block-aide").empty();
    }
}

//-----------------------------------------------------------------------------//
// Check du pattern des textArea
//-----------------------------------------------------------------------------//
function validateTextArea(elem) {
    var errorMsg = "Merci de ne pas mettre de caractères interdits ('# $ @ / - é à ê etc...') dans le corps de votre message.";
    var textarea = $(elem);
    var pattern = new RegExp('^' + $(textarea).attr('pattern') + '$');
    // check each line of text
    $.each($(elem).val().split("\n"), function () {
        // check if the line matches the pattern
        var hasError = !this.match(pattern);
        if (typeof textarea.setCustomValidity === 'function') {
            textarea.setCustomValidity(hasError ? errorMsg : '');
        } else {
            // Not supported by the browser, fallback to manual error display...
            $(textarea).toggleClass('error', hasError);
            $(textarea).toggleClass('ok', !hasError);
            if (hasError) {
                $(textarea).attr('title', errorMsg);
            } else {
                $(textarea).removeAttr('title');
            }
        }
        return !hasError;
    });
}


//----------------------------------------------------------------------------//
// Création d'un formulaire contenant le message a renvoyer au controller
// a la redirection suite suppression frais
//----------------------------------------------------------------------------// 
function postAndRedirect(url, postData) {
    var postFormStr = "<form method='POST' action='" + url + "'>\n";

    postFormStr += "<input type='hidden' name='message' value='" + postData + "'></input>";

    postFormStr += "</form>";

    var formElement = $(postFormStr);

    $('body').append(formElement);
    $(formElement).submit();
}


//----------------------------------------------------------------------------//
// Rendre la selection de plusieurs lignes possibles
//----------------------------------------------------------------------------// 
function optionSelectforTabFrais() {
    jQuery("tbody").mousedown(function (e) {
        //Enable multiselect with shift key
        if (e.shiftKey) {
            var oTarget = jQuery(e.target);
            if (!oTarget.is('.ui-selectee')) oTarget = oTarget.parents('.ui-selectee');

            var iNew = jQuery(e.currentTarget).find('.ui-selectee').index(oTarget);
            var iCurrent = jQuery(e.currentTarget).find('.ui-selectee').index(jQuery(e.currentTarget).find('.ui-selected'));

            if (iCurrent < iNew) {
                var iHold = iNew;
                iNew = iCurrent;
                iCurrent = iHold;
            }

            if (iNew != '-1') {
                jQuery(e.currentTarget).find('.ui-selected').removeClass('ui-selected');
                for (var i = iNew; i <= iCurrent; i++) {
                    //enlever la possibilité de selectionner un frais ayant été validé si sélection par shift+click
                    if (jQuery(e.currentTarget).find('.ui-selectee').eq(i).find("td:contains('Valide')").length != 0) {
                        jQuery(e.currentTarget).find('.ui-selectee').eq(i).find('.ui-selected').removeClass("ui-selected");
                        if ($('.errorLine').length == 0) {
                            $(".tab-frais").prepend("<p class='errorLine'>La modification d'un frais ayant été validé n'est pas possible...</p>");
                        }
                    } else {
                        jQuery(e.currentTarget).find('.ui-selectee').eq(i).addClass('ui-selected');
                        $("thead tr th:last-child").show();
                        jQuery(e.currentTarget).find('.ui-selectee').eq(i).children("td").last().show();
                    }
                }
                e.stopImmediatePropagation();
                e.stopPropagation();
                e.preventDefault();
                return false;
            }
        }
    }).selectable({
        selected: function (event, ui) {
            //enlever la possibilité de selectionner un frais ayant été validé si sélection par ctrl+click ou click
            if ($(ui.selected).find("td:contains('Valide')").length != 0) {
                $(ui.selected).removeClass("ui-selected");
                if ($('.errorLine').length == 0) {
                    $(".tab-frais").prepend("<p class='errorLine'>La modification d'un frais ayant été validé n'est pas possible...</p>");
                }
            } else {
                //enlever le message d'erreur
                if ($('.errorLine').length != 0) {
                    $('.errorLine').remove();
                }
                if ($(ui.selected).hasClass('selectedfilter')) {
                    if (!($(ui.selected).children("td").last().children().data("clicked")) && !($(".paperclip").data("clicked"))) {
                        $(ui.selected).removeClass('selectedfilter').removeClass('ui-selected');
                        //cacher la colonne header pour la modification et le bouton "modifier"
                        $("thead tr th:last-child").hide();
                        $(ui.selected).children("td").last().hide();
                    }

                } else {
                    $(ui.selected).addClass('selectedfilter').addClass('ui-selected');
                    //affichage de la colonne header pour la modification et du bouton "modifier"
                    $("thead tr th:last-child").show();
                    $(ui.selected).children("td").last().show();

                }
            }
        },
        unselected: function (event, ui) {
            if ($('.errorLine').length != 0) {
                $('.errorLine').remove();
            }
            //cacher la colonne header pour la modification et le bouton "modifier"
            if ($(ui.unselected).hasClass('selectedfilter')) {
                $(ui.unselected).removeClass('selectedfilter').removeClass('ui-selected');
                $(ui.unselected).children("td").last().hide();
            }
            //parcours du tbody pour vérifier qu'il ne reste pas des boutons a cacher
            $($("tbody").children("tr").children("td:last-child")).each(function () {
                if ($(this).is(":visible")) {
                    $(this).hide();
                }
            });
        }
    });
}

//----------------------------------------------------------------------------//
//	Affiche l'élément JQuery avec le message voulu
//  - l'element Jquery doit avoir une balise <p> et une balise <button>
//----------------------------------------------------------------------------// 
function setMessageUtilisateur($pElem, message) {
    $pElem.find('p').html(message);
    $pElem.show();

    $pElem.find('button').click(function () {
        $pElem.hide();
    });
}

// Namespace pour les formatter de colonnes sur la fenêtre modale (accessible dans modalValidAbsences() avec that.formatter.?)
var formatter = {
    dateAbsence: function(value){
        return moment(value).local().add(dstInFrance(moment()) - moment().utcOffset(), 'minutes').format("DD/MM/YYYY HH:mm")
    },

    date: function (value, row, index) {
        return moment(value).local().format("DD/MM/YYYY HH:mm");
    },
    etat: function (value, row, index) {
        if (null != value)
            return value.etat.etat;
    },
    mission: function (value, row, index) {
        if (null != value)
            return value.mission;

    },
    typeAbsence: function (value, row, index) {
        if (null != value)
            return value.typeAbsence.typeAbsence;
    },
    row: function (row, index) {
        if (null != row.absence && row.absence.etat.code != "RE" && row.absence.etat.code != "BR" && row.absence.etat.code != "CO" && row.absence.etat.code != "AN") {
            return {
                classes: "active"
            }
        } else if (null != row.absence && row.absence.etat.code == "BR"  || row.absence.etat.code == "CO" && null == that.currentAbsenceId) {
            return {
                classes: "danger"
            }
        } else if (null != row.ferie && that.WITHFERIE == false) {
            return {
                classes: "danger"
            }
        } else if (null != row.ferie && that.WITHFERIE == true) {
            return {
                classes: "danger"
            }
        } else {
            return {
                classes: "active"
            }
        }
    }

};

function initListEventsWithDates(pListEvents, pStart, pEnd, pWithSaturday, pCurrentMonth) {
    var listEvents = pListEvents;
    var lStart = moment(pStart).tz("Europe/Paris");
    var lStartUtc = moment(pStart).utc();
    //var lEnd = moment(pEnd).local();
    var lEndUtc = moment(pEnd).utc();

    // bab 12/05/2016 correction dernier jour du mois non pris en compte
    // inversion de la condition existante
    //	while (lStart.isBefore(lEnd, 'hours')){
    while (!lStartUtc.isAfter(lEndUtc)) {
        var lDay = lStart.days();

        // Si le jour est !=  Dimanche ET ( != Samedi si pWithSaturday != false )
        if (null == pCurrentMonth || ( pCurrentMonth == lStart.month() )) {

            if (lDay != 0 && ( lDay != 6 || pWithSaturday)) {

                var lStartEvents = moment(lStart).hours(8);
                var lEndEvents = moment(lStart).hours(12);

                var events = findEventByDate(lStartEvents, lEndEvents, listEvents);

                if (events.length == 0) {
                    var tmpEventAM = {};
                    tmpEventAM["title"] = "vide";
                    tmpEventAM["start"] = moment(lStart).hours(8);
                    tmpEventAM["end"] = moment(lStart).hours(12);

                    listEvents.push(tmpEventAM);
                }
                var lStartEvents = moment(lStart).hours(13);
                var lEndEvents = moment(lStart).hours(17);

                var events = findEventByDate(lStartEvents, lEndEvents, listEvents);

                if (events.length == 0) {
                    var tmpEventPM = {};
                    tmpEventPM["title"] = "vide";
                    tmpEventPM["start"] = moment(lStart).hours(13);
                    tmpEventPM["end"] = moment(lStart).hours(17);

                    listEvents.push(tmpEventPM);
                }
            }
        }
        lStart = moment(lStart).add(1, 'days');
        lStartUtc = moment(lStartUtc).add(1, 'days');
    }

}


function findEventByDate(pStart, pEnd, pListEvents) {
    var lEvents = $.grep(pListEvents, function (val) {
        var lStart = val.start;
        var lEnd = val.end;
        return ( pStart.isSame(lStart, 'hours') && pEnd.isSame(lEnd, 'hours') );
    });
    return lEvents;
}

function WaitingApp($pTarget, pBackDrop) {
    var that = this;
    this.backDropSelector = pBackDrop;
    this.$backDrop = null;
    this.$that = $pTarget.find("div");
    this.olDbackDropIndex = null;
    this.zIndex = 9980;

    this.begin = function () {
        that.valueNow(0);
        that.$backDrop = $(that.backDropSelector);
        that.olDbackDropIndex = that.backdropIndex();
        that.backdropIndex(that.zIndex + 1);
    };

    this.done = function () {
        that.valueNow(0);
        that.calculAndSetWidth();
        that.backdropIndex(that.olDbackDropIndex)
    };

    this.valueMax = function (val) {
        if (null != val)
            this.$that.attr("aria-valuemax", val);
        else
            return parseInt(this.$that.attr("aria-valuemax"));
    };

    this.increment = function (val) {
        var current = that.valueNow();
        if (val > 1)
            current += val;
        else
            current++;

        if (current >= that.valueMax()) {
            that.done();
        } else {
            that.valueNow(current);
            that.calculAndSetWidth();
        }

    };

    this.valueNow = function (val) {
        if (null != val)
            this.$that.attr("aria-valuenow", val);
        else
            return parseInt(this.$that.attr("aria-valuenow"));
    };

    this.calculAndSetWidth = function () {
        var newWidth = 100 * ( that.valueNow() / that.valueMax() );
        this.$that.css("width", newWidth + "%");
    };

    this.backdropIndex = function (val) {
        if (null != that.$backDrop) {
            if (null != val) {
                that.$backDrop.css("z-index", val);
            } else {
                return that.$backDrop.css("z-index");
            }
        }
    };

    this.init = function () {

        that.valueNow(0);
        that.calculAndSetWidth();
        that.$that.css("z-index", that.zIndex);

        return that;
    };

    return this;
}


function dstInFrance(date){
    return moment.tz(date, 'Europe/Paris').isDST() ? 120 : 60;
}

/**
 * Comparateur de dates au format JJ-MM-AAAA
 * @param a
 * @param b
 * @returns -1 si a < b, 0 si a = b, 1 si a > b
 */
function ddMMyyyyCustomSorter(a, b) {
    var format = "DD-MM-YYYY";
    var momentA = moment(a, format);
    var momentB = moment(b, format);

    if (momentA.isBefore(momentB)) {
        return -1;
    } else if (momentA.isSame(momentB)) {
        return 0;
    } else
        return 1;
}

/**
 * Comparateur de dates au format MM-AAAA
 * @param a
 * @param b
 * @returns -1 si a < b, 0 si a = b, 1 si a > b
 */
function MMyyyyCustomSorter(a, b) {
    return ddMMyyyyCustomSorter("01-" + a, "01-" + b);
}

// Extends 'from' object with members from 'to'. If 'to' is null, a deep clone of 'from' is returned
function extend(from, to) {
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from) {
        to[name] = typeof to[name] == "undefined" ? extend(from[name], null) : to[name];
    }

    return to;
}

// Retourne l'id de la mission sur le jour donné
function verifPresenceMission(pDate, pIdMission) {
    var lResIdMission = "";
    $.ajax({
        url: 'mesNotesDeFrais/verifPresenceMission/' + pDate + '/' + pIdMission,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lResIdMission = result;
        },
        error: function (xhr, status, error) {
            lResIdMission = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResIdMission;
}

// Retourne true si la persiode donnée correspond bien à la mission
function verifPresenceMissionPeriode(pDateDebut, pDateFin, pIdMission) {
    var lResIdMission = "";
    $.ajax({
        url: 'Deplacements/verifPresenceMissionPeriode/' + pDateDebut + '/' + pDateFin + '/' + pIdMission,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lResIdMission = result;
        },
        error: function (xhr, status, error) {
            lResIdMission = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lResIdMission;
}

// Retourne 'true' si les jours passés en paramètre sont des jours avant le démarrage du collaborateur
// Retourne 'false' sinon
function beforeJourDemarrage(lastJourVide) {
    var lRes = "";
    $.ajax({
        url: 'Rapport-Activites/beforeJourDemarrage/' + lastJourVide,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            lRes = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

// Retourne 'true' si il existe deja un nom de mission pour le collaborateur
// Retourne 'false' sinon
function nomMissionExist(nomMission) {
    var lRes = "";
    var idCollaboratorToEdit = idCollaborator;
    $.ajax({
        url: 'Administration/' + idCollaboratorToEdit + '/nomMissionExist/' + nomMission,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            lRes = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

/**
 * Method pour couper le lien après un click pour éviter la duplication de mail
 * @param link
 */
function clickAndDisable(link) {
    // disable subsequent clicks
    link.onclick = function(event) {
        event.preventDefault();
    }
}

/*===============================
// Date picker
 ================================*/
$(function(){
    $('.monthPicker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-7:c+3',
        showButtonPanel: true,
        dateFormat: 'mm/yy',
        closeText: 'Ok',
        onClose: function (dateText) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            $(this).change();
        },
        onInit: function () {
            $(this).datepicker('setDate', new Date());
        }
    });

});

/*===============================
// Fixed Bootstrap Table Scroll
 ================================*/
function resetHeightTable() {
    $(".fixed-table-container").css({"height":"auto", "padding-bottom": "0"});
}

$(function () {
    resetHeightTable();

    // Resize columns with window
    $(window).resize(function () {
        $('[data-toggle="table"]').bootstrapTable('resetView');
        resetHeightTable();
        $(".navbar-form.navbar-left").css("margin-top", "50px");
    });

    $("body").on("page-change.bs.table", "[data-toggle='table']", function(){
        resetHeightTable();
    });

    $("body").on("click", ".sortable", function(){
        resetHeightTable();
    });
});
