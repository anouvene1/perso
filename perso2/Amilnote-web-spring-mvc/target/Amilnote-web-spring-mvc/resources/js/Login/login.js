/*
 * ©Amiltone 2017
 */

/**
 * Fichier JavaScript de gestion du login
 * @author Kévin Esprit
 */

/**
 * Méthode permettant de lire la valeur du cookie portant le nom en paramètre
 */
function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

/**
 * Méthode permettant de lire le cookie de l'éventuel login saisi dans la page de login
 */
function readCookieLoginSaisi() {
    var value = getCookie("loginSaisi");
    $("#mail").val(value);
}

/**
 * Méthode permettant d'assigner le cookie de l'éventuel login saisi dans la page de login
 */
function createCookieLogin() {
    document.cookie = "loginSaisi=" + $("#email").val();
}

/**
 * Méthode permettant d'assigner le cookie dans le champ login après récupération du mot de passe
 */
function resetCookieLogin() {
    var value = getCookie("loginSaisi");
    $("#email").val(value);
}

/**
 * Hash and submit password
 */
function hashAndSubmit() {
    var hashedPassword;
    var passwordToHash = $('#passwordToHash').val();

    hashedPassword = CryptoJS.SHA512(passwordToHash).toString();

    $('#password').val(hashedPassword);
    $('#passwordToHash').val("");
}
