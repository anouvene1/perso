/*
 ~ ©Amiltone 2017
 */
$(document).ready(function () {
    //Initialisation du tableau contenant toutes les invitations à mettre à jour {idSession, idCollab, etatInvit}
    var allInvitations = [];
    fixTableHeaders();
    fillInvitationsAddvise();
    manageInvitations(allInvitations);
    manageSessions(allInvitations);
    manageTrisAndFilters();

    var modalCreationSession = $('#addvise-modal-creation-session');
    $('#btn-addvise-modal-creation-session').click(function () {
        modalCreationSession.modal({backdrop: 'static'});
        datepickerCreationSession();
        anticipateNewModuleCode();
        applyEventsModal();
    });

    var modalModificationSession = $('#addvise-modal-modification-session');
    $('.btn-addvise-modal-modification-session').click(function () {
        modalModificationSession.modal({backdrop: 'static'});

        var module = $(this).data("module");
        var idformateur = $(this).data("idformateur");
        var jour = $(this).data("jour");
        var mois = $(this).data("mois");
        var annee = $(this).data("annee");
        var heure = $(this).data("heure");
        var minute = $(this).data("minute");
        var idSession = $(this).data("idsession")
        var idmodule = $(this).data("idmodule");
        console.log(idmodule);

        datepickerModificationSession(jour, mois, annee, heure, minute);
        $('#addvise-field-modification-session-module > option').attr('selected', null);
        $('#addvise-field-modification-session-module > option[value='+idmodule+']').attr('selected', 'selected');
        $('#addvise-field-modification-session-formateur > option[value='+idformateur+']').attr('selected', 'selected');


        var form = $('#form-edit');
        $("<input>", {
            type: "hidden",
            name: "idSession",
            value: idSession
        }).appendTo(form);
        $("<input>", {
            type: "hidden",
            name: "allInvitations",
            value: allInvitations
        }).appendTo(form);
        $('#btn-modale-edit-session').prop('disabled', true);
        $('#addvise-field-modification-session-module').change(function() {
            var valeur =$(this).val();
            console.log(valeur + idmodule)
            if(valeur != idmodule) {
                $("#btn-modale-edit-session").prop('disabled',false);
            } else {
                $("#btn-modale-edit-session").prop('disabled',true);
            }
        });
        $('#addvise-field-modification-session-formateur').change(function() {
            var valeur =$(this).val();
            console.log(valeur + idformateur)
            if(valeur != idmodule) {
                $("#btn-modale-edit-session").prop('disabled',false);
            } else {
                $("#btn-modale-edit-session").prop('disabled',true);
            }
        });

        $('#addvise-field-modification-session-date').click(function() {
                $("#btn-modale-edit-session").prop('disabled',false);
        });

    });

    $(".tooltip-addvise").tooltip();
});

function manageInvitations(allInvitations) {
    var btnValidation = $("#btn-validate-addvise-session");
    btnValidation.prop("disabled", true);

    var btnAlertClose = $(".close-alert");
    btnAlertClose.click(function () {
        $(".checkInvitDate").css("display", "none");
    });

    //Changement d'état des invitations quand on clique sur les icônes
    var icones = $('.icone');
    icones.click(function () {
        var invitationElement = $(this).closest(".invitation-addvise");
        var idSession = invitationElement.data("session");
        var idCollab = invitationElement.data("collab");

        if (!invitationElement.hasClass("alreadyStored") && $(this).css("opacity") == "1") {
            $(this).css("opacity", "0.2");
            invitationElement.removeData("etatinvitation");
            allInvitations = removeInvitation(allInvitations, idSession, idCollab);

            //compter confirmées
            var span = $(".ft_rwrapper .session-modif.session-id-" + idSession)
                .find(".countConfirmSpan");
            if ($(this).hasClass("icone_2")) {
                $(span).text(parseInt($(span).text()) - 1);
            }
        } else if ($(this).css("opacity") == "0.2") {
            //compter confirmées
            var span2 = $(".ft_rwrapper .session-modif.session-id-" + idSession).find(".countConfirmSpan");

            var dateSession = new Date(
                $(".cell-addvise-dateSession-annee.session-id-" + idSession).first().text() + "-" +
                $(".cell-addvise-dateSession-mois.session-id-" + idSession).first().text() + "-" +
                $(".cell-addvise-dateSession-jour.session-id-" + idSession).first().text() + " " +
                $(".cell-addvise-dateSession-heure.session-id-" + idSession).first().text() + ":" +
                $(".cell-addvise-dateSession-minutes.session-id-" + idSession).first().text()
            );
            var today = new Date();

            if ($(this).hasClass("icone_2")) {
                $(span2).text(parseInt($(span2).text()) + 1);
            } else if (invitationElement.data("etatinvitation") == 2) {
                $(span2).text(parseInt($(span2).text()) - 1);
            }

            //Mise à jour des invitations
            if ($(this).hasClass("icone_0")) {
                invitationElement.data("etatinvitation", 0);
            } else if ($(this).hasClass("icone_1")) {
                if (dateSession >= (today)) {
                    invitationElement.data("etatinvitation", 1);
                } else {
                    $(".checkInvitDate").css("display", "block");
                    if ($(this).siblings(".icone_2").css("opacity") == 1) {
                        $(span2).text(parseInt($(span2).text()) + 1);
                    }
                }
            } else if ($(this).hasClass("icone_2")) {
                invitationElement.data("etatinvitation", 2);
            } else if ($(this).hasClass("icone_3")) {
                invitationElement.data("etatinvitation", 3);
            }

            if ($(this).hasClass("icone_1")) {
                if (dateSession > today) {
                    $(this).siblings('.icone').css('opacity', '0.2');
                    $(this).css('opacity', '1.0');
                }
            } else {
                $(this).siblings('.icone').css('opacity', '0.2');
                $(this).css('opacity', '1.0');
            }

            //On met à jour les invitations ajoutées ou modifiées
            var etatInvit = invitationElement.data("etatinvitation");

            if (idSession && idCollab && etatInvit !== null) { //attention, le signe != est incorrect car peut être = 0
                updateInvitation(allInvitations, idSession, idCollab, etatInvit);
            }
        }

        if (allInvitations.length == 0) {
            btnValidation.prop("disabled", true);
        } else {
            btnValidation.prop("disabled", false);
        }
    });

    btnValidation.click(function () {
        saveAllInvitations(allInvitations);
    });
}

function saveAllInvitations(allInvitations) {
    //On envoie à la requête POST du formulaire les données voulues
    var form = $("#addvise-validate-sessions");
    $("<input>", {
        type: "hidden",
        name: "allInvitations",
        value: allInvitations
    }).appendTo(form);
    form.submit();
}

function manageSessions(allInvitations) {
    $(".btn-confirm-session").click(function () {
        var idSession = $(this).data("idsession");
        var modalClone = $("#modale-confirm-session-" + idSession).clone().attr("id", "modale-clone-" + idSession);
        $("#modalesAddvise").append(modalClone);

        var btn = $(this);
        $("#modale-clone-" + idSession).modal({backdrop: 'static'});
        if ($(this).data("etatsession") == "CONFIRMEE") {
            $(".modal-confirm-session").hide();
        } else {
            $(".modal-validate-session").hide();
        }

        $(".btn-modale-confirm-session").click(function () {
            var form = btn.closest(".form-confirm-session");
            $("<input>", {
                type: "hidden",
                name: "allInvitations",
                value: allInvitations
            }).appendTo(form);
            form.submit();
        });

        $("#btn-cancel-" + idSession).click(function () {
            modalClone.remove();
        });
    });

    $(".btn-remove-session").click(function () {
        var btn = $(this);
        $("#modale-remove-session").modal({backdrop: 'static'});

        $("#btn-modale-remove-session").click(function () {
            var form = btn.closest(".form-remove-session");
            $("<input>", {
                type: "hidden",
                name: "allInvitations",
                value: allInvitations
            }).appendTo(form);
            form.submit();
        });
    });



    //On cache simplement le bouton pour imprimer quand il est inutile (ie si la session n'est pas confirmée)
    $(".btn-print-session[data-etatsession='EN_COURS_DE_SAISIE']").hide();
}

function updateInvitation(allInvitations, idSession, idCollab, etatInvit) {
    var existingInvit = false;
    allInvitations.forEach(function (invit, i) {
        if (invit[0] == idSession && invit[1] == idCollab) {
            allInvitations[i] = [idSession, idCollab, etatInvit];
            existingInvit = true;
            return false;
        }
    });

    if (!existingInvit) {
        allInvitations.push([idSession, idCollab, etatInvit]);
    }
}

function removeInvitation(allInvitations, idSession, idCollab) {
    var updatedInvitations = [];
    allInvitations.forEach(function (invit) {
        if (invit[0] != idSession || invit[1] != idCollab) {
            updatedInvitations.push(invit);
        }
    });
    return updatedInvitations;
}

function fixTableHeaders() {

    var arrColModal = [{width: 350, align: 'center'}, {width: 200, align: 'center'}];
    $(".session-modif").each(function () {
        arrColModal.push({width: 200, align: 'center'});
    });

    $("#table-addvise-gestion").fxdHdrCol({
        fixedCols: 2,
        width: "100%",
        height: "80%",
        colModal: arrColModal
    });
    $(".cell-addvise-1").css("width", "320px");
    $(".cell-addvise-2").css("width", "200px");
    $(".ft_cwrapper").css("height", "auto");
}

function fillInvitationsAddvise() {
    //La position et le numéro de l'état sont les positions des énumérations d'AddviseLinkCollaborateurSession
    var color = ["#A9E2F3", "#F5F6CE", "#D8F6CE", "#F6CECE"];
    $('.icone').css("opacity", "0.2");

    var idsCollabSession = "";
    $(".invitation-addvise").each(function (i, el) {
        idsCollabSession += $(el).data("collab") + "," + $(el).data("session") + ",";
    });

    if (idsCollabSession) {
        $.ajax({
            type: "GET",
            url: "Administration/addvise/getInvitationsByCollabSession",
            data: {"idsCollabSession": idsCollabSession},
            dataType: "html",
            success: function (listeEtats) {
                listeEtats = listeEtats.slice(1, -1).split(",");
                for (var i = 0; i < listeEtats.length; i += 3) {
                    var element = $('.invitation-addvise[data-collab="' + listeEtats[i] + '"][data-session="' +
                        listeEtats[i + 1] + '"]');
                    var etat = listeEtats[i + 2];
                    $(element).data("etatinvitation", etat);
                    $(element).find(".icone_" + etat).css("opacity", "1.0");
                    $(element).closest("td").css("background-color", color[etat]);
                    $(element).addClass("alreadyStored");

                    if (etat == 2) { //compter confirmées
                        var span = $(".ft_rwrapper .session-modif.session-id-" + listeEtats[i + 1])
                            .find(".countConfirmSpan");
                        $(span).text(parseInt($(span).text()) + 1);
                    }
                }
            }
        });
    }
}

function datepickerCreationSession() {
    var datetimepicker = $('#addvise-field-creation-session-date');
    datetimepicker.datetimepicker();
    datetimepicker.data('DateTimePicker').date(new Date(new Date().setHours(18, 30, 0)));
}


function datepickerModificationSession(jour, mois, annee, heure, minute) {
    var datetimepicker = $('#addvise-field-modification-session-date');
    datetimepicker.datetimepicker();
    var d = new Date();
    d.setHours(heure, minute, 0);
    d.setMonth(mois-1, jour);
    d.setFullYear(annee);
    datetimepicker.data('DateTimePicker').date(d);
}


function anticipateNewModuleCode() {
    var numeroModule = $('#addvise-field-creation-modification-module option').length + 1;
    $("#addvise-field-creation-module-code").val("M" + numeroModule);
    $("#addvise-field-creation-module-code").prop("disabled", true); //Empêche l'édition et donc les erreurs
}

// Traitements aynchrones : nouveau module (M10...) et nouveau formateur

function applyEventsModal() {

    // Traitements aynchrones : nouveau module (M10...) et nouveau formateur

    $("#addvise-create-module").click(function () {
        var code = $("#addvise-field-creation-module-code").val();
        var nom = $("#addvise-field-creation-module-nom").val();

        var divMsg = $(this).next(".addvise-msg");

        $.ajax({
            type: "POST",
            url: "Administration/addvise/addNewModule",
            data: {"codeModule": code, "nomModule": nom},
            dataType: "html",
            success: function (result) {
                $('#addvise-field-creation-modification-module').append($('<option>', {
                    value: result,
                    text: code + " : " + nom
                }));
                $("#addvise-field-creation-module-code").val('');
                $("#addvise-field-creation-module-nom").val('');
                $("div#addvise-fields-creation-module input").css("border-color", "green");
                divMsg.css("color", "green");
                divMsg.text("Module ajouté. Vous pouvez le sélectionner pour créer une session.");
                anticipateNewModuleCode();
            },
            statusCode: {
                400: function () {
                    $("div#addvise-fields-creation-module input").css("border-color", "red");
                    divMsg.css("color", "red");
                    divMsg.text("Valeurs incorrectes : vérifiez les champs.");
                },
                500: function () {
                    $("div#addvise-fields-creation-module input").css("border-color", "red");
                    divMsg.css("color", "red");
                    divMsg.text("Erreur du serveur.");
                }
            }
        });
    });

    $("#addvise-create-formateur").click(function () {
        var nom = $("#addvise-create-formateur-nom").val();
        var prenom = $("#addvise-create-formateur-prenom").val();
        var email = $("#addvise-create-formateur-email").val();

        var divMsg = $(this).next(".addvise-msg");

        $.ajax({
            type: "POST",
            url: "Administration/addvise/addNewFormateur",
            data: {"nomFormateur": nom, "prenomFormateur": prenom, "emailFormateur": email},
            dataType: "html",
            success: function (result) {
                $('#addvise-field-creation-session-formateur').append($('<option>', {
                    value: result,
                    text: nom + " " + prenom
                }));
                $("#addvise-create-formateur-nom").val('');
                $("#addvise-create-formateur-prenom").val('');
                $("#addvise-create-formateur-email").val('');
                $("div#addvise-fields-creation-formateur input").css("border-color", "green");
                divMsg.css("color", "green");
                divMsg.text("Formateur ajouté. Vous pouvez le sélectionner pour créer une session.");
            },
            statusCode: {
                400: function () {
                    $("div#addvise-fields-creation-formateur input").css("border-color", "red");
                    divMsg.css("color", "red");
                    divMsg.text("Valeurs incorrectes ou existantes : vérifiez les champs.");
                },
                500: function () {
                    $("div#addvise-fields-creation-formateur input").css("border-color", "red");
                    divMsg.css("color", "red");
                    divMsg.text("Erreur du serveur.");
                }
            }
        });
    });

    $(".addvise-session-form").submit(function () {
        var dateField = $('#addvise-field-creation-session-date');
        if (!dateField.val()) {
            dateField.css("border-color", "red");
            return false;
        }
    });
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function manageTrisAndFilters() {
    var trier = getUrlParameter("trier");
    var cacher = getUrlParameter("cacher");
    var chercher = getUrlParameter("chercher");
    var filtrer = getUrlParameter("filtrer");

    $("#addvise-cacher").val(cacher ? "1" : "0");
    if (chercher) {
        $("#addvise-chercher").val(chercher);
    }
    $("#addvise-filtrer").val(filtrer);

    if (!trier || trier == "date-entree") {
        $("#addvise-tri-date-entree").css("opacity", 1);
    } else if (trier == "alpha") {
        $("#addvise-tri-alpha").css("opacity", 1);
    } else { //dernier module
        $("#addvise-tri-date-dernier-module").css("opacity", 1);
    }

    var refresh = function () {
        //Redirection avec le nouvel url
        var url;
        if (window.location.href.indexOf("?") >= 0) {
            url = window.location.href.substr(0, window.location.href.indexOf('?') + 1);
        } else {
            url = window.location.href + "?";
        }

        if (trier) {
            url += "trier=" + trier + "&";
        }
        if (cacher) {
            url += "cacher=" + cacher + "&";
        }
        if (chercher) {
            url += "chercher=" + chercher + "&";
        }
        if (filtrer) {
            url += "filtrer=" + filtrer;
        } else {
            url = url.slice(0, -1); //suppression du dernier & inutile
        }

        window.location.href = url;
    };

    $("#addvise-tri-alpha").click(function () {
        if (trier != "alpha") {
            trier = "alpha";
            refresh();
        }
    });

    $("#addvise-tri-date-entree").click(function () {
        if (trier != "date-entree") {
            trier = "date-entree";
            refresh();
        }
    });

    $("#addvise-tri-date-dernier-module").click(function () {
        if (trier != "date-dernier-module") {
            trier = "date-dernier-module";
            refresh();
        }
    });

    $("#addvise-filtrer").bind('change', function () {
        filtrer = $("#addvise-filtrer").val();
        refresh();
    });

    $("#addvise-cacher").bind('change', function () {
        cacher = $(this).val() != "0";
        refresh();
    });

    $("#addvise-chercher").keypress(function (e) {
        if (event.keyCode == 13) { //disable ENTER
            e.preventDefault();
            var collabName = $("#addvise-chercher").val();
            if (chercher != collabName) {
                chercher = collabName;
                refresh();
            }
        }
    });
}
