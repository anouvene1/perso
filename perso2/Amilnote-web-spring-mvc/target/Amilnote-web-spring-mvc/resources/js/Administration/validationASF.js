/*
 * ©Amiltone 2017
 */

// Data formatter pour la checkbox
function stateFormatter(value, row, index) {
    if (row.code !== "SO") {
        return {
            disabled: true,
            checked: false
        };
    }
    return value;
}

function annulerFiltre() {
    var dir = window.location.pathname.split('/');
    var res=dir[0]+"/";
    for(var i=1; i<dir.length-2; i++)
        res += dir[i]+"/";
    window.location.href = res + "null/TO";
}

$(function() {
    nomFichier = document.location.href.substring(document.location.href.lastIndexOf("/") + 1);
    if (nomFichier == "VA") {
        nomFichier = "(Validées)";
    }
    else if (nomFichier == "SO") {
        nomFichier = "(Soumises)";
    }
    else if (nomFichier == "SD") {
        nomFichier = "(Soldées)";
    }
    else {
        nomFichier = "";
    }
    document.getElementById("filtre").innerHTML = "Notes de Frais " + nomFichier;

    $('.monthPicker').change(function () {
        var dir = window.location.pathname.split('/');

        var res = dir[0]+"/" ;
        for(var i=1; i<dir.length-1; i++)
            res += dir[i]+"/";

        var monthYear = this.value.split("/");
        window.location.href = res + monthYear[0] + "_" + monthYear[1];
    });

    // Navigation filter
    var dir = window.location.pathname.split('/');
    document.getElementById("lienFiltre1").setAttribute("href",document.getElementById("lienFiltre1")+"/"+dir[dir.length-1]);
    document.getElementById("lienFiltre2").setAttribute("href",document.getElementById("lienFiltre2")+"/"+dir[dir.length-1]);
    document.getElementById("lienFiltre3").setAttribute("href",document.getElementById("lienFiltre3")+"/"+dir[dir.length-1]);
    document.getElementById("lienFiltre4").setAttribute("href",document.getElementById("lienFiltre4")+"/"+dir[dir.length-1]);
    var filtre;
    switch(dir[dir.length-2]) {
        case "SO":
            filtre="lienFiltre2";
            break;
        case "VA":
            filtre="lienFiltre3";
            break;
        case "SD":
            filtre="lienFiltre4";
            break;
        default:
            filtre="lienFiltre1";
    }
    document.getElementById(filtre).style.fontWeight='bold';
    document.getElementById(filtre).style.fontSize = "14px";
    document.getElementById(filtre).style.textDecoration = 'underline';

    var $bootstrapTable = $("#idBootstrapTable");
    var $modalLoading = $("#idModalLoading");

    $(".btnActionSelection").click(function (e) {
        //Action qu'on veut attacher au formulaire temporaire
        var action = $(this).data("action");
        //Liste des cases cochées
        var selectedDepl = $bootstrapTable.bootstrapTable("getSelections");

        var $tmpForm = $("<form>", {
            "action": action,
            "method": "POST"
        });
        //Pour chaque absences cochées
        //	- création input
        //	- ajout de l'input au form
        $.each(selectedDepl, function (key, val) {
            if (val.code === 'SO') {
                $("<input>", {
                    "name": "listFrais[" + key + "].id",
                    "value": val.id
                }).appendTo($tmpForm);
            }
        });
        $modalLoading.find(".modal-footer").append("Cela peut prendre quelques instants.")
        $modalLoading.modal({
            backdrop: 'static',
            keyboard: false
        });
        $tmpForm.appendTo('body').submit();
    });
});