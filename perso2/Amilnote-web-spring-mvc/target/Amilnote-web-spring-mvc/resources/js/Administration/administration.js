/*
 * ©Amiltone 2017
 */

$(function () {

    var URL;

    $('.alert-delete').click(function () {
        URL = $(this).attr('data-url');
        $('#warning-delete-Action').modal({
            backdrop: 'static'
        });
        return false;
    });

    $('#delete-Action').click(function () {
        $(location).attr('href', URL);
    });

    var myLanguage = {
        errorTitle: 'Form submission failed!',
        requiredFields: 'You have not answered all required fields',
        badTime: 'You have not given a correct time',
        badEmail: 'Vous n\'avez pas entrer une adresse valide',
        badTelephone: 'Vous devez entrer uniquement des numéros',
        badSecurityAnswer: 'You have not given a correct answer to the security question',
        badDate: 'Vous devez entrer une date valide',
        lengthBadStart: 'Vous devez entrer un mot entre ',
        lengthBadEnd: ' caractères.',
        lengthTooLongStart: 'Vous ne pouvez pas dépasser ',
        lengthTooShortStart: 'You have given an answer shorter than ',
        notConfirmed: 'Values could not be confirmed',
        badDomain: 'Incorrect domain value',
        badUrl: 'The answer you gave was not a correct URL',
        badCustomVal: 'Ce champ n\'est pas valide.',
        badInt: 'Vous devez entrer uniquement des numéros valides',
        badSecurityNumber: 'Your social security number was incorrect',
        badUKVatAnswer: 'Incorrect UK VAT Number',
        badStrength: 'The password isn\'t strong enough',
        badNumberOfSelectedOptionsStart: 'You have to choose at least ',
        badNumberOfSelectedOptionsEnd: ' réponses',
        badAlphaNumeric: 'The answer you gave must contain only alphanumeric characters ',
        badAlphaNumericExtra: ' and ',
        wrongFileSize: 'The file you are trying to upload is too large',
        wrongFileType: 'The file you are trying to upload is of wrong type',
        groupCheckedRangeStart: 'Please choose between ',
        groupCheckedTooFewStart: 'Please choose at least ',
        groupCheckedTooManyStart: 'Please choose a maximum of ',
        groupCheckedEnd: ' item(s)'
    };

    $.validate({
        language: myLanguage
    });

    $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
})

/**
 * Appelé quand l'utilisateur click sur le bouton envoyer mail de rappel
 */
function eventClikOnBtnSendRappel() {

    var lRes = "";
    $.ajax({
        url: 'Administration/listeSoumissionRA/Rappel',
        type: 'POST',
        async: false,
        success: function (result) {
            lRes = result;
        },
        error: function (xhr, status, error) {
            var lResIdMission = "error";
        },
        statusCode: {
            901: function () {
                top.location.href = "login";
            }
        }
    });

    return lRes;
}

/**
 * Appelé quand on supprime un élément d'un tableau => fenetre modal confirmation
 */
function eventClikDelate(url) {
    var URL = url;
    $('#warning-delete-Action').modal({
        backdrop: 'static'
    });

    $('#delete-Action').click(function () {
        $(location).attr('href', URL);
    });
}

function openModalSupprimerModule(url) {
    var URL = url;
    // on ouvre la modal
    $('#warning-delete-Action').modal({
        backdrop: 'static'
    });

    $('#delete-Action').click(function () {
        $(location).attr('href', URL);
    });
}

function openModalDateSolde(id) {
    // on ouvre la modal
    $('#warning-missing-parameter').modal({
        backdrop: false
    });
    var form = document.getElementById("solderASFPE");
    var inputHidden = document.createElement("input");
    inputHidden.setAttribute("type", "hidden");
    inputHidden.setAttribute("id", "idNoteFrais");
    inputHidden.setAttribute("name", "idNoteFrais");
    inputHidden.setAttribute("value", id);
    form.appendChild(inputHidden);
    return false;

}

/**
 * Open confirmation modal to disable a collaborator or subcontractor
 * @param modalID Modal identifier
 * @param workerID Collaborator or subcontractor identifier
 * @param inputID Hidden input that content the collaborator or subcontractor id
 * @param formID Identifier of disable collaborator or subcontractor form
 * @param tableContainerType The type of table to display collaborators or subcontractors
 * @returns {boolean} Boolean to prevent the default navigator event
 */
function openModalDateSortie(modalID, workerID, inputID, formID, tableContainerType) {
    // On ouvre le modal
    $('#' + modalID).modal({
        backdrop: false
    });

    // Get collaborator id
    var inputHidden = document.getElementById(inputID);
    inputHidden.setAttribute("value", workerID);

    // Setting controller url
    // $("#" + formID).attr("action", "Administration/deleteCollaborateur/"+ workerID + "?type=" + tableContainerType);

    $("#" + formID).attr("action", "Administration/changeActivationStatusWorkers/"+ workerID + "?type=" + tableContainerType);

    return false;
}

/**
 * Validation form to disable or not a collaborator or subcontractor
 * @param formID Form identifier
 * @param msgID Message identifier
 * @returns {boolean} Boolean to prevent the default navigator event
 */
function validateFormDelete(formID, msgID) {
    var myForm = document.getElementById(formID);

    if (myForm.dteSortie.value !== "") {
        myForm.submit();
        return true;
    } else {
        var span = document.getElementById(msgID);

        setTimeout(function () {
            $("#"+formID).parents('.modal').modal('show');

            // On affiche le message d'erreur
            span.style.display = "block";
        }, 250);

        return false;
    }
}

function validateForm() {
    var myForm = document.getElementById('solderASFPE');
    if (myForm.dateSolde.value !== "") {
        myForm.submit();
        return true;
    } else {
        var span = document.getElementById('msgDateManquante');
        // on affiche le message d'eurreur
        span.style.display = "block";
        setTimeout(function () {
            $('#warning-missing-parameter').modal('show');
        }, 250);
        return false;
    }
}

function validateFormDeleteModule() {
    var myForm = document.getElementById('supprModuleAddvise');
    myForm.submit();
    return true;
}

function openCollabForm(e, id) {
    // on ouvre la modal
    $('#warning-delete-Action').modal({
        backdrop: false
    });
}

function deleteCollabForm(e) {
    e.preventDefault();
    var myForm = document.getElementById('disabledCollabForm');
    if (myForm.dateSortie.value !== "") {
        var inputHidden = document.createElement("input");
        inputHidden.setAttribute("type", "hidden");
        inputHidden.setAttribute("id", "dteSortie");
        inputHidden.setAttribute("name", "dteSortie");
        inputHidden.setAttribute("value", myForm.dateSortie.value);
        myForm.appendChild(inputHidden);
        myForm.submit();
    } else {
        var span = document.getElementById('msgDateManquante');
        // on affiche le message d'eurreur
        span.style.display = "block";
        setTimeout(function () {
            $('#warning-delete-Action').modal('show');
        }, 250);

    }
}

// The url variable is used to retrieve the source page's url with the filters used when the action occurs
function annulerRA(id) {
    $("#idRA").attr({value: id});

    var url = window.location.href.split('/');
    if (url.length > 6) {
        var param = url[5] + "/" + url[6];
        var date = param.split('=');
        $("#filtreDate").attr({value: date[1]});
        var filtre = url[5].split('?');
        $("#filtreRA").attr({value: filtre[0]});
    } else {
        param = url[5];
        $("#filtreRA").attr({value: param});
    }
    document.getElementById("dateFiltre").setAttribute("value",url[6]);

    $("#modalAnnulationRA").modal({
        backdrop: 'static'
    });
}

/*Utilities for travailleur.jsp
--------------------------------*/
$(function () {

    /* Collaborators and subtractors reactivation
    ---------------------------------------------------------*/
    var $tdEtat = null;
    var $tdEdition = null;
    var $tdDesactiver = null;

    // Load a gif image when ajax start
    $(document).ajaxStart(function() {
        if($tdEtat !== null && $tdEdition !== null && $tdDesactiver !== null) {
            $tdEtat.html("<div class='loading'><img src='/amilnote/resources/gif/load.gif' alt='Loading ...'></div>");
            $tdEdition.html("<div class='loading'><img src='/amilnote/resources/gif/load.gif' alt='Loading ...'></div>");
            $tdDesactiver.html("<div class='loading'><img src='/amilnote/resources/gif/load.gif' alt='Loading ...'></div>");
        }
    });

    // AJAX call for reactivate a collaborator or subcontractor treatement
    $("body").on("click", ".reactivatedLink", function(e) {
        e.preventDefault();

        $this = $(this);
        $tdEtat = $this.parent("td").closest("tr").find("td.etat");
        $tdEdition = $this.parent("td").closest("tr").find("td.edition");
        $tdDesactiver = $this.parent("td.desactivation");

        $.ajax({
            url: "Administration/changeActivationStatusWorkers/" +  $this.attr("data-idTravailleur") + "?type=",
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            // async: false,
            success: function (result) {
                $(".loading").hide();
                if(result === true) {
                    $tdEtat.html("Activé");

                    var editLink = "<a href='Administration/editTravailleur/";
                    editLink += $this.attr('data-typeTravailleur') + "/";
                    editLink += $this.attr('data-idTravailleur') + "/null'>";
                    editLink += "<span class='glyphicon glyphicon-edit'></span></a>";

                    $tdEdition.html(editLink);

                    var title = "";
                    ($this.attr("data-typeTravailleur") === "collaborateur") ? title += " collaborateur" : title += "sous-traitant";

                    var desactivateLink = "<a data-title='Désactivation' ";
                    desactivateLink += "title='Désactiver le " + title + "'";
                    desactivateLink += " href=''";
                    desactivateLink += " onclick=\"return openModalDateSortie('warning-delete-Action', ";
                    desactivateLink += $this.attr('data-idTravailleur');
                    desactivateLink +=  ", 'idCollab', 'disabledCollabForm', 'idTableContainer')\">";
                    desactivateLink += "<span class='glyphicon glyphicon-remove'></span>";
                    desactivateLink += "</a>";

                    $tdDesactiver.html(desactivateLink);
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
                $tdEtat.html("Erreur");
                $tdEdition.html("Erreur");
                $tdDesactiver.html("Erreur");
            }
        });
    });
    // End of reactivation
});

/**
 * Display toggle disactivated collaborators or subcontractors
 * @param cb The checbox button to display activated or desactivate collaborators or subcontractors
 * @param typeTravailleur String to distinct "travailleurs" or "collaborators"
 * @param tableContainerType Type of table container for collaborators or subcontractors
 */
function handleClick(cb, typeTravailleur, tableContainerType) {
    if (cb.checked) {
        $(location).attr('href', "Administration/" + typeTravailleur + "/true" + tableContainerType);
    } else {
        $(location).attr('href', "Administration/" + typeTravailleur + "/false" + tableContainerType);
    }
}
// End utilities for travailleur.jsp


/* Utilities for collaborateur.jsp view
----------------------------------------*/
/**
 * Search and filter collaborators
 */
function filterCollaborators() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("scrolltab");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    }
}

var urlDelate = "Administration/deleteCollaborateur/";
// End utilities for collaborateur.jsp view