$(function(){
    $('.monthPicker').datepicker('setDate', new Date());

    // To show datepicker calendar in the modal when we disable a collaborator or subcontractor
    $(".datepicker").datepicker({
        showOn: "both",
        dateFormat: 'yy-mm-dd'
    }).on("click", function() {
        $(".ui-datepicker-calendar").css("display", "block");
    });
});