var urlDelate = "Administration/deleteMission/";

function collaboratorEmailVerif() {
    // When we try to add or edit collaborator, we need to select specific domain name within a select input
    // We concatenate the two parts of the email address after submission of the collaborators' form
    $("#idFormColl").on("submit", function () {
        $('#idEmailColl').val(
            $('#idFirstPartEmail').val() + $('#idSecondPartEmail option:selected').text()
        )
    });

    // We store full email address of collaborator which is in an hidden input
    var email = $("#idEmailColl").val();

    // Set only first part of email until @domainName
    $("#idFirstPartEmail").val(email.split("@", 2)[0]);

    // We're looking for the right domain name to set good selected option
    $("#idSecondPartEmail option").each(function () {
        if (email.indexOf($(this).val()) != -1) {
            $(this).prop("selected", true);
            return false;
        }
    });
}

$(function() {
    $('.monthPicker').change(function () {
        window.location.href = window.location.pathname + "?monthYearExtract=" + this.value;
    });

    // To show datepicker calendar in the modal when we disable a collaborator or subcontractor
    $(".datepicker, #ui-datepicker-div").datepicker({
        showOn: "both",
        dateFormat: 'yy-mm-dd'
    }).on("click", function() {
        $(".ui-datepicker-calendar").css("display", "block");
    });
});

function annulerFiltre() {
    window.location.href = window.location.pathname;
}