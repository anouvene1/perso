
$(function () {

    var $bootstrapTable1 = $("#idBootstrapTable1");
    var $bootstrapTable2 = $("#idBootstrapTable2");


    //  [AMA][AMNT-579] Prépare un PDF pour les collaborateurs
    $("#btnCreationMultiplePdfFirstPageCollab").click(function (e) {
        var action = $(this).data("action");

        //Liste des cases cochées
        var selectedRA = $bootstrapTable1.bootstrapTable("getSelections");

        if (selectedRA.length > 0) {

                var $tmpForm = $("<form>", {
                "action": action,
                "method": "POST"
            });
            //Pour chaque RA cochés
            //	- création input
            //	- ajout de l'input au form

            $.each(selectedRA, function (key, val) {
                $("<input>", {
                    "name": "listRA[" + key + "].id",
                    "value": val.id,
                    "hidden": true
                }).appendTo($tmpForm);
            });

            $tmpForm.appendTo('body').submit();
        }
    });

    $("#btnCreationMultiplePdfAllPagesCollab").click(function (e) {
        var action = $(this).data("action");

        //Liste des cases cochées
        var selectedRA = $bootstrapTable1.bootstrapTable("getSelections");

        if (selectedRA.length > 0) {

            var $tmpForm = $("<form>", {
                "action": action,
                "method": "POST"
            });
            //Pour chaque RA cochés
            //	- création input
            //	- ajout de l'input au form

            $.each(selectedRA, function (key, val) {
                $("<input>", {
                    "name": "listRA[" + key + "].id",
                    "value": val.id,
                    "hidden": true
                }).appendTo($tmpForm);
            });

            $tmpForm.appendTo('body').submit();
        }
    });


    // [AMA]AMNT-579] Prépare un PDF pour les sous-traitants
    $("#btnCreationMultiplePdfFirstPageStt").click(function (e) {
        var action = $(this).data("action");

        //Liste des cases cochées
        var selectedRA = $bootstrapTable2.bootstrapTable("getSelections");

        if (selectedRA.length > 0) {

            var $tmpForm = $("<form>", {
                "action": action,
                "method": "POST"
            });
            //Pour chaque RA cochés
            //	- création input
            //	- ajout de l'input au form

            $.each(selectedRA, function (key, val) {
                $("<input>", {
                    "name": "listRA[" + key + "].id",
                    "value": val.id,
                    "hidden": true
                }).appendTo($tmpForm);
            });


            $tmpForm.appendTo('body').submit();
        }
    });

});

// gère si le bouton de PDF est clickable ou non (tableau Collaborateurs)
function changeCollabPdfBtnState() {

    //[AMA][AMNT-579] on récupère le tableau concerné
    var $bootstrapTable = $("#idBootstrapTable1");

    //[AMA][AMNT-579] vérifie combien de RA sont selectionnés
    var selectedRA = $bootstrapTable.bootstrapTable("getSelections");

    //[AMA][AMNT-579] vérifie l'état du bouton de génération de PDF
    var firstButtonIsDisabled = $("#btnCreationMultiplePdfFirstPageCollab").prop('disabled');
    var secondButtonIsDisabled = $("#btnCreationMultiplePdfAllPagesCollab").prop('disabled');


    if (selectedRA.length > 0 && firstButtonIsDisabled) //[AMA][AMNT-579] si au moins un RA est sélectionné et que le bouton est inactif : on l'active
    {
        $("#btnCreationMultiplePdfFirstPageCollab").prop('disabled', false);
        $("#btnCreationMultiplePdfAllPagesCollab").prop('disabled', false);

    }
    else if (selectedRA.length === 0 && !firstButtonIsDisabled) //[AMA][AMNT-579] si aucun RA n'est selectionné et que le bouton est actif : on le desactive
    {
        $("#btnCreationMultiplePdfFirstPageCollab").prop('disabled', true);
        $("#btnCreationMultiplePdfAllPagesCollab").prop('disabled', true);
    }

};

// gère si lebouton de PDF est clickable ou non (tableau Sous-Traitant)
function changeSttPdfBtnState() {

    //[AMA][AMNT-579] on récupère le tableau concerné
    var $bootstrapTable = $("#idBootstrapTable2");

    //[AMA][AMNT-579] vérifie combien de RA sont selectionnés
    var selectedRA = $bootstrapTable.bootstrapTable("getSelections");

    //[AMA][AMNT-579] vérifie l'état du bouton de génération de PDF
    var firstButtonIsDisabled = $("#btnCreationMultiplePdfFirstPageStt").prop('disabled');

    if (selectedRA.length > 0 && firstButtonIsDisabled)
    {
        $("#btnCreationMultiplePdfFirstPageStt").prop('disabled', false); //[AMA][AMNT-579] si au moins un RA est sélectionné et que le bouton est inactif : on l'active
    }
    else if (selectedRA.length === 0 && !firstButtonIsDisabled)
    {
        $("#btnCreationMultiplePdfFirstPageStt").prop('disabled', true); //[AMA][AMNT-579] si aucun RA n'est selectionné et que le bouton est actif : on le desactive
    }

};

