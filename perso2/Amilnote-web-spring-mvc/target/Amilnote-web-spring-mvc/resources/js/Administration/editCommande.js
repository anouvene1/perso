/*
 * ©Amiltone 2017
 */

// [JNA][AMNOTE 53] Remplissage dynamique de la liste déroulante des missions en fonction du collab
function loadMission(idCollab) {
    var miss = $("#listeMissions"); // On définit la liste a remplir
    var id = idCollab.value; // on récupère l'ID du collab sélectionné
    $.ajax({
        type: 'GET',
        url: 'Administration/loadMission/' + id,
        dataType: 'json',
        success: function (json) {
            var firstId = false;
            $.each(json, function (index, elem) {
                miss.append('<option id="mission_' + elem.id + '" value="' + elem.id + '">' + elem.mission + '</option>'); // On récupère les
                if (!firstId) {
                    changeClient(document.getElementById("mission_" + elem.id));
                }
                firstId = true;
            });
            if(!firstId) {
                $('#responsableFacturation').html(''); // On vide la liste déroulante pour le prochain rafraichissement
                document.getElementById("choixRespFacturation").hidden = true; // On vide la liste déroulante pour le prochain rafraichissement
                document.getElementById("client_0").selected = true; //on repasse le client à rien du tout
                setValueIdResp();
            }
        }
    });
    $('#listeMissions').html(''); // On vide la liste déroulante pour le prochain rafraichissement
}

function changeClient(idMission) {
    var idMiss = idMission.value;
    $.ajax({
        type: 'GET',
        url: 'Administration/changeClient/' + idMiss,
        dataType: 'json',
        success: function (id) {
            document.getElementById("client_" + id).selected = true;
            changeRespFacturation(id);
        }
    });
}

function changeRespFacturation(idCl) {
    var div = document.getElementById("choixRespFacturation");
    //div.setAttribute("hidden","false");
    var miss = document.getElementById("responsableFacturation"); // On définit la liste a remplir
    var idClient = idCl;
    $.ajax({
        type: 'GET',
        url: 'Administration/changeRespFacturation/' + idClient,
        dataType: 'json',
        success: function (json) {
            var first = false;
            $.each(json, function (index, elem) {
                var el = document.createElement('option');
                el.setAttribute('id','respFact_' + elem.id);
                el.setAttribute('value',elem.id);
                el.innerHTML = elem.nom + " " + elem.prenom;
                miss.append(el); // On récupère les responsables
                first = true;
            });

            //Pour savoir si il existe au moins un contact
            if(first) {
                div.hidden=false;
            } else {
                div.hidden=true;
            }
            setValueIdResp();
        }
    });
    $('#responsableFacturation').html(''); // On vide la liste déroulante pour le prochain rafraichissement
}

function setValueIdResp() {
    var idResp=document.getElementById("responsableFacturation").value;
    document.getElementById("valueIdResp").setAttribute("value",idResp);
}

$(document).ready(function() {
    // To show datepicker calendar
    $(".datepicker, #ui-datepicker-div").datepicker({
        showOn: "both",
        dateFormat: 'yy-mm-dd'
    }).on("click", function() {
        $(".ui-datepicker-calendar").css("display", "block");
    });
});

