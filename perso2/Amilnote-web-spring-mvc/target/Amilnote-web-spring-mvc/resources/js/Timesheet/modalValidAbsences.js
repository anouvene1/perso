/*
 * ©Amiltone 2017
 */

function modalValidAbsences($modalValidAbsences) {
    var that = this;
    this.$that = $modalValidAbsences;

    this.$header = that.$that.find('.modal-header');
    this.$body = that.$that.find('.modal-body');
    // this.$footer = that.$that.find('.modal-footer');

    this.idTable = "idTable";

    this.messageUtilisateur = that.$that.find('#idMessageUtilisateur');
    this.$userComment = that.$that.find('#comment');
    this.$submitButton = that.$that.find('#btnValidAbsencesModal');
    that.$submitButton.click(function () {
        that.submitModal();
    });

    //Namespace pour les accesserurs sur la fenêtre modale (accessible dans modalValidAbsences() avec that.get.?)
    this.get = {
        rows: [],
        checkBoxHeader: function () {
            return that.$table.find('thead th input[type="checkbox"]');
        },
        tBodyTr: function () {
            return that.$table.find('tBody tr');
        },
        tBodyTrCheckBoxInput: function () {
            return that.get.tBodyTr().find('input[name="btSelectItem"]');
        },
        checkedRow: function () {
            that.get.rows = that.$table.bootstrapTable('getSelections');
            return that.get.rows;
        },
        checkedRowDataIndex: function () {
            var lCheckedRowDataIndex = [];
            $.each(that.get.checkedRow(), function (key, row) {
                var index = row.data("data-index");
                lCheckedRowDataIndexval.push(index);
            });
            return lCheckedRowDataIndex;
        },
        allRow: function () {
            var lAllRow = that.$table.bootstrapTable('getData');
            return lAllRow;
        }
    };
    this.init = function () {
        that.$body.empty();
        that.$body.append($('<table>').prop('id', that.idTable));

        that.$table = that.$body.find('#' + that.idTable);

        that.$table.bootstrapTable({
                data: getAbsenceBrouillon(),
                cache: false,
                striped: true,
                clickToSelect: true,
                height: 400,
                columns: [{
                    field: 'state',
                    checkbox: true
                }, {
                    field: 'dateDebut',
                    title: 'Début',
                    align: 'center',
                    valign: 'middle',
                    sortable: true,
                    formatter: formatter.dateAbsence
                }, {
                    field: 'dateFin',
                    title: 'Fin',
                    align: 'center',
                    valign: 'middle',
                    sortable: true,
                    formatter: formatter.dateAbsence
                }, {
                    field: 'nbJours',
                    title: 'Durée',
                    align: 'center',
                    sortable: true,
                    valign: 'middle'
                }, {
                    field: "typeAbsence",
                    title: 'Type',
                    align: 'center',
                    valign: 'middle',
                    sortable: true,
                    formatter: function (value, row, index) {
                        if (null != value)
                            return value.typeAbsence;
                    },
                    sorter: function(a, b) {
                        if (a.typeAbsence > b.typeAbsence)
                            return 1;
                        if (a.typeAbsence < b.typeAbsence)
                            return -1;
                        return 0;
                    }
                }, {
                    field: "etat",
                    title: "État",
                    align: 'center',
                    valign: 'middle',
                    sortable: false,
                    formatter: function (value, row, index) {
                        if (null != value)
                            return value.etat;
                    }
                }]

            }
        );

        that.$table.on('check.bs.table', function (e, row) {
            that.disableOrNotSubmitButton();
        });
        that.$table.on('uncheck.bs.table', function (e, row) {
            that.disableOrNotSubmitButton();
        });
        that.$table.on('uncheck-all.bs.table', function (e, row) {
            that.disableOrNotSubmitButton();
        });
        that.$table.on('check-all.bs.table', function (e, row) {
            that.disableOrNotSubmitButton();
        });

        that.$that.modal({backdrop: 'static'});
        that.disableOrNotSubmitButton();
        this.$userComment.prop('value', '');
    };
    this.checkModal = function () {
        if (that.get.checkedRow().length > 0) {
            that.messageUtilisateur.empty();
            return true;
        } else {
            that.messageUtilisateur.html('Au moins une case doit être cochée.');
            return false;
        }
    };
    this.disableOrNotSubmitButton = function () {
        if (that.checkModal()) {
            that.$submitButton.prop('disabled', false);
        } else {
            that.$submitButton.prop('disabled', true);
        }
    };
    this.submitModal = function () {
        if (that.checkModal()) {
            var checkedData = that.get.rows;
            var listId = [];

            $.each(checkedData, function (key, val) {
                var jsonVal = {};
                // Can not construct instance of long from String value 'idTable':
                if( val.id !== 'idTable') {
                    jsonVal['id'] = val.id;
                    listId.push(jsonVal);
                }
            });

            var comment = document.getElementById("comment").value;
            updateAbsenceBrouillon(listId, comment); //mise à jour de l'état des absences soumises
        }
    }
}
