<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>
<link rel="stylesheet" href="resources/css/editDeplacement.css">

<script>
    function remplaceVirgule(input) {
        input.value = input.value.replace(',', '.');
    }

    function valider(form) {
        var dateDebut = document.getElementById("dateDebut").value;
        var dateFin = document.getElementById("dateFin").value;
        var idMission = document.getElementById("idMission").value;
        var res = verifPresenceMissionPeriode(dateDebut, dateFin, idMission);
        var $modalWarning = $("#modalWarning");
        if (res == "-1") {
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Erreur dans la sélection des dates. Celles-ci ne correspondent pas à la mission séléctionnée.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            //alert("Erreur dans la sélection des dates. Celles-ci ne correspondent pas à la mission séléctionnée.");
            return false;
        }
        else if (res == "0") {
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Erreur dans la sélection de la date. Celle-ci correspond à une absence.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            //alert("Erreur dans la sélection de la date. Celle-ci correspond à une absence.");
            return false;
        }
        else {
            return true;
        }
    }
</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li class="active">Nouvelle demande de déplacement</li>
</ol>

<div id="idProfilContainer" class="form-horizontal">
    <fieldset>
        <legend class="text-center">
            Création d'une demande de déplacement :
        </legend>

        <form:form method="post" action="${saveDeplacement}" enctype="multipart/form-data" class="form-horizontal"
                   commandName="demandeDeplacement" name="formulaire" onsubmit="return valider(this)">

            <c:if test="${not empty error}">
                <div class="errorblock">
                        ${error}
                </div>
            </c:if>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="mission">Mission</label>
                <div class="col-md-4">
                    <select class="form-control" name="idMission" id="idMission">
                        <c:forEach var="optionMission" items="${listMission}">
                            <option value="${optionMission.id}">${optionMission.mission}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="dateDebut">Date
                    de début<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input type="text"
                                id="dateDebut"
                                path="dateDebut"
                                class="form-control input-md datepicker"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                    autocomplete="off"/>
                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="dateFin">Date
                    de fin<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input type="text"
                                id="dateFin"
                                path="dateFin"
                                class="form-control input-md datepicker"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                    autocomplete="off"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="motif">Motif</label>
                <div class="col-md-4">
                    <form:textarea id="motif"
                                   path="motif"
                                   placeholder="Motif"
                                   class="form-control input-md"
                    />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="codeProjet">Code projet client</label>
                <div class="col-md-4">
                    <form:input cssClass="form-control input-md"
                                id="codeProjet"
                                path="codeProjet"
                                placeholder="Code Projet Client"
                                class="form control"/>

                </div>
            </div>
            <!-- <div class="form-group">
            <label class="col-md-4 control-label" for="idValidationClient">Validation Client</label>
            <div class="col-md-4">
            <form:select id=""
                         class="form-control"
                         path="validationClient">
                <form:option value="true">Oui</form:option>
                <form:option value="false">Non</form:option>
                <form:option value="false">En cours</form:option>
            </form:select>
            </div>
            </div> -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="lieu">Lieu<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input cssClass="form-control input-md"
                                id="lieu"
                                path="lieu"
                                placeholder="Lieu"
                                class="form control"
                                required="required"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="commentaire">Commentaire</label>
                <div class="col-md-4">
                    <form:textarea id="commentaire"
                                   path="commentaire"
                                   placeholder="Commentaire (voiture,...)"
                                   class="form-control input-md"/>
                </div>
            </div>


            <!-- Boutons actions -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <a class="btn btn-danger" href="${welcome}"
                       role="button">Retour</a>
                    <button id="idSubmitCollaborateur" type="submit"
                            name="SubmitCollaborateur"
                            class="btn btn-success">Soumettre
                    </button>
                </div>
            </div>
        </form:form>
    </fieldset>
</div>
