<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>
<head>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/x64-core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/sha512.js"></script>
</head>

<script>
    function hashAndSubmit() {
        var hashedAncien;
        var hashedNouveau;
        var hashedConfirmation;
        var ancienToHash = $('#ancienToHash').val();
        var nouveauToHash = $('#nouveauToHash').val();
        var confirmationToHash = $('#confirmationToHash').val();

        hashedAncien = CryptoJS.SHA512(ancienToHash).toString();
        hashedNouveau = CryptoJS.SHA512(nouveauToHash).toString();
        hashedConfirmation = CryptoJS.SHA512(confirmationToHash).toString();

        $('#ancien').val(hashedAncien);
        $('#nouveau').val(hashedNouveau);
        $('#confirmation').val(hashedConfirmation);
        $('#ancienToHash').val("");
        $('#nouveauToHash').val("");
        $('#confirmationToHash').val("");
    }
</script>
<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>


<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li class="active">Gestion du profil</li>
</ol>

<div id="idTooltipWelcome" class="alert alert-info alert-dismissible" role="alert">
    <h4>Profil de ${prenomNomCollab}</h4>
    <h6>${mail}</h6>
</div>
<!-- Bloc de changement de mot de passe -->
<div class="center-block ">
    <div class="panel-primary col-md-6">
        <div class="panel-heading text-center">Changement de mot de passe</div>
        <div class="panel-body">
            <form method="post" action="Profil/resetMdp" class="form-horizontal">
                <div class="input-group">
                    <span class="input-group-addon" style="width:10em;text-align:right;">Ancien :</span>
                    <input id="ancienToHash" type="password" class="form-control" placeholder="ancien mot de passe" name="ancienToHash"
                           maxlength="32" size="32">
                    <input id="ancien" type="password" name="ancien" hidden>
                </div>
                <div class="input-group">
                    <span class="input-group-addon" style="width:10em;text-align:right;">Nouveau :</span>
                    <input id="nouveauToHash" type="password" class="form-control" placeholder="nouveau mot de passe" name="nouveauToHash"
                           maxlength="32" size="32">
                    <input id="nouveau" type="password" name="nouveau" hidden>
                </div>
                <div class="input-group">
                    <span class="input-group-addon" style="width:10em;text-align:right;">Confirmation :</span>
                    <input id="confirmationToHash" type="password" class="form-control" placeholder="confirmation mot de passe"
                           name="confirmationToHash" maxlength="32" size="32">
                    <input id="confirmation" type="password" name="confirmation" hidden>
                </div>
                <div class="form-group">
                    <!-- Colonne vide pour aligner le bouton aux champs de saisies -->
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <input class="btn btn-success" type="submit" value="Modifier" onclick="hashAndSubmit()"><br/>
                    </div>
                </div>
                <c:if test="${errorValidationMDP}">
                    <span style="color:red;font-size:large;">${messageValidationMDP}</span>
                </c:if>
                <c:if test="${!errorValidationMDP}">
                    <span style="color:green;font-size:large;">${messageValidationMDP}</span>
                </c:if>
            </form>
        </div>
    </div>
</div>
<!-- Bloc statistique -->
<div class="center-block ">
    <div class="panel-primary col-md-6">
        <div class="panel-heading text-center">Statistiques</div>
        <div class="panel-body">
            <h4 style='text-align:center;'>À VENIR</h4>
        </div>
    </div>
</div>
