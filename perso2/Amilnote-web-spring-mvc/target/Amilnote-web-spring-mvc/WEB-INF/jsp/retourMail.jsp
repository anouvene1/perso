<%--
  ~ ©Amiltone 2017
--%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<head>
    <link rel="stylesheet" type="text/CSS" href="${contextPath}/resources/css/addvise.css"/>
</head>
<body>
<section id="messageReponseMailAddvise">
    <p>Bonjour ${invitation.collaborateur.prenom},</p>
    <c:if test="${invitation == null}"> L'invitation n'est plus modifiable. </c:if>
    <c:if test="${ invitation != null}">
        <p>Tu as
            <c:if test="${invitation.etatInvitation == 'CONFIRMEE'}"> confirmé </c:if>
            <c:if test="${invitation.etatInvitation == 'DECLINEE'}"> décliné </c:if>
            l'invitation pour la session ${invitation.session.moduleAddvise.nomModuleAddvise} du programme Addvise pour la date du <fmt:formatDate value="${invitation.session.date}" pattern="dd/MM/yyyy"/>.
        </p>
        <p>Merci pour ta réponse.</p>
    </c:if>
</section>
</body>
