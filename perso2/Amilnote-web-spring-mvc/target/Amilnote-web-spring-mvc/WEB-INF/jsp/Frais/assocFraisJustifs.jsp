<%--
  ~ ©Amiltone 2017
  --%>

<%--

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script> 
    var urlDelate = "mesNotesDeFrais/deleteJustif/";
</script>
<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden = true>
		<button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
		<p> -- </p>
</section>
<ol class="breadcrumb">
	<li><a href="${ welcome }">AmilNote</a></li>
	<li><a href="${ mesNotesDeFrais }${ frais }/null">Mes Notes de
			frais</a></li>
	<li class="active">Association Frais Justificatifs</li>
</ol>
	<fieldset>
		<!-- Form Name -->
		<legend class="text-center"> Liste des justificatifs existants</legend>
		<div id="idTableContainer">
			<p class="messageSucces">${messageSucces}</p>
			<p class="messageErreur">${messageErreur}</p>
			
				<div id="toolbar">
				</div>
				<form id="formAssocier" method="post" action="${mesNotesDeFrais}/associJustif/${idFrais}">
					<c:choose>
						<c:when test="${assocJustif}">
							<a class="btn btn-danger" href="${mesNotesDeFrais}${editFrais}/${idFrais}" role="button">Retour</a>
							<button id="btnAssocier" type="submit" class="btn btn-success" disabled>Associer au frais</button>
						</c:when>
						<c:otherwise>
							<a class="btn btn-danger" href="${mesNotesDeFrais}${frais}/null" role="button">Retour</a>
						</c:otherwise>
					</c:choose>
				</form>
				<table 	data-toggle="table"
						data-height="800" 
						data-search="true"
						data-striped="true" 
						data-toolbar="#toolbar"
						data-cookie="true"
						data-cookie-id-table="saveIdAssocFraisJustifs">
					<thead>
						<tr>
							<c:if test="${assocJustif}"><th data-field="state" data-checkbox="false"></th></c:if>
							<th data-field="justif" data-sortable="true" data-align="center">Nom du justificatif</th>
							<th data-align="center">Visualiser</th>
							<th data-align="center">Supprimer</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listJustifs}" var="justif" varStatus="indice">
							<tr>
								<c:if test="${assocJustif}"><td><input type="checkbox" id="idJustif${indice.index}" name="idJustif" value="${justif.id}" form="formAssocier"/></td></c:if>
								<td>${justif.descriptif}</td>
								<td>
									<a href="${mesNotesDeFrais}${download}/${justif.id}" target="_blank">
										<span class="glyphicon glyphicon-eye-open"></span>
									</a>
								</td>
								<td>
									<a onclick = "eventClikDelate(urlDelate + ${justif.id})"> 
										<span class="glyphicon glyphicon-remove"></span>
									</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		</div>
	</fieldset>

<script type="text/javascript">
$(document).ready(function() {
		// pour chaque checkbox, activer le bouton Associer dès qu'au moins une checkbox est cochée. Sinon désactiver le bouton.
	    $('input[type="checkbox"][name="idJustif"]').click(function() {
			if ($('input[type="checkbox"][name="idJustif"]:checked').length > 0){
				$('#btnAssocier').removeAttr("disabled");
			} else {
				$('#btnAssocier').attr("disabled","disabled");
			}
	    });
	});
</script>

<div class="modal fade" id="warning-delete-Action" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Suppression d'un justificatif</h3>
			</div>
			<div class="modal-body" id="warning-delete-Collaborateur-body">
				<h4>Etes-vous sûre de vouloir supprimer ce justificatif ?</h4>
				<br>
			</div>
			<div class="modal-footer">
				<button type="button" id="delete-Action"
					class="btn btn-success dismiss" data-dismiss="modal">Oui</button>
				<button type="button" class="btn btn-danger dismiss"
					data-dismiss="modal">Non</button>
			</div>

		</div>
	</div>
</div>--%>
