<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  ~ ©Amiltone 2017
  --%>

<script>
    var urlDelate = "mesNotesDeFrais/deleteFrais/";
</script>

<script type="text/javascript" src='resources/js/NotesDeFrais/notesFrais.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li class="active" id="filtre">Mes notes de fraisdfghj</li>
</ol>

<script>

    $(function () {
        $('#idBootstrapTable').bootstrapTable(); // init via javascript

        //resize columns with window
        $(window).resize(function () {
            $('#idBootstrapTable').bootstrapTable('resetView');
        });
    });

    nomFichier = document.location.href.substring(document.location.href.lastIndexOf("/") + 1);
    if (nomFichier == "VA") {
        nomFichier = "(Validées)";
    }
    else if (nomFichier == "SO") {
        nomFichier = "(Soumises)";
    }
    else if (nomFichier == "SD") {
        nomFichier = "(Soldées)";
    }
    else if (nomFichier == "BR") {
        nomFichier = "(Brouillons)";
    }
    else {
        nomFichier = "";
    }
    document.getElementById("filtre").innerHTML = "Mes notes de frais " + nomFichier;
</script>

<div id="idTableContainer" class="form-horizontal">
    <div class="row">
        <c:if test="${not empty error}">
            <div class="errorblock">
                    ${error}
            </div>
        </c:if>
        <h3>Nouveau frais</h3>
        <div class="form-inline-frais">
            <form class="form-inline" method="post" action="${mesNotesDeFrais}${editFrais}">
                <div class="form-group">
                    <label for="idMission">Mission</label>
                    <select class="form-control" name="idMission" id="idMission">
                        <c:forEach var="optionMission" items="${listMission}">
                            <option value="${optionMission.id}" selected="">${optionMission.mission}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label for="typeFrais">Type de frais</label>
                    <select class="form-control" name="typeFrais" id="typeFrais">
                        <c:forEach var="optionTypeFrais" items="${listTypeFrais}">
                            <option value="${optionTypeFrais.code}">${optionTypeFrais.typeFrais}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Nouveau frais</button>
                </div>
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        <h3>Mes frais</h3>
        <a href="${mesNotesDeFrais}${frais}/null" id="lienFiltre">Tous</a>
        <a href="${mesNotesDeFrais}${frais}/BR" id="lienFiltre">Brouillons</a>
        <a href="${mesNotesDeFrais}${frais}/VA" id="lienFiltre">Validés</a>
        <a href="${mesNotesDeFrais}${frais}/SO" id="lienFiltre">Soumis</a>
        <a href="${mesNotesDeFrais}${frais}/SD" id="lienFiltre">Soldés</a>
        <div id="mini-scrolltab">
            <table data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbar"
                   data-pagination="true"
                   data-page-size=25
                   data-page-list="[10, 25, 50, 100, ${listFrais.size()}]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="right"
                   data-pagination-detail-h-align="left"
                   id="idBootstrapTable"
                   data-cookie="true"
                   data-cookie-id-table="saveIdFrais">
                <thead>
                <tr>
                    <th data-field="typeFrais" data-sortable="true" data-align="center">Type de frais</th>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="depense" data-sortable="true" data-align="center">Montant remboursé</th>
                    <th data-field="date" data-sortable="true" data-align="center" data-sorter="ddMMyyyyCustomSorter">
                        Date
                    </th>
                    <th data-field="state" data-sortable="true" data-align="center">État</th>
                    <th data-field="motif" data-sortable="true" data-align="center">Motif</th>
                    <th data-field="justif" data-sortable="true" data-align="center">Justificatif(s)</th>
                    <th data-align="center">Édition</th>
                    <th data-align="center">Suppression</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listFrais}" var="frais">
                    <tr>
                        <!-- Type de Frais -->
                        <td>${frais.typeFrais}
                            <c:if test="${frais.fraisForfait == 'FRF' }"><br/> (- le forfait)</c:if>
                            <c:if test="${frais.fraisForfait == 'FAF' }"><br/> (+ le forfait)</c:if>
                            <c:if
                                test="${frais.typeAvance == 'PE' && frais.typeFrais == 'Avance sur frais'}"><br/>(Permanente)</c:if>
                            <c:if
                                test="${frais.typeAvance == 'PO' && frais.typeFrais == 'Avance sur frais'}"><br/>(Ponctuelle)</c:if>
                        </td>
                        <!-- Mission -->
                        <td>${frais.mission}</td>
                        <!-- Dépense -->
                        <c:if test="${frais.typeFrais == 'Voiture personnelle'}">
                            <td>${frais.nombreKm} km</td>
                        </c:if>
                        <c:if test="${frais.typeFrais != 'Voiture personnelle'}">
                            <td>${frais.montant} €</td>
                        </c:if>
                        <!--  Date -->
                        <td><fmt:formatDate value="${frais.dateEvenement}" pattern="dd/MM/yyyy"/></td>
                        <!-- Etat -->
                        <c:if test="${frais.etat.etat == 'Brouillon' && frais.typeFrais == 'Avance sur frais'}">
                            <td>
                                <form action="${mesNotesDeFrais}${soumettreAvance}/${frais.id}"
                                      style='margin-bottom:0px;'>
                                    <button class="btn btn-success">Soumettre</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${!(frais.etat.etat == 'Brouillon' && frais.typeFrais == 'Avance sur frais')}">
                            <td>${frais.etat.etat}</td>
                        </c:if>
                        <!-- Motif -->
                        <td>${fn:escapeXml(frais.commentaire)}</td>
                        <!-- Justificatif(s) -->
                        <c:if test="${frais.hasJustif == 0}">
                            <td>
                                -
                            </td>
                        </c:if>
                        <c:if test="${frais.hasJustif >=1}">
                            <td>
                                <c:forEach items="${frais.justifs}" var="justif">
                                    ${fn:escapeXml(justif.descriptif)}
                                    <a href="${mesNotesDeFrais}${download}/${justif.id}" target="_blank">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                    <br>
                                </c:forEach>
                            </td>
                        </c:if>
                        <!-- Edition -->
                        <c:if test="${frais.etat.etat == 'Brouillon'}">
                            <td><a href="${mesNotesDeFrais}${editFrais}/${frais.id}">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a></td>
                        </c:if>
                        <c:if test="${frais.etat.etat != 'Brouillon'}">
                            <td>
                                -
                            </td>
                        </c:if>
                        <!-- Suppression -->

                        <c:choose>
                            <c:when test="${frais.etat.etat == 'Brouillon'}">
                                <td>
                                    <a onclick="eventClikDelate(urlDelate + ${frais.id})">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    -
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="warning-delete-Action" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Suppression d'un frais</h3>
            </div>
            <div class="modal-body" id="warning-delete-Collaborateur-body">
                <h4>Êtes-vous sûr de vouloir supprimer ce frais ?</h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-Action"
                        class="btn btn-success dismiss" data-dismiss="modal">Oui
                </button>
                <button type="button" class="btn btn-danger dismiss"
                        data-dismiss="modal">Non
                </button>
            </div>

        </div>
    </div>

</div>

<!-- pop in pour la vue en grand des justificatifs -->
<section class="modal fade" id="img-full">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Votre justificatif</h4>
                <!-- <p>Pour firefox, merci de cocher "aperçu dans firefox" dans
                    Options>Applications</p>-->
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</section>
