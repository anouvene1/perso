<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Liste de soumissions des RA</li>
</ol>

<script>
    nomFichier = document.location.href.substring(document.location.href.lastIndexOf("/") + 1);
    if (nomFichier == "BR") {
        nomFichier = "(Brouillon)";
    }
    else if (nomFichier == "SO") {
        nomFichier = "(Soumis)";
    }
    else {
        nomFichier = "";
    }
    document.getElementById("filtre").innerHTML = "Liste de soumissions des RA " + nomFichier;
</script>

<div id="idTableContainer">

    <a onclick="eventClikOnBtnSendRappel()"
       type="button" class="btn btn-lg btn-default btn-action vaderBlue" id="btn-sendRappelRA" style="float:right;">
        <span class="glyphicon glyphicon-envelope"></span>
        Envoyer mail de rappel validation RA
    </a>

    <h3>Liste de soumission des rapports d'activités du mois</h3>
    <a href="${administration}${listeSoumissionRA}/null" id="lienFiltre">Tous</a>
    <a href="${administration}${listeSoumissionRA}/BR" id="lienFiltre">Brouillon</a>
    <a href="${administration}${listeSoumissionRA}/SO" id="lienFiltre">Soumis</a>
    <div id="scrolltab">
        <table id="idBootstrapTable"
               data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbar"
               data-pagination="true"
               data-page-size=100
               data-page-list="[10, 25, 50, 100, ${listeSoumis.size()}]"
               data-pagination-v-align="top"
               data-pagination-h-align="right"
               data-pagination-detail-h-align="left"
               data-cookie="true"
               data-cookie-id-table="saveIdlistRA">
            <thead>
            <tr>
                <th data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <th data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                <th data-field="mail" data-sortable="true" data-align="center">Mail</th>
                <th data-field="state" data-sortable="true" data-align="center">État</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listeSoumis}" var="listeSoumis">
                <tr>
                    <td>${listeSoumis.collaborateur.nom}</td>
                    <td>${listeSoumis.collaborateur.prenom}</td>
                    <td>${listeSoumis.collaborateur.mail} </td>
                    <td>${listeSoumis.etat.etat}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
