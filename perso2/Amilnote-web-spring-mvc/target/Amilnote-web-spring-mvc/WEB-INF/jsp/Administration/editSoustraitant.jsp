<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<!-- Edition ou Ajout d'un Soustraitant -->

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${ administration }${ travailleurs }#idTableContainerSS">Sous-traitants</a></li>
    <c:choose>
        <c:when test="${!empty soustraitant.id}">
            <li class="active">Édition d'un sous-traitant</li>
        </c:when>
        <c:otherwise>
            <li class="active">Création d'un sous-traitant</li>
        </c:otherwise>
    </c:choose>
</ol>

<form:form class="form-horizontal" method="POST"
           commandName="soustraitant"
           action="${ administration }${saveSousTraitant}">

    <div id="idProfilContainer">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center">

                <c:choose>
                    <c:when test="${!empty soustraitant.id}">
                        Modification du sous-traitant :   ${ soustraitant.mail }
                    </c:when>
                    <c:otherwise>
                        Création d'un sous-traitant 
                    </c:otherwise>
                </c:choose>

            </legend>
            <form:hidden path="id"/>
            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idEmailColl">Email<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idEmailColl" path="mail" type="email"
                                placeholder="" class="form-control input-md"
                                data-validation="custom"
                                data-validation-regexp="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|"
                                data-validation-help="Vous devez entrer une adresse mail en .fr ou .com"/>
                </div>
            </div>

            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idNomColl">Nom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idNomColl" path="nom" type="text" placeholder=""
                                class="form-control input-md" data-validation-length="2-50"
                                data-validation="length"/>
                </div>
            </div>

            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idPrenomColl">Prénom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idPrenomColl" path="prenom" type="text"
                                placeholder="" class="form-control input-md"
                                data-validation-length="2-50" data-validation="length"/>
                </div>
            </div>

            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idRaisonSocStt">Raison sociale<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idRaisonSocStt" path="raisonSociale" type="text"
                                placeholder="" class="form-control input-md"
                                data-validation-length="2-50" data-validation="length"/>
                </div>
            </div>

            <!-- SBE_185 adresse Postal -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idAdresseColl">Adresse</label>
                <div class="col-md-4">
                    <form:input id="idAdresseColl" path="adressePostale" type="text" placeholder=""
                                class="form-control input-md" data-validation-length="0-500"
                                data-validation="length"/>
                </div>
            </div>

            <!-- Telephone -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idTelColl">Téléphone</label>
                <div class="col-md-4">
                    <form:input id="idTelColl" path="telephone" type="tel"
                                class="form-control input-md" data-validation="custom"
                                data-validation-regexp="^0[1-79][0-9]{8}$|^$"
                                data-validation-help="Veuillez entrer un numéro de téléphone valide en 10 chiffres."/>
                </div>
            </div>

            <!-- Select manager -->
            <div class="form-group">
                <!-- Si le collaborateur n'est pas un manager ou un admin
                ( codeManager et codeAdmin viennent d'attributs issues du controller -->

                <!-- Si l'attribut existe on affiche la liste -->
                <c:if test="${not empty listManagers}">

                    <label class="col-md-4 control-label" for="listManagers">Choix
                        du prescripteur</label>
                    <div class="col-md-4">
                        <select name="prescripteur.id" class="form-control">
                            <c:forEach var="optionManager" items="${listManagers}">

                                <option value="${optionManager.id}"
                                    <c:if test="${optionManager.id == soustraitant.prescripteur.id}">
                                        <c:out value='selected="selected"'/>
                                    </c:if>>
                                        ${optionManager.prenom} ${optionManager.nom} -
                                        ${optionManager.mail}</option>

                            </c:forEach>
                        </select>
                    </div>
                </c:if>

            </div>

            <br><br>

            <!-- Boutons actions -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <a class="btn btn-danger" href="${administration}${travailleurs}"
                       role="button">Retour</a>
                    <button id="idSubmitSoustraitant" type="submit"
                            name="submitSoustraitant" class="btn btn-success">Valider
                    </button>
                </div>
            </div>


        </fieldset>
    </div>
</form:form>

<fieldset>
    <!-- Form Name -->
    <legend class="text-center"><br>Liste des missions</legend>

    <div id="idTableContainer">
        <div>
            <a
                href="${administration}${editSoustraitant}/${soustraitant.id}${editMission}"
                class="btn btn-success"
                <c:if test="${empty soustraitant.id }"> disabled="true" </c:if>>Nouvelle
                mission </a>
        </div>
        <div id="stt-scrolltab">
            <table data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbar"
                   data-pagination="true"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-page-size=100
                   id="idBootstrapTable"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditStt">
                <thead>
                <tr>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="dateDebut" data-sortable="true" data-align="center"
                        data-sorter="ddMMyyyyCustomSorter">Date de début
                    </th>
                    <th data-field="dateFin" data-sortable="true" data-align="center"
                        data-sorter="ddMMyyyyCustomSorter">Date de fin
                    </th>
                    <th data-align="center">Édition</th>
                    <th data-align="center">Visualiser</th>
                    <th data-align="center">Suppression</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listMissions}" var="mission">
                    <tr>
                        <td>${mission.mission}</td>
                        <td><fmt:formatDate value="${mission.dateDebut}"
                                            pattern="dd/MM/yyyy"/></td>
                        <td><fmt:formatDate value="${mission.dateFin}"
                                            pattern="dd/MM/yyyy"/></td>
                        <td><a
                            href="${administration}${editSoustraitant}/${soustraitant.id}${editMission}/${mission.id}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${mission.pdfODM !='null' }">
                                    <a href="${administration}${editSoustraitant}/${soustraitant.id}${editMission}/${mission.id}${download}"
                                       target="_blank">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-eye-close"></span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td><a onclick="eventClikDelate(urlDelate + ${mission.id})">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</fieldset>

<c:if test="${!empty soustraitant.id}">
    <script>
        $(function () {
            $('.monthPicker').change(function () {
                window.location.href = window.location.pathname + "?monthYearExtract=" + this.value;
            });
        });

        function annulerFiltre() {
            window.location.href = window.location.pathname;
        }

    </script>

    <style>
        #checkbox-addvise {margin-top: 10px; margin-left: 34.4%;}
    </style>
</c:if>

<div class="modal fade" id="warning-delete-Action" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Suppression d'une mission</h3>
            </div>
            <div class="modal-body" id="warning-delete-Mission-body">
                <h4>Êtes-vous sûr de vouloir supprimer cette mission ?</h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-Action"
                        class="btn btn-success dismiss" data-dismiss="modal">Oui
                </button>
                <button type="button" class="btn btn-danger dismiss"
                        data-dismiss="modal">Non
                </button>
            </div>
        </div>
    </div>
</div>
