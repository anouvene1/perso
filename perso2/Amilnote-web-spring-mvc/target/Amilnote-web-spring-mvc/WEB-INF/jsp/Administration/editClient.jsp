<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
  ~ ©Amiltone 2017
  --%>

<!-- Edition ou Ajout d'un client -->
<script>
    var lstContacts = ${listContactsAsJson};
</script>
<script type="text/javascript" src="resources/js/Administration/editClient.js"></script>
<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${ administration }${ clients }">Clients</a></li>
    <c:choose>
        <c:when test="${!empty client.id}">
            <li class="active">Édition d'un client</li>
        </c:when>
        <c:otherwise>
            <li class="active">Création d'un client</li>
        </c:otherwise>
    </c:choose>
</ol>

<form:form class="form-horizontal" method="POST"
           commandName="client"
           action="${ administration }${saveClient}">

    <div id="idProfilContainer">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center">

                <c:choose>
                    <c:when test="${!empty client.id}">
                        Modification du client :   ${ client.nom_societe }
                    </c:when>
                    <c:otherwise>
                        Création d'un client
                    </c:otherwise>
                </c:choose>

            </legend>

            <form:hidden path="id"/>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idNomColl">Nom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idNomColl" path="nom_societe" type="text" placeholder=""
                                class="form-control input-md" required="required"/>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label">Adresse du siège<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <input id="adresseClient" name="adresseClient"
                           placeholder="N° et rue, CP et ville" class="form-control input-md"
                           required="required" value="${adresseClientSiege}"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Adresse de facturation<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <input id="adresseFacturation" name="adresseFacturation" pattern="^[^,]+, ?[0-9]{5}[^,]+$"
                           TITLE="n'oubliez pas la virgule juste avant le code postal"
                           placeholder="N° et rue, CP et ville" class="form-control input-md"
                           value="${adresseClientFacturation}"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Auxiliaire<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <input id="auxiliaire" name="auxiliaire"
                           placeholder="Code Auxiliaire" class="form-control input-md"
                           required="required" value="${auxiliaire}"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Libellé comptable<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <input id="libelle_comptable" name="libelle_comptable"
                           placeholder="Valeur libelle client comptable" class="form-control input-md"
                           required="required" value="${libelle_comptable}"/>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label">N° TVA Intrac</label>
                <div class="col-md-4">
                    <form:input type="text" id="idTvaClient" name="tvaClient"
                                placeholder="N° TVA Intrac du client" class="form-control input-md"
                                path="tva"/>
                </div>
            </div>

            <!-- RIB selector-->
            <div class="form-group ribSelecteur">
                <label class="col-md-4 control-label"> RIB </label>
                <div class="col-md-4">
                    <form:select path="ribClient" class="form-control"
                                 placeholder="Sélectionner un moyen de payement">
                        <c:forEach items="${listeRIB}" var="rib">
                            <c:choose>
                                <c:when test="${rib == selectedRIB}">
                                    <form:option value="${rib}" selected="true">${rib}</form:option>
                                </c:when>
                                <c:otherwise>
                                    <form:option value="${rib}">${rib}</form:option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </div>
            </div>

            <!-- Boutons actions -->
            <div class="form-group">
                <label class="col-md-4 control-label" ></label>
                <div class="col-md-8">
                    <a class="btn btn-danger" href="${administration}${clients}"
                       role="button">Retour</a>
                    <button onclick="essai(adresseFacturation)" id="idSubmitCollaborateur" type="submit"
                            name="submitCollaborateur" class="btn btn-success">Valider
                    </button>
                </div>
            </div>

        </fieldset>
    </div>
</form:form>

<fieldset>
    <!-- Form Name -->
    <legend class="text-center"><br>Liste des contacts</legend>

    <div id="idTableContainer">
        <div>
            <a
                href="${administration}${editClient}/${client.id}/${editContactClient }"
                class="btn btn-success"
                <c:if test="${empty client.id }"> disabled="true" </c:if>>Nouveau contact </a>
            <a
                href="${administration}/${addDefaultContactClient}"
                class="btn btn-warning"
                <c:if test="${fn:length(listContacts) > 0}"> style="display: none;" </c:if>>Ajouter contact par défaut </a>
        </div>
        <table class="table"
               data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbar"
               data-pagination="true"
               data-page-size=50
               id="idBootstrapTable"
               data-cookie="true"
               data-cookie-id-table="saveIdEditClient">
            <thead>
            <tr>
                <th data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <th data-field="prenom" data-sortable="true" data-align="center">Prenom</th>
                <th data-field="mail" data-sortable="true" data-align="center">Mail</th>
                <th data-field="tel" data-sortable="true" data-align="center">Téléphone</th>
                <th data-field="role" data-sortable="true" data-align="center">Rôle(s)</th>
                <th data-align="center">Édition</th>
                <th data-align="center">Action</th>
            </tr>
            </thead>
            <tbody id="clientContactRows">
                <c:forEach items="${listContacts}" var="contact" varStatus="count">
                    <tr>
                        <td>${contact.nom}</td>
                        <td>${contact.prenom}</td>
                        <td>${contact.mail}</td>
                        <td>${contact.telephone}</td>
                        <td>
                            <c:choose>
                                <c:when
                                    test="${contact.respClient && contact.respFacturation}">Responsable client & facturation
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${contact.respFacturation}">Responsable facturation</c:when>
                                        <c:when test="${contact.respClient}">Responsable client</c:when>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="contactEditLink" data-url="${administration}${editClient}/${client.id}${editContactClient}/${contact.id}">
                            <c:if test="${contact.enabled}">
                                <a class="contactEdit" href="${administration}${editClient}/${client.id}${editContactClient}/${contact.id}">
                                    &Eacute;diter
                                </a>
                            </c:if>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${contact.enabled}">
                                    <a class="contactAction" href="#" title="Désactiver le contact" data-contactindex="${count.index}">
                                        Désactiver
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a class="contactAction" href="#" title="Activer le contact"  data-contactindex="${count.index}">
                                        Activer
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</fieldset>