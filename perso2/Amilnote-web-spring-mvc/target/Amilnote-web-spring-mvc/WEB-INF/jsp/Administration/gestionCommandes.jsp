<%--
  ~ ©Amiltone 2017
  --%>

<%--
  Created by: azicaro
  Date: 13/09/2016
  Time: 16:34
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="resources/js/Administration/confirm.js"></script>
<script type="text/javascript" src="resources/js/Administration/gestionCommandes.js"></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<section id="msgErreurExport" class="alert alert-danger" role="alert" hidden="hidden">
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p>Un problème est survenu lors de l'export du fichier.</p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Commandes</li>
</ol>

<div id="idTableContainer">

    <h3>Gestion des commandes ${filtre == "CO" ? " en cours" : "terminées"}</h3>

    <FORM id="formFiltre" method="GET">
        <div>
            <input name="monthYearExtract"
                   id="monthExtract"
                   class="monthPicker form-control"
                   placeholder="MM/AAAA"
                   autocomplete="off"
                   value="${monthYearExtract}"
            >
            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </FORM>

    <div id="mini-scrolltab">

        <div id="toolbarCommandes">
            <a
                href="${administration}${editCommande}/-1"
                class="btn btn-success"> Nouvelle commande </a>

            <!-- boutons pour accéder aux commandes terminées ou en cours -->
            <a
                href="${administration}${gestionCommandes}/CO"
                class="btn btn-info"> Commandes en cours </a>
            <a
                href="${administration}${gestionCommandes}/TE"
                class="btn btn-warning"> Commandes arrêtées </a>

            <a
                href="${administration}${gestionCommandes}/"
                class="exportExcelCommandes btn btn-primary"> Exporter </a>

        </div>
        <table class="table"
               data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbarCommandes"
               data-pagination="true"
               data-page-size=50
               data-page-list="[10, 25, 50, 100, ${listeCommandes.size()}]"
               data-pagination-v-align="top"
               data-pagination-h-align="right"
               data-pagination-detail-h-align="left"
               data-cookie="true"
               id="idBootstrapTable"
               data-cookie-id-table="saveIdConsultCommandes">
            <thead>
            <tr>
                <th data-field="numero" data-sortable="true" data-align="center">Numéro</th>
                <th data-field="client" data-sortable="true" data-align="center">Client</th>
                <th data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur</th>
                <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                <th data-field="budget" data-sortable="true" data-align="center">Budget</th>
                <th data-field="dateD" data-sortable="true" data-align="center">Date début</th>
                <th data-field="dateF" data-sortable="true" data-align="center">Date fin</th>
                <th data-field="justificatif" data-sortable="true" data-align="center">Justificatif</th>

                <%-- Column"Edition" for the current orders --%>
                <c:if test="${filtre == 'CO'}">
                    <th data-align="center">Édition</th>
                </c:if>

                <%-- Column "Arrêt" for the current orders --%>
                <c:if test="${filtre == 'CO'}">
                    <th data-align="center">Arrêt</th>
                </c:if>

                <th data-align="center">Dupliquer</th>

            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listeCommandes}" var="commande">
                <tr>
                    <td>${commande.numero}</td>
                    <td>${commande.client.nom_societe}</td>
                    <td>${commande.mission.collaborateur.nom} ${commande.mission.collaborateur.prenom}</td>
                    <td>${commande.mission.mission}</td>
                    <td>${commande.budget}</td>
                    <td><fmt:formatDate value="${commande.dateDebut}" pattern="dd/MM/yyyy"/></td>
                    <td><fmt:formatDate value="${commande.dateFin}" pattern="dd/MM/yyyy"/></td>
                    <%-- Order justification--%>
                    <td>
                        <c:choose>
                            <c:when test="${commande.justificatif!= null}">
                                ${commande.justificatif}  <a href="${administration}${download}/${commande.id}"
                                                             target="_blank"><span
                                class="glyphicon glyphicon-eye-open "></span></a>
                            </c:when>
                            <c:otherwise>
                                <span class="glyphicon glyphicon-eye-close "></span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <!-- Buttons "édition" and "arrêt" for the current order, plus linked confirm message -->
                    <c:if test="${filtre == 'CO'}">
                        <td><a
                            href="${administration}${editCommande}/${commande.id}">
                            <span class="glyphicon glyphicon-edit "></span>
                        </a></td>
                    </c:if>
                    <c:if test="${filtre == 'CO'}">
                        <td>
                            <a
                                href="${administration}${stopCommande}/${commande.id}"
                                data-confirm="Êtes-vous certain de vouloir arrêter la commande ${commande.numero} ?">
                                <span class="glyphicon glyphicon-stop"/>
                            </a>
                        </td>
                    </c:if>
                    <td>
                        <a
                            href="${administration}${duplicateStoppedCommande}/${commande.id}"
                            data-confirm="Êtes-vous certain de vouloir dupliquer la commande ${commande.numero} ?">
                            <span class="glyphicon"> + </span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
