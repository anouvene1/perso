<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- Table Missions not invoicing -->
<table class="table"
        data-toggle="table"
        data-height="520"
        data-search="true"
        data-striped="true"
        data-toolbar="#toolbarFacture"
        data-pagination="true"
        data-page-size=10
        data-page-list="[10, 20, 30, 40,50]"
        data-pagination-v-align="top"
        data-pagination-h-align="left"
        data-pagination-detail-h-align="left"
        data-show-multi-sort="true"
        data-sort-priority='[{"sortName": "nom","sortOrder":"asc"}{"sortName": "id", "sortOrder":"asc"}]'
        id="idBootstrapTable7"
        style="margin-top:50px">
    <thead>
    <tr>
        <th data-field="id" data-sortable="true" data-align="center">Mission</th>
        <th data-field="client" data-sortable="true" data-align="center">Client</th>
        <th data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur</th>
        <th data-field="debut" data-sortable="true" data-align="center">Début de mission</th>
        <th data-field="fin" data-sortable="true" data-align="center">Fin de mission</th>
        <th data-field="commande" data-sortable="true" data-align="center">Commande(s)</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${listMissionsWithoutInvoice}" var="mission">
        <tr>
            <td><c:out value="${mission.mission}"/></td>
            <c:choose>
                <c:when test="${(mission.client!=null)}">
                    <td><c:out value="${mission.client.nom_societe}"/></td>
                </c:when>
                <c:otherwise>
                    <td><span class="manqueCommande">Non renseigné</span></td>
                </c:otherwise>
            </c:choose>
            <td><c:out value="${mission.collaborateur}"/></td>
            <td><fmt:formatDate pattern="dd/MM/yyyy" value="${mission.dateDebut}"/></td>
            <td><fmt:formatDate pattern="dd/MM/yyyy" value="${mission.dateFin}"/></td>
            <c:choose>
                <c:when test="${(mission.commandes!=null) and (not empty mission.commandes)}">
                    <td>
                        <c:forEach items="${mission.commandes}" var="commande">
                            <c:out value="${commande.numero}"/>
                        </c:forEach>
                    </td>
                </c:when>
                <c:otherwise>
                    <td><span class="manqueCommande">Aucune commande</span></td>
                </c:otherwise>
            </c:choose>
        </tr>
    </c:forEach>
    </tbody>
</table>