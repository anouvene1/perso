<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="tab-pane" id="tabFfrais">
    <br/>
    <div class="col-md-12 text-right" style="font-size: xx-large; float:top;">
        <div class="col-md-6">
            <div class="col-md-6"></div>
        </div>
        <div class="col-md-6 text-right">${moisActuel} ${anneeActuel}<br/></div>
    </div>
    </br>
    </br>


    <div class="row">
        <div class="col-md-12 text-center">
            <form class="form-inline" method="post" action="${administration}${factures}/factures#tabFfrais">
                <div class="form-group">
                    <label for="monthExtractFfrais" id="lblMonthPickerFfrais">Date voulue : </label>
                    <input class="form-control monthPicker" name="monthYearExtract" id="monthExtractFfrais">
                </div>
                <button class="btn btn-primary" type="submit" name="action"
                        id="btnMonthPickerFfrais" value="">Valider</button>
            </form>
            <br/>
        </div>
    </div>

    <div class="col-md-12" style="border-bottom:1px solid #e5e5e5">

        <br/>

        <div id="divFacturesFrais">
            <%-- [JNA][AMNOTE 168] Tableau des factures de frais --%>
            <legend class="text-center">
                <br/>
                <h3>Factures de frais</h3>
                <p class="headerLabel">Factures correspondant aux frais des consultants (collaborateurs et
                    sous-traitants), facturés après les facturables.</p>
                <br/>
            </legend>
            <br/>
            <fieldset>
                <div id="toolbarCreerFactureFrais">
                    <label class="col-md-6 control-label text-right" for="listeConsultants"
                           style="font-size: larger; margin-top: 5px;"> Choisir un consultant </label>
                    <div class="col-md-6 sautLigne">
                        <form:form class="form-horizontal" method="GET" id="listeConsultants"
                                   modelAttribute="listeConsultants">
                            <div class="col-md-6 text-left">
                                <input class="form-control" list="filtreCollaborateur"
                                       onChange="loadMission(this, true, 'btnCreate', '')"/>
                                <datalist style="display:none" name="listeConsultants"
                                          path=""
                                          id="filtreCollaborateur"
                                          class="form-control"
                                          itemLabel="listeConsultants">
                                    <c:forEach items="${listeConsultants}" var="collaborateur">
                                        <option value="${collaborateur.id} ${collaborateur.nom} ${collaborateur.prenom}"
                                                data-id-manager="${collaborateur.manager.id}"></option>
                                    </c:forEach>
                                </datalist>
                            </div>
                        </form:form>
                    </div>
                    <br/>
                    <br/>

                    <form:form id="listMissionsClientesDuMoisVouluConsultants" class="form-horizontal" method="POST"
                               action="${administration}${creationFactureVierge}${FFRAIS}/${dateVoulueRefonte}"
                               modelAttribute="listMissionsClientesDuMoisVoulu">

                        <label class="col-md-6 control-label text-right" for="listMissionsClientesDuMoisVoulu"
                               style="font-size: larger; margin-top: 5px;">
                            Créer une nouvelle facture à la main pour la mission cliente :
                        </label>

                        <div class="col-md-6 sautLigne">
                            <div class="col-md-6 text-left">
                                <form:select name="listMissionsClientesDuMoisVoulu"
                                             onChange="loadCommandes(this, 'btnCreate', '')"
                                             class="form-control" path=""
                                             id="listMissionsClientesDuMoisVoulu"
                                             placeholder="Sélectionner une mission cliente"
                                             itemLabel="listMissionsClientesDuMoisVouluConsultants">
                                    <option value="" disabled selected hidden>Choisir une mission</option>
                                    <c:forEach items="${listMissionsClientesDuMoisVouluConsultants}" var="mission">
                                        <option value="${mission.id}">
                                                ${mission.mission} ${debugmode ? ' -- '.concat(mission) : ''}
                                        </option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </div>

                        <label class="col-md-6 control-label text-right" for="listCommandesDuMoisVouluMission"
                               style="font-size: larger; margin-top: 5px;">
                            Sélectionnez la commande correspondante :
                        </label>
                        <div class="col-md-6">
                            <div class="col-md-6 text-left">
                                <form:select name="listCommandesDuMoisVouluMission" class="form-control" path=""
                                             id="listCommandesDuMoisVouluMission"
                                             placeholder="Sélectionner la commande associée"
                                             itemLabel="listCommandesDuMoisVouluMission" disabled="true">
                                    <c:forEach items="${listCommandesDuMoisVouluMission}" var="commande">
                                        <form:option value="${commande.id}">
                                            ${commande.numero} ${debugmode ? ' -- '.concat(commande) : ''}
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                            <div class="col-md-6">
                                <button id="btnCreate" class="btn btn-info" type="submit" disabled="true">Créer</button>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    </form:form>

                    <div id="toolbarBoutonsFacturesFrais">
                        <!--SBE_AMNOTE-183 POSSIBILITE DE SOUMETTRE LES FACTURES SEULEMENT POUR LES DRH -->
                        <sec:authorize access="hasAnyAuthority('DRH')">
                            <c:set var="verifSoumissionFrais" scope="session" value="0"/>
                            <c:forEach items="${listeFactureFrais}" var="itemFrais">
                                <c:if test="${itemFrais.etat.id != 2 && itemFrais.etat.id != 10}">
                                    <c:set var="verifSoumissionFrais" scope="session" value="1"/>
                                </c:if>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${verifSoumissionFrais==1}">
                                    <c:if test="${not empty listeFactureFrais}">
                                        <c:set var="nbsFacturesFraisSansNum" scope="session" value="0"/>
                                        <c:forEach items="${listeFactureFrais}" var="factureFrais">
                                            <c:if test="${factureFrais.numFacture==0}">
                                                <c:set var="nbsFacturesFraisSansNum" scope="session" value="1"/>
                                            </c:if>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${nbsFacturesFraisSansNum==0}">
                                                <a class="btn btn-danger"
                                                   onclick="soumissionFactures(2,'collaborateurs')">Soumettre l'ordre
                                                    de facturation des frais </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="btn btn-info"
                                                   onclick="openModalNumerotation(2,'collaborateurs')">Numéroter les
                                                    factures de frais </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${not empty listeFactureFrais}">
                                        <a class="btn btn-default" disabled="true"
                                           onclick="soumissionFactures(2,'collaborateurs')">Soumettre
                                            l'ordre de facturation des frais </a>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </sec:authorize>

                        <!--SBE_AMNOTE-183 POSSIBILITE DE VALIDER LES FACTURES SEULEMENT POUR LA DIRECTION -->
                        <sec:authorize access="hasAnyAuthority('DIR')">
                            <c:if test="${not empty listeFactureFrais}">
                                <c:set var="verifExistSoumissionFrais" scope="session" value="0"/>
                                <c:forEach items="${listeFactureFrais}" var="itemFrais">
                                    <c:if test="${itemFrais.etat.id == 2}">
                                        <c:set var="verifExistSoumissionFrais" scope="session" value="1"/>
                                    </c:if>
                                </c:forEach>

                                <!--affichage du boutton valider si il existe des facture soumises-->
                                <c:choose>
                                    <c:when test="${verifExistSoumissionFrais==1}">
                                        <a class="btn btn-success" onclick="createExcels(2,'collaborateurs')">Valider
                                            l'ordre de facturation
                                            des frais </a>
                                        <a class="btn btn-danger" onclick="refusFactures(2,'collaborateurs')">Refuser
                                            l'ordre de facturation
                                            des frais </a>
                                    </c:when>
                                </c:choose>
                            </c:if>
                        </sec:authorize>

                        <c:set var="verifListeFraisVide" scope="session" value="0"/>
                        <c:forEach items="${listeFactureFrais}" var="itemFrais">
                            <c:if test="${itemFrais.etat.id != 10}">
                                <c:set var="verifListeFraisVide" scope="session" value="1"/>
                            </c:if>
                        </c:forEach>
                        <c:choose>
                            <%-- SBE_AMNOTE-175 Si la liste n'est pas vide, le bouton est clicable, sinon non --%>
                            <c:when test="${verifListeFraisVide==1}">
                                <a href="${administration}/factures/genererExcel/collaborateurs/${FFRAIS}/${dateVoulueRefonte}"
                                   id="exportFacturesFrais"
                                   class="exportExcelFacturesFrais btn btn-primary"> Exporter </a>

                                <a href="${administration}/factures/csv/${FFRAISCSV}/${dateVoulueRefonte}"
                                   id="exportFacturesCSV"
                                   class="btn btn-primary"> Export comptable </a>

                                <a href="${administration}/factures/SaveAllFacturesPDF/collaborateurs/${FFRAIS}/${dateVoulueRefonte}"
                                   id="pdfFacturesFrais"
                                   class="btn btn-warning"> Télécharger PDF complet </a>
                            </c:when>

                            <c:otherwise>
                                <a id="margeEnBasBoutton" class="exportExcelFacturesFrais btn btn-default"
                                   disabled="true">
                                    Exporter </a>
                            </c:otherwise>

                        </c:choose>
                    </div>

                    <table class="table"
                           data-toggle="table"
                           data-height="520"
                           data-search="true"
                           data-striped="true"
                           data-toolbar="#toolbarElementManquant"
                           data-pagination="true"
                           data-page-size=20
                           data-page-list="[10, 22, 30, 40,50]"
                           data-pagination-v-align="top"
                           data-pagination-h-align="right"
                           data-pagination-detail-h-align="left"
                           data-sort-name="id"
                           data-sort-order="asc"
                           id="idBootstrapTable1"
                           data-cookie="true"
                           data-cookie-id-table="saveIdEditCollabSansFacture">
                        <thead>
                        <tr>
                            <th data-field="id" data-sortable="true" data-align="center">N°</th>
                            <th data-field="nom" data-sortable="true" data-align="center">Client</th>
                            <th data-field="prenom" data-sortable="true" data-align="center">Consultant</th>
                            <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                            <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                            <th data-field="montantHT" data-sortable="true" data-align="center">Montant HT</th>
                            <th data-field="montantTTC" data-sortable="true" data-align="center">Montant TTC</th>
                            <th data-field="editer" data-sortable="true" data-align="center">Éditer</th>
                            <th data-field="Supprimer" data-sortable="true" data-align="center">Supprimer</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%--[JNA][AMNOTE 142] Affiche de la Hashmap <Commande,Facture> --%>
                        <c:forEach items="${listeFactureFrais}" var="factureFrais">
                            <c:if test="${factureFrais.etat.id != 10}">
                                <c:choose>
                                    <%--[JNA][AMNOTE-166] Si l'id de l'état est à 9, la facture à été éditée--%>
                                    <c:when test="${factureFrais.etat.id == 9}">
                                        <tr class="info">
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                    </c:otherwise>
                                </c:choose>
                                <%--[JNA][AMNOTE 142] item.key récupère une commande et item.value récpuère une facture --%>
                                <td><c:out value="${factureFrais.numFacture}"/></td>
                                <td><c:out value="${factureFrais.commande.client.nom_societe}"/></td>
                                <td><c:out value="${factureFrais.mission.collaborateur.nom}"/> <c:out
                                        value="${factureFrais.mission.collaborateur.prenom}"/></td>
                                <td><c:out value="${factureFrais.mission.mission}"/></td>
                                <c:choose>
                                    <c:when test="${factureFrais.commande!=null}">
                                        <td><c:out value="${factureFrais.commande.numero}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><span class="manqueCommande"> Suivant proposition commerciale</span></td>
                                    </c:otherwise>
                                </c:choose>
                                <td><c:out value="${montantsParFactures[factureFrais.id].first}"/></td>
                                <td><c:out value="${montantsParFactures[factureFrais.id].second}"/></td>

                                <c:choose>
                                    <%--[JNA][AMNOTE-166] Si l'id de l'état est à 9, la facture à été archivée, on peut la visualiser--%>
                                    <c:when test="${factureFrais.etat.id == 10}">
                                        <td><a href="${administration}${afficher}/${factureFrais.id}" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open "></span></a>
                                        </td>

                                        <td><span class="glyphicon glyphicon-ban-circle"></span></td>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${factureFrais.etat.id == 2}">
                                                <td><a href="${administration}${afficher}/${factureFrais.id}"
                                                       disabled="true" target="_blank">
                                                    <a disabled="true"></a>
                                                    <span class="glyphicon glyphicon-edit" disabled="true"></span>
                                                </a>
                                                </td>

                                                <td>
                                                    <span class="glyphicon glyphicon-ban-circle"></span>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td>
                                                    <a href="${administration}${editFacture}/<c:out value="${factureFrais.id}"/>">

                                                        <span class="glyphicon glyphicon-edit "></span>
                                                    </a></td>

                                                <%--[OZE][AMNOTE-180] 24-11-2016, Bouton pour supprimer une facture--%>
                                                <td>
                                                    <a href="${administration}${deleteFacture}/<c:out value="${factureFrais.id}"/>"
                                                       data-confirm="Etes-vous certain de vouloir supprimer la facture ${factureFrais.numFacture} ?">
                                                        <span class="glyphicon glyphicon-remove "></span>
                                                    </a>
                                                </td>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                                </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </fieldset>
            <br>
        </div>
    </div>
</div>
