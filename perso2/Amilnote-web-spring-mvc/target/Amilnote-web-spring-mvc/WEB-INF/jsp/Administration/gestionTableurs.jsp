<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.amilnote.project.metier.domain.utils.Constantes"%>

<%--
  ~ ©Amiltone 2017
  --%>

<!-- 13-07-2016 GDE : Script de gestion du datepicker pour choix date, permet de set la date par défaut et mois + année -->
<script>
    $(function () {
        $('.monthPicker').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: 'c-7:c+3',
            showButtonPanel: true,
            dateFormat: 'mm/yy',
            closeText: 'Ok',
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            onInit: function () {
                $(this).datepicker('setDate', new Date());
            }
        });
        $('.monthPicker').datepicker('setDate', new Date());
    });
</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Gestion des tableurs</li>
</ol>

<style>
    .panel-heading {
        font-size: 200%;
    }

    .ui-datepicker-calendar {
        display: none;
    }

    .ui-widget-header {
        color: #000000;
    }
</style>

<div class="center-block ">
    <div class="panel-primary">
        <div class="panel-heading text-center">Tableurs Excel</div>
        <div class="panel-body" id="frais">


            <fieldset>
                <legend class="text-center">
                    Tableurs de gestion du chiffre d'affaire
                </legend>

                <!--gestion du chiffre d'affaire par mois ou par année -->
                <form class="text-left col-md-offset-4 col-md-8 " method="post"
                      action="${administration}${tableurExcel}/revenues">
                    <label for="monthExtractRevenues">Date de l'extraction : </label>
                    <input name="monthExtractRevenues" id="monthExtractRevenues" class="monthPicker" autocomplete="off">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="${Constantes.MONTHLY_REVENUES_EXCEL_TYPE}">
                        Chiffre d'affaire mensuel
                    </button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="${Constantes.ANNUAL_REVENUES_EXCEL_TYPE}">
                        Chiffre d'affaire annuel
                    </button>
                </form>
            </fieldset>


            <fieldset>
                <legend class="text-center">
                    Tableurs de gestion des frais
                </legend>
                <!--SBE_11/09/16: ajout d'un case pour traiter dynamiquement tout les cas de la page (frais, jour de presences,...-->
                <!--gestion des frais par mois -->
                <form class="text-left col-md-offset-4 col-md-8" method="post"
                      action="${administration}${tableurExcel}/frais">
                    <label for="monthExtract">Date du mois voulue : </label>

                    <input name="monthYearExtract2" id="monthExtract2" class="monthPicker" autocomplete="off">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="MoisSimple">
                        Frais du mois
                    </button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="Mois"> Frais
                        détaillés du mois
                    </button>
                </form>


                <!--gestion des frais par année -->
                <form class="text-left col-md-offset-4 col-md-8" method="post"
                      action="${administration}${tableurExcel}/fraisAn">
                    <label for="monthExtract">Date de l'année voulue : </label>

                    <input name="monthYearExtract3" id="monthExtract3" class="monthPicker" autocomplete="off">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="Simple"> Frais sur l'année</button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value=""> Frais
                        détaillés sur l'année
                    </button>
                </form>
            </fieldset>
        </div>

        <div class="panel-body" id="pres">
            <fieldset>
                <legend class="text-center">
                    Tableurs de gestion des présences/absences
                </legend>

                <!--gestion des abscences par année -->
                <form class="text-left col-md-offset-4 col-md-8" method="post"
                      action="${administration}${tableurExcel}/absencesMois">

                    <label for="monthExtract">Date du mois voulue (sur l'année en cours) : </label>

                    <input name="monthYearExtract4" id="monthExtract4" class="monthPicker" autocomplete="off">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value=""> Absence sur le mois</button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="SST"> Absence sur le mois (sous-traitants)</button>
                </form>

                <!--gestion des jours de presences par année -->
                <form class="text-left col-md-offset-4 col-md-8" method="post"
                      action="${administration}${tableurExcel}/presenceMois">

                    <label for="monthExtract">Date de l'année voulue : </label>

                    <input name="monthYearExtract5" id="monthExtract5" class="monthPicker" autocomplete="off">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value=""> Jour de présence par mois</button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="SST"> Jour de présence par mois (sous-traitants)</button>
                </form>
            </fieldset>
        </div>

        <div class="panel-body" id="clients">
            <fieldset>
                <legend class="text-center">
                    Tableurs des collaborateurs en mission cliente
                </legend>

                <!--gestion des Collaborateurs en mission cliente -->
                <form class="text-center" method="post" action="${administration}${tableurExcel}/clients">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="CO"> Collaborateurs en mission
                        cliente</button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="SST"> Sous-traitants en mission
                        cliente</button>
                </form>
                <form class="text-center" method="post" action="${administration}${tableurExcel}/clients">
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="clientsPerCollaborator">Clients du mois par collaborateur</button>
                    <button class="btn btn-action vaderBlue" type="submit" name="action" value="clientsPerCollaboratorSTT">Clients du mois par sous-traitants</button>
                </form>

            </fieldset>
        </div>
    </div>
</div>
