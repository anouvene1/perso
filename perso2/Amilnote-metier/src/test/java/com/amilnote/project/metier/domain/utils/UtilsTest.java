package com.amilnote.project.metier.domain.utils;

import org.junit.Assert;
import org.junit.Test;

public class UtilsTest {

    @Test
    public void testMoisIntToString() {
        Assert.assertEquals("Janvier", Utils.moisIntToString(0));
        Assert.assertEquals("Février", Utils.moisIntToString(1));
        Assert.assertEquals("Mars", Utils.moisIntToString(2));
        Assert.assertEquals("Avril", Utils.moisIntToString(3));
        Assert.assertEquals("Mai", Utils.moisIntToString(4));
        Assert.assertEquals("Juin", Utils.moisIntToString(5));
        Assert.assertEquals("Juillet", Utils.moisIntToString(6));
        Assert.assertEquals("Août", Utils.moisIntToString(7));
        Assert.assertEquals("Septembre", Utils.moisIntToString(8));
        Assert.assertEquals("Octobre", Utils.moisIntToString(9));
        Assert.assertEquals("Novembre", Utils.moisIntToString(10));
        Assert.assertEquals("Décembre", Utils.moisIntToString(11));
    }

    @Test
    public void testMoisStringToString() {
        Assert.assertEquals("Janvier", Utils.moisStringToString("01"));
        Assert.assertEquals("Février", Utils.moisStringToString("02"));
        Assert.assertEquals("Mars", Utils.moisStringToString("03"));
        Assert.assertEquals("Avril", Utils.moisStringToString("04"));
        Assert.assertEquals("Mai", Utils.moisStringToString("05"));
        Assert.assertEquals("Juin", Utils.moisStringToString("06"));
        Assert.assertEquals("Juillet", Utils.moisStringToString("07"));
        Assert.assertEquals("Août", Utils.moisStringToString("08"));
        Assert.assertEquals("Septembre", Utils.moisStringToString("09"));
        Assert.assertEquals("Octobre", Utils.moisStringToString("10"));
        Assert.assertEquals("Novembre", Utils.moisStringToString("11"));
        Assert.assertEquals("Décembre", Utils.moisStringToString("12"));
    }

}
