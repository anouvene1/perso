package com.amilnote.project.metier.domain.task;

import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.services.MissionService;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ScheduledTasksTest {

    @Mock
    private MissionService missionService;

    @Test
    public void genererFactures() {
        Facture facture= Mockito.mock(Facture.class);
        DateTime dateVoulue = new DateTime();

        Mockito.when(missionService.findMissionsClientesByMonth(dateVoulue)).thenReturn(new ArrayList<>());
    }
}
