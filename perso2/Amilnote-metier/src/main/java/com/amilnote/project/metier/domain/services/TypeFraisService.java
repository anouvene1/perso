/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.TypeFrais;
import com.amilnote.project.metier.domain.entities.json.TypeFraisAsJson;
import com.amilnote.project.metier.domain.utils.TypeFraisForm;

import java.util.List;

/**
 * The interface Type frais service.
 */
public interface TypeFraisService extends AbstractService<TypeFrais> {

    /**
     * Retourne tous les Types de frais
     *
     * @return all type frais
     */
    List<TypeFrais> getAllTypeFrais();

    /**
     * Retourne un type de frais en fonction de l'id
     *
     * @param pId the p id
     * @return TypeFrais type frais by id
     */
    TypeFrais getTypeFraisById(long pId);

    /**
     * Retourne tous les type de frais au format json
     *
     * @return String (Liste de TypeFraisAsJson)
     */
    List<TypeFraisAsJson> getAllTypeFraisAsJson();


    /**
     * Met à jour l'ensemble des TVA pour les type de frais passés en paramètres
     *
     * @param pTypeFrais Liste de type de frais
     */
    void updateAllTVAForFrais(TypeFraisForm pTypeFrais);

    /**
     * Retourne le type de frais correspondant au code renseigné
     *
     * @param code code du type de frais souhaité
     * @return Type de frais correspondant au code
     */
    TypeFrais getTypeFraisByCode(String code);

}
