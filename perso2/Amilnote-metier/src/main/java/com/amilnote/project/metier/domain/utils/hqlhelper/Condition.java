/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;


/**
 * The type Condition.
 */
public class Condition {

    private Object champ1; //de type Champ ou Condition
    private Object champ2; //de type Champ ou Condition
    private Operator operateur;

    private Condition(Object champ1, Object champ2, Operator operateur) {
        this.champ1 = champ1;
        this.champ2 = champ2;
        this.operateur = operateur;
    }

    /**
     * And condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition and(Object champ1, Object champ2) {
        return new Condition(champ1, champ2, Operator.and());
    }

    /**
     * Or condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition or(Object champ1, Object champ2) {
        return new Condition(champ1, champ2, Operator.or());
    }

    /**
     * In condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition in(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.in());
    }

    /**
     * Like condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition like(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.like());
    }

    /**
     * Eq condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition eq(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.eq());
    }

    /**
     * Sup condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition sup(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.sup());
    }

    /**
     * Inf condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition inf(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.inf());
    }

    /**
     * Ge condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition ge(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.ge());
    }

    /**
     * Le condition.
     *
     * @param champ1 the champ 1
     * @param champ2 the champ 2
     * @return the condition
     */
    public static Condition le(Champ champ1, Champ champ2) {
        return new Condition(champ1, champ2, Operator.le());
    }

    /**
     * Is null condition.
     *
     * @param champ1 the champ 1
     * @return the condition
     */
    public static Condition isNull(Champ champ1) {
        return new Condition(champ1, null, Operator.isNull());
    }

    /**
     * Not condition.
     *
     * @param condition the condition
     * @return the condition
     */
    public static Condition not(Condition condition) {
        condition.operateur = Operator.not(condition.operateur);
        return condition;
    }

    /**
     * Gets restriction.
     *
     * @return the restriction
     */
    public String getRestriction() {
        String retour = "";
        String droit = "", gauche = "";

        if (operateur.isOr()) {
            droit = ")";
            gauche = "(";
        }

        if (champ1.getClass().equals(Champ.class)) {
            retour = retour + ((Champ) champ1).getChamp();
        } else if (champ1.getClass().equals(Condition.class)) {
            retour = retour + gauche + ((Condition) champ1).getRestriction();
        }

        retour = retour + " " + operateur.getOperator() + " ";

        if (champ2 != null) {
            if (champ2.getClass().equals(Champ.class)) {
                retour = retour + ((Champ) champ2).getChamp();
            } else if (champ2.getClass().equals(Condition.class)) {
                retour = retour + ((Condition) champ2).getRestriction() + droit;
            }
        }

        return retour;

    }
}
