/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.*;

import java.util.Date;

/**
 * The type Facture as json.
 */
public class FactureAsJson {

    private int id;
    private Mission mission;
    private Commande commande;
    private String adresse_facturation;
    private int id_client;
    private float quantite;
    private float prix;
    private float montant;
    private int contact;
    private Etat etat;
    private String pdf;
    private Date date_facture;
    private Date mois_prestations;
    private String rib_facture;
    private String commentaire;
    private int num_facture;
    private Date dateCreation;
    private TypeFacture typeFacture;
    private int raValide;
    private boolean isAvoir;
    private TVA tva;

    /**
     * Instantiates a new Facture as json. Constructeur.
     *
     * @param facture the facture
     */
    public FactureAsJson(Facture facture) {
        this.setId(facture.getId());
        this.setMission(facture.getMission());
        this.setCommande(facture.getCommande());
        this.setAdresseFacturation(facture.getAdresseFacturation());
        this.setIdClient(facture.getIdClient());
        this.setQuantite(facture.getQuantite());
        this.setPrix(facture.getPrix());
        this.setMontant(facture.getMontant());
        this.setContact(facture.getContact());
        this.setEtat(facture.getEtat());
        this.setPdf(facture.getPdf());
        this.setDateFacture(facture.getDateFacture());
        this.setMoisPrestations(facture.getMoisPrestations());
        this.setRIBFacture(facture.getRIBFacture());
        this.setCommentaire(facture.getCommentaire());
        this.setNumFacture(facture.getNumFacture());
        this.setDateCreation(facture.getDateCreation());
        this.setTypeFacture(facture.getTypeFacture());
        this.setRaValide(facture.getRaValide());
        this.setIsAvoir(facture.getIsAvoir());
        this.setTVA(facture.getTVA());
    }

    /**
     * Instantiates a new Facture as json. Constructeur par défaut.
     */
    public FactureAsJson() {
    }

    /**
     * Gets the mission
     *
     * @return the mission
     */
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets the mission
     *
     * @param pMission the mission
     */
    public void setMission(Mission pMission) {
        this.mission = pMission;
    }

    /**
     * Gets the raValide
     *
     * @return the raValide
     */
    public int getRaValide() {
        return raValide;
    }

    /**
     * Sets the raValide
     *
     * @param raValide the raValide
     */
    public void setRaValide(int raValide) {
        this.raValide = raValide;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets commande.
     *
     * @return the id_commande
     */
    public Commande getCommande() {
        return commande;
    }

    /**
     * Sets commande.
     *
     * @param commande the id_commande to set
     */
    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    /**
     * Gets adresse facturation.
     *
     * @return the adresse_facturation
     */
    public String getAdresseFacturation() {
        return adresse_facturation;
    }

    /**
     * Sets adresse facturation.
     *
     * @param adresse the adresse_facturation to set
     */
    public void setAdresseFacturation(String adresse) {
        this.adresse_facturation = adresse;
    }

    /**
     * Gets id client.
     *
     * @return the id_client
     */
    public int getIdClient() {
        return id_client;
    }

    /**
     * Sets id client.
     *
     * @param idClient the id_client to set
     */
    public void setIdClient(int idClient) {
        this.id_client = idClient;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param q the quantite to set
     */
    public void setQuantite(float q) {
        this.quantite = q;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param p the prix to set
     */
    public void setPrix(float p) {
        this.prix = p;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param m the montant to set
     */
    public void setMontant(float m) {
        this.montant = m;
    }

    /**
     * Gets contact.
     *
     * @return the contact
     */
    public int getContact() {
        return contact;
    }

    /**
     * Sets contact.
     *
     * @param c the contact to set
     */
    public void setContact(int c) { this.contact = c; }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat to set
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets pdf.
     *
     * @return the pdf
     */
    public String getPdf() {
        return pdf;
    }

    /**
     * Sets pdf.
     *
     * @param pdf the pdf
     */
    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    /**
     * Gets date facture.
     *
     * @return the date facture
     */
    public Date getDateFacture() {
        return date_facture;
    }

    /**
     * Sets date facture.
     *
     * @param d the d
     */
    public void setDateFacture(Date d) {
        this.date_facture = d;
    }

    /**
     * Gets mois prestations.
     *
     * @return the mois prestations
     */
    public Date getMoisPrestations() {
        return mois_prestations;
    }

    /**
     * Sets mois prestations.
     *
     * @param mois the mois
     */
    public void setMoisPrestations(Date mois) {
        this.mois_prestations = mois;
    }

    /**
     * Gets rib facture.
     *
     * @return the rib facture
     */
    public String getRIBFacture() {
        return rib_facture;
    }

    /**
     * Sets rib facture.
     *
     * @param rib the rib
     */
    public void setRIBFacture(String rib) {
        this.rib_facture = rib;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param comm the comm
     */
    public void setCommentaire(String comm) {
        this.commentaire = comm;
    }

    /**
     * Gets num facture.
     *
     * @return the num facture
     */
    public int getNumFacture() {
        return num_facture;
    }

    /**
     * Sets num facture.
     *
     * @param num the num
     */
    public void setNumFacture(int num) {
        this.num_facture = num;
    }

    /**
     * Gets date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Sets date creation.
     *
     * @param d the d
     */
    public void setDateCreation(Date d) {
        this.dateCreation = d;
    }

    /**
     * Gets facture vierge.
     *
     * @return the facture vierge
     */
    public TypeFacture getTypeFacture() {
        return typeFacture;
    }

    /**
     * Sets facture vierge.
     *
     * @param fVierge the f vierge
     */
    public void setTypeFacture(TypeFacture fVierge) {
        this.typeFacture = fVierge;
    }

    /**
     * Gets avoir.
     *
     * @return the avoir
     */
    public boolean getIsAvoir() {
        return isAvoir;
    }

    /**
     * Sets avoir.
     *
     * @param avoir the isAvoir
     */
    public void setIsAvoir(boolean avoir) {
        isAvoir = avoir;
    }


    /**
     * Gets tva.
     *
     * @return the id_tva
     */
    public TVA getTVA() {
        return tva;
    }

    /**
     * Sets tva.
     *
     * @param ptva the id_tva to set
     */
    public void setTVA(TVA ptva) {
        this.tva = ptva;
    }

}
