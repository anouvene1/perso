/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.LinkEvenementTimesheetAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Link evenement timesheet.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_link_evenement_timesheet")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class LinkEvenementTimesheet implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";

    /**
     * The constant PROP_ABSENCE.
     */
    public static final String PROP_ABSENCE = "absence";

    /**
     * The constant PROP_DATE.
     */
    public static final String PROP_DATE = "date";

    /**
     * The constant PROP_MOMENTJOURNEE.
     */
    public static final String PROP_MOMENTJOURNEE = "momentJournee";

    /**
     * The constant PROP_LISTFRAIS.
     */
    public static final String PROP_LISTFRAIS = "listFrais";

    /**
     * The constant PROP_RA.
     */
    public static final String PROP_RA = "rapportActivite";

    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_COLLABORATEUR = "collaborateur";

    private static final long serialVersionUID = 5930258519057182398L;

    private Long id;
    private Mission mission = null;
    private Absence absence = null;
    private Date date;
    private int momentJournee;
    private List<Frais> listFrais = new ArrayList<>();
    private RapportActivites rapportActivite;
    private Collaborator collaborator;

	
	/* CONSTRUCTORS */

    /**
     * Instantiates a new Link evenement timesheet.
     *
     * @param pMission         the p mission
     * @param pAbsence         the p absence
     * @param pDate            the p date
     * @param pMomentJournee   the p moment journee
     * @param pListFrais       the p list frais
     * @param pRapportActivite the p rapport activite
     * @param collaborator   the p collaborateur
     */
    public LinkEvenementTimesheet(Mission pMission, Absence pAbsence, Date pDate, int pMomentJournee, List<Frais> pListFrais, RapportActivites pRapportActivite, Collaborator collaborator) {

        this.mission = pMission;
        this.absence = pAbsence;
        this.date = pDate;
        this.momentJournee = pMomentJournee;
        this.listFrais = pListFrais;
        this.rapportActivite = pRapportActivite;
        this.collaborator = collaborator;
    }

    /**
     * Instantiates a new Link evenement timesheet.
     */
    public LinkEvenementTimesheet() {
        super();
    }

    /**
     * To json link evenement timesheet as json.
     *
     * @return the link evenement timesheet as json
     */
    public LinkEvenementTimesheetAsJson toJson() {
        return new LinkEvenementTimesheetAsJson(this);
    }
    /* GETTERS and SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * retourne la mission du collaborateur
     *
     * @return un objet de type LinkMissionCollaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_mission", nullable = true)
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(Mission pMission) {
        mission = pMission;
    }

    /**
     * retourne  une absence
     *
     * @return un objet de type Absence
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_absence", nullable = true)
    public Absence getAbsence() {
        return absence;
    }

    /**
     * Sets absence.
     *
     * @param pAbsence the p absence
     */
    public void setAbsence(Absence pAbsence) {
        absence = pAbsence;
    }

    /**
     * Gets list frais.
     *
     * @return the frais
     */
    @OneToMany(mappedBy = "linkEvenementTimesheet")
    public List<Frais> getListFrais() {
        return listFrais;
    }

    /**
     * Sets list frais.
     *
     * @param pListFrais the p list frais
     */
    public void setListFrais(
        List<Frais> pListFrais) {
        listFrais = pListFrais;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    @Column(name = "date", nullable = false)
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param pDate the p date
     */
    public void setDate(Date pDate) {
        date = pDate;
    }

    /**
     * Gets moment journee.
     *
     * @return the momentJournee
     */
    @Column(name = "momentJournee", nullable = false)
    public int getMomentJournee() {
        return momentJournee;
    }

    /**
     * Sets moment journee.
     *
     * @param pMomentJournee the p moment journee
     */
    public void setMomentJournee(int pMomentJournee) {
        momentJournee = pMomentJournee;
    }


    /**
     * retourne  un rapport d'activité
     *
     * @return un objet de type RapportActivite
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rapport_activite", nullable = true)
    public RapportActivites getRapportActivite() {
        return rapportActivite;
    }

    /**
     * Sets rapport activite.
     *
     * @param pRapportActivite the rapportActivite to set
     */
    public void setRapportActivite(RapportActivites pRapportActivite) {
        rapportActivite = pRapportActivite;
    }

    /**
     * retourne  un collaborateur
     *
     * @return un objet de type Collaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_collaborateur", nullable = true)
    public Collaborator getCollaborator() {
        return collaborator;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborator collaborator entity
     */
    public void setCollaborator(Collaborator collaborator) {
        this.collaborator = collaborator;
    }

    /**
     * Retourne la date et l'heure du début du linkEvenement en fonction de this.momentJournee et this.getDate
     *
     * @return the date time
     */
    public DateTime calculDateStart() {
        if (this.momentJournee == 0) {
            return new DateTime(this.getDate()).withHourOfDay(8);
        }

        else {
            return new DateTime(this.getDate()).withHourOfDay(13);

        }
    }

    /**
     * Retourne la date et l'heure de la fin du linkEvenement en fonction de this.momentJournee et this.getDate
     *
     * @return the date time
     */
    public DateTime calculDateEnd() {
        if (this.momentJournee == 0) {
            return new DateTime(this.getDate()).withHourOfDay(12);

        }
        else {
            return new DateTime(this.getDate()).withHourOfDay(17);

        }
    }

    /**
     * Initialise le momentJournee en fonction de l'heure de la date début et date Fin
     *
     * @param pStart the p start
     * @param pEnd   the p end
     * @return the moment journee
     */
    public String setMomentJournee(DateTime pStart, DateTime pEnd) {

        if (pStart.getDayOfYear() == pEnd.getDayOfYear() && pStart.getYearOfCentury() == pEnd.getYearOfCentury()) {
            if (pStart.getHourOfDay() == 8 && pEnd.getHourOfDay() == 12) {
                this.setMomentJournee(0);
                return "ok";
            } else if (pStart.getHourOfDay() == 13 && pEnd.getHourOfDay() == 17) {
                this.setMomentJournee(1);
                return "ok";
            } else {
                return "Les horaires doivent être AM : 8h - 12h ou PM : 13h - 17h";
            }
        } else {
            return "les dates doivent être le même jour";
        }
    }

    /**
     * Sets moment journee.
     *
     * @param pStart the p start
     * @param pEnd   the p end
     * @return the moment journee
     */
    public String setMomentJournee(Date pStart, Date pEnd) {
        return this.setMomentJournee(new DateTime(pStart), new DateTime(pEnd));
    }

    /**
     * La date de l'event doit être comprise entre la date de début  et la date de fin de la mission passée en paramètre
     * * @param Mission mission
     *
     * @param mission the mission
     * @return Bool boolean
     */
    public boolean checkDateAndSetMission(Mission mission) {

        boolean res = false;

        DateTime missDateDebut = new DateTime(mission.getDateDebut());
        DateTime missDateFin = new DateTime(mission.getDateFin());
        DateTime eventDate = new DateTime(this.getDate());

        // si strictement inf ou sup
        if (eventDate.isAfter(missDateDebut) && eventDate.isBefore(missDateFin)) {
            res = true;
            // sinon si date debut est même jour
        } else if (eventDate.dayOfYear().get() == missDateDebut.dayOfYear().get() && eventDate.getYearOfCentury() == missDateDebut.getYearOfCentury()) {
            res = true;
            // sinon si date fin est même jour
        } else if (eventDate.dayOfYear().get() == missDateFin.dayOfYear().get() && eventDate.getYearOfCentury() == missDateFin.getYearOfCentury()) {
            res = true;
        }

        if (res) {
            this.setMission(mission);
            return true;
        } else {
            return false;
        }


    }

    /**
     * Calcul a mor pm string.
     *
     * @return the string
     */
    public String calculAMorPM() {
        if (this.getMomentJournee() == 0) {
            return "AM";
        } else {
            return "PM";
        }
    }

}
