/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * The type Properties.
 */
public class Properties {

    private MessageSource messages;

    /**
     * Gets messages.
     *
     * @return the messages
     */
    public MessageSource getMessages() {
        return this.messages;
    }

    /**
     * Sets messages.
     *
     * @param messages the messages
     */
    public void setMessages(MessageSource messages) {
        this.messages = messages;
    }

    /**
     * Accesseurs pour les propriétés
     *
     * @param key      the key
     * @param params   the params
     * @param required the required
     * @param locale   the locale
     * @return String la propriété
     */
    public String get(String key, String params[], String required, Locale locale) {
        return this.messages.getMessage(key, params, required, locale);
    }

    /**
     * Get string.
     *
     * @param key    the key
     * @param params the params
     * @return the string
     */
    public String get(String key, String params[]) {
        return this.messages.getMessage(key, params, null, null);
    }

    /**
     * Get string.
     *
     * @param key the key
     * @return the string
     */
    public String get(String key) {
        return this.messages.getMessage(key, null, null, null);
    }

    /**
     * Get string.
     *
     * @param key   the key
     * @param param the param
     * @return the string
     */
    public String get(String key, String param) {
        String[] params = {param};
        return this.messages.getMessage(key, params, null, null);
    }

}
