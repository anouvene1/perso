/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ModuleAddviseDAO;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type Module addvise dao.
 */
@Repository("ModuleAddviseDAO")
public class ModuleAddviseDAOImpl extends AbstractDAOImpl<ModuleAddvise> implements ModuleAddviseDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<ModuleAddvise> getReferenceClass() {
        return ModuleAddvise.class;
    }

    /**
     * {@linkplain ModuleAddviseDAO#findById(Long)}
     */
    @Override
    public ModuleAddvise findById(Long id) {
        return (ModuleAddvise) currentSession().get(ModuleAddvise.class, id);
    }

    /**
     * {@linkplain ModuleAddviseDAO#findAll()}
     */
    @Override
    public List<ModuleAddvise> findAll() {
        return (List<ModuleAddvise>) getCriteria().list();
    }

    /**
     * {@linkplain ModuleAddviseDAO#deleteModuleAddvise(ModuleAddvise)}
     */
    @Override
    @Transactional
    public int deleteModuleAddvise(ModuleAddvise pModuleAddvise) {

        delete(pModuleAddvise);
        return 0;
    }

    /**
     * {@linkplain ModuleAddviseDAO#createOrUpdateModuleAddvise(ModuleAddvise)}
     */
    @Override
    @Transactional
    public int createOrUpdateModuleAddvise(ModuleAddvise pModuleAddvise) {

        saveOrUpdate(pModuleAddvise);
        return 0;
    }
}
