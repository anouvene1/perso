/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;


import com.amilnote.project.metier.domain.dao.ParametreDAO;
import com.amilnote.project.metier.domain.entities.Parametre;
import com.amilnote.project.metier.domain.entities.json.ParametreAsJson;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Parametre dao.
 */
@Repository("parametreDAO")
public class ParametreDAOImpl extends AbstractDAOImpl<Parametre> implements ParametreDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Parametre> getReferenceClass() {
        return Parametre.class;
    }

    /**
     * {@linkplain ParametreDAO#createOrUpdateParametre(ParametreAsJson)}
     */
    @Override
    public int createOrUpdateParametre(ParametreAsJson parametreJson) {
        String msgRetour = "";

        Parametre parametre = new Parametre();

        // On vérifit que l'id soit bien récupéré en cas de modification
        if (parametreJson.getId() != 0) {
            //On récupère le parametre grâce à l'ID
            parametre = findUniqEntiteByProp(Parametre.PROP_ID_PARAMETRE, parametreJson.getId());
        } else { // si on est en mode création

            if (!parametreJson.getLibelle().equals("")) {
                parametre.setLibelle(parametreJson.getLibelle());
            } else {
                parametre.setLibelle(null);
            }

            if (!parametreJson.getValeur().equals("")) {
                parametre.setValeur(parametreJson.getValeur());
            } else {
                parametre.setValeur(null);
            }

        }

        // --- Création de la session et de la transaction
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        // --- Sauvegarde et Commit de la mise à jour
        session.save(parametre);
        session.flush();
        transaction.commit();
        parametreJson.setId(parametre.getId());
        msgRetour = "ok";

        return 1;
    }

    /**
     * {@linkplain ParametreDAO#deleteParametre(Parametre)}
     */
    @Override
    public String deleteParametre(Parametre pParametre) {

        if (null != pParametre) {

            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // --- Sauvegarde et Commit de la mise à jour
            session.delete(pParametre);
            session.flush();
            transaction.commit();
            return "Suppression effectuée avec succès";
        }
        return "Aucune suppression n'a été effectuée";
    }

    /**
     * {@linkplain ParametreDAO#findParametres()}
     */
    @Override
    public List<Parametre> findParametres() {
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc(Parametre.PROP_ID_PARAMETRE));
        return criteria.list();
    }
}
