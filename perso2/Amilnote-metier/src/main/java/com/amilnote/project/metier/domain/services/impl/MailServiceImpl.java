package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.Properties;
import com.amilnote.project.metier.domain.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The type Mail service.
 */
@Service("MailService")
public class MailServiceImpl implements MailService {

    private static final Logger logger = LogManager.getLogger(MailServiceImpl.class);

    private static final String EMPTY_MAIL_COMMENT = "Aucun commentaire";
    private static final String SIGNATURE_ADMINISTRATION = "Administration";
    private static final List<Collaborator> EMPTY_COLLABORATOR_COLLECTION = Collections.emptyList();
    private static final String EXPENSES = "de frais";
    private static final String ORDER_NUMBER = "- commande numéro <b>";
    private static final String MAIL_DEBUG_START = "-------------------- Début mail --------------------";
    private static final String MAIL_DEBUG_END = "-------------------- Fin mail --------------------";


    /**
     * The Properties.
     */
    @Autowired
    protected Properties properties;

    /**
     * The Collaborator service.
     */
    @Autowired
    protected CollaboratorService collaboratorService;

    /**
     * The Rapport activites service.
     */
    @Autowired
    protected RapportActivitesService rapportActivitesService;

    /**
     * The Facture service.
     */
    @Autowired
    protected FactureService factureService;

    @Autowired
    protected MissionService missionService;

    @Autowired
    private ParametreService parametreService;

    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private FileService fileService;

    @Autowired
    private StatutCollaborateurService statutCollaborateurService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private PDFBuilderRA pdfBuilderRA;

    private String expediteurMail;
    private String expediteurNom;
    private int compteurDocuments;

    private List<Collaborator> mailRecipient;

    public MailServiceImpl() throws NamingException {
        expediteurMail = Parametrage.getContext(Constantes.EMAIL_ADMIN_MAIL_CONTEXT);
        expediteurNom = Parametrage.getContext(Constantes.EMAIL_ADMIN_NAME_CONTEXT);

        mailRecipient = new ArrayList<>();
    }

    /**
     * Envoi mail factures soumises.
     *
     * @param cheminComplet the piece jointe
     * @param typeFacture   the type facture
     */
    @Transactional
    public void envoiMailFacturesSoumises(String cheminComplet, int typeFacture) {

        List<Collaborator> collaborators = collaboratorService.findAllOrderByNomAsc();
        List<Collaborator> directionMembers = new ArrayList<>();

        String[] paramsForObjProperties = new String[1];
        String[] paramsForMsgProperties = new String[1];

        if (typeFacture == 1) {
            paramsForObjProperties[0] = "";
            paramsForMsgProperties[0] = "";
        } else {
            paramsForObjProperties[0] = EXPENSES;
            paramsForMsgProperties[0] = EXPENSES;
        }

        for (Collaborator collaborator : collaborators) {
            if (collaborator.getStatut().getCode() == StatutCollaborateur.STATUT_DIRECTION) {
                directionMembers.add(collaborator);
            }
        }


        List<String> attachedFiles = new ArrayList<>();
        attachedFiles.add(cheminComplet);
        if (!directionMembers.isEmpty()) {
            prepareMailComposure(
                    properties.get("email.soumissionFacture.object", paramsForObjProperties),
                    properties.get("email.soumissionFacture.message", paramsForMsgProperties),
                    attachedFiles,
                    directionMembers,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        }
    }

    /**
     * Envoi mail factures confirmees ou refusees.
     *
     * @param typeFacture      the type facture
     * @param choix            the choix
     * @param commentaireRefus the commentaire refus
     */
    @Transactional
    public void envoiMailFacturesConfirmeesOuRefusees(int typeFacture, boolean choix, String commentaireRefus) {

        String[] paramsForObjProperties = new String[1];
        String[] paramsForMsgProperties = new String[2];

        if (typeFacture == 1) {
            paramsForObjProperties[0] = "";
            paramsForMsgProperties[0] = "";
        } else {
            paramsForObjProperties[0] = EXPENSES;
            paramsForMsgProperties[0] = EXPENSES;
        }

        if (commentaireRefus == null) {
            paramsForMsgProperties[1] = "Aucune justification";
        } else {
            paramsForMsgProperties[1] = commentaireRefus;
        }

        if (!mailRecipient.isEmpty()) {
            if (choix) {
                prepareMailComposure(
                        properties.get("email.validationFacture.object", paramsForObjProperties),
                        properties.get("email.validationFacture.message", paramsForMsgProperties),
                        null,
                        mailRecipient,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION
                );
            } else {
                prepareMailComposure(
                        properties.get("email.refusFacture.object", paramsForObjProperties),
                        properties.get("email.refusFacture.message", paramsForMsgProperties),
                        null,
                        mailRecipient,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION
                );
            }
        }
    }

    /**
     * Envoi mail rappel date.
     *
     * @param collaborateur the collaborators
     * @param type          the type
     */
    @Transactional
    public void envoiMailRappelDate(Collaborator collaborateur, int type) {

        List<Collaborator> recipients = new ArrayList<>();
        StatutCollaborateur humanResourcesStatus = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        mailRecipient = collaboratorService.retrieveCollaboratorByStatus(true, humanResourcesStatus);

        try {
            recipients = collaboratorService.findAllByStatutOrderByNom(StatutCollaborateur.STATUT_DIRECTION);

            if (recipients != null) {
                recipients.addAll(mailRecipient);
            }
        } catch (Exception e) {
            logger.error("[send reminder email] collaborator id : {}", collaborateur.getId(), e);
        }

        String fullNameCollab = getCollabLastAndFirstName(collaborateur);

        if (recipients != null && !recipients.isEmpty()) {
            if (type == 0) {
                String[] paramsForObjProperties = {fullNameCollab};
                String[] paramsForMsgProperties = {fullNameCollab};
                prepareMailComposure(
                        properties.get("email.rappelBirthday.object", paramsForObjProperties),
                        properties.get("email.rappelBirthday.message", paramsForMsgProperties),
                        null,
                        recipients,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION
                );
            } else {
                if (type == 1) {
                    String[] paramsForObjProperties = {fullNameCollab};
                    String[] paramsForMsgProperties = {fullNameCollab};
                    prepareMailComposure(
                            properties.get("email.rappelEntretienAnnuel.object", paramsForObjProperties),
                            properties.get("email.rappelEntretienAnnuel.message", paramsForMsgProperties),
                            null,
                            recipients,
                            EMPTY_COLLABORATOR_COLLECTION,
                            EMPTY_COLLABORATOR_COLLECTION
                    );
                } else {
                    String[] paramsForObjProperties = {fullNameCollab};
                    String[] paramsForMsgProperties = new String[2];
                    paramsForMsgProperties[0] = fullNameCollab;
                    if (type == 2) {
                        paramsForMsgProperties[1] = "7";
                    } else {
                        paramsForMsgProperties[1] = "14";
                    }

                    prepareMailComposure(
                            properties.get("email.rappelFinPeriodeEssai.object", paramsForObjProperties),
                            properties.get("email.rappelFinPeriodeEssai.message", paramsForMsgProperties),
                            null,
                            recipients,
                            EMPTY_COLLABORATOR_COLLECTION,
                            EMPTY_COLLABORATOR_COLLECTION
                    );

                }
            }
        }
    }

    /**
     * Envoi mail rappel ra.
     *
     * @throws NamingException the naming exception
     */
    @Transactional
    public void envoiMailRappelRA() throws NamingException {

        List<Collaborator> collaborators = collaboratorService.findAllOrderByNomAscWithoutADMIN();
        List<Collaborator> collaboratorsWithUnsubmittedActivityReports = new ArrayList<>();
        SimpleDateFormat formater = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_01);
        Date date = new Date();
        String dateString = formater.format(date);
        String dateS = "";
        Etat etat = new Etat();
        etat.setCode(Etat.ETAT_BROUILLON_CODE);
        etat.setEtat(Etat.ETAT_BROUILLON);

        for (Collaborator collab : collaborators) {
            List<RapportActivites> listRA = collab.getRapportActivites();
            RapportActivites raMoisEnCours = new RapportActivites();
            int i = 0;
            int nbRA = listRA.size();
            //On récupère le RA du mois si il existe, sinon on enregistre un RA à l'Etat "brouillon"
            while (i < nbRA) {
                RapportActivites ra = listRA.get(i);
                dateS = formater.format(ra.getMoisRapport());
                if (!dateS.equals(dateString)) {
                    i++;
                } else {
                    raMoisEnCours = ra;
                    break;
                }
            }

            //Si le ra du mois en cours n'existe pas (etat==null) ou si le ra est à l'état annulé alors on ajoute le collab à la liste d'envoi
            if (raMoisEnCours.getEtat() == null || raMoisEnCours.getEtat().getCode().equals(Etat.ETAT_ANNULE)) {
                collaboratorsWithUnsubmittedActivityReports.add(collab);
            }
        }

        if (!collaboratorsWithUnsubmittedActivityReports.isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_MM_YYYY);

            String[] paramsForObjProperties = {dateFormat.format(date)
            };

            String[] paramsForMsgProperties = {dateFormat.format(date)
            };

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            if (!Boolean.valueOf(Parametrage.getContext(Constantes.EMAIL_PROD_LIST_CONTEXT))) {
                collaboratorsWithUnsubmittedActivityReports.clear();
                Collaborator developer = new Collaborator();
                developer.setMail(Parametrage.getContext(Constantes.EMAIL_DEV_LIST_CONTEXT));
                collaboratorsWithUnsubmittedActivityReports.add(developer);
            }

            if (day < 20) {
                prepareMailComposure(
                        properties.get("email.msgRappelRaValidation.object", paramsForObjProperties),
                        properties.get("email.msgRappelRaValidation.message", paramsForMsgProperties),
                        null,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION,
                        collaboratorsWithUnsubmittedActivityReports
                );
            } else {
                prepareMailComposure(
                        properties.get("email.msgRappelRaValidationBis.object", paramsForObjProperties),
                        properties.get("email.msgRappelRaValidationBis.message", paramsForMsgProperties),
                        null,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION,
                        collaboratorsWithUnsubmittedActivityReports
                );
            }
        }
    }

    /**
     * Envoi d'un seul mail limite commande.
     *
     * @param typeMail       the type mail
     * @param listeCommandes les commandes
     * @throws NamingException the naming exception
     */
    @Transactional
    public void envoiMailLimiteDesCommandes(int typeMail, List<Commande> listeCommandes) throws NamingException {
        List<Collaborator> collaborators = collaboratorService.findAll(null);
        List<Collaborator> recipients = new ArrayList<>();

        DateFormat formatFullDate = DateFormat.getDateInstance(DateFormat.FULL);
        for (Collaborator collab : collaborators) {
            if (collab.getStatut().getCode() == StatutCollaborateur.STATUT_DRH) { // If the collaborator is amongst human resources directors
                recipients.add(collab);
            }
        }
        if (!recipients.isEmpty() && !Boolean.valueOf(Parametrage.getContext(Constantes.EMAIL_PROD_LIST_CONTEXT))) { // if the recipients list isn't empty and the environment isn't set to production
            recipients.clear();
            Collaborator developer = new Collaborator();
            developer.setMail(Parametrage.getContext(Constantes.EMAIL_DEV_LIST_CONTEXT));
            recipients.add(developer);
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String mois = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        String[] paramsForObjProperties = {mois};
        if (typeMail == 1) { // Si le mail concerne le budget + la date
            // On initialise les parametres du mail
            String texte = "";
            for (Commande com : listeCommandes)
                texte = texte + ORDER_NUMBER + com.getNumero() + "</b> avec pour  budget restant <b>" + com.getBudget().toString() + " € </b> et date de fin <b>" + formatFullDate.format(com.getDateFin()) + "</b>.<p><p>";

            String[] paramsForMsgProperties = {texte};

            prepareMailComposure(
                    properties.get("email.alerteDesFacturationsBudgetDate.object", paramsForObjProperties),
                    properties.get("email.alerteDesFacturationsBudgetDate.message", paramsForMsgProperties),
                    null,
                    recipients,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        } else if (typeMail == 2) { // Si le mail concerne seulement le budget
            List<Parametre> parametre = parametreService.findAll(null);
            // Le budget est le premier élément de la table params
            Float seuilBudget = Float.valueOf(parametre.get(0).getValeur());
            //On initialise un Etat à "généré" pour les nouvelles factures
            String texte = "";

            for (Commande com : listeCommandes)
                texte = texte + ORDER_NUMBER + com.getNumero() + "</b> avec pour seuil minimum <b>" + seuilBudget.toString() + "€ </b>et pour budget restant <b>" + com.getBudget().toString() + "</b>€.<p><p>";

            String[] paramsForMsgProperties = {texte};

            prepareMailComposure(
                    properties.get("email.alerteDesFacturationsBudget.object", paramsForObjProperties),
                    properties.get("email.alerteDesFacturationsBudget.message", paramsForMsgProperties),
                    null,
                    recipients,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        } else if (typeMail == 3) { // Si le mail concerne que la date

            String texte = "";
            for (Commande com : listeCommandes)
                texte = texte + ORDER_NUMBER + com.getNumero() + "</b> avec date de fin le <b>" + formatFullDate.format(com.getDateFin()) + "</b>.<p><p>";
            String[] paramsForMsgProperties = {texte};

            prepareMailComposure(
                    properties.get("email.alerteDesFacturationsDate.object", paramsForObjProperties),
                    properties.get("email.alerteDesFacturationsDate.message", paramsForMsgProperties),
                    null,
                    recipients,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        }
    }


    /**
     * Envoi du mail aux commerciaux pour les facture sans bon de commande.
     *
     * @param pCommandeSansBonCmd the p commande sans bon cmd
     */
    @Transactional
    public void envoiMailFactureSansBonDeCmd(CommandeAsJson pCommandeSansBonCmd) {
        String[] paramsForObjProperties = {pCommandeSansBonCmd.getMission().getMission()};
        String[] paramsForMsgProperties = {pCommandeSansBonCmd.getMission().getMission(),
                pCommandeSansBonCmd.getMission().getCollaborateur().getPrenom(),
                pCommandeSansBonCmd.getMission().getCollaborateur().getNom()
        };
        List<Collaborator> recipients = new ArrayList<>();
        // retrieving the manager associated with the order
        recipients.add(pCommandeSansBonCmd.getMission().getCollaborateur().getManager());

        prepareMailComposure(
                properties.get("email.factureSansBonCmd.object", paramsForObjProperties),
                properties.get("email.factureSansBonCmd.message", paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Méthode destinée à un envoi de mail asynchrone aux responsables après demande de validation des absences par le collaborators
     *
     * @param collaborateur celui qui demande la validation des absences
     * @param pListId       la liste des id des absences concernées
     * @param pComment      le commentaire entré lors de la soumission des absences
     * @throws IOException the io exception
     */
    public void envoiMailDemandeAbsences(Collaborator collaborateur, String pListId, String pComment) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory tf = objectMapper.getTypeFactory();
        List<AbsenceAsJson> lListAbsenceAsJson;
        lListAbsenceAsJson = objectMapper.readValue(pListId, tf.constructCollectionType(List.class, AbsenceAsJson.class));

        StatutCollaborateur humanResourcesStatus = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        StatutCollaborateur directionStatut = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        mailRecipient = collaboratorService.retrieveCollaboratorByStatus(true, directionStatut, humanResourcesStatus);

        Collaborator manager = collaborateur.getManager();

        // si le manager du collaborators ne fait pas deja parti de la liste DRH, alors on l'ajoute
        if (null != manager && !mailAlreadyListed(mailRecipient, manager.getMail())) {
            mailRecipient.add(manager);

        }

        String collabFullName = getCollabLastAndFirstName(collaborateur);

        String subjectForDRH = "[Amilnote]: " + collabFullName + " a soumis une/des absence(s).";
        //Création du corps du mail.
        String contentForDRH;
        String contentForDRHVerif;
        if (lListAbsenceAsJson.size() > 1) {
            contentForDRH = collabFullName + " a soumis des absences :<br/><br/>";
        } else {
            contentForDRH = collabFullName + " a soumis une absence :<br/><br/>";
        }
        contentForDRHVerif = contentForDRH;
        for (AbsenceAsJson absenceJson : lListAbsenceAsJson) {

            Absence absence = absenceService.findUniqEntiteByProp("id", absenceJson.getId());
            SimpleDateFormat dateDebut = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);
            SimpleDateFormat dateFin = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);

            String dateDeb = dateDebut.format(absence.getDateDebut());
            String dateF = dateFin.format(absence.getDateFin());
            String typeAbsence = absence.getTypeAbsence().getTypeAbsence();

            contentForDRH = contentForDRH + "- " + typeAbsence + " du " + dateDeb + " au " + dateF + " <br/>";
        }

        if (!contentForDRH.equals(contentForDRHVerif)) {
            if ("".equals(pComment)) {
                contentForDRH = contentForDRH + "<p>" + EMPTY_MAIL_COMMENT + ".</p>";
            } else {
                contentForDRH = contentForDRH + "<p> Commentaire: " + pComment + "</p>";
            }

            // Envoi mail au(x) manager(s) et aux RH
            if (!mailRecipient.isEmpty()) {
                prepareMailComposure(
                        subjectForDRH,
                        contentForDRH,
                        null,
                        mailRecipient,
                        EMPTY_COLLABORATOR_COLLECTION,
                        EMPTY_COLLABORATOR_COLLECTION
                );
            }
        }

    }

    /**
     * checks if an email is already in a mailing list
     *
     * @param collaborators      is the mailing list
     * @param collabEmailAddress is the person that may be added
     * @return a boolean (true if the person is already in the mail list)
     */
    private boolean mailAlreadyListed(List<Collaborator> collaborators, String collabEmailAddress) {
        for (Collaborator collaborator : collaborators) {
            if ((collaborator.getMail()).equals(collabEmailAddress)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Send a mail for mission prolongation
     *
     * @param mission : modified mission
     */
    @Transactional
    public void sendMailMissionExtension(Mission mission) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT);

        String nomMission = mission.getMission();
        String[] paramsForObjProperties = {nomMission};
        String[] paramsForMsgProperties = {
                nomMission,
                dateFormat.format(mission.getDateFin())
        };

        List<Collaborator> recipients = Collections.singletonList(collaboratorService.get(mission.getCollaborateur().getId()));
        prepareMailComposure(
                properties.get("email.sendingMissionExtension.object", paramsForObjProperties),
                properties.get("email.sendingMissionExtension.message", paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send an email to the IT support when a collaborator is deactivated
     *
     * @param collaborateur collaborator to disable
     * @throws NamingException the NamingException
     */
    @Transactional
    public void sendMailDesactivationCollab(Collaborator collaborateur) throws NamingException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT);

        // Paramètres donnés à la propriété objetSources
        String[] paramsForObjProperties = {
                collaborateur.getNom()
        };
        // Paramètres donnés à la propriété messageSources
        String[] paramsForMsgProperties = {
                getCollabLastAndFirstName(collaborateur),
                dateFormat.format(collaborateur.getDateSortie()),
                SIGNATURE_ADMINISTRATION
        };

        Collaborator iTSupport = new Collaborator();

        // on récupère l'adresse email du Service Informatique via le fichier amilnote.xml
        iTSupport.setMail(Parametrage.getContext(Constantes.EMAIL_INFO_SERVICE_CONTEXT));
        List<Collaborator> iTSupportEmails = new ArrayList<>();
        iTSupportEmails.add(iTSupport);

        prepareMailComposure(
                properties.get("email.envoiDesactivationCollaborator.object", paramsForObjProperties),
                properties.get("email.envoiDesactivationCollaborator.message", paramsForMsgProperties),
                null,
                iTSupportEmails,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Méthode permettant d'envoyer la demande de déplacement au collaborators et aux statuts DRH
     *
     * @param collaborateur the collaborators
     * @param cheminFichier the chemin fichier
     * @param nomFichier    the nom fichier
     */
    public void sendMailMovement(Collaborator collaborateur, String cheminFichier, String nomFichier) {

        logger.debug(MAIL_DEBUG_START);
        List<Collaborator> recipients = new ArrayList<>();
        recipients.add(collaborateur);

        StatutCollaborateur humanResourcesStatus = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        StatutCollaborateur directionStatut = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        mailRecipient = collaboratorService.retrieveCollaboratorByStatus(true, directionStatut, humanResourcesStatus);

        String[] paramsForMsg = {getCollabLastAndFirstName(collaborateur)};

        List<String> attachedFiles = new ArrayList<>();
        if (cheminFichier != null && !cheminFichier.equals("") && nomFichier != null && !nomFichier.equals("")) {
            attachedFiles.add(cheminFichier + nomFichier);
        } else {
            attachedFiles = null;
        }
        prepareMailComposure(
                properties.get("email.sendingMovementCollab.object"),
                properties.get("email.sendingMovementCollab.message"),
                attachedFiles,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
        if (!mailRecipient.isEmpty()) {
            prepareMailComposure(
                    properties.get("email.sendingMovementHRD.object"),
                    properties.get("email.sendingMovementHRD.message", paramsForMsg),
                    attachedFiles,
                    mailRecipient,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        }

        logger.debug(MAIL_DEBUG_END);

    }

    public void sendMailODMCreation(Collaborator collabPdf) {
        List<Collaborator> directors = collaboratorService.findAllByStatutOrderByNom(StatutCollaborateur.STATUT_DIRECTION);

        // Paramètres donnés à la propriété messageSources
        String[] paramsForMsgProperties = {
                collabPdf.getNom(),
                collabPdf.getPrenom()
        };

        prepareMailComposure(
                properties.get("email.creationODM.object"),
                properties.get("email.creationODM.message", paramsForMsgProperties),
                null,
                directors,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail depending on mission order status (validate / refused)
     *
     * @param collaborateur collab concerned by mission order
     * @param mission       mission concerned by mission order
     * @param action        mission order status (validate or refused)
     * @param commentaire   comment for mail content
     * @throws Exception exception
     */
    public void sendMailMissionOrder(Collaborator collaborateur, Mission mission, String action, String commentaire) throws Exception {
        if (action.equals(Etat.ETAT_VALIDE_CODE)) {
            sendMailMissionOrderValidation(collaborateur, mission);
        } else if (action.equals(Etat.ETAT_REFUSE)) {
            sendMailMissionOrderRefused(collaborateur, commentaire);
        }
    }

    /**
     * Send a mail for mission order validation
     *
     * @param collaborateur the collaborator
     * @param mission       the mission
     * @throws NamingException
     */
    private void sendMailMissionOrderValidation(Collaborator collaborateur, Mission mission) throws NamingException {

        logger.debug(MAIL_DEBUG_START);

        List<Collaborator> recipients = (collaborateur != null) ? Collections.singletonList(collaborateur) : Collections.emptyList();
        StatutCollaborateur humanResourcesDirectorStatus = statutCollaborateurService.findUniqEntiteByProp(StatutCollaborateur.PROP_CODE, StatutCollaborateur.STATUT_DRH);
        mailRecipient = collaboratorService.findListEntitesByProp(Collaborator.PROP_STATUT, humanResourcesDirectorStatus);

        String[] paramsForMsgProperties = {
                mission.getMission(),
        };

        String[] paramsForHumanResourcesDirectorMsgProperties = {
                getCollabLastAndFirstName(collaborateur),
                mission.getMission()
        };

        String cheminFichier = Parametrage.getContext(Constantes.CONTEXT_FOLDER_ODM);
        String nomFichier;
        String[] paramsNomFichier = {
                collaborateur.getNom(),
                mission.getMission(),
                Long.toString(mission.getId()),
        };

        List<String> attachedFile = new ArrayList<>();
        //Mise en forme du nom du fichier
        nomFichier = properties.get("pdf.nomFichierODM", paramsNomFichier);

        if (cheminFichier != null && !"".equals(cheminFichier) && nomFichier != null && !"".equals(nomFichier)) {
            attachedFile.add(cheminFichier + nomFichier);
        }


        //Envoi du mail aux DRH: RA+justifs
        if (!mailRecipient.isEmpty()) {
            prepareMailComposure(
                    properties.get("email.sendingMissionOrderValidationHRD.object", paramsForHumanResourcesDirectorMsgProperties),
                    properties.get("email.sendingMissionOrderValidationHRD.message", paramsForHumanResourcesDirectorMsgProperties),
                    attachedFile,
                    mailRecipient,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION

            );
        }

        // Envoi du mail au collab seulement le RA
        prepareMailComposure(
                properties.get("email.sendingMissionOrderValidation.object", paramsForMsgProperties),
                properties.get("email.sendingMissionOrderValidation.message", paramsForMsgProperties),
                attachedFile,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );

        logger.debug(MAIL_DEBUG_END);

    }

    /**
     * Send a mail for mission order refusal
     *
     * @param collaborateur collab concerned by mission order
     * @param commentaire   comment for mail content
     */
    private void sendMailMissionOrderRefused(Collaborator collaborateur, String commentaire) {
        // Envoi de mail aux patrons pour validation ODM
        List<Collaborator> recipients = collaboratorService.findAllByStatutOrderByNom(StatutCollaborateur.STATUT_DRH);
        commentaire = (commentaire != null) ? new String(commentaire.getBytes(), Charset.forName(Constantes.UTF_8)) : "";

        String[] paramsForMsgProperties = {
                collaborateur.getNom(),
                collaborateur.getPrenom(),
                commentaire
        };

        prepareMailComposure(
                properties.get("email.refusODMCollab.object"),
                properties.get("email.refusODMCollab.message", paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Envoyer mail du rapport d'activités
     *
     * @param pApresSoumissionFacture the p après soumission ou non boolean
     * @param collaborators           Collaborator à traiter
     * @param cheminFichier           Chemin du fichier PDF à envoyer
     * @param nomFichier              Nom du fichier PDF
     * @param pDateVoulue             Date du traitement
     * @param pAvecFrais              the p avec frais
     * @throws IOException     the io exception
     * @throws NamingException the naming exception
     */
    public void sendMailActivityReportSubmit(Collaborator collaborators, String cheminFichier, String nomFichier, DateTime pDateVoulue, boolean pAvecFrais, String pApresSoumissionFacture) throws IOException, NamingException {

        logger.debug(MAIL_DEBUG_START);

        List<Collaborator> recipients = (collaborators != null) ? Collections.singletonList(collaborators) : Collections.emptyList();

        StatutCollaborateur humanResourcesStatus = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        mailRecipient = collaboratorService.retrieveCollaboratorByStatus(true, humanResourcesStatus);

        DateTimeFormatter fmt = DateTimeFormat.forPattern(Constantes.DATE_FORMAT_MM_YYYY);
        String subject;
        String content;
        String subjectForDRH;
        String contentForDRH;

        String[] paramsForObjProperties = {fmt.print(pDateVoulue)};
        String[] paramsForMsgProperties = {
                getCollabLastAndFirstName(collaborators),
                fmt.print(pDateVoulue)
        };

        //Dans le cas d'une soumission sans les Frais, on l'indique dans le mail
        if (pAvecFrais) {
            subject = "email.sendingActivityReportValidationCollab.object";
            content = "email.sendingActivityReportValidationCollab.message";

            subjectForDRH = "email.sendingActivityReportValidationHRD.object";
            contentForDRH = "email.sendingActivityReportValidationHRD.message";
        } else {
            subject = "email.sendingActivityReportValidationCostCollab.object";
            content = "email.sendingActivityReportValidationCostCollab.message";

            subjectForDRH = "email.sendingActivityReportValidationCostHRD.object";
            contentForDRH = "email.sendingActivityReportValidationCostHRD.message";
        }

        if ("".equals(pApresSoumissionFacture)) {
            if ("mauvaiseQuantite".equals(pApresSoumissionFacture) && pAvecFrais) {
                contentForDRH = "email.sendingActivityReportValidationHRDWrongAmount.message";
            } else if ("mauvaiseQuantite".equals(pApresSoumissionFacture) && !pAvecFrais) {
                contentForDRH = "email.sendingActivityReportValidationCostHRDWrongAmount.message";
            } else if ("bonneQuantite".equals(pApresSoumissionFacture) && pAvecFrais) {
                contentForDRH = "email.sendingActivityReportValidationHRDRightAmount.message";
            } else if ("bonneQuantite".equals(pApresSoumissionFacture) && !pAvecFrais) {
                contentForDRH = "email.sendingActivityReportValidationCostHRDRightAmount.message";
            }
        }

        List<String> attachedFiles = new ArrayList<>();

        DateTime lDebutPeriode = pDateVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = pDateVoulue.dayOfMonth().withMaximumValue();

        String[] fichiersFrais = new String[32];
        compteurDocuments = 0;
        if (cheminFichier != null && !cheminFichier.equals("")) {
            if (pAvecFrais) {
                // RA avec frais
                nomFichier = cheminFichier + nomFichier.concat(Constantes.EXTENSION_FILE_PDF);

                // Justifs frais
                List<LinkEvenementTimesheet> listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDates(collaborators, lDebutPeriode.toString(), lFinPeriode.toString());
                fichiersFrais = getListEvent(listLinkEvent, fichiersFrais);
            } else {
                nomFichier = cheminFichier + nomFichier.concat(Constantes.RA_SOCIAL_PDF_EXTENSION);

            }
        }
        attachedFiles.add(nomFichier);


        // Envoi du mail au collab seulement le RA
        prepareMailComposure(
                properties.get(subject, paramsForObjProperties),
                properties.get(content, paramsForMsgProperties),
                attachedFiles,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION);


        //Envoi du mail aux DRH: RA+justifs
        if (!mailRecipient.isEmpty()) {
            for (String fichier : fichiersFrais) {
                // on vérifie si le fichier existe bien. S'il n'existe pas on cherche une version ZIP. Si l'un des deux existe, on ajoute le fichier en piece jointe
                if (fichier != null) {
                    File file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + fichier);

                    if (file.exists()) {
                        attachedFiles.add(file.getPath());
                    } else if (!file.exists() && fileService.fileWasZipped(file.getPath())) {
                        file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + fichier.substring(0, fichier.lastIndexOf('.')) + Constantes.EXTENSION_FILE_ZIP);
                        attachedFiles.add(fileService.unZipFile(file).getPath());
                    }
                }
            }

            prepareMailComposure(
                    properties.get(subjectForDRH, paramsForObjProperties),
                    properties.get(contentForDRH, paramsForMsgProperties),
                    attachedFiles,
                    mailRecipient,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION);


            for (String file : attachedFiles) {
                if (fileService.fileWasZipped(file)) fileService.deleteFile(new File(file));
            }
        }


        logger.debug(MAIL_DEBUG_END);

    }

    private String[] getListEvent(List<LinkEvenementTimesheet> listLinkEvent, String[] nomFichiers) {
        for (LinkEvenementTimesheet tmpEvent : listLinkEvent) {
            // --- Evénement autre qu'une absence Validée ou Soumise
            if (!pdfBuilderRA.isAbsenceVAOrSO(tmpEvent.getAbsence())) {
                nomFichiers = getListCosts(tmpEvent, nomFichiers);
            }
        }
        return nomFichiers;
    }

    private String[] getListCosts(LinkEvenementTimesheet tmpEvent, String[] nomFichiers) {
        for (Frais tmpFrais : tmpEvent.getListFrais()) {
            // Si on a un frais ou une ASF ponctuelle validée
            if ((!Constantes.AVANCE_FRAIS_CODE.equals(tmpFrais.getTypeFrais().getCode()))
                    || (Constantes.AVANCE_FRAIS_CODE.equals(tmpFrais.getTypeFrais().getCode()) && Etat.ETAT_VALIDE_CODE.equals(tmpFrais.getEtat().getCode()) && Constantes.AVANCE_FRAIS_PONCTUEL_CODE.equals(tmpFrais.getTypeAvance()))) {
                nomFichiers = getListJustificatives(tmpFrais, nomFichiers);
            }
        }
        return nomFichiers;
    }

    private String[] getListJustificatives(Frais tmpFrais, String[] nomFichiers) {
        List<Justif> justifs = tmpFrais.getJustifs();
        for (Justif justif : justifs) {
            nomFichiers[compteurDocuments] = justif.getFichier();
            compteurDocuments++;
        }
        return nomFichiers;
    }


    /**
     * Send a mail depending on absence request reply
     *
     * @param state                 replay status
     * @param absence               absence concerned
     * @param collaborateurConnecte collab concerned
     * @param pCommentaire          comment
     */
    public void sendMailAbsenceValidation(String state, Absence absence, Collaborator collaborateurConnecte, String pCommentaire) {
        // Paramètres donnés à properties (va remplacer les {n} dans le message)
        List<Collaborator> recipients = new ArrayList<>();
        Collaborator collaborator = absence.getCollaborateur();
        recipients.add(collaborator);

        List<Collaborator> recipientsManagers = new ArrayList<>();
        Collaborator manager = collaborator.getManager();
        if (null != manager) {
            recipientsManagers.add(manager);
        }

        //Création des dates formatées pour le contenu des mails
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT);
        String absenceDateDebut = dateFormat.format(absence.getDateDebut());
        String absenceDateFin = dateFormat.format(absence.getDateFin());

        String collabConnecteFullName = getCollabLastAndFirstName(collaborateurConnecte);
        // Paramètres donnés à properties (va remplacer les {n} dans le message)
        String[] paramsForObjProperties = {
                collabConnecteFullName,
        };
        String[] paramsForMsgProperties = {
                absence.getTypeAbsence().getTypeAbsence(),
                Float.toString(absence.getNbJours()),
                absenceDateDebut,
                absenceDateFin,
                (pCommentaire != null ? pCommentaire : EMPTY_MAIL_COMMENT)
        };
        String[] paramsForObjPropertiesManager = {
                collaborator.getPrenom(),
                collaborator.getNom(),
                collabConnecteFullName
        };
        String[] paramsForMsgPropertiesManager = {
                collaborator.getPrenom(),
                collaborator.getNom(),
                absence.getTypeAbsence().getTypeAbsence(),
                Float.toString(absence.getNbJours()),
                absenceDateDebut,
                absenceDateFin,
                (pCommentaire != null ? pCommentaire : EMPTY_MAIL_COMMENT)
        };

        String object = "";
        String message = "";
        String objectForManager = "";
        String messageForManager = "";
        switch (state) {
            //cas absence soumise --> validée
            case Etat.ETAT_VALIDE_CODE:

                object = "email.msgAbsenceValidee.object";
                message = "email.msgAbsenceValidee.message";
                objectForManager = "email.msgAbsenceValideeForManager.object";
                messageForManager = "email.msgAbsenceValideeForManager.message";

                break;

            //cas absence soumise --> refusée
            case Etat.ETAT_REFUSE:

                object = "email.msgAbsenceRefusee.object";
                message = "email.msgAbsenceRefusee.message";
                objectForManager = "email.msgAbsenceRefuseeForManager.object";
                messageForManager = "email.msgAbsenceRefuseeForManager.message";

                break;
            //cas absence validée --> annulée
            case Etat.ETAT_ANNULE:

                object = "email.msgAbsenceAnnulee.object";
                message = "email.msgAbsenceAnnulee.message";
                objectForManager = "email.msgAbsenceAnnuleeForManager.object";
                messageForManager = "email.msgAbsenceAnnuleeForManager.message";

                break;

            default:
        }
        prepareMailComposure(
                properties.get(object, paramsForObjProperties),
                properties.get(message, paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );

        if (!recipientsManagers.isEmpty()) {

            prepareMailComposure(
                    properties.get(objectForManager, paramsForObjPropertiesManager),
                    properties.get(messageForManager, paramsForMsgPropertiesManager),
                    null,
                    recipientsManagers,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );

        }
    }

    /**
     * Send a mail for activity report cancellation
     *
     * @param date          date
     * @param commentaire   commentary
     * @param collaborateur a {@link CollaboratorAsJson}
     */
    public void sendMailActivityReportCanceled(String date, String commentaire, CollaboratorAsJson collaborateur) {
        List<Collaborator> recipients = new ArrayList<>();
        recipients.add(collaboratorService.findByMail(collaborateur.getMail()));
        //envoi du mail
        String[] paramsForMsgProperties = {date, commentaire};
        prepareMailComposure(
                properties.get("email.msgRADeValidee.object"),
                properties.get("email.msgRADeValidee.message", paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail for collab creation with login and password
     *
     * @param pCollaboratorAsJson a {@link CollaboratorAsJson}
     * @param clearTmpPassword     a string
     * @param env                  env
     */
    public void sendMailCreationCollab(CollaboratorAsJson pCollaboratorAsJson, String clearTmpPassword, String env) {
        List<Collaborator> recipients = new ArrayList<>();
        recipients.add(collaboratorService.findByMail(pCollaboratorAsJson.getMail()));
        prepareMailComposure(
                properties.get("email.msgCreationUser.object", new String[]{env}),
                properties.get("email.msgCreationUser.message", new String[]{
                        pCollaboratorAsJson.getMail(),
                        clearTmpPassword,
                }),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail for mission order validation
     *
     * @param mission {@link Mission}
     */
    public void sendMailAdminMissionOrder(Mission mission) {
        String[] paramsForMsgProperties = {
                mission.getMission()
        };
        List<Collaborator> recipients = Collections.singletonList(mission.getCollaborateur());

        prepareMailComposure(
                properties.get("email.validationODMAdmin.object"),
                properties.get("email.validationODMAdmin.message", paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail for advance submit
     *
     * @param frais frais
     */
    public void sendMailAdvanceSubmit(Frais frais) {
        List<Collaborator> recipients = new ArrayList<>();
        recipients.add(frais.getLinkEvenementTimesheet().getMission().getCollaborateur());

        StatutCollaborateur humanResourcesStatus = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        StatutCollaborateur directionStatut = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        mailRecipient = collaboratorService.retrieveCollaboratorByStatus(true, directionStatut, humanResourcesStatus);

        if (!mailRecipient.isEmpty() && !recipients.isEmpty()) {
            String collabFullName = getCollabLastAndFirstName(recipients.get(0));

            String[] paramsForObjProperties = {};
            String[] paramsForMsgProperties = {collabFullName,
                    frais.getMontant().toString(), frais.getCommentaire()};

            prepareMailComposure(
                    properties.get("email.msgSoumissionDemandeAvance.object", paramsForObjProperties),
                    properties.get("email.msgSoumissionDemandeAvance.message", paramsForMsgProperties),
                    null,
                    mailRecipient,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        }
    }

    /**
     * Send a mail depending on advance request reply
     *
     * @param state        reply status
     * @param frais        cost
     * @param pCommentaire comment
     */
    public void sendMailAdvanceValidation(String state, Frais frais, String pCommentaire) {
        List<Collaborator> recipients = new ArrayList<>();
        Mission mission = frais.getLinkEvenementTimesheet().getMission();
        Collaborator collaborator = mission.getCollaborateur();
        recipients.add(collaborator);

        // Paramètres donnés à properties (va remplacer les {n} dans le message)
        String[] paramsForMsgProperties = {
                frais.getMontant().toString(),
                (pCommentaire != null ? pCommentaire : EMPTY_MAIL_COMMENT)
        };

        String object = "";
        String message = "";

        switch (state) {
            //cas soumis --> validé
            case Etat.ETAT_VALIDE_CODE:

                object = "email.msgGestionDemandeAvanceOK.object";
                message = "email.msgGestionDemandeAvanceOK.message";

                break;

            //cas soumis --> refusé
            case Etat.ETAT_REFUSE:

                object = "email.msgGestionDemandeAvanceNOK.object";
                message = "email.msgGestionDemandeAvanceNOK.message";

                break;

            //cas validé --> soldé
            case Etat.ETAT_SOLDE:

                object = "email.msgGestionDemandeAvanceSolde.object";
                message = "email.msgGestionDemandeAvanceSolde.message";

                break;

            default:
        }

        prepareMailComposure(
                properties.get(object),
                properties.get(message, paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail for password reset
     *
     * @param collaborateur collab concerned
     * @param env           environment (dev or prod)
     * @param newMdp        new password
     */
    public void sendMailPasswordReset(Collaborator collaborateur, String env, String newMdp) {
        List<Collaborator> recipients = (collaborateur != null) ? Collections.singletonList(collaborateur) : Collections.emptyList();
        prepareMailComposure(
                properties.get("email.msgReinitMdp.object", new String[]{env}),
                properties.get("email.msgReinitMdp.message", new String[]{newMdp}),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }

    /**
     * Send a mail depending on move request reply
     *
     * @param state                 reply status
     * @param collaborateurConnecte collab concerned
     * @param deplacement           move request
     * @param pCommentaire          comment for reply
     */
    public void sendMailMovementValidation(String state, Collaborator collaborateurConnecte, DemandeDeplacement deplacement, String pCommentaire) {
        List<Collaborator> recipients = new ArrayList<>();
        Mission mission = deplacement.getMission();
        Collaborator collaborator = mission.getCollaborateur();
        recipients.add(collaborator);

        // dates handling
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT);
        String deplacementDateDebut = dateFormat.format(deplacement.getDateDebut());
        String deplacementDateFin = dateFormat.format(deplacement.getDateFin());
        String nameCollab = getCollabLastAndFirstName(collaborateurConnecte);
        String nameMission = mission.getMission();

        // Recipient list of managers and HRD
        List<Collaborator> recipientsManagersList = (collaborator.getManager() != null) ? Collections.singletonList(collaborator.getManager()) : Collections.emptyList();

        // Parameters for mail content
        String[] paramsForObjProperties = {
                collaborator.getPrenom(),
                collaborator.getNom(),
                nameCollab
        };

        String[] paramsForMsgProperties = {
                collaborator.getPrenom(),
                collaborator.getNom(),
                Float.toString(deplacement.getNbJours()),
                deplacementDateDebut,
                deplacementDateFin,
                deplacement.getLieu(),
                nameMission,
                (pCommentaire != null ? pCommentaire : EMPTY_MAIL_COMMENT)
        };


        String object = "";
        String message = "";
        String objectForManager = "";
        String messageForManager = "";
        switch (state) {
            case Etat.ETAT_ANNULE:
                object = "email.msgDeplacementAnnule.object";
                message = "email.msgDeplacementAnnule.message";
                objectForManager = "email.msgDeplacementAnnuleForManager.object";
                messageForManager = "email.msgDeplacementAnnuleForManager.message";

                break;

            case Etat.ETAT_VALIDE_CODE:

                object = "email.msgDeplacementValide.object";
                message = "email.msgDeplacementValide.message";
                objectForManager = "email.msgDeplacementValideForManager.object";
                messageForManager = "email.msgDeplacementValideForManager.message";

                break;

            case Etat.ETAT_REFUSE:

                object = "email.msgDeplacementRefuse.object";
                message = "email.msgDeplacementRefuse.message";
                objectForManager = "email.msgDeplacementRefuseForManager.object";
                messageForManager = "email.msgDeplacementRefuseForManager.message";

                break;

            default:
        }

        prepareMailComposure(
                properties.get(object, paramsForObjProperties),
                properties.get(message, paramsForMsgProperties),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );

        if (!recipientsManagersList.isEmpty()) {

            prepareMailComposure(
                    properties.get(objectForManager, paramsForObjProperties),
                    properties.get(messageForManager, paramsForMsgProperties),
                    null,
                    recipientsManagersList,
                    EMPTY_COLLABORATOR_COLLECTION,
                    EMPTY_COLLABORATOR_COLLECTION
            );
        }
    }

    public void sendMailAddviseCollaborator(Properties properties, List<Collaborator> recipients, String keyObject, String keyMessage, String[] params) {
        prepareMailComposure(
                properties.get(keyObject, params),
                properties.get(keyMessage, params),
                null,
                recipients,
                EMPTY_COLLABORATOR_COLLECTION,
                EMPTY_COLLABORATOR_COLLECTION
        );
    }


    private String getCollabLastAndFirstName(Collaborator collaborateur) {
        return (collaborateur.getNom() + " " + collaborateur.getPrenom());
    }

    /**
     * Gathers all the information required to send an email
     *
     * @param senderMail    the email address of the sender
     * @param senderName    the name of the sender
     * @param subject       the subject of the mail
     * @param content       the content of the mail
     * @param filePaths     the filepaths of the attached files
     * @param recipients    the list of recipients
     * @param recipientsCC  the list of recipients in CC
     * @param recipientsBCC the list of recipients in BCC
     */
    private void prepareMailComposure(String senderMail, String senderName, String subject, String content,
                                      List<String> filePaths, List<Collaborator> recipients,
                                      List<Collaborator> recipientsCC, List<Collaborator> recipientsBCC) {

        List<String> addresses = new ArrayList<>();
        List<String> addressesCC = new ArrayList<>();
        List<String> addressesBCC = new ArrayList<>();

        for (Collaborator recipient : recipients) {
            addresses.add(recipient.getMail());
        }
        for (Collaborator recipient : recipientsCC) {
            addressesCC.add(recipient.getMail());
        }
        for (Collaborator recipient : recipientsBCC) {
            addressesBCC.add(recipient.getMail());
        }

        List<File> attachedFiles = new ArrayList<>();

        if (filePaths != null && !filePaths.isEmpty()) {
            for (String path : filePaths) {
                attachedFiles.add(new File(path));
            }
        }

        Utils.sendEmail(senderMail, senderName, subject, content, attachedFiles, addresses, addressesCC, addressesBCC);

    }

    /**
     * Gathers all the information required to send an email with default sender
     *
     * @param subject       the subject of the mail
     * @param content       the content of the mail
     * @param filePaths     the filepaths of the attached files
     * @param recipients    the list of recipients
     * @param recipientsCC  the list of recipients in CC
     * @param recipientsBCC the list of recipients in BCC
     */
    private void prepareMailComposure(String subject, String content,
                                      List<String> filePaths, List<Collaborator> recipients,
                                      List<Collaborator> recipientsCC, List<Collaborator> recipientsBCC) {
        prepareMailComposure(expediteurMail, expediteurNom, subject, content, filePaths, recipients, recipientsCC,
                recipientsBCC);

    }
}