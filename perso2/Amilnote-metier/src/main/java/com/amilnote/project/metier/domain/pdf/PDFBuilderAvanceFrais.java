/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.TypeFrais;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.naming.Context;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import static com.amilnote.project.metier.domain.utils.Constantes.PDF_HEADER_BLUE_COLOR;

/**
 * The type Pdf builder avance frais.
 */
@Service("PDFBuilderAvanceFrais")
public class PDFBuilderAvanceFrais extends PDFBuilder {

    private static final Logger logger = LogManager.getLogger(PDFBuilderAvanceFrais.class);
    private static String repertoireAvanceFrais = null;

    private Context ctx = null;

    /**
     * Create pdf avance frais string.
     *
     * @param collaborator the p collaborateur
     * @param pFrais         the p frais
     * @return the string
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     * @throws NamingException    the naming exception
     */
    public String createPDFAvanceFrais(Collaborator collaborator, FraisAsJson pFrais) throws IOException, SchedulerException, EmailException, MessagingException, DocumentException, NamingException {

        repertoireAvanceFrais = Parametrage.getContext("dossierPDFAVANCEFRAIS");

        logger.debug("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");

        String nomFichier = "";
        String[] paramsNomFichier = {
                Long.toString(pFrais.getId()),
                collaborator.getNom()
        };

        //Mise en forme du nom du fichier
        nomFichier = properties.get("pdf.nomFichierASF", paramsNomFichier);
        String chemin = repertoireAvanceFrais + nomFichier;

        File directory = new File(chemin).getParentFile(); //on récupère les dossiers parents
        directory.mkdirs(); //s'il en manque on les crée
        FileOutputStream lBaos = new FileOutputStream(chemin);
        pFrais.setPdfAvanceFrais(nomFichier); //on mémorise le nom pour pouvoir visulaiser le fichier dans la partie admin

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;

        lWriter = newWriter(lDocument, lBaos);
        TableHeader lEvent = new TableHeader();
        lEvent.setHeader("Avance sur frais collaborateur: " + "\n" + collaborator.getNom());

        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);

        // -----------------------------------------------------------//
        // CONSTITUTION DU CONTENU DU PDF DEMANDE D'AVANCE SUR FRAIS //
        // ---------------------------------------------------------//
        // ouverture du document PDF
        lDocument.open();

        // table date courante
        lDocument.add(createTableDateCreation("DATE DE CREATION DE L'AVANCE SUR FRAIS", new Date()));
        // table collaborateur & responsable
        lDocument.add(createTableUser(collaborator));
        //Table de détail de la demande
        lDocument.add(createTableDetailAvance(pFrais));

        // fermeture du document pdf
        lDocument.close();
        logger.debug("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");

        return nomFichier;
    }


    private PdfPTable createTableDetailAvance(FraisAsJson pFrais) {

        // création des fonts
        FontFactory.registerDirectories();
        Font headerWhite = FONT_HEADER;
        headerWhite.setColor(BaseColor.WHITE);

        PdfPTable lTableDetail = new PdfPTable(2);
        lTableDetail.setWidthPercentage(100);
        lTableDetail.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableDetail.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableDetail.getDefaultCell().setColspan(2);
        lTableDetail.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableDetail.addCell(new Phrase("DETAIL DE L'AVANCE SUR FRAIS", headerWhite));
        lTableDetail.getDefaultCell().setColspan(1);
        lTableDetail.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        try {
            lTableDetail.setWidths(new int[]{50, 50});
        } catch (DocumentException e) {
            logger.error("[create table advance] set widths", e);
        }

        //On rempli le tableau avec les différentes informations présentes dans la bdd
        if (null != pFrais.getDateEvenement()) {
            lTableDetail.addCell(new Phrase("Date de la demande: ", FONT_BOLD));
            lTableDetail.addCell(DATE_FORMATER.format(pFrais.getDateEvenement()));
        }

        if (null != pFrais.getMontant()) {
            lTableDetail.addCell(new Phrase("Montant de l'avance: ", FONT_BOLD));
            lTableDetail.addCell(new Phrase(String.format("%.2f", pFrais.getMontant()) + " €"));
        }

        if (null != pFrais.getTypeAvance()) {
            lTableDetail.addCell(new Phrase("Type: ", FONT_BOLD));
            if (TypeFrais.TYPEFRAIS_PEAGE.equals(pFrais.getTypeAvance())) {
                lTableDetail.addCell("Avance permanente");
            } else if ("PO".equals(pFrais.getTypeAvance())) {
                lTableDetail.addCell("Avance ponctuelle");
            }
        }

        if (null != pFrais.getCommentaire() && !(pFrais.getCommentaire()).equals("")) {
            lTableDetail.addCell(new Phrase("Motif: ", FONT_BOLD));
            lTableDetail.addCell(pFrais.getCommentaire());
        }

        if (null != pFrais.getDateValidation()) {
            lTableDetail.addCell(new Phrase("Date de paiement: ", FONT_BOLD));
            lTableDetail.addCell(DATE_FORMATER.format(pFrais.getDateValidation()));

        }

        //Ajout de la colonne date a solder pour typeAvance ='PE'
        if (TypeFrais.TYPEFRAIS_PEAGE.equals(pFrais.getTypeAvance()) && (pFrais.getEtat().getCode().equals(Etat.ETAT_SOLDE)) && pFrais.getDateSolde() != null) {
            String now = DATE_FORMATER.format(pFrais.getDateSolde());
            lTableDetail.addCell(new Phrase("Date de solde: ", FONT_BOLD));
            lTableDetail.addCell(now);
        }

        return lTableDetail;
    }
}
