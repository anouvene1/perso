/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.DemandeDeplacement;

/**
 * The interface Demande deplacement dao.
 */
public interface DemandeDeplacementDAO extends LongKeyDAO<DemandeDeplacement> {

}
