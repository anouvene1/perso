/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ModuleAddviseAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Module addvise.
 */
/*
 * SHA AMNOTE-203 12/01/2017.
 */
@Entity
@Table(name = "ami_ref_module_addvise")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ModuleAddvise implements Serializable, Comparable<ModuleAddvise> {


    /**
     * The constant PROP_ID_MODULE_ADDVISE.
     */
    public static final String PROP_ID_MODULE_ADDVISE = "id";
    /**
     * The constant PROP_CODE_MODULE_ADDVISE.
     */
    public static final String PROP_CODE_MODULE_ADDVISE = "code";
    /**
     * The constant PROP_NOM_MODULE_ADDVISE.
     */
    public static final String PROP_NOM_MODULE_ADDVISE = "nom_addvise";

    private Long id;
    private String codeModuleAddvise;
    private String nomModuleAddvise;

    /**
     * Constructeur
     */
    public ModuleAddvise() {
    }

    /**
     * Instantiates a new Module addvise.
     *
     * @param id                the id
     * @param codeModuleAddvise the code module addvise
     * @param nomModuleAddvise  the nom module addvise
     */
    public ModuleAddvise(Long id, String codeModuleAddvise, String nomModuleAddvise) {
        this.id = id;
        this.codeModuleAddvise = codeModuleAddvise;
        this.nomModuleAddvise = nomModuleAddvise;
    }

    /**
     * Convertit un PerimetreMission en LinkPerimetreMissionAsJson
     *
     * @return the module addvise as json
     */
    public ModuleAddviseAsJson toJson() {
        return new ModuleAddviseAsJson(this);
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets code module addvise.
     *
     * @return codeModuleAddvise code module addvise
     */
    @Column(name = "code", nullable = false)
    public String getCodeModuleAddvise() {
        return codeModuleAddvise;
    }

    /**
     * Sets code module addvise.
     *
     * @param codeModuleAddvise the dateDebut to set
     */
    public void setCodeModuleAddvise(String codeModuleAddvise) {
        this.codeModuleAddvise = codeModuleAddvise;
    }

    /**
     * Gets nom module addvise.
     *
     * @return nomModuleAddvise nom module addvise
     */
    @Column(name = "nom_addvise", nullable = false)
    public String getNomModuleAddvise() {
        return nomModuleAddvise;
    }

    /**
     * Sets nom module addvise.
     *
     * @param nomModuleAddvise the dateDebut to set
     */
    public void setNomModuleAddvise(String nomModuleAddvise) {
        this.nomModuleAddvise = nomModuleAddvise;
    }

    @Override
    public int compareTo(ModuleAddvise module2) {

        String[] codeM1 = this.getCodeModuleAddvise().split("M");
        String[] codeM2 = module2.getCodeModuleAddvise().split("M");

        if (codeM1.length > 1 && codeM2.length > 1)
            if (Integer.parseInt(codeM1[1]) > Integer.parseInt(codeM2[1])) {
                return 10;
            } else if (Integer.parseInt(codeM1[1]) < Integer.parseInt(codeM2[1])) {
                return -10;
            } else {
                return 0;
            } else {
            return this.getCodeModuleAddvise().compareTo(module2.getCodeModuleAddvise());
        }
    }
}
