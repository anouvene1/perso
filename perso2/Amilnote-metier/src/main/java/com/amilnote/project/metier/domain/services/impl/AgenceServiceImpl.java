package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.AgenceDAO;
import com.amilnote.project.metier.domain.dao.impl.AgenceDAOImpl;
import com.amilnote.project.metier.domain.entities.Agence;
import com.amilnote.project.metier.domain.entities.json.AgenceAsJson;
import com.amilnote.project.metier.domain.services.AgenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The Agence service.
 */
@Service("AgenceService")
public class AgenceServiceImpl extends AbstractServiceImpl<Agence, AgenceDAOImpl> implements AgenceService {

    private static final Logger logger = LogManager.getLogger(Agence.class);

    @Autowired
    private AgenceDAO agenceDAO;


    @Override
    public List<Agence> getAllAgence() {
        return agenceDAO.getAllAgence();
    }

    @Override
    public List<Agence> getAgenceChoixODM() {
        List<Agence> listeAgence = new ArrayList<Agence>();
        listeAgence.add(this.getAgenceSiegeSocial());
        listeAgence.add(this.getAgenceByCode(Agence.PROP_AGENCE_NIORT));
        listeAgence.add(this.getAgenceByCode(Agence.PROP_AGENCE_AIX_EN_PROVENCE));
        return listeAgence;
    }

    @Override
    public Agence getAgenceById(long pId) {
        return agenceDAO.getAgenceById(pId);
    }

    @Override
    public Agence getAgenceSiegeSocial() {
        Agence agenceFinal = null;
        List<Agence> lListAgence = getAllAgence();
        for (Agence ag : lListAgence) {
            if (ag.isSiege_social()) {
                agenceFinal = ag;
            }
        }
        return agenceFinal;
    }

    @Override
    public List<AgenceAsJson> getAllAgenceAsJson() {
        List<Agence> lListAgence = agenceDAO.getAllAgence();
        List<AgenceAsJson> lListAgenceAsJson = new ArrayList<AgenceAsJson>();

        for (Agence ag : lListAgence) {
            lListAgenceAsJson.add(new AgenceAsJson(ag));
        }
        return lListAgenceAsJson;
    }

    @Override
    public Agence getAgenceByCode(String pCode) {
        Agence agenceFinal = null;
        List<Agence> lListAgence = getAllAgence();
        for (Agence ag : lListAgence) {
            if (ag.getCode().equals(pCode)) {
                agenceFinal = ag;
            }
        }
        return agenceFinal;
    }

    @Override
    public AgenceDAOImpl getDAO() {
        return (AgenceDAOImpl) agenceDAO;
    }
}
