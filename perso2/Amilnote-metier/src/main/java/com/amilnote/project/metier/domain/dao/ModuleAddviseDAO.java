/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.ModuleAddvise;

import java.util.List;

/**
 * The interface Module addvise dao.
 */
public interface ModuleAddviseDAO extends LongKeyDAO<ModuleAddvise> {

    /**
     * Find by id module addvise.
     *
     * @param id the id
     * @return the module addvise
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    ModuleAddvise findById(Long id);

    /**
     * Find all list.
     *
     * @return the list
     */
    List<ModuleAddvise> findAll();

    /**
     * Delete module addvise int.
     *
     * @param pModuleAddvise the p module addvise
     * @return the int
     */
    int deleteModuleAddvise(ModuleAddvise pModuleAddvise);

    /**
     * Create or update module addvise int.
     *
     * @param pModuleAddvise the p module addvise
     * @return the int
     */
    int createOrUpdateModuleAddvise(ModuleAddvise pModuleAddvise);
}
