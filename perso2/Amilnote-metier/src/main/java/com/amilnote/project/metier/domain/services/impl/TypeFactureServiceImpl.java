/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.TypeFactureDAOImpl;
import com.amilnote.project.metier.domain.entities.TypeFacture;
import com.amilnote.project.metier.domain.services.TypeFactureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("typeFactureService")
public class TypeFactureServiceImpl extends AbstractServiceImpl<TypeFacture, TypeFactureDAOImpl> implements TypeFactureService {

    @Autowired
    public TypeFactureDAOImpl typeFactureDao;


    @Override
    public TypeFacture findById(long id) {
        return typeFactureDao.get(id);
    }

    @Override
    public TypeFacture findByCode(String code) {
        return typeFactureDao.findUniqEntiteByProp(TypeFacture.PROP_CODE, code);
    }

    @Override
    public TypeFactureDAOImpl getDAO() {
        return typeFactureDao;
    }
}
