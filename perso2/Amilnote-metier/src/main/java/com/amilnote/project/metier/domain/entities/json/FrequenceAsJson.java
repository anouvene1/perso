/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Frequence;


/**
 * The type Frequence as json.
 *
 * @author GCornet
 */
public class FrequenceAsJson {


    private Long id;
    private String code;
    private String frequence;

    /**
     * Instantiates a new Frequence as json.
     */
    public FrequenceAsJson() {
    }

    /**
     * Instantiates a new Frequence as json.
     *
     * @param pFrequence the p frequence
     */
    public FrequenceAsJson(Frequence pFrequence) {
        super();
        this.setId(pFrequence.getId());
        this.setCode(pFrequence.getCode());
        this.setFrequence(pFrequence.getFrequence());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }


    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets frequence.
     *
     * @return the frequence
     */
    public String getFrequence() {
        return frequence;
    }

    /**
     * Sets frequence.
     *
     * @param pFrequence the frequence to set
     */
    public void setFrequence(String pFrequence) {
        frequence = pFrequence;
    }
}
