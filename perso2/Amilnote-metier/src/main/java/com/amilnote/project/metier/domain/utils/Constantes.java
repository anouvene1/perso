/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.itextpdf.text.BaseColor;

/**
 * The type Constantes.
 */
public class Constantes {

    /**
     * tableau des mois en français
     */
    public static final String[] FRENCH_MONTHS_NAMES = new String[]{
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
    };


    public static final String COLLABORATOR_LABEL = "collaborateurs";
    public static final String SUBCONTRACTOR_LABEL = "sous_traitants";

    //titres
    public static final String RECAP_MOIS = "Votre récapitulatif mensuel";
    public static final  String TITRE_BLOCK_GAUCHE = "Vos missions :";
    public static final  String SSTITRE_ACCORDION_1 = "Liste des dépenses";
    public static final String SSTITRE_ACCORDION_2 = "Historique & Synthèse";

    //phrases
    public static final String NB_FRAIS_VALIDATION = "Nombre de frais en attente de votre validation :";
    public static final String SUCCESS_RA = "<h4>Votre rapport d'activités est en cours de traitement.</h4><p>Un mail vous sera envoyé à votre adresse amiltone pour confirmer l'envoi de votre rapport à votre manager.</p>";
    public static final String FAIL_RA = "<h4>Votre rapport d'activités n'a pas pu être transmis...</h4><p>Veuillez contacter  l'administrateur.</p>";
    public static final String RA_TYPE_PDF = "RA";
    public static final String RA_SANS_FRAIS_TYPE_PDF = "RASF";
    public static final String NOM_PDF_SOCIAL = "social";
    public static final String NOM_PDF_COLLAB = "collab";
    public static final String NOM_PDF_ADMIN = "admin";
    public static final String ALIAS_FRAIS = "frais";
    public static final String ALIAS_EVENT = "event";
    public static final String ALIAS_ETAT = "etat";
    public static final String ALIAS_COLLAB = "collab";
    public static final String SIMPLE_RETURN = "\n";
    public static final String DOUBLE_RETURN = "\n\n";
    public static final String COMMA_SEPARATION = ", ";
    public static final String SPACE_SEPARATION = " ";
    public static final String DASH = "- ";
    public static final String SLASH = "/";
    public static final String UNDERSCORE = "_";
    public static final String NOT_ASSIGNED = "N/A";

    // EXTENSION FILE
    public static final String EXTENSION_FILE_XLS = ".xls";
    public static final String EXTENSION_FILE_CSV = ".csv";
    public static final String EXTENSION_FILE_PDF = ".pdf";
    public static final String EXTENSION_FILE_ZIP = ".zip";
    public static final String RA_SOCIAL_PDF_EXTENSION= "_SOCIAL.pdf";
    public static final String RA_SOCIAL_ZIP_EXTENSION= "_SOCIAL.zip";
    public static final String SOCIAL_EXTENSION= "_SOCIAL";
    public static final String AVANCE_FRAIS_CODE = "ASF";
    public static final String AVANCE_FRAIS_PONCTUEL_CODE = "PO";

    // EMAIL PARAMETERS
    public static final String EMAIL_DEV_LIST_CONTEXT = "devTargetRecipient";
    public static final String EMAIL_PROD_LIST_CONTEXT = "env.prod";
    public static final String EMAIL_INFO_SERVICE_CONTEXT = "infoServiceMailAddress";
    public static final String EMAIL_ADDVISE_CONTEXT = "addviseMailAddress";
    public static final String EMAIL_ADMIN_MAIL_CONTEXT = "mailAdmin";
    public static final String EMAIL_ADMIN_NAME_CONTEXT = "nomAdmin";

    // EMAIL DOMAINES
    public static final String AMILTONE_EMAIL_DOMAIN_FR = "@amiltone.fr";
    public static final String AMILTONE_EMAIL_DOMAIN_COM = "@amiltone.com";

    // ADMINISTRATOR CONTEXT EMAIL PARAMETERS
    public static final String AUTHENTICATION_ADMIN_ADDRESS_CONTEXT = "authenticationAdminAddress";
    public static final String AUTHENTICATION_ADMIN_PASSWORD_CONTEXT = "authenticationAdminPassword";
    public static final String EMAIL_ADMIN_ADDRESS_CONTEXT = "adminMailAddress";
    public static final String HOSTNAME_ADMIN_EMAIL_CONTEXT = "hostnameAdminMail";

    //access
    public String recapMois = RECAP_MOIS;
    public String titreBlockGauche = TITRE_BLOCK_GAUCHE;
    public String ssTitreAccordion1 = SSTITRE_ACCORDION_1;
    public String ssTitreAccordion2 = SSTITRE_ACCORDION_2;
    public String nbFraisValidation = NB_FRAIS_VALIDATION;
    public String successRA = SUCCESS_RA;
    public String failRA = FAIL_RA;

    // CONTEXT PATH FOLDER
    public static final String CONTEXT_FOLDER_RA = "dossierPDFRA";
    public static final String CONTEXT_FOLDER_FRAIS = "dossierFrais";
    public static final String TEMP_FOLDER_FILES = "tempFolder";
    public static final String CONTEXT_FOLDER_DEPLACEMENT = "dossierPDFDEPLACEMENT";
    public static final String CONTEXT_FOLDER_AVANCE_FRAIS = "dossierPDFAVANCEFRAIS";
    public static final String CONTEXT_FOLDER_ODM = "dossierPDFODM";
    public static final String CONTEXT_FOLDER_JUSTIF_COMMANDES = "dossierJustificatifsCommandes";
    public static final String CONTEXT_FOLDER_ADDVISE = "dossierAddvise";

    // DATE FORMAT
    public static final String STANDARD_DATE_FORMAT = "dd-MM-yyyy";
    public static final String STANDARD_DATE_FORMAT_SLASH = "dd/MM/yyyy";
    public static final String DATE_FORMAT_YYYY_MM_01 = "yyyy-MM-01";
    public static final String DATE_FORMAT_MMMM = "MMMM";
    public static final String DATE_FORMAT_YYYY= "YYYY";
    public static final String DATE_FORMAT_MMYYYY = "MMyyyy";
    public static final String DATE_FORMAT_MM_YYYY = "MM-YYYY";
    public static final String DATE_FORMAT_YYYY_MM = "yyyy-MM";
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_MMMM_YYYY = "MMMM yyyy";
    public static final String DATE_FORMAT_yyyy_MM_dd_HH_mm = "yyyy-MM-dd-HH-mm";
    public static final String DATE_FORMAT_dd_MMMM_yyyy_SPACE = "dd MMMM yyyy";
    public static final String DATE_FORMAT_dd_MM_yyyy_SPACE = "dd MM yyyy";

    // PDF
    public static final BaseColor PDF_HEADER_BLUE_COLOR = new BaseColor(0x08,0x4E,0x93);
    public static final String UTF_8 = "UTF-8";
    public static final String ERROR_MSG_UPDATE_FRAIS = "Erreur lors de la mise à jour des Frais";
    public static final String SOUSTRAITANT_CODE = "SST";
    public static final String COLLABORATEUR_CODE = "CO";
    public static final String EURO_PER_WORKINGDAY_UNIT = " € / jour travaillé";
    public static final String EURO_PER_MONTH_UNIT = " € / mois";
    public static final String PROPERTY_ODM_PDF_FILENAME = "pdf.nomFichierODM";

    public static final BaseColor NEW_TEXT_HEADER = new BaseColor(41, 50, 134);
    public static final BaseColor NEW_HEADER_ODM = new BaseColor(208, 46, 137);
    public static final BaseColor NEW_BACKGROUND_HEADER = new BaseColor(234, 234, 234);
    public static final BaseColor NEW_TEXT_MISSION_HEADER = new BaseColor(208, 46, 137);

    // Month picker
    public static final String MONTH_YEAR_EXTRACT = "monthYearExtract";
    public static final String MONTH_YEAR_EXTRACT2 = "monthYearExtract2";
    public static final String MONTH_YEAR_EXTRACT3= "monthYearExtract3";
    public static final String MONTH_YEAR_EXTRACT4 = "monthYearExtract4";
    public static final String MONTH_YEAR_EXTRACT5 = "monthYearExtract5";
    public static final String MONTH_EXTRACT_REVENUES = "monthExtractRevenues";

    //Excel
    public static final String COLUMN_EXCEL_ID = "ID";
    public static final String COLUMN_EXCEL_FACTURE_NUMBER= "n° facture";
    public static final String COLUMN_EXCEL_CLIENT = "Client";
    public static final String COLUMN_EXCEL_COLLABORATOR = "Collaborateur";
    public static final String COLUMN_EXCEL_MISSION_MANAGER= "Manager de mission";
    public static final String COLUMN_EXCEL_WORKED_DAYS = "Nb Jour W";
    public static final String COLUMN_EXCEL_TJM = "TJM";
    public static final String COLUMN_EXCEL_FEES = "(frais, divers)";
    public static final String COLUMN_EXCEL_LAST_NAME = "Nom";
    public static final String COLUMN_EXCEL_FIRST_NAME = "Prénom";
    public static final String COLUMN_EXCEL_MISSION = "Mission";
    public static final String COLUMN_EXCEL_BENEFIT_MONTH = "Date prestation";
    public static final String COLUMN_EXCEL_BILLING_MONTH = "Mois facturé";
    public static final String COLUMN_EXCEL_TYPE = "Type";
    public static final String COLUMN_EXCEL_INVOICE_NUMBER = "Numéro de facture";
    public static final String COLUMN_EXCEL_MISSON_MANAGER = "Manager de la mission";
    public static final String COLUMN_EXCEL_CONTACT = "Contact";
    public static final String COLUMN_EXCEL_NUMBER = "Numéro";
    public static final String COLUMN_EXCEL_BUSINESS_ENGINEER = "Ingénieur d'affaires";
    public static final String COLUMN_EXCEL_BUDGET = "Budget";
    public static final String COLUMN_EXCEL_ORDER = "Commande";
    public static final String COLUMN_EXCEL_DAYS_NUMBER = "Nombre de jours";
    public static final String COLUMN_EXCEL_STARTING_DATE = "Date de début";
    public static final String COLUMN_EXCEL_ENDING_DATE = "Date de fin";
    public static final String COLUMN_EXCEL_BILLING_DATE = "Date facturation";

    public static final String COLUMN_EXCEL_PRE_TAX_AMOUNT = "Montant HT";
    public static final String COLUMN_EXCEL_PRE_TAX_AMOUNT_EUROS = "Montant HT €";
    public static final String COLUMN_EXCEL_POST_TAX_AMOUNT = "Montant TTC";
    public static final String COLUMN_EXCEL_OTHER_REVENUE = "Autre CA";
    public static final String COLUMN_EXCEL_TOTAL = "Total";
    public static final String COLUMN_EXCEL_TYPE_EXPENSE = "Type de frais";
    public static final String COLUMN_EXCEL_DATE_PERIOD = "Date / Periode";
    public static final String COLUMN_EXCEL_COMMENT = "Commentaire";
    public static final String COLUMN_EXCEL_DESIGNATION = "Désignation";
    public static final String COLUMN_EXCEL_QUANTITY = "Quantité";
    public static final String COLUMN_EXCEL_PU = "P.U.";

    public static final String TOTAL_MONTANT_HT = "Total Montant H.T";
    public static final String MONTANT_TVA = "Montant TVA";
    public static final String MONTANT_TTC = "Total Montant T.T.C";
    public static final String TOTAL_MONTANT_CA_HT = "Total chiffre d'affaire HT";

    public static final String HEADER_EXCEL_MONTHLY_REVENUES = "CHIFFRE D'AFFAIRES DU MOIS / ";
    public static final String HEADER_EXCEL_YEARLY_REVENUES = "CHIFFRE D'AFFAIRES DE L'ANNEE / ";
    public static final String HEADER_EXCEL_MONTHLY_INVOICES = "FACTURES DU MOIS / ";
    public static final String HEADER_EXCEL_SST_INVOICES = "FACTURES SOUS-TRAITANTS / ";
    public static final String HEADER_EXCEL_HANDWRITTEN_INVOICES = "FACTURES CREES A LA MAIN / ";
    public static final String HEADER_EXCEL_YEARLY_INVOICES = "FACTURES DE L'ANNEE / ";
    public static final String HEADER_EXCEL_YEARLY_ACTIVES_ORDER = "COMMANDES EN COURS DE L'ANNEE / ";
    public static final String HEADER_EXCEL_MONTHLY_ACTIVES_ORDER = "COMMANDES EN COURS DU MOIS / ";
    public static final String HEADER_EXCEL_YEARLY_STOPPED_ORDER = "COMMANDES TERMINEES DE L'ANNEE / ";
    public static final String HEADER_EXCEL_MONTHLY_STOPPED_ORDER = "COMMANDES TERMINEES DU MOIS / ";

    public static final String ANNUAL_REVENUES_EXCEL_TYPE = "chiffre_affaire_annuel";
    public static final String MONTHLY_REVENUES_EXCEL_TYPE = "chiffre_affaire_mensuel";

    //Excel Type values
    public static final String TYPE_VALUE_EXCEL_SOUS_TRAITANT = "STT";
    public static final String TYPE_VALUE_EXCEL_COLLABORATOR = "MIS";
    public static final String TYPE_VALUE_EXCEL_COLLABORATOR_EXPENSES = "FRS";

    // Urls
    public static final String APPLICATION_URL = "http://amilnote.amiltone.com/";
    public static final String REDIRECT = "redirect:/";
    public static final String NULL = "/null";

    // Check environment
    public static final String ENVIRONMENT = "environment";
    public static final String ENV_PROD = "env.prod";

    // MIME
    public static final String APPLICATION_JSON = "application/json";

    // Facture service parameters
    public static final String FRENCH_INTRA_COMMUNITY_ACCOUNT_NUMBER_START = "FR";
}
