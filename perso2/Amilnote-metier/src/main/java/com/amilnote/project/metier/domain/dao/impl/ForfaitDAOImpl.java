/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ForfaitDAO;
import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.Mission;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Forfait dao.
 */
@Repository("forfaitDAO")
public class ForfaitDAOImpl extends AbstractDAOImpl<Forfait> implements ForfaitDAO {

    @Autowired
    private static final String PROP_CODE_LIB = "LIB";

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Forfait> getReferenceClass() {
        return Forfait.class;
    }

    /**
     * {@linkplain ForfaitDAO#findForfaitsForMission(Mission)}
     */
    @Override
    public List<Forfait> findForfaitsForMission(Mission pMission) {
        //liste des éléments à renvoyer
        ProjectionList lProjectionList = Projections.projectionList();
        lProjectionList.add(Projections.property("id"), "id");
        lProjectionList.add(Projections.property("nom"), "nom");
        lProjectionList.add(Projections.property("montant"), "montant");

        //création de la requête
        Criteria lCritForfait = getCriteria();
        lCritForfait.createAlias("missions", "m");
        lCritForfait.add(Restrictions.eq("m.id", pMission.getId()));
        lCritForfait.setProjection(Projections.distinct(lProjectionList));

        //typage du résultat sous forme d'objets de la classe forfait
        lCritForfait.setResultTransformer(Transformers.aliasToBean(Forfait.class));
        return (List<Forfait>) lCritForfait.list();
    }

    /**
     *
     */
    @Override
    public List<Forfait> findForfaitLib() {
        Criteria crit = getCriteria();
        crit.add(Restrictions.ilike(Forfait.PROP_CODE, PROP_CODE_LIB, MatchMode.ANYWHERE));
        return crit.list();
    }

    @Override
    public List<Forfait> findForfaitNotLib() {
        Criteria crit = getCriteria();
        crit.add(Restrictions.not(Restrictions.ilike(Forfait.PROP_CODE, PROP_CODE_LIB, MatchMode.ANYWHERE)));
        return crit.list();
    }


}
