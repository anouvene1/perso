/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkFraisTFC;
import org.joda.time.DateTime;

/**
 * The type Link frais tfc as json.
 */
public class LinkFraisTFCAsJson {

    private Long id;
    private FraisAsJson frais;
    private LinkTypeFraisCaracAsJson typeFraisCarac;
    private Boolean val_tiny;
    private DateTime val_datetime;
    private String val_varchar;
    private float val_decimal;

    /**
     * Instantiates a new Link frais tfc as json.
     *
     * @param pFrais the p frais
     */
    public LinkFraisTFCAsJson(FraisAsJson pFrais) {
        super();
        frais = pFrais;
    }

    /**
     * Instantiates a new Link frais tfc as json.
     *
     * @param linkFraisTFC the link frais tfc
     */
    public LinkFraisTFCAsJson(LinkFraisTFC linkFraisTFC) {
        super();
        this.setId(linkFraisTFC.getId());
        this.setTypeFraisCarac(new LinkTypeFraisCaracAsJson(linkFraisTFC.getTypeFraisCarac()));
        if (linkFraisTFC.getVal_datetime() != null) {
            this.setVal_datetime(linkFraisTFC.getVal_datetime());
        }
        if (linkFraisTFC.getVal_varchar() != null) {
            this.setVal_varchar(linkFraisTFC.getVal_varchar());
        }
        if (linkFraisTFC.getVal_decimal() >= 0) {
            this.setVal_decimal(linkFraisTFC.getVal_decimal());
        }
        if (linkFraisTFC.getVal_tiny() != null) {
            this.setVal_tiny(linkFraisTFC.getVal_tiny());
        }
        this.setTypeFraisCarac(new LinkTypeFraisCaracAsJson(linkFraisTFC.getTypeFraisCarac()));
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets type frais carac.
     *
     * @return the type frais carac
     */
    public LinkTypeFraisCaracAsJson getTypeFraisCarac() {
        return typeFraisCarac;
    }

    /**
     * Sets type frais carac.
     *
     * @param pTypeFraisCarac the p type frais carac
     */
    public void setTypeFraisCarac(LinkTypeFraisCaracAsJson pTypeFraisCarac) {
        typeFraisCarac = pTypeFraisCarac;
    }

    /**
     * Gets frais.
     *
     * @return the frais
     */
    public FraisAsJson getFrais() {
        return frais;
    }

    /**
     * Sets frais.
     *
     * @param pFrais the p frais
     */
    public void setFrais(FraisAsJson pFrais) {
        frais = pFrais;
    }

    /**
     * Gets val tiny.
     *
     * @return the val tiny
     */
    public Boolean getVal_tiny() {
        return val_tiny;
    }

    /**
     * Sets val tiny.
     *
     * @param pVal_tiny the p val tiny
     */
    public void setVal_tiny(Boolean pVal_tiny) {
        val_tiny = pVal_tiny;
    }

    /**
     * Gets val datetime.
     *
     * @return the val datetime
     */
    public DateTime getVal_datetime() {
        return val_datetime;
    }

    /**
     * Sets val datetime.
     *
     * @param pVal_datetime the p val datetime
     */
    public void setVal_datetime(DateTime pVal_datetime) {
        val_datetime = pVal_datetime;
    }

    /**
     * Gets val varchar.
     *
     * @return the val varchar
     */
    public String getVal_varchar() {
        return val_varchar;
    }

    /**
     * Sets val varchar.
     *
     * @param pVal_varchar the p val varchar
     */
    public void setVal_varchar(String pVal_varchar) {
        val_varchar = pVal_varchar;
    }

    /**
     * Gets val decimal.
     *
     * @return the val decimal
     */
    public float getVal_decimal() {
        return val_decimal;
    }

    /**
     * Sets val decimal.
     *
     * @param pVal_decimal the p val decimal
     */
    public void setVal_decimal(float pVal_decimal) {
        val_decimal = pVal_decimal;
    }
}
