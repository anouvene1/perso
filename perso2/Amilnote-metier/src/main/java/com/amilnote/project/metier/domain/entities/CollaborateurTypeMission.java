/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.MissionAsJson;

import java.util.List;

/**
 * Created by sbenslimane on 15/11/2016.
 */
//SBE_15/11/2016_AMNOTE_148
public class CollaborateurTypeMission {

    private Collaborator collaborator;
    private List<MissionAsJson> listMission;

    /**
     * Instantiates a new Collaborateur type mission.
     *
     * @param collab      the collab
     * @param listMission the list mission
     */
    public CollaborateurTypeMission(Collaborator collab, List<MissionAsJson> listMission) {
        this.collaborator = collab;
        this.listMission = listMission;
    }

    /**
     * Instantiates a new Collaborateur type mission.
     */
    public CollaborateurTypeMission() {
    }

    /**
     * Gets collab.
     *
     * @return the collab
     */
    public Collaborator getCollaborator() {
        return collaborator;
    }

    /**
     * Sets collab.
     *
     * @param collaborator the collab
     */
    public void setCollaborator(Collaborator collaborator) {
        this.collaborator = collaborator;
    }

    /**
     * Gets list mission.
     *
     * @return the list mission
     */
    public List<MissionAsJson> getListMission() {
        return listMission;
    }

    /**
     * Sets list mission.
     *
     * @param listMission the list mission
     */
    public void setListMission(List<MissionAsJson> listMission) {
        this.listMission = listMission;
    }
}
