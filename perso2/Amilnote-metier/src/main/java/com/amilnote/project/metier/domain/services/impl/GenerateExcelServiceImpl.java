package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.GenerateExcelService;
import com.amilnote.project.metier.domain.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.util.*;

import static com.amilnote.project.metier.domain.utils.Constantes.FRENCH_MONTHS_NAMES;

/**
 * Cette classe va servir à générer l'excel voulu à partir de la liste renvoyée par la classe CalculFraisService
 **/
@Service("GenerateExcelService")
public class GenerateExcelServiceImpl implements GenerateExcelService {

    private static final int COLUMNS_NUMBER_ANNUAL_EXPENSES = 17;
    private static final int COLUMNS_NUMBER_MONTHLY_EXPENSES = 10;
    private static final String MONTHLY_EXPENSES_FILE_HEADER = "Excel récapitulatif des frais par collaborateur / ";
    private static final String YEARLY_EXPENSES_FILE_HEADER = "Excel récapitulatif des frais collaborateur sur l'année / ";
    private static final String MONTHLY_EXPENSES_DETAILED_FILE_HEADER = "Excel récapitulatif détaillé des frais par collaborateur / ";
    private static final String YEARLY_EXPENSES_DETAILED_FILE_HEADER = "Excel récapitulatif détaillé des frais collaborateur sur l'année / ";
    private static final String VALUE_YEAR = "annee";
    private static final String VALUE_MANAGER = "Manager";
    private static final String VALUE_NAME = "Nom";
    private static final String VALUE_FIRSTNAME = "Prénom";
    private static final String VALUE_TOTAL = "Total";
    private static final String VALUE_SIMPLE = "simple";
    private static final String VALUE_ID = "Id";

    private static Logger logger = LogManager.getLogger(GenerateExcelService.class);

    protected CollaboratorService collaboratorService;

    @Autowired
    public GenerateExcelServiceImpl(CollaboratorService collaboratorService) {
        this.collaboratorService = collaboratorService;
    }

    @Override
    public List<Object[]> finalizeTableListAnnee(List<List<Object[]>> list, int yearFrais) throws Exception {

        List<Object[]> header;
        List<Object[]> content;
        HashMap<Long, Object[]> contentMap;
        List<Object[]> finalList;
        List<String> headerTitle = new ArrayList<>();
        List<String> columns;
        int monthColumn = 4;
        float yearTotal = 0.0F;
        long collaboratorId;
        float collaboratorExpenses;


        try {
            header = new ArrayList<>();
            contentMap = new HashMap<>();
            finalList = new ArrayList<>();

            headerTitle.add(YEARLY_EXPENSES_FILE_HEADER + yearFrais);
            columns = Arrays.asList(VALUE_ID,
                    VALUE_NAME,
                    VALUE_FIRSTNAME,
                    VALUE_MANAGER,
                    FRENCH_MONTHS_NAMES[0],
                    FRENCH_MONTHS_NAMES[1],
                    FRENCH_MONTHS_NAMES[2],
                    FRENCH_MONTHS_NAMES[3],
                    FRENCH_MONTHS_NAMES[4],
                    FRENCH_MONTHS_NAMES[5],
                    FRENCH_MONTHS_NAMES[6],
                    FRENCH_MONTHS_NAMES[7],
                    FRENCH_MONTHS_NAMES[8],
                    FRENCH_MONTHS_NAMES[9],
                    FRENCH_MONTHS_NAMES[10],
                    FRENCH_MONTHS_NAMES[11],
                    VALUE_TOTAL);

            header.add(headerTitle.toArray());
            header.add(columns.toArray());

            List<Object> fileContent;
            List<Object> totalLine = new ArrayList<>();
            totalLine.add("Totaux");

            for (List<Object[]> monthList : list) {
                Float monthTotal = 0F;
                for (Object[] collaborator : monthList) {

                    collaboratorId = (long) collaborator[0];
                    collaboratorExpenses = (collaborator[5] == null) ? 0 : (float) collaborator[5];

                    if (contentMap.containsKey(collaboratorId)) {
                        monthTotal += collaboratorExpenses;
                        fileContent = Arrays.asList(contentMap.get(collaboratorId));
                        fileContent.set(monthColumn, collaboratorExpenses);
                        contentMap.put(collaboratorId, fileContent.toArray());
                    } else {
                        fileContent = Arrays.asList(initObject());
                        Collaborator collaboratorChecked = collaboratorService.get(collaboratorId);
                        if (!collaboratorService.isSubcontractor(collaboratorChecked)) {
                            monthTotal += collaboratorExpenses;
                            fileContent.set(1, collaboratorChecked.getNom());
                            fileContent.set(2, collaboratorChecked.getPrenom());
                            fileContent.set(3, (collaboratorChecked.getManager() == null) ? "" : collaboratorChecked.getManager().toString());
                            fileContent.set(monthColumn, collaboratorExpenses);
                            contentMap.put(collaboratorId, fileContent.toArray());
                        }
                    }
                }
                totalLine.add(monthTotal);
                yearTotal += monthTotal;
                monthColumn++;
            }

            totalLine.add(yearTotal);

            Set cles = contentMap.keySet();
            Iterator it = cles.iterator();
            while (it.hasNext()) {
                float somme = 0;
                float tmp = 0;
                Object cle = it.next(); // tu peux typer plus finement ici
                Object[] objects = contentMap.get(cle); // tu peux typer plus finement ici
                for (int i = 4; i < COLUMNS_NUMBER_ANNUAL_EXPENSES - 1; i++) {
                    if (objects[i] != null) {
                        tmp = (float) objects[i];
                        somme += tmp;
                    } else {
                        objects[i] = 0;
                    }
                }
                objects[COLUMNS_NUMBER_ANNUAL_EXPENSES - 1] = somme;
            }

            content = new ArrayList<>(contentMap.values());
            Comparator<Object[]> comparator = (x, y) -> (x[1].toString().compareToIgnoreCase(y[1].toString()));
            content.sort(comparator);
            int i = 1;
            for (Object[] ligne : content) {
                ligne[0] = i;
                i++;
            }
            finalList.addAll(header);
            finalList.addAll(content);
            finalList.add(totalLine.toArray());
            return finalList;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Object[]> finalizeTableList(List<Object[]> list, Date pDate) throws Exception {

        List<Object[]> header = new ArrayList<>();
        List<Object[]> content = new ArrayList<>();
        List<Object[]> finalList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(pDate);

        Float firstSummary = 0f;
        Float secondSummary = 0f;
        Float thirdSummary = 0f;
        Float fourthSummary = 0f;

        List headerTitle = new ArrayList<>();
        headerTitle.add(MONTHLY_EXPENSES_FILE_HEADER + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + "" + cal.get(Calendar.YEAR));

        List columns = Arrays.asList(VALUE_ID,
                VALUE_NAME,
                VALUE_FIRSTNAME,
                VALUE_MANAGER,
                "Avances de frais ponctuelles",
                "Avances de frais permanentes",
                "Total des frais",
                "Total des forfaits",
                VALUE_TOTAL,
                "Reste à payer");

        header.add(headerTitle.toArray());
        header.add(columns.toArray());

        for (Object[] currentCollaborator : list) {
            long idCollaborator = Long.parseLong(currentCollaborator[0].toString());
            Collaborator collaborator = collaboratorService.get(idCollaborator);

            List lineContent = new ArrayList<>();
            lineContent.add(null); // This value will be replaced by the line number of the Excel file
            lineContent.add(collaborator.getNom());
            lineContent.add(collaborator.getPrenom());
            lineContent.add((collaborator.getManager() == null) ? "" : collaborator.getManager().toString());
            lineContent.add(currentCollaborator[1]);
            lineContent.add(currentCollaborator[2]);
            lineContent.add(currentCollaborator[3]);
            firstSummary += (Float) currentCollaborator[3];
            lineContent.add(currentCollaborator[4]);
            secondSummary += (Float) currentCollaborator[4];
            lineContent.add(currentCollaborator[5]);
            thirdSummary += (Float) currentCollaborator[5];
            lineContent.add(currentCollaborator[6]);
            fourthSummary += (Float) currentCollaborator[6];

            content.add(lineContent.toArray());

        }

        Collections.sort(content, (l1, l2) -> l1[1].toString().compareTo(l2[1].toString()));

        int i = 1;
        for (Object[] line : content) {
            line[0] = i;
            i++;
        }

        List footer = Arrays.asList("Totaux : ", firstSummary, secondSummary, thirdSummary, fourthSummary);
        finalList.addAll(header);
        finalList.addAll(content);
        finalList.add(footer.toArray());

        return finalList;
    }

    @Override
    public List<Object[]> finalizeTableListExpensesDetail(List<Object[]> list, Date pDate, boolean byYear) throws Exception {

        List<Object[]> header = new ArrayList<>();
        List<Object[]> content = new ArrayList<>();
        List<Object[]> finalList = new ArrayList<>();
        List columns;
        List headerTitle = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(pDate);

        if (byYear) {
            headerTitle.add(YEARLY_EXPENSES_DETAILED_FILE_HEADER + cal.get(Calendar.YEAR));
        } else {
            headerTitle.add(MONTHLY_EXPENSES_DETAILED_FILE_HEADER + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + cal.get(Calendar.YEAR));
        }

        columns = Arrays.asList(VALUE_ID,
                VALUE_NAME,
                VALUE_FIRSTNAME,
                VALUE_MANAGER,
                "Voiture perso",
                "Train",
                "Transport",
                "Avion",
                "Location",
                "Essence",
                "Péage",
                "Parking",
                "Hôtel",
                "Restaurant midi",
                "Restaurant soir",
                "Taxi",
                "Téléphone",
                "Divers",
                "Abonnement transports en commun",
                "Avance sur frais ponctuelle",
                "Avance sur frais permanente",
                "Forfait repas",
                "Forfait déplacement",
                "Forfait téléphone",
                "Forfait logement",
                "Total frais",
                "Total forfait",
                VALUE_TOTAL,
                "RESTE À PAYER");


        header.add(headerTitle.toArray());
        header.add(columns.toArray());

        for (Object[] currentCollaborator : list) {
            Long idCollab = new Long(currentCollaborator[0].toString());
            Collaborator collaborator = collaboratorService.get(idCollab);

            List lineContent = new ArrayList<>();
            lineContent.add(null); // This value will be replaced by the line number
            lineContent.add(collaborator.getNom());
            lineContent.add(collaborator.getPrenom());
            lineContent.add((collaborator.getManager()) == null ? "" : collaborator.getManager().toString());

            for (int i = 1; i < currentCollaborator.length; i++) {
                lineContent.add(currentCollaborator[i]);
            }
            content.add(lineContent.toArray());
        }

        Collections.sort(content, (l1, l2) -> l1[1].toString().compareTo(l2[1].toString()));

        int i = 1;
        for (Object[] line : content) {
            line[0] = i;
            i++;
        }

        finalList.addAll(header);
        finalList.addAll(content);

        return finalList;
    }

    @Override
    public String generateExcel(String fichierExcel, Date pDateFrais, String typePeriodeFrais, List<Object[]> list) throws Exception {

        String chemin = fichierExcel;

        Workbook wb = new HSSFWorkbook();
        Sheet mySheet = DocumentServiceImpl.createSheetFrais(wb);
        Row myRow = mySheet.createRow(0);

        // Création de la première ligne avec le mois correspondant
        Calendar calendrier = Calendar.getInstance();
        calendrier.setTime(pDateFrais);


        try (FileOutputStream fileOutputStream = new FileOutputStream(chemin)) {
            fillTable(wb, list, typePeriodeFrais);

            setStyle(wb, list.get(1).length, typePeriodeFrais);
            wb.write(fileOutputStream);

        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            wb.close();
        }

        return chemin;
    }

    /**
     * Generates an array containing the value of the Excel file's columns
     *
     * @return an array of {@link Object} representing the values of the annual collaborators' expenses summary
     */
    private Object[] initObject() {
        Object[] o = new Object[COLUMNS_NUMBER_ANNUAL_EXPENSES];
        for (Object objet : o) {
            objet = 0;
        }
        return o;
    }

    /**
     * Allows to lay out the data according to Excel standards
     *
     * @param workbook the workbook parameter
     * @param list     the list of expenses
     * @param type     the type of the file
     */
    private void fillTable(Workbook workbook, List<Object[]> list, String type) {
        int rowNumber;
        int decalage = 0;
        Sheet sheet;
        try {
            switch (type) {
                case VALUE_SIMPLE:
                    decalage = 5;
                    break;
                case VALUE_YEAR:
                    decalage = 3;
                    break;
                default:
            }
            sheet = workbook.getSheetAt(0);
            rowNumber = 0;
            for (Object[] o : list) {
                Row row = sheet.createRow(rowNumber++);

                // si c'est la derniere ligne on ecrit des totaux, ils sont places differemment dans excel
                if (rowNumber == list.size() && (type.equals(VALUE_SIMPLE) || (type.equals(VALUE_YEAR)))) {
                    for (int i = 0; i < o.length; i++) {
                        if (o[i] != null) {
                            if (i > 0) {
                                Float total = Float.parseFloat(o[i].toString());
                                row.createCell(i + decalage).setCellValue(Math.round(total * 100.00) / 100.00);
                            } else {
                                row.createCell(i + decalage).setCellValue(o[i].toString());
                            }

                        } else {
                            row.createCell(i).setCellValue(0f);
                        }
                    }
                } else {
                    for (int i = 0; i < o.length; i++) {
                        if (o[i] != null) {
                            if (i == 0 && rowNumber > 2) {
                                int id = rowNumber - 2;
                                row.createCell(i).setCellValue(id);
                            } else if (i >= 4 && rowNumber > 2) {
                                Float montant = Float.parseFloat(o[i].toString());
                                row.createCell(i).setCellValue(Math.round(montant * 100.00) / 100.00);
                            } else {
                                row.createCell(i).setCellValue(o[i].toString());
                            }
                        } else {
                            row.createCell(i).setCellValue(0f);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * methode pour mettre en page le recap des frais
     *
     * @param wb      the workbook
     * @param largeur the number of columns
     * @param type    the type of the file
     */
    private void setStyle(Workbook wb, int largeur, String type) {
        Sheet mySheet = wb.getSheetAt(0);
        Row myRow;
        Row rowCell;
        mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) (largeur - 1)));
        CellStyle style;
        CellStyle styleTitre;
        CellStyle styleTotal;
        Font font;
        Font fontTitre;
        Font fontTotal;
        int deb = 0;
        int fin = 0;

        // Parametre largeur colonne
        mySheet.setColumnWidth(0, 5 * 256); // ID
        mySheet.setColumnWidth(1, 20 * 256); // Nom
        mySheet.setColumnWidth(2, 20 * 256); // Prenom
        mySheet.setColumnWidth(3, 20 * 256); // Manager

        switch (type) {
            case VALUE_YEAR:
                for (int i = 4; i < largeur; i++) mySheet.setColumnWidth(i, 14 * 256); // mois et total
                myRow = mySheet.getRow(1);
                myRow.setHeightInPoints((17)); // hauteur plus grand pour la celulle
                mySheet.createFreezePane(4, 2); // freeze (id,nom,prenom,manager) et (titre, nom colonne)
                deb = 3;
                fin = COLUMNS_NUMBER_ANNUAL_EXPENSES;
                break;
            case "detail":
                for (int i = 4; i < largeur; i++) mySheet.setColumnWidth(i, 14 * 256); // mois et total
                myRow = mySheet.getRow(1);
                myRow.setHeightInPoints((50)); // hauteur plus grand pour la celulle
                mySheet.createFreezePane(4, 2); // freeze (id,nom,prenom,manager) et (titre, nom colonne)
                break;

            default:
                mySheet.setColumnWidth(3, 20 * 256);
                mySheet.setColumnWidth(4, 20 * 256);
                for (int i = 5; i < largeur; i++) mySheet.setColumnWidth(i, 14 * 256); // mois et total
                myRow = mySheet.getRow(1);
                myRow.setHeightInPoints((28)); // hauteur plus grand pour la celulle
                mySheet.createFreezePane(0, 2); // freeze (titre, nom colonne)
                deb = 5;
                fin = COLUMNS_NUMBER_MONTHLY_EXPENSES;
                break;
        }


        // Parametre titre
        styleTitre = wb.createCellStyle();
        styleTitre.setAlignment(CellStyle.ALIGN_CENTER);
        styleTitre.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        fontTitre = wb.createFont();
        fontTitre.setFontHeight((short) (16 * 22));
        fontTitre.setBold(true);
        styleTitre.setFont(fontTitre);
        styleTitre.setWrapText(true);
        rowCell = mySheet.getRow(0);
        rowCell.setHeightInPoints((40)); // hauteur plus grand pour la celulle
        // Titre
        Utils.createCellule(rowCell, wb, 0, mySheet.getRow(0).getCell(0).toString(), null, styleTitre);

        // Parametre entete
        style = wb.createCellStyle();
        font = wb.createFont();
        font.setFontHeight((short) (11 * 20));
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setWrapText(true);
        // entete
        for (int j = 0; j < mySheet.getRow(1).getLastCellNum(); j++) {
            Utils.createCellule(myRow, wb, j, mySheet.getRow(1).getCell(j).toString(), null, style);
        }

        // fond gris une ligne sur deux
        CellStyle greyStyle = wb.createCellStyle();
        greyStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        greyStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        for (int nbRow = 2; nbRow <= mySheet.getLastRowNum() - 1; nbRow += 2) {
            for (int nbCell = 0; nbCell < mySheet.getRow(nbRow).getLastCellNum(); nbCell++) {
                mySheet.getRow(nbRow).getCell(nbCell).setCellStyle(greyStyle);
            }
        }

        // Parametre totaux
        styleTotal = wb.createCellStyle();
        fontTotal = wb.createFont();
        fontTotal.setFontHeight((short) (11 * 20));
        fontTotal.setBold(true);
        fontTotal.setItalic(true);
        styleTotal.setFont(fontTotal);
        styleTotal.setAlignment(CellStyle.ALIGN_CENTER);
        styleTotal.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        styleTotal.setWrapText(true);
        styleTotal.setBorderBottom((short) 2);
        styleTotal.setBorderLeft((short) 2);
        styleTotal.setBorderRight((short) 2);
        styleTotal.setBorderTop((short) 2);
        for (int nbCell = deb; nbCell < fin; nbCell++) {
            mySheet.getRow(mySheet.getLastRowNum()).getCell(nbCell).setCellStyle(styleTotal);
        }
    }

}
