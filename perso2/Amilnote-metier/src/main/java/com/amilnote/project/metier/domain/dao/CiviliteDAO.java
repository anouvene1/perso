package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Civilite;

import java.util.List;

public interface CiviliteDAO extends LongKeyDAO<Civilite> {

    List<Civilite> findAllOrderById();
}
