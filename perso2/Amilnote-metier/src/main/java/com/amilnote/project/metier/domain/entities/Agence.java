package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.AgenceAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Etat.
 *
 * @author SBenslim
 */
@Entity
@Table(name = "ami_ref_agence")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Agence implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_ADRESSE.
     */
    public static final String PROP_ADRESSE = "adresse";

    /**
     * The constant PROP_CODE_POSTAL.
     */
    public static final String PROP_CODE_POSTAL = "code_postal";

    /**
     * The constant PROP_VILLE.
     */
    public static final String PROP_VILLE = "VILLE";

    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";

    /**
     * The constant PROP_AGENCE_VILLEURBANNE.
     */
    public static final String PROP_AGENCE_VILLEURBANNE = "VIL";

    /**
     * The constant PROP_AGENCE_NANTES.
     */
    public static final String PROP_AGENCE_NANTES = "NAN";

    /**
     * The constant PROP_AGENCE_BORDEAUX.
     */
    public static final String PROP_AGENCE_BORDEAUX = "BOR";

    /**
     * The constant PROP_AGENCE_NIORT.
     */
    public static final String PROP_AGENCE_NIORT = "NIO";

    /**
     * The constant PROP_AGENCE_AIX_EN_PROVENCE.
     */
    public static final String PROP_AGENCE_AIX_EN_PROVENCE = "AIX";

    private static final long serialVersionUID = -1319603069615603284L;
    private Long id;
    private String adresse;
    private Long codePostal;
    private String ville;
    private String code;
    private boolean siege_social;

    /**
     * Constructeur par defaut de la classe Agence
     */
    public Agence() {
    }

    /**
     * Constructeur de la classe Agence
     *
     * @param id                the p id
     * @param adresse           the p adresse
     * @param codePostal        the p codePostal
     * @param ville             the p ville
     * @param code              the p code
     * @param siege_social      the p siege_social
     */
    public Agence(Long id, String adresse, Long codePostal, String ville, String code, boolean siege_social) {
        this.id = id;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.code = code;
        this.siege_social = siege_social;
    }

    /*
     * GETTERS AND SETTERS
     */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    @Column(name = "adresse", nullable = false)
    public String getAdresse() {
        return adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Gets codePostal.
     *
     * @return the codePostal
     */
    @Column(name = "code_postal", nullable = false)
    public Long getCodePostal() {
        return codePostal;
    }

    /**
     * Sets codePostal.
     *
     * @param codePostal the codePostal to set
     */
    public void setCodePostal(Long codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Gets ville.
     *
     * @return the ville
     */
    @Column(name = "ville", nullable = false)
    public String getVille() {
        return ville;
    }

    /**
     * Sets ville.
     *
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets siege_social.
     *
     * @return the siege_social
     */
    @Column(name = "siege_social", nullable = false)
    public boolean isSiege_social() {
        return siege_social;
    }

    /**
     * Sets siege_social.
     *
     * @param siege_social the siege_social to set
     */
    public void setSiege_social(boolean siege_social) {
        this.siege_social = siege_social;
    }

    public AgenceAsJson toJson() {return new AgenceAsJson(this);}
}
