/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Classe AddviseLinkSessionCollaborateurDAO pour manipuler les collaborateurs et invitations des sessions Addvise
 */
@Entity
@Table(name = "ami_addvise_link_session_collaborateur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AddviseLinkSessionCollaborateur implements Serializable {
    /**
     * POTENTIELLE : l'invitation est marquée pour être potentiellement modifiée plus tard
     * ENVOYEE : l'invitation a été envoyée au collaborateur pour confirmation qu'il viendra à la session
     * CONFIRMEE : l'invitation est marquée comme confirmée lorsque le collaborateur pense venir à la session
     * DECLINEE : l'invitation est marquée comme déclinée lorsque le collaborateur ne viendra pas à la session
     */
    public enum EtatInvitation {
        POTENTIELLE, ENVOYEE, CONFIRMEE, DECLINEE
    }

    private int id;
    private Collaborator collaborateur;
    private AddviseSession session;
    private EtatInvitation etatInvitation;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_collaborateur")
    public Collaborator getCollaborateur() {
        return collaborateur;
    }

    public void setCollaborateur(Collaborator collaborateur) {
        this.collaborateur = collaborateur;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_addvise_session")
    public AddviseSession getSession() {
        return session;
    }

    public void setSession(AddviseSession session) {
        this.session = session;
    }

    @Column(name = "etat_invitation")
    public EtatInvitation getEtatInvitation() {
        return etatInvitation;
    }



    public void setEtatInvitation(EtatInvitation etatInvitation) {
        this.etatInvitation = etatInvitation;
    }

    }
