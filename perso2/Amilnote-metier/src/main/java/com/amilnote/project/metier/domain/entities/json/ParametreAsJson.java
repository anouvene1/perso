/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;


import com.amilnote.project.metier.domain.entities.Parametre;

/**
 * The type Parametre as json.
 */
public class ParametreAsJson {

    private int id;
    private String libelle;
    private String valeur;

    /**
     * Instantiates a new Parametre as json.
     *
     * @param parametre the parametre
     */
    public ParametreAsJson(Parametre parametre) {
        this.setId(parametre.getId());
        this.setLibelle(parametre.getLibelle());
        this.setValeur(parametre.getValeur());
    }

    /**
     * Instantiates a new Parametre as json.
     */
    public ParametreAsJson() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle.
     *
     * @param lib the lib
     */
    public void setLibelle(String lib) {
        this.libelle = lib;
    }

    /**
     * Gets valeur.
     *
     * @return the valeur
     */
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets valeur.
     *
     * @param val the val
     */
    public void setValeur(String val) {
        this.valeur = val;
    }
}
