/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Element facture.
 */
@Entity
@Table(name = "ami_element_facture")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id_element_facture")
public class ElementFacture implements java.io.Serializable {

    /**
     * The constant PROP_ID_ELEMENT_FACTURE.
     */
    public static final String PROP_ID_ELEMENT_FACTURE = "idElementFacture";
    /**
     * The constant PROP_FACTURE_ELEMENT.
     */
    public static final String PROP_FACTURE_ELEMENT = "facture";
    /**
     * The constant PROP_LIBELLE_ELEMENT.
     */
    public static final String PROP_LIBELLE_ELEMENT = "libelle";
    /**
     * The constant PROP_QUANTITE_ELEMENT.
     */
    public static final String PROP_QUANTITE_ELEMENT = "quantite";
    /**
     * The constant PROP_PRIX_ELEMENT.
     */
    public static final String PROP_PRIX_ELEMENT = "prix";
    /**
     * The constant PROP_MONTANT_ELEMENT.
     */
    public static final String PROP_MONTANT_ELEMENT = "montant";
    private static final long serialVersionUID = -329072377657745936L;
    private int idElementFacture;
    private Facture facture;
    private String libelle;
    private Float quantite;
    private Float prix;
    private Float montant;


	/* CONSTRUCTORS */

    /**
     * Instantiates a new Element facture.
     */
    public ElementFacture() {
    }


    /**
     * Instantiates a new Element facture.
     *
     * @param pFacture  the p facture
     * @param pLibelle  the p libelle
     * @param pQuantite the p quantite
     * @param pPrix     the p prix
     * @param pMontant  the p montant
     */
    public ElementFacture(Facture pFacture, String pLibelle, Float pQuantite, Float pPrix, Float pMontant) {
        facture = pFacture;
        libelle = pLibelle;
        quantite = pQuantite;
        prix = pPrix;
        montant = pMontant;
    }

    /**
     * Instantiates a new Element facture.
     *
     * @param elementFactureAsJson the element facture as json
     */
    public ElementFacture(ElementFactureAsJson elementFactureAsJson) {
        facture = elementFactureAsJson.getFacture();
        libelle = elementFactureAsJson.getLibelleElement();
        quantite = elementFactureAsJson.getQuantiteElement();
        prix = elementFactureAsJson.getPrixElement();
        montant = elementFactureAsJson.getMontantElement();
    }

    /**
     * To json element facture as json.
     *
     * @return the element facture as json
     */
    public ElementFactureAsJson toJson() {
        return new ElementFactureAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id element facture.
     *
     * @return the id element facture
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_element_facture", unique = true, nullable = false)
    public int getIdElementFacture() {
        return idElementFacture;
    }

    /**
     * Sets id element facture.
     *
     * @param idElementFacture the id element facture
     */
    public void setIdElementFacture(int idElementFacture) {
        this.idElementFacture = idElementFacture;
    }

    /**
     * Gets facture.
     *
     * @return the facture
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_facture", nullable = false)
    public Facture getFacture() {
        return facture;
    }

    /**
     * Sets facture.
     *
     * @param facture the facture
     */
    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    /**
     * Gets libelle element.
     *
     * @return the libelle element
     */
    @Column(name = "libelle_element", nullable = true)
    public String getLibelleElement() {
        return libelle;
    }

    /**
     * Sets libelle element.
     *
     * @param libelle the libelle
     */
    public void setLibelleElement(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets quantite element.
     *
     * @return the quantite element
     */
    @Column(name = "quantite_element", nullable = true)
    public Float getQuantiteElement() {
        return quantite;
    }

    /**
     * Sets quantite element.
     *
     * @param quantite the quantite
     */
    public void setQuantiteElement(Float quantite) {
        this.quantite = quantite;
    }

    /**
     * Gets prix element.
     *
     * @return the prix element
     */
    @Column(name = "prix_element", nullable = true)
    public Float getPrixElement() {
        return prix;
    }

    /**
     * Sets prix element.
     *
     * @param prix the prix
     */
    public void setPrixElement(Float prix) {
        this.prix = prix;
    }

    /**
     * Gets montant element.
     *
     * @return the montant element
     */
    @Column(name = "montant_element", nullable = true)
    public Float getMontantElement() {
        return montant;
    }

    /**
     * Sets montant element.
     *
     * @param montant the montant
     */
    public void setMontantElement(Float montant) {
        this.montant = montant;
    }


}
