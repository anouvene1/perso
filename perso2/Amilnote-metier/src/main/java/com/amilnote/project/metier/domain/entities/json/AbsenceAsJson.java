/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Absence;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Absence as json.
 */
public class AbsenceAsJson {

    private Long id = null;

    private Date dateDebut;

    private Date dateFin;

    private float nbJours;

    private TypeAbsenceAsJson typeAbsence;

    private EtatAsJson etat;

    @JsonIgnore
    private CollaboratorAsJson collaborateur;

    private List<LinkEvenementTimesheetAsJson> listLinkEvenementTimesheetAsJson = new ArrayList<>();

    private String commentaire;

    /**
     * Instantiates a new Absence as json.
     *
     * @param pAbsence the p absence
     */
    public AbsenceAsJson(Absence pAbsence) {
        setId(pAbsence.getId());
        setDateDebut(pAbsence.getDateDebut());
        setDateFin(pAbsence.getDateFin());
        setNbJours(pAbsence.getNbJours());
        setTypeAbsence(new TypeAbsenceAsJson(pAbsence.getTypeAbsence()));
        setEtat(new EtatAsJson(pAbsence.getEtat()));
        setCollaborateur(pAbsence.getCollaborateur().toJson());
        setCommentaire(pAbsence.getCommentaire());
    }

    /**
     * Instantiates a new Absence as json.
     */
    public AbsenceAsJson() {
    }


    /**
     * Gets type absence.
     *
     * @return the type absence
     */
    public TypeAbsenceAsJson getTypeAbsence() {
        return typeAbsence;
    }

    /**
     * Sets type absence.
     *
     * @param pTypeAbsence the p type absence
     */
    public void setTypeAbsence(TypeAbsenceAsJson pTypeAbsence) {
        typeAbsence = pTypeAbsence;
    }


    /**
     * Gets date fin.
     *
     * @return the date fin
     */
    public Date getDateFin() {
        return dateFin;
    }


    /**
     * Sets date fin.
     *
     * @param pDateFin the p date fin
     */
    public void setDateFin(Date pDateFin) {
        dateFin = pDateFin;
    }


    /**
     * Gets date debut.
     *
     * @return the date debut
     */
    public Date getDateDebut() {
        return dateDebut;
    }


    /**
     * Sets date debut.
     *
     * @param pDateDebut the p date debut
     */
    public void setDateDebut(Date pDateDebut) {
        dateDebut = pDateDebut;
    }

    /**
     * Gets nb jours.
     *
     * @return the nb jours
     */
    public float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param pNbJours the p nb jours
     */
    public void setNbJours(float pNbJours) {
        nbJours = pNbJours;
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }


    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }


    /**
     * Gets etat.
     *
     * @return the etat
     */
    public EtatAsJson getEtat() {
        return etat;
    }


    /**
     * Sets etat.
     *
     * @param pEtat the etat to set
     */
    public void setEtat(EtatAsJson pEtat) {
        etat = pEtat;
    }


    /**
     * Gets collaborateur.
     *
     * @return the mailCollaborateur
     */
    public CollaboratorAsJson getCollaborateur() {
        return collaborateur;
    }


    /**
     * Sets collaborateur.
     *
     * @param pCollaborateur the p collaborateur
     */
    public void setCollaborateur(CollaboratorAsJson pCollaborateur) {
        collaborateur = pCollaborateur;
    }

    /**
     * Gets list link evenement timesheet as json.
     *
     * @return the listLinkEvenementTimesheetAsJson
     */
    public List<LinkEvenementTimesheetAsJson> getListLinkEvenementTimesheetAsJson() {
        return listLinkEvenementTimesheetAsJson;
    }

    /**
     * Sets list link evenement timesheet as json.
     *
     * @param pListLinkEvenementTimesheetAsJson the listLinkEvenementTimesheetAsJson to set
     */
    public void setListLinkEvenementTimesheetAsJson(List<LinkEvenementTimesheetAsJson> pListLinkEvenementTimesheetAsJson) {
        listLinkEvenementTimesheetAsJson = pListLinkEvenementTimesheetAsJson;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the commentaire to set
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }


}
