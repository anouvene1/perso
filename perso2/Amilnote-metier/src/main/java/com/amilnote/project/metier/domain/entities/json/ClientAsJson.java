/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Client as json.
 */
public class ClientAsJson {

    private Long id;
    private String nom_societe;
    private String adresse;
    private String adresse_facturation;
    private String tva;
    private String ribClient;
    private String auxiliaire;
    private String libelleComptable;
    private List<ContactClient> contacts = new ArrayList<ContactClient>();

    /**
     * Instantiates a new Client as json.
     */
    public ClientAsJson() {}

    /**
     * Instantiates a new Client as json.
     *
     * @param client the client
     */
    public ClientAsJson(Client client) {
        this.setId(client.getId());
        this.setAdresse(client.getAdresse());
        this.setNom_societe(client.getNom_societe());
        this.setAdresse_facturation(client.getAdresseFacturation());
        this.setContacts(client.getContacts());
        this.setTva(client.getTva());
        this.setRibClient(client.getRibClient());
        this.setAuxiliaire(client.getAuxiliaire());
        this.setLibelleComptable(client.getLibelleComptable());
    }
    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets nom societe.
     *
     * @return the nom_societe
     */
    public String getNom_societe() {
        return nom_societe;
    }

    /**
     * Sets nom societe.
     *
     * @param nom_societe the nom_societe to set
     */
    public void setNom_societe(String nom_societe) {
        this.nom_societe = nom_societe;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * get the Adresse_facturation
     * @return string
     */
    public String getAdresse_facturation() {
        return adresse_facturation;
    }

    /**
     * Set the Adresse_facturation
     * @param adresse_facturation facture adress
     */
    public void setAdresse_facturation(String adresse_facturation) {
        this.adresse_facturation = adresse_facturation;
    }
    /**
     * Gets contacts.
     *
     * @return the contacts
     */
    public List<ContactClient> getContacts() {
        return contacts;
    }

    /**
     * Sets contacts.
     *
     * @param contacts the contacts to set
     */
    public void setContacts(List<ContactClient> contacts) {
        this.contacts = contacts;
    }

    /**
     * Gets tva.
     *
     * @return the tva
     */
    public String getTva() {
        return tva;
    }

    /**
     * Sets tva.
     *
     * @param tva the tva to set
     */
    public void setTva(String tva) {
        this.tva = tva;
    }

    /**
     * Gets ribClient.
     *
     * @return the ribClient
     */
    public String getRibClient() {
        return ribClient;
    }

    /**
     * Sets tva.
     *
     * @param ribClient the rib to set
     */
    public void setRibClient(String ribClient) {
        this.ribClient = ribClient;
    }

    /**
     * get auxiliaire
     * @return string
     */
    public String getAuxiliaire() {
        return auxiliaire;
    }


    /**
     * set auxiliarie
     * @param auxiliaire auxiliaire
     */
    public void setAuxiliaire(String auxiliaire) {
        this.auxiliaire = auxiliaire;
    }

    /**
     * get libelle comptable
     * @return string
     */
    public String getLibelleComptable() {
        return libelleComptable;
    }

    /**
     * set libelle comptable
     * @param libelleComptable libelleComptable
     */
    public void setLibelleComptable(String libelleComptable) {
        this.libelleComptable = libelleComptable;
    }
}
