package com.amilnote.project.metier.domain.utils.enumerations;

public enum FileFormatEnum {

    IMAGE_JPG("image/jpeg"),
    EXT_JPG(".jpg"),
    EXT_JPEG(".jpeg"),
    IMAGE_PNG("image/png"),
    EXT_PNG(".png"),
    IMAGE_BMP("image/bmp"),
    EXT_BMP(".bmp"),
    EXT_DIB(".dib"),
    PDF("application/pdf"),
    EXT_PDF(".pdf"),
    DOC("application/msword"),
    EXT_DOC(".doc"),
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    EXT_DOCX(".docx"),
    XLS("application/xls"),
    EXT_XLS(".xls"),
    XSLX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    EXT_XLSX(".xlsx"),
    ZIP("application/zip"),
    EXT_ZIP(".zip"),
    TIKA_XLS("application/vnd.ms-excel"),
    TIKA_BMP("image/x-ms-bmp"),
    TIKA_MS_OFFICE("application/x-tika-msoffice"),
    TIKA_ZIP("application/x-tika-ooxml");

    private final String formatAction;

    private static final String ERROR_MESSAGE = "Error : invalid file format";

    FileFormatEnum(String action){
        formatAction = action;
    }

    public static FileFormatEnum getFileFormat(String action){
        for (FileFormatEnum formatType : FileFormatEnum.values())
        {
            if (action.equals(formatType.formatAction)) return formatType;
        }
        throw new IllegalArgumentException(ERROR_MESSAGE);
    }

    public String toString() {
        return this.formatAction;
    }
}
