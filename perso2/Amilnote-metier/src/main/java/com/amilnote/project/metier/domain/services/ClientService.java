/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;

import java.util.List;

/**
 * The interface Client service.
 */
public interface ClientService extends AbstractService<Client> {

    /**
     * retrouver tous les clients et les trier par ordre ascendant
     *
     * @return List de clients
     */
    List<Client> findAllOrderByNomAsc();

    /**
     * retrouver tous les clients et les trier par ordre ascendant au format Json
     *
     * @return List de clients
     */
    List<ClientAsJson> findAllOrderByNomAscAsJson();

    /**
     * Supprimer un Client.
     *
     * @param pClient Client à supprimer
     * @return Message de retour
     */
    String deleteClient(Client pClient);

    /**
     * Met à jour ou Créer un Client
     *
     * @param pClient Client à créer ou à mettre à jour
     * @return Message de retour
     * @throws Exception the exception
     */
    String updateOrCreateClient(ClientAsJson pClient) throws Exception;

    /**
     * Find by nom client.
     *
     * @param pNom the p nom
     * @return the client
     */
    Client findByNom(String pNom);

    /**
     * Méthode qui renvoit l'id du responsable facturation du client, et 0 s'il n'en a pas.
     * @param client {@link Client}
     * @return id ou 0
     */
    int getResponsableFacturation(Client client);

    /**
     * Méthode qui renvoit l'id du responsable client du client, et 0 s'il n'en a pas.
     * @param client {@link Client}
     * @return id ou 0
     */
    int getResponsableClient(Client client);

    /**
     * Méthode qui renvoit l'id du responsable facturation du client, et 0 s'il n'en a pas.
     * @param idClient client id
     * @return id ou 0
     */
    int getResponsableFacturation(Long idClient);

    /**
     * Méthode qui renvoit l'id du responsable client du client, et 0 s'il n'en a pas.
     * @param idClient client id
     * @return id ou 0
     */
    int getResponsableClient(Long idClient);

    /**
     * check if the clients intracommunautary number is from France.
     * @param customer Client object
     * @return true if is from France else false
     */
    boolean  isClientIntracNumberFR(Client customer);
}
