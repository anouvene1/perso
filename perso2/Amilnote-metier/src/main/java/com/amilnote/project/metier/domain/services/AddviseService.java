/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.AddviseFormateur;
import com.amilnote.project.metier.domain.entities.AddviseLinkSessionCollaborateur;
import com.amilnote.project.metier.domain.entities.AddviseSession;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import com.amilnote.project.metier.domain.utils.Pair;
import com.itextpdf.text.DocumentException;
import org.apache.commons.mail.EmailException;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.IOException;
import java.text.ParseException;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

public interface AddviseService extends AbstractService<AddviseSession> {
    List<Collaborator> findAddviseCollaborators();

    List<ModuleAddvise> findAddviseAllModules();

    List<AddviseFormateur> findAddviseAllFormateurs();

    String[][] getStatsTable(List<Collaborator> collaborateursAddvise, String dateDebut, String dateFin) throws ParseException;

    List<Pair<String, Long>> getStatsHist(List<AddviseSession> sessionsPassees);

    List<AddviseSession> findAddviseSessionsModifiables();

    long addNewModule(String codeModule, String nomModule) throws InvalidPropertiesFormatException;

    long addNewFormateur(String nomFormateur, String prenomFormateur, String emailFormateur) throws InvalidPropertiesFormatException;

    void addNewSession(long idModuleStr, String dateStr, int idFormateurStr) throws ParseException;

    List<Long[]> findInvitationsEtatsByCollabSession(String idsCollabSession);

    void saveAllInvitations(String allInvitationsStr, String url, Collaborator currentUser);

    AddviseSession validationSession(int idSession, String allInvitationsStr, String url, Collaborator currentUser);

    AddviseSession editSession(int idSession, long idModule, String date,int idFormateur, String allInvitationsStr, String url, Collaborator currentUser) throws ParseException;

    void removeSession(int idSession, String allInvitationsStr, String url, Collaborator currentUser);

    List<Collaborator> sortAndFilterCollaborators(List<Collaborator> collaborateursAddvise, String trier, String cacher, String chercher, String filtrer);

    List<AddviseSession> findSessionsPassees(boolean annulees, String dateDebut, String dateFin) throws ParseException;

    void restoreSession(int idSession);

    boolean confirmerOuDeclinerInvitationParMail(String url, String action, AddviseLinkSessionCollaborateur invitation, Collaborator currentUser) throws MessagingException, IOException, EmailException, NamingException;

    byte[] printSessionToSign(int idSession) throws DocumentException, NamingException, IOException;

    ModuleAddvise getPreviousModule(long idModule);

    AddviseLinkSessionCollaborateur manageInvitationByToken(String token, String url, String action) throws MessagingException, NamingException, EmailException, IOException;
}
