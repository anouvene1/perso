/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Etat;

import java.util.List;

/**
 * The interface Etat dao.
 */
public interface EtatDAO extends LongKeyDAO<Etat> {

    /**
     * Retourne tous les Etats
     *
     * @return List all etat
     */
    List<Etat> getAllEtat();


    /**
     * Retourne un etat en fonction de l'id
     *
     * @param pId the p id
     * @return Etat etat by id
     */
    Etat getEtatById(int pId);
}
