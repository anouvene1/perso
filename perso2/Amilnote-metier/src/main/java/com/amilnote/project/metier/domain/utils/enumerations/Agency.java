package com.amilnote.project.metier.domain.utils.enumerations;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public enum Agency {

    VILLEURBANNE(0, "Villeurbanne"),
    NIORT(1, "Niort"),
    AIX(2, "Aix");

    private int id;
    private String agency;

    Agency() {
    }

    Agency(int id, String ville) { this.id = id; this.agency = ville;}

    public static Agency getFromId(int agency) {
        return Agency.values()[agency];
    }

    public void setId(int id){
        this.id =  id;
    }

    @JsonValue
    public int getId(){
        return id;
    }

    public void setAgency(String agency){
        this.agency = agency;
    }

    public String getAgency(){
        return agency;
    }
}