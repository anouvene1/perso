/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.dao.StatutCollaborateurDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.Utils;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;

import static com.amilnote.project.metier.domain.utils.Constantes.*;

/**
 * The type Pdf builder ra.
 */
@Service("PDFBuilderRA")
public class PDFBuilderRA extends PDFBuilder {

    private static final Logger logger = LogManager.getLogger(PDFBuilderRA.class);
    private static String repertoireRA;
    private static Float mntTotalForfait;
    private static Float mntTotalFrais;
    private static Float mntTotalAvanceFrais;

    private static Float mntTotalAvanceFraisSansPermanent;
    private static Float totalTVADed;
    private static Float totalAvanceTVADed;

    /**
     * RA labels
     */
    private static final String TOTAL_DAYS_MISSION = "TOTAL JOUR(S) PAR MISSION";
    private static final String TOTAL_DAYS_ABSENCE = "TOTAL JOUR(S) PAR ABSENCE";
    private static final String TOTAL_FRAIS = "TOTAL FRAIS";
    private static final String ABSENCE_TYPE_LABEL = "Type Absence";
    private static final String NB_JOURS_LABEL = "Nombre de jour(s)";
    private static final String JOURS_LABEL = "jour(s)";
    private static final String SAMEDIS = "samedi(s)";
    private static final String JOURS_FERIES = "jour(s) férié(s)";
    private static final String DETAIL_JOURS = "DETAIL JOUR(S)";
    private static final String JOUR_MISSION = "JOUR MISSION";

    /**
     * Pdf style setting
     */
    private static final float SPACING = 10f;
    private static final float SPACING_LARGE = 20f;

    @Autowired
    private FraisService fraisService;

    @Autowired
    private TvaService tvaService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private CollaboratorDAO collaboratorDAO;

    @Autowired
    private StatutCollaborateurDAO statutCollaborateurDAO;


    /**
     * création du pdf de rapport d'activites
     *
     * @param collaborator the p collaborateur
     * @param pDateRAVoulue  the p date ra voulue
     * @param typePDF        the type pdf
     * @return the string
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     */
    public String createPDF(Collaborator collaborator, DateTime pDateRAVoulue, String typePDF) throws IOException, SchedulerException, EmailException, MessagingException, DocumentException {

        logger.debug("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");

        Date today = new Date();

        String nomFichier = "";
        DateTime lDebutPeriode = pDateRAVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = pDateRAVoulue.dayOfMonth().withMaximumValue();

        DateTimeFormatter fmtHead = DateTimeFormat.forPattern("MM-YYYY");

        // différenciation du nom suivant le type de PDF
        if (Constantes.NOM_PDF_SOCIAL.equals(typePDF)) {
            nomFichier = "RA_" + collaborator.getId() + "-" + collaborator.getNom() + "_" + DateTimeFormat.forPattern("MMyyyy").print(pDateRAVoulue) + "_SOCIAL.pdf";
        } else if (Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            nomFichier = "RA_" + collaborator.getId() + "-" + collaborator.getNom() + "_" + DateTimeFormat.forPattern("MMyyyy").print(pDateRAVoulue) + ".pdf";
        } else if (Constantes.NOM_PDF_ADMIN.equals(typePDF)) {
            nomFichier = "RA_" + collaborator.getId() + "-" + collaborator.getNom() + "_" + DateTimeFormat.forPattern("MMyyyy").print(pDateRAVoulue) + "_ADMIN.pdf";
        }
        try {
            repertoireRA = Parametrage.getContext("dossierPDFRA");
        } catch (NamingException e) {
            logger.error("[create pdf] context parameter naming error", e);
        }

        String chemin = repertoireRA + nomFichier; //chemin du fichier
        File directory = new File(chemin).getParentFile(); //on récupère les dossiers parents
        directory.mkdirs(); // on crée les dossiers parents s'il en manque
        FileOutputStream lBaos = new FileOutputStream(chemin); // création du fichier

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;

        lWriter = newWriter(lDocument, lBaos);
        TableHeader lEvent = new TableHeader();
        if (Constantes.NOM_PDF_SOCIAL.equals(typePDF)) {
            lEvent.setHeader("Rapport d'activités " + fmtHead.print(lDebutPeriode) + " \n Version Social");
        } else if (Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            lEvent.setHeader("Rapport d'activités " + fmtHead.print(lDebutPeriode) + " \n Version Avec Frais");
        } else if (Constantes.NOM_PDF_ADMIN.equals(typePDF)) {
            lEvent.setHeader("Rapport d'activités " + fmtHead.print(lDebutPeriode) + " \n Version DRH");
        }
        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);

        // ------------------------------------------------------//
        // CONSTITUTION DU CONTENU DU PDF RAPPORT ACTIVITES //
        // -----------------------------------------------------//
        // ouverture du document PDF
        lDocument.open();

        // table date courante
        lDocument.add(createTableDateCreation("DATE DE CREATION DU RAPPORT D'ACTIVITES", today));

        // table collaborateur & responsable
        lDocument.add(createTableUser(collaborator));

        List<Mission> listMission = missionService.findMissionCollabPourRA(collaborator, lDebutPeriode, lFinPeriode);
        List<LinkEvenementTimesheet> listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDates(collaborator, lDebutPeriode.toString(), lFinPeriode.toString());
        LinkedHashMap<Date, List<LinkEvenementTimesheet>> mapDateEvents = new LinkedHashMap<Date, List<LinkEvenementTimesheet>>();

        // --- Tri des événements par date et moment de la journée
        Collections.sort(listLinkEvent, new Comparator<LinkEvenementTimesheet>() {
            @Override
            public int compare(LinkEvenementTimesheet event1, LinkEvenementTimesheet event2) {
                if (event1.getDate().before(event2.getDate())) {
                    return -1;
                } else if (event1.getDate().after(event2.getDate())) {
                    return 1;
                } else {
                    if (event1.getMomentJournee() < event2.getMomentJournee()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        });

        Utils.mapEventsByDate(listLinkEvent, mapDateEvents);

        mntTotalForfait = (float) 0;
        mntTotalFrais = (float) 0;
        mntTotalAvanceFrais = (float) 0;
        mntTotalAvanceFraisSansPermanent = (float) 0;
        totalTVADed = (float) 0;
        totalAvanceTVADed = (float) 0;
        Paragraph lParagraph = new Paragraph();
        // Affichage des blocs dans le cas du PDF pour le social ou le
        // collaborateur
        if (Constantes.NOM_PDF_SOCIAL.equals(typePDF) || Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            // Création du tableau du nombre de jours par mission
            lParagraph.add(createMissionsDaysTable(listMission, pDateRAVoulue));
            // Création du tableau récapitulatif des absences/jours
            lParagraph.add(createTableNbrJoursAbsences(listLinkEvent, pDateRAVoulue));
            // Création du tableau des détails missions/jours
            lParagraph.add(createTableDetailJoursMissions(mapDateEvents));
        }

        if (!Constantes.NOM_PDF_SOCIAL.equals(typePDF)) {
            // Ajout d'une seconde page au document
            lParagraph.add(Chunk.NEXTPAGE);
        }

        // Affichage des blocs dans le cas du PDF pour le collaborateur ou
        // l'administrateur
        if (Constantes.NOM_PDF_ADMIN.equals(typePDF) || Constantes.NOM_PDF_COLLAB.equals(typePDF) && !collaborator.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            // table collaborateur & responsable
            lParagraph.add(createTableUser(collaborator));
            // Création du tableau de la liste des forfaits
            lParagraph.add(createTableForfait(listMission, listLinkEvent, mapDateEvents, pDateRAVoulue));
        }

        if (!Constantes.NOM_PDF_SOCIAL.equals(typePDF)) {
            // Création du tableau des frais
            lParagraph.add(createTableFrais(listLinkEvent, typePDF));
            // Création du tableau Avance de Frais
            lParagraph.add(createAvanceFrais(listLinkEvent, typePDF, listMission, pDateRAVoulue.toDate()));
            // Création du tableau de récap Total
            lParagraph.add(createTotal(listLinkEvent, typePDF));
        }

        // Ajout des tableaux au PDF
        lDocument.add(lParagraph);
        // fermeture du document pdf
        lDocument.close();
        logger.debug("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");

        return nomFichier;
    }

    /**
     * Create and save pdf string.
     *
     * @param collaborator the p collaborateur
     * @param pDateRAVoulue  the p date ra voulue
     * @param typePDF        the type pdf
     * @return the string
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     * @throws EmailException     the email exception
     * @throws SchedulerException the scheduler exception
     * @throws IOException        the io exception
     */
    public String createAndSavePDF(Collaborator collaborator, DateTime pDateRAVoulue, String typePDF)
            throws MessagingException, DocumentException, EmailException, SchedulerException, IOException {
        String nomFichier = createPDF(collaborator, pDateRAVoulue, typePDF);
        return nomFichier;
    }

    /***
     * Create and set a {@link PdfPTable} about a collaborator's missions's days (while specifying worked saturdays and holidays)
     * @param missions missions of a collaborator which pdfTable's creation will be based on
     * @param dateOfSearch the date of the search
     * @return a {@link PdfPTable} about a collaborator's missions's days
     */
    private PdfPTable createMissionsDaysTable(List<Mission> missions, DateTime dateOfSearch) {

        final double workingTimeUnit = 0.5;
        final String EOF_WHOSE = "\ndont ";

        PdfPTable missionsDaysTable = new PdfPTable(2);
        missionsDaysTable.setWidthPercentage(100);
        missionsDaysTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        missionsDaysTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        missionsDaysTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        missionsDaysTable.getDefaultCell().setColspan(2);
        missionsDaysTable.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        missionsDaysTable.addCell(new Phrase(TOTAL_DAYS_MISSION, fontHeaderWhite));
        missionsDaysTable.getDefaultCell().setColspan(1);
        missionsDaysTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        missionsDaysTable.addCell(new Phrase(MISSION_LABEL, FONT_BOLD));
        missionsDaysTable.addCell(new Phrase(NB_JOURS_LABEL, FONT_BOLD));

        Calendar missionsCalendar = Calendar.getInstance();
        missionsCalendar.setTime(dateOfSearch.toDate());

        // --- Teste de la présence de mission pour le mois
        if (!missions.isEmpty()) {
            double daysAllMissions = 0;
            double saturdaysAllMissions = 0;
            double holidaysAllMissions = 0;

            // --- Déroulement pour chaque mission
            for (Mission missionTemp : missions) {
                double daysMission = 0;
                double saturdaysMission = 0;
                double holidaysMission = 0;

                // --- Récupération des évenements de la mission et ajout d'une demi - journée si celui-ci est dans le mois courant.
                for (LinkEvenementTimesheet eventTemp : missionTemp.getLinkEvenementTimesheetLine()) {

                    Calendar eventCalandar = Calendar.getInstance();
                    eventCalandar.setTime(eventTemp.getDate());

                    // L'evenement est le même mois que le RA ET il n'y a pas d'absence OU présence d'absence différente de valide ou différente de soumise
                    if (eventCalandar.get(Calendar.YEAR) == missionsCalendar.get(Calendar.YEAR) &&
                            eventCalandar.get(Calendar.MONTH) == missionsCalendar.get(Calendar.MONTH) &&
                            !isAbsenceVAOrSO(eventTemp.getAbsence())) {

                        daysMission += workingTimeUnit;
                        //si le jour travaillé est un samedi
                        if (eventCalandar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                            saturdaysMission += workingTimeUnit;

                        //si le jour travaillé est un jour férié
                        for (Date date : linkEvenementTimesheetService.getHolidays(dateOfSearch.getYear())) {
                            if (date.compareTo(eventCalandar.getTime()) == 0)
                                holidaysMission += workingTimeUnit;
                        }
                    }
                }
                daysAllMissions += daysMission;
                saturdaysAllMissions += saturdaysMission;
                holidaysAllMissions += holidaysMission;

                if (daysMission > 0) { // ne pas afficher les missions actives avec 0 jours déclarés dans le mois
                    missionsDaysTable.addCell(new Phrase(missionTemp.getMission()));
                    String daysMissionPhrase = new StringBuilder().append(daysMission).append(BLANK_SPACE)
                            .append(JOURS_LABEL).append(BLANK_SPACE).toString();
                    if (saturdaysMission > 0 || holidaysMission > 0) {
                        if (saturdaysMission > 0)
                            daysMissionPhrase = daysMissionPhrase.concat(EOF_WHOSE).concat(String.valueOf(saturdaysMission))
                                    .concat(BLANK_SPACE).concat(SAMEDIS);
                        if (holidaysMission > 0)
                            daysMissionPhrase = daysMissionPhrase.concat(EOF_WHOSE).concat(String.valueOf(holidaysMission))
                                    .concat(BLANK_SPACE).concat(JOURS_FERIES);
                    }
                    missionsDaysTable.addCell(new Phrase(daysMissionPhrase));
                }

            }

            missionsDaysTable.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));

            String daysAllMissionsPhrase = new StringBuilder().append(daysAllMissions).append(BLANK_SPACE)
                    .append(JOURS_LABEL).append(BLANK_SPACE).toString();
            if (saturdaysAllMissions > 0 || holidaysAllMissions > 0) {
                if (saturdaysAllMissions > 0)
                    daysAllMissionsPhrase = daysAllMissionsPhrase.concat(EOF_WHOSE).concat(String.valueOf(saturdaysAllMissions))
                            .concat(BLANK_SPACE).concat(SAMEDIS);
                if (holidaysAllMissions > 0)
                    daysAllMissionsPhrase = daysAllMissionsPhrase.concat(EOF_WHOSE).concat(String.valueOf(holidaysAllMissions))
                            .concat(BLANK_SPACE).concat(JOURS_FERIES);
            }
            missionsDaysTable.addCell(new Phrase(daysAllMissionsPhrase, FONT_BOLD));

        }

        // --- Aucune mission présente pour le mois, affichage de tirets
        else {
            missionsDaysTable.addCell(new Phrase("-"));
            missionsDaysTable.addCell(new Phrase("-"));
        }

        missionsDaysTable.setSpacingAfter(SPACING);
        return missionsDaysTable;
    }

    /***
     * Création de la table du nombre de jours par type d'absence
     *
     * @param pListLinkEvent liste des évenements d'un collaborateur
     * @return Tableau de la liste des absences avec les jours associés
     */
    private PdfPTable createTableNbrJoursAbsences(List<LinkEvenementTimesheet> pListLinkEvent, DateTime pDateRAVoulue) {

        // --- ENTETE DU TABLEAU
        PdfPTable lTableAbsences = new PdfPTable(2);
        lTableAbsences.setWidthPercentage(100);
        lTableAbsences.setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableAbsences.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableAbsences.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableAbsences.getDefaultCell().setColspan(2);
        lTableAbsences.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableAbsences.addCell(new Phrase(TOTAL_DAYS_ABSENCE, fontHeaderWhite));
        lTableAbsences.getDefaultCell().setColspan(1);
        lTableAbsences.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableAbsences.addCell(new Phrase(ABSENCE_TYPE_LABEL, FONT_BOLD));
        lTableAbsences.addCell(new Phrase(NB_JOURS_LABEL, FONT_BOLD));

        Calendar calRA = Calendar.getInstance();
        calRA.setTime(pDateRAVoulue.toDate());

        double nbAbsenceAutorisee = 0;
        double nbAbsenceExceptionnelle = 0;
        double nbCongesPayes = 0;
        double nbCongesSansSolde = 0;
        double nbFinDeContrat = 0;
        double nbRTT = 0;
        double nbCongePaternite = 0;
        double nbMaladie = 0;
        double nbMaternite = 0;
        Absence tmpAbsence = null;
        // --- Teste la présence d'évements
        if (0 < pListLinkEvent.size()) {
            // --- Déroulement pour chaque mission
            for (LinkEvenementTimesheet tmpEvent : pListLinkEvent) {
                tmpAbsence = null;
                // --- Evenement présent dans le mois en cours
                Calendar calEvent = Calendar.getInstance();
                calEvent.setTime(tmpEvent.getDate());

                if (calEvent.get(Calendar.YEAR) == calRA.get(Calendar.YEAR) &&
                        calEvent.get(Calendar.MONTH) == calRA.get(Calendar.MONTH)) {
                    tmpAbsence = tmpEvent.getAbsence();
                    // --- Test s'il s'agit bien d'un absence et que celle-ci
                    // est à
                    // --- l'état validé
                    if (isAbsenceVAOrSO(tmpAbsence)) {

                        switch (tmpAbsence.getTypeAbsence().getCode()) {
                            case "AA": // --- Absence Autorisée
                                nbAbsenceAutorisee = nbAbsenceAutorisee + 0.5;
                                break;
                            case "AE": // --- Absence Exceptionnelle
                                nbAbsenceExceptionnelle = nbAbsenceExceptionnelle + 0.5;
                                break;
                            case "CP": // --- Congés Payés
                                nbCongesPayes = nbCongesPayes + 0.5;
                                break;
                            case "CS": // --- Congés Sans Solde
                                nbCongesSansSolde = nbCongesSansSolde + 0.5;
                                break;

                            //SBE_AMNOTE_171 21/11/2016 : rajout du cas pour les absences de type fin de contrat
                            case "FC": // --- Fin de contrat
                                nbFinDeContrat = nbFinDeContrat + 0.5;
                                break;
                            case "RTT": // --- RTT
                                nbRTT = nbRTT + 0.5;
                                break;
                            case "PAT": // --- Congé Paternité
                                nbCongePaternite = nbCongePaternite + 0.5;
                                break;
                            case "MA": // --- Maladie
                                nbMaladie = nbMaladie + 0.5;
                                break;
                            case "MT": // --- Maternité
                                nbMaternite = nbMaternite + 0.5;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        double nbAbsenceTotal = 0;

        String daysStr = BLANK_SPACE + JOURS_LABEL;

        if (nbAbsenceAutorisee != 0) {
            lTableAbsences.addCell(new Phrase("Absence Autorisée"));
            lTableAbsences.addCell(new Phrase(nbAbsenceAutorisee + daysStr));
            nbAbsenceTotal += nbAbsenceAutorisee;
        }

        if (nbAbsenceExceptionnelle != 0) {
            lTableAbsences.addCell(new Phrase("Absence Exceptionnelle"));
            lTableAbsences.addCell(new Phrase(nbAbsenceExceptionnelle + daysStr));
            nbAbsenceTotal += nbAbsenceExceptionnelle;
        }

        if (nbCongesPayes != 0) {
            lTableAbsences.addCell(new Phrase("Congés Payés"));
            lTableAbsences.addCell(new Phrase(nbCongesPayes + daysStr));
            nbAbsenceTotal += nbCongesPayes;
        }

        if (nbCongesSansSolde != 0) {
            lTableAbsences.addCell(new Phrase("Congés sans Solde"));
            lTableAbsences.addCell(new Phrase(nbCongesSansSolde + daysStr));
            nbAbsenceTotal += nbCongesSansSolde;
        }
        if (nbRTT != 0) {
            lTableAbsences.addCell(new Phrase("RTT"));
            lTableAbsences.addCell(new Phrase(nbRTT + daysStr));
            nbAbsenceTotal += nbRTT;
        }

        if (nbCongePaternite != 0) {
            lTableAbsences.addCell(new Phrase("Congé Paternité"));
            lTableAbsences.addCell(new Phrase(nbCongePaternite + daysStr));
            nbAbsenceTotal += nbCongePaternite;
        }

        if (nbMaladie != 0) {
            lTableAbsences.addCell(new Phrase("Maladie"));
            lTableAbsences.addCell(new Phrase(nbMaladie + daysStr));
            nbAbsenceTotal += nbMaladie;
        }

        if (nbMaternite != 0) {
            lTableAbsences.addCell(new Phrase("Maternité"));
            lTableAbsences.addCell(new Phrase(nbMaternite + daysStr));
            nbAbsenceTotal += nbMaternite;
        }

        lTableAbsences.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));
        lTableAbsences.addCell(new Phrase(nbAbsenceTotal + daysStr, FONT_BOLD));
        lTableAbsences.setSpacingAfter(SPACING);

        return lTableAbsences;
    }

    /**
     * Créer la table des forfaits pour le PDF
     *
     * @param pListMissions  Liste des missions
     * @param pListEvents    Liste des événements
     * @param pMapDateEvents Map Dates - Evénements
     * @return Table des forfaits
     * @throws DocumentException
     */
    private PdfPTable createTableForfait(List<Mission> pListMissions, List<LinkEvenementTimesheet> pListEvents, LinkedHashMap<Date, List<LinkEvenementTimesheet>> pMapDateEvents,
                                         DateTime pDateRAVoulue) throws DocumentException {
        // Paragraphe avec 4 colonnes
        PdfPTable lTableForfaits = new PdfPTable(4);
        boolean testLigne = false;

        Forfait tmpForfait;
        String tmpCode;
        float total = 0;

        // Parcours de la liste de mission
        for (Mission tmpMission : pListMissions) {
            // Pour chaque mission : récuperation de la liste des forfaits
            List<LinkMissionForfait> tmpListForfaits = tmpMission.getListMissionForfaits();

            if (tmpListForfaits.size() > 0) {
                // Pour chaque forfait
                for (LinkMissionForfait tmpLinkMissionForfait : tmpListForfaits) {
                    if (!testLigne) {
                        // Dans le cas du tout premier passage, on créé le bloc
                        // Forfait
                        lTableForfaits.setWidthPercentage(100);
                        lTableForfaits.setHorizontalAlignment(Element.ALIGN_CENTER);
                        lTableForfaits.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                        lTableForfaits.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                        lTableForfaits.getDefaultCell().setColspan(4);
                        lTableForfaits.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                        lTableForfaits.addCell(new Phrase("LISTE DES FORFAITS", fontHeaderWhite));
                        lTableForfaits.getDefaultCell().setColspan(1);
                        lTableForfaits.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                        lTableForfaits.setWidths(new int[]{46, 17, 20, 17});
                        lTableForfaits.addCell(new Phrase("Nom", FONT_BOLD));
                        lTableForfaits.addCell(new Phrase("Fréquence", FONT_BOLD));
                        lTableForfaits.addCell(new Phrase("Nombre de jours liés", FONT_BOLD));
                        lTableForfaits.addCell(new Phrase(COLUMN_EXCEL_POST_TAX_AMOUNT, FONT_BOLD));

                        lTableForfaits.setSpacingAfter(SPACING);
                        testLigne = true;
                    }
                    tmpForfait = tmpLinkMissionForfait.getForfait();
                    tmpCode = tmpForfait.getCode();

                    if (tmpCode.equals(Forfait.CODE_REPAS)) {
                        // --- Traitement des Forfaits repas
                        total += setForfaitRepasRow(lTableForfaits, tmpMission, tmpForfait, tmpLinkMissionForfait, pDateRAVoulue);
                    } else if (tmpCode.equals(Forfait.CODE_DEPLACEMENT)) {
                        // --- Traitement des forfaits déplacement
                        total += setForfaitDeplacementRow(lTableForfaits, pMapDateEvents, tmpLinkMissionForfait, pDateRAVoulue);
                    } else if (tmpCode.equals(Forfait.CODE_LOGEMENT)) {
                        // --- Traitement des forfaits logement
                        total += setForfaitLogementRow(lTableForfaits, tmpMission, tmpForfait, tmpLinkMissionForfait, pDateRAVoulue);
                    } else if (tmpCode.equals(Forfait.CODE_TELEPHONE)) {
                        // --- Traitement des forfaits transport
                        // On ne passe qu'une fois dans ce forfait
                        total += setForfaitMensuelRow(lTableForfaits, tmpLinkMissionForfait);
                    }
                }
            }

        }

        if (testLigne) {
            // --- Affichage du total de tous les forfaits
            lTableForfaits.getDefaultCell().setColspan(3);
            lTableForfaits.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));
            lTableForfaits.addCell(new Phrase(String.format("%.2f", total) + " €"));
            mntTotalForfait = total;
        }
        return lTableForfaits;
    }

    private float setForfaitLogementRow(PdfPTable pTableForfaits, Mission pMission, Forfait pForfait, LinkMissionForfait pLinkMissionForfait, DateTime pDateRAVoulue) {

        Absence tmpAbsence;
        int nbJoursLogement = 0;

        Calendar calRA = Calendar.getInstance();
        calRA.setTime(pDateRAVoulue.toDate());

        // --- Comptage du nombre de jours pour le forfait repas
        for (LinkEvenementTimesheet tmpEvent : pMission.getLinkEvenementTimesheetLine()) {

            Calendar calEvent = Calendar.getInstance();
            calEvent.setTime(tmpEvent.getDate());

            if (calEvent.get(Calendar.YEAR) == calRA.get(Calendar.YEAR) &&
                    calEvent.get(Calendar.MONTH) == calRA.get(Calendar.MONTH)) {

                tmpAbsence = tmpEvent.getAbsence();

                if (!isAbsenceVAOrSO(tmpAbsence)) {

                    if (tmpEvent.getMomentJournee() == 0) {

                        nbJoursLogement++;
                    }
                }
            }
        }

        // Ajout de la ligne du forfait logement
        pTableForfaits.getDefaultCell().setColspan(1);
        PdfPCell lCell = new PdfPCell();
        lCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        lCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        lCell.addElement(new Phrase(pMission.getMission() + " - " + pForfait.getNom() + " (" + pLinkMissionForfait.getMontant() + "€)"));
        pTableForfaits.addCell(lCell);
        pTableForfaits.addCell(pForfait.getFrequence().getFrequence());
        pTableForfaits.addCell(nbJoursLogement + " jours");

        // --- Calcul du montant total du forfait
        float totalForfait = pLinkMissionForfait.getMontant();
        totalForfait *= nbJoursLogement;

        // --- Ajoute ligne montant total du forfait
        pTableForfaits.addCell(String.format("%.2f €", totalForfait));

        return totalForfait;

    }

    /**
     * Ajouter une ligne d'informations dans le PDF pour un forfait repas
     *
     * @param pTableForfaits      Tableau PDF des forfaits
     * @param pMission            Mission concernée par le forfait
     * @param pForfait            Forfait repas à traiter
     * @param pLinkMissionForfait Lien Mission - Forfait
     * @return Montant Total du forfait
     * @throws DocumentException
     */
    private float setForfaitRepasRow(PdfPTable pTableForfaits, Mission pMission, Forfait pForfait, LinkMissionForfait pLinkMissionForfait, DateTime pDateRAVoulue) throws DocumentException {

        Absence tmpAbsence;
        int nbJoursRepas = 0;

        Calendar calRA = Calendar.getInstance();
        calRA.setTime(pDateRAVoulue.toDate());

        // --- Comptage du nombre de jours pour le forfait repas
        for (LinkEvenementTimesheet tmpEvent : pMission.getLinkEvenementTimesheetLine()) {

            Calendar calEvent = Calendar.getInstance();
            calEvent.setTime(tmpEvent.getDate());

            if (calEvent.get(Calendar.YEAR) == calRA.get(Calendar.YEAR) &&
                    calEvent.get(Calendar.MONTH) == calRA.get(Calendar.MONTH)) {

                tmpAbsence = tmpEvent.getAbsence();

                if (!isAbsenceVAOrSO(tmpAbsence)) {

                    if (tmpEvent.getMomentJournee() == 0) {

                        nbJoursRepas++;
                    }
                }
            }
        }

        // On retire les forfaits repas où il y a eu un frais REM (repas du
        // midi)
        DateTime dateDeb = pDateRAVoulue;
        DateTime dateFin = pDateRAVoulue.dayOfMonth().withMaximumValue().toDateTime();
        nbJoursRepas = nbJoursRepas - fraisService.findNbRepasFraisMidi(pMission, dateDeb, dateFin);

        // Ajout de la ligne du forfait repas
        pTableForfaits.getDefaultCell().setColspan(1);
        PdfPCell lCell = new PdfPCell();
        lCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        lCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        lCell.addElement(new Phrase(pMission.getMission() + " - " + pForfait.getNom() + " (" + pLinkMissionForfait.getMontant() + "€)"));
        pTableForfaits.addCell(lCell);
        pTableForfaits.addCell(pForfait.getFrequence().getFrequence());
        pTableForfaits.addCell(nbJoursRepas + " jours");

        // --- Calcul du montant total du forfait
        float totalForfait = pLinkMissionForfait.getMontant();
        totalForfait *= nbJoursRepas;

        // --- Ajoute ligne montant total du forfait
        pTableForfaits.addCell(String.format("%.2f €", totalForfait));

        return totalForfait;

    }

    /**
     * Ajouter une ligne d'informations dans le PDF pour un forfait déplacement
     *
     * @param pTableForfaits      Tableau PDF des forfaits
     * @param pMapDateEvents      Ensenble des événements concernés
     * @param pLinkMissionForfait Lien Mission - Forfait
     * @param pDateRAVoulue
     * @return Montant Total du forfait
     * @throws DocumentException
     */
    private double setForfaitDeplacementRow(PdfPTable pTableForfaits, LinkedHashMap<Date, List<LinkEvenementTimesheet>> pMapDateEvents, LinkMissionForfait pLinkMissionForfait, DateTime pDateRAVoulue)
            throws DocumentException {

        // On calcul le nombre de frais prenant le pas sur les forfaits
        // déplacements.
        // A la fin du décompte, on retirera le nombre de frais remplacant le
        // forfait aux nombres de jours total calculé.

        int nbJoursFraisForfait = _calculNbJoursFraisForfait(pLinkMissionForfait, pDateRAVoulue);
        int nbJours = Utils.calculNbJours(pMapDateEvents, pLinkMissionForfait);

        Forfait tmpForfait = pLinkMissionForfait.getForfait();
        // Ajout de la ligne du forfait repas
        pTableForfaits.getDefaultCell().setColspan(1);

        PdfPCell lCell = new PdfPCell();
        lCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        lCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        lCell.addElement(new Phrase(pLinkMissionForfait.getMission().getMission() + " - " + tmpForfait.getNom()));
        pTableForfaits.addCell(lCell);

        pTableForfaits.addCell(tmpForfait.getFrequence().getFrequence());
        nbJours = nbJours - nbJoursFraisForfait;
        pTableForfaits.addCell(nbJours + " jours");

        float totalDecimal = pLinkMissionForfait.getMontant();
        totalDecimal *= nbJours;

        pTableForfaits.addCell(String.format("%.2f €", totalDecimal));

        return totalDecimal;

    }

    private int _calculNbJoursFraisForfait(LinkMissionForfait pLinkMissionForfait, DateTime pDateRAVoulue) {
        int nbFraisRemplaceForfait = 0;
        Mission mission = pLinkMissionForfait.getMission();

        List<LinkEvenementTimesheet> listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDatesByDateTime(mission.getCollaborateur(), pDateRAVoulue.dayOfMonth().withMinimumValue(), pDateRAVoulue.dayOfMonth().withMaximumValue());
        List<Long> identifiants = new ArrayList<>();
        for (LinkEvenementTimesheet tmpEvent : listLinkEvent) {

            for (Frais tmpFrais : tmpEvent.getListFrais()) {
                if (!identifiants.contains(tmpEvent.getId()) && "FRF".equalsIgnoreCase(tmpFrais.getFraisForfait())) {
                    identifiants.add(tmpEvent.getId());
                    nbFraisRemplaceForfait++;
                }
            }
        }
        return nbFraisRemplaceForfait;
    }

    /**
     * Ajouter une ligne d'informations dans le PDF pour un forfait mensuel
     *
     * @param pTableForfaits         Tableau PDF des forfaits
     * @param pTmpLinkMissionForfait Lien Mission - Forfait
     * @return Montant du forfait
     * @throws DocumentException
     */
    private float setForfaitMensuelRow(PdfPTable pTableForfaits, LinkMissionForfait pTmpLinkMissionForfait) throws DocumentException {

        Forfait tmpForfait = pTmpLinkMissionForfait.getForfait();
        // Ajout de la ligne du forfait repas
        pTableForfaits.getDefaultCell().setColspan(1);

        PdfPCell lCell = new PdfPCell();
        lCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        lCell.addElement(new Phrase(pTmpLinkMissionForfait.getMission().getMission() + " - " + tmpForfait.getNom()));
        pTableForfaits.addCell(lCell);

        pTableForfaits.addCell(tmpForfait.getFrequence().getFrequence());
        pTableForfaits.addCell(" - ");
        pTableForfaits.addCell(String.format("%.2f €", pTmpLinkMissionForfait.getMontant()));

        return pTmpLinkMissionForfait.getMontant();

    }

    /**
     * Ajouter une ligne d'informations dans le PDF pour un forfait libre -- PAS ENCORE UTILISEE mais le sera pas la suite
     *
     * @param pTableForfaits         Tableau PDF des forfaits
     * @param pTmpLinkMissionForfait Lien Mission - Forfait
     * @return Montant du forfait
     * @throws DocumentException
     */
    private float setForfaitLibreRow(PdfPTable pTableForfaits, LinkMissionForfait pTmpLinkMissionForfait, String pNomForfait) throws DocumentException {

        Forfait tmpForfait = pTmpLinkMissionForfait.getForfait();
        pTableForfaits.getDefaultCell().setColspan(1);

        PdfPCell lCell = new PdfPCell();
        lCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        lCell.addElement(new Phrase(pTmpLinkMissionForfait.getMission().getMission() + " - " + pNomForfait));
        pTableForfaits.addCell(lCell);

        pTableForfaits.addCell(tmpForfait.getFrequence().getFrequence());
        pTableForfaits.addCell(" - ");
        pTableForfaits.addCell(String.format("%.2f €", pTmpLinkMissionForfait.getMontant()));

        return pTmpLinkMissionForfait.getMontant();

    }

    /**
     * Créer la table des frais pour le PDF
     *
     * @param pListEvents Liste des événements
     * @return Table des frais
     * @throws DocumentException
     */
    private PdfPTable createTableFrais(List<LinkEvenementTimesheet> pListEvents, String typePDF) throws DocumentException {
        Integer nbCell = 0;
        if (Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            nbCell = 4;
        } else {
            nbCell = 5;
        }
        PdfPTable lTableFrais = new PdfPTable(nbCell);

        boolean testligne = false;
        for (LinkEvenementTimesheet tmpEvent : pListEvents) {

            // --- Evénement autre qu'une absence Validée ou Soumise
            if (!isAbsenceVAOrSO(tmpEvent.getAbsence())) {

                for (Frais tmpFrais : tmpEvent.getListFrais()) {
                    // On ne récupère pas ici les avances sur frais
                    if (!"ASF".equals(tmpFrais.getTypeFrais().getCode())) {
                        if (!testligne) {
                            // Dans le cas du tout premier passage, on créé le
                            // bloc DETAIL FRAIS
                            lTableFrais.setWidthPercentage(100);
                            lTableFrais.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                            lTableFrais.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                            lTableFrais.getDefaultCell().setColspan(5);
                            lTableFrais.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                            lTableFrais.addCell(new Phrase("DETAIL FRAIS", fontHeaderWhite));
                            lTableFrais.getDefaultCell().setColspan(1);
                            lTableFrais.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                            if (Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
                                lTableFrais.setWidths(new int[]{22, 18, 47, 13});
                            } else {
                                lTableFrais.setWidths(new int[]{22, 18, 35, 13, 12});
                            }
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_TYPE_EXPENSE, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_DATE_PERIOD, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_COMMENT, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_POST_TAX_AMOUNT, FONT_BOLD));
                            if (!Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
                                lTableFrais.addCell(new Phrase("TVA déductible", FONT_BOLD));
                            }
                            testligne = true;
                        }
                        setFraisRow(lTableFrais, tmpFrais, typePDF);
                    }
                }
            }
        }

        if (testligne) {
            // Ligne du total des frais
            lTableFrais.getDefaultCell().setColspan(3);
            lTableFrais.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));
            lTableFrais.getDefaultCell().setColspan(1);
            java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
            lTableFrais.addCell(new Phrase(df.format(mntTotalFrais) + " €", FONT_BOLD));
            lTableFrais.addCell(new Phrase(df.format(totalTVADed).concat(" €"), FONT_BOLD));

            lTableFrais.setSpacingAfter(SPACING_LARGE);
        }

        return lTableFrais;
    }

    /**
     * Ajouter une ligne d'informations de frais pour le PDF
     *
     * @param pTableFrais Table des frais pour le PDF
     * @param pFrais      Frais à traiter
     */
    private void setFraisRow(PdfPTable pTableFrais, Frais pFrais, String typePDF) {

        // Ajout de la colonne TypeFrais
        // Récupération de la colonne "fraisForfait" pour concaténation à la
        // fin.
        String fraisForfait = "";
        if (pFrais.getFraisForfait() != null) {
            if (Frais.PROP_FRAISREMPLACEFORFAIT.equals(pFrais.getFraisForfait())) {
                fraisForfait = "\n (- le forfait)";
            } else if (Frais.PROP_FRAISADDITIONFORFAIT.equals(pFrais.getFraisForfait())) {
                fraisForfait = "\n (+ le forfait)";
            }
        }

        if (pFrais.getTypeFrais().getTypeFrais().length() >= 20) {
            pTableFrais.addCell(new Phrase(pFrais.getTypeFrais().getTypeFrais().substring(0, 20).concat(fraisForfait)));
        } else {
            pTableFrais.addCell(new Phrase(pFrais.getTypeFrais().getTypeFrais().concat(fraisForfait)));
        }

        // Ajout de la colonne Date
        String textForDateCell;
        LinkEvenementTimesheet tmpEvent;
        tmpEvent = pFrais.getLinkEvenementTimesheet();
        textForDateCell = DATE_FORMATER.format(tmpEvent.getDate()); // + " - " +

        pTableFrais.addCell(new Phrase(textForDateCell));

        // Ajout de la colonne Commentaire
        String commentaire = "";

        commentaire = (pFrais.getCommentaire() != null ? pFrais.getCommentaire() : "-");
        if (pFrais.getMontant() == null) {
            commentaire += " (" + pFrais.getNombreKm() + " km)";
        } else if (pFrais.getNbNuit() != null) {
            commentaire += " (" + pFrais.getNbNuit() + " nuits)";
        }

        pTableFrais.addCell(new Phrase(commentaire));

        // Ajout de la colonne Montant et TVA déductible
        float tva = pFrais.getTypeFrais().getTva().getMontant();
        java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
        // On différencie les frais classiques
        // du frais de voiture personnelle exprimé par le collaborateur en
        // Kilomètre
        if (pFrais.getMontant() == null) {
            TVA tauxVoiturePerso = tvaService.getTVAByCode("KM");
            Integer kmVoiture = pFrais.getNombreKm();
            Float mntVoiture = kmVoiture * tauxVoiturePerso.getMontant();
            mntTotalFrais += mntVoiture;
            pTableFrais.addCell(new Phrase(String.valueOf(df.format(mntVoiture)).concat(" €")));
            if (Constantes.NOM_PDF_ADMIN.equals(typePDF)) {
                // Calcul de la TVA déductible
                float tvaDed = mntVoiture * (1 - (1 / (1 + (tva / 100))));
                totalTVADed += tvaDed;
                pTableFrais.addCell(new Phrase(String.valueOf(df.format(tvaDed)).concat(" €")));
            }
        } else {
            pTableFrais.addCell(new Phrase(String.valueOf(pFrais.getMontant()).concat(" €")));
            if (Constantes.NOM_PDF_ADMIN.equals(typePDF)) {
                float tvaDed = (pFrais.getMontant().floatValue()) * (1 - (1 / (1 + (tva / 100))));
                totalTVADed += tvaDed;
                pTableFrais.addCell(new Phrase(String.valueOf(df.format(tvaDed)).concat(" €")));
            }
            mntTotalFrais += pFrais.getMontant().floatValue();
        }
    }

    /**
     * Créer la partie sur les avance de frais
     *
     * @param pListEvents Liste des évènements
     * @return Table des avances de frais
     * @throws DocumentException
     */
    private PdfPTable createAvanceFrais(List<LinkEvenementTimesheet> pListEvents, String typePDF, List<Mission> listMission, Date pDateRAVoulue) throws DocumentException {
        Integer nbCell = 3;
        PdfPTable lTableFrais = new PdfPTable(nbCell);
        boolean testLigne = false;

        // Gestion des avances ponctuelles
        for (LinkEvenementTimesheet tmpEvent : pListEvents) {

            // --- Evénement autre qu'une absence Validée ou Soumise
            if (!isAbsenceVAOrSO(tmpEvent.getAbsence())) {
                for (Frais tmpFrais : tmpEvent.getListFrais()) {
                    if ("ASF".equals(tmpFrais.getTypeFrais().getCode())
                            && (Etat.ETAT_VALIDE_CODE.equals(tmpFrais.getEtat().getCode()) || Etat.ETAT_SOUMIS_CODE.equals(tmpFrais.getEtat().getCode()))
                            && "PO".equals(tmpFrais.getTypeAvance())
                    ) {
                        if (!testLigne) {
                            // Dans le cas du tout premier passage, on créé le
                            // bloc Avance de Frais
                            lTableFrais.setWidthPercentage(100);
                            lTableFrais.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                            lTableFrais.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                            lTableFrais.getDefaultCell().setColspan(nbCell);
                            lTableFrais.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                            lTableFrais.addCell(new Phrase("DETAIL AVANCE DE FRAIS", fontHeaderWhite));
                            lTableFrais.getDefaultCell().setColspan(1);
                            lTableFrais.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                            lTableFrais.setWidths(new int[]{28, 57, 15});
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_DATE_PERIOD, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_COMMENT, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_POST_TAX_AMOUNT, FONT_BOLD));

                            testLigne = true;
                        }
                        setAvanceFraisRow(lTableFrais, tmpFrais, typePDF, null, pDateRAVoulue);
                    }
                }

            }
        }

        // Gestion des avances permanentes
        for (Mission mission : listMission) {
            List<LinkEvenementTimesheet> eventTimeSheet = mission.getLinkEvenementTimesheetLine();
            for (LinkEvenementTimesheet evt : eventTimeSheet) {
                List<Frais> tmpFrais = evt.getListFrais();
                for (Frais frais : tmpFrais) {

                    // on teste si la date de solde est renseignee
                    int moisCourant = 0;
                    int anneeCourante = 0;
                    int moisSolde = 0;
                    int anneeSolde = 0;
                    Calendar dateSolde = null;
                    Calendar now = null;

                    if (frais.getDateSolde() != null) {
                        // on recupere le mois et l'annee en cours
                        now = new GregorianCalendar();
                        now.setTime(new Date());
                        moisCourant = now.get(Calendar.MONTH);
                        anneeCourante = now.get(Calendar.YEAR);

                        // on recupere le mois et l'annee de solde
                        dateSolde = new GregorianCalendar();
                        dateSolde.setTimeInMillis(frais.getDateSolde().getTime());
                        moisSolde = dateSolde.get(Calendar.MONTH);
                        anneeSolde = dateSolde.get(Calendar.YEAR);
                    }

                    // BAB 20/05/2016  [AMILNOTE-101] : on inclut aussi l'ASFPE soldée dans le RA
                    if ("ASF".equals(frais.getTypeFrais().getCode()) && "PE".equals(frais.getTypeAvance())
                            && (Etat.ETAT_VALIDE_CODE.equals(frais.getEtat().getCode()) || Etat.ETAT_SOUMIS_CODE.equals(frais.getEtat().getCode()) ||
                            Etat.ETAT_SOLDE.equals(frais.getEtat().getCode()))) {

                        if (!testLigne) {
                            // Dans le cas du tout premier passage, on créé le
                            // bloc Avance de Frais
                            lTableFrais.setWidthPercentage(100);
                            lTableFrais.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                            lTableFrais.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                            lTableFrais.getDefaultCell().setColspan(nbCell);
                            lTableFrais.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                            lTableFrais.addCell(new Phrase("DETAIL AVANCE DE FRAIS", fontHeaderWhite));
                            lTableFrais.getDefaultCell().setColspan(1);
                            lTableFrais.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                            lTableFrais.setWidths(new int[]{28, 57, 15});
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_DATE_PERIOD, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_COMMENT, FONT_BOLD));
                            lTableFrais.addCell(new Phrase(COLUMN_EXCEL_POST_TAX_AMOUNT, FONT_BOLD));

                            testLigne = true;
                        }
                        Calendar dateRA = Calendar.getInstance();
                        dateRA.setTime(pDateRAVoulue);

                        Calendar dateFrais = Calendar.getInstance();
                        dateFrais.setTime(frais.getLinkEvenementTimesheet().getDate());
                        if (anneeSolde >= dateRA.get(Calendar.YEAR) && (moisSolde >= dateRA.get(Calendar.MONTH)) &&
                                (dateRA.get(Calendar.YEAR) >= dateFrais.get(Calendar.YEAR)) && (dateRA.get(Calendar.MONTH) >= dateFrais.get(Calendar.MONTH))) {
                            setAvanceFraisRow(lTableFrais, frais, typePDF, mission, pDateRAVoulue);
                        }

                    }
                }
            }
        }

        if (testLigne) {
            // Ligne du total des frais
            lTableFrais.getDefaultCell().setColspan(2);
            lTableFrais.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));
            lTableFrais.getDefaultCell().setColspan(1);
            java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
            lTableFrais.addCell(new Phrase(df.format(mntTotalAvanceFrais) + " €", FONT_BOLD));

            lTableFrais.setSpacingAfter(SPACING_LARGE);
        }
        return lTableFrais;
    }

    /**
     * Ajouter une ligne d'informations de frais pour le PDF
     *
     * @param pTableFrais Table des frais pour le PDF
     * @param pFrais      Frais à traiter
     */
    private void setAvanceFraisRow(PdfPTable pTableFrais, Frais pFrais, String typePDF, Mission mission, Date pDateRAVoulue) {

        LinkFraisTFC tmpDateDebut = null;
        LinkFraisTFC tmpDateFin = null;
        LinkFraisTFC tmpLieuDepart = null;
        LinkFraisTFC tmpLieuArrive = null;
        LinkFraisTFC tmpNombreKm = null;
        LinkFraisTFC tmpSoireeEtape = null;

        Caracteristique tmpCarac;
        String tmpCode;

        // On stock toutes les caracteristiques liées au frais
        for (LinkFraisTFC tmpLinkFraisTFC : pFrais.getLinkFraisTFC()) {

            tmpCarac = tmpLinkFraisTFC.getTypeFraisCarac().getCarac();
            tmpCode = tmpCarac.getCode();

            if (tmpCode.equals(Caracteristique.CARAC_DEBUT)) {
                tmpDateDebut = tmpLinkFraisTFC;
            } else if (tmpCode.equals(Caracteristique.CARAC_FIN)) {
                tmpDateFin = tmpLinkFraisTFC;
            } else if (tmpCode.equals(Caracteristique.CARAC_DEPART)) {
                tmpLieuDepart = tmpLinkFraisTFC;
            } else if (tmpCode.equals(Caracteristique.CARAC_ARRIVE)) {
                tmpLieuArrive = tmpLinkFraisTFC;
            } else if (tmpCode.equals(Caracteristique.CARAC_NOMBREKM)) {
                tmpNombreKm = tmpLinkFraisTFC;
            } else if (tmpCode.equals(Caracteristique.CARAC_SOIREEETAPE)) {
                tmpSoireeEtape = tmpLinkFraisTFC;
            }

        }

        // Ajout de la colonne Date
        if ("PO".equals(pFrais.getTypeAvance())) {
            String textForDateCell;
            LinkEvenementTimesheet tmpEvent;
            if (null != tmpDateDebut && null != tmpDateFin) {
                textForDateCell = DATE_FORMATER.format(tmpDateDebut.getVal_datetime().toDate()) + " - " + DATE_FORMATER.format(tmpDateFin.getVal_datetime().toDate());
            } else {
                tmpEvent = pFrais.getLinkEvenementTimesheet();
                textForDateCell = DATE_FORMATER.format(tmpEvent.getDate());
            }
            pTableFrais.addCell(new Phrase(textForDateCell));
        } else {
            pTableFrais.addCell(new Phrase("Avance Permanente"));
        }
        // Ajout de la colonne Commentaire
        String commentaire = "";
        if ("PO".equals(pFrais.getTypeAvance())) {
            commentaire = "";
        } else if ("PE".equals(pFrais.getTypeAvance())) {
            commentaire = "(" + mission.getMission() + ") ";
        }

        if (null != tmpNombreKm) {
            commentaire += "Nombre de km " + tmpNombreKm.getVal_decimal() + " - ";
        }
        if (null != tmpLieuDepart && null != tmpLieuArrive) {
            commentaire += "De " + tmpLieuDepart.getVal_varchar() + " à " + tmpLieuArrive.getVal_varchar() + " - ";
        }
        if (null != tmpSoireeEtape) {
            commentaire += "Soirée etape : " + (tmpSoireeEtape.getVal_tiny() ? "Oui - " : "Non - ");
        }

        commentaire += (pFrais.getCommentaire() != null ? pFrais.getCommentaire() : "Pas de commentaire.");

        pTableFrais.addCell(new Phrase(commentaire));

        // Calcul de la fin d'une mission pour savoir si on doit solder ou non
        // la mission
        Boolean aSolder;
        String soldeMission = "";
        if (mission != null) {
            aSolder = testMissionASolder(mission);
            if (aSolder) {
                soldeMission = " (S)";
            }
        } else {
            aSolder = true;
        }

        pTableFrais.addCell(new Phrase(String.valueOf(pFrais.getMontant()).concat(" €").concat(soldeMission)));
        if ("PO".equals(pFrais.getTypeAvance()) || aSolder) {
            mntTotalAvanceFrais += pFrais.getMontant().floatValue();
            mntTotalAvanceFraisSansPermanent += pFrais.getMontant().floatValue();
            // BAB 20/05/2016 : [AMILNOTE-101] on ne déduit que asf permanentes soldees
        } else if ("PE".equals(pFrais.getTypeAvance()) && Etat.ETAT_SOLDE.equals(pFrais.getEtat().getCode()) && "ASF".equals(pFrais.getTypeFrais().getCode())) {
            Calendar dateRA = GregorianCalendar.getInstance();
            Calendar dateSolde = GregorianCalendar.getInstance();

            dateRA.setTime(pDateRAVoulue);
            dateSolde.setTime(pFrais.getDateSolde());
            if (dateRA.get(Calendar.YEAR) == dateSolde.get(Calendar.YEAR) && (dateRA.get(Calendar.MONTH) == dateSolde.get(Calendar.MONTH))) {
                mntTotalAvanceFrais += pFrais.getMontant().floatValue();
            }
        }
    }

    /**
     * Méthode permettant de voir si une mission est a solder pour le RA en
     * cours ou non
     *
     * @param mission La mission a tester
     * @return True si il faut la solder, False sinon
     */
    public Boolean testMissionASolder(Mission mission) {
        Date dateFin = mission.getDateFin();
        Date jour = new Date();

        if (null == dateFin || dateFin.after(jour)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Met en place le total des frais du RA
     *
     * @param pListEvents Liste des évènements
     * @return Table des avances de total
     * @throws DocumentException
     */
    private PdfPTable createTotal(List<LinkEvenementTimesheet> pListEvents, String typePDF) throws DocumentException {

        Integer nbCell = 0;
        if (Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            nbCell = 3;
        } else {
            nbCell = 4;
        }
        PdfPTable lTableTotal = new PdfPTable(nbCell);
        lTableTotal.setWidthPercentage(100);
        lTableTotal.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableTotal.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableTotal.getDefaultCell().setColspan(nbCell);
        lTableTotal.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableTotal.addCell(new Phrase(TOTAL_FRAIS, fontHeaderWhite));
        lTableTotal.getDefaultCell().setColspan(1);
        lTableTotal.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        if (!Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            lTableTotal.setWidths(new int[]{25, 25, 25, 25});
        } else {
            lTableTotal.setWidths(new int[]{33, 33, 34});
        }
        lTableTotal.addCell(new Phrase("Total Frais + Forfaits", FONT_BOLD));
        lTableTotal.addCell(new Phrase("Avance de frais déductible", FONT_BOLD));
        lTableTotal.addCell(new Phrase("Total à payer TTC", FONT_BOLD));
        if (!Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            lTableTotal.addCell(new Phrase("Total TVA déductible", FONT_BOLD));
        }

        // Total de frais
        java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");
        lTableTotal.addCell(new Phrase(df.format(mntTotalForfait + mntTotalFrais) + " €"));

        // Avance de frais déductible
        lTableTotal.addCell(new Phrase(df.format(mntTotalAvanceFrais) + " €"));

        // Total à payer TTC
        float totalAPayer = mntTotalForfait + mntTotalFrais - mntTotalAvanceFrais;
        lTableTotal.addCell(new Phrase(df.format(totalAPayer) + " €"));

        // Total TVA déductible
        if (!Constantes.NOM_PDF_COLLAB.equals(typePDF)) {
            lTableTotal.addCell(new Phrase(df.format(totalTVADed + totalAvanceTVADed) + " €"));
        }

        lTableTotal.setSpacingAfter(SPACING_LARGE);

        return lTableTotal;
    }

    /**
     * Créer la table de détails des jours par mission pour le PDF
     *
     * @param pMapDateEvents Map Dates - Evénements
     * @return Table de détails des jours par mission
     */
    private PdfPTable createTableDetailJoursMissions(LinkedHashMap<Date, List<LinkEvenementTimesheet>> pMapDateEvents) {

        // table du detail des jrs travaillés
        PdfPTable lTableEvts = new PdfPTable(3);
        lTableEvts.setWidthPercentage(100);
        lTableEvts.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableEvts.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableEvts.getDefaultCell().setColspan(3);
        lTableEvts.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableEvts.addCell(new Phrase(DETAIL_JOURS, fontHeaderWhite));
        lTableEvts.getDefaultCell().setColspan(1);
        lTableEvts.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableEvts.addCell(new Phrase(JOUR_MISSION, FONT_BOLD));
        lTableEvts.addCell(new Phrase("AM", FONT_BOLD));
        lTableEvts.addCell(new Phrase("PM", FONT_BOLD));
        lTableEvts.setSpacingAfter(SPACING_LARGE);

        Date tmpDate;
        List<LinkEvenementTimesheet> tmpListEvents = new ArrayList<LinkEvenementTimesheet>();
        LinkEvenementTimesheet tmpEvent, tmpEvent2;
        double nbEventsForKey;
        double nbJoursTotal = 0;
        boolean verifExistDateDuJourDansLePdf;
        // --- Pour Chaques jours du mois
        for (Map.Entry<Date, List<LinkEvenementTimesheet>> entry : pMapDateEvents.entrySet()) {
            // --- Récupération de la date et des événements
            tmpDate = entry.getKey();
            tmpListEvents = entry.getValue();

            //SBE_AMNOTE_171 21/11/2016 : rajout du cas pour les absences de type fin de contrat

            nbEventsForKey = tmpListEvents.size();
            // --- Evénements présents AM & PM
            if (nbEventsForKey == 2) { // Dans tous les cas on a 2 events
                //on affiche les details seulement si on n'a pas une fin de contrat
                tmpEvent = tmpListEvents.get(0);
                tmpEvent2 = tmpListEvents.get(1);

                //SI on a une absence et que ce n'est pas une fin de contrat pour l'une des 2 demi journée de la journée
                if ((tmpEvent.getMission() != null || (tmpEvent.getAbsence() != null && !tmpEvent.getAbsence().getTypeAbsence().getCode().equals(TypeAbsence.TYPE_FC)))
                        || (tmpEvent2.getMission() != null || (tmpEvent2.getAbsence() != null && !tmpEvent2.getAbsence().getTypeAbsence().getCode().equals(TypeAbsence.TYPE_FC)))) {
                    // --- Ajout d'une cellule contenant la date
                    lTableEvts.getDefaultCell().setColspan(1);
                    lTableEvts.addCell(DATE_FORMATER.format(tmpDate));
                }

                // --- Ajout d'une cellule pour AM
                if (tmpEvent.getMission() != null || (tmpEvent.getAbsence() != null && !tmpEvent.getAbsence().getTypeAbsence().getCode().equals(TypeAbsence.TYPE_FC))) {
                    lTableEvts.addCell(getCellEventDay(tmpEvent));
                    if (tmpEvent.getMission() != null || (tmpEvent.getAbsence() != null && !(tmpEvent.getAbsence().getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE)))) {
                        nbJoursTotal += 0.5;

                    }
                }

                // --- Ajout d'une cellule pour PM
                if (tmpEvent2.getMission() != null || (tmpEvent2.getAbsence() != null && !tmpEvent2.getAbsence().getTypeAbsence().getCode().equals(TypeAbsence.TYPE_FC))) {
                    lTableEvts.addCell(getCellEventDay(tmpEvent2));
                    if (tmpEvent2.getMission() != null || (tmpEvent2.getAbsence() != null && !(tmpEvent2.getAbsence().getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE))))
                        nbJoursTotal += 0.5;
                }

            } else {
                tmpEvent = tmpListEvents.get(0);
                if (tmpEvent.getMission() != null || (tmpEvent.getAbsence() != null && !tmpEvent.getAbsence().getTypeAbsence().getCode().equals(TypeAbsence.TYPE_FC))) {
                    // --- Ajout d'une cellule contenant la date
                    lTableEvts.getDefaultCell().setColspan(1);
                    lTableEvts.addCell(DATE_FORMATER.format(tmpDate));

                    // --- Evénement uniquemement AM
                    if (tmpEvent.getMomentJournee() == 0) {
                        lTableEvts.addCell(getCellEventDay(tmpEvent));
                        lTableEvts.addCell("");
                        // --- Evénement uniquemement PM
                    } else {
                        lTableEvts.addCell("");
                        lTableEvts.addCell(getCellEventDay(tmpEvent));
                    }
                    nbJoursTotal = nbJoursTotal + 0.5;
                }
            }
        }

        lTableEvts.getDefaultCell().setColspan(2);
        lTableEvts.addCell(new Phrase(TOTAL_LABEL, FONT_BOLD));
        lTableEvts.addCell(new Phrase(nbJoursTotal + " " + JOURS_LABEL, FONT_BOLD));

        return lTableEvts;
    }

    /**
     * Créer le message d'une cellule de tableau pour un événement
     *
     * @param event Evénement à traiter
     * @return Message de la cellule
     */
    private String getCellEventDay(LinkEvenementTimesheet event) {
        String tmpStr;
        Mission tmpMission = event.getMission();
        Absence tmpAbsence = event.getAbsence();

        // --- Présence d'une absence Validée ou Soumise
        if (isAbsenceVAOrSO(tmpAbsence)) {
            tmpStr = tmpAbsence.getTypeAbsence().getTypeAbsence();
            if (tmpAbsence.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)) {
                tmpStr += " (Non Validée)";
            }
            // --- Présence d'une mission
        } else if (tmpMission != null) {
            tmpStr = tmpMission.getMission();
            // --- Autre
        } else {
            tmpStr = "";
        }

        return tmpStr;
    }

    /**
     * Si l'absence est valide ou soumise ou pas null retourne vrai
     *
     * @param pAbsence absence to check
     * @return bool conform value
     */
    public boolean isAbsenceVAOrSO(Absence pAbsence) {
        boolean res = false;
        if (null != pAbsence) {
            res = (pAbsence.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE) || pAbsence.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE) || pAbsence.getEtat().getCode().equals(Etat.ETAT_COMMENCE));
        }
        return res;
    }

    /**
     * Create PDF with and without charges for business review consultation (Consultation des Rapports d'activités)
     *
     * @param Rapports    the Rapports
     * @param nomFichier  the nomFichier
     * @param factureOrRa the factureOrRA
     * @return the string
     * @throws NamingException    the naming exception
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     */
    public String createSeveralPdfRA(List<RapportActivites> Rapports, String nomFichier, String factureOrRa)
            throws NamingException, IOException, SchedulerException, EmailException, MessagingException, DocumentException {
        logger.debug("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");
        Date submissionDate;
        try {
            repertoireRA = Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA);
        } catch (NamingException e) {
            logger.error("[create pdf RA] context parameter naming error", e);
        }
        String chemin = repertoireRA + nomFichier; //chemin du fichier
        File directory = new File(chemin).getParentFile(); //on récupère les dossiers parents
        directory.mkdirs(); // on crée les dossiers parents s'il en manque
        FileOutputStream lBaos = new FileOutputStream(chemin); // création du fichier

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;
        lWriter = PdfWriter.getInstance(lDocument, lBaos);
        TableHeader lEvent = new TableHeader();
        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);
        lDocument.open();
        DateTime pDateRAVoulue = null;

        // CONTENU :
        DateTimeFormatter fmtHead = DateTimeFormat.forPattern(Constantes.DATE_FORMAT_MM_YYYY);
        for (RapportActivites rapportActivites : Rapports) {
            pDateRAVoulue = new DateTime(rapportActivites.getMoisRapport());
            submissionDate = rapportActivites.getDateSoumission();
            if (factureOrRa.equals(Constantes.RA_SANS_FRAIS_TYPE_PDF)) {
                lEvent.setHeader("Rapport d'activités " + fmtHead.print(pDateRAVoulue.dayOfMonth().withMinimumValue()) +
                        " \n Version Sans Frais");
            } else {
                lEvent.setHeader("Rapport d'activités " + fmtHead.print(pDateRAVoulue.dayOfMonth().withMinimumValue()) +
                        " \n Version Avec Frais");
            }
            pdfContains(rapportActivites, lDocument, pDateRAVoulue, submissionDate, factureOrRa);
            lDocument.newPage();
        }
        lDocument.close();
        logger.debug("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");
        return chemin;
    }

    /**
     * Generate the contain of PDF created by createSeveralPdfRA method
     *
     * @param rapportActivites the business reviews
     * @param lDocument        the current pdf
     * @param pDateRAVoulue    the business reviews' dates
     * @param submissionDate    current day date
     * @param factureOrRa      allows the comparison between String
     * @throws IOException       the io exception
     * @throws DocumentException the document exception
     */
    private void pdfContains(RapportActivites rapportActivites, Document lDocument, DateTime pDateRAVoulue, Date submissionDate, String factureOrRa)
            throws IOException, DocumentException {
        String typePDF = Constantes.NOM_PDF_COLLAB;
        DateTime lDebutPeriode = pDateRAVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = pDateRAVoulue.dayOfMonth().withMaximumValue();

        // table date courante
        lDocument.add(createTableDateCreation("DATE DE CREATION DU RAPPORT D'ACTIVITES", submissionDate));
        List<Mission> listMission = missionService.findMissionCollabPourRA(rapportActivites.getCollaborateur(), lDebutPeriode, lFinPeriode);
        List<LinkEvenementTimesheet> listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDates(rapportActivites.getCollaborateur(),
                lDebutPeriode.toString(), lFinPeriode.toString());
        LinkedHashMap<Date, List<LinkEvenementTimesheet>> mapDateEvents = new LinkedHashMap<Date, List<LinkEvenementTimesheet>>();

        // --- Tri des événements par date et moment de la journée
        Collections.sort(listLinkEvent, new Comparator<LinkEvenementTimesheet>() {
            @Override
            public int compare(LinkEvenementTimesheet event1, LinkEvenementTimesheet event2) {
                if (event1.getDate().before(event2.getDate())) {
                    return -1;
                } else if (event1.getDate().after(event2.getDate())) {
                    return 1;
                } else {
                    if (event1.getMomentJournee() < event2.getMomentJournee()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        });
        Utils.mapEventsByDate(listLinkEvent, mapDateEvents);
        mntTotalForfait = (float) 0;
        mntTotalFrais = (float) 0;
        mntTotalAvanceFrais = (float) 0;
        mntTotalAvanceFraisSansPermanent = (float) 0;
        totalTVADed = (float) 0;
        totalAvanceTVADed = (float) 0;
        Paragraph lParagraph = new Paragraph();

        if (factureOrRa.equalsIgnoreCase(Constantes.RA_TYPE_PDF)
                && !rapportActivites.getCollaborateur().getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            // génère les tableaux du RA avec frais
            lParagraph.add(createTableUser(rapportActivites.getCollaborateur()));
            lParagraph.add(createTableForfait(listMission, listLinkEvent, mapDateEvents, pDateRAVoulue));
            lParagraph.add(createTableFrais(listLinkEvent, typePDF));
            lParagraph.add(createAvanceFrais(listLinkEvent, typePDF, listMission, pDateRAVoulue.toDate()));
            lParagraph.add(createTotal(listLinkEvent, typePDF));
        } else {
            // génère les tableaux du RA sans frais
            lParagraph.add(createTableUser(rapportActivites.getCollaborateur()));
            lParagraph.add(createMissionsDaysTable(listMission, pDateRAVoulue));
            lParagraph.add(createTableNbrJoursAbsences(listLinkEvent, pDateRAVoulue));
            lParagraph.add(createTableDetailJoursMissions(mapDateEvents));
        }
        // Ajout des tableaux au PDF
        lDocument.add(lParagraph);
    }
}
