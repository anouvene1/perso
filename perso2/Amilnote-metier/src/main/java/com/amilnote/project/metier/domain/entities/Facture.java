/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Facture.
 */
@Entity
@Table(name = "ami_facture")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Facture implements java.io.Serializable, Comparable<Facture> {

    /**
     * The constant PROP_ID_FACTURE.
     */
    public static final String PROP_ID_FACTURE = "id";
    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";
    /**
     * The constant PROP_COMMANDE.
     */
    public static final String PROP_COMMANDE = "commande";
    /**
     * The constant PROP_ADRESSE_FACTURATION.
     */
    public static final String PROP_ADRESSE_FACTURATION = "adresse_facturation";
    /**
     * The constant PROP_ID_CLIENT.
     */
    public static final String PROP_ID_CLIENT = "idClient";
    /**
     * The constant PROP_QUANTITE.
     */
    public static final String PROP_QUANTITE = "quantite";
    /**
     * The constant PROP_PRIX.
     */
    public static final String PROP_PRIX = "prix";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    /**
     * The constant PROP_ID_CONTACT.
     */
    public static final String PROP_ID_CONTACT = "contact";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant PROP_ACCES_PDF.
     */
    public static final String PROP_ACCES_PDF = "pdf";
    /**
     * The constant PROP_DATE_FACTURATION.
     */
    public static final String PROP_DATE_FACTURATION = "dateFacture";
    public static final String SQL_DATE_FACTURATION = "date_facture";

    /**
     * The constant PROP_MOIS_PRESTATIONS.
     */
    public static final String PROP_MOIS_PRESTATIONS = "mois_prestations";
    /**
     * The constant PROP_RIB_FACTURE.
     */
    public static final String PROP_RIB_FACTURE = "rib_facture";
    /**
     * The constant PROP_COMMENTAIRE.
     */
    public static final String PROP_COMMENTAIRE = "commentaire";
    /**
     * The constant PROP_NUMFACTURE.
     */
    public static final String PROP_NUMFACTURE = "numFacture";
    /**
     * The constant PROP_DATE_CREATION.
     */
    public static final String PROP_DATE_CREATION = "date_creation";
    /**
     * The constant PROP_TYPE_FACTURE.
     */
    public static final String PROP_TYPE_FACTURE = "typeFacture";
    /**
     * The constant PROP_RA_VALIDE.
     */
    public static final String PROP_RA_VALIDE = "raValide";
    /**
     * The constant PROP_IS_AVOIR.
     */
    public static final String PROP_IS_AVOIR = "isAvoir";

    /**
     * The constant PROP_ID_TVA.
     */
    public static final String PROP_ID_TVA = "id_tva";

    private static final long serialVersionUID = -329072377657745936L;
    private int id;
    private Mission mission;
    private Commande commande;
    private String adresse_facturation;
    private int idClient;
    private float quantite;
    private float prix;
    private float montant;
    private int contact;
    private Etat etat;
    private String pdf;
    private Date dateFacture;
    private Date mois_prestations;
    private String rib_facture;
    private String commentaire;
    private int numFacture;
    private Date dateCreation;
    private TypeFacture typeFacture;
    private int raValide;
    private boolean isAvoir;
    private TVA tva;

	/* CONSTRUCTORS */

    /**
     * Instantiates a new Facture.
     */
    public Facture() {
        super();
    }

    /**
     * Instantiates a new Facture.
     *
     * @param pCommande            the p commande
     * @param pAdresse_facturation the p adresse facturation
     * @param pId_client           the p id client
     * @param pQuantite            the p quantite
     * @param pPrix                the p prix
     * @param pMontant             the p montant
     * @param pContact             the p contact
     * @param pEtat                the p etat
     * @param pPdf                 the p pdf
     * @param pDate                the p date
     * @param pMoisPrestations     the p mois prestations
     * @param pRib                 the p rib
     * @param pCommentaire         the p commentaire
     * @param pNumFacture          the p num facture
     * @param pDateCreation        the p date creation
     * @param pTypeFacture         the p type facture
     * @param pIsAvoir             the p is_avoir
     * @param pTva             the p id_tva
     */
    public Facture(Commande pCommande,
                   String pAdresse_facturation,
                   int pId_client,
                   float pQuantite,
                   float pPrix,
                   float pMontant,
                   int pContact,
                   Etat pEtat,
                   String pPdf,
                   Date pDate,
                   Date pMoisPrestations,
                   String pRib,
                   String pCommentaire,
                   int pNumFacture,
                   Date pDateCreation,
                   TypeFacture pTypeFacture,
                   Boolean pIsAvoir,
                   TVA pTva) {

        super();
        commande = pCommande;
        adresse_facturation = pAdresse_facturation;
        idClient = pId_client;
        quantite = pQuantite;
        prix = pPrix;
        montant = pMontant;
        contact = pContact;
        etat = pEtat;
        pdf = pPdf;
        dateFacture = pDate;
        mois_prestations = pMoisPrestations;
        rib_facture = pRib;
        commentaire = pCommentaire;
        numFacture = pNumFacture;
        dateCreation = pDateCreation;
        typeFacture = pTypeFacture;
        isAvoir = pIsAvoir;
        tva = pTva;
    }

    /**
     * Constructeur après la refonte
     * @param pMission             the p mission
     * @param pCommande            the p commande
     * @param pAdresse_facturation the p adresse facturation
     * @param pId_client           the p id client
     * @param pQuantite            the p quantite
     * @param pPrix                the p prix
     * @param pMontant             the p montant
     * @param pContact             the p contact
     * @param pEtat                the p etat
     * @param pPdf                 the p pdf
     * @param pDate                the p date
     * @param pMoisPrestations     the p mois prestations
     * @param pRib                 the p rib
     * @param pCommentaire         the p commentaire
     * @param pNumFacture          the p num facture
     * @param pDateCreation        the p date creation
     * @param pTypeFacture         the p type facture
     * @param pRaValide            the p raValide
     * @param pIsAvoir             the p is_avoir
     * @param pTva             the p id_tva
     */
    public Facture(
        Mission pMission,
        Commande pCommande,
        String pAdresse_facturation,
        int pId_client,
        float pQuantite,
        float pPrix,
        float pMontant,
        int pContact,
        Etat pEtat,
        String pPdf,
        Date pDate,
        Date pMoisPrestations,
        String pRib,
        String pCommentaire,
        int pNumFacture,
        Date pDateCreation,
        TypeFacture pTypeFacture,
        int pRaValide,
        Boolean pIsAvoir,
        TVA pTva) {

        super();
        mission = pMission;
        commande = pCommande;
        adresse_facturation = pAdresse_facturation;
        idClient = pId_client;
        quantite = pQuantite;
        prix = pPrix;
        montant = pMontant;
        contact = pContact;
        etat = pEtat;
        pdf = pPdf;
        dateFacture = pDate;
        mois_prestations = pMoisPrestations;
        rib_facture = pRib;
        commentaire = pCommentaire;
        numFacture = pNumFacture;
        dateCreation = pDateCreation;
        typeFacture = pTypeFacture;
        raValide = pRaValide;
        isAvoir = pIsAvoir;
        tva = pTva;
    }

    /**
     * To json facture as json.
     *
     * @return the facture as json
     */
    public FactureAsJson toJson() {
        return new FactureAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets commande.
     *
     * @return the commande
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_commande")
    public Commande getCommande() {
        return commande;
    }

    /**
     * Sets commande.
     *
     * @param commande the commande
     */
    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    /**
     * Gets adresse facturation.
     *
     * @return the adresse facturation
     */
    @Column(name = "adresse_facturation")
    public String getAdresseFacturation() {
        return adresse_facturation;
    }

    /**
     * Sets adresse facturation.
     *
     * @param adresse the adresse
     */
    public void setAdresseFacturation(String adresse) {
        this.adresse_facturation = adresse;
    }

    /**
     * Gets id client.
     *
     * @return the id client
     */
    @Column(name = "id_client")
    public int getIdClient() {
        return idClient;
    }

    /**
     * Sets id client.
     *
     * @param idClient the id client
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    @Column(name = "quantite")
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param q the q
     */
    public void setQuantite(float q) {
        this.quantite = q;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    @Column(name = "prix")
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param p the p
     */
    public void setPrix(float p) {
        this.prix = p;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "montant")
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param m the m
     */
    public void setMontant(float m) {
        this.montant = m;
    }

    /**
     * Gets contact.
     *
     * @return the contact
     */
    @Column(name = "id_contact_client")
    public int getContact() {
        return contact;
    }

    /**
     * Sets contact.
     *
     * @param c the c
     */
    public void setContact(int c) {
        this.contact = c;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etat")
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets pdf.
     *
     * @return the pdf
     */
    @Column(name = "acces_pdf")
    public String getPdf() {
        return pdf;
    }

    /**
     * Sets pdf.
     *
     * @param pdf the pdf
     */
    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    /**
     * Gets date facture.
     *
     * @return the date facture
     */
    @Column(name = "date_facture")
    public Date getDateFacture() {
        return dateFacture;
    }

    /**
     * Sets date facture.
     *
     * @param d the d
     */
    public void setDateFacture(Date d) {
        this.dateFacture = d;
    }

    /**
     * Gets mois prestations.
     *
     * @return the mois prestations
     */
    @Column(name = "mois_prestations")
    public Date getMoisPrestations() {
        return mois_prestations;
    }

    /**
     * Sets mois prestations.
     *
     * @param mois the mois
     */
    public void setMoisPrestations(Date mois) {
        this.mois_prestations = mois;
    }

    /**
     * Gets rib facture.
     *
     * @return the rib facture
     */
    @Column(name = "rib_facture")
    public String getRIBFacture() {
        return rib_facture;
    }

    /**
     * Sets rib facture.
     *
     * @param rib the rib
     */
    public void setRIBFacture(String rib) {
        this.rib_facture = rib;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    @Column(name = "commentaire")
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param comm the comm
     */
    public void setCommentaire(String comm) {
        this.commentaire = comm;
    }

    /**
     * Gets num facture.
     *
     * @return the num facture
     */
    @Column(name = "num_facture")
    public int getNumFacture() {
        return numFacture;
    }

    /**
     * Sets num facture.
     *
     * @param numFacture the num facture
     */
    public void setNumFacture(int numFacture) {
        this.numFacture = numFacture;
    }

    /**
     * Gets date creation.
     *
     * @return the date creation
     */
    @Column(name = "date_creation", nullable = false)
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Sets date creation.
     *
     * @param d the d
     */
    public void setDateCreation(Date d) {
        this.dateCreation = d;
    }

    /**
     * Gets the facture type.
     *
     * @return the facture type
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_type_facture")
    public TypeFacture getTypeFacture() {
        return typeFacture;
    }

    /**
     * Sets the facture type
     *
     * @param pTypeFacture the facture type
     */
    public void setTypeFacture(TypeFacture pTypeFacture) {
        this.typeFacture = pTypeFacture;
    }

    /**
     * Gets raValide
     *
     * @return the raValide
     */
    @Column(name = "ra_valide")
    public int getRaValide() {
        return raValide;
    }

    /**
     * Sets raValide
     *
     * @param raValide the raValide
     */
    public void setRaValide(int raValide) {
        this.raValide = raValide;
    }

    /**
     * Gets isAvoir
     *
     * @return the isAvoir
     */
    @Column(name = "is_avoir")
    public boolean getIsAvoir() {
        return isAvoir;
    }

    /**
     * Sets isAvoir
     *
     * @param isAvoir the isAvoir
     */
    public void setIsAvoir(boolean isAvoir) {
        this.isAvoir = isAvoir;
    }


    /**
     * Gets the mission
     *
     * @return the mission
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_mission")
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets the mission
     * @param pMission the mission
     */
    public void setMission(Mission pMission) {
        this.mission = pMission;
    }


    /**
     * Gets TVA.
     *
     * @return the tva
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tva")
    public TVA getTVA() {
        return tva;
    }

    /**
     * Sets TVA.
     *
     * @param ptva the tva
     */
    public void setTVA(TVA ptva) {
        this.tva = ptva;
    }


    /**
     * Permet de trier une liste de Facture en fonction du numéro de facture
     * @param facture la facture
     * @return -1, 0 ou 1
     */
    @Override
    public int compareTo(Facture facture) {

        int compareNumFacture = facture.getNumFacture();
        return this.numFacture - compareNumFacture;
    }

    /**
     * Permet de trier une liste de Facture en fonction du nom du client
     * @param facture the facture
     * @return int
     */
    public int compareToByClient(Facture facture) {
        String clientName1;
        String clientName2;
        if (this.getCommande() != null) {
            clientName1 = this.getCommande().getClient().getNom_societe();
        }
        else {
            clientName1 = this.getMission().getClient().getNom_societe();
        }
        if (facture.getCommande() != null) {
            clientName2 = facture.getCommande().getClient().getNom_societe();
        }
        else {
            clientName2 = facture.getMission().getClient().getNom_societe();
        }
        clientName1 = clientName1.replace(" ", "");
        clientName2 = clientName2.replace(" ", "");
        int result = clientName1.compareToIgnoreCase(clientName2);
        return result;
    }
}
