/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.TVA;

;

/**
 * The interface Tva dao.
 */
public interface TvaDAO extends LongKeyDAO<TVA> {

}
