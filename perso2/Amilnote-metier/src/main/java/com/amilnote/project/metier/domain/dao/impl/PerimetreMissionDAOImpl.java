/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.PerimetreMissionDAO;
import com.amilnote.project.metier.domain.entities.PerimetreMission;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sbenslimane on 22/12/2016.
 */
@Repository("PerimetreMissionDAO")
public class PerimetreMissionDAOImpl extends AbstractDAOImpl<PerimetreMission> implements PerimetreMissionDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<PerimetreMission> getReferenceClass() {
        return PerimetreMission.class;
    }

    /**
     * {@linkplain PerimetreMissionDAO#findById(Long)}
     */
    @Override
    public PerimetreMission findById(Long id) {
        PerimetreMission lPerimetreMission = (PerimetreMission) currentSession().get(PerimetreMission.class, id);
        return lPerimetreMission;
    }

    /**
     * {@linkplain PerimetreMissionDAO#findAll()}
     */
    @Override
    public List<PerimetreMission> findAll() {
        Criteria lCritPerimetreMission = getCriteria();
        return (List<PerimetreMission>) lCritPerimetreMission.list();
    }
}
