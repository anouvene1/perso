/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.ForfaitDAO;
import com.amilnote.project.metier.domain.dao.impl.ForfaitDAOImpl;
import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.ForfaitAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amilnote.project.metier.domain.services.ForfaitService;

import java.util.*;

/**
 * The type Forfait service.
 */
@Service("ForfaitService")
public class ForfaitServiceImpl extends AbstractServiceImpl<Forfait, ForfaitDAOImpl> implements ForfaitService {

    private Map<Long, List<LinkMissionForfaitAsJson>> mapForfaitsLIBToAdd = new HashMap<>();
    private Map<Long, List<LinkMissionForfaitAsJson>> mapForfaitsNonLIBToAdd = new HashMap<>();

    private List<LinkMissionForfaitAsJson> listForfaitsLIBToAdd = new ArrayList<>();
    private List<LinkMissionForfaitAsJson> listForfaitsNonLIBToAdd = new ArrayList<>();

    @Autowired
    private ForfaitDAO forfaitDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public ForfaitDAOImpl getDAO() {
        return (ForfaitDAOImpl) forfaitDAO;
    }

    /**
     * {@linkplain ForfaitService#findForfaitsForMission(Mission)}
     */
    @Override
    public List<Forfait> findForfaitsForMission(Mission pMission) {
        return forfaitDAO.findForfaitsForMission(pMission);
    }

    /**
     * {@linkplain ForfaitService#findForfaitByCode(String)}
     */
    @Override
    public Forfait findForfaitByCode(String code) {
        return forfaitDAO.findUniqEntiteByProp(Forfait.PROP_CODE, code);
    }

    /**
     * {@linkplain ForfaitService#getAllForfaitsAsJson()}
     */
    @Override
    public List<ForfaitAsJson> getAllForfaitsAsJson() {

        List<Forfait> lListForfaits = forfaitDAO.loadAll();
        List<ForfaitAsJson> lListForfaitsAsJson = new ArrayList<ForfaitAsJson>();

        for (Forfait lForfait : lListForfaits) {
            lListForfaitsAsJson.add(lForfait.toJson());
        }
        return lListForfaitsAsJson;
    }

    /**
     * {@linkplain ForfaitService#getListForfaitsLIBToAdd()}
     */
    @Override
    public List<LinkMissionForfaitAsJson> getListForfaitsLIBToAdd() {
        return listForfaitsLIBToAdd;
    }

    /**
     * {@linkplain ForfaitService#setListForfaitsLIBToAdd(List)}
     */
    @Override
    public void setListForfaitsLIBToAdd(List<LinkMissionForfaitAsJson> listForfaitsToAdd) {
        this.listForfaitsLIBToAdd = listForfaitsToAdd;
    }

    /**
     * {@linkplain ForfaitService#getListForfaitsNonLIBToAdd()}
     */
    @Override
    public List<LinkMissionForfaitAsJson> getListForfaitsNonLIBToAdd() {
        return listForfaitsNonLIBToAdd;
    }

    /**
     * {@linkplain ForfaitService#setListForfaitsNonLIBToAdd(List)}
     */
    @Override
    public void setListForfaitsNonLIBToAdd(List<LinkMissionForfaitAsJson> listForfaitsToAdd){
        this.listForfaitsNonLIBToAdd = listForfaitsToAdd;
    }


    /**
     * {@linkplain ForfaitService#getListForfaitsLIBToAdd(Long)}
     */
    @Override
    public List<LinkMissionForfaitAsJson> getListForfaitsLIBToAdd(Long collaboratorId) {
        mapForfaitsLIBToAdd.computeIfAbsent(collaboratorId, aLong -> new ArrayList<>());
        return mapForfaitsLIBToAdd.get(collaboratorId);
    }

    /**
     * {@linkplain ForfaitService#setListForfaitsLIBToAdd(Long, List)}
     */
    @Override
    public void setListForfaitsLIBToAdd(Long collaboratorId, List<LinkMissionForfaitAsJson> listForfaitsToAdd) {
        this.mapForfaitsLIBToAdd.put(collaboratorId, listForfaitsToAdd);
    }

    /**
     * {@linkplain ForfaitService#getListForfaitsNonLIBToAdd(Long)}
     */
    @Override
    public List<LinkMissionForfaitAsJson> getListForfaitsNonLIBToAdd(Long collaboratorId) {
        mapForfaitsNonLIBToAdd.computeIfAbsent(collaboratorId, aLong -> new ArrayList<>());
        return mapForfaitsNonLIBToAdd.get(collaboratorId);
    }

    /**
     * {@linkplain ForfaitService#setListForfaitsNonLIBToAdd(Long, List)}
     */
    @Override
    public void setListForfaitsNonLIBToAdd(Long collaboratorId, List<LinkMissionForfaitAsJson> listForfaitsToAdd){
        this.mapForfaitsNonLIBToAdd.put(collaboratorId, listForfaitsToAdd);
    }

    /**
     * {@linkplain ForfaitService#clearForfaitsMaps(Long)}
     */
    @Override
    public void clearForfaitsMaps(Long collaboratorId){
        mapForfaitsLIBToAdd.remove(collaboratorId);
        mapForfaitsNonLIBToAdd.remove(collaboratorId);
    }

    /**
     *{@linkplain ForfaitService#removeNonLibForfait(List, String, float)}
     */
    @Override
    public void removeNonLibForfait(List<LinkMissionForfaitAsJson> listForfaits, String code, float amount){
        Iterator<LinkMissionForfaitAsJson> listLibIt = listForfaits.iterator();
        while(listLibIt.hasNext()){
            LinkMissionForfaitAsJson value = listLibIt.next();
            if((value.getForfait().getCode().equals(code)) && (value.getMontant() == amount)){
                listLibIt.remove();
            }
        }
    }

    /**
     *{@linkplain ForfaitService#removeLibForfait(List, String)}
     */
    @Override
    public void removeLibForfait(List<LinkMissionForfaitAsJson> listForfaits, String code) {
        Iterator<LinkMissionForfaitAsJson> listLibIt = listForfaits.iterator();
        while (listLibIt.hasNext()) {
            LinkMissionForfaitAsJson value = listLibIt.next();
            if (value.getForfait().getCode().equals(code)) {
                listLibIt.remove();
            }
        }
    }
}
