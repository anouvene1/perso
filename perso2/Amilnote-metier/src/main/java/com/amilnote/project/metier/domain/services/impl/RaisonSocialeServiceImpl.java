package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.RaisonSocialeDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RaisonSociale;
import com.amilnote.project.metier.domain.entities.json.RaisonSocialeAsJson;
import com.amilnote.project.metier.domain.services.RaisonSocialeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("RaisonServiceService")
public class RaisonSocialeServiceImpl extends AbstractServiceImpl<RaisonSociale, RaisonSocialeDAO> implements RaisonSocialeService{

    @Autowired
    private RaisonSocialeDAO raisonSocialeDAO;

    @Override
    public List<RaisonSociale> getAllRaisonSociale() {
        return this.getDAO().loadAll();
    }

    //TO DO : A tester les fonction si besoin est

    @Override
    public List<RaisonSocialeAsJson> getAllRaisonSocialeAsJson() {
        List<RaisonSociale> lListRaisonSociale = this.getAllRaisonSociale();
        List<RaisonSocialeAsJson> lListRaisonSocialeAsJson = new ArrayList<>(lListRaisonSociale.size());

        for (RaisonSociale lPoste : lListRaisonSociale) {
            lListRaisonSocialeAsJson.add(lPoste.toJson());
        }
        return lListRaisonSocialeAsJson;
    }

    @Override
    public RaisonSociale getRaisonSocialeByCollab(Collaborator collab) {
        List<RaisonSociale> lListRaisonSociale = getAllRaisonSociale();
        RaisonSociale raisonSocialeRetour = null;
        for (RaisonSociale tempRaisonSociale : lListRaisonSociale) {
            if (tempRaisonSociale.getCollaborator().getId()== collab.getId()) {
                raisonSocialeRetour = tempRaisonSociale;
            }
        }
        return raisonSocialeRetour;
    }

    @Override
    public RaisonSocialeDAO getDAO() {
        return raisonSocialeDAO;
    }
}
