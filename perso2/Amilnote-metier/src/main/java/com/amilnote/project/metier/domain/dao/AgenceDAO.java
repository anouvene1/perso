package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Agence;

import java.util.List;

/**
 * The interface Agence dao.
 */
public interface AgenceDAO extends LongKeyDAO<Agence>{
    /**
     * Retourne tous les agences
     *
     * @return List all agences
     */
    List<Agence> getAllAgence();

    /**
     * Retourne une agence en fonction de l'id
     *
     * @param pId the p id
     * @return Agence agence by id
     */
    Agence getAgenceById(long pId);
}
