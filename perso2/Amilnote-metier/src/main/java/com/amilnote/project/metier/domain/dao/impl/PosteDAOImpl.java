/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.PosteDAO;
import com.amilnote.project.metier.domain.entities.Poste;
import org.springframework.stereotype.Repository;

/**
 * The type Poste dao.
 */
@Repository("posteDAO")
public class PosteDAOImpl extends AbstractDAOImpl<Poste> implements PosteDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Poste> getReferenceClass() {
        return Poste.class;
    }
}
