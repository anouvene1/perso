/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.MoyensPaiement;

import java.util.Date;

/**
 * The type Commande as json.
 */
public class CommandeAsJson {

    private Long id;
    private Mission mission;
    private Date dateDebut;
    private Date dateFin;
    private Float nbJours;
    private String numero;
    private float montant;
    private String commentaire;
    private String justificatif;
    private MoyensPaiement paiement;
    private String condition_paiement;
    private Float budget;
    private Etat etat;
    private Client client;
    private Long idResponsableFacturation;

    /**
     * Instantiates a new Commande as json.
     *
     * @param commande the commande
     */
    public CommandeAsJson(Commande commande) {
        this.setId(commande.getId());
        this.setMission(commande.getMission());

        if (commande.getDateDebut() != null) {
            this.setDateDebut(commande.getDateDebut());
        }
        if (commande.getDateFin() != null){
            this.setDateFin(commande.getDateFin());
        }
        if (commande.getNbJours() != null) {
            this.setNbJours(commande.getNbJours());
        }
        if (commande.getNumero() != null) {
            this.setNumero(commande.getNumero());
        }
        if (commande.getMontant() != null){
            this.setMontant(commande.getMontant());
        }
        if (commande.getCommentaire() != null){
            this.setCommentaire(commande.getCommentaire());
        }
        if (commande.getJustificatif() != null){
            this.setJustificatif(commande.getJustificatif());
        }
        this.setMoyensPaiement(commande.getMoyensPaiement());
        this.setConditionPaiement(commande.getConditionPaiement());

        if (commande.getBudget() != null){
            this.setBudget(commande.getBudget());
        }
        if (commande.getEtat() != null){
            this.setEtat(commande.getEtat());
        }
        if (commande.getIdResponsableFacturation() != null){
            this.setIdResponsableFacturation(idResponsableFacturation);
        }

        this.setClient(commande.getClient());

    }

    /**
     * Instantiates a new Commande as json.
     */
    public CommandeAsJson() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission to set
     */
    public void setMission(Mission mission) {
        this.mission = mission;
    }

    /**
     * Gets date debut.
     *
     * @return the dateDebut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param dateDebut the dateDebut to set
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return the dateFin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param dateFin the dateFin to set
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets nb jours.
     *
     * @return the nbJours
     */
    public Float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param nbJours the nbJours to set
     */
    public void setNbJours(Float nbJours) {
        this.nbJours = nbJours;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param montant the montant
     */
    public void setMontant(float montant) {
        this.montant = montant;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets justificatif.
     *
     * @return the justificatif
     */
    public String getJustificatif() {
        return justificatif;
    }

    /**
     * Sets justificatif.
     *
     * @param justificatif the justificatif
     */
    public void setJustificatif(String justificatif) {
        this.justificatif = justificatif;
    }

    /**
     * Gets moyens paiement.
     *
     * @return the moyens paiement
     */
    public MoyensPaiement getMoyensPaiement() {
        return paiement;
    }

    /**
     * Sets moyens paiement.
     *
     * @param paiement the paiement
     */
    public void setMoyensPaiement(MoyensPaiement paiement) {
        this.paiement = paiement;
    }

    /**
     * Gets condition paiement.
     *
     * @return the condition paiement
     */
    public String getConditionPaiement() {
        return condition_paiement;
    }

    /**
     * Sets condition paiement.
     *
     * @param condition the condition
     */
    public void setConditionPaiement(String condition) {
        this.condition_paiement = condition;
    }

    /**
     * Gets budget.
     *
     * @return the budget
     */
    public Float getBudget() {
        return budget;
    }

    /**
     * Sets budget.
     *
     * @param budget the budget
     */
    public void setBudget(Float budget) {
        this.budget = budget;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat to set
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Gets respFacturation
     * @return the billing manager
     */
    public Long getIdResponsableFacturation() {
        return idResponsableFacturation;
    }

    /**
     * Sets respFacturation
     * @param idResponsableFacturation id
     */
    public void setIdResponsableFacturation(Long idResponsableFacturation) {
        this.idResponsableFacturation = idResponsableFacturation;
    }
}
