/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.ContactClient;

/**
 * The type Contact client as json.
 */
public class ContactClientAsJson {

    private Long id;
    private String nom;
    private String prenom;
    private String mail;
    private String telephone;
    private ClientAsJson client;
    private boolean respClient;
    private boolean respFacturation;
    private boolean enabled;


    /**
     * Instantiates a new Contact client as json.
     */
    public ContactClientAsJson() {
    }

    /**
     * Instantiates a new Contact client as json.
     *
     * @param contactClient the contactclient
     */
    public ContactClientAsJson(ContactClient contactClient) {
        this.setId(contactClient.getId());
        this.setClient(contactClient.getClient().toJson());
        this.setMail(contactClient.getMail());
        this.setNom(contactClient.getNom());
        this.setPrenom(contactClient.getPrenom());
        this.setTelephone(contactClient.getTelephone());
        this.setRespClient(contactClient.isRespClient());
        this.setRespFacturation(contactClient.isRespFacturation());
        this.setEnabled(contactClient.isEnabled());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets mail.
     *
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets mail.
     *
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Gets telephone.
     *
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets telephone.
     *
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public ClientAsJson getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client to set
     */
    public void setClient(ClientAsJson client) {
        this.client = client;
    }

    /**
     * Gets resp client.
     *
     * @return the respClient
     */
    public Boolean getRespClient() {
        return respClient;
    }

    /**
     * Sets resp client.
     *
     * @param respClient the respClient to set
     */
    public void setRespClient(Boolean respClient) {
        this.respClient = respClient;
    }

    /**
     * Gets resp facturation.
     *
     * @return the respFacturation
     */
    public Boolean getRespFacturation() {
        return respFacturation;
    }

    /**
     * Sets resp facturation.
     *
     * @param respFacturation the respFacturation to set
     */
    public void setRespFacturation(Boolean respFacturation) {
        this.respFacturation = respFacturation;
    }

    /**
     * Set contact client to enabled status
     * @param enabled The enabled status of contact client
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    /**
     * Get enabled status
     * @return Enabled status
     */
    public boolean isEnabled() {
        return enabled;
    }

}