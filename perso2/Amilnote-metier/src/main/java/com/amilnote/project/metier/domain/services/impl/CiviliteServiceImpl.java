package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.CiviliteDAO;
import com.amilnote.project.metier.domain.entities.Civilite;
import com.amilnote.project.metier.domain.services.CiviliteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("civiliteService")
public class CiviliteServiceImpl extends AbstractServiceImpl<Civilite, CiviliteDAO> implements CiviliteService {

    @Autowired
    private CiviliteDAO civiliteDAO;

    @Override
    public CiviliteDAO getDAO() {
        return civiliteDAO;
    }

    @Override
    public void save(Civilite civilite) {
        civiliteDAO.save(civilite);
    }

    @Override
    public void saveOrUpdate(Civilite civilite) {
        civiliteDAO.saveOrUpdate(civilite);
    }

    @Override
    public void delete(Civilite civilite) {
        civiliteDAO.delete(civilite);
    }

    public Civilite get(Long id){
        return civiliteDAO.get(id);
    }


    /**
     *  @return une liste de civilités triés par ID
     */
    @Override
    public List<Civilite> findAllOrderById() {
        return civiliteDAO.findAllOrderById();
    }
}
