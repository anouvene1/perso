/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.LinkTypeFraisCaracDAO;
import com.amilnote.project.metier.domain.dao.impl.LinkTypeFraisCaracDAOImpl;
import com.amilnote.project.metier.domain.entities.LinkTypeFraisCarac;
import com.amilnote.project.metier.domain.entities.json.LinkTypeFraisCaracAsJson;
import com.amilnote.project.metier.domain.services.LinkTypeFraisCaracService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Link type frais carac service.
 */
@Service("linkTypeFraisCarac")
public class LinkTypeFraisCaracServiceImpl extends AbstractServiceImpl<LinkTypeFraisCarac, LinkTypeFraisCaracDAOImpl> implements LinkTypeFraisCaracService {

    @Autowired
    private LinkTypeFraisCaracDAO linkTypeFraisCaracDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public LinkTypeFraisCaracDAOImpl getDAO() {
        return (LinkTypeFraisCaracDAOImpl) linkTypeFraisCaracDAO;
    }

    /**
     * {@linkplain LinkTypeFraisCaracService#findAll()}
     */
    @Override
    public List<LinkTypeFraisCarac> findAll() {
        return linkTypeFraisCaracDAO.findAll();
    }

    /**
     * {@linkplain LinkTypeFraisCaracService#findById(Long)}
     */
    @Override
    public LinkTypeFraisCarac findById(Long pId) {
        return linkTypeFraisCaracDAO.findUniqEntiteByProp(LinkTypeFraisCarac.PROP_ID, pId);
    }

    /**
     * {@linkplain LinkTypeFraisCaracService#findAllAsJson()}
     */
    @Override
    public String findAllAsJson() {
        List<LinkTypeFraisCarac> lListFraisCarac = this.findAll();
        List<LinkTypeFraisCaracAsJson> lLinkTypeFraisCaracAsJson = new ArrayList<>();
        for (LinkTypeFraisCarac lFraisCarac : lListFraisCarac) {
            lLinkTypeFraisCaracAsJson.add(lFraisCarac.toJson());
        }
        return this.writeJson(lLinkTypeFraisCaracAsJson);
    }
}
