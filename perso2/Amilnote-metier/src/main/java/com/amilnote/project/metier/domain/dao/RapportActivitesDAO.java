/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RapportActivites;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

/**
 * The interface Rapport activites dao.
 */
public interface RapportActivitesDAO extends LongKeyDAO<RapportActivites> {

    /**
     * sauvegarde ou met à jour un rapport d'activites
     *
     * @param pDateSoumission     the p date soumission
     * @param pPdfRapportActivite the p pdf rapport activite
     * @param collaborator        the p travailleur
     * @param pDateRAVoulue       the p date ra voulue
     * @param pAvecFrais          the p avec frais
     * @return string string
     */
    String saveNewRACollaborator(Date pDateSoumission, String pPdfRapportActivite, Collaborator collaborator, DateTime pDateRAVoulue, boolean pAvecFrais);

    /**
     * Retourne la date de soumission du dernier RA
     *
     * @param collaborator the p collaborateur
     * @return date dernier ra soumis
     */
    Date getDateDernierRASoumis(Collaborator collaborator);

    /**
     * Retourne les RA qui existent déjà et qui ne sont pas Annulés ou refusés
     *
     * @param collaborator the p collaborateur
     * @param pDateTime      the p date time
     * @return existing ra for month
     */
    List<RapportActivites> getExistingRAForMonth(Collaborator collaborator, DateTime pDateTime);

    List<RapportActivites> getAllRAForCollaboratorBetweenDate(Collaborator collaborator, DateTime pDateTime);

    //[JNA][AMNOTE 53] Récupération des RA brouillon

    /**
     * Retourne les RA brouillon
     *
     * @param collaborator the p collaborateur
     * @param pDateTime      the p date time
     * @return brouillon ra for month
     */
    List<RapportActivites> getBrouillonRAForMonth(Collaborator collaborator, DateTime pDateTime);


    List<RapportActivites> getRACollaboratorMissionClienteForMonth(DateTime pDateTime);
}

