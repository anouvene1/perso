package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

public interface FileService {

    /**
     * Convert a file (with its extension) into a .ZIP file
     * @param fileName is the name of the file that must be converted
     * @param folder is the location of the file (folder context)
     */
     void convertFileToZip(String fileName, String folder);

    /**
     * Unzip a file
     * @param file is the file that must be unzipped (must be in a ZIP format)
     * @return the file that is contained into the .ZIP file
     */
     File unZipFile(File file);

    /**
     * deletes a file from the system
     * @param fileName is the name of the file that must be deleted
     * @param folder is the location of the file (folder context)
     */
     void deleteFile(String fileName, String folder);

    /**
     * deletes a file from the system
     * @param file is the file that must be deleted
     */
     void deleteFile(File file);

    /**
     * Check if the file was zipped
     * @param fullPathFile File path to check
     * @return file was zipped or not
     */
     boolean fileWasZipped(String fullPathFile);

    /**
     * Check if the file was zipped
     * @param file file to check
     * @return file was zipped or not
     */
     boolean fileWasZipped(File file);

    /**
     * Read all bytes of a file retrieved by a path and return a byte array
     * @param filePath the path to the file to convert
     * @return a byte array of the converted file
     * @throws IOException file Network/Local read/write problem
     */
     byte[] readFileByPath(String filePath) throws IOException;

    /**
     * Creates a folder according to the filePath passed as parameter
     * @param filePath the path of the file to be created
     */
     void createFolder(String filePath);

    /**
     * Write a file to the given destination
     * @param filePath      the file's path
     * @param fileAsBytes   the file as bytes
     */
    void writeFile(String filePath, byte[] fileAsBytes);

    /**
     * To write an Output Stream file
     * @param filepath the path of the file to be created
     * @param wb new Workbook ('Workbook' means Microsoft Excel file)
     * @throws IOException the IOException
     */
    void writeFileOutputStream(String filepath, Workbook wb) throws IOException;

    /**
     * Add a folder to a given zip
     * @param path path to the folder to include into the zip
     * @param srcFolder the zip file path
     * @param zip the zip file who will include the folder
     * @throws IOException network/local file/folder read/write problem
     */
    void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws IOException ;

    /**
     * Add files retrieved by their file path to a generated zip
     * @param path the folder path to put the zip
     * @param generatedZipFilePath the zip file path where will be placed the generated result zip
     * @param filesPathsAsString files paths for retrieving the files to add to the zip
     */
    void addFilesToGeneratedZip(String path, String generatedZipFilePath, String... filesPathsAsString) ;

    /**
     * Add files retrieved by their file path to a given zip
     * @param path the folder path to put the zip
     * @param resultZip the zip file path where will be placed the generated result zip
     * @param filesPathsAsString files paths for retrieving the files to add to the zip
     * @throws IOException network/local file/folder read/write problem
     */
    void addFilesToExistingZip(String path, ZipOutputStream resultZip, String... filesPathsAsString) throws IOException ;

    /**
     * Upload and valid and store a file from local storage user to server storage
     * @param file a file to uploaded
     * @param fullPath the full path of file ex: directory/fichier.ext (path + filename + extention)
     * @param fileSizeMax the max size file validator
     * @param acceptedFormat accepted format(s)
     * @throws Exception transfert exception from FileUpload object
     */
    void uploadFile(MultipartFile file, String fullPath, Long fileSizeMax, FileFormatEnum[] acceptedFormat) throws Exception;

    /**
     * File upload validator with custom size file and format(s)
     * @param file a file to checked
     * @param fileSizeMax the max size file validator
     * @param acceptedFormat accepted format(s)
     * @throws Exception exception
     */
    void isValidToUpload(MultipartFile file, Long fileSizeMax, FileFormatEnum[] acceptedFormat) throws Exception;

    /**
     * Check if file contain valid content
     * @param file a file to check
     * @throws IOException stream failed
     */
    void isValidImageContent(MultipartFile file) throws IOException;

}
