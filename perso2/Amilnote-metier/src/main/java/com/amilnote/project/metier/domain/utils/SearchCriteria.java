package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator;

/**
 * A class which will help for restricting data search based on a property, a comparison operator, and values
 * @param <T> the type of the values
 * @author gralandison
 */
public class SearchCriteria<T> {

    private ComparisonOperator comparator;
    private String property;
    private T[] values;

    public SearchCriteria(String property, ComparisonOperator comparator, T... values) {
        this.property = property;
        this.comparator = comparator;
        this.values = values;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public T[] getValues() {
        return values;
    }

    public void setValues(T[] values) {
        this.values = values;
    }

    public ComparisonOperator getComparator() {
        return comparator;
    }

    public void setComparator(ComparisonOperator comparator) {
        this.comparator = comparator;
    }
}
