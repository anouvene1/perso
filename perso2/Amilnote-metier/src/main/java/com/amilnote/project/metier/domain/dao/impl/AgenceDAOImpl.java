package com.amilnote.project.metier.domain.dao.impl;


import com.amilnote.project.metier.domain.dao.AgenceDAO;
import com.amilnote.project.metier.domain.entities.Agence;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Agence dao.
 */
@Repository("agenceDAO")
public class AgenceDAOImpl extends AbstractDAOImpl<Agence> implements AgenceDAO{

    private static final Logger logger = LoggerFactory.getLogger(Agence.class);

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Agence> getReferenceClass() {
        return Agence.class;
    }

    @Override
    public List<Agence> getAllAgence() {
        return currentSession().createCriteria(Agence.class).list();
    }

    @Override
    public Agence getAgenceById(long pId) {
        Agence agence = (Agence) currentSession().get(Agence.class, pId);
        return agence;
    }
}
