/*
 * ©Amiltone 2018
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.TravailleurAsJson;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The abstract supertype AbstractTravailleur
 *
 */
@MappedSuperclass
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//TODO will: tester la serialisation/deserialisation json avec jackson
public abstract class AbstractTravailleur<JSON extends TravailleurAsJson> implements Serializable {

    /**
     * The constant PROP_NOM.
     */
    public static final String PROP_NOM = "nom";
    /**
     * The constant PROP_PRENOM.
     */
    public static final String PROP_PRENOM = "prenom";
    /**
     * The constant PROP_TEL.
     */
    public static final String PROP_TEL = "telephone";
    /**
     * The constant PROP_MAIL.
     */
    public static final String PROP_MAIL = "mail";
    /**
     * The constant PROP_PASSWORD.
     */
    public static final String PROP_PASSWORD = "password";
    /**
     * The constant PROP_STATUT.
     */
    public static final String PROP_STATUT = "statut";
    /**
     * The constant PROP_ENABLED.
     */
    public static final String PROP_ENABLED = "enabled";
    /**
     * The constant PROP_MANAGER.
     */
    public static final String PROP_MANAGER = "manager";
    /**
     * The constant PROP_POSTE.
     */
    public static final String PROP_POSTE = "poste";
    /**
     * The constant PROP_NONLOCKED.
     */
    public static final String PROP_NONLOCKED = "accountNonLocked";
    /**
     * The constant PROP_NBATTEMPTS.
     */
    public static final String PROP_NBATTEMPTS = "nbAttempts";
    /**
     * The constant PROP_ADRESS.
     */
    public static final String PROP_ADRESS = "adresse_postale";

    private static final long serialVersionUID = 6002716660940563085L;

    private Long id;
    private String nom;
    private String prenom;
    private StatutCollaborateur statut;
    private String mail;
    private String password;
    private String telephone;
    private Boolean enabled;
    private List<Mission> missions;
    private List<Collaborator> collaborateurs;
    private List<RapportActivites> rapportActivites;
    private Poste poste;
    private Boolean accountNonLocked;
    private int nbAttempts;
    private String adressePostale;
    private Civilite civilite;
    private Agency agency;

    public AbstractTravailleur() {
    }

    protected AbstractTravailleur(String pNom, String pPrenom, StatutCollaborateur pStatut, String pMail, String pPassword, String pTelephone, Boolean pEnabled, List<Mission> pMissions, List<Collaborator> collaborateurs, List<RapportActivites> pRapportActivites, Poste pPoste, Boolean pAccountNonLocked, int pNbAttempts, String pAdressePostale, Civilite civilite, Agency agency) {
        this.nom = pNom;
        this.prenom = pPrenom;
        this.statut = pStatut;
        this.mail = pMail;
        this.password = pPassword;
        this.telephone = pTelephone;
        this.enabled = pEnabled;
        this.missions = pMissions;
        this.collaborateurs = collaborateurs;
        this.rapportActivites = pRapportActivites;
        this.poste = pPoste;
        this.accountNonLocked = pAccountNonLocked;
        this.nbAttempts = pNbAttempts;
        this.adressePostale = pAdressePostale;
        this.civilite = civilite;
        this.agency = agency;
    }

    /**
     * retourne l'identifiant du collaborateur
     *
     * @return Long id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    /**
     * mise a jour de l'id du collaborateur
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * retourne le nom du collaborateur
     *
     * @return String nom
     */
    @Column(name = "nom", nullable = false)
    public String getNom() {
        return this.nom;
    }

    /**
     * mise a jour du nom du collaborateur
     *
     * @param pNom the p nom
     */
    public void setNom(String pNom) {
        this.nom = pNom;
    }

    /**
     * retourne le prenom du collaborateur
     *
     * @return String prenom
     */
    @Column(name = "prenom", nullable = false)
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * mise a jour du prenom du collaborateur
     *
     * @param pPrenom the p prenom
     */
    public void setPrenom(String pPrenom) {
        this.prenom = pPrenom;
    }

    /**
     * retourne le statut du collaborateur
     *
     * @return un objet de type StatutCollaborateur
     */
    @ManyToOne
    @JoinColumn(name = "id_statut", nullable = false)
    public StatutCollaborateur getStatut() {
        return this.statut;
    }

    /**
     * mise a jour du statut du collaborateur
     *
     * @param pStatut the p statut
     */
    public void setStatut(StatutCollaborateur pStatut) {
        this.statut = pStatut;
    }


    /**
     * retourne le mail (ici username) du collaborateur
     *
     * @return String mail
     */
    @Column(name = "mail", nullable = false)
    public String getMail() {
        return this.mail;
    }

    /**
     * mise a jour du mail du collaborateur
     *
     * @param pMail the p mail
     */
    public void setMail(String pMail) {
        this.mail = pMail;
    }

    /**
     * retourne le telephone du collaborateur
     *
     * @return String telephone
     */
    @Column(name = "telephone", nullable = false)
    public String getTelephone() {
        return this.telephone;
    }

    /**
     * muise a jour du telephone du collaborateur
     *
     * @param pTelephone the p telephone
     */
    public void setTelephone(String pTelephone) {
        this.telephone = pTelephone;
    }

    /**
     * retourne le mot de passe du collaborateur (encodage MD5)
     *
     * @return String password
     */
    @Column(name = "password", nullable = false)
    public String getPassword() {
        return this.password;
    }

    /**
     * mise a jour du mot de passe du collaborateur
     *
     * @param pPassword the p password
     */
    public void setPassword(String pPassword) {
        this.password = pPassword;
    }

    /**
     * permet de savoir si un compte utilisateur est ouvert ou non
     *
     * @return boolean boolean
     */
    @Column(name = "enabled", nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * mise a jour du statut du compte utilisateur
     *
     * @param pEnabled the p enabled
     */
    public void setEnabled(Boolean pEnabled) {
        enabled = pEnabled;
    }

    /**
     * Gets missions.
     *
     * @return the linkMissionsCollaborateurs
     */
    @OneToMany(mappedBy = "collaborateur", fetch = FetchType.LAZY)
    public List<Mission> getMissions() {
        return missions;
    }

    /**
     * Sets missions.
     *
     * @param pMissions the p missions
     */
    public void setMissions(List<Mission> pMissions) {
        missions = pMissions;
    }

    /**
     * Gets collaborateurs.
     *
     * @return the collaborateurs
     *
     * TODO erreur de conception. un collab lambda n'a aucune raison d'avoir d'autres collab!
     */
    @OneToMany(mappedBy = "manager", fetch = FetchType.LAZY)
    public List<Collaborator> getCollaborateurs() {
        return collaborateurs;
    }

    /**
     * Sets collaborateurs.
     *
     * @param collaborators the collaborateurs to set
     */
    public void setCollaborateurs(List<Collaborator> collaborators) {
        this.collaborateurs = collaborators;
    }

    /**
     * Gets rapport activites.
     *
     * @return the rapportActivites
     */
    @OneToMany(mappedBy = "collaborateur", fetch = FetchType.LAZY)
    public List<RapportActivites> getRapportActivites() {
        return rapportActivites;
    }

    /**
     * Sets rapport activites.
     *
     * @param pRapportActivites the rapportActivites to set
     */
    public void setRapportActivites(List<RapportActivites> pRapportActivites) {
        rapportActivites = pRapportActivites;
    }

    /**
     * Gets poste.
     *
     * @return the poste
     */
    @ManyToOne
    @JoinColumn(name = "id_poste", nullable = false)
    public Poste getPoste() {
        return poste;
    }

    /**
     * Sets poste.
     *
     * @param pPoste the poste to set
     */
    public void setPoste(Poste pPoste) {
        poste = pPoste;
    }

    /**
     * Gets account non locked.
     *
     * @return the accountNonLocked
     */
    @Column(name = "accountNonLocked", nullable = false)
    public Boolean getAccountNonLocked() {
        return accountNonLocked;
    }

    /**
     * Sets account non locked.
     *
     * @param accountNonLocked the accountNonLocked to set
     */
    public void setAccountNonLocked(Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    /**
     * Gets nb attempts.
     *
     * @return nbAttempts nb attempts
     */
    @Column(name = "nbAttempts", nullable = false)
    @Max(value = 5L)
    public int getNbAttempts() {
        return nbAttempts;
    }

    /**
     * Sets nb attempts.
     *
     * @param nbAttempts the nbAttempts to set
     */
    public void setNbAttempts(int nbAttempts) {
        this.nbAttempts = nbAttempts;
    }

    /**
     * Applique le nouveau mot de passe et l'encode en SHA
     *
     * @param pPassword Nouvea mot de passe
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public void setPasswordAndEncode(String pPassword) throws NoSuchAlgorithmException {
        String secret = "";
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder(secret, 185000, 512);
        this.password = pbkdf2PasswordEncoder.encode(pPassword);
    }

    /**
     * Gets adresse postale.
     *
     * @return l 'adresse postale
     */
    @Column(name = "adresse_postale")
    public String getAdressePostale() {
        return adressePostale;
    }

    /**
     * Gets civility.
     *
     * @return the civility
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_civilite")
    public Civilite getCivilite() {
        return civilite;
    }

    /**
     * set the civility
     *
     * @param civilite the civility
     */
    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    /**
     * Gets agency.
     *
     * @return the agency.
     */
    @Column(name = "id_agency")
    public Agency getAgency() {
        return agency;
    }

    /**
     * set the agency
     *
     * @param agency the agency
     */
    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    /**
     * set la date d'entree
     *
     * @param adressePostale the adresse postale
     */
    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    @Transient
    public String toString() {
        return nom.toUpperCase() + " " + prenom;
    }

    /**
     *
     * @return l'objet AsJson correspondant
     */
    @Transient
    public abstract JSON toJson();
}
