/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

/**
 * The type Pair.
 *
 * @param <A> the type parameter
 * @param <B> the type parameter
 */
public class Pair<A, B> {
    private A first = null;
    private B second = null;


    /**
     * Instantiates a new Pair.
     */
    public Pair() {
    }

    /**
     * Instantiates a new Pair.
     *
     * @param first  the first
     * @param second the second
     */
    public Pair(A first, B second) {
        setFirst(first);
        setSecond(second);
    }


    /**
     * Gets first.
     *
     * @return the first
     */
    public A getFirst() {
        return first;
    }

    /**
     * Sets first.
     *
     * @param first the first
     */
    public void setFirst(A first) {
        this.first = first;
    }

    /**
     * Gets second.
     *
     * @return the second
     */
    public B getSecond() {
        return second;
    }

    /**
     * Sets second.
     *
     * @param second the second
     */
    public void setSecond(B second) {
        this.second = second;
    }

}
