/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;


import com.amilnote.project.metier.domain.entities.Parametre;
import com.amilnote.project.metier.domain.entities.json.ParametreAsJson;

import java.util.List;

/**
 * The interface Parametre dao.
 */
public interface ParametreDAO extends LongKeyDAO<Parametre> {

    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param parametreJson the parametre json
     * @return int int
     */
    int createOrUpdateParametre(ParametreAsJson parametreJson);

    /**
     * Suppression d'un parametre
     *
     * @param pParametre the p parametre
     * @return string string
     */
    String deleteParametre(Parametre pParametre);

    /**
     * Find parametres list.
     *
     * @return La liste de tous les parametres
     */
    List<Parametre> findParametres();

}
