/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The UploadFile Object class
 * Uploader and Validator Multipart file
 */
public final class UploadFile {

    private static final Logger logger = LogManager.getLogger(UploadFile.class);

    private static final int TAILLE_TAMPON = 10240;              //10Ko
    public static final long MAX_SIZE_JUSTIF_FRAIS = 10485760;   //10Mo
    public static final long MAX_SIZE_JUSTIF_COMMAND = 10485760;   //10Mo

    public static final String ERROR_MSG_FORMAT = "Le format de votre fichier est invalide ou non autorisé.";
    public static final String ERROR_KEY_FILE = "file_error";
    public static final String ERROR_MSG_FILE_SIZE = "Le fichier que vous avez sélectionné est trop volumineux, choisissez un autre fichier ou réduisez sa taille.";
    public static final String ERROR_MSG_FILE_EMPTY = "Le fichier sélectionné est vide !";
    public static final String ERROR_MSG_CONTENT_FILE = "Le contenu du fichier est invalide";

    private long maxSize = 5242880; // default size = 5Mo
    private FileFormatEnum[] acceptFormat;

    private Map<String, String> errors = new HashMap<>();

    public UploadFile() {
    }

    public UploadFile(FileFormatEnum[] acceptFormat) {
        this.acceptFormat = acceptFormat;
    }

    public UploadFile(long maxSize, FileFormatEnum[] acceptFormat) {
        this.maxSize = maxSize;
        this.acceptFormat = acceptFormat;
    }

    /**
     * Save a file in server storage
     *
     * @param document MultipartFile fichier soumis
     * @param fullPath String full document path + nom + ext  (c:/tmp/fichier.txt)
     * @return boolean true = pas d'erreur, exception = failed
     * @throws Exception a customized message exception
     */
    public boolean storeFile(MultipartFile document, String fullPath) throws Exception {

        isValidFile(document); //throw exception if not valid size file or format mime

        String nomFichier;
        InputStream contenuFichier = null;
        try {
            nomFichier = document.getName();

            if (nomFichier != null && !nomFichier.isEmpty()) {
                contenuFichier = document.getInputStream();
            }
        } catch (IllegalStateException e) {
            logger.error(e.getMessage());
            errors.put(ERROR_KEY_FILE, ERROR_MSG_FILE_SIZE);
            throw new Exception(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
            errors.put(ERROR_KEY_FILE, "[FICHIER REFUSE]. IO : Anomalie lors de la lecture du fichier");
            throw new Exception(e.getMessage());
        }

        if (errors.isEmpty()) {
            try {
                createDirectory(fullPath);
                writeFile(contenuFichier, fullPath);
            } catch (Exception e) {
                logger.error(e.getMessage());
                errors.put(ERROR_KEY_FILE, "Anomalie lors de l'écriture du fichier sur le disque.");
            }
        }

        return errors.isEmpty();
    }

    /**
     * Write the file to server storage
     *
     * @param contenu  content file stream
     * @param fullPath the local path storage
     * @throws Exception Stream IO
     */
    private void writeFile(InputStream contenu, String fullPath) throws Exception {

        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            entree = new BufferedInputStream(contenu, TAILLE_TAMPON);
            sortie = new BufferedOutputStream(new FileOutputStream(new File(fullPath)), TAILLE_TAMPON);

            byte[] tampon = new byte[TAILLE_TAMPON];
            int longueur;
            while ((longueur = entree.read(tampon)) > 0) {
                sortie.write(tampon, 0, longueur);
            }
        } catch (Exception e) {
            logger.error("Fail to write file");
            errors.put(ERROR_KEY_FILE, "writing to server failed");
            throw new Exception("writing to server failed");
        } finally {
            try {
                sortie.close();
            } catch (IOException ignore) {
                logger.error("Fail to close upload outputStream");
            }
            try {
                entree.close();
            } catch (IOException ignore) {
                logger.error("Fail to close upload inputStream");
            }
        }
    }

    /**
     * Get file extention from Multipart file
     *
     * @param document a Multipart file
     * @return Optional String extention of file (ex: .pdf)
     */
    public static Optional<String> retrieveExtension(MultipartFile document) {
        Optional<String> optFileName = Optional.of(document.getOriginalFilename());
        Optional<String> optExtension;
        if (!optFileName.isPresent() || optFileName.get() == "") {
            optFileName = Optional.of(document.getName());
        }
        optExtension = Optional.of(optFileName.get().substring(optFileName.get().lastIndexOf('.')));
        return optExtension;
    }

    /**
     * Get MIME file type from Tika library
     *
     * @param document a Multipart file
     * @return Optional String MIME type (ex: application/pdf)
     */
    public static Optional<String> retrievetMIMEType(MultipartFile document) {
        Optional<String> optTikaMIME = Optional.empty();
        try {
            optTikaMIME = Optional.of(new Tika().detect(document.getBytes()));
        } catch (IOException e) {
            logger.error("TIKA : detector extention failed");
        }
        return optTikaMIME;
    }

    /**
     * Verify if format extention or MIME is valid for file upload
     *
     * @param document a Multipart file data
     * @param formats  a array of FileFormat form enum
     * @return true = accepted / false = not valid
     */
    public static boolean isAcceptedFormat(MultipartFile document, FileFormatEnum[] formats) {
        boolean accepted = false;
        Optional<String> optExtention = retrieveExtension(document);
        Optional<String> optTikaMIME = retrievetMIMEType(document);
        StringBuilder builder = new StringBuilder().append("[verifyImageFileContents]: Reçu fichier ")
                .append(document.getOriginalFilename())
                .append(" et timestamp: ").append(System.currentTimeMillis());
        logger.info(builder.toString());
        for (FileFormatEnum format : formats) {
            if (optExtention.isPresent() && optExtention.get().equals(format.toString()) ||
                    optTikaMIME.isPresent() && optTikaMIME.get().equals(format.toString())) {
                accepted = true;
                logger.info("[FICHIER OK]. (Raison: Extension/mime autorisé).");
                break;
            }
        }
        if (!accepted) {
            logger.info("[FICHIER REFUSE]. (Raison: Extension/mime PAS autorisé).");
        }
        return accepted;
    }

    /**
     * File upload validator
     *
     * @param document a Multipart file data
     * @throws SizeException      custom size file exception
     * @throws FormatException    custom format file exception
     * @throws FileEmptyException custom empty file exception
     */
    public void isValidFile(MultipartFile document) throws SizeException, FormatException, FileEmptyException {
        if (document.isEmpty() || document.getSize() == 0) {
            throw new FileEmptyException(ERROR_MSG_FILE_EMPTY);
        }
        if (!isAcceptedFormat(document, this.acceptFormat)) {
            throw new FormatException(ERROR_MSG_FORMAT);
        }
        if (document.getSize() > this.getMaxSize()) {
            throw new SizeException(ERROR_MSG_FILE_SIZE);
        }
    }

    /**
     * Verify if Picture file content is valid
     *
     * @param document a Multipart file data
     * @return true = valid / Exception = not valid
     * @throws IOException custom invalid content exception
     */
    public static boolean isValidImageFileContent(MultipartFile document) throws IOException {
        boolean isValid = false;
        Optional<String> mimeFile = retrievetMIMEType(document);
        if (!mimeFile.isPresent()) {
            return isValid;
        }
        if (mimeFile.get().split("/")[0].equals("image")) {
            try (InputStream input = document.getInputStream()) {
                try {
                    ImageIO.read(input).toString();
                    isValid = true;
                    logger.info("ImageIO: [FICHIER OK].");

                } catch (Exception e) {
                    logger.info("ImageIO: [FICHIER REFUSE].");
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
                throw new IOException(ERROR_MSG_CONTENT_FILE);
            }
        }
        return isValid;
    }

    /**
     * create directory
     *
     * @param fullPath path + filename + extention
     */
    private void createDirectory(String fullPath) {
        if (fullPath != null) {
            File directory = new File(fullPath).getParentFile();
            directory.mkdirs();
        }
    }

    public long getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public FileFormatEnum[] getAcceptFormat() {
        return acceptFormat;
    }

    public void setAcceptFormat(FileFormatEnum[] acceptFormat) {
        this.acceptFormat = acceptFormat;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    /**************************************************************/
    /********************** CUSTOM EXCEPTION **********************/
    /**************************************************************/

    /**
     * Custom class exception occur if file is empty
     */
    public static class FileEmptyException extends Exception {
        public FileEmptyException(String message) {
            super(message);
        }
    }

    /**
     * Custom class exception occur if file size is too bulky
     */
    public static class SizeException extends Exception {
        public SizeException(String message) {
            super(message);
        }
    }

    /**
     * Custom class exception occur if file format is not valid
     */
    public static class FormatException extends Exception {
        public FormatException(String message) {
            super(message);
        }
    }

    /**
     * Custom class exception occur if file content type img is not valid
     */
    public static class InvalidContentException extends Exception {
        public InvalidContentException(String message) {
            super(message);
        }
    }
}
