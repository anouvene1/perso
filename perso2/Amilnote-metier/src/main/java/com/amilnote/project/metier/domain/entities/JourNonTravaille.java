/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.JourNonTravailleAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Absence.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_jours_non_travailles")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class JourNonTravaille implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_DATEDEBUT.
     */
    public static final String PROP_DATEDEBUT = "dateDebut";

    /**
     * The constant PROP_DATEFIN.
     */
    public static final String PROP_DATEFIN = "dateFin";

    /**
     * The constant PROP_REFABSENCE.
     */
    public static final String PROP_REFABSENCE = "typeAbsence";

    /**
     * The constant PROP_NBJOURS.
     */
    public static final String PROP_NBJOURS = "nb_jours";

    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_SOUS_TRAITANT = "sousTraitant";

    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";

    /**
     * The constant PROP_COMMENTAIRE.
     */
    public static final String PROP_COMMENTAIRE = "commentaire";

    private static final long serialVersionUID = 4412240278282002064L;

    private Long id;

    private TypeAbsence typeAbsence;

    private Set<LinkEvenementTimesheet> linkEvenementTimesheets = new HashSet<>(0);

    private Date dateDebut;

    private Date dateFin;

    private float nbJours;

    private Etat etat;

    private Date dateSoumission;

    private Date dateValidation;

    private String commentaire;

    /* CONSTRUCTORS */

    /**
     * Instantiates a new Absence.
     *
     * @param pId                      the p id
     * @param pRefAbsence              the p ref absence
     * @param pLinkEvenementTimesheets the p link evenement timesheets
     * @param pDateDebut               the p date debut
     * @param pDateFin                 the p date fin
     * @param pNbJours                 the p nb jours
     * @param pEtat                    the p etat
     * @param pDateSoumission          the p date soumission
     * @param pDateValidation          the p date validation
     * @param pCommentaire             the p commentaire
     */
    public JourNonTravaille(Long pId, TypeAbsence pRefAbsence, Set<LinkEvenementTimesheet> pLinkEvenementTimesheets,
                            Date pDateDebut, Date pDateFin,
                            float pNbJours, Etat pEtat, Date pDateSoumission, Date pDateValidation,
                            String pCommentaire) {
        super();
        this.id = pId;
        this.typeAbsence = pRefAbsence;
        this.linkEvenementTimesheets = pLinkEvenementTimesheets;
        this.dateDebut = pDateDebut;
        this.dateFin = pDateFin;
        this.nbJours = pNbJours;
        this.etat = pEtat;
        this.dateSoumission = pDateSoumission;
        this.dateValidation = pDateValidation;
        this.commentaire = pCommentaire;

    }


    /**
     * Instantiates a new Absence.
     */
    public JourNonTravaille() {
        super();
    }

    /* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * retourne le référence de l'absence
     *
     * @return un objet de type RefAbsence
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ref_absence", nullable = false)
    public TypeAbsence getTypeAbsence() {
        return typeAbsence;
    }

    /**
     * Sets type absence.
     *
     * @param pRefAbsence the p ref absence
     */
    public void setTypeAbsence(TypeAbsence pRefAbsence) {
        this.typeAbsence = pRefAbsence;
    }

    /**
     * Gets link evenement timesheets.
     *
     * @return la liste des TimesheetLines avec cette absence
     */
    @OneToMany(mappedBy = "absence")
    public Set<LinkEvenementTimesheet> getLinkEvenementTimesheets() {
        return linkEvenementTimesheets;
    }

    /**
     * Sets link evenement timesheets.
     *
     * @param pLinkEvenementTimesheets the p link evenement timesheets
     */
    public void setLinkEvenementTimesheets(Set<LinkEvenementTimesheet> pLinkEvenementTimesheets) {
        this.linkEvenementTimesheets = pLinkEvenementTimesheets;
    }

    /**
     * Gets date debut.
     *
     * @return dateDebut date debut
     */
    @Column(name = "date_debut", nullable = false)
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param dateDebut the date debut
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return dateFin date fin
     */
    @Column(name = "date_fin", nullable = false)
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param dateFin the date fin
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets nb jours.
     *
     * @return the nb jours
     */
    @Column(name = "nb_jours", nullable = false)
    public float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param pNbJours the p nb jours
     */
    public void setNbJours(float pNbJours) {
        nbJours = pNbJours;
    }

    /**
     * retourne l'état de l'absence
     *
     * @return un objet de type StatutCollaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_etat", nullable = false)
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(Etat pEtat) {
        etat = pEtat;
    }

    /**
     * Gets date soumission.
     *
     * @return the dateSoumission
     */
    @Column(name = "date_soumission", nullable = false)
    public Date getDateSoumission() {
        return dateSoumission;
    }

    /**
     * Sets date soumission.
     *
     * @param pDateSoumission the dateSoumission to set
     */
    public void setDateSoumission(Date pDateSoumission) {
        dateSoumission = pDateSoumission;
    }

    /**
     * Gets date validation.
     *
     * @return the dateValidation
     */
    @Column(name = "date_validation", nullable = true)
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param pDateValidation the dateValidation to set
     */
    public void setDateValidation(Date pDateValidation) {
        dateValidation = pDateValidation;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    @Column(name = "commentaire", nullable = true)
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the commentaire to set
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }


    /**
     * Calcul nb jours float.
     *
     * @return the float
     */
    public float calculNbJours() {
        float nb = (float) this.linkEvenementTimesheets.size();
        return (float) (nb / 2.0);
    }

    /**
     * Calcul first or last date date time.
     *
     * @param firstOrLast the first or last
     * @return the date time
     */
    public DateTime calculFirstOrLastDate(String firstOrLast) {
        Set<LinkEvenementTimesheet> listEvents = this.getLinkEvenementTimesheets();

        DateTime date = null;

        for (LinkEvenementTimesheet event : listEvents) {
            Boolean bool;
            DateTime tempDate = event.calculDateStart();
            if (firstOrLast.equals("first")) {
                bool = tempDate.isBefore(date);
            } else if (firstOrLast.equals("last")) {
                bool = tempDate.isAfter(date);
            } else {
                return null;
            }

            if (date == null || bool) {
                date = tempDate;
            }
        }
        return date;
    }

    /**
     * To json absence as json.
     *
     * @return the absence as json
     */
    public JourNonTravailleAsJson toJson() {
        return new JourNonTravailleAsJson(this);
    }

}
