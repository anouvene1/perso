/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Frais.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_frais")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Frais implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    /**
     * The constant PROP_NOMBREKM.
     */
    public static final String PROP_NOMBREKM = "nombreKm";
    /**
     * The constant PROP_LINKFRAISTFC.
     */
    public static final String PROP_LINKFRAISTFC = "linkFraisTFC";
    /**
     * The constant PROP_LINKEVTTS.
     */
    public static final String PROP_LINKEVTTS = "linkEvenementTimesheet";
    /**
     * The constant PROP_COMMENTAIRE.
     */
    public static final String PROP_COMMENTAIRE = "commentaire";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant PROP_JUSTIF.
     */
    public static final String PROP_JUSTIF = "justif";
    /**
     * The constant PROP_TYPEFRAIS.
     */
    public static final String PROP_TYPEFRAIS = "typeFrais";
    /**
     * The constant PROP_DEMANDEDEPLACEMENT.
     */
    public static final String PROP_DEMANDEDEPLACEMENT = "demandeDeplacement";
    /**
     * The constant PROP_JUSTIFS.
     */
    public static final String PROP_JUSTIFS = "justifs";
    /**
     * The constant PROP_NBNUIT.
     */
    public static final String PROP_NBNUIT = "nbNuit";
    /**
     * The constant PROP_NATIONAL.
     */
    public static final String PROP_NATIONAL = "national";
    /**
     * The constant PROP_FRAIS_FORFAIT.
     */
    public static final String PROP_FRAIS_FORFAIT = "fraisForfait";
    /**
     * The constant PROP_TYPEAVANCE.
     */
    public static final String PROP_TYPEAVANCE = "typeAvance";
    /**
     * The constant PROP_FRAISREMPLACEFORFAIT.
     */
    public static final String PROP_FRAISREMPLACEFORFAIT = "FRF";
    /**
     * The constant PROP_FRAISADDITIONFORFAIT.
     */
    public static final String PROP_FRAISADDITIONFORFAIT = "FAF";
    /**
     * The constant FRAIS_VOITUREPERSO.
     */
    public static final String FRAIS_VOITUREPERSO = "VO";
    /**
     * The constant FRAIS_TRAIN.
     */
    public static final String FRAIS_TRAIN = "TRI";
    /**
     * The constant FRAIS_TRANSPORT.
     */
    public static final String FRAIS_TRANSPORT = "TRS";
    /**
     * The constant FRAIS_AVION.
     */
    public static final String FRAIS_AVION = "AV";
    /**
     * The constant FRAIS_LOCATION.
     */
    public static final String FRAIS_LOCATION = "LOC";
    /**
     * The constant FRAIS_ESSENCE.
     */
    public static final String FRAIS_ESSENCE = "ES";
    /**
     * The constant FRAIS_PEAGE.
     */
    public static final String FRAIS_PEAGE = "PE";
    /**
     * The constant FRAIS_PARKING.
     */
    public static final String FRAIS_PARKING = "PA";
    /**
     * The constant FRAIS_HOTEL.
     */
    public static final String FRAIS_HOTEL = "HO";
    /**
     * The constant FRAIS_RESTOMIDI.
     */
    public static final String FRAIS_RESTOMIDI = "REM";
    /**
     * The constant FRAIS_RESTOSOIR.
     */
    public static final String FRAIS_RESTOSOIR = "RES";
    /**
     * The constant FRAIS_AVANCE.
     */
    public static final String FRAIS_AVANCE = "ASF";
    /**
     * The constant FRAIS_TAXI.
     */
    public static final String FRAIS_TAXI = "TAX";
    /**
     * The constant FRAIS_TELEPHONE.
     */
    public static final String FRAIS_TELEPHONE = "TEL";
    /**
     * The constant FRAIS_DIVERS.
     */
    public static final String FRAIS_DIVERS = "DIV";
    /**
     * The constant FRAIS_ABO_TRANSPORT_COMMUN.
     */
    public static final String FRAIS_ABO_TRANSPORT_COMMUN = "ABT";
    private static final long serialVersionUID = 7709497639289263975L;
    private Long id;
    private BigDecimal montant;
    private Integer nombreKm;
    private TypeFrais typeFrais;
    private List<LinkFraisTFC> linkFraisTFC;
    private LinkEvenementTimesheet linkEvenementTimesheet;
    private String commentaire;
    private Etat etat;
    private DemandeDeplacement demandeDeplacement = null;
    private List<Justif> justifs = new ArrayList<Justif>();
    private Boolean national;
    private Integer nbNuit;
    private String typeAvance;
    private String fraisForfait;
    private Date dateValidation;
    private Date dateSolde;

    /**
     * CONSTRUCTORS
     *
     * @param pMontant                 the p montant
     * @param pLinkFraisTypeFraisCarac the p link frais type frais carac
     * @param pLinkEvenementTimesheet  the p link evenement timesheet
     * @param pCommentaire             the p commentaire
     * @param pEtat                    the p etat
     * @param pJustif                  the p justif
     * @param pDemandeDeplacement      the p demande deplacement
     * @param pTypeFrais               the p type frais
     * @param pJustifs                 the p justifs
     * @param pNational                the p national
     * @param nombreDeKm               the nombre de km
     * @param nombreNuit               the nombre nuit
     * @param pTypeAvance              the p type avance
     * @param pFraisForfait            the p frais forfait
     */
    public Frais(BigDecimal pMontant, List<LinkFraisTFC> pLinkFraisTypeFraisCarac, LinkEvenementTimesheet pLinkEvenementTimesheet,
                 String pCommentaire, Etat pEtat, String pJustif, DemandeDeplacement pDemandeDeplacement,
                 TypeFrais pTypeFrais, List<Justif> pJustifs, Boolean pNational, Integer nombreDeKm, Integer nombreNuit, String pTypeAvance, String pFraisForfait) {

        super();
        montant = pMontant;
        linkFraisTFC = pLinkFraisTypeFraisCarac;
        linkEvenementTimesheet = pLinkEvenementTimesheet;
        commentaire = pCommentaire;
        etat = pEtat;
        demandeDeplacement = pDemandeDeplacement;
        typeFrais = pTypeFrais;
        justifs = pJustifs;
        national = pNational;
        nombreKm = nombreDeKm;
        nbNuit = nombreNuit;
        typeAvance = pTypeAvance;
        fraisForfait = pFraisForfait;

    }

    /**
     * Instantiates a new Frais.
     */
    public Frais() {
        super();
    }

	/*
      * GETTERS AND SETTERS
	 */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "montant", nullable = true)
    public BigDecimal getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the p montant
     */
    public void setMontant(BigDecimal pMontant) {
        montant = pMontant;
    }

    /**
     * Gets link frais tfc.
     *
     * @return the link frais tfc
     */
    @OneToMany(mappedBy = "frais")
    public List<LinkFraisTFC> getLinkFraisTFC() {
        return linkFraisTFC;
    }

    /**
     * Sets link frais tfc.
     *
     * @param pLinkFraisTFC the p link frais tfc
     */
    public void setLinkFraisTFC(List<LinkFraisTFC> pLinkFraisTFC) {
        linkFraisTFC = pLinkFraisTFC;
    }

    /**
     * Gets link evenement timesheet.
     *
     * @return the link evenement timesheet
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_link_evenement_timesheet", nullable = false)
    public LinkEvenementTimesheet getLinkEvenementTimesheet() {
        return linkEvenementTimesheet;
    }

    /**
     * Sets link evenement timesheet.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     */
    public void setLinkEvenementTimesheet(LinkEvenementTimesheet pLinkEvenementTimesheet) {
        linkEvenementTimesheet = pLinkEvenementTimesheet;
    }


    /**
     * retourne l'état du frais
     *
     * @return un objet de type StatutCollaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_etat", nullable = false)
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(Etat pEtat) {
        etat = pEtat;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    @Column(name = "commentaire", nullable = true)
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the p commentaire
     */
    public void setCommentaire(String pCommentaire) {
        this.commentaire = pCommentaire;
    }


    /**
     * retourne l'état du frais
     *
     * @return un objet de type StatutCollaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dem_deplacement", nullable = true)
    public DemandeDeplacement getDemandeDeplacement() {
        return demandeDeplacement;
    }

    /**
     * Sets demande deplacement.
     *
     * @param pDemandeDeplacement the demandeDeplacement to set
     */
    public void setDemandeDeplacement(DemandeDeplacement pDemandeDeplacement) {
        demandeDeplacement = pDemandeDeplacement;
    }

    /**
     * Gets type frais.
     *
     * @return the typeFrais
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_type_frais", nullable = true)
    public TypeFrais getTypeFrais() {
        return typeFrais;
    }

    /**
     * Sets type frais.
     *
     * @param pTypeFrais the typeFrais to set
     */
    public void setTypeFrais(TypeFrais pTypeFrais) {
        typeFrais = pTypeFrais;
    }


    /**
     * Gets justifs.
     *
     * @return the justifs
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ami_link_justif_frais", joinColumns = {
        @JoinColumn(name = "id_frais", nullable = false, updatable = false)},
        inverseJoinColumns = {@JoinColumn(name = "id_justif",
            nullable = false, updatable = false)})
    public List<Justif> getJustifs() {
        return justifs;
    }

    /**
     * Sets justifs.
     *
     * @param pJustifs the justifs to set
     */
    public void setJustifs(List<Justif> pJustifs) {
        justifs = pJustifs;
    }

    /**
     * Gets national.
     *
     * @return the national
     */
    @Column(name = "national", nullable = false)
    public Boolean getNational() {
        return national;
    }

    /**
     * Sets national.
     *
     * @param pNational the p national
     */
    public void setNational(Boolean pNational) {
        national = pNational;
    }

    /**
     * Gets nombre km.
     *
     * @return the nombre km
     */
    @Column(name = "nb_km", nullable = true)
    public Integer getNombreKm() {
        return nombreKm;
    }

    /**
     * Sets nombre km.
     *
     * @param pNombreKm the p nombre km
     */
    public void setNombreKm(Integer pNombreKm) {
        nombreKm = pNombreKm;
    }

    /**
     * Gets nb nuit.
     *
     * @return the nb nuit
     */
    @Column(name = "nb_nuit", nullable = true)
    public Integer getNbNuit() {
        return nbNuit;
    }

    /**
     * Sets nb nuit.
     *
     * @param pNbNuit the p nb nuit
     */
    public void setNbNuit(Integer pNbNuit) {
        nbNuit = pNbNuit;
    }

    /**
     * Gets type avance.
     *
     * @return the type avance
     */
    @Column(name = "type_avance", nullable = true)
    public String getTypeAvance() {
        return typeAvance;
    }

    /**
     * Sets type avance.
     *
     * @param pTypeAvance the p type avance
     */
    public void setTypeAvance(String pTypeAvance) {
        typeAvance = pTypeAvance;
    }

    /**
     * Gets frais forfait.
     *
     * @return the frais forfait
     */
    @Column(name = "fraisForfait", nullable = true)
    public String getFraisForfait() {
        return fraisForfait;
    }

    /**
     * Sets frais forfait.
     *
     * @param pFraisForfait the p frais forfait
     */
    public void setFraisForfait(String pFraisForfait) {
        fraisForfait = pFraisForfait;
    }

    /**
     * To j son frais as json.
     *
     * @return the frais as json
     */
    public FraisAsJson toJSon() {
        return new FraisAsJson(this);
    }

    /**
     * Suppr justif boolean.
     *
     * @param pJustif the p justif
     * @return the boolean
     */
    public boolean supprJustif(Justif pJustif) {
        return this.justifs.remove(pJustif);
    }

    /**
     * Add justif boolean.
     *
     * @param pJustif the p justif
     * @return the boolean
     */
    public boolean addJustif(Justif pJustif) {
        return this.justifs.add(pJustif);
    }

    /**
     * Gets date validation.
     *
     * @return the dateValidation
     */
    @Column(name = "dateValidation", nullable = true)
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param dateValidation the dateValidation to set
     */
    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    /**
     * Gets date solde.
     *
     * @return the dateSolde
     */
    @Column(name = "dateSolde", nullable = true)
    public Date getDateSolde() {
        return dateSolde;
    }

    /**
     * Sets date solde.
     *
     * @param dateSolde the dateSolde to set
     */
    public void setDateSolde(Date dateSolde) {
        this.dateSolde = dateSolde;
    }

}
