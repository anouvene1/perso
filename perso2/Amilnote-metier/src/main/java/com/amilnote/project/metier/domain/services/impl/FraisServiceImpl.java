/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.dao.FraisDAO;
import com.amilnote.project.metier.domain.dao.impl.EtatDAOImpl;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.entities.json.JustifAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.NamingException;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Frais service.
 */
@Service("fraisService")
public class FraisServiceImpl extends AbstractServiceImpl<Frais, FraisDAO> implements FraisService {

    private static final Logger logger = LogManager.getLogger(FraisServiceImpl.class);

    public static final String ADVANCE_VALIDATION_OK = "Validation de l'avance de frais effectuée avec succès";
    public static final String ADVANCE_VALIDATION_NOK = "Refus de l'avance de frais effectué avec succès";
    public static final String ADVANCE_VALIDATION_PAYED = "Avance sur frais soldée avec succès";


    @Autowired
    private FraisDAO fraisDAO;

    @Autowired
    private EtatDAOImpl etatDAO;

    @Autowired
    private TypeFraisService typeFraisService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private CollaboratorDAO collaboratorDAO;

    @Autowired
    private MailService mailService;

    @Autowired
    private FileService fileService;

    /**
     * {@inheritDoc}
     */
    @Override
    public FraisDAO getDAO() {
        return fraisDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveNewFrais(MultipartFile proofFile) throws Exception {
        String fileName;
        if (!proofFile.isEmpty()) {
            fileName = proofFile.getOriginalFilename();
            String extJustif = fileName.substring(fileName.lastIndexOf("."));
            String cheminJustif = null;
            try {
                cheminJustif = Parametrage.getContext("dossierFrais");
            } catch (NamingException e) {
                logger.error("[save new fees] context parameter naming error", e);
            }
            int idFrais = getDAO().getAutoIncrement("ami_frais");
            fileName = "FRAIS_" + idFrais + extJustif;
            fileService.uploadFile(proofFile, cheminJustif + fileName, UploadFile.MAX_SIZE_JUSTIF_FRAIS, FileFormatEnum.values());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteFrais(List<Long> pListFraisToDelete) {
        return fraisDAO.deleteFrais(pListFraisToDelete);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String modifyFrais(Long pIdFrais, BigDecimal pMontant, LinkTypeFraisCarac pLinkTFraisCarac, LinkEvenementTimesheet pLinkEvtTS,
                              BigDecimal pARembourser, String pCommentaire, Double pNbKm, String pJustif) {

        // EPE tmp
        LinkFraisTFC pLinkFraisTFC = new LinkFraisTFC();

        return fraisDAO.modifyFrais(pIdFrais, pMontant, pLinkFraisTFC, pLinkEvtTS, pCommentaire, pJustif, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<String, InputStream> getPictureForFrais(Long pIdFrais) {
        return fraisDAO.getPictureForFrais(pIdFrais);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object[]> findAllByMissionOrderByDateCriteria(Mission pMission, int pMois, int pAnnee) {
        return fraisDAO.findByMissionCriteria(pMission, pMois, pAnnee);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object[]> findAllByMissionOrderByDate(Mission pMission, int pMois, int pAnnee) {
        return fraisDAO.findByMission(pMission, pMois, pAnnee);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object[]> findAllByMissionOrderByDateCriteria(Mission pMission, DateTime date) {
        return fraisDAO.findByMissionCriteria(pMission, date);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<byte[]> findJustifsByMission(Mission pMission) {
        DateTime lFirstDayOnThisMonth = new DateTime(DateTime.now().withDayOfMonth(1));
        return fraisDAO.findJustifsByMission(pMission, lFirstDayOnThisMonth);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String modifyEtatFrais(List<Frais> pListFrais) {
        return fraisDAO.modifyEtatFrais(pListFrais);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Frais> findFraisAvanceDeFrais() {
        List<Frais> avanceFrais = new ArrayList<>();
        List<Frais> allFrais = findAll(null);

        for (Frais frais : allFrais) {
            if (TypeFrais.TYPEFRAIS_AVANCESURFRAIS.equals(frais.getTypeFrais().getCode())
                    && !(Etat.ETAT_BROUILLON_CODE.equals(frais.getEtat().getCode()) || Etat.ETAT_ANNULE.equals(frais.getEtat().getCode()))) {
                avanceFrais.add(frais);
            }
        }
        return avanceFrais;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Frais> findFraisAvanceDeFraisSO() {
        List<Frais> avanceFrais = new ArrayList<>();
        List<Frais> allFrais = findAll(null);

        for (Frais frais : allFrais) {
            if (TypeFrais.TYPEFRAIS_AVANCESURFRAIS.equals(frais.getTypeFrais().getCode())
                    && (Etat.ETAT_SOUMIS_CODE.equals(frais.getEtat().getCode()))) {
                avanceFrais.add(frais);
            }
        }
        return avanceFrais;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Frais> getAllFraisByMonth(DateTime pDateVoulue) {
        return fraisDAO.getAllFraisByMonth(pDateVoulue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FraisAsJson> getFraisByCollaborator(Collaborator collaborator) {

        List<Frais> lListFrais = fraisDAO.getFraisByCollaborator(collaborator);
        List<FraisAsJson> lListFraisAsJson = new ArrayList<>();
        FraisAsJson tmpFraisAsJSon;


        for (Frais tmpFrais : lListFrais) {
            tmpFraisAsJSon = tmpFrais.toJSon();
            tmpFraisAsJSon.setDateEvenement(tmpFrais.getLinkEvenementTimesheet().getDate());
            tmpFraisAsJSon.setTypeFrais(tmpFrais.getTypeFrais().getCode());
            tmpFraisAsJSon.setMission(tmpFrais.getLinkEvenementTimesheet().getMission().getMission());
            List<Justif> listJustif = tmpFrais.getJustifs();
            List<JustifAsJson> listJustifJson = new ArrayList<>();
            for (Justif tmpJustif : listJustif) {
                listJustifJson.add(tmpJustif.toJson());
            }
            tmpFraisAsJSon.setJustifs(listJustifJson);
            lListFraisAsJson.add(tmpFraisAsJSon);
        }

        return lListFraisAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FraisAsJson> getFraisBOByCollaborator(Collaborator collaborator) {
        List<FraisAsJson> lListFrais = getFraisByCollaborator(collaborator);
        List<FraisAsJson> lListFraisEtatBO = new ArrayList<>();

        // --- Récupération d'uniquement tous les frais à l'état brouillon.
        for (FraisAsJson tempFraisAsJson : lListFrais) {
            if (tempFraisAsJson.getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE)) {
                lListFraisEtatBO.add(tempFraisAsJson);
            }
        }
        return lListFraisEtatBO;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createOrUpdateFrais(FraisAsJson pFraisAsJson,
                                      Mission pMission) {

        if (null != pFraisAsJson) {
            BigDecimal tauxRemboursement50 = new BigDecimal(Float.toString(2));
            Frais lFrais;
            DateTime dateFrais = new DateTime(pFraisAsJson.getDateEvenement());
            TypeFrais typeFrais = typeFraisService.getTypeFraisByCode(pFraisAsJson.getTypeFrais());
            List<LinkEvenementTimesheet> lListEvent = linkEvenementTimesheetService.findDaysMissionPeriod(pMission, dateFrais, dateFrais);

            if (0 == lListEvent.size()) {
                return "Impossible de créer le frais. La date doit correspondre à la mission séléctionnée.";
            }

            // --- Frais existant
            if (null != pFraisAsJson.getId()) {
                lFrais = get(pFraisAsJson.getId());
                if (null != pFraisAsJson.getCommentaire()) {
                    lFrais.setCommentaire(pFraisAsJson.getCommentaire());
                }
                if (null != pFraisAsJson.getEtat()) {
                    lFrais.setEtat(etatService.getEtatByCode(pFraisAsJson.getEtat().getCode()));
                }
                if (null != pFraisAsJson.getMontant()) {
                    if (pFraisAsJson.getTypeFrais().equals(TypeFrais.TYPEFRAIS_ABONNEMENTTRANSPORTS))
                        lFrais.setMontant(pFraisAsJson.getMontant().divide(tauxRemboursement50));
                    else lFrais.setMontant(pFraisAsJson.getMontant());
                }
                if (null != pFraisAsJson.getNational()) {
                    lFrais.setNational(pFraisAsJson.getNational());
                }
                if (null != pFraisAsJson.getNombreKm()) {
                    lFrais.setNombreKm(pFraisAsJson.getNombreKm());
                }
                if (null != pFraisAsJson.getTypeFrais()) {
                    lFrais.setTypeFrais(typeFrais);
                }
                if (null != pFraisAsJson.getNbNuit()) {
                    lFrais.setNbNuit(pFraisAsJson.getNbNuit());
                }
                if (null != pFraisAsJson.getTypeAvance() && ("on".equals(pFraisAsJson.getTypeAvance()) || "PE".equals(pFraisAsJson.getTypeAvance()))) {
                    lFrais.setTypeAvance("PE");
                } else {
                    lFrais.setTypeAvance("PO");
                }
                if (null != pFraisAsJson.getDateValidation()) {
                    lFrais.setDateValidation(pFraisAsJson.getDateValidation());
                }
                if (null != pFraisAsJson.getDateSolde()) {
                    lFrais.setDateSolde(pFraisAsJson.getDateSolde());
                }
                lFrais.setFraisForfait(pFraisAsJson.getFraisForfait());
            }
            // --- Nouveau Frais
            else {
                String typedAvance = "PO";
                if ("on".equals(pFraisAsJson.getTypeAvance())) {
                    typedAvance = "PE";
                }
// si le type est "abonnement transport en commun il n'est remboursé qu'à moitié (arrondi au centime supperieur en cas de nombre de centimes impair)
                BigDecimal montant;
                if (pFraisAsJson.getTypeFrais().equals("ABT")) {
                    montant = pFraisAsJson.getMontant().divide(BigDecimal.valueOf(2));
                    montant = montant.setScale(2, BigDecimal.ROUND_UP);
                } else {
                    montant = pFraisAsJson.getMontant();
                }
                String fraisForfait = pFraisAsJson.getFraisForfait();
                lFrais = new Frais(montant, null, lListEvent.get(0), pFraisAsJson.getCommentaire(),
                        etatService.getEtatByCode(Etat.ETAT_BROUILLON_CODE), null, null, typeFrais, null, pFraisAsJson.getNational(), pFraisAsJson.getNombreKm(),
                        pFraisAsJson.getNbNuit(), typedAvance, fraisForfait);

            }

            // --- Vérification présence de timesheet
            if (0 < lListEvent.size()) {
                lFrais.setLinkEvenementTimesheet(lListEvent.get(0));

                Session session = fraisDAO.getCurrentSession();
                Transaction transaction = session.beginTransaction();

                // --- Sauvegarde et Commit de la mise à jour
                session.save(lFrais);
                session.flush();
                transaction.commit();

                // dans le cas d'une création, penser à retourner dans l'objet en entrée l'id de l'objet créé.
                if (pFraisAsJson.getId() == null) {
                    pFraisAsJson.setId(lFrais.getId());
                }

            } else {
                // --- Attention : Si mission existante mais aucun timesheet de
                // positionner dessus un frais ne peut être affecté -> Exception
                return "Erreur lors de l'ajout/modification du frais";
            }

        }
        return "Ajout/Modification effectué avec succès. Pensez à appuyer sur le bouton \"Soumettre\" afin que celle-ci soit soumise";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void soumissionDemandeAvance(Long pIdFrais) throws Exception {
        Frais frais = get(pIdFrais);
        Etat etat = new Etat();
        etat.setCode(Etat.ETAT_SOUMIS_CODE);
        etat.setId(2);
        frais.setEtat(etat);
        Session session = fraisDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        session.save(frais);
        session.flush();
        transaction.commit();

        mailService.sendMailAdvanceSubmit(frais);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void supprJustificatifToFrais(Long pIdFrais, Justif pJustif) throws Exception {
        if (null != pIdFrais && null != pJustif) {
            Frais frais = get(pIdFrais);

            if (null != pJustif && null != frais) {
                if (!frais.supprJustif(pJustif)) {
                    throw new Exception("Une erreur est survenue lors de l'ajout d'un justificatif au frais en cours");
                }
            }
            Session session = fraisDAO.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // --- Sauvegarde et Commit de la mise à jour
            session.save(frais);
            session.flush();
            transaction.commit();

        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Integer findNbRepasFraisMidi(Mission pMission, DateTime dateDeb, DateTime dateFin) {
        Session session = fraisDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String requete = "select count(*) AS nb_repas from Frais " +
                "where id_link_evenement_timesheet in ( " +
                "select id from LinkEvenementTimesheet " +
                "where id_mission in (select id from Mission where mission='" + pMission.getMission() + "') " +
                "and date >= '" + dateDeb + "' " +
                "and date <= '" + dateFin + "') " +
                "and id_type_frais in (select id from TypeFrais where code = 'REM')";

        Query query = session.createQuery(requete);
        List result = query.list();
        Long nbRepasMidi = (Long) result.get(0);

        transaction.commit();

        return nbRepasMidi.intValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Boolean updateFraisEnCours(Collaborator collaborator, DateTime date, Etat etat) {
        return fraisDAO.updateFraisEnCours(collaborator, date, etat);
    }

    /**
     * {@inheritDoc}
     */
    //TODO reecrire cette methode correctement
    @Override
    public List<Frais> findFraisByJustif(Justif justif) {
        List<Frais> lstFrais = new ArrayList<>();
        Session session = fraisDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        //TODO PAS DE REQUETE SQL DANS UN SERVICE!!!!! ON A LES DAOs POUR CA!!
        //TODO PAS DE CONCATENATION DE CHAINE DANS LES REQUETES SQL!!
        String requete = "select id from Frais " +
                "where id in ( " +
                "select frais from LinkFraisJustif " +
                "where id_justif=" + justif.getId() + " " +
                ")";
        Query query = session.createQuery(requete);
        transaction.commit();
        session.flush();
        try {
            List<Long> result = query.list();
            int size = result.size();
            int i = 0;
            while (i < size) {
                //TODO POURQUOI AVOIR FAIT EN 2 TEMPS??!!
                Frais frais = fraisDAO.load(result.get(i));
                lstFrais.add(frais);
                i++;
            }
        } catch (HibernateException e) {
            return null;
        }

        return lstFrais;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getFileASFbyId(Long pId) {
        // --- Récupération du frais
        Frais lFrais = this.get(pId);
        if (null == lFrais) {
            logger.warn("[get file fee] fee for id {} does not exist", pId);
            return null;
        }
        String[] paramsNomFichier = {
                Long.toString(pId),
                lFrais.getLinkEvenementTimesheet().getMission().getCollaborateur().getNom(),
        };
        // --- Récupération du fichier PDF
        String nomFichier = properties.get("pdf.nomFichierASF", paramsNomFichier);
        File tmpFile = null;
        try {
            tmpFile = new File(Parametrage.getContext("dossierPDFAVANCEFRAIS") + nomFichier);
        } catch (NamingException e) {
            logger.error("[get file fee] context parameter naming error", e);
        }

        // --- Vérification de l'existance du fichier
        if (!tmpFile.exists()) {
            return null;
        }
        return tmpFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String actionValidationASF(Long pIdFrais, String pCodeEtat, String pCommentaire, Date pDateValidation) throws Exception {
        String msgRetour;
        Frais frais = this.getDAO().get(pIdFrais);
        Etat newEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pCodeEtat);
        if (null == frais) {
            throw new Exception("L'id du frais (" + pIdFrais + ") est inconnu");
        }

        if (null == newEtat) {
            throw new Exception("Ce code etat (" + pCodeEtat + ") est inconnu. ");
        }


        //Liste des destinataires
        List<CollaboratorAsJson> listDestinataires = new ArrayList<>();
        Mission mission = frais.getLinkEvenementTimesheet().getMission();
        Collaborator collab = mission.getCollaborateur();
        listDestinataires.add(collab.toJson());

        // Paramètres donnés à properties (va remplacer les {n} dans le message)
        String[] paramsForObjProperties = {};

        String[] paramsForMsgProperties = {
                frais.getMontant().toString(),
                (pCommentaire != null ? pCommentaire : "Aucun commentaire")
        };

        // --- Création de la session et de la transaction
        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();


        switch (newEtat.getCode()) {
            //cas soumis --> validé
            case Etat.ETAT_VALIDE_CODE:

                frais.setEtat(newEtat);
                frais.setDateValidation(pDateValidation);

                mailService.sendMailAdvanceValidation(newEtat.getCode(), frais, pCommentaire);
                msgRetour = ADVANCE_VALIDATION_OK;

                break;

            //cas soumis --> refusé
            case Etat.ETAT_REFUSE:

                Etat br = new Etat();
                br.setCode(Etat.ETAT_BROUILLON_CODE);
                br.setEtat("Brouillon");
                br.setId(1);
                frais.setEtat(br);

                mailService.sendMailAdvanceValidation(newEtat.getCode(), frais, pCommentaire);

                msgRetour = ADVANCE_VALIDATION_NOK;


                break;

            //cas validé --> soldé
            case Etat.ETAT_SOLDE:

                frais.setEtat(newEtat);
                frais.setDateValidation(pDateValidation);

                mailService.sendMailAdvanceValidation(newEtat.getCode(), frais, pCommentaire);

                msgRetour = ADVANCE_VALIDATION_PAYED;

                break;
            default:
                msgRetour = "";
        }

        session.flush();
        transaction.commit();
        // --- Fin de la session et commit des changements
        return msgRetour;
    }
}
