package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.OutilsInterne;

public class OutilsInterneAsJson {

    private long id;
    private String nom;
    private String code;
    private int etat;

    public OutilsInterneAsJson() {
    }

    public OutilsInterneAsJson(OutilsInterne pOutilsInterne) {
        this.id = pOutilsInterne.getId();
        this.nom = pOutilsInterne.getNom();
        this.code = pOutilsInterne.getCode();
        this.etat = pOutilsInterne.getEtat();
    }

    public long getId() {
        return id;
    }

    public void setId(long pId) {
        this.id = pId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        this.nom = pNom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String pCode) {
        this.code = pCode;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int pEtat) {
        this.etat = pEtat;
    }
}
