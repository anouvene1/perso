/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.TypeMissionDAO;
import com.amilnote.project.metier.domain.entities.TypeMission;
import org.springframework.stereotype.Repository;

/**
 * The type Type mission dao.
 */
@Repository("typeMissionDAO")
public class TypeMissionDAOImpl extends AbstractDAOImpl<TypeMission> implements TypeMissionDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<TypeMission> getReferenceClass() {
        return TypeMission.class;
    }
}
