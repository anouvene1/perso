package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.LongKeyDAO;
import com.amilnote.project.metier.domain.dao.StatutCollaborateurDAO;
import com.amilnote.project.metier.domain.dao.TravailleurDAO;
import com.amilnote.project.metier.domain.entities.AbstractTravailleur;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.TravailleurAsJson;
import com.amilnote.project.metier.domain.services.AbstractTravailleurService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Travailleur service.
 */
public abstract class TravailleurServiceImpl<T extends AbstractTravailleur<JSON>, DAO extends LongKeyDAO<T>, JSON extends TravailleurAsJson>
    extends AbstractServiceImpl<T, DAO>
    implements AbstractTravailleurService<T, JSON> {

    @Autowired
    private TravailleurDAO<T> travailleurDAO;

    @Autowired
    private StatutCollaborateurDAO statutCollaboratorDAO;

    /**
     * {@linkplain CollaboratorService#findByMail(String)}
     */
    @Override
    public T findByMail(String pMail) {
        return travailleurDAO.findUniqEntiteByProp(Collaborator.PROP_MAIL, pMail);
    }

    /**
     * {@linkplain CollaboratorService#findByUserNameAndPassword(String, String)}
     */
    @Override
    public T findByUserNameAndPassword(String pMail, String pPassword) {
        return travailleurDAO.findByUsernameAndPassword(pMail, pPassword);
    }

    /**
     * {@inheritDoc}
     */
    public abstract List<T> findAllOrderByNomAsc();

//    /**
//     * {@linkplain CollaborateurService#findAllOrderByNomAscWithoutADMIN()}
//     */
//    @Override
//    public List<T> findAllOrderByNomAscWithoutADMIN() {
//        //without admin, without disabled
//        return travailleurDAO.findCollaborators(false, false);
//    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsForAbsenceMonthAsJson(Date)}
     */
    @Override
    public List<JSON> findCollaboratorsForAbsenceMonthAsJson(Date pDate) {
        List<T> tmpListCollaborator = travailleurDAO.findCollaboratorsForAbsenceMonth(pDate);
        List<JSON> tmpListCollaboratorAsJson = new ArrayList<>(tmpListCollaborator.size());

        for (T tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsForFraisAnnuelAsJson(int)}
     */
    @Override
    public List<JSON> findCollaboratorsForFraisAnnuelAsJson(int yearFrais) {
        List<T> tmpListCollaborator = travailleurDAO.findCollaboratorsForFraisAnnuel(yearFrais);
        List<JSON> tmpListCollaboratorAsJson = new ArrayList<>();

        for (T tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public T findCollaboratorByName(String firstname, String lastname) {
        return travailleurDAO.findCollaboratorByName(firstname, lastname);
    }

//    /**
//     *{@linkplain CollaborateurService#findCollaboratorsByResponsable(Collaborator)}
//     */
//    @Override
//    public List<T> findCollaboratorsByResponsable(Collaborator responsable) {
//        return travailleurDAO.findCollaboratorsByResponsable(responsable);
//    }

    /**
     *{@linkplain CollaboratorService#findCollaboratorByNameAsJSON(String, String)}
     */
    @Override
    public JSON findCollaboratorByNameAsJSON(String firstname, String lastname) {
        return travailleurDAO.findCollaboratorByName(firstname, lastname).toJson();
    }

//    /**
//     *{@linkplain CollaborateurService#findCollaboratorsByResponsableAsJSON(Collaborator)}
//     */
//    @Override
//    public List<JSON> findCollaboratorsByResponsableAsJSON(Collaborator responsable) {
//        List<T> collaborateurs = travailleurDAO.findCollaboratorsByResponsable(responsable);
//        List<JSON> collaborateursJSON = new ArrayList<>();
//
//        for(T tmpCollab : collaborateurs){
//            if (tmpCollab.isEnabled() && !isAdminManager(tmpCollab)) {
//                collaborateursJSON.add(tmpCollab.toJson());
//            }
//        }
//
//        return collaborateursJSON;
//    }

//    /**
//     * {@linkplain CollaborateurService#findAllOrderByNomAscAsJsonWithoutADMIN()}
//     */
//    @Override
//    public List<JSON> findAllOrderByNomAscAsJsonWithoutADMIN() {
//
//        List<T> tmpListCollaborator = this.findAllOrderByNomAscWithoutADMIN();
//        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<CollaboratorAsJson>();
//
//        for (T tmpCollaborator : tmpListCollaborator) {
//
//            if (!tmpCollaborator.getStatut().equals(statutCollaboratorDAO.get(2l))) {
//                tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
//            }
//        }
//        return tmpListCollaboratorAsJson;
//    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<JSON> findAllOrderByNomAscAsJson() {

        List<T> tmpListCollaborator = this.findAllOrderByNomAsc();
        List<JSON> tmpListCollaboratorAsJson = new ArrayList<>();
        for (T tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    @Override
    public List<JSON> findAllWithMissionOrderByNomAscAsJson() {
        List<T> tmpListCollaborator = travailleurDAO.findTravailleurs();
        List<JSON> tmpListCollaboratorAsJson = new ArrayList<>();

        for (T tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@linkplain CollaboratorService#findAllByStatutOrderByNomAsJson(String)}
     */
    @Override
    public List<JSON> findAllByStatutOrderByNomAsJson(String pStatut) throws Exception {
        //Recuperation d'un statut
        StatutCollaborateur statut =
            statutCollaboratorDAO.findUniqEntiteByProp(
                StatutCollaborateur.PROP_CODE,
                pStatut
            );

        if (null == statut) {
            throw new Exception("Statut (" + pStatut + ") inconnu.");
        }

        //Recuperation de la liste des managers triés par nom
        List<T> listManagers =
            this.getDAO().findListEntitesByProp(
                Collaborator.PROP_STATUT,
                statut,
                Collaborator.PROP_NOM
            );

        List<JSON> tmpListManagerAsJson = new ArrayList<>();

        for (T tmpManager : listManagers) {
            if (tmpManager.isEnabled()) {
                tmpListManagerAsJson.add(tmpManager.toJson());
            }
        }

        return tmpListManagerAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<JSON> findAllOrderWithDisabledByNomAscAsJson() {

        List<T> tmpListCollaborator = travailleurDAO.findTravailleurs();
        List<JSON> tmpListCollaboratorAsJson = new ArrayList<>();

        for (T tmpCollaborator : tmpListCollaborator) {

            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());

        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findById(Long aIdTravailleur) {
        return travailleurDAO.get(aIdTravailleur);
    }

}
