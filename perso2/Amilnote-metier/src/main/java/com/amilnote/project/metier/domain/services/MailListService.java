package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;

import java.util.List;


public interface MailListService {

    /**
     * Send asynchronously an email to inform an absence needs validation
     *
     * @param state        {@link String}
     * @param absenceIds   a list of {@link Long}
     * @param collaborator {@link Collaborator}
     * @param comment      {@link String}
     */
    void sendAbsenceListValidationEmail(String state, List<Long> absenceIds, Collaborator collaborator, String comment);

}
