/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.ContactClient;
import java.util.List;

/**
 * The interface Contact client dao.
 */
public interface ContactClientDAO extends LongKeyDAO<ContactClient> {

    /**
     * retrouver tous les client classés par nom (ascendant)
     *
     * @return une liste de client
     */
    List<ContactClient> findAllOrderByNomAsc();

    /**
     * Supression du contact dans la BDD
     *
     * @param contact the contact
     * @return string string
     */
    String deleteContactClient(ContactClient contact);
}
