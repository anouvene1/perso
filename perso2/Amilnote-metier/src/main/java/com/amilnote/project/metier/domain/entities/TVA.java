/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.TVAAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Tva.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_tva")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class TVA implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPETVA.
     */
    public static final String PROP_TYPETVA = "tva";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    /**
     * The constant TAUX_NORMAL.
     */
    public static final String TAUX_NORMAL = "NOR";
    /**
     * The constant TAUX_INTERMEDIAIRE.
     */
    public static final String TAUX_INTERMEDIAIRE = "INT";
    /**
     * The constant TAUX_REDUIT.
     */
    public static final String TAUX_REDUIT = "RED";
    /**
     * The constant TAUX_PARTICULIER.
     */
    public static final String TAUX_PARTICULIER = "PAR";
    /**
     * The constant TAUX_ZERO.
     */
    public static final String TAUX_ZERO = "ZER";
    /**
     * The constant TAUX_KM.
     */
    public static final String TAUX_KM = "KM";
    private static final long serialVersionUID = 4412240278282002064L;
    private Long id;
    private String tva;
    private String code;
    private float montant;

    /***
     * Constructor
     *
     * @param pTva     the p tva
     * @param pCode    the p code
     * @param pMontant the p montant
     */
    public TVA(String pTva, String pCode, float pMontant) {
        super();
        this.tva = pTva;
        this.code = pCode;
        this.setMontant(pMontant);
    }

    /**
     * Instantiates a new Tva.
     */
    public TVA() {
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /***
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /***
     * @return the tva
     */
    @Column(name = "type_tva", nullable = false)
    public String getTva() {
        return this.tva;
    }


    /***
     * @param pTva the p tva
     */
    public void setTva(String pTva) {
        this.tva = pTva;
    }

    /***
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /***
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "montant", nullable = false)
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the montant to set
     */
    public void setMontant(float pMontant) {
        montant = pMontant;
    }

    /**
     * To json tva as json.
     *
     * @return the tva as json
     */
    public TVAAsJson toJson() {
        return new TVAAsJson(this);
    }
}
