/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.TypeMission;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;


/**
 * The interface Mission dao.
 */
public interface MissionDAO extends LongKeyDAO<Mission> {
    /**
     * retourne une liste de mission dont le collaborateur, la date début et la date fin correspondent
     * à ceux passés en paramètre
     *
     * @param collaborator the p collaborateur
     * @param dateFin        the date fin
     * @return List list
     */
    List<Mission> findByDate(Collaborator collaborator, DateTime dateFin);

    /**
     * retrouver toutes les missions liées à un collaborateur et dont la dateFin est superieur à pDateFin
     *
     * @param collaborator the p collaborateur
     * @param pDateFin       the p date fin
     * @return List list
     */
    List<Mission> findAllMissionsNotFinishForCollaborator(Collaborator collaborator, DateTime pDateFin);

    /**
     * Retourne la mission par défault pour le collaborateur
     *
     * @param collaborator the p collaborateur
     * @return Mission mission
     */
    Mission findDefaultMissionForCollaborator(Collaborator collaborator);

    /**
     * retrouver toutes les missions liées à un collaborateur et dont la dateFin est superieur à pDateFin
     *
     * @param collaborator the p collaborateur
     * @param pDebutPeriode  the p debut periode
     * @param pFinPeriode    the p fin periode
     * @return List list
     */
    List<Mission> findMissionEncoursPeriode(Collaborator collaborator, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * retrouver toutes les missions clientes d'un mois voulu
     *
     * @param pDate la date voulue
     * @return List list
     * @throws Exception exception
     */
    List<Mission> findMissionsClientesByMonth(DateTime pDate) throws Exception;

    /**
     * retrouver toutes les missions clientes d'un mois voulu
     *
     * @param pDate          la date voulue
     * @param collaborator le collaborateur voulu
     * @return List list
     */
    List<Mission> findMissionsClientesByMonthForCollaborator(Collaborator collaborator, DateTime pDate);

    /**
     * retrouver toutes les missions clientes entre deux dates
     *
     * @param debut         la date de depart
     * @param fin           la date de fin
     * @param collaborator le collaborateur voulu
     * @return List list
     */
    List<Mission> findClientMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator);

    /**
     * Retourne le nombre de missions à l'état Brouillon (donc en attente de validationpar l'admin)
     *
     * @return nb mission attente val
     */
    int getNbMissionAttenteVal();

    /**
     * Retourne le nombre de missions à l'état Soumis (donc en attente de validation par le collab passé en paramètre
     *
     * @param collaborator the p collaborateur
     * @return nb mission attente val
     */
    int getNbMissionAttenteVal(Collaborator collaborator);

    /**
     * retourne toutes les missions en cours
     *
     * @param date the date
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionEnCours(Date date);

    /**
     * retourne la liste des missions clientes à une date précise
     *
     * @param date the date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionClientEnCours(Date date);

    /**
     * retourne la liste des missions clientes en cours du mois
     *
     * @param date the date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionClientActuelle(DateTime date);


    /**
     * retourne la liste des missions non clientes à une date précise
     *
     * @param date the date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionEnCoursNonClient(Date date);

    /**
     * retourne les missions d'un collaborateur à une date précise
     *
     * @param date          date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionForCollaborator(DateTime date, Collaborator collaborator);

    /**
     * return mission for collaborateur between two dates
     *
     * @param debut         the start date
     * @param fin           the end date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator);

    /**
     * return all mission between two dates
     *
     * @param debut the start date
     * @param fin   the end date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionBetweenTwoDates(Date debut, Date fin);

    /**
     * return mission not client between two dates
     *
     * @param debut the start date
     * @param fin   the end date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionNonClientBetweenTwoDates(Date debut, Date fin);

    /**
     * return mission not client between two dates for collaboateur
     *
     * @param debut         the start date
     * @param fin           the end date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionNonClientBetweenTwoDatesForCollab(Date debut, Date fin, Collaborator collaborator);

    /**
     * Return all missions by type of mission, status of collaborator for a given period
     *
     * @param typeMission        {@link TypeMission} list of missions for bill checking
     * @param collaboratorStatus {@link StatutCollaborateur}
     * @param lastDayOfMonth     {@link Date} the last day of selected month
     * @return list of {@link Mission}
     */
    List<Mission> findMissionByTypeCollaboratorStatusAndCurrentPeriod(TypeMission typeMission, StatutCollaborateur collaboratorStatus, Date lastDayOfMonth);
}
