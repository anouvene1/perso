/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import com.itextpdf.text.DocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.DateTime;

import javax.naming.NamingException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * The interface Facture service.
 */
public interface FactureService extends AbstractService<Facture> {

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pFacture la facture à traiter
     * @return int int
     * @throws IOException the io exception
     */
    int createOrUpdateFacture(Facture pFacture) throws IOException;

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pFactureAsJson the p facture as json
     * @param pCommande      the commande json
     * @param edit           the edit
     * @return int int
     */
    int createOrUpdateFactureAVANTREFONTE(FactureAsJson pFactureAsJson, Commande pCommande, Boolean edit);

    /**
     * Suppression de la commande
     *
     * @param pFacture the p facture
     * @return string string
     */
    String deleteFacture(Facture pFacture);

    /**
     * Find invoices by type (Facturables, Main, Frais) and activity report.
     *
     * @param factureType            the type of invoices we want to.
     * @param wantedDate             to get the invoice's month
     * @param withYear               to make distinction between invoices by month or year
     * @param activityReportValidate Find if the invoice depends on validated RA or not.
     * @return the list of invoices we want to by type and activity report.
     */
    List<Facture> findFacturesByTypeAndByRA(TypeFacture factureType, DateTime wantedDate, boolean withYear, Integer... activityReportValidate);

    /**
     * Find all order by nom asc list.
     *
     * @return the list
     */
    List<Facture> findAllOrderByNomAsc();

    /**
     * Retourne les factures qui sont complètes et peuvent être facturées
     *
     * @return the list
     */
    List<Facture> findFacturables();

    /**
     * Trouver les factures pour lesquelles le collaborateur n'a pas soumis son RA
     *
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesRANonSoumis();

    /**
     * Trouver les factures de frais
     *
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesFrais();

    /**
     * Retourne toutes les factures qui sont complètes et peuvent être facturées pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list
     * @author clome
     */
    List<Facture> findFacturablesByMonth(DateTime pDateVoulue);

    /**
     * return the {@link Facture} ready for the billing above zero for a given month
     *
     * @param dateTime a given month
     * @return the list containing all {@link Facture} above zero
     */
    List<Facture> findFacturablesAboveZeroByMonth(DateTime dateTime);

    /**
     * Trouver les factures pour lesquelles le collaborateur n'a pas soumis son RA pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesRANonSoumisByMonth(DateTime pDateVoulue);

    /**
     * Trouver les factures de frais pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesFraisByMonth(DateTime pDateVoulue);

    /**
     * Trouver les factures à la main pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesALaMainByMonth(DateTime pDateVoulue);

    /**
     * Trouver le numéro de facture maximum
     *
     * @return int le max des numéros de facture
     */
    int findMaxNumFacture();

    /**
     * Find by id facture.
     *
     * @param pIdFacture the p id facture
     * @return the facture
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    Facture findById(int pIdFacture);

    /**
     * Find by commande list.
     *
     * @param pCommande the p commande
     * @return the list
     */
    List<Facture> findByCommande(Commande pCommande);

    /**
     * Retourne la facture correspondant à la mission pour le mois voulu
     *
     * @param pMission la mission
     * @param pDate    le mois voulu
     * @return la facture correspondante
     * @author clome
     */
    Facture findByMissionByMonth(Mission pMission, DateTime pDate);

    /**
     * Retourne la liste des factures correspondant à la mission entre les dates voulues
     *
     * @param pMission   la mission
     * @param pDateDebut la date de début
     * @param pDateFin   la date de fin
     * @return la liste des factures correspondante
     * @author clome
     */
    List<Facture> findByMissionBetweenDates(Mission pMission, DateTime pDateDebut, DateTime pDateFin);

    /**
     * Find by commande by month facture.
     *
     * @param pCommande the p commande
     * @return the facture
     */
    Facture findByCommandeByMonth(Commande pCommande);

    /**
     * retrouver toutes les commandes et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<FactureAsJson> findAllOrderByNomAscAsJson();

    /**
     * Permet de mettre à jour les Factures :
     * - Met à jour la quantité
     * - Met à jour le montant
     * - Met à jour le prix
     *
     * @param dateRAVoulue  DateTime
     * @param collaborateur Collaborator
     * @throws IOException the io exception
     */
    void updateFactures(DateTime dateRAVoulue, Collaborator collaborateur) throws IOException;

    /**
     * Calcule le montant HT de l'ensemble des frais de la facture
     *
     * @param facture Facture
     * @return le montant HT de l'ensemble des frais de la facture
     */
    Pair<Float, Float> getMontantHT(Facture facture);

    /**
     * Méthode servant à créer une facture à partir d'une mission. La misison doit être une mission cliente
     * REFONTE DU MODULE DE FACTURATION
     *
     * @param mission la mission pour laquelle on crée une facture
     * @param tva     a {@link TVA}
     * @return la facture correspondante
     * @author clome
     */
    Facture createFactureFromMission(Mission mission, TVA tva);

    /**
     * Méthode servant à renvoyant les valeurs de quantité et de montant correspondant à une facture, qui évolue en
     * fonction de ses frais
     *
     * @param facture la facture à traiter
     * @return la quantité et le montant à afficher dans l'ordre de facturation pour la facture
     */
    Pair<Float, Float> getQuantiteMontantAvecFrais(Facture facture);

    /**
     * Méthode servant à renvoyer les valeurs des montant de tous les frais correspondant à une facture
     *
     * @param facture la facture à traiter
     * @return le montant total des frais à afficher dans l'ordre de facturation pour la facture
     */
    Float getMontantFrais(Facture facture);

    /**
     * Méthode qui retourne un booléen selon que le numéro de facture existe déjà ou non
     *
     * @param numFacture led numéro de facture
     * @param idFacture  facture id
     * @return le booléen correspondant
     * @throws IOException the IO exception
     */
    Boolean checkExistsNumFacture(int numFacture, int idFacture) throws IOException;

    /**
     * Dans les factures, on veut afficher comme date, le dernier jour ouvrable du mois
     * Cette méthode sert à renvoyer le plus récent jour ouvrable depuis le jour donnée en argument (si c'est un jour
     * ouvrable, pas de changement, et si c'est un samedi ou un dimanche, on renvoit le dernier vendredi)
     *
     * @param dateOuvree le jour à convertir
     * @return le plus récent jour ouvrable
     */
    DateTime getMostRecentWorkingDay(DateTime dateOuvree);


    /**
     * Permet de retrouver le #hash correspondant au type de facture traité
     * utile afin de faire une redirection vers la bonne interface après validation ou édition d'une facture
     *
     * @param idTypeFacture est l'id de la facture traitée
     * @param idMission     est l'id de la mission concernée par la facture
     * @return le #hash du type de facture traité
     */
    String findTypeFactureById(Integer idTypeFacture, Long idMission);

    void createFactureAfterCron(Mission missionFacture) throws IOException;

    /**
     * Method for retrieving the list of all invoices whose date is in a given month, and whose state is part of a given set of states
     *
     * @param dateOfSearch          the date of the month of search
     * @param propertyNameToOrderBy the result list will be ordered by this property
     * @param codesEtatsFactures    list of invoices states codes, "BR" Brouillon, "SO" Soumis, "VA" Validé, etc.
     * @return all invoices whose date is in a given month, and whose state is part of a given set of states
     */
    List<Facture> findFactureByMonthAndState(DateTime dateOfSearch, String propertyNameToOrderBy, String... codesEtatsFactures);

    /**
     * Calculate the total of the {@link Facture} + any attached {@link ElementFacture} without value-added tax
     *
     * @param facture the {@link Facture}
     * @return the total as a float
     */
    float calculateTotalFactureAndElementFactureHt(Facture facture);

    /**
     * Calculate the total value-added tax of the {@link Facture} + any attached {@link ElementFacture}
     *
     * @param facture the {@link Facture}
     * @return the total as a float
     */
    float calculateTotalFactureAndElementFactureTva(Facture facture);

    /**
     * Calculate the total of the {@link Facture} + any attached {@link ElementFacture} with the value-added tax
     *
     * @param facture the {@link Facture}
     * @return the total as a float
     */
    float calculateTotalFactureAndElementFactureTtc(Facture facture);

    /**
     * This method sorts the invoice by their number.
     *
     * @param listeFactures list of the invoices
     */
    void orderFacturesByNumber(List<Facture> listeFactures);

    /**
     * To find worked days (quantite)
     *
     * @param facture current invoice from the chosen list
     * @return float with the chosen element
     */
    float calculateWorkedDaysforInvoicesExcel(Facture facture);

    /**
     * To find amount (montant)
     *
     * @param facture current invoice from the chosen list
     * @return float with the amount element
     */
    float calculateAmountforInvoicesExcel(Facture facture);

    /**
     * Method for retrieving all invoices by given collaborator status code and one or multiple {@link SearchCriteria}
     *
     * @param collaborateurStatusCode the status code of the collaborator
     * @param orderingProperty        the resulting list will be ordered by this property
     * @param isAscendingOrdering     is ordering ascending or descending
     * @param searchCriterias         one or multiple (varargs) {@link SearchCriteria}
     * @return a list of all invoices corresponding to a given collaborator status code
     */
    List<Facture> findByCollaboratorStatusCodeAndSearchCriterias(String collaborateurStatusCode, String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias);

    /**
     * Validate all invoices af a given month and year which consist of setting invoices states to 10 (archived), generate and save corresponding pdf, persist corresponding archived invoices
     *
     * @param monthOfSearch      all invoices which {@link Facture#getDateFacture()} 's month correspond to this month of search
     * @param yearOfSearch       all invoices which {@link Facture#getDateFacture()} 's year correspond to this year of search
     * @param invoiceTypeId      the type of the invoice
     * @param collaboratorStatus the status of the collaborator
     * @return true if there is at least one validated invoices, else return false if no validated invoice
     * @throws IOException            Local/Network File/Folder read/write issues
     * @throws DocumentException      issues related to pdf/excel read/write
     * @throws NamingException        naming exception
     * @throws InvalidFormatException invalid format exception for method arguments
     * @throws ParseException         parsing exception
     */
    boolean validateInvoices(String monthOfSearch, String yearOfSearch, long invoiceTypeId, String collaboratorStatus) throws IOException, DocumentException, NamingException, InvalidFormatException, ParseException;

    /**
     * Method to calculate the amount with value added taxes
     *
     * @param preTaxAmount  the amount without taxes (Hors Taxes)
     * @param valueAddedTax the percentage of the value added tax
     * @return a double of the calculated amount with value added taxes
     */
    double calculateTotalAmountWithVAT(double preTaxAmount, double valueAddedTax);

    /**
     * Method to calculate the sum of given invoices's amount including VAT (Value added Tax) or not
     *
     * @param withVAT  if invoices's amounts include VAT or not
     * @param invoices the sum of will be based on this invoices's amount
     * @return a double result of the sum of all invoices's amount
     */
    double calculateInvoicesTotalAmount(boolean withVAT, Facture... invoices);

    /**
     * Methode to get Facture list by Missions list
     *
     * @param missions {@link Mission}
     * @return List Facture
     */
    List<Facture> getFactureByMissionsList(List<Mission> missions);

    /**
     * method to give a number to each invoice of a list retrieved by the search, and check if the starting number invoiceNumber is greater than the highest invoice number in database
     *
     * @param idTypeFacture     the type of the invoices
     * @param numFacture        the numbering will start at this number
     * @param typeDeTravailleur the type of collaborator's status
     * @param yearOfSearch      the search will retrieve all invoices which year correspond to this
     * @param monthOfSearch     the search will retrieve all invoices which month correspond to this
     * @return a boolean if the numFacture is valid, if it's greater than the last numFacture number in database
     * @throws IOException Local/Network File/Folder Read/Write issue(s)
     */
    boolean assignNumbersToInvoices(int idTypeFacture, int numFacture, String typeDeTravailleur, String yearOfSearch, String monthOfSearch) throws IOException;

    /**
     * Retrieve all unarchived invoices by type and activity report validity ra_valid
     *
     * @param startSearchDate all invoices which dates are after this date
     * @param endSearchDate   all invoices which dates are before this date
     * @param invoiceType     the type of the invoice
     * @param raValid         a varargs of wanted ra_valid status of the invoice
     * @return a list of unarchived invoices which type an ra_valid correspond to given ones
     */
    List<Facture> findUnarchivedInvoicesByTypeAndValidRa(Date startSearchDate, Date endSearchDate, TypeFacture invoiceType, Integer... raValid);

    /**
     * Sort invoice list by client
     *
     * @param invoiceList list to sort
     * @return the list sorted
     *
     */
    List<Facture> sortInvoicesByClient(List<Facture> invoiceList);
}
