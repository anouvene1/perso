/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.TypeMissionAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * TypeMission
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_type_mission")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class TypeMission implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPE_FRAIS.
     */
    public static final String PROP_TYPE_FRAIS = "typeMission";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant TRAVAUX_INTERNES.
     */
    public static final String TRAVAUX_INTERNES = "TI";
    /**
     * The constant ACTIVITE_COMMERCIALE.
     */
    public static final String ACTIVITE_COMMERCIALE = "AC";
    /**
     * The constant FORMATION.
     */
    public static final String FORMATION = "FO";
    /**
     * The constant MISSION_CLIENTE.
     */
    public static final String MISSION_CLIENTE = "MI";
    /**
     * The constant WEB_FACTORY.
     */
    public static final String WEB_FACTORY = "WF";
    /**
     * The constant MOBILE_FACTORY.
     */
    public static final String MOBILE_FACTORY = "MF";
    /**
     * The constant DATA_FACTORY.
     */
    public static final String DATA_FACTORY = "DF";

    /**
     * The constant SOFT_FACTORY.
     */
    public static final String SOFT_FACTORY = "SF";


    private static final long serialVersionUID = -3551222991204906031L;
    private Long id;
    private String typeMission;
    private String code;
    private int etat;

    /**
     * Constructeur par defaut de la classe TypeFrais
     */
    public TypeMission() {
    }

    /**
     * Constructeur de la classe TypeFrais
     *
     * @param pTypeMission the p type mission
     * @param pCode        the p code
     * @param pEtat        the p etat
     */
    public TypeMission(String pTypeMission, String pCode, int pEtat) {
        super();
        typeMission = pTypeMission;
        code = pCode;
        etat = pEtat;
    }

	/* 
     * GETTERS AND SETTERS
	 */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets type mission.
     *
     * @return the type_frais
     */
    @Column(name = "type_mission", nullable = false)
    public String getTypeMission() {
        return typeMission;
    }

    /**
     * Sets type mission.
     *
     * @param pTypeMission the p type mission
     */
    public void setTypeMission(String pTypeMission) {
        this.typeMission = pTypeMission;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets etat.
     *
     * @return etat etat
     */
    @Column(name = "etat", nullable = false, columnDefinition = "TINYINT")
    public int getEtat() {
        return etat;
    }

    /**
     * Récupère un int, 0 inactif, 1 actif
     *
     * @param pEtat the etat to set
     */
    public void setEtat(int pEtat) {
        this.etat = pEtat;
    }

    /**
     * To json type mission as json.
     *
     * @return the type mission as json
     */
    public TypeMissionAsJson toJson() {
        return new TypeMissionAsJson(this);
    }

}
