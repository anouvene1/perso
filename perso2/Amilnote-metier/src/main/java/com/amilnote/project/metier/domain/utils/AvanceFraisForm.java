/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.FraisAsJson;

import java.util.List;


/**
 * The type Avance frais form.
 */
public class AvanceFraisForm {
    private List<FraisAsJson> listFrais;
    private String commentaire;

    /**
     * Instantiates a new Avance frais form.
     */
    public AvanceFraisForm() {
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the p commentaire
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }


    /**
     * Gets list frais.
     *
     * @return the list frais
     */
    public List<FraisAsJson> getListFrais() {
        return listFrais;
    }

    /**
     * Sets list frais.
     *
     * @param pListFrais the p list frais
     */
    public void setListFrais(List<FraisAsJson> pListFrais) {
        listFrais = pListFrais;
    }
}
