/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.FrequenceAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Frequence.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_frequence")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Frequence implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_FREQUENCE.
     */
    public static final String PROP_FREQUENCE = "frequence";
    /**
     * The constant FREQUENCE_PONCTUEL.
     */
    public static final String FREQUENCE_PONCTUEL = "PONC";
    /**
     * The constant FREQUENCE_JOURNALIER.
     */
    public static final String FREQUENCE_JOURNALIER = "JRS";
    /**
     * The constant FREQUENCE_HEBDOMADAIRE.
     */
    public static final String FREQUENCE_HEBDOMADAIRE = "HEB";
    /**
     * The constant FREQUENCE_MENSUEL.
     */
    public static final String FREQUENCE_MENSUEL = "MENS";
    /**
     * The constant FREQUENCE_TRIMESTRIELLE.
     */
    public static final String FREQUENCE_TRIMESTRIELLE = "TRIM";
    /**
     * The constant FREQUENCE_ANNUEL.
     */
    public static final String FREQUENCE_ANNUEL = "AN";
    private static final long serialVersionUID = -6808514250677769837L;
    private Long id;
    private String code;
    private String frequence;

    /**
     * Instantiates a new Frequence.
     */
    public Frequence() {
    }

    /**
     * Instantiates a new Frequence.
     *
     * @param pCode      the p code
     * @param pFrequence the p frequence
     */
    public Frequence(String pCode, String pFrequence) {
        super();
        this.setCode(pCode);
        this.setFrequence(pFrequence);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }


    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets frequence.
     *
     * @return the frequence
     */
    @Column(name = "frequence", nullable = false)
    public String getFrequence() {
        return frequence;
    }

    /**
     * Sets frequence.
     *
     * @param pFrequence the frequence to set
     */
    public void setFrequence(String pFrequence) {
        frequence = pFrequence;
    }

    /**
     * To json frequence as json.
     *
     * @return the frequence as json
     */
    public FrequenceAsJson toJson() {
        return new FrequenceAsJson(this);
    }
}
