/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.ElementFacture;
import com.amilnote.project.metier.domain.entities.Facture;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The type Element facture as json.
 */
public class ElementFactureAsJson {

    @JsonProperty("id")
    private int id_element_facture;

    private Facture facture;

    @JsonProperty("libelle")
    private String libelle;

    @JsonProperty("quantite")
    private Float quantite;

    @JsonProperty("pu")
    private Float prix;

    @JsonProperty("montant")
    private Float montant;

    /**
     * Instantiates a new Element facture as json.
     *
     * @param element the element
     */
    public ElementFactureAsJson(ElementFacture element) {
        this.setIdElementFacture(element.getIdElementFacture());
        this.setFacture(element.getFacture());
        this.setLibelleElement(element.getLibelleElement());
        if (element.getQuantiteElement() != null){
            this.setQuantiteElement(element.getQuantiteElement());
        }
        if (element.getPrixElement() != null){
            this.setPrixElement(element.getPrixElement());
        }
        if (element.getMontantElement() != null) {
            this.setMontantElement(element.getMontantElement());
        }

    }

    /**
     * Instantiates a new Element facture as json.
     */
    public ElementFactureAsJson() {
    }

    /**
     * Gets id element facture.
     *
     * @return the id_element_facture
     */
    public int getIdElementFacture() {
        return id_element_facture;
    }

    /**
     * Sets id element facture.
     *
     * @param id_element_facture the id_element_facture to set
     */
    public void setIdElementFacture(int id_element_facture) {
        this.id_element_facture = id_element_facture;
    }

    /**
     * Gets facture.
     *
     * @return the facture
     */
    public Facture getFacture() {
        return facture;
    }

    /**
     * Sets facture.
     *
     * @param facture the facture to set
     */
    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    /**
     * Gets libelle element.
     *
     * @return the nom
     */
    public String getLibelleElement() {
        return libelle;
    }

    /**
     * Sets libelle element.
     *
     * @param libelle the nom to set
     */
    public void setLibelleElement(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets quantite element.
     *
     * @return the quantite
     */
    public Float getQuantiteElement() {
        return quantite;
    }

    /**
     * Sets quantite element.
     *
     * @param quantite the quantite to set
     */
    public void setQuantiteElement(Float quantite) {
        this.quantite = quantite;
    }

    /**
     * Gets prix element.
     *
     * @return the prix
     */
    public Float getPrixElement() {
        return prix;
    }

    /**
     * Sets prix element.
     *
     * @param prix the prix to set
     */
    public void setPrixElement(Float prix) {
        this.prix = prix;
    }

    /**
     * Gets montant element.
     *
     * @return the montant
     */
    public Float getMontantElement() {
        return montant;
    }

    /**
     * Sets montant element.
     *
     * @param montant the montant to set
     */
    public void setMontantElement(Float montant) {
        this.montant = montant;
    }


}
