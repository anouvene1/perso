/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ElementFactureDAO;
import com.amilnote.project.metier.domain.entities.ElementFacture;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Element facture dao.
 */
@Repository("elementFactureDAO")
public class ElementFactureDAOImpl extends AbstractDAOImpl<ElementFacture> implements ElementFactureDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<ElementFacture> getReferenceClass() {
        return ElementFacture.class;
    }

    /**
     * {@linkplain ElementFactureDAO#createOrUpdateElementFacture(ElementFactureAsJson, Facture)}
     */
    @Override
    public int createOrUpdateElementFacture(ElementFactureAsJson pElementFactureAsJson, Facture facture) {
        String msgRetour = "";
        int id = 0;
        //[JNA][AMNOTE 142] Si le facture n'est pas vide, on enregiste l'élément en le reliant a cette facture
        if (facture != null) {
            // Récupération de la facture
            ElementFacture element = new ElementFacture();

            if (pElementFactureAsJson.getIdElementFacture() != 0) { // Si l'ID n'est pas null on est dans le cas d'une mise à jour
                element = findUniqEntiteByProp(ElementFacture.PROP_ID_ELEMENT_FACTURE, pElementFactureAsJson.getIdElementFacture());// On recupère l'élément
            }

            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // --- Ajout des informations concernant la commande
            element.setFacture(facture);
            if (pElementFactureAsJson.getLibelleElement() != null) {
                element.setLibelleElement(pElementFactureAsJson.getLibelleElement());
            } else {
                element.setLibelleElement("");
            }
            if (pElementFactureAsJson.getQuantiteElement() != null) {
                element.setQuantiteElement(pElementFactureAsJson.getQuantiteElement());
            } else {
                element.setQuantiteElement((float) 0.0);
            }
            if (pElementFactureAsJson.getPrixElement() != null) {
                element.setPrixElement(pElementFactureAsJson.getPrixElement());
            } else {
                element.setPrixElement((float) 0.0);
            }
            if (pElementFactureAsJson.getMontantElement() != null) {
                element.setMontantElement(pElementFactureAsJson.getMontantElement());
            } else {
                element.setMontantElement((float) 0.0);
            }

            // --- Sauvegarde et Commit de la mise à jour
            session.save(element);
            id = element.getIdElementFacture();
            session.flush();
            transaction.commit();
            pElementFactureAsJson.setIdElementFacture(element.getIdElementFacture());
            msgRetour = "ok";
        } else
            msgRetour = "error";

        return id;
    }

    /**
     * {@linkplain ElementFactureDAO#deleteElementFacture(ElementFacture)}
     */
    @Override
    public String deleteElementFacture(ElementFacture pElementFacture) {

        if (null != pElementFacture) {

            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            // --- Sauvegarde et Commit de la mise à jour
            session.delete(pElementFacture);

            session.flush();
            transaction.commit();
            return "Suppression effectuée avec succès";
        }
        return "Aucune suppression n'a été effectuée";
    }

    /**
     * {@linkplain ElementFactureDAO#findElementfacture()}
     */
    @Override
    public List<ElementFacture> findElementfacture() {
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc(ElementFacture.PROP_LIBELLE_ELEMENT));
        return criteria.list();
    }

}
