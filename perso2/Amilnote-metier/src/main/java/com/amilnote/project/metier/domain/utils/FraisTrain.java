/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import org.joda.time.DateTime;

/**
 * The type Frais train.
 */
public class FraisTrain {

    private float montant;

    private String villeDepart;

    private String villeArrivee;

    private DateTime date;

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the p montant
     */
    public void setMontant(float pMontant) {
        montant = pMontant;
    }

    /**
     * Gets ville depart.
     *
     * @return the ville depart
     */
    public String getVilleDepart() {
        return villeDepart;
    }

    /**
     * Sets ville depart.
     *
     * @param pVilleDepart the p ville depart
     */
    public void setVilleDepart(String pVilleDepart) {
        villeDepart = pVilleDepart;
    }

    /**
     * Gets ville arrivee.
     *
     * @return the ville arrivee
     */
    public String getVilleArrivee() {
        return villeArrivee;
    }

    /**
     * Sets ville arrivee.
     *
     * @param pVilleArrivee the p ville arrivee
     */
    public void setVilleArrivee(String pVilleArrivee) {
        villeArrivee = pVilleArrivee;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public DateTime getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param pDate the p date
     */
    public void setDate(DateTime pDate) {
        date = pDate;
    }
}
