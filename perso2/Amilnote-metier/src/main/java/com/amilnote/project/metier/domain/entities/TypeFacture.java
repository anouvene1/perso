/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.entities;


import com.amilnote.project.metier.domain.entities.json.TypeFactureAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "ami_ref_type_facture")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class TypeFacture implements java.io.Serializable {

    public static final String PROP_ID = "id";
    public static final String PROP_TYPE_FACTURE = "typeFacture";
    public static final String PROP_CODE = "code";

    // Standard billable invoices
    public static final String TYPEFACTURE_FACTURABLE = "FRABLE";
    public static final String TYPEFACTURE_FACTURABLE_SST = "FRABLESST";
    public static final String TYPEFACTURE_FACTURABLE_CSV = "FRABLECSV";
    public static final String TYPEFACTURE_FACTURABLE_SST_CSV = "FRABLESSTCSV";

    // Expenses invoices
    public static final String TYPEFACTURE_FACTURE_FRAIS = "FFRAIS";
    public static final String TYPEFACTURE_FACTURE_FRAIS_CSV = "FFRAISCSV";
    public static final String TYPEFACTURE_FACTURE_FRAIS_COLLAB_CSV = "FFRAISCOLLABCSV";

    // Handwritten invoices
    public static final String TYPEFACTURE_FACTURE_A_LA_MAIN = "FMAIN";
    public static final String TYPEFACTURE_FACTURE_A_LA_MAIN_CSV = "FMAINCSV";
    public static final String TYPEFACTURE_FACTURE_A_LA_MAIN_SST_CSV = "FMAINSSTCSV";

    // Variables used to define which tab the user is currently visualizing
    public static final String TYPEFACTURE_FACTURE_FRAIS_HASH = "#tabFfrais";
    public static final String TYPEFACTURE_FACTURABLE_HASH = "#tabFacturable";
    public static final String TYPEFACTURE_FACTURE_A_LA_MAIN_HASH = "#tabFMain";
    public static final String TYPEFACTURE_FACTURABLE_STT_HASH = "#tabFacturableSS";
    public static final String TYPEFACTURE_FACTURE_A_LA_MAIN_STT_HASH = "#tabFMainSS";


    private Long id;
    /**
     * le nom du type de facture en toutes lettres
     */
    private String typeFacture;
    /**
     * un code de référence pour chaque type de facture
     */
    private String code;

    /**
     * Enum used to replace the use of billing-related constants inside the application
     */
    public enum InvoiceType {
        FRABLE,             // Billable
        FRABLECSV,          // Billable CSV export
        FRABLESST,          // Billable subcontractors
        FRABLESSTCSV,       // Billable subcontractors CSV export

        FFRAIS,             // Expenses
        FFRAISCSV,          // Expenses CSV export
        FFRAISCOLLABCSV,    // Collaborators' expenses invoice CSV export

        FMAIN,              // Handwritten invoice
        FMAINCSV,           // Handwritten invoice CSV export
        FMAINSSTCSV         // Handwritten subcontractor invoice CSV export
    }

    public TypeFactureAsJson toJson() {
        return new TypeFactureAsJson(this);
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Type(type="long")
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "type_facture", nullable = false)
    public String getTypeFacture() {
        return typeFacture;
    }

    public void setTypeFacture(String typeFacture) {
        this.typeFacture = typeFacture;
    }

    @Column(name = "type_code", nullable = false, unique = true)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
