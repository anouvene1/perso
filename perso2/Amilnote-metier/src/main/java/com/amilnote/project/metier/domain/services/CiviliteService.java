package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Civilite;

import java.util.List;

public interface CiviliteService extends AbstractService<Civilite> {

    /**
     * renvoie la liste des civilités trié par ID
     * @return une liste de civilite
     */
    List<Civilite> findAllOrderById();
}
