/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by jnallet on 21/11/2016.
 */
public class JacksonJsonListConverter {

    private static Logger logger = LogManager.getLogger(JacksonJsonListConverter.class);

    /**
     * From json t.
     *
     * @param <T>           the type parameter
     * @param typeReference the type reference
     * @param json          the json
     * @return the t
     */
    public static <T> T fromJson(TypeReference<T> typeReference, String json) {
        T data = null;

        try {
            data = new ObjectMapper().readValue(json, typeReference);
        } catch (IOException e) {
            logger.error("[json converter] from json", e);
        }

        return data;

    }

}
