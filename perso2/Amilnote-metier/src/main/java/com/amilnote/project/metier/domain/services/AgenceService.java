package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Agence;
import com.amilnote.project.metier.domain.entities.json.AgenceAsJson;

import java.util.List;

/**
 * The interface Agence Service.
 */
public interface AgenceService extends AbstractService<Agence> {

    /**
     * Retourne toutes les agences
     *
     * @return all agences
     */
    List<Agence> getAllAgence();

    /**
     * Retourne liste d'agences pour le choix dans les missions activités commerciales
     *
     * @return liste agences
     */
    List<Agence> getAgenceChoixODM();

    /**
     * Retourne une Agence en fonction de l'id
     *
     * @param pId the p id
     * @return Agence agence by id
     */
    Agence getAgenceById(long pId);

    /**
     * Retourne Ale siège social de l'entreprise
     *
     * @return Agence agence siege social
     */
    Agence getAgenceSiegeSocial();

    /**
     * Retourne tous les agences au format json
     *
     * @return String (Liste de AgenceAsJson)
     */
    List<AgenceAsJson> getAllAgenceAsJson();

    /**
     * Retourne l'agence correspondant au code renseigné
     *
     * @param pCode agence souhaité
     * @return agence correspondant au code
     */
    Agence getAgenceByCode(String pCode);

}
