package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.CalculFraisDAOImpl;
import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.services.CalculFraisService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("CalculFraisService")

public class CalculFraisServiceImpl extends AbstractServiceImpl<Frais, CalculFraisDAOImpl> implements CalculFraisService {

    private static final Logger logger = LogManager.getLogger(CalculFraisServiceImpl.class);

    @Autowired
    private CalculFraisDAOImpl calculFraisDAO;


    /**
     * fonction de conversion de liste de frais en map
     *
     * @param list list
     * @return l'id du collab et le montant du frais
     */
    @Override
    public Map<Long, Object[]> doCalculAvance(List<Object[]> list) {
        Map<Long, Object[]> montants = new HashMap<Long, Object[]>();
        Object[] tabAvance = null;
        List<Long> listIdCollaborateur = getListIdCollaborateur(list);

        for (Long id : listIdCollaborateur) {
            Float montantForIdCollaborateurPO = 0f;
            Float montantForIdCollaborateurPE = 0f;
            tabAvance = new Object[2];

            for (Object[] frais : list) {

                Long idCollabFrais = new Long((frais[0].toString()));
                if (id.intValue() == idCollabFrais.intValue()) {
                    if (frais[1].toString().equals("12") && frais[3].toString().equals("PO")) {

                        montantForIdCollaborateurPO += Float.parseFloat(frais[2].toString());

                    } else if (frais[1].toString().equals("12") && frais[3].toString().equals("PE")) {

                        montantForIdCollaborateurPE += Float.parseFloat(frais[2].toString());

                    }
                }

                tabAvance[0] = montantForIdCollaborateurPO;
                tabAvance[1] = montantForIdCollaborateurPE;
            }

            montants.put(id, tabAvance);
        }
        return montants;
    }

    /**
     * fonction de conversion liste de frais en map de frais detaillé
     *
     * @param list list
     * @return Map pour chaque type de frais le montant
     */
    @Override
    public Map<Long, Map<Long, Object[]>> doCalculFraisDetail(List<Object[]> list) {
        Map<Long, Map<Long, Object[]>> montants = new HashMap<>();

        List<Long> listIdCollaborateur = getListIdCollaborateur(list);

        for (Long id : listIdCollaborateur) {
            Float montantForIdCollaborateur = 0f;
            Map<Long, Object[]> mapFraisDetail = new HashMap<>();
            Long idMissionFrais = 0L;
            int nbJoursFrais = 0;
            //il faut stocker la clé du frais


            for (Object[] fraisDetail : list) {
                Long idCollabFraisDetail = new Long((fraisDetail[0].toString())); //id du collab courant

                Object[] tabFrais = new Object[3];

                // recuperation des datails de frais du collab courant
                if (id.equals(idCollabFraisDetail)) {

                    //id du frais courant
                    Long idFraisCourant = (Long) fraisDetail[1];

                    Object[] tabMontantFraisTemp = mapFraisDetail.get(idFraisCourant);

                    //test si la map de frais du collab courant n'est pas vide et qu'il contient déjà l'id du frais
                    if (!mapFraisDetail.isEmpty() && mapFraisDetail.containsKey((Long) idFraisCourant)) {

                        //Object [] tabMontantFraisTemp=mapFraisDetail.get(idFraisCourant); //tableau temporaire avec montant en 0 et qte en 1
                        montantForIdCollaborateur = (Float) tabMontantFraisTemp[0]; // recupere le montant du frais associé au collab
                        nbJoursFrais = (int) tabMontantFraisTemp[1];
                        idMissionFrais = (Long) fraisDetail[5];
                    } else {
                        montantForIdCollaborateur = 0f;
                        nbJoursFrais = 0;
                        //idMission=(Long)tabMontantFraisTemp[2];
                    }
                    idMissionFrais = (Long) fraisDetail[5];
                    String typeFrais = fraisDetail[1].toString();
                    String fraisForfait = fraisDetail[4].toString();


                    switch (typeFrais) {
                        case "10"://frais restaurant midi
                            if (fraisForfait.equals("FRF") || fraisForfait.equals("")) {//comportement par defaut: on enleve le nb de jour de frais voiture au nb de jour de forfait
                                nbJoursFrais++;
                            }
                            montantForIdCollaborateur += Float.parseFloat(fraisDetail[2].toString());
                            break;
                        case "13"://frais restaurant midi
                            if (fraisForfait.equals("FRF") || fraisForfait.equals("")) {//comportement par defaut: on enleve le nb de jour de frais voiture au nb de jour de forfait
                                nbJoursFrais++;
                            }
                            montantForIdCollaborateur += Float.parseFloat(fraisDetail[2].toString());
                            break;
                        case "1"://Cas où on traite des frais voiture(km)
                            Float nbKm = Float.parseFloat(fraisDetail[3].toString());
                            montantForIdCollaborateur += nbKm * 0.38f;
                            if (fraisForfait.equals("FRF") || fraisForfait.equals("")) {//comportement par defaut: on enleve le nb de jour de frais voiture au nb de jour de forfait
                                nbJoursFrais++;
                            }


                            break;

                        case "2":
                        case "3":
                        case "4":
                            montantForIdCollaborateur += Float.parseFloat(fraisDetail[2].toString());
                            if (fraisForfait.equals("FRF") || fraisForfait.equals("")) {//comportement par defaut: on enleve le nb de jour de frais voiture au nb de jour de forfait
                                nbJoursFrais++;
                            }
                            break;
                        default:
                            montantForIdCollaborateur += Float.parseFloat(fraisDetail[2].toString());
                            break;
                    }


                    tabFrais[0] = montantForIdCollaborateur;
                    tabFrais[1] = nbJoursFrais;
                    if (idMissionFrais != 0) {
                        tabFrais[2] = idMissionFrais;
                    } else {
                        tabFrais[2] = null;
                    }
                    mapFraisDetail.put((Long) fraisDetail[1], tabFrais);

                }
            }

            montants.put(id, mapFraisDetail);
        }
        return montants;
    }


    @Override
    public List<Object[]> findFraisByDate(Date dateDebut, Date dateFin, String typeCalcul) {
        return calculFraisDAO.getDataFrais(dateDebut, dateFin, typeCalcul);
    }


    @Override
    public List<Object[]> findForfaitByDate(Date dateDebut, Date dateFin, String typeForfait) {
        return calculFraisDAO.getDataForfait(dateDebut, dateFin, typeForfait);
    }

    @Override
    public List<Object[]> findAbsence(Date dateDebut, Date dateFin) {
        return calculFraisDAO.getAbsenceCollab(dateDebut, dateFin);
    }


    /**
     * fonction des calcul detaillé des forfaits
     *
     * @param listResultatReq list
     * @param mapFrais        map
     * @param mapAbsence      map
     * @return map avec en clé l'id collab et en value tous ses forfaits
     */
    @Override
    public Map<Long, Map<Long, Float>> doCalculForfaitsDetail(List<Object[]> listResultatReq, Map<Long, Map<Long, Object[]>> mapFrais, Map<Long, Integer> mapAbsence) {

        Map<Long, Map<Long, Float>> montants = new HashMap<>();
        List<Long> listIdCollaborateur = getListIdCollaborateur(listResultatReq);

        try {

            for (Long idCollab : listIdCollaborateur) {

                Map<Long, Float> mapForfaitCollab = doCalculForfaitsDetail_byCollab(idCollab, listResultatReq, mapFrais, mapAbsence);
                montants.put(idCollab, mapForfaitCollab);

            }

        } catch (Exception e) {
            logger.error("[fixed price calculation]", e);
        }
        return montants;
    }


    /**
     * fonction des calcul detaillé des forfaits pour un collab
     *
     * @param idCollabCourant
     * @param list
     * @param mapFrais
     * @param mapAbsence
     * @return
     */
    private Map<Long, Float> doCalculForfaitsDetail_byCollab(Long idCollabCourant, List<Object[]> list, Map<Long, Map<Long, Object[]>> mapFrais, Map<Long, Integer> mapAbsence) {

        Float montantForIdCollaborateur = 0f;
        Map<Long, Float> mapForfaitCollab = new HashMap<>();
        boolean deja = false;
        for (Object[] forfaitDetail : list) {
            Long idCollabForfait = new Long(forfaitDetail[0].toString()); //id collab de la liste

            Long idMissionForfait = new Long(forfaitDetail[5].toString()); //id de la mission de la liste

            if (idCollabCourant.intValue() == idCollabForfait.intValue()) {

                if (!mapForfaitCollab.isEmpty() && mapForfaitCollab.containsKey((Long) forfaitDetail[4])) {//verifie si on a déjà traité le type de forfait dans un élément précedent de la liste

                    montantForIdCollaborateur = mapForfaitCollab.get((Long) forfaitDetail[4]); //recuperation de la valeur anciennement recuperee
                } else {
                    montantForIdCollaborateur = 0f;
                }

                Float montantUnitaire = Float.parseFloat(forfaitDetail[2].toString()); //recuperation du montant du forfait
                if (forfaitDetail[1].toString().equals("2")) { //verification frequence journalier


                    if (mapFrais.get(idCollabForfait) != null) {

                        Map<Long, Object[]> mapFraisTemp = new HashMap<>(mapFrais.get(idCollabForfait));// recuperation des frais du collab dans un map temporaire
                        String idForfait = forfaitDetail[4].toString();
                        Float quantite;
                        Long idMissionfrais = 0l;

                        quantite = Float.parseFloat(forfaitDetail[3].toString()); // recuperation de la quantite

                        if (idForfait.equals("1")) {//cas où on traite un forfait repas
                            if (mapFraisTemp.containsKey(10l)) {//cas où il existe des frais de repas midi
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(10l))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(10)))[1];
                                }
                            }
                            montantForIdCollaborateur += montantUnitaire * quantite;
                            mapForfaitCollab.put((Long) forfaitDetail[4], montantForIdCollaborateur);
                        }


                        if (idForfait.equals("2")) {//cas où on traite un forfait deplacement


                            if (mapAbsence.containsKey(idCollabForfait)) {
                                Integer i = mapAbsence.get(idCollabForfait);
                                if (!deja) {
                                    quantite += i;
                                    deja = true;
                                }
                            }

                            //Pour chaque type de Frais on relie le frais à la mission. S'il existe, on retraite la quantité  correspondante au forfait.
                            if (mapFraisTemp.containsKey(1l)) {//cas où il existe des frais de voiture
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(1l))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(1)))[1];
                                }
                            }
                            if (mapFraisTemp.containsKey(2l)) {//cas où il existe des frais de train
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(2))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(2)))[1];
                                }
                            }
                            if (mapFraisTemp.containsKey(3l)) {//cas où il existe des frais de transport
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(3))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(3)))[1];
                                }
                            }
                            if (mapFraisTemp.containsKey(4l)) {//cas où il existe des frais d'avion
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(4))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(4)))[1];
                                }
                            }
                            if (mapFraisTemp.containsKey(13l)) {//cas où il existe des frais d'avion
                                idMissionfrais = (Long) mapFraisTemp.get(new Long(13))[2];
                                if (idMissionfrais.equals(idMissionForfait) && idMissionfrais != 0l) {
                                    quantite -= (Integer) (mapFraisTemp.get(new Long(13)))[1];
                                }
                            }

                            montantForIdCollaborateur += montantUnitaire * quantite;
                            mapForfaitCollab.put((Long) forfaitDetail[4], montantForIdCollaborateur);
                        }

                    } else { // cas où y'a aucun frais
                        String idForfait = forfaitDetail[4].toString();
                        Float quantite;
                        quantite = Float.parseFloat(forfaitDetail[3].toString());
                        if (idForfait.equals("2")) {
                            if (mapAbsence.containsKey(idCollabForfait)) {
                                Integer i = mapAbsence.get(idCollabForfait);
                                if (!deja) {
                                    quantite += i;
                                    deja = true;
                                }
                            }
                        }
                        montantForIdCollaborateur += montantUnitaire * quantite;
                        mapForfaitCollab.put((Long) forfaitDetail[4], montantForIdCollaborateur);
                    }


                } else if (forfaitDetail[1].toString().equals("4")) { //verification frequence mensuel
                    montantForIdCollaborateur += Float.parseFloat(forfaitDetail[2].toString());
                    //on ajoute le forfait dans la liste
                    mapForfaitCollab.put((Long) forfaitDetail[4], montantForIdCollaborateur);
                }

            }

        }
        return mapForfaitCollab;
    }

    /**
     * fonction pour recuperir l'Id du collaborateur à partir d'une list criteria
     *
     * @param list objet criteria
     * @return list d'id de collaborateurs
     */
    private List<Long> getListIdCollaborateur(List<Object[]> list) {

        List<Long> listLong = new ArrayList<>(list.size());
        for (Object[] idCollab : list) {
            listLong.add((Long) idCollab[0]);
        }

        return listLong;

    }

    /**
     * fonction pour recuperir l'Id du collaborateur à partir d'une map
     *
     * @param map
     * @return
     */
    private List<Long> map2List(Map<Long, Float> map) {
        List<Long> listLong = new ArrayList<>();
        for (Map.Entry<Long, Float> idCollab : map.entrySet()) {
            listLong.add((Long) idCollab.getKey());
        }
        return listLong;
    }

    /**
     * fonction pour recuperir l'Id du collaborateur à partir d'une map
     *
     * @param map
     * @return
     */
    private List<Long> map2ListAvance(Map<Long, Object[]> map) {
        List<Long> listLong = new ArrayList<>();
        for (Map.Entry<Long, Object[]> idCollab : map.entrySet()) {
            listLong.add((Long) idCollab.getKey());
        }
        return listLong;
    }

    /**
     * fonction pour recuperir l'Id du collaborateur à partir d'une map
     *
     * @param map
     * @return
     */
    private List<Long> map2ListDetail(Map<Long, Map<Long, Float>> map) {
        List<Long> listLong = new ArrayList<>();
        for (Map.Entry<Long, Map<Long, Float>> idCollab : map.entrySet()) {
            listLong.add((Long) idCollab.getKey());
        }
        return listLong;
    }

    /**
     * fonction pour recuperir l'Id du collaborateur à partir d'une map avec gestion d'Object
     *
     * @param map
     * @return
     */
    private List<Long> map2ListDetailObject(Map<Long, Map<Long, Object[]>> map) {
        List<Long> listLong = new ArrayList<>();
        for (Map.Entry<Long, Map<Long, Object[]>> idCollab : map.entrySet()) {
            listLong.add((Long) idCollab.getKey());
        }
        return listLong;
    }


    @Override
    public CalculFraisDAOImpl getDAO() {
        return null;
    }


    /**
     * La fonction reupere pour chaque collaborateur le detail de chaque frais
     *
     * @param montantFrais    montant
     * @param montantForfaits montant
     * @param montantAvances  montant
     * @return la liste formatée pour le fichier xls des frais detaillé pour chaque type de frais
     */
    @Override
    public List<Object[]> doCalculDetail(String typeDetail, Map<Long, Map<Long, Object[]>> montantFrais, Map<Long, Map<Long, Float>> montantForfaits, Map<Long, Object[]> montantAvances) {
        List<Object[]> listMontants = new ArrayList<>();

        Map<Long, Float> mFrais;

        Set cles;
        Iterator it;


        List<Long> listIdCollaborateur = getListIdCollaborateurTotalDetail(montantFrais, montantForfaits, montantAvances);

        for (Long id : listIdCollaborateur) {
            Float fraisVoiture = 0f;
            Float fraisTrain = 0f;
            Float fraisTrans = 0f;
            Float fraisAvion = 0f;
            Float fraisLocation = 0f;
            Float fraisEssence = 0f;
            Float fraisPeage = 0f;
            Float fraisParking = 0f;
            Float fraisHotel = 0f;
            Float fraisRestoMidi = 0f;
            Float fraisRestoSoir = 0f;
            Float fraisTaxi = 0f;
            Float fraisTelephone = 0f;
            Float fraisDivers = 0f;
            Float fraisAbo = 0f;
            Float fraisPetitDej = 0f;
            Float forfaitRepas = 0f;
            Float forfaitDeplacement = 0f;
            Float forfaitTelephone = 0f;
            Float forfaitLogement = 0f;
            Float forfaitAbo = 0f;
            Float avance = 0f;
            Float avancePO = 0f;
            Float avancePE = 0f;

// on recupere pour le collab l'ensemble des frais (autre que avance)

            if (montantFrais.containsKey(id)) { //si l'id du collab est dans la liste

                Map<Long, Object[]> mapFraisCollab = montantFrais.get(id);//on assigne les frais du collab dans une map temporaire

                mFrais = new HashMap<>();
                cles = mapFraisCollab.keySet();
                it = cles.iterator();
                while (it.hasNext()) {
                    Object cle = it.next();
                    mFrais.put((Long) cle, (Float) mapFraisCollab.get(cle)[0]);
                }
                cles.clear();

                for (Map.Entry<Long, Float> objetFraisCollab : mFrais.entrySet()) {
                    Float fraisTemp = objetFraisCollab.getValue();
                    int idFraisTemp = objetFraisCollab.getKey().intValue();
                    switch (idFraisTemp) {
                        case 1:
                            fraisVoiture = fraisTemp;
                            break;
                        case 2:
                            fraisTrain = fraisTemp;
                            break;
                        case 3:
                            fraisTrans = fraisTemp;
                            break;
                        case 4:
                            fraisAvion = fraisTemp;
                            break;
                        case 5:
                            fraisLocation = fraisTemp;
                            break;
                        case 6:
                            fraisEssence = fraisTemp;
                            break;
                        case 7:
                            fraisPeage = fraisTemp;
                            break;
                        case 8:
                            fraisParking = fraisTemp;
                            break;
                        case 9:
                            fraisHotel = fraisTemp;
                            break;
                        case 10:
                            fraisRestoMidi = fraisTemp;
                            break;
                        case 11:
                            fraisRestoSoir = fraisTemp;
                            break;
                        case 12:
                            avance = fraisTemp;
                            break;
                        case 13:
                            fraisTaxi = fraisTemp;
                            break;
                        case 14:
                            fraisTelephone = fraisTemp;
                            break;
                        case 15:
                            fraisDivers += fraisTemp;
                            break;
                        case 16:
                            fraisAbo = fraisTemp;
                            break;
                        case 17:
                            fraisDivers += fraisTemp;
                            break;


                    }


                }
                mFrais.clear();
            }

            // on recupere pour le collab l'ensemble des forfaits associées
            if (montantForfaits.containsKey(id)) {

                Map<Long, Float> mapForfaitCollab = montantForfaits.get(id);
                for (Map.Entry<Long, Float> objetForfaitCollab : mapForfaitCollab.entrySet()) {
                    Float forfaitTemp = objetForfaitCollab.getValue();
                    int idForfaitTemp = objetForfaitCollab.getKey().intValue();
                    switch (idForfaitTemp) {
                        case 1:
                            forfaitRepas = forfaitTemp;
                            break;
                        case 2:
                            forfaitDeplacement = forfaitTemp;
                            break;
                        case 3:
                            forfaitTelephone = forfaitTemp;
                            break;
                        case 4:
                            forfaitLogement = forfaitTemp;
                            break;
                        case 6:
                            forfaitAbo = forfaitTemp;
                            break;

                    }


                }
            }
            // on recupere pour le collab le montant des avance associées
            if (montantAvances.containsKey(id)) {
                avancePO = (Float) (montantAvances.get(id))[0];
                avancePE = (Float) (montantAvances.get(id))[1];
            }
            Object[] o = null;
            float totalFrais;
            float totalForfait;
            totalFrais = fraisVoiture + fraisTrain + fraisTrans + fraisAvion + fraisLocation + fraisEssence + fraisPeage + fraisParking + fraisHotel + fraisRestoMidi + fraisRestoSoir + fraisTaxi + fraisTelephone + fraisDivers + fraisAbo;
            totalForfait = forfaitAbo + forfaitRepas + forfaitDeplacement + forfaitTelephone + forfaitLogement;

            switch (typeDetail) {
                case "detail":
                    // on crée un tableau formatée pour le fichier xls
                    o = new Object[26];
                    o[0] = id;
                    o[1] = fraisVoiture;
                    o[2] = fraisTrain;
                    o[3] = fraisTrans;
                    o[4] = fraisAvion;
                    o[5] = fraisLocation;
                    o[6] = fraisEssence;
                    o[7] = fraisPeage;
                    o[8] = fraisParking;
                    o[9] = fraisHotel;
                    o[10] = fraisRestoMidi;
                    o[11] = fraisRestoSoir;
                    o[12] = fraisTaxi;
                    o[13] = fraisTelephone;
                    o[14] = fraisDivers;
                    o[15] = fraisAbo;
                    o[16] = avancePO;
                    o[17] = avancePE;
                    o[18] = forfaitRepas;
                    o[19] = forfaitDeplacement;
                    o[20] = forfaitTelephone;
                    o[21] = forfaitLogement;
                    o[22] = totalFrais;
                    o[23] = totalForfait;
                    o[24] = totalFrais + totalForfait;
                    o[25] = totalFrais + totalForfait - (avancePO + avancePE);
                    break;

                case "simple":
                    o = new Object[7];
                    o[0] = id;
                    o[1] = avancePO;
                    o[2] = avancePE;
                    o[3] = totalFrais;
                    o[4] = totalForfait;
                    o[5] = totalFrais + totalForfait;
                    o[6] = totalFrais + totalForfait - (avancePO + avancePE);
                    break;

            }
            listMontants.add(o);
        }

        return listMontants;
    }

    /**
     * Fonction de recuperation des ID collaborateur
     *
     * @param montantFrais
     * @param montantForfaits
     * @param montantAvances
     * @return list d'id
     */
    private List<Long> getListIdCollaborateurTotal(Map<Long, Float> montantFrais, Map<Long, Float> montantForfaits, Map<Long, Float> montantAvances) {
        // on recupere les id collab List
        HashSet<Long> hashset = new HashSet<>();
        List<Long> listLongTotalDistinct;

        List<Long> listLongFrais;
        listLongFrais = map2List(montantFrais);

        List<Long> listLongForfait;
        listLongForfait = map2List(montantForfaits);

        List<Long> listLongAvance;
        listLongAvance = map2List(montantAvances);

        //On concatene les resultats
        List<Long> listLongTotal = new ArrayList<>();
        listLongTotal.addAll(listLongFrais);
        listLongTotal.addAll(listLongForfait);
        listLongTotal.addAll(listLongAvance);

        hashset.addAll(listLongTotal);
        listLongTotalDistinct = new ArrayList<>(hashset);
        return listLongTotalDistinct;
    }

    /**
     * fonction qui recupere la liste des id collab pour chaque map de frais
     *
     * @param montantFrais
     * @param montantForfaits
     * @param montantAvances
     * @return la liste des id collab
     */
    private List<Long> getListIdCollaborateurTotalDetail(Map<Long, Map<Long, Object[]>> montantFrais, Map<Long, Map<Long, Float>> montantForfaits, Map<Long, Object[]> montantAvances) {
        // on recupere les id collab List
        HashSet<Long> hashset = new HashSet<>();
        List<Long> listLongTotalDistinct;

        List<Long> listLongFrais;
        listLongFrais = map2ListDetailObject(montantFrais);

        List<Long> listLongForfait;
        listLongForfait = map2ListDetail(montantForfaits);

        List<Long> listLongAvance;
        listLongAvance = map2ListAvance(montantAvances);

        //On concatene les resultats
        List<Long> listLongTotal = new ArrayList<>();
        listLongTotal.addAll(listLongFrais);
        listLongTotal.addAll(listLongForfait);
        listLongTotal.addAll(listLongAvance);

        hashset.addAll(listLongTotal);
        listLongTotalDistinct = new ArrayList<>(hashset);
        return listLongTotalDistinct;
    }

}
