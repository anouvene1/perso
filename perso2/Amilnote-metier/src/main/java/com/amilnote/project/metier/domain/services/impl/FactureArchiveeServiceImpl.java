/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.FactureArchiveeDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FactureArchiveeAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilder;
import com.amilnote.project.metier.domain.pdf.PDFBuilderFacture;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.*;
import com.amilnote.project.metier.domain.services.FactureArchiveeService;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import com.amilnote.project.metier.domain.utils.Utils;
import com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator;
import com.itextpdf.text.DocumentException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import javax.naming.NamingException;
import java.util.*;

import static com.amilnote.project.metier.domain.utils.Constantes.EXTENSION_FILE_PDF;
import static com.amilnote.project.metier.domain.utils.Constantes.FRENCH_MONTHS_NAMES;
import static com.amilnote.project.metier.domain.utils.Constantes.SLASH;
import static com.amilnote.project.metier.domain.utils.Utils.concatenateObjectsToString;
import static com.amilnote.project.metier.domain.utils.Constantes.UNDERSCORE;

/**
 * The type Facture archivee service.
 */
@Service("factureArchiveeService")
public class FactureArchiveeServiceImpl extends AbstractServiceImpl<FactureArchivee, FactureArchiveeDAO> implements FactureArchiveeService {

    @Autowired
    private FactureService factureService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private ContactClientService contactClientService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private PDFBuilderFacture pdfBuilderFacture;

    /**
     * The Facture archivee dao.
     */
    @Autowired
    private FactureArchiveeDAO factureArchiveeDAO;

    /**
     * {@linkplain FactureArchiveeService#findAllFacturesArchivees()}
     */
    @Override
    public List<FactureArchivee> findAllFacturesArchivees() {
        return factureArchiveeDAO.findAllFacturesArchivees();
    }

    /**
     * {@linkplain FactureArchiveeService#findMaxNumFacture()}
     */
    @Override
    public int findMaxNumFacture() {
        return factureArchiveeDAO.findMaxNumFacture();
    }

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public FactureArchiveeDAO getDAO() {
        return factureArchiveeDAO;
    }

    /**
     * {@linkplain FactureArchiveeService#findById(int)}
     */
    @Override
    public FactureArchivee findById(int pIdFactureArchivee) {
        return factureArchiveeDAO.findUniqEntiteByProp(FactureArchivee.PROP_ID_FACTURE_ARCHIVEE, pIdFactureArchivee);
    }


    /**
     * {@linkplain FactureArchiveeService#createOrUpdateFactureArchivee(FactureArchiveeAsJson)}
     */
    @Override
    public String createOrUpdateFactureArchivee(FactureArchiveeAsJson pFactureArchiveeAsJson) {
        return factureArchiveeDAO.createOrUpdateFactureArchivee(pFactureArchiveeAsJson);
    }

    /**
     * Retrieve the files paths ({@link FactureArchivee#getCheminPDF()}) of each archived invoices from a list of archived invoices
     * @param factureArchiveeList a list of archived invoices
     * @return a String array of each {@link FactureArchivee#getCheminPDF()} of the list
     */
    public String[] extractPdfFilesPaths(List<FactureArchivee> factureArchiveeList) {
        String[] pdfFilesPaths = new String[factureArchiveeList.size()];
        for (int i=0; i<factureArchiveeList.size(); i++){
            pdfFilesPaths[i] = factureArchiveeList.get(i).getCheminPDF();
        }
        return pdfFilesPaths;
    }

    /**
     * Retrieve all archived invoices pdf files related to a given month and year and merge them to one pdf
     * @param yearOfSearch all {@link FactureArchivee} which {@link FactureArchivee#getDate_archivage()}'s year equals this year
     * @param monthOfSearch all {@link FactureArchivee} which {@link FactureArchivee#getDate_archivage()}'s month equals this year
     * @throws IOException when no pdf file of archived invoices for the given month found on storage
     */
    @Override
    public String mergeArchivedInvoicesPdfFiles(Integer yearOfSearch, String monthOfSearch) throws IOException, NamingException {

        String resultFileDestinationPath = concatenateObjectsToString(
                Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT),
                monthOfSearch,
                UNDERSCORE,
                yearOfSearch
        );

        DateTime[] startEndSearchDates = Utils.firstAndLastDatesOfMonth(yearOfSearch, monthOfSearch, FRENCH_MONTHS_NAMES);

        // Récuperer la liste de toutes les factures archivées
        List<FactureArchivee> factureArchiveeList = findBySearchCriterias (
                FactureArchivee.PROP_NUMERO_FACTURE_ARCHIVEE,
                true,
                new SearchCriteria<>(
                        FactureArchivee.PROP_DATE_ARCHIVAGE,
                        ComparisonOperator.BETWEEN,
                        startEndSearchDates[0].toDate(),
                        startEndSearchDates[1].toDate()
                )
        );

        // Combiner les fichiers pdf récupérés à partir du tableau de chemins pdf en un seul fichier pdf stocké temporairement sur le disque
        PDFBuilder.mergePdfFilesByFilesPath(
                resultFileDestinationPath + EXTENSION_FILE_PDF,
                false,
                extractPdfFilesPaths(factureArchiveeList)
        );

        return resultFileDestinationPath;
    }

    /**
     * Find all archived invoices which {@link FactureArchivee#getDate_archivage()} are between given start end dates of search
     * @param startSearchDate the start date of the between search
     * @param endSearchDate the end date of the between search
     * @return a list of archived invoiced which {@link FactureArchivee#getDate_archivage()} are between given start end dates of search
     */
    @Override
    public List<FactureArchivee> findByStartEndDates(Date startSearchDate, Date endSearchDate) {
        return findBySearchCriterias(
                FactureArchivee.PROP_NUMERO_FACTURE_ARCHIVEE,
                true,
                new SearchCriteria(FactureArchivee.PROP_DATE_ARCHIVAGE, ComparisonOperator.BETWEEN, startSearchDate, endSearchDate)
        );
    }
    /**
     * method which archive a given invoice, extract elements and save an archived invoices containing those informations
     *
     * @param resultDirectoryPath the full path to the directory which will include all the archived invoices's pdf
     * @param invoice the invoice to archive
     * @param archivingDate archiving date
     * @param odfFolderName the folder which will include all customers sub folders
     * @throws IOException local/network file/folder read/write problems
     * @throws DocumentException exception related to pdf read/write problems
     */
    public void archiveInvoice(String resultDirectoryPath, Facture invoice, DateTime archivingDate, String odfFolderName) throws IOException, DocumentException {

        //On récupère les informations
        List<ElementFacture> elements = elementFactureService.findByFacture(invoice);

        Mission mission = invoice.getMission();

        Commande commande = invoice.getCommande() != null ? invoice.getCommande() : new Commande();

        Client client = invoice.getCommande() != null ? invoice.getCommande().getClient() : mission.getClient();

        ContactClient contactClient = null;
        if (commande.getIdResponsableFacturation() != null || invoice.getContact() != 0) {
            if (commande.getIdResponsableFacturation() != null) {
                contactClient = contactClientService.findById(commande.getIdResponsableFacturation()); // On récupère le contact
            } else if (invoice.getContact() != 0) { // Si le contact n'est pas null
                contactClient = contactClientService.findById(Long.valueOf(invoice.getContact())); // On récupère le contact
            }
        }

        String societyName = client.getNom_societe();
        // On met le chemin du PDF créé dans la facture
        if (client.getNom_societe().contains(SLASH))
            societyName = societyName.replace(SLASH, "");

        String societyDirectoryPath = concatenateObjectsToString(
                resultDirectoryPath,
                SLASH,
                societyName.trim()
        );

        File societyDirectory = new File(societyDirectoryPath);
        societyDirectory.mkdirs();

        invoice.setPdf( concatenateObjectsToString(
                societyDirectoryPath,
                "/Fac_",
                invoice.getNumFacture(),
                Constantes.EXTENSION_FILE_PDF
        ));

        invoice.setEtat(etatService.getEtatByCode(Etat.ETAT_ARCHIVEE));
        factureService.createOrUpdateFacture(invoice); // On sauvegarde la facture

        // On crée le PDF de la facture
        pdfBuilderFacture.createPdfFacture(
                mission,
                invoice,
                elements,
                invoice.getContact(),
                false,
                odfFolderName,
                archivingDate,
                client
        );

        FactureArchivee factureArchivee;
        Pair<Float, Float> quantiteEtMontantAvecElementsFacture = factureService.getQuantiteMontantAvecFrais(invoice);
        factureArchivee = new FactureArchivee(
                client.getNom_societe(),
                mission.getCollaborateur().getNom() + " " + mission.getCollaborateur().getPrenom(),
                mission.getMission(),
                contactClient != null ? contactClient.getNom() + " " + contactClient.getPrenom() : "",
                invoice.getCommande() != null ? invoice.getCommande().getNumero() : "Suivant proposition commerciale" ,
                invoice.getPdf(),
                quantiteEtMontantAvecElementsFacture.getFirst(),
                invoice.getPrix(),
                quantiteEtMontantAvecElementsFacture.getSecond(),
                invoice.getNumFacture(),
                archivingDate.toDate(),
                invoice.getTypeFacture().getId().intValue(),
                invoice.getIsAvoir()
        );

        // On met en BD la facture archivée
        createOrUpdateFactureArchivee(factureArchivee.toJson());
    }

}
