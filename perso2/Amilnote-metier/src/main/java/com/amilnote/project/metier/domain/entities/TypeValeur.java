/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.TypeValeurAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Type valeur.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_type_valeur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class TypeValeur implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPE_VALEUR.
     */
    public static final String PROP_TYPE_VALEUR = "typeValeur";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_NON_CHAMP.
     */
    public static final String PROP_NON_CHAMP = "nomChamp";
    private static final long serialVersionUID = -6808514250677769837L;
    private Long id;
    private String typeValeur;
    private String code;
    private String nomChamp;

    /**
     * Instantiates a new Type valeur.
     */
    public TypeValeur() {
    }

    /**
     * Instantiates a new Type valeur.
     *
     * @param pTypeValeur the p type valeur
     * @param pCode       the p code
     * @param pNomChamp   the p nom champ
     */
    public TypeValeur(String pTypeValeur, String pCode, String pNomChamp) {
        super();
        typeValeur = pTypeValeur;
        code = pCode;
        nomChamp = pNomChamp;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }


    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets type valeur.
     *
     * @return the type valeur
     */
    @Column(name = "type_val", nullable = false)
    public String getTypeValeur() {
        return typeValeur;
    }

    /**
     * Sets type valeur.
     *
     * @param pTypeValeur the p type valeur
     */
    public void setTypeValeur(String pTypeValeur) {
        typeValeur = pTypeValeur;
    }

    /**
     * Gets nom champ.
     *
     * @return the nom champ
     */
    @Column(name = "nom_champ", nullable = false)
    public String getNomChamp() {
        return nomChamp;
    }

    /**
     * Sets nom champ.
     *
     * @param pNomChamp the p nom champ
     */
    public void setNomChamp(String pNomChamp) {
        nomChamp = pNomChamp;
    }


    /**
     * To json type valeur as json.
     *
     * @return the type valeur as json
     */
    public TypeValeurAsJson toJson() {
        return new TypeValeurAsJson(this);
    }
}
