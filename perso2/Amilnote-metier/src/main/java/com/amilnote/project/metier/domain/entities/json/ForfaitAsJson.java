/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Forfait;


/**
 * The type Forfait as json.
 */
public class ForfaitAsJson {


    private Long id;

    private String nom;

    private FrequenceAsJson frequence;

    private Boolean justif;

    private String code;


    /**
     * Constructeur par defaut de la classe LinkMissionForfait
     */
    public ForfaitAsJson() {
    }

    ;

    /**
     * Instantiates a new Forfait as json.
     *
     * @param forfait the forfait
     */
    public ForfaitAsJson(Forfait forfait) {
        setNom(forfait.getNom());
        setId(forfait.getId());
        setJustif(forfait.getJustif());
        setCode(forfait.getCode());
        setFrequence(forfait.getFrequence().toJson());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param pNom the nom to set
     */
    public void setNom(String pNom) {
        nom = pNom;
    }

    /**
     * Gets justif.
     *
     * @return the justif
     */
    public Boolean getJustif() {
        return justif;
    }

    /**
     * Sets justif.
     *
     * @param pJustif the justif to set
     */
    public void setJustif(Boolean pJustif) {
        justif = pJustif;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        code = pCode;
    }

    /**
     * Gets frequence.
     *
     * @return the frquence
     */
    public FrequenceAsJson getFrequence() {
        return frequence;
    }

    /**
     * Sets frequence.
     *
     * @param pFrquence the frquence to set
     */
    public void setFrequence(FrequenceAsJson pFrquence) {
        frequence = pFrquence;
    }

}
