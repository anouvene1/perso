/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TypeMission;

/**
 * The type Type mission as json.
 */
public class TypeMissionAsJson {
    private long id;
    private String typeMission;
    private String code;

    /**
     * Instantiates a new Type mission as json.
     */
    public TypeMissionAsJson() {
    }

    /**
     * Instantiates a new Type mission as json.
     *
     * @param pTypeMission the p type mission
     */
    public TypeMissionAsJson(TypeMission pTypeMission) {
        id = pTypeMission.getId();
        typeMission = pTypeMission.getTypeMission();
        code = pTypeMission.getCode();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(long pId) {
        id = pId;
    }

    /**
     * Gets type mission.
     *
     * @return the type mission
     */
    public String getTypeMission() {
        return typeMission;
    }

    /**
     * Sets type mission.
     *
     * @param pMission the p mission
     */
    public void setTypeMission(String pMission) {
        typeMission = pMission;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        code = pCode;
    }

}
