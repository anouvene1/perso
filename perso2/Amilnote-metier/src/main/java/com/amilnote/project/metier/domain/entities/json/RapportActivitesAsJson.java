/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.RapportActivites;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Rapport activites as json.
 */
public class RapportActivitesAsJson {

    private Long id;
    private Date dateSoumission;
    private Date dateValidation;
    private String pdfRapportActivite;
    private Etat etat;
    private List<LinkEvenementTimesheet> linkEvenementTimesheets = new ArrayList<LinkEvenementTimesheet>();
    private CollaboratorAsJson collaborateur;
    private Date moisRapport;
    private String commentaire;

    /**
     * Instantiates a new Rapport activites as json.
     *
     * @param pRapportActivite the p rapport activite
     */
    public RapportActivitesAsJson(RapportActivites pRapportActivite) {
        this.setCollaborateur(pRapportActivite.getCollaborateur().toJson());
        this.setDateSoumission(pRapportActivite.getDateSoumission());
        this.setDateValidation(pRapportActivite.getDateValidation());
        this.setEtat(pRapportActivite.getEtat());
        this.setId(pRapportActivite.getId());
        this.setLinkEvenementTimesheets(pRapportActivite.getLinkEvenementTimesheets());
        this.setPdfRapportActivite(pRapportActivite.getPdfRapportActivite());
        this.setMoisRapport(pRapportActivite.getMoisRapport());
        this.setCommentaire(pRapportActivite.getCommentaire());
    }

    /**
     * Instantiates a new Rapport activites as json.
     */
    public RapportActivitesAsJson() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets date soumission.
     *
     * @return the date soumission
     */
    public Date getDateSoumission() {
        return dateSoumission;
    }

    /**
     * Sets date soumission.
     *
     * @param pDateSoumission the p date soumission
     */
    public void setDateSoumission(Date pDateSoumission) {
        dateSoumission = pDateSoumission;
    }

    /**
     * Gets date validation.
     *
     * @return the date validation
     */
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param pDateValidation the p date validation
     */
    public void setDateValidation(Date pDateValidation) {
        dateValidation = pDateValidation;
    }

    /**
     * Gets pdf rapport activite.
     *
     * @return the pdf rapport activite
     */
    public String getPdfRapportActivite() {
        return pdfRapportActivite;
    }

    /**
     * Sets pdf rapport activite.
     *
     * @param pPdfRapportActivite the p pdf rapport activite
     */
    public void setPdfRapportActivite(String pPdfRapportActivite) {
        pdfRapportActivite = pPdfRapportActivite;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(Etat pEtat) {
        etat = pEtat;
    }

    /**
     * Gets link evenement timesheets.
     *
     * @return the link evenement timesheets
     */
    public List<LinkEvenementTimesheet> getLinkEvenementTimesheets() {
        return linkEvenementTimesheets;
    }

    /**
     * Sets link evenement timesheets.
     *
     * @param pLinkEvenementTimesheets the p link evenement timesheets
     */
    public void setLinkEvenementTimesheets(List<LinkEvenementTimesheet> pLinkEvenementTimesheets) {
        linkEvenementTimesheets = pLinkEvenementTimesheets;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public CollaboratorAsJson getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param pCollaborateur the p collaborateur
     */
    public void setCollaborateur(CollaboratorAsJson pCollaborateur) {
        collaborateur = pCollaborateur;
    }

    /**
     * Gets mois rapport.
     *
     * @return the moisRapport
     */
    public Date getMoisRapport() {
        return moisRapport;
    }

    /**
     * Sets mois rapport.
     *
     * @param pMoisRapport the moisRapport to set
     */
    public void setMoisRapport(Date pMoisRapport) {
        moisRapport = pMoisRapport;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the commentaire to set
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }

    ;

}
