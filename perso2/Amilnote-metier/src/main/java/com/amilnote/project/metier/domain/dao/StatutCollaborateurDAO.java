/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.StatutCollaborateur;

/**
 * The interface Statut collaborateur dao.
 */
public interface StatutCollaborateurDAO extends LongKeyDAO<StatutCollaborateur> {
    StatutCollaborateur findStatutCollaborateurByCode(String code);
}
