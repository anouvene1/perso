/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.ElementFacture;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;

/**
 * The interface Element facture service.
 */
public interface ElementFactureService extends AbstractService<ElementFacture> {

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pElementFactureAsJson the p element facture as json
     * @param pFacture          the p facture
     * @return int int
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    int createOrUpdateElementFacture(ElementFactureAsJson pElementFactureAsJson, Facture pFacture) throws JsonProcessingException, IOException;

    /**
     * Suppression de la commande
     *
     * @param pElementFacture the p element facture
     * @return string string
     */
    String deleteElementFacture(ElementFacture pElementFacture);

    /**
     * Find all order by nom asc list.
     *
     * @return the list
     */
    List<ElementFacture> findAllOrderByNomAsc();

    /**
     * Find by id element facture.
     *
     * @param pIdElementFacture the p id element facture
     * @return the element facture
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    ElementFacture findById(int pIdElementFacture);

    /**
     * Récupération de tous les éléments en fonction d'une facture
     *
     * @param pFacture the p facture
     * @return list list
     */
    List<ElementFacture> findByFacture(Facture pFacture);

    /**
     * retrouver toutes les commandes et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<ElementFactureAsJson> findAllOrderByNomAscAsJson();

    /**
     * Method which called after validating invoices, which consisting of deleting all invoices's fields
     * @param invoiceElementsList the elements to be deleted
     */
    void deleteInvoiceElements(List<ElementFacture> invoiceElementsList) ;
}
