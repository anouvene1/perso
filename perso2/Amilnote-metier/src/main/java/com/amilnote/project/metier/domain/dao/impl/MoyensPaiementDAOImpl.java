/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.MoyensPaiementDAO;
import com.amilnote.project.metier.domain.entities.MoyensPaiement;
import com.amilnote.project.metier.domain.entities.json.MoyensPaiementAsJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Moyens paiement dao.
 */
@Repository("moyensPaiementDAO")
public class MoyensPaiementDAOImpl extends AbstractDAOImpl<MoyensPaiement> implements MoyensPaiementDAO {

    @Autowired
    private MissionDAOImpl missionDAO;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<MoyensPaiement> getReferenceClass() {
        return MoyensPaiement.class;
    }

    /**
     * {@linkplain MoyensPaiementDAO#createOrUpdateMoyensPaiement(MoyensPaiementAsJson)}
     */
    @Override
    public int createOrUpdateMoyensPaiement(MoyensPaiementAsJson paiementJson) {

        return 0;
    }

    /**
     * {@linkplain MoyensPaiementDAO#deleteMoyensPaiement(MoyensPaiement)}
     */
    @Override
    public String deleteMoyensPaiement(MoyensPaiement paiement) {

        return "Aucune suppression n'a été effectuée";
    }

    /**
     * {@linkplain MoyensPaiementDAO#findMoyensPaiement()}
     */
    @Override
    public List<MoyensPaiement> findMoyensPaiement() {
        List<MoyensPaiement> moyensPaiement = new ArrayList<>();
        return moyensPaiement;
    }
}
