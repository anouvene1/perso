/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ForfaitDAO;
import com.amilnote.project.metier.domain.dao.LinkMissionForfaitDAO;
import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.LinkMissionForfait;
import com.amilnote.project.metier.domain.entities.Mission;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Link mission forfait dao.
 */
@Repository("linkMissionForfaitDAO")
public class LinkMissionForfaitDAOImpl extends AbstractDAOImpl<LinkMissionForfait> implements LinkMissionForfaitDAO {

    private static String CODE_LIB = "LIB";

    @Autowired
    private ForfaitDAO forfaitDAO;


    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<LinkMissionForfait> getReferenceClass() {
        return LinkMissionForfait.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LinkMissionForfait> findByMission(Mission pMission) {
        Criteria lCritMission = getCriteria();
        lCritMission.add(Restrictions.eq(Mission.PROP_ID, pMission.getId()));
        return lCritMission.list();
    }

    @Override
    public List<LinkMissionForfait> findLib() {
        Criteria criteria = getCriteria();
        List<Forfait> forfaitsLib = forfaitDAO.findForfaitLib();

        for (Forfait forfaitLib : forfaitsLib) {
            criteria.add(Restrictions.eq(LinkMissionForfait.PROP_FORFAIT, forfaitLib));
        }

        return criteria.list();
    }

    @Override
    public List<LinkMissionForfait> findNotLib() {

        List<Forfait> forfaitsLib = forfaitDAO.findForfaitLib();
        Criteria criteria = getCriteria();
        for (Forfait forfaitLib : forfaitsLib) {
            criteria.add(Restrictions.ne(LinkMissionForfait.PROP_FORFAIT, forfaitLib));
        }
        return criteria.list();
    }

    @Override
    public List<LinkMissionForfait> findLibByMission(Mission pMission) {
        Criteria crit = getCriteria();
        crit.list();
        crit.add(Restrictions.eq(Mission.PROP_MISSION, pMission));
        List<Forfait> forfaitsLib = forfaitDAO.findForfaitNotLib();
        for (Forfait forfaitLib : forfaitsLib) {
            crit.add(Restrictions.ne(LinkMissionForfait.PROP_FORFAIT, forfaitLib));
        }
        return crit.list();
    }

    @Override
    public List<LinkMissionForfait> findNotLibByMission(Mission pMission) {
        Criteria crit = getCriteria();
        crit.add(Restrictions.eq(Mission.PROP_MISSION, pMission));
        List<Forfait> forfaitsLib = forfaitDAO.findForfaitLib();
        for (Forfait forfaitLib : forfaitsLib) {
            crit.add(Restrictions.ne(LinkMissionForfait.PROP_FORFAIT, forfaitLib));
        }
        return crit.list();
    }
}
