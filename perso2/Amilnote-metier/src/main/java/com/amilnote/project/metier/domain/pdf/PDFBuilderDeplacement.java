/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.dao.StatutCollaborateurDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import static com.amilnote.project.metier.domain.utils.Constantes.PDF_HEADER_BLUE_COLOR;

/**
 * The type Pdf builder deplacement.
 */
@Service("PDFBuilderDeplacemnt")
public class PDFBuilderDeplacement extends PDFBuilder {

    private static final Logger logger = LogManager.getLogger(PDFBuilderDeplacement.class);

    @Autowired
    private CollaboratorDAO collaboratorDAO;

    @Autowired
    private StatutCollaborateurDAO statutCollaborateurDAO;

    /**
     * Create pdf demande deplacement string.
     *
     * @param pCollaborator      the p collaborateur
     * @param pDemandeDeplacement the p demande deplacement
     * @return the string
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     * @throws NamingException    the naming exception
     */
    public String createPDFDemandeDeplacement(Collaborator pCollaborator, DemandeDeplacementAsJson pDemandeDeplacement) throws IOException, SchedulerException, EmailException, MessagingException, DocumentException, NamingException {

        logger.debug("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");

        String nomFichier = "";
        String[] paramsNomFichier = {
                Long.toString(pDemandeDeplacement.getId()),
                pCollaborator.getNom()
        };

        //Mise en forme du nom du fichier
        nomFichier = properties.get("pdf.nomFichierDeplacement", paramsNomFichier);
        String chemin = Parametrage.getContext("dossierPDFDEPLACEMENT") + nomFichier;

        File directory = new File(chemin).getParentFile(); //on récupère les dossiers parents
        directory.mkdirs(); //s'il en manque on les crée

        FileOutputStream lBaos = new FileOutputStream(chemin);
        pDemandeDeplacement.setPdfDeplacement(nomFichier); //on mémorise le nom pour pouvoir visulaiser le fichier dans la partie admin

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;

        lWriter = newWriter(lDocument, lBaos);
        TableHeader lEvent = new TableHeader();
        lEvent.setHeader("Demande de déplacement collaborateur: " + "\n" + pCollaborator.getNom());

        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);

        // ------------------------------------------------------//
        // CONSTITUTION DU CONTENU DU PDF DEMANDE DE DEPLACEMENT //
        // -----------------------------------------------------//
        // ouverture du document PDF
        lDocument.open();

        Date today = new Date();
        // table date courante
        lDocument.add(createTableDateCreation("DATE DE CREATION DE LA DEMANDE DE DEPLACEMENT", today));
        // table collaborateur & responsable
        lDocument.add(createTableUser(pCollaborator));
        //Table de détail de la demande
        lDocument.add(createTableDetailDeplacement(pDemandeDeplacement));

        // fermeture du document pdf
        lDocument.close();
        logger.debug("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");

        return nomFichier;
    }

    private PdfPTable createTableDetailDeplacement(DemandeDeplacementAsJson pDemandeDeplacement) {
        PdfPTable lTableDetail = new PdfPTable(2);
        lTableDetail.setWidthPercentage(100);
        lTableDetail.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableDetail.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableDetail.getDefaultCell().setColspan(2);
        lTableDetail.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableDetail.addCell(new Phrase("DETAIL DE LA DEMANDE DE DEPLACEMENT", fontHeaderWhite));
        lTableDetail.getDefaultCell().setColspan(1);
        lTableDetail.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        try {
            lTableDetail.setWidths(new int[]{50, 50});
        } catch (DocumentException e) {
            logger.error("[create table travel] set widths", e);
        }

        //On rempli le tableau avec les différentes informations présentes dans la bdd
        if (null != pDemandeDeplacement.getDateDebut()) {
            lTableDetail.addCell(new Phrase("Début du déplacement: ", FONT_BOLD));
            lTableDetail.addCell(DATE_FORMATER.format(pDemandeDeplacement.getDateDebut()));
        }

        if (null != pDemandeDeplacement.getDateFin()) {
            lTableDetail.addCell(new Phrase("Fin du déplacement: ", FONT_BOLD));
            lTableDetail.addCell(DATE_FORMATER.format(pDemandeDeplacement.getDateFin()));
        }

        if (null != pDemandeDeplacement.getMission()) {
            lTableDetail.addCell(new Phrase("Mission: ", FONT_BOLD));
            lTableDetail.addCell(pDemandeDeplacement.getMission().getMission());
        }

        if (null != pDemandeDeplacement.getMotif() && !(pDemandeDeplacement.getMotif()).equals("")) {
            lTableDetail.addCell(new Phrase("Motif: ", FONT_BOLD));
            lTableDetail.addCell(pDemandeDeplacement.getMotif());
        }

        if (null != pDemandeDeplacement.getCodeProjet() && !(pDemandeDeplacement.getCodeProjet()).equals("")) {
            lTableDetail.addCell(new Phrase("Code Projet Client: ", FONT_BOLD));
            lTableDetail.addCell(pDemandeDeplacement.getCodeProjet());
        }

        if (null != pDemandeDeplacement.getLieu()) {
            lTableDetail.addCell(new Phrase("Lieu: ", FONT_BOLD));
            lTableDetail.addCell(pDemandeDeplacement.getLieu());
        }

        if (null != pDemandeDeplacement.getCommentaire() && !(pDemandeDeplacement.getCommentaire()).equals("")) {
            lTableDetail.addCell(new Phrase("Commentaire: ", FONT_BOLD));
            lTableDetail.addCell(pDemandeDeplacement.getCommentaire());
        }
        return lTableDetail;
    }

}

