/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ForfaitAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Forfait.
 */
@Entity
@Table(name = "ami_forfait")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Forfait implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_NOM.
     */
    public static final String PROP_NOM = "nom";
    /**
     * The constant PROP_FREQUENCE.
     */
    public static final String PROP_FREQUENCE = "frequence";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant CODE_REPAS.
     */
    public static final String CODE_REPAS = "REP";
    /**
     * The constant CODE_DEPLACEMENT.
     */
    public static final String CODE_DEPLACEMENT = "DEP";
    /**
     * The constant CODE_TELEPHONE.
     */
    public static final String CODE_TELEPHONE = "TEL";
    /**
     * The constant CODE_LOGEMENT.
     */
    public static final String CODE_LOGEMENT = "LOG";
    /**
     * The constant CODE_LIBRE.
     */
    public static final String CODE_LIBRE = "LIB";
    /**
     * The constant CODE_TRANSPORTS_EN_COMMUN.
     */
    public static final String CODE_TRANSPORTS_EN_COMMUN = "LIB_TCM";

    private static final long serialVersionUID = 245548726734692240L;
    private Long id;
    private String nom;
    private Frequence frequence;
    private Boolean justif;
    private String code;

    /**
     * Constructeur par defaut de la classe Forfait
     */
    public Forfait() {
    }

    /**
     * Instantiates a new Forfait.
     *
     * @param pForfaitNom the p forfait nom
     * @param pFrequence  the p frequence
     * @param pJustif     the p justif
     * @param pCode       the p code
     */
    public Forfait(String pForfaitNom, Frequence pFrequence, Boolean pJustif, String pCode) {
        super();
        nom = pForfaitNom;
        this.setFrequence(pFrequence);
        setJustif(pJustif);
        this.setCode(pCode);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    @Column(name = "nom", nullable = false)
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param pForfaitNom the p forfait nom
     */
    public void setNom(String pForfaitNom) {
        nom = pForfaitNom;
    }

    /**
     * retourne la frequence du forfait
     *
     * @return un objet de type Frequence
     */
    @ManyToOne
    @JoinColumn(name = "id_frequence", nullable = false)
    public Frequence getFrequence() {
        return this.frequence;
    }

    /**
     * Sets frequence.
     *
     * @param pFrequence the p frequence
     */
    public void setFrequence(Frequence pFrequence) {
        this.frequence = pFrequence;
    }

    /**
     * Gets justif.
     *
     * @return the justif
     */
    public Boolean getJustif() {
        return justif;
    }

    /**
     * Sets justif.
     *
     * @param pJustif the justif to set
     */
    @Column(name = "justif", nullable = false)
    public void setJustif(Boolean pJustif) {
        justif = pJustif;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        code = pCode;
    }

    /**
     * To json forfait as json.
     *
     * @return the forfait as json
     */
    public ForfaitAsJson toJson() {
        return new ForfaitAsJson(this);
    }
}
