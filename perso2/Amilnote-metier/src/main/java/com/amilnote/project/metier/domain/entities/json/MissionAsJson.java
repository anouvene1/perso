/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.LinkMissionForfait;
import com.amilnote.project.metier.domain.entities.Mission;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Mission as json.
 */
public class MissionAsJson {
    private Long id;

    private String collaborateur;

    private String mission;

    private Date dateDebut;

    private Date dateFin;

    private TypeMissionAsJson typeMission;

    private List<LinkEvenementTimesheetAsJson> listLinkEvenementTimesheetAsJson = new ArrayList<>();
    private List<LinkMissionForfaitAsJson> listMissionForfait = new ArrayList<>();
    private List<FraisAsJson> listFrais = new ArrayList<>();

    private String pdfODM;
    private String description;
    private float heuresHebdo;
    private String suivi; //mensuel ou trimestriel

    @JsonIgnore
    private ClientAsJson client;

    @JsonIgnore
    private ContactClientAsJson responsableClient;
    private EtatAsJson etat;

    private String factureExcel;
    private List<CommandeAsJson> commandesAsJson = new ArrayList<>();
    private float tjm;

    private OutilsInterneAsJson outilsInterne;

    private List<LinkPerimetreMissionAsJson> perimetreMissionAsJson = new ArrayList<>();

    private AgenceAsJson agence;

    @JsonIgnore
    private CollaboratorAsJson manager;


    /**
     * Instantiates a new Mission as json.
     *
     * @param lMission the l mission
     */
    public MissionAsJson(Mission lMission) {
        this.setId(lMission.getId());
        this.mission = lMission.getMission();
        this.collaborateur = lMission.getCollaborateur().getMail();
        this.setDateDebut(lMission.getDateDebut());
        this.setDateFin(lMission.getDateFin());
        this.setTypeMission(new TypeMissionAsJson(lMission.getTypeMission()));

        if (lMission.getOutilsInterne() != null) {
            this.setOutilsInterne(new OutilsInterneAsJson(lMission.getOutilsInterne()));
        }

        if (lMission.getDescription() != null) {
            this.setDescription(lMission.getDescription());
        }
        if (lMission.getDuree_hebdo() != null) {
            this.setHeuresHebdo(lMission.getDuree_hebdo());
        }
        if (lMission.getSuivi() != null) {
            this.setSuivi(lMission.getSuivi());
        }
        if (lMission.getResp_client() != null) {
            this.setResponsableClient(lMission.getResp_client().toJson());
        }
        if (lMission.getClient() != null) {
            this.setClient(lMission.getClient().toJson());
        }
        if (lMission.getEtat() != null) {
            this.setEtat(new EtatAsJson(lMission.getEtat()));
        }
        if (lMission.getTjm() != null) {
            this.setTjm(lMission.getTjm());
        }
        if(lMission.getAgence() != null) {
            this.setAgence(lMission.getAgence().toJson());
        }

        if(lMission.getManager() !=null){
            this.setManager(lMission.getManager().toJson());
        }

    }

    /**
     * Instantiates a new Mission as json.
     */
    public MissionAsJson() {
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public String getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param pCollaborateur the p collaborateur
     */
    public void setCollaborateur(String pCollaborateur) {
        collaborateur = pCollaborateur;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(String pMission) {
        mission = pMission;
    }

    /**
     * Gets date debut.
     *
     * @return the date debut
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param pDateDebut the p date debut
     */
    public void setDateDebut(Date pDateDebut) {
        dateDebut = pDateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return the date fin
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param pDateFin the p date fin
     */
    public void setDateFin(Date pDateFin) {
        dateFin = pDateFin;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets type mission.
     *
     * @return the type mission
     */
    public TypeMissionAsJson getTypeMission() {
        return typeMission;
    }

    /**
     * Sets type mission.
     *
     * @param pTypeMission the p type mission
     */
    public void setTypeMission(TypeMissionAsJson pTypeMission) {
        typeMission = pTypeMission;
    }

    /**
     * Gets list link evenement timesheet as json.
     *
     * @return the listLinkEvenementTimesheetAsJson
     */
    public List<LinkEvenementTimesheetAsJson> getListLinkEvenementTimesheetAsJson() {
        return listLinkEvenementTimesheetAsJson;
    }

    /**
     * Sets list link evenement timesheet as json.
     *
     * @param pListLinkEvenementTimesheet the p list link evenement timesheet
     */
    public void setListLinkEvenementTimesheetAsJson(List<LinkEvenementTimesheetAsJson> pListLinkEvenementTimesheet) {
        this.listLinkEvenementTimesheetAsJson = pListLinkEvenementTimesheet;

    }

    /**
     * Sets list link evenement timesheet.
     *
     * @param pListLinkEvenementTimesheet the p list link evenement timesheet
     */
    public void setListLinkEvenementTimesheet(List<LinkEvenementTimesheet> pListLinkEvenementTimesheet) {

        List<LinkEvenementTimesheetAsJson> tmpList = new ArrayList<>();

        for (LinkEvenementTimesheet event : pListLinkEvenementTimesheet) {

            LinkEvenementTimesheetAsJson tmp = new LinkEvenementTimesheetAsJson();
            tmp.constructForFrais(event);
            tmpList.add(tmp);

        }
        this.setListLinkEvenementTimesheetAsJson(tmpList);
    }

    /**
     * Sets list link evenement timesheet.
     *
     * @param pListLinkEvenementTimesheet the p list link evenement timesheet
     * @param pStart                      the p start
     * @param pEnd                        the p end
     */
    public void setListLinkEvenementTimesheet(List<LinkEvenementTimesheet> pListLinkEvenementTimesheet, DateTime pStart, DateTime pEnd) {
        List<LinkEvenementTimesheetAsJson> tmpList = new ArrayList<>();

        for (LinkEvenementTimesheet event : pListLinkEvenementTimesheet) {
            if (new DateTime(event.getDate()).isAfter(pStart) && new DateTime(event.getDate()).isBefore((pStart))) {

                LinkEvenementTimesheetAsJson tmp = new LinkEvenementTimesheetAsJson();
                tmp.constructForFrais(event);
                tmpList.add(tmp);

            }
        }
        this.setListLinkEvenementTimesheetAsJson(tmpList);
    }

    /**
     * Gets list mission forfait.
     *
     * @return the listMissionForfait
     */
    public List<LinkMissionForfaitAsJson> getListMissionForfait() {
        return listMissionForfait;
    }

    /**
     * Sets list mission forfait.
     *
     * @param pListMissionForfait the p list mission forfait
     */
    public void setListMissionForfait(List<LinkMissionForfait> pListMissionForfait) {
        for (LinkMissionForfait linkMissionForfait : pListMissionForfait) {
            this.getListMissionForfait().add(new LinkMissionForfaitAsJson(linkMissionForfait));
        }
    }

    /**
     * Sets list mission forfait as json.
     *
     * @param pListMissionForfait the listMissionForfait to set
     */
    public void setListMissionForfaitAsJson(List<LinkMissionForfaitAsJson> pListMissionForfait) {
        listMissionForfait = pListMissionForfait;
    }

    /**
     * Gets list frais.
     *
     * @return the frais
     */
    public List<FraisAsJson> getListFrais() {
        return listFrais;
    }


    /**
     * Sets list frais as json.
     *
     * @param pFrais the frais to set
     */
    public void setListFraisAsJson(List<FraisAsJson> pFrais) {
        listFrais = pFrais;
    }

    /**
     * Initialise la listes des FraisAsJson à partir d'une liste d'éléments qui sont des tableaux d'objets
     *
     * @param pFrais the p frais
     */
    public void setListFraisAsObject(List<Object[]> pFrais) {
        for (Object[] e : pFrais) {
            this.getListFrais().add(new FraisAsJson(e));
        }
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets heures hebdo.
     *
     * @return the heuresHebdo
     */
    public float getHeuresHebdo() {
        return heuresHebdo;
    }

    /**
     * Sets heures hebdo.
     *
     * @param heuresHebdo the heuresHebdo to set
     */
    public void setHeuresHebdo(float heuresHebdo) {
        this.heuresHebdo = heuresHebdo;
    }

    /**
     * Gets suivi.
     *
     * @return the suivi
     */
    public String getSuivi() {
        return suivi;
    }

    /**
     * Sets suivi.
     *
     * @param suivi the suivi to set
     */
    public void setSuivi(String suivi) {
        this.suivi = suivi;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public ClientAsJson getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(ClientAsJson client) {
        this.client = client;
    }

    /**
     * Gets responsable client.
     *
     * @return the responsableClient
     */
    public ContactClientAsJson getResponsableClient() {
        return responsableClient;
    }

    /**
     * Sets responsable client.
     *
     * @param responsableClient the responsableClient to set
     */
    public void setResponsableClient(ContactClientAsJson responsableClient) {
        this.responsableClient = responsableClient;
    }

    /**
     * Gets pdf odm.
     *
     * @return the pdfODM
     */
    public String getPdfODM() {
        return pdfODM;
    }

    /**
     * Sets odm.
     *
     * @param pdfODM the pdfODM to set
     */
    public void setpdfODM(String pdfODM) {
        this.pdfODM = pdfODM;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public EtatAsJson getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the etat to set
     */
    public void setEtat(EtatAsJson pEtat) {
        etat = pEtat;
    }

    /**
     * Gets facture excel.
     *
     * @return the facture excel
     */
    public String getFactureExcel() {
        return factureExcel;
    }

    /**
     * Sets facture excel.
     *
     * @param factureExcel the facture excel
     */
    public void setFactureExcel(String factureExcel) {
        this.factureExcel = factureExcel;
    }

    /**
     * Gets commandes.
     *
     * @return the listCommandes
     */
    public List<CommandeAsJson> getCommandes() {
        return commandesAsJson;
    }

    /**
     * Sets commandes.
     *
     * @param pCommandes the p commandes
     */
    public void setCommandes(List<Commande> pCommandes) {
        for (Commande commande : pCommandes) {
            this.getCommandes().add(new CommandeAsJson(commande));
        }
    }

    /**
     * Sets commandes as json.
     *
     * @param commandes the commandes
     */
    public void setCommandesAsJson(List<CommandeAsJson> commandes) {
        this.commandesAsJson = commandes;
    }

    /**
     * Gets tjm.
     *
     * @return the tjm
     */
    public float getTjm() {
        return tjm;
    }

    /**
     * on set le tjm
     *
     * @param tjm the tjm
     */
    public void setTjm(float tjm) {
        this.tjm = tjm;
    }

    public OutilsInterneAsJson getOutilsInterne() {
        return outilsInterne;
    }

    public void setOutilsInterne(OutilsInterneAsJson outilsInterne) {
        this.outilsInterne = outilsInterne;
    }

    /**
     * Gets perimetre mission as json.
     *
     * @return the frais
     */
    public List<LinkPerimetreMissionAsJson> getPerimetreMissionAsJson() {
        return perimetreMissionAsJson;
    }


    /**
     * Sets perimetre mission as json.
     *
     * @param pPerimetreMissionAsJson the frais to set
     */
    public void setPerimetreMissionAsJson(List<LinkPerimetreMissionAsJson> pPerimetreMissionAsJson) {
        perimetreMissionAsJson = pPerimetreMissionAsJson;
    }


    /**
     * Gets AgenceAsJson agence as json.
     *
     * @return the agence
     */
    public AgenceAsJson getAgence() {
        return agence;
    }

    /**
     * Sets agence as json.
     *
     * @param pAgenceAsJson the agence to set
     */
    public void setAgence(AgenceAsJson pAgenceAsJson) {
        this.agence = pAgenceAsJson;
    }

    /**
     * get the manager
     * @return manager
     */
    public CollaboratorAsJson getManager() {
        return manager;
    }

    /**
     *  set the manager
     * @param pManager the manager to set
     */
    public void setManager(CollaboratorAsJson pManager) {
        this.manager = pManager;
    }
}
