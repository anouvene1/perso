/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.AddviseLinkSessionCollaborateurDAO;
import com.amilnote.project.metier.domain.entities.AddviseLinkSessionCollaborateur;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("addviseLinkSessionCollaborateurDAO")
public class AddviseLinkSessionCollaborateurDAOImpl extends AbstractDAOImpl<AddviseLinkSessionCollaborateur> implements AddviseLinkSessionCollaborateurDAO {

    @Override
    protected Class<AddviseLinkSessionCollaborateur> getReferenceClass() {
        return AddviseLinkSessionCollaborateur.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int createOrUpdateAddviseLinkSessionCollaborateur(AddviseLinkSessionCollaborateur addviseLinkSessionCollaborateur) {

        saveOrUpdate(addviseLinkSessionCollaborateur);
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AddviseLinkSessionCollaborateur findById(int id) {
        return (AddviseLinkSessionCollaborateur) currentSession().get(AddviseLinkSessionCollaborateur.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AddviseLinkSessionCollaborateur>
    findAllByIdCollaboratorAndIdSession(String[] idsCollabSession) {
        Criteria criteria = currentSession().createCriteria(AddviseLinkSessionCollaborateur.class, "invit");
        criteria.createAlias("invit.collaborateur", "collaborateur");
        criteria.createAlias("invit.session", "session");
        Disjunction disjunction = Restrictions.disjunction();
        for (int i = 0; i < idsCollabSession.length; i+=2) {
            Criterion criterion = Restrictions.and(
                Restrictions.eq("collaborateur.id", Long.parseLong(idsCollabSession[i])),
                Restrictions.eq("session.id", Integer.parseInt(idsCollabSession[i+1]))
            );
            disjunction.add(criterion);
        }
        return (List<AddviseLinkSessionCollaborateur>) criteria.add(disjunction).list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AddviseLinkSessionCollaborateur findByIdCollaboratorAndIdSession(long idCollaborateur, int idSession) {
        Criteria criteria = currentSession().createCriteria(
            AddviseLinkSessionCollaborateur.class, "addviseLinkSessionCollaborateur"
        );
        criteria.createAlias("addviseLinkSessionCollaborateur.collaborateur", "collaborateur");
        criteria.createAlias("addviseLinkSessionCollaborateur.session", "session");
        criteria.add(Restrictions.eq("collaborateur.id", idCollaborateur));
        criteria.add(Restrictions.eq("session.id", idSession));
        return (AddviseLinkSessionCollaborateur) criteria.uniqueResult();
    }

}
