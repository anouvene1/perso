package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.CiviliteDAO;
import com.amilnote.project.metier.domain.entities.Civilite;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("civiliteDAO")
public class CiviliteDAOImpl extends AbstractDAOImpl<Civilite> implements CiviliteDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Civilite> getReferenceClass() {
        return Civilite.class;
    }

    /**
     * {@linkplain CiviliteDAO#findAllOrderById()}
     * @return la liste des civilités triés par ID
     */
    @Override
    public List<Civilite> findAllOrderById() {
        Criteria critListCivilite = getCriteria();
        critListCivilite.addOrder(Order.asc(Civilite.PROP_ID));

        return critListCivilite.list();
    }
}
