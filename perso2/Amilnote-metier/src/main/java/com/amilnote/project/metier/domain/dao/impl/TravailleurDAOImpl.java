/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.TravailleurDAO;
import com.amilnote.project.metier.domain.entities.AbstractTravailleur;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe DAO pour la recherche d'un utilisateur en BDD
 */
@Repository("travailleurDAO")
public abstract class TravailleurDAOImpl<T extends AbstractTravailleur> extends AbstractDAOImpl<T> implements TravailleurDAO<T> {

    //private static final Logger logger = LoggerFactory.getLogger(TravailleurDAOImpl.class);

    private static final int ADMIN_ID = 2;
    private static final String ADMIN_CODE = "AD";
    private static final String ADMIN_STATUS = "admin";

    /**
     * {@inheritDoc}
     */
    @Override
    public T findByUsernameAndPassword(String pUsername, String pPassword) {
        Criteria lCritCollaborateur = getCriteria();
        lCritCollaborateur.add(Restrictions.eq(AbstractTravailleur.PROP_MAIL, pUsername));
        lCritCollaborateur.add(Restrictions.eq(AbstractTravailleur.PROP_PASSWORD, pPassword));
        return (T) lCritCollaborateur.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findByMail(String pMail) {
        Criteria lCritCollaborateur = getCriteria();
        lCritCollaborateur.add(Restrictions.eq(AbstractTravailleur.PROP_MAIL, pMail));
        return (T) lCritCollaborateur.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findCollaboratorsForAbsenceMonth(Date pDate) {
        Criteria criteria = reqWithoutAdmin(true);
        List<T> collaborateurs = new ArrayList<>();
        addResultToList(collaborateurs, criteria);
        removeDisabledLastMonth(collaborateurs, pDate);
        return collaborateurs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findCollaboratorByName(String firstname, String lastname) {
        Criteria lCritCollaborateur = getCriteria();
        lCritCollaborateur.add(Restrictions.eq(AbstractTravailleur.PROP_PRENOM, firstname));
        lCritCollaborateur.add(Restrictions.eq(AbstractTravailleur.PROP_NOM, lastname));
        return (T) lCritCollaborateur.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findCollaboratorsForFraisAnnuel(int yearFrais) {
        Criteria criteria = reqWithoutAdmin(true);
        List<T> collaborateurs = new ArrayList<>();
        addResultToList(collaborateurs, criteria);
        removeDisabledLastYear(collaborateurs, yearFrais);
        return collaborateurs;
    }

    /**
     * Create a request to the DB to retrieve all the Travailleur.
     *
     * @param isWithDisabled should add a restriction to enabled = 1.
     * @return The right request to the DB.
     */
    protected Criteria reqAll(boolean isWithDisabled) {
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc(AbstractTravailleur.PROP_NOM));
        if (!isWithDisabled) {
            criteria.add(Restrictions.eq(AbstractTravailleur.PROP_ENABLED, true));
        }
        return criteria;
    }

    /**
     * Create a request to the DB to retrieve all the Travailleur without Admins.
     *
     * @param isWithDisabled should add a restriction to enabled = 1.
     * @return The right request to the DB.
     */
    protected Criteria reqWithoutAdmin(boolean isWithDisabled) {
        Criteria criteria = getCriteria();
        StatutCollaborateur admin = createStatutAdmin();
        criteria.add(Restrictions.not(Restrictions.eq(AbstractTravailleur.PROP_STATUT, admin)));
        criteria.addOrder(Order.asc(AbstractTravailleur.PROP_NOM));
        if (!isWithDisabled) {
            criteria.add(Restrictions.eq(AbstractTravailleur.PROP_ENABLED, true));
        }
        return criteria;
    }

    /**
     * @param collaborateurs a list of collaborateurs
     * @param yearFrais year
     */
    protected abstract void removeDisabledLastYear(List<T> collaborateurs, int yearFrais);

    /**
     * @param collaborateurs a list of collaborateurs
     * @param pDate date
     */
    protected abstract void removeDisabledLastMonth(List<T> collaborateurs, Date pDate);

    /**
     * @param collaborateurs a list of collaborateurs
     * @param criteria criteria
     */
    protected void addResultToList(List<T> collaborateurs, Criteria criteria) {
        collaborateurs.addAll(
            criteria.list()
        );
    }

    /**
     * @return a {@link StatutCollaborateur}
     */
    private StatutCollaborateur createStatutAdmin() {
        return createStatus((long) ADMIN_ID, ADMIN_CODE, ADMIN_STATUS);
    }

    /**
     * To create a status to include or eliminate (e.g., manager, directeur, etc.)
     *
     * @param id     corresponding to the wanted StatutCollaborateur
     * @param code   corresponding to the wanted StatutCollaborateur
     * @param status corresponding to the wanted StatutCollaborateur
     */
    private StatutCollaborateur createStatus(long id, String code, String status) {
        StatutCollaborateur collab = new StatutCollaborateur();
        collab.setId(id);
        collab.setCode(code);
        collab.setStatut(status);
        return collab;
    }



}
