/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.FactureArchivee;
import com.amilnote.project.metier.domain.entities.json.FactureArchiveeAsJson;
import com.itextpdf.text.DocumentException;
import org.joda.time.DateTime;

import javax.naming.NamingException;
import java.io.IOException;
import java.util.List;
import java.util.Date;

/**
 * The interface Facture archivee service.
 */
public interface FactureArchiveeService extends AbstractService<FactureArchivee> {

    List<FactureArchivee> findAllFacturesArchivees();

    /**
     * Trouver le numéro de facture maximum
     *
     * @return int le max des numéros de facture
     */
    int findMaxNumFacture();

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pFactureArchiveeAsJson the p facture archivee as json
     * @return string string
     * @throws IOException             the io exception
     */
    String createOrUpdateFactureArchivee(FactureArchiveeAsJson pFactureArchiveeAsJson) throws IOException;

    /**
     * Find by id facture archivee.
     *
     * @param pIdFactureArchivee the p id facture archivee
     * @return the facture archivee
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    FactureArchivee findById(int pIdFactureArchivee);

    /**
     * Retrieve all {@link FactureArchivee#getCheminPDF()} from a List of archived invoices ( FactureArchivee )
     * @param factureArchiveeList a list of archived invoices
     * @return a String array of each {@link FactureArchivee} of the list
     */
    String[] extractPdfFilesPaths(List<FactureArchivee> factureArchiveeList);

    /**
     * Retrieve all archived invoices pdf files related to a given month and year and merge them to one pdf
     * @param yearOfSearch all {@link FactureArchivee} which {@link FactureArchivee#getDate_archivage()}'s year equals this year
     * @param monthOfSearch all {@link FactureArchivee} which {@link FactureArchivee#getDate_archivage()}'s month equals this year
     * @throws IOException when no pdf file of archived invoices for the given month found on storage
     * @return file path
     * @throws IOException ioexception
     * @throws NamingException namingexception
     */
    String mergeArchivedInvoicesPdfFiles(Integer yearOfSearch, String monthOfSearch) throws IOException, NamingException;

    /**
     * Find all archived invoices which {@link FactureArchivee#getDate_archivage()} are between given start end dates of search
     * @param startSearchDate the start date of the between search
     * @param endSearchDate the end date of the between search
     * @return a list of archived invoiced which {@link FactureArchivee#getDate_archivage()} are between given start end dates of search
     */
    List<FactureArchivee> findByStartEndDates(Date startSearchDate, Date endSearchDate) ;

    /**
     * method which archive a given invoice, extract elements and save an archived invoices containing those informations
     *
     * @param resultDirectoryPath the full path to the directory which will include all the archived invoices's pdf
     * @param invoice the invoice to archive
     * @param archivingDate archiving date
     * @param odfFolderName the folder which will include all customers sub folders
     * @throws IOException local/network file/folder read/write problems
     * @throws DocumentException exception related to pdf read/write problems
     */
    void archiveInvoice(String resultDirectoryPath, Facture invoice, DateTime archivingDate, String odfFolderName) throws IOException, DocumentException ;

}
