/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.LinkEvenementTimesheetDAO;
import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.Mission;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The type Link evenement timesheet dao.
 */
@Repository("linkEvenementTimesheetDAO")
public class LinkEvenementTimesheetDAOImpl extends AbstractDAOImpl<LinkEvenementTimesheet> implements LinkEvenementTimesheetDAO {

    private static final Logger logger = LogManager.getLogger(LinkEvenementTimesheet.class);

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<LinkEvenementTimesheet> getReferenceClass() {
        return LinkEvenementTimesheet.class;
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findEventsBetweenDates(Collaborator, DateTime, DateTime)}
     */
    @Override
    public List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, DateTime pStart, DateTime pEnd) {

        Criteria lListEvent = currentSession().createCriteria(LinkEvenementTimesheet.class, "evenement");
        lListEvent.createAlias("evenement.mission", "entityMission", JoinType.LEFT_OUTER_JOIN);
        lListEvent.createAlias("evenement.absence", "entityAbsence", JoinType.LEFT_OUTER_JOIN);

        Disjunction lDisjunctionMissionOrAbsence = Restrictions.disjunction();
        lDisjunctionMissionOrAbsence.add(Restrictions.eq("entityMission.collaborateur", collaborator));
        lDisjunctionMissionOrAbsence.add(Restrictions.eq("entityAbsence.collaborateur", collaborator));

        Conjunction and = Restrictions.conjunction();
        and.add(Restrictions.ge("date", pStart.toDate()));
        and.add(Restrictions.le("date", pEnd.toDate()));
        and.add(lDisjunctionMissionOrAbsence);

        lListEvent.add(and);

        List<LinkEvenementTimesheet> results = lListEvent.list();

        return results;
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findEventsBetweenDates(Collaborator, Date, Date)}
     */
    @Override
    public List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, Date pStart, Date pEnd) {

        Criteria lListEvent = currentSession().createCriteria(LinkEvenementTimesheet.class);

        lListEvent.add(Restrictions.eq(LinkEvenementTimesheet.PROP_COLLABORATEUR, collaborator));
        lListEvent.add(Restrictions.ge(LinkEvenementTimesheet.PROP_DATE, pStart));
        lListEvent.add(Restrictions.le(LinkEvenementTimesheet.PROP_DATE, pEnd));

        List<LinkEvenementTimesheet> results = lListEvent.list();

        return results;
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#getCountSaturdaysSinceDate(Collaborator, DateTime)}
     */
    @Override
    public int getCountSaturdaysSinceDate(Collaborator collaborateur, DateTime since) {

        Criteria lListEventSaturday = currentSession().createCriteria(LinkEvenementTimesheet.class, "evenement");
        lListEventSaturday.createAlias("evenement.mission", "entityMission", JoinType.LEFT_OUTER_JOIN);

        lListEventSaturday.add(Restrictions.eq("entityMission.collaborateur", collaborateur));

        lListEventSaturday.add(Restrictions.sqlRestriction("dayofweek(" + LinkEvenementTimesheet.PROP_DATE + ") = 7"));

        lListEventSaturday.add(Restrictions.ge("evenement." + LinkEvenementTimesheet.PROP_DATE, since.toDate()));

        Integer totalResult = ((Number) lListEventSaturday.setProjection(Projections.rowCount()).uniqueResult()).intValue();
        logger.debug(totalResult.toString());
        return totalResult;
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findNbWorkedDaysByMissionAndPeriod(Mission, Date, Date)}
     */
    @Override
    public List<LinkEvenementTimesheet> findNbWorkedDaysByMissionAndPeriod(Mission pMission, Date dateDebut, Date dateFin) {
        Criteria lNbEvent = currentSession().createCriteria(LinkEvenementTimesheet.class);

        lNbEvent.add(
                Restrictions.and(
                        Restrictions.eq(LinkEvenementTimesheet.PROP_MISSION, pMission),
                        Restrictions.isNull(LinkEvenementTimesheet.PROP_ABSENCE),
                        Restrictions.ge(LinkEvenementTimesheet.PROP_DATE, dateDebut),
                        Restrictions.le(LinkEvenementTimesheet.PROP_DATE, dateFin)
                )
        );
        lNbEvent.setProjection(Projections.distinct(Projections.property(LinkEvenementTimesheet.PROP_DATE)));

        return lNbEvent.list();
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findDaysMissionPeriod(Mission, DateTime, DateTime)}
     */
    @Override
    public List<LinkEvenementTimesheet> findDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode) {

        Criteria lEvent = currentSession().createCriteria(LinkEvenementTimesheet.class);
        lEvent.add(Restrictions.eq("mission", pMission));
        lEvent.add(Restrictions.ge(LinkEvenementTimesheet.PROP_DATE, pDebutPeriode.toDate()));
        lEvent.add(Restrictions.le(LinkEvenementTimesheet.PROP_DATE, pFinPeriode.toDate()));

        return lEvent.list();
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findAllByMission(Mission)}
     */
    @Override
    public List<LinkEvenementTimesheet> findAllByMission(Mission pMission) {
        Criteria lEvent = getCriteria();
        Criterion lRest1 = Restrictions.and(Restrictions.sqlRestriction("YEAR(date) =" + Calendar.getInstance().get(Calendar.YEAR)),
                Restrictions.sqlRestriction("MONTH(date) = " + Calendar.getInstance().get(Calendar.MONTH) + 1));
        Criterion lRest2 = Restrictions.eq(LinkEvenementTimesheet.PROP_MISSION, pMission);
        lEvent.add(Restrictions.and(lRest1, lRest2));
        lEvent.addOrder(Order.asc(LinkEvenementTimesheet.PROP_DATE)).addOrder(Order.asc(LinkEvenementTimesheet.PROP_MOMENTJOURNEE));

        return (List<LinkEvenementTimesheet>) lEvent.list();

    }


    /**
     * {@linkplain LinkEvenementTimesheetDAO#findAllByMissionWithoutDateRestriction(Mission)}
     */
    @Override
    public List<LinkEvenementTimesheet> findAllByMissionWithoutDateRestriction(Mission pMission) {
        Criteria lEvent = getCriteria();
        //Criterion lRest1 = Restrictions.and(Restrictions.sqlRestriction("YEAR(date) =" + Calendar.getInstance().get(Calendar.YEAR)),
        //Restrictions.sqlRestriction("MONTH(date) = " + Calendar.getInstance().get(Calendar.MONTH) + 1));
        Criterion lRest2 = Restrictions.eq(LinkEvenementTimesheet.PROP_MISSION, pMission);
        lEvent.add(lRest2);
        lEvent.addOrder(Order.asc(LinkEvenementTimesheet.PROP_DATE)).addOrder(Order.asc(LinkEvenementTimesheet.PROP_MOMENTJOURNEE));

        return (List<LinkEvenementTimesheet>) lEvent.list();

    }


    /**
     * {@linkplain LinkEvenementTimesheetDAO#findByDateAndMomentJourneeAndCollaborator(DateTime, int, Collaborator)}
     */
    @Override
    public LinkEvenementTimesheet findByDateAndMomentJourneeAndCollaborator(DateTime pDate, int pMomentJournee, Collaborator collaborator) {
        // Récupère toutes les missions
        Criteria lListEventCriteria = currentSession().createCriteria(LinkEvenementTimesheet.class, "evenement");

        lListEventCriteria.createAlias("evenement.mission", "entityMission", JoinType.LEFT_OUTER_JOIN);
        lListEventCriteria.createAlias("evenement.absence", "entityAbsence", JoinType.LEFT_OUTER_JOIN);

        // on filtre sur le collaborateur
        Criterion missColl = Restrictions.eq("entityMission.collaborateur", collaborator);
        Criterion missAbs = Restrictions.eq("entityAbsence.collaborateur", collaborator);
        lListEventCriteria.add(Restrictions.or(missAbs, missColl));

        Date lDate = pDate.withMillisOfDay(0).toDate();
        lListEventCriteria.add(Restrictions.eq(LinkEvenementTimesheet.PROP_MOMENTJOURNEE, pMomentJournee));
        lListEventCriteria.add(Restrictions.eq(LinkEvenementTimesheet.PROP_DATE, lDate));

        LinkEvenementTimesheet lResults = (LinkEvenementTimesheet) lListEventCriteria.uniqueResult();

        if (lResults != null) {
            return lResults;
        } else {
            return null;
        }
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findAllNotFinishEventOnlyDate(Mission)}
     */
    @Override
    public List<LinkEvenementTimesheet> findAllNotFinishEventOnlyDate(Mission pMission) {
        // Récupère toutes les missions
        Criteria lListEvent = currentSession().createCriteria(LinkEvenementTimesheet.class, "evenement");

        //liste des resultats a obtenir
        ProjectionList lProjList = Projections.projectionList();
        lProjList.add(Projections.property("evenement.id"), "id");
        lProjList.add(Projections.property("evenement.date"), LinkEvenementTimesheet.PROP_DATE);

        // on filtre sur le collaborateur
        lListEvent.add(Restrictions.eq("evenement.mission", pMission));

        Criterion lRest1 = Restrictions.and(Restrictions.sqlRestriction("YEAR(date) =" + Calendar.getInstance().get(Calendar.YEAR)),
                Restrictions.sqlRestriction("YEAR(date) =" + Calendar.getInstance().get(Calendar.YEAR)));
        Criterion lRrest2 = Restrictions.isNull("evenement.absence");
        lListEvent.add(Restrictions.and(lRest1, lRrest2));
        lListEvent.setProjection(lProjList);
        // Récup du résultat
        lListEvent.setResultTransformer(Transformers.aliasToBean(LinkEvenementTimesheet.class));

        return lListEvent.list();
    }

    /**
     * Gets uniq by absence and order.
     *
     * @param absence the absence
     * @param order   the order
     * @return the uniq by absence and order
     */
    public LinkEvenementTimesheet getUniqByAbsenceAndOrder(Absence absence, Order order) {

        Criteria criteria = this.getCriteria();
        criteria.add(Restrictions.eq(this.getReferenceClass() + "." + LinkEvenementTimesheet.PROP_ABSENCE, absence))
                .addOrder(order)
                .setMaxResults(1);
        LinkEvenementTimesheet res = (LinkEvenementTimesheet) criteria.uniqueResult();
        return res;
    }

    /**
     * {@linkplain LinkEvenementTimesheetDAO#findNbDaysForForfaitDeplacement(Mission, DateTime, DateTime)}
     */
    @Override
    public List<LinkEvenementTimesheet> findNbDaysForForfaitDeplacement(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode) {

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LinkEvenementTimesheet> findByAbsence(Absence pAbsence) {
        return findListEntitesByProp("absence", pAbsence);
    }


}
























