package com.amilnote.project.metier.domain.services.impl;


import com.amilnote.project.metier.domain.dao.impl.FrequenceDAOImpl;

import com.amilnote.project.metier.domain.entities.Frequence;

import com.amilnote.project.metier.domain.services.FrequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FrequenceService")
public class FrequenceServiceImpl extends AbstractServiceImpl<Frequence, FrequenceDAOImpl> implements FrequenceService {

    @Autowired
    private FrequenceDAOImpl frequenceDAO;
    @Override
    public FrequenceDAOImpl getDAO() {
        return frequenceDAO;
    }
}
