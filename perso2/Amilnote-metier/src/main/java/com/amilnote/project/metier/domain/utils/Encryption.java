/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;
/**
 * @author lsouai
 */

import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The type Encryption.
 */
@Component("encryption")
public class Encryption {

    /**
     * *
     * Méthode d'encryptage SHA 255 d'un String passé en paramètre
     *
     * @param source the source
     * @return sha the source encrypted in SHA 512
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public static String encryptSHA(String source) throws NoSuchAlgorithmException {

        byte[] key = source.getBytes();

        MessageDigest mdEnc = MessageDigest.getInstance("SHA-512"); // Encryption algorithm
        mdEnc.update(source.getBytes());

        byte[] hash = mdEnc.digest();

        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }
}
