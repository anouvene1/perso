/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.JourNonTravaille;
import com.amilnote.project.metier.domain.entities.TypeAbsence;

import java.util.List;

/**
 * The interface Absence dao.
 */
public interface JourNonTravailleDAO extends LongKeyDAO<JourNonTravaille> {

    /**
     * Retourne tous les Types d'absence
     *
     * @return all type absence
     */
    List<TypeAbsence> getAllTypeAbsence();

    /**
     * Retorune un typeAbsence en fonction de l'id
     *
     * @param id the id
     * @return type absence by id
     */
    TypeAbsence getTypeAbsenceById(long id);

    /**
     * Nombre d'absences par etat et collaborateur
     *
     * @param pSousTraitant  the p SousTraitant
     * @return the nb absence by etat and collaborateur
     */
}
