/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;


import com.amilnote.project.metier.domain.dao.impl.ParametreDAOImpl;
import com.amilnote.project.metier.domain.entities.Parametre;
import com.amilnote.project.metier.domain.entities.json.ParametreAsJson;
import com.amilnote.project.metier.domain.services.ParametreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Parametre service.
 */
@Service("parametreService")
public class ParametreServiceImpl extends AbstractServiceImpl<Parametre, ParametreDAOImpl> implements ParametreService {

    @Autowired
    private ParametreDAOImpl parametreDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public ParametreDAOImpl getDAO() {
        return parametreDAO;
    }

    /**
     * {@linkplain ParametreService#findById(int)}
     */
    @Override
    public Parametre findById(int pIdParametre) {
        return parametreDAO.findUniqEntiteByProp(Parametre.PROP_ID_PARAMETRE, pIdParametre);
    }

    /**
     * {@linkplain ParametreService#findById(int)}
     */
    @Override
    public int createOrUpdateParametre(ParametreAsJson pParametreJson) throws IOException {
        return parametreDAO.createOrUpdateParametre(pParametreJson);
    }

    /**
     * {@linkplain ParametreService#findById(int)}
     */
    @Override
    public String deleteParametre(Parametre pParametre) {
        return parametreDAO.deleteParametre(pParametre);
    }

    /**
     * {@linkplain ParametreService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Parametre> findAllOrderByNomAsc() {
        //with admin, without disabled
        return parametreDAO.findParametres();
    }

    /**
     * {@linkplain ParametreService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<ParametreAsJson> findAllOrderByNomAscAsJson() {
        List<Parametre> tmpListParametre = this.findAllOrderByNomAsc();
        List<ParametreAsJson> tmpListParametreJson = new ArrayList<>();

        for (Parametre tmpParams : tmpListParametre) {

            tmpListParametreJson.add(tmpParams.toJson());

        }
        return tmpListParametreJson;
    }

}
