/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;

/**
 * The interface Commande dao.
 */
public interface CommandeDAO extends LongKeyDAO<Commande> {

    public String createOrUpdateCommande(CommandeAsJson commandeJson, MissionAsJson missionJson) throws IOException;

    /**
     * Suppression de la commande
     *
     * @param pCommande the p commande
     * @return string string
     */
    String deleteCommande(Commande pCommande);

    /**
     * Find commandes list.
     *
     * @return La liste de toutes les commandes
     */
    List<Commande> findCommandes();

    /**
     * Find commandes list.
     * @return la liste des commandes actives
     */
    List<Commande> findActivesCommandes();

    /**
     * Récupère la liste des commandes pour un mois donné
     * @param debut date de début
     * @param fin date de fin
     * @param etat {@link Etat} etat de la commande
     * @return une liste de {@link Commande}
     */
    List<Commande> findCommandesForMonth(DateTime debut, DateTime fin, Etat etat);

    /**
     * Récupères toutes les commandes ayant l'état commencé
     * @param etatCommencee {@link Etat} etat de la commande
     * @return une liste de {@link Commande}
     */
    List<Commande> findStartedCommandes(Etat etatCommencee);

    /**
     * Récupère toutes les commandes ayant l'état terminé
     * @param etatTerminee {@link Etat} etat de la commande
     * @return une liste de {@link Commande}
     */
    List<Commande> findStoppedCommandes(Etat etatTerminee);


    /**
     * Trouve la commande de la mission pour la date voulue
     * @param mission la mission
     * @param dateVoulue la date voulue
     * @return la commande correspondante
     * @author clome
     */
    List<Commande> findByIdMissionByMonth(Mission mission, DateTime dateVoulue);


    /**
     * Trouve les commandes de la mission
     * @param mission la mission
     * @return la commande correspondante
     * @author clome
     */
    List<Commande> findByIdMission(Mission mission);
}
