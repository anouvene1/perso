/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.RapportActivitesAsJson;

import java.util.List;

/**
 * The type Ra form.
 */
public class RAForm {

    private List<RapportActivitesAsJson> listRA;
    private String commentaire;

    /**
     * Instantiates a new Ra form.
     */
    public RAForm() {
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the p commentaire
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }

    /**
     * Gets list ra.
     *
     * @return the listRA
     */
    public List<RapportActivitesAsJson> getListRA() {
        return listRA;
    }

    /**
     * Sets list ra.
     *
     * @param listRA the listRA to set
     */
    public void setListRA(List<RapportActivitesAsJson> listRA) {
        this.listRA = listRA;
    }
}
