/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Client.
 */
@Entity
@Table(name = "ami_client")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Client implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_NOM.
     */
    public static final String PROP_NOM = "nom_societe";
    /**
     * The constant PROP_ADRESSE.
     */
    public static final String PROP_ADRESSE = "adresse";
    /**
     * The constant PROP_TVA.
     */
    public static final String PROP_TVA = "tva";
    /**
     * The constant PROP_RIB_CLIENT
     */
    public static final String PROP_RIB_CLIENT = "ribClient";
    /**
 * The constant PROP_ADRESSE_FACTURATION
     */
    public static final String PROP_ADRESSE_FACTURATION = "adresseFacturation";

    /**
     * The constant PROP_AUXILIAIRE
     */
    public static final String PROP_AUXILIAIRE ="auxiliaire";

    /**
     * The constant PROP_LIB_COMPTA
     */
    public static final String PROP_LIB_COMPTA="libelle_comptable";


    private static final long serialVersionUID = -2868597209884852866L;
    private Long id;
    private String nom_societe;
    private String adresse;
    private String tva;
    private String adresseFacturation;
    private String ribClient;
    private List<ContactClient> contacts = new ArrayList<ContactClient>(0);


    private String auxiliaire;
    private String libelleComptable;
    /**
     * Instantiates a new Client.
     */
    public Client() {
        super();
    }

    /**
     * Instantiates a new Client.
     *
     * @param nom_societe the nom societe
     * @param adresse     the adresse
     * @param tva         the tva
     * @param contacts    the contacts
     */
    public Client(String nom_societe, String adresse, String tva, List<ContactClient> contacts) {
        super();
        this.nom_societe = nom_societe;
        this.adresse = adresse;
        this.tva = tva;
        this.contacts = contacts;
    }

    /**
     * Instantiates a new Client with adresseFacturation.
     *
     * @param nom_societe the nom societe
     * @param adresse     the adresse
     * @param tva         the tva
     * @param contacts    the contacts
     * @param adresseFacturation facture adresse
     */
    public Client(String nom_societe, String adresse, String adresseFacturation, String tva, List<ContactClient> contacts) {
        super();
        this.nom_societe = nom_societe;
        this.adresse = adresse;
        this.adresseFacturation = adresseFacturation;
        this.tva = tva;
        this.contacts = contacts;
    }

    /**
     * Instantiates a new Client with ribClient.
     *
     * @param nom_societe the nom societe
     * @param adresse     the adresse
     * @param tva         the tva
     * @param contacts    the contacts
     * @param rib         rib
     * @param adresseFacturation facture adress
     */
    public Client(String nom_societe, String adresse, String adresseFacturation, String tva, String rib, List<ContactClient> contacts) {
        super();
        this.nom_societe = nom_societe;
        this.adresse = adresse;
        this.adresseFacturation = adresseFacturation;
        this.tva = tva;
        this.ribClient = rib;
        this.contacts = contacts;
    }

    /**
     * Instantiate a new with all fields
     * @param nom_societe nom societe
     * @param adresse adresse
     * @param adresseFacturation adresse facturation
     * @param tva tva
     * @param rib rib
     * @param auxiliaire auxiliaire
     * @param libelleComptable libelleComptable
     * @param contacts contacts
     */
    public Client(String nom_societe, String adresse, String adresseFacturation, String tva, String rib, String auxiliaire,String libelleComptable, List<ContactClient> contacts) {
        super();
        this.nom_societe = nom_societe;
        this.adresse = adresse;
        this.adresseFacturation = adresseFacturation;
        this.tva = tva;
        this.ribClient = rib;
        this.auxiliaire=auxiliaire;
        this.libelleComptable=libelleComptable;
        this.contacts = contacts;
    }

    /**
     * Parse se Collaborateur au format json
     *
     * @return un collaborateur json
     */
    public ClientAsJson toJson() {
        return new ClientAsJson(this);
    }

/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Gets nom societe.
     *
     * @return the nom societe
     */
    @Column(name = "nom_societe", nullable = true)
    public String getNom_societe() {
        return nom_societe;
    }

    /**
     * Sets nom societe.
     *
     * @param nom_societe the nom societe
     */
    public void setNom_societe(String nom_societe) {
        this.nom_societe = nom_societe;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    @Column(name = "adresse", nullable = true)
    public String getAdresse() {
        return adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }


    /**
     * get the adresseFacturation
     * @return string
     */
    @Column(name = "adresse_facturation", nullable = true)
    public String getAdresseFacturation() {
        return adresseFacturation;
    }


    /**
     * Set the adresseFacturation
     * @param adresseFacturation adresse
     */
    public void setAdresseFacturation(String adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }

    /**
     *
     * @return string
     */
    @Column(name = "Auxiliaire", unique = true, nullable = false)
    public String getAuxiliaire() {
        return auxiliaire;
    }

    /**
     *
     * @param auxiliaire auxiliaire
     */
    public void setAuxiliaire(String auxiliaire) {
        this.auxiliaire = auxiliaire;
    }

    /**
     *
     * @return string
     */
    @Column(name = "Libelle_comptable",  nullable = false)
    public String getLibelleComptable() {
        return libelleComptable;
    }

    /**
     *
     * @param libelleComptable libelleComptable
     */
    public void setLibelleComptable(String libelleComptable) {
        this.libelleComptable = libelleComptable;
    }

    /**
     * Gets contacts.
     *
     * @return the contacts
     */
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    public List<ContactClient> getContacts() {
        return contacts;
    }

    /**
     * Sets contacts.
     *
     * @param contacts the contacts to set
     */
    public void setContacts(List<ContactClient> contacts) {
        this.contacts = contacts;
    }

    /**
     * Gets tva.
     *
     * @return the tva
     */
    public String getTva() {
        return tva;
    }

    /**
     * Sets tva.
     *
     * @param tva the tva to set
     */
    public void setTva(String tva) {
        this.tva = tva;
    }

    /**
     * Get the rib
     *
     * @return the rib
     */
    @Column(name = "rib_client")
    public String getRibClient() {
        return ribClient;
    }

    /**
     * Sets the rib
     *
     * @param ribClient the rib to set
     */
    public void setRibClient(String ribClient) {
        this.ribClient = ribClient;
    }



}
