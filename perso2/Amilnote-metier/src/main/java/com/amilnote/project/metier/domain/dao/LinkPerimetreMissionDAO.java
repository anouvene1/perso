/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.LinkPerimetreMission;
import com.amilnote.project.metier.domain.entities.Mission;

import java.util.List;

/**
 * The interface Link perimetre mission dao.
 */
public interface LinkPerimetreMissionDAO extends LongKeyDAO<LinkPerimetreMission> {

    /**
     * retourne une liste de lien entre les perimetres de mission et sa mission
     *
     * @return List list
     */
    List<LinkPerimetreMission> findAll();

    /**
     * supprime les lien entre les perimetres et sa mission mise en paramètre
     *
     * @param pLink the p link
     * @return the int
     */
    int deleteLinkPerimetreMissionByMission(LinkPerimetreMission pLink);

    /**
     * Creer les liens entre les perimetres de mission et sa mission mise en paramètre
     *
     * @param pMission the p mission
     * @return the int
     */
    int createOrUpdateLinkPerimetreMissionByMission(Mission pMission);

}
