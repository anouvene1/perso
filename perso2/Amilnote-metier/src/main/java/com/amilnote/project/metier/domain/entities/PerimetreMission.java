/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.PerimetreMissionAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by sbenslimane on 22/12/2016.
 */
@Entity
@Table(name = "ami_ref_perimetre_mission")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PerimetreMission implements Serializable {


    /**
     * The constant PROP_ID_PERIMETRE_MISSION.
     */
    public static final String PROP_ID_PERIMETRE_MISSION = "id";
    /**
     * The constant PROP_CODE_PERIMETRE_MISSION.
     */
    public static final String PROP_CODE_PERIMETRE_MISSION = "code";
    /**
     * The constant PROP_NOM_PERIMETRE_MISSION.
     */
    public static final String PROP_NOM_PERIMETRE_MISSION = "nom_perimetre";

    private Long id;
    private String codePerimetre;
    private String nomPerimetre;

    /**
     * Constructeur
     */
    public PerimetreMission() {
    }

    /**
     * Instantiates a new Perimetre mission.
     *
     * @param id            the id
     * @param codePerimetre the code perimetre
     * @param nomPerimetre  the nom perimetre
     */
    public PerimetreMission(Long id, String codePerimetre, String nomPerimetre) {
        this.id = id;
        this.codePerimetre = codePerimetre;
        this.nomPerimetre = nomPerimetre;
    }

    /**
     * Convertit un PerimetreMission en LinkPerimetreMissionAsJson
     *
     * @return the perimetre mission as json
     */
    public PerimetreMissionAsJson toJson() {
        return new PerimetreMissionAsJson(this);
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets code perimetre.
     *
     * @return codePerimetre code perimetre
     */
    @Column(name = "code")
    public String getCodePerimetre() {
        return codePerimetre;
    }

    /**
     * Sets code perimetre.
     *
     * @param codePerimetre the dateDebut to set
     */
    public void setCodePerimetre(String codePerimetre) {
        this.codePerimetre = codePerimetre;
    }

    /**
     * Gets nom perimetre.
     *
     * @return nomPerimetre nom perimetre
     */
    @Column(name = "nom_perimetre")
    public String getNomPerimetre() {
        return nomPerimetre;
    }

    /**
     * Sets nom perimetre.
     *
     * @param nomPerimetre the dateDebut to set
     */
    public void setNomPerimetre(String nomPerimetre) {
        this.nomPerimetre = nomPerimetre;
    }
}
