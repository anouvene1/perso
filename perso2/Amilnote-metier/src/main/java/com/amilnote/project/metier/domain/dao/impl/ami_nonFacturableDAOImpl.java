package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ami_nonFacturableDAO;
import com.amilnote.project.metier.domain.entities.ami_nonFacturable;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * Created by clome on 27/04/2017.
 */
@Repository("ami_nonFacturableDAO")
public class ami_nonFacturableDAOImpl extends AbstractDAOImpl<ami_nonFacturable> implements ami_nonFacturableDAO {

    /**
     * {@linkplain ami_nonFacturableDAO#findListEntitesByDateBetweenDates(String, Date, Date)}
     */
    @Override
    public List<ami_nonFacturable> findListEntitesByDateBetweenDates(String pProp, Date pDateDebutMois, Date pDateFinMois) {
        Criteria criteria = currentSession().createCriteria(ami_nonFacturable.class);
        criteria.add(Restrictions.ge(ami_nonFacturable.PROP_MOIS_RAPPORT, pDateDebutMois));
        criteria.add(Restrictions.lt(ami_nonFacturable.PROP_MOIS_RAPPORT, pDateFinMois));
        criteria.add(Restrictions.ge("dateFinMission", pDateDebutMois));
        criteria.add(Restrictions.lt("dateDebutMission", pDateFinMois));
        Criterion criterion = Restrictions.or(
            Restrictions.isNull("etatCommande"),
            Restrictions.and(
                Restrictions.ne("etatRapport", 1),
                Restrictions.ne("etatRapport", 2)
            ),
            Restrictions.eq("nbContact", 0)
        );
        criteria.add(criterion);
        return (List<ami_nonFacturable>) criteria.list();
    }

    @Override
    protected Class<ami_nonFacturable> getReferenceClass() {
        return ami_nonFacturable.class;
    }
}
