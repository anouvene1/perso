/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.LinkPerimetreMission;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.PerimetreMission;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * The interface Mission service.
 */
public interface MissionService extends AbstractService<Mission> {

    /**
     * retrouver une mission par son nom
     *
     * @param pNameMission the p name mission
     * @return Mission mission
     */
    Mission findByName(String pNameMission);

    /**
     * retrouver une mission par son nom
     *
     * @param pIdMission the id mission
     * @return Mission mission
     */
    Mission findById(Long pIdMission);

    /**
     * retourne une mission par son nom si celle-ci appartient au collaborateur
     *
     * @param pNameMission   Nom de la mission
     * @param collaborator Collaborateur
     * @return mission du collaborateur portant le nom souhaité
     */
    Mission findByNameForCollab(String pNameMission, Collaborator collaborator);

    /**
     * retourne la liste des missions, non terminées liées, au collaborateur
     *
     * @param collaborator       the p collaborateur
     * @param nbMonthBeforeCurrent number of month before current month
     * @return list list
     */
    List<Mission> findAllMissionsNotFinishForCollaborator(Collaborator collaborator, int nbMonthBeforeCurrent);

    /**
     * retourne la liste json des missions, non terminées liées, au collaborateur
     *
     * @param collaborator the p collaborateur
     * @param pMois          the p mois
     * @param pAnnee         the p annee
     * @return String string
     */
    String findAllMissionsNotFinishForCollaboratorAsJson(Collaborator collaborator, int pMois, int pAnnee);

    /**
     * Renvoie la mission par default pour le collaborateur
     *
     * @param collaborator the p collaborateur
     * @return Mission mission
     */
    Mission findDefaultMissionForCollaborator(Collaborator collaborator);

    /**
     * retourne la liste des missions du collaborateur pour le RA
     *
     * @param collaborator Collaborateur à gérer
     * @param pDebutPeriode  the p debut periode
     * @param pFinPeriode    the p fin periode
     * @return list list
     */
    List<Mission> findMissionCollabPourRA(Collaborator collaborator, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * retourne la liste des missions clientes du mois
     *
     * @param pDate the p debut periode
     * @return list list
     */
    List<Mission> findMissionsClientesByMonth(DateTime pDate);

    /**
     * retourne la liste des missions clientes du mois pour le collaborateur voulu
     *
     * @param collaborator the p collaborateur
     * @param pDate          the p date
     * @return la liste des missions
     * @author clome
     */
    List<Mission> findMissionsClientesByMonthForCollaborator(Collaborator collaborator, DateTime pDate);

    /**
     * retourne la liste des missions clientes pour un collaborateur et une période donnée
     *
     * @param debut         start date
     * @param fin           end date
     * @param collaborator a {@link Collaborator}
     * @return a list of {@link Mission}
     */
    List<Mission> findClientMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator);

    /**
     * retourne la liste des missions avec les linkEvenements liés et les frais liées a ces evenements avec les frais du mois et de l'année fournis
     *
     * @param collaborator Collaborateur à gérer
     * @param pMois          the p mois
     * @param pAnnee         the p annee
     * @return string string
     */
    String findAllMissionByCollaboratorForFraisAsJson(Collaborator collaborator, int pMois, int pAnnee);

    /**
     * Find mission collab pour ra as mission as json list.
     *
     * @param collaborator the p collaborateur
     * @param pDebutPeriode  the p debut periode
     * @param pFinPeriode    the p fin periode
     * @return the list
     */
    List<MissionAsJson> findMissionCollabPourRAAsMissionAsJson(Collaborator collaborator, DateTime pDebutPeriode, DateTime pFinPeriode);

    /***
     * retourne la liste des missions du collaborateur passé en paramètre
     *
     * @param collaborator Collaborateur voulu
     * @return Liste des missions du collaborateur
     */
    List<MissionAsJson> findAllMissionsCollaborator(Collaborator collaborator);

    /***
     * retourne la liste des missions du collaborateur passé en paramètre ordonnancé par le champ passé.
     *
     * @param collaborator Collaborateur voulu
     * @param orderBy        Colonne sur lequel on veut ordonnancé les résultats
     * @return Liste des missions du collaborateur
     */
    List<MissionAsJson> findAllMissionsCollaborator(Collaborator collaborator, String orderBy);

    /***
     * retourne la liste des missions du collaborateur passé en paramètre
     *
     * @param pSousTraitant Collaborateur voulu
     * @return Liste des missions du collaborateur
     */
    List<MissionAsJson> findAllMissionsSubcontractor(Collaborator pSousTraitant);

    /***
     * retourne la liste des missions du collaborateur passé en paramètre ordonnancé par le champ passé.
     *
     * @param pSousTraitant Collaborateur voulu
     * @param orderBy        Colonne sur lequel on veut ordonnancé les résultats
     * @return Liste des missions du collaborateur
     */
    List<MissionAsJson> findAllMissionsSubcontractor(Collaborator pSousTraitant, String orderBy);

    /**
     * Met à jour ou créer une mission
     *
     * @param mission       the mission
     * @param collaborator the collaborateur
     * @return string string
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    String updateMission(MissionAsJson mission, Collaborator collaborator) throws JsonProcessingException, IOException;

    /**
     * Met à jour ou créer une mission
     *
     * @param mission       the mission
     * @param sousTraitant the sousTraitant
     * @return string string
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */


    /**
     * Create or update link perimetre mission int.
     *
     * @param mission the mission
     * @return the int
     */
    int createOrUpdateLinkPerimetreMission(MissionAsJson mission);

    /**
     * Supprime une mission.
     *
     * @param pMission Mission à supprimer
     * @return Message de retour
     */
    String deleteMission(Mission pMission);

    /**
     * Gets file odm id.
     *
     * @param pId the p id
     * @return the file odm id
     */
    File getFileODMId(Long pId);

    /**
     * Gets all mission etat so.
     *
     * @return the all mission etat so
     */
    List<MissionAsJson> getAllMissionEtatSO();

    /**
     * Gets all mission etat br.
     *
     * @return the all mission etat br
     */
    List<MissionAsJson> getAllMissionEtatBR();

    /**
     * Update etat mission string.
     *
     * @param pIdMission the p id mission
     * @param pCodeEtat  the p code etat
     * @return the string
     * @throws Exception the exception
     */
    String updateEtatMission(Long pIdMission, String pCodeEtat) throws Exception;

    /**
     * Gets all mission etat va.
     *
     * @return the all mission etat va
     */
    List<MissionAsJson> getAllMissionEtatVA();

    /**
     * Gets nb mission attente val.
     *
     * @param collaborator the p collaborateur
     * @return the nb mission attente val
     */
    int getNbMissionAttenteVal(Collaborator collaborator);

    /**
     * Gets nb mission attente val.
     *
     * @return the nb mission attente val
     */
    int getNbMissionAttenteVal();

    /**
     * Gets file facture.
     *
     * @param pMission the p mission
     * @param pDate    the p date
     * @return the file facture
     */
    File getFileFacture(Mission pMission, DateTime pDate);

    /**
     * Gets list perimetre mission.
     *
     * @param mission the mission
     * @return the list perimetre mission
     */
//------------Perimetre Mission----------//
    //SBE_AMNOTE-197
    List<LinkPerimetreMission> getListPerimetreMission(Mission mission);

    /**
     * Gets all perimetre mission.
     *
     * @return the all perimetre mission
     */
    List<PerimetreMission> getAllPerimetreMission();

    /**
     * Find perimetre mission by code perimetre mission.
     *
     * @param code the code
     * @return the perimetre mission
     */
    PerimetreMission findPerimetreMissionByCode(String code);

    /**
     * retourne toutes les missions en cours, date du jours définie ici
     *
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionEnCours();

    /**
     * retourne toutes les missions client en cours, date du jours définie ici
     *
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionClientEnCours();

    /**
     * retourne toutes les missions non client en cours, date du jours définie ici
     *
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionEnCoursNonClient();

    /**
     * retourne toutes les missions à une date données pour un collaborateur
     *
     * @param date          the date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findAllMissionForCollaborator(DateTime date, Collaborator collaborator);

    /**
     * retourne toutes les missions entre deux dates données pour un collaborateur
     *
     * @param debut         the start date
     * @param fin           the end date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator);

    /**
     * retourne toutes les missions entre deux dates données
     *
     * @param start the start date
     * @param end   the end date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionBetweenTwoDates(String start, String end);

    /**
     * retourne les missions non client entre deux dates
     *
     * @param start the start date
     * @param end   the end date
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionNonClientBetweenTwoDates(String start, String end);

    /**
     * retourne les missions non client entre deux dates pour un collab
     *
     * @param start         the start date
     * @param end           the end date
     * @param collaborator the collaborateur
     * @return une liste de {@link Mission}
     */
    List<Mission> findMissionNonClientBetweenTwoDatesForCollab(String start, String end, Collaborator collaborator);

    /**
     * return a list of missions without invoice by type of mission and collaborator status and given period
     *
     * @param missionTypeCode        {@link String} code of the mission type
     * @param collaboratorStatusCode {@link String} status code of the collaborator
     * @param lastDayOfMonth         {@link Date} last day of selected month
     * @return list of {@link Mission}
     */
    List<Mission> findMissionWithoutInvoice(String missionTypeCode, String collaboratorStatusCode, Date lastDayOfMonth);
}
