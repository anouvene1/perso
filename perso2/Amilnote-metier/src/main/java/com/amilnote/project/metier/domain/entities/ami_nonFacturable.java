package com.amilnote.project.metier.domain.entities;

import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.util.Date;

/**
 * The type Ami nonFacturable.
 */
@Entity
@Immutable
@Table(name = "ami_nonFacturable")
public class ami_nonFacturable implements java.io.Serializable {

    /**
     * The constant PROP_MOIS_RAPPORT.
     */
    public static final String PROP_MOIS_RAPPORT = "mois_rapport";

    @Id
    @Column(name="id")
    @GeneratedValue
    private int id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "mission")
    private String mission;

    @Column(name = "client")
    private String client;

    @Column(name = "nb_contact")
    private int nbContact;

    @Column(name = "commande_etat")
    private String etatCommande;

    @Column(name = "ra_etat")
    private int etatRapport;

    @Column(name = "mois_rapport")
    private Date mois_rapport;

    @Column(name = "date_debut_mission")
    private Date dateDebutMission;

    @Column(name = "date_fin_mission")
    private Date dateFinMission;

    /**
     * Instantiates a new Ami facture de commande.
     */
    public ami_nonFacturable() {
        super();
    }

    public ami_nonFacturable(String nom, String prenom, String mission, String client, int nbContact, Integer etat_commande, int etat_rapport, Date mois_rapport) {
        this.nom = nom;
        this.prenom = prenom;
        this.mission = mission;
        this.client = client;
        this.nbContact = nbContact;
        this.etatCommande = etat_commande.toString();
        this.etatRapport = etat_rapport;
        this.mois_rapport = mois_rapport;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public int getNbContact() {
        return nbContact;
    }

    public void setNbContact(int nbContact) {
        this.nbContact = nbContact;
    }

    public String getEtatCommande() {
        return etatCommande;
    }

    public void setEtatCommande(Integer etatCommande) {
        this.etatCommande = etatCommande.toString();
    }

    public int getEtatRapport() {
        return etatRapport;
    }

    public void setEtatRapport(int etatRapport) {
        this.etatRapport = etatRapport;
    }

    public Date getMoisRapport() {
        return mois_rapport;
    }

    public void setMoisRapport(Date mois_rapport) {
        this.mois_rapport = mois_rapport;
    }

    public Date getDateDebutMission() {
        return dateDebutMission;
    }

    public void setDateDebutMission(Date dateDebutMission) {
        this.dateDebutMission = dateDebutMission;
    }

    public Date getDateFinMission() {
        return dateFinMission;
    }

    public void setDateFinMission(Date dateFinMission) {
        this.dateFinMission = dateFinMission;
    }
}
