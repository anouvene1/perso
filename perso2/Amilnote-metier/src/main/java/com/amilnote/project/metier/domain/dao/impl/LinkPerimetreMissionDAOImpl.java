/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.LinkPerimetreMissionDAO;
import com.amilnote.project.metier.domain.entities.LinkPerimetreMission;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.services.MissionService;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Link perimetre mission dao.
 */
@Repository("LinkPerimetreMissionDAO")
public class LinkPerimetreMissionDAOImpl extends AbstractDAOImpl<LinkPerimetreMission> implements LinkPerimetreMissionDAO {


    @Autowired
    private MissionService missionService;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<LinkPerimetreMission> getReferenceClass() {
        return LinkPerimetreMission.class;
    }

    /**
     * {@linkplain LinkPerimetreMissionDAO#findAll()}
     */
    @Override
    public List<LinkPerimetreMission> findAll() {
        Criteria lCritLinkPerimetreMission = getCriteria();
        return (List<LinkPerimetreMission>) lCritLinkPerimetreMission.list();
    }

    /**
     * {@linkplain LinkPerimetreMissionDAO#deleteLinkPerimetreMissionByMission(LinkPerimetreMission)}
     */
    @Override
    @Transactional
    public int deleteLinkPerimetreMissionByMission(LinkPerimetreMission pLink) {
        // --- Création de la session et de la transaction
        Session session = getSessionFactory().getCurrentSession();

        delete(pLink);
        session.flush();
        return 0;
    }

    /**
     * {@linkplain LinkPerimetreMissionDAO#createOrUpdateLinkPerimetreMissionByMission(Mission)}
     */
    @Override
    @Transactional
    public int createOrUpdateLinkPerimetreMissionByMission(Mission pMission) {

        boolean verifExist = false;
        //On récupère tout les link perimetre de la mission
        List<LinkPerimetreMission> listLinkOld = missionService.getListPerimetreMission(pMission);

        //liste des perimetre de l'objet
        List<LinkPerimetreMission> listDesLinkDeMission = pMission.getListPerimetreMission();

        //VERIF SI ON SUPPRIME D'ANCIEN LINK
        for (LinkPerimetreMission linkPerimetreTemp : listLinkOld) {
            verifExist = false;
            for (LinkPerimetreMission perimetreTemp : listDesLinkDeMission) {
                if (linkPerimetreTemp.getPerimetreMission().getId().equals(perimetreTemp.getPerimetreMission().getId())) {
                    verifExist = true;
                }
            }

            //s'il n'existe plus on le supprime
            if (verifExist == false) {
                deleteLinkPerimetreMissionByMission(linkPerimetreTemp);
            }


        }

        //VERIF SI ON AJOUTE DE NOUVEAU LINK
        for (LinkPerimetreMission perimetreTemp : listDesLinkDeMission) {
            LinkPerimetreMission leLinkAmodifier;
            verifExist = false;
            for (LinkPerimetreMission linkPerimetreTemp : listLinkOld) {
                if (linkPerimetreTemp.getPerimetreMission().getId().equals(perimetreTemp.getPerimetreMission().getId())) {
                    verifExist = true;
                }
            }

            //s'il n'existe pas on le crée
            if (verifExist == false) {
                save(perimetreTemp);
            }
        }

        return 0;
    }


}
