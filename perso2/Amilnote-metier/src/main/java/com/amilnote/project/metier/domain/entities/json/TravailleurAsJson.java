/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Civilite;

/**
 * The type Travailleur as json.
 *
 */
public abstract class TravailleurAsJson {

    private Long id;
    private String nom;
    private String prenom;
    private String mail;
    private String password;
    private String telephone;
    private Boolean enabled;
    private PosteAsJson poste;
    private StatutCollaborateurAsJson statut;
    private String adressePostale;
    private Civilite civilite;
    private int agency;

    /**
     * Constructeur par défaut de la classe Collaborateur
     */
    public TravailleurAsJson() {
    }

    public TravailleurAsJson(Long id, String nom, String prenom,
                             String mail, String password, String telephone,
                             Boolean enabled, PosteAsJson poste,
                             StatutCollaborateurAsJson statut,
                             String adressePostale, Civilite civilite,
                             int agency) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.password = password;
        this.telephone = telephone;
        this.enabled = enabled;
        this.poste = poste;
        this.statut = statut;
        this.adressePostale = adressePostale;
        this.civilite = civilite;
        this.agency = agency;
    }

    /**
     * retourne l'identifiant du collaborateur
     *
     * @return Long id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * mise a jour de l'id du collaborateur
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * retourne le nom du collaborateur
     *
     * @return String nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * mise a jour du nom du collaborateur
     *
     * @param pNom the p nom
     */
    public void setNom(String pNom) {
        this.nom = pNom;
    }


    /**
     * retourne le prenom du collaborateur
     *
     * @return String prenom
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * mise a jour du prenom du collaborateur
     *
     * @param pPrenom the p prenom
     */
    public void setPrenom(String pPrenom) {
        this.prenom = pPrenom;
    }


    /**
     * retourne le mail (ici username) du collaborateur
     *
     * @return String mail
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * mise a jour du mail du collaborateur
     *
     * @param pMail the p mail
     */
    public void setMail(String pMail) {
        this.mail = pMail;
    }

    /**
     * retourne le telephone du collaborateur
     *
     * @return String telephone
     */
    public String getTelephone() {
        return this.telephone;
    }


    /**
     * muise a jour du telephone du collaborateur
     *
     * @param pTelephone the p telephone
     */
    public void setTelephone(String pTelephone) {
        this.telephone = pTelephone;
    }


    /**
     * retourne le mot de passe du collaborateur (encodage MD5)
     *
     * @return String password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * mise a jour du mot de passe du collaborateur
     *
     * @param pPassword the p password
     */
    public void setPassword(String pPassword) {
        this.password = pPassword;
    }

    /**
     * permet de savoir si un compte utilisateur est ouvert ou non
     *
     * @return enabled enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }


    /**
     * mise a jour du statut du compte utilisateur
     *
     * @param pEnabled the p enabled
     */
    public void setEnabled(Boolean pEnabled) {
        enabled = pEnabled;
    }

    /**
     * Sets enabled with bool.
     *
     * @param pEnabled the p enabled
     */
    public void setEnabledWithBool(String pEnabled) {
        if (pEnabled.equals("true")) {
            enabled = true;
        } else {
            enabled = false;
        }
    }

    /**
     * Gets poste.
     *
     * @return the poste
     */
    public PosteAsJson getPoste() {
        return poste;
    }

    /**
     * Sets poste.
     *
     * @param pPoste the poste to set
     */
    public void setPoste(PosteAsJson pPoste) {
        poste = pPoste;
    }

    /**
     * Gets statut.
     *
     * @return the statut
     */
    public StatutCollaborateurAsJson getStatut() {
        return statut;
    }

    /**
     * Sets statut.
     *
     * @param pStatut the statut to set
     */
    public void setStatut(StatutCollaborateurAsJson pStatut) {
        statut = pStatut;
    }

    /**
     * Gets adresse postale.
     *
     * @return adresse postale
     */
    public String getAdressePostale() {
        return adressePostale;
    }

    /**
     * Sets adresse postale.
     *
     * @param adressePostale the dismiss date to set
     */
    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    /**
     * Gets civility
     *
     * @return civilite the civility of collab
     */
    public Civilite getCivilite() {
        return civilite;
    }

    /**
     * Sets civilite
     *
     * @param civilite the civility of collab
     */
    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    /**
     * Gets agency
     *
     * @return civilite the agency of collab
     */
    public int getAgency() {
        return agency;
    }

    /**
     * Sets agency
     *
     * @param agency the agency of collab
     */
    public void setAgency(int agency) {
        this.agency = agency;
    }

}
