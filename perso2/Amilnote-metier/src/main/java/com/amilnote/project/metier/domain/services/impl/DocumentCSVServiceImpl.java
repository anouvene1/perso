package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.TypeFacture;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang.StringUtils.trim;


@Service("DocumentCSVService")
public class DocumentCSVServiceImpl implements DocumentCSVService {

    // Frais related
    private static final String ACCOUNTING_JOURNAL_ODF = "ODF";
    private static final String GENERAL_CODE_EXPENSES_1 = "401000";
    private static final String GENERAL_CODE_EXPENSES_2 = "625100";
    private static final String AUXILIARY_EXPENSES = "9FRAIS";

    // Facture related
    private static final String ACCOUNTING_JOURNAL_VE = "VE";
    private static final String GENERAL_CODE_INVOICE_1 = "411000";
    private static final String GENERAL_CODE_INVOICE_HANDWRITTEN_SUBCONTRACTOR = "706000";
    private static final String GENERAL_CODE_INVOICE_INTRA_COMMUNITY_TVA_RATE = "706920";
    private static final String GENERAL_CODE_INVOICE_EXPENSES = "708500";
    private static final String GENERAL_CODE_INVOICE_3 = "445710";

    // Formatters
    private static final String DECIMAL_FORMAT = "#0.00";
    private static final String DATE_FORMAT_SLASH_MONTH_YEAR = "MM/yyyy";

    // File manipulation
    private static final String BYTE_ORDER_MARK = "\uFEFF";
    private static final String SEPARATOR_SEMICOLON = ";";
    private static final String SEPARATOR_LINE = System.getProperty("line.separator");
    private static final String FILENAME_INVOICE_BILLABLE_SUBCONTRACTOR = "FacturableExportComptaSST_";
    private static final String FILENAME_INVOICE_BILLABLE = "FacturableExportCompta";
    private static final String FILENAME_INVOICE_HANDWRITTEN = "FacturableMainExportCompta_";
    private static final String FILENAME_INVOICE_HANDWRITTEN_SUBCONTRACTOR = "FacturableMainExportComptaSST_";
    private static final String FILENAME_INVOICE_EXPENSES = "FraisExportCompta_";
    private static final String FILENAME_INVOICE_EXPENSES_COLLABORATORS = "FraisExportCollabCompta_";

    // Miscellaneous
    private static final String VALUE_ZERO = "0.0";
    private static final String VALUE_ZERO_STR = "0,00";
    private static final String VALUE_TOTAL = "Total";
    private static final String VALUE_REPLACE = "replace";


    private static Logger logger = LogManager.getLogger(DocumentCSVServiceImpl.class);


    @Autowired
    private FileService fileService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private FactureService factureService;

    /**
     * Allows to get the path of the file to be generated
     *
     * @param date         date of the day
     * @param monthODFPath the monthODFPath parameter
     * @param typeFacture  invoice type
     * @return a String containing the path of the file
     */
    public String createCSVFilePath(DateTime date, String monthODFPath, String typeFacture) {

        // Declaring all the useful variables
        final String slash = "/";
        String file = "";
        String filePath;
        String fileTimestamp = Utils.moisIntToString(date.getMonthOfYear() - 1) + "_" + date.getYear();
        String fileBaseName = "";

        // Setting up the file extension
        try {
            fileBaseName = Parametrage.getContext(Constantes.TEMP_FOLDER_FILES);
        } catch (NamingException namingException) {
            logger.error(namingException.getMessage());
        }

        // Determining the file's name based on the typeFacture
        switch (typeFacture) {
            case TypeFacture.TYPEFACTURE_FACTURABLE_CSV:
                file = FILENAME_INVOICE_BILLABLE + fileTimestamp;
                break;
            case TypeFacture.TYPEFACTURE_FACTURABLE_SST_CSV:
                file = FILENAME_INVOICE_BILLABLE_SUBCONTRACTOR + fileTimestamp;
                break;
            case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_CSV:
                file = FILENAME_INVOICE_HANDWRITTEN + fileTimestamp;
                break;
            case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_SST_CSV:
                file = FILENAME_INVOICE_HANDWRITTEN_SUBCONTRACTOR + fileTimestamp;
                break;
            case TypeFacture.TYPEFACTURE_FACTURE_FRAIS_CSV:
                file = FILENAME_INVOICE_EXPENSES + fileTimestamp;
                break;
            case TypeFacture.TYPEFACTURE_FACTURE_FRAIS_COLLAB_CSV:
                file = FILENAME_INVOICE_EXPENSES_COLLABORATORS + fileTimestamp;
                break;
            default:
                break;
        }

        // Creating the filePath
        if (monthODFPath == null) {
            filePath = fileBaseName + file + Constantes.EXTENSION_FILE_CSV;
        } else {
            filePath = fileBaseName + slash + monthODFPath + slash + file + Constantes.EXTENSION_FILE_CSV;
        }
        return filePath;
    }

    /**
     * Generic CSV file creation method
     *
     * @param date         date of the day
     * @param monthODFPath the monthODFPath parameter
     * @param typeFacture  invoice type
     * @param csvGlobal    contains header + body of the file to be created
     * @return a String representing the file
     */
    private String createCSVFile(DateTime date, String monthODFPath, String typeFacture, String csvGlobal) {

        // Getting the path of the file as well as the full name of the file to be created
        String filePath = createCSVFilePath(date, monthODFPath, typeFacture);

        // Creating the folder containing the file
        fileService.createFolder(filePath);

        // Declaring a byte array to store the file's content
        byte[] fileAsBytes = csvGlobal.getBytes();

        fileService.writeFile(filePath, fileAsBytes);

        return filePath;

    }

    /**
     * This function will generate the CSV file
     *
     * @param listFactures a list of {@link Facture}
     * @param date         date of the day
     * @param typeFacture  invoice type
     * @param monthODFPath the monthODFPath parameter
     * @return a String which represents the file itself
     */
    public String createCSVFileFacture(List<Facture> listFactures, DateTime date, String typeFacture, String monthODFPath) {

        String filePath = "";

        // Defining the header of the file
        String csvHeader = BYTE_ORDER_MARK + "Journal;Date;Général;Auxiliaire;Référence;Libellé;Débit;Crédit" + SEPARATOR_LINE;

        // Defining the body of the file
        String csvBody = createCSVLineFacture(listFactures, typeFacture);

        // Declaring a new String containing the header and the body
        String csvGlobal = csvHeader + csvBody;

        // Generating all the standard CSV information
        filePath = createCSVFile(date, monthODFPath, typeFacture, csvGlobal);

        return filePath;

    }

    /**
     * This function will generate the CSV file
     *
     * @param dateToParse  The date as "MM/AAAA" format to parse
     * @param typeFacture  invoice type
     * @param monthODFPath the monthODFPath parameter
     * @return a String which represents the file itself
     */
    public String createCSVFileFrais(String dateToParse, String typeFacture, String monthODFPath) {

        String filePath = "";
        Calendar calendar = Calendar.getInstance();

        if (null != dateToParse && !dateToParse.isEmpty()) {
            int year = Integer.parseInt(dateToParse.substring(dateToParse.length() - 4));
            int month = Integer.parseInt(dateToParse.substring(0, 2)) - 1;
            calendar.set(year, month, 1, 0, 0, 0);
        }

        DateTime dateTime = new DateTime(calendar.getTime());

        // Defining the header of the file
        String cSVHeader = BYTE_ORDER_MARK + "Journal;Date;Général;Auxiliaire;Référence;Libellé;Débit;Crédit" + SEPARATOR_LINE;

        // Declaring a new String containing the header and the body
        String csvGlobal = cSVHeader + createCSVLineFrais(dateTime);

        // Generating all the standard CSV information
        filePath = createCSVFile(dateTime, monthODFPath, typeFacture, csvGlobal);

        return filePath;
    }

    /**
     * Allows to generate the content of the CSV file
     *
     * @param listFactures a list of {@link Facture}
     * @param typeFacture  invoice type
     * @return a String representing the body of the file
     */
    private String createCSVLineFacture(List<Facture> listFactures, String typeFacture) {

        boolean isCredit;
        String strDate = "";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);
        DecimalFormat decimalFormatter = new DecimalFormat(DECIMAL_FORMAT);

        // Accounting related variables
        String generalCode2 = "";
        String auxiliary = "";
        String reference = "";
        String title = "";
        String strDebit = "";
        String strCredit = "";
        String strAmountTTC = "";
        String strAmountHT = "";
        String strAmountTVA = "";
        float fAmountTTC = 0F;
        float fAmountHT = 0F;
        float fAmountTVA = 0F;

        // Variables used to organize the file
        StringBuilder firstLine = new StringBuilder();
        StringBuilder secondLine = new StringBuilder();
        StringBuilder thirdLine = new StringBuilder();
        StringBuilder totalLine = new StringBuilder();

        try {
            for (Facture facture : listFactures) {

                isCredit = facture.getIsAvoir();

                firstLine.setLength(0);
                secondLine.setLength(0);
                thirdLine.setLength(0);

                // Calculating the different amounts, the total depends on the invoice itself + all the various expenses attached to it
                fAmountHT = factureService.calculateTotalFactureAndElementFactureHt(facture);
                strAmountHT = decimalFormatter.format(fAmountHT);
                strAmountHT = strAmountHT.replace("-","");

                fAmountTVA = factureService.calculateTotalFactureAndElementFactureTva(facture);
                strAmountTVA = decimalFormatter.format(fAmountTVA);
                strAmountTVA = strAmountTVA.replace("-","");

                fAmountTTC = factureService.calculateTotalFactureAndElementFactureTtc(facture);
                strAmountTTC = decimalFormatter.format(fAmountTTC);
                strAmountTTC = strAmountTTC.replace("-","");

                strDate = dateFormatter.format(facture.getDateFacture());
                reference = "" + facture.getNumFacture();

                if (facture.getCommande() != null) {
                    auxiliary = facture.getCommande().getClient().getAuxiliaire();
                    title = facture.getCommande().getClient().getLibelleComptable();
                } else {
                    auxiliary = facture.getMission().getClient().getAuxiliaire();
                    title = facture.getMission().getClient().getLibelleComptable();
                }

                switch (typeFacture) {

                    // It's the same accountant code for contractors and subcontractors invoices
                    case TypeFacture.TYPEFACTURE_FACTURABLE_CSV:
                    case TypeFacture.TYPEFACTURE_FACTURABLE_SST_CSV:
                    case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_CSV:
                    case TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_SST_CSV:
                        if(facture.getCommande().getClient().getTva().contains(Constantes.FRENCH_INTRA_COMMUNITY_ACCOUNT_NUMBER_START)|| facture.getCommande().getClient().getTva().isEmpty()) {
                            generalCode2 = GENERAL_CODE_INVOICE_HANDWRITTEN_SUBCONTRACTOR;
                        }
                        else{
                            generalCode2 = GENERAL_CODE_INVOICE_INTRA_COMMUNITY_TVA_RATE;
                        }
                        break;

                    case TypeFacture.TYPEFACTURE_FACTURE_FRAIS_CSV:
                        generalCode2 = GENERAL_CODE_INVOICE_EXPENSES;
                        break;

                    default:
                        break;

                }

                // Generating the 3 lines' content
                String[] fileStructure = {ACCOUNTING_JOURNAL_VE, SEPARATOR_SEMICOLON, strDate, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_SEMICOLON, reference, SEPARATOR_SEMICOLON, title, SEPARATOR_SEMICOLON};
                firstLine = createLine(fileStructure, new String[]{GENERAL_CODE_INVOICE_1, auxiliary});
                secondLine = createLine(fileStructure, new String[]{generalCode2, ""});
                thirdLine = createLine(fileStructure, new String[]{GENERAL_CODE_INVOICE_3, ""});

                // In case of an invoice
                if (!isCredit) {

                    strDebit = strAmountTTC;

                    // First line
                    firstLine.append(strDebit + SEPARATOR_SEMICOLON + SEPARATOR_LINE);

                    // Second line
                    strCredit = strAmountHT;
                    secondLine.append(SEPARATOR_SEMICOLON + strCredit + SEPARATOR_LINE);

                    // Third line
                    strCredit = strAmountTVA;
                    thirdLine.append(SEPARATOR_SEMICOLON + strCredit + SEPARATOR_LINE);

                    // In case of a credit (avoir)
                } else {

                    strCredit = strAmountTTC;

                    // First line
                    firstLine.append(SEPARATOR_SEMICOLON + strCredit + SEPARATOR_LINE);

                    // Second line
                    strDebit = strAmountHT;
                    secondLine.append(strDebit + SEPARATOR_SEMICOLON + SEPARATOR_LINE);

                    // Third line
                    strDebit = strAmountTVA;
                    thirdLine.append(strDebit + SEPARATOR_SEMICOLON + SEPARATOR_LINE);

                }

                // Adding the three lines to the file
                totalLine.append(firstLine.toString() + secondLine.toString());
                if (!strDebit.equals(VALUE_ZERO_STR) && !strCredit.equals(VALUE_ZERO_STR)) {
                    totalLine.append(thirdLine.toString());
                }
            }

        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }
        return trim(totalLine.toString());
    }

    /**
     * Allows to generate the body of the CSV file displaying all the collaborators' expenses on a given month
     *
     * @param dateTime the date and time of the day
     * @return a String representing the body of the CSV file
     */
    private String createCSVLineFrais(DateTime dateTime) {

        Calendar startingCalendar = Calendar.getInstance();
        Calendar endingCalendar = Calendar.getInstance();

        // Calendar.MONTH is zero based so I have to subtract 1 month of the DateTime year.
        startingCalendar.set(dateTime.getYear(), dateTime.getMonthOfYear() - 1, 1);
        endingCalendar.set(dateTime.getYear(), dateTime.getMonthOfYear() - 1, startingCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        Date startingDate = startingCalendar.getTime();
        Date endingDate = endingCalendar.getTime();

        // Getting all the collaborators' expenses
        List<Object[]> listFrais = documentService.generateListeFrais(startingDate, endingDate, "simple");

        // Sorting the list in alphabetical order
        Collections.sort(listFrais, (l1, l2) -> collaboratorService.get(Long.parseLong(l1[0].toString())).getNom().compareTo(collaboratorService.get(Long.parseLong(l2[0].toString())).getNom()));

        // Declaring and initializing all the useful variables
        String strDate = "";
        String reference = "";
        String title = "";
        String strDebit = "";
        String strCredit = "";
        String strTotalFile = "";

        // Variables used to organize the file
        StringBuilder firstLine = new StringBuilder();
        StringBuilder secondLine = new StringBuilder();
        StringBuilder totalLine = new StringBuilder();

        double totalDebit = 0D;
        double totalCredit = 0D;

        SimpleDateFormat dateFormatter = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);
        SimpleDateFormat referenceFormatter = new SimpleDateFormat(DATE_FORMAT_SLASH_MONTH_YEAR);
        DecimalFormat decimalFormatter = new DecimalFormat(DECIMAL_FORMAT);

        try {
            for (Object[] frais : listFrais) {

                // Skip this iteration if the collaborator's expenses are at 0
                if (frais[5].toString().equals(VALUE_ZERO))
                    continue;

                firstLine.setLength(0);
                secondLine.setLength(0);


                Collaborator collaborateur = collaboratorService.get(Long.parseLong(frais[0].toString()));
                title = collaborateur.toString();

                Date lastDayOfMonth = dateTime.dayOfMonth().withMaximumValue().toDate();
                Date dateMonthYear = dateTime.toDate();
                reference = referenceFormatter.format(dateMonthYear);
                strDate = dateFormatter.format(lastDayOfMonth);

                // Balancing debit and credit
                strCredit = decimalFormatter.format(Double.parseDouble(frais[5].toString()));
                strDebit = strCredit;

                // Generating the 2 lines' content
                String[] fileStructure = {ACCOUNTING_JOURNAL_ODF, SEPARATOR_SEMICOLON, strDate, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_SEMICOLON, reference, SEPARATOR_SEMICOLON, title, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_SEMICOLON, VALUE_REPLACE, SEPARATOR_LINE};
                firstLine = createLine(fileStructure, new String[]{GENERAL_CODE_EXPENSES_1, AUXILIARY_EXPENSES, "", strCredit});
                secondLine = createLine(fileStructure, new String[]{GENERAL_CODE_EXPENSES_2, "", strDebit, ""});


                // Incrementing the total amount
                totalCredit += Double.parseDouble(frais[5].toString());
                totalDebit += Double.parseDouble(frais[5].toString());

                // Adding both lines to the file
                totalLine.append(firstLine.toString() + secondLine.toString());

            }

            // Generating the last line with the total amount
            strTotalFile = VALUE_TOTAL + SEPARATOR_SEMICOLON + "" + SEPARATOR_SEMICOLON + "" + SEPARATOR_SEMICOLON + "" + SEPARATOR_SEMICOLON + "" + SEPARATOR_SEMICOLON + "" + SEPARATOR_SEMICOLON + decimalFormatter.format(totalCredit) + SEPARATOR_SEMICOLON + decimalFormatter.format(totalDebit);

            // Adding the last line to the file
            totalLine.append(strTotalFile);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return trim(totalLine.toString());
    }

    /**
     * Allows to generate a CSV line based on two arrays.
     * <p>
     * Example: structure{"test","replace"} valuesToReplace{example};
     * Output: stringBuilder("test","example")
     *
     * @param structure       an array with all the "constant" values of the line with "replace" values to lookup inside the second array
     * @param valuesToReplace the variable values to insert where the "replace" string is encountered in the first array
     * @return a StringBuilder object representing the CSV line's content
     */
    private StringBuilder createLine(String[] structure, String[] valuesToReplace) {
        int valuesToReplaceIterator = 0;
        StringBuilder stringBuilder = new StringBuilder();

        for (int structureIterator = 0; structureIterator < structure.length; structureIterator++) {

            if (structure[structureIterator].equals(VALUE_REPLACE)) {
                stringBuilder.append(valuesToReplace[valuesToReplaceIterator]);
                valuesToReplaceIterator++;
            } else {
                stringBuilder.append(structure[structureIterator]);
            }

        }
        return stringBuilder;
    }

}
