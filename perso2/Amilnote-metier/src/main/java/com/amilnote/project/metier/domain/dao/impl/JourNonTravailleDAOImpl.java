/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.AbsenceDAO;
import com.amilnote.project.metier.domain.dao.JourNonTravailleDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.JourNonTravaille;
import com.amilnote.project.metier.domain.entities.TypeAbsence;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Absence dao.
 */
@Repository("jourNonTravailleDAO")
public class JourNonTravailleDAOImpl extends AbstractDAOImpl<JourNonTravaille> implements JourNonTravailleDAO {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<JourNonTravaille> getReferenceClass() {
        return JourNonTravaille.class;
    }

    /**
     * {@linkplain AbsenceDAO#getAllTypeAbsence()}
     */
    @Override
    public List<TypeAbsence> getAllTypeAbsence() {
        return currentSession().createCriteria(TypeAbsence.class).list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TypeAbsence getTypeAbsenceById(long id) {
        TypeAbsence lTypeAbsence = (TypeAbsence) currentSession().get(TypeAbsence.class, id);
        return lTypeAbsence;
    }
}



