/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.LinkTypeFraisCarac;

import java.util.List;

/**
 * The interface Link type frais carac dao.
 */
public interface LinkTypeFraisCaracDAO extends LongKeyDAO<LinkTypeFraisCarac> {

    /**
     * retrouver ts les LinkTypeFraisCarac
     *
     * @return List list
     */
    List<LinkTypeFraisCarac> findAll();

}
