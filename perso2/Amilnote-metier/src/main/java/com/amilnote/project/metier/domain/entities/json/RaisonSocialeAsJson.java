package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RaisonSociale;

public class RaisonSocialeAsJson {

    private Long id;
    private Collaborator collaborator;
    private String raisonSociale;

    public RaisonSocialeAsJson(RaisonSociale lRaisonSociale) {
        this.setId(lRaisonSociale.getId());
        this.setCollaborator(lRaisonSociale.getCollaborator());
        this.setRaisonSociale(lRaisonSociale.getRaisonSociale());
    }

    public RaisonSocialeAsJson() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collaborator getCollaborator() {
        return collaborator;
    }

    public void setCollaborator(Collaborator collaborator) {
        this.collaborator = collaborator;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }
}
