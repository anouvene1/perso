/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.MissionDAO;
import com.amilnote.project.metier.domain.dao.TypeMissionDAO;
import com.amilnote.project.metier.domain.entities.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.amilnote.project.metier.domain.entities.Mission.*;

/**
 * The type Mission dao.
 */
@Repository("missionDAO")
public class MissionDAOImpl extends AbstractDAOImpl<Mission> implements MissionDAO {

    @Autowired
    private EtatDAO etatDAO;

    @Autowired
    private TypeMissionDAO typeMissionDAO;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Mission> getReferenceClass() {
        return Mission.class;
    }

    /**
     * Gets mission by id.
     *
     * @param pIdMission the p id mission
     * @return the mission by id
     */
    public Mission getMissionById(Long pIdMission) {
        Criteria lCritMission = getCriteria();
        lCritMission.add(Restrictions.eq(Mission.PROP_ID, pIdMission));
        return (Mission) lCritMission.uniqueResult();
    }

    /**
     * {@linkplain MissionDAO#findAllMissionsNotFinishForCollaborator(Collaborator, DateTime)}
     */
    @Override
    public List<Mission> findAllMissionsNotFinishForCollaborator(Collaborator pCollaborator, DateTime pDateFin) {

        // Récupère toutes les missions
        Criteria lListMission = getCriteria();
        // on filtre sur le collaborateur
        lListMission.add(Restrictions.eq(PROP_COLLABORATEUR, pCollaborator));
        // Toute les missions qui ne sont pas terminées après la date "dateFin"
        lListMission.add(Restrictions.ge(PROP_DATEFIN, pDateFin.toDate()));
        //On trie les missions par date de début
        lListMission.addOrder(org.hibernate.criterion.Order.asc(PROP_MISSION));
        return (List<Mission>) lListMission.list();
    }

    /**
     * {@linkplain MissionDAO#findByDate(Collaborator, DateTime)}
     */
    @Override
    public List<Mission> findByDate(Collaborator pCollaborator, DateTime dateFin) {
        Criteria linkMissionByDateAndCollaborator = getCriteria();
        //critere de sélection selon collaborateur passé en parametre
        linkMissionByDateAndCollaborator.add(Restrictions.like(PROP_COLLABORATEUR, pCollaborator));
        linkMissionByDateAndCollaborator.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        //critere selon date fin passée en parametre
        linkMissionByDateAndCollaborator.add(Restrictions.lt(Mission.PROP_DATEFIN, dateFin));
        return (List<Mission>) linkMissionByDateAndCollaborator.list();
    }

    /**
     * {@linkplain MissionDAO#findDefaultMissionForCollaborator(Collaborator)}
     */
    @Override
    public Mission findDefaultMissionForCollaborator(Collaborator pCollaborator) {
        // Récupère toutes les missions
        Criteria lMission = getCriteria();
        // on filtre sur le collaborateur
        lMission.add(Restrictions.eq(PROP_COLLABORATEUR, pCollaborator));
        // Toute les missions qui ne sont pas terminées après la date "dateFin"
        lMission.add(Restrictions.eq(Mission.PROP_ISDEFAULT, 1));
        return (Mission) lMission.uniqueResult();
    }

    /**
     * {@linkplain MissionDAO#findMissionEncoursPeriode(Collaborator, DateTime, DateTime)}
     */
    @Override
    public List<Mission> findMissionEncoursPeriode(Collaborator pCollaborator, DateTime pDebutPeriode, DateTime pFinPeriode) {
        // Récupère toutes les missions
        Criteria lListMission = getCriteria();
        // on filtre sur le collaborateur
        lListMission.add(Restrictions.eq(PROP_COLLABORATEUR, pCollaborator));
        // Toute les missions en cours pendants la période
        lListMission.add(Restrictions.ge(Mission.PROP_DATEFIN, pDebutPeriode.toDate()));
        lListMission.add(Restrictions.le(Mission.PROP_DATEDEBUT, pFinPeriode.toDate()));
        return (List<Mission>) lListMission.list();
    }


    /**
     * {@linkplain MissionDAO#findMissionsClientesByMonth(DateTime)}
     */
    @Override
    public List<Mission> findMissionsClientesByMonth(DateTime pDate) throws Exception {
        Criteria criteria = getCriteria();
        TypeMission typeMission = typeMissionDAO.findUniqEntiteByProp(TypeMission.PROP_CODE, TypeMission.MISSION_CLIENTE);

        criteria.add(Restrictions.eq(Mission.PROP_TYPEMISSION, typeMission));

        // Toute les missions en cours pendants la période

        Date dateDebut = pDate.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDate.getMonthOfYear() == 12) {

            dateFin = pDate.withMonthOfYear(01).withYear(pDate.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDate.withMonthOfYear(pDate.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = simpleDateFormat.parse(dateD);
        Date pDateFin = simpleDateFormat.parse(dateF);

        criteria.add(Restrictions.ge(Mission.PROP_DATEFIN, pDateDebut));
        criteria.add(Restrictions.lt(Mission.PROP_DATEDEBUT, pDateFin));

        return (List<Mission>) criteria.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionsClientesByMonthForCollaborator(Collaborator, DateTime)}
     */
    @Override
    public List<Mission> findMissionsClientesByMonthForCollaborator(Collaborator pCollaborator, DateTime pDate) {
        Criteria lListMission = getCriteria();
        TypeMission typeMission = typeMissionDAO.findUniqEntiteByProp(TypeMission.PROP_CODE, TypeMission.MISSION_CLIENTE);

        lListMission.add(Restrictions.eq(Mission.PROP_TYPEMISSION, typeMission));
        // Toute les missions en cours pendants la période
        DateTime lDebutPeriode = pDate.dayOfMonth().withMinimumValue();
        pDate = pDate.plusMonths(1);
        DateTime lFinPeriode = pDate.dayOfMonth().withMaximumValue();
        lListMission.add(Restrictions.ge(PROP_DATEFIN, lDebutPeriode.toDate()));
        lListMission.add(Restrictions.le(PROP_DATEDEBUT, lFinPeriode.toDate()));
        lListMission.add(Restrictions.eq(PROP_COLLABORATEUR, pCollaborator));

        return (List<Mission>) lListMission.list();
    }

    /**
     * {@linkplain MissionDAO#getNbMissionAttenteVal()}
     */
    @Override
    public int getNbMissionAttenteVal() {

        Criteria lnbMissions = currentSession().createCriteria(Mission.class);

        Etat etatBR = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);
        lnbMissions.add(Restrictions.eq(Mission.PROP_ETAT, etatBR));

        List<Mission> listeMissions = lnbMissions.list();

        return listeMissions.size();
    }

    /**
     * {@linkplain MissionDAO#getNbMissionAttenteVal(Collaborator)}
     */
    @Override
    public int getNbMissionAttenteVal(Collaborator pCollaborator) {

        Criteria lnbMissions = currentSession().createCriteria(Mission.class);

        Etat etatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        lnbMissions.add(Restrictions.eq(PROP_ETAT, etatSO));
        lnbMissions.add(Restrictions.eq(PROP_COLLABORATEUR, pCollaborator));

        List<Mission> listeMissions = lnbMissions.list();

        return listeMissions.size();
    }

    /**
     * {@linkplain MissionDAO#findAllMissionEnCours(Date)}
     */
    @Override
    public List<Mission> findAllMissionEnCours(Date date) {
        Criteria linkMissionEnCours = getCriteria();
        Criterion dateDebut = Restrictions.le(Mission.PROP_DATEDEBUT, date);
        Criterion dateFin = Restrictions.ge(Mission.PROP_DATEFIN, date);
        linkMissionEnCours.add(Restrictions.and(dateDebut, dateFin));
        return (List<Mission>) linkMissionEnCours.list();
    }

    /**
     * {@linkplain MissionDAO#findAllMissionForCollaborator(DateTime, Collaborator)}
     */
    @Override
    public List<Mission> findAllMissionForCollaborator(DateTime date, Collaborator collaborateur) {
        Criteria linkMissionForCollab = getCriteria();
        Criterion dateDebut = Restrictions.le(PROP_DATEDEBUT, date);
        // la valeur de la propriété doit être inferieure à la valeur fournie en paramètre
        Criterion dateFin = Restrictions.ge(PROP_DATEFIN, date);
        Criterion collab = Restrictions.eq(PROP_COLLABORATEUR, collaborateur.getId());
        linkMissionForCollab.add(Restrictions.and(collab, Restrictions.and(dateDebut, dateFin)));
        return linkMissionForCollab.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionClientEnCours(Date)}
     */
    @Override
    public List<Mission> findMissionClientEnCours(Date date) {
        Criteria linkMissionClientEnCours = getCriteria();
        Criterion dateDebut = Restrictions.le(PROP_DATEDEBUT, date);
        // la valeur de la propriété doit être inferieur à la valeur fourni en parametre
        Criterion dateFin = Restrictions.ge(PROP_DATEFIN, date);
        // la valeur de la propriété doit être inférieur à la valeur fournie en parametre
        Criterion typeMission = Restrictions.like(PROP_TYPEMISSION, typeMissionDAO.get(5l));
        linkMissionClientEnCours.add(dateDebut);
        linkMissionClientEnCours.add(dateFin);
        linkMissionClientEnCours.add(typeMission);
        linkMissionClientEnCours.add(Restrictions.and(typeMission, Restrictions.and(dateDebut, dateFin)));
        return linkMissionClientEnCours.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionClientActuelle(DateTime)}
     */
    @Override
    public List<Mission> findMissionClientActuelle(DateTime pDate) {
        Criteria linkMissionClientEnCours = getCriteria();
        Date dateMinimum = pDate.dayOfMonth().withMinimumValue().toDate();
        Date dateMaximumm = pDate.dayOfMonth().withMaximumValue().toDate();

        Criterion dateDebut = Restrictions.le(PROP_DATEDEBUT, dateMaximumm);

        // la valeur de la propriété doit être inferieur à la valeur fourni en parametre
        Criterion dateFin = Restrictions.ge(PROP_DATEFIN, dateMinimum);

        // la valeur de la propriété doit être inférieur à la valeur fournie en parametre
        Criterion typeMission = Restrictions.like(PROP_TYPEMISSION, typeMissionDAO.get(5L));
        linkMissionClientEnCours.add(Restrictions.and(typeMission, Restrictions.and(dateDebut, dateFin)));
        return linkMissionClientEnCours.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionEnCoursNonClient(Date)}
     */
    @Override
    public List<Mission> findMissionEnCoursNonClient(Date date) {
        Criteria linkMissionEnCoursNonClient = getCriteria();
        Criterion dateDebut = Restrictions.le(Mission.PROP_DATEDEBUT, date);
        // la valeur de la propriété doit être inferieur à la valeur fourni en parametre
        Criterion dateFin = Restrictions.ge(Mission.PROP_DATEFIN, date);
        // la valeur de la propriété doit être inférieur à la valeur fournie en parametre
        Criterion typeMission = Restrictions.ne(Mission.PROP_TYPEMISSION, typeMissionDAO.get(5l));
        linkMissionEnCoursNonClient.add(Restrictions.and(typeMission, Restrictions.and(dateDebut, dateFin)));
        return (List<Mission>) linkMissionEnCoursNonClient.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionForCollaboratorBetweenTwoDates(Date, Date, Collaborator)}
     */
    @Override
    public List<Mission> findMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator) {
        Criteria linkMissionForCollaboratorBetweenTwoDates = getCriteria();
        // date de debut de mission > date debut
        Criterion debutSup = Restrictions.ge(PROP_DATEDEBUT, debut);
        // date debut mission < date fin
        Criterion endLess = Restrictions.le(PROP_DATEDEBUT, fin);
        // date debut mission < date debut
        Criterion debutless = Restrictions.le(PROP_DATEDEBUT, debut);
        // date fin mission > date debut
        Criterion endSup = Restrictions.ge(PROP_DATEFIN, debut);
        Criterion collab = Restrictions.eq(PROP_COLLABORATEUR, collaborator.getId());
        // liste = collab ET ((debutSup ET endless) OU (debutLess ET endSup))
        linkMissionForCollaboratorBetweenTwoDates.add(Restrictions.and(collab, Restrictions.or(Restrictions.and(debutSup, endLess), Restrictions.and(debutless, endSup))));
        //
        return linkMissionForCollaboratorBetweenTwoDates.list();
    }

    /**
     * {@linkplain MissionDAO#findClientMissionForCollaboratorBetweenTwoDates(Date, Date, Collaborator)}
     */
    public List<Mission> findClientMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborateur) {

        Criteria linkMissionForCollaboratorBetweenTwoDates = getCriteria();
        Criterion debutSup = Restrictions.ge(Mission.PROP_DATEDEBUT, debut);
        Criterion endLess = Restrictions.le(Mission.PROP_DATEDEBUT, fin);
        Criterion debutless = Restrictions.le(Mission.PROP_DATEDEBUT, debut);
        Criterion endSup = Restrictions.ge(Mission.PROP_DATEFIN, debut);
        Criterion collab = Restrictions.eq(PROP_COLLABORATEUR, collaborateur);
        Criterion typeMission = Restrictions.eq(Mission.PROP_TYPEMISSION, typeMissionDAO.get(Mission.MISSION_CLIENT_TYPE_ID));

        linkMissionForCollaboratorBetweenTwoDates.add(Restrictions.and(collab, typeMission, Restrictions.or(Restrictions.and(debutSup, endLess), Restrictions.and(debutless, endSup))));

        return linkMissionForCollaboratorBetweenTwoDates.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionBetweenTwoDates(Date, Date)}
     */
    @Override
    public List<Mission> findMissionBetweenTwoDates(Date debut, Date fin) {
        Criteria linkMissionBetweenTwoDates = getCriteria();
        // date de debut de mission superieur date debut
        Criterion debutSup = Restrictions.ge(Mission.PROP_DATEDEBUT, debut);
        // date debut mission < date fin
        Criterion endLess = Restrictions.le(Mission.PROP_DATEDEBUT, fin);
        // date debut mission < date debut
        Criterion debutless = Restrictions.le(Mission.PROP_DATEDEBUT, debut);
        // date fin mission superieur date debut
        Criterion endSup = Restrictions.ge(Mission.PROP_DATEFIN, debut);
        // liste = collab ET ((debutSup ET endless) OU (debutLess ET endSup))
        linkMissionBetweenTwoDates.add(Restrictions.or(Restrictions.and(debutSup, endLess), Restrictions.and(debutless, endSup)));
        //
        return linkMissionBetweenTwoDates.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionNonClientBetweenTwoDates(Date, Date)}
     */
    @Override
    public List<Mission> findMissionNonClientBetweenTwoDates(Date debut, Date fin) {
        Criteria linkMissionNonClientBetweenTwoDates = getCriteria();
        // date de debut de mission > date debut
        Criterion debutSup = Restrictions.ge(Mission.PROP_DATEDEBUT, debut);
        // date debut mission < date fin
        Criterion endLess = Restrictions.le(Mission.PROP_DATEDEBUT, fin);
        // date debut mission < date debut
        Criterion debutless = Restrictions.le(Mission.PROP_DATEDEBUT, debut);
        // date fin mission > date debut
        Criterion endSup = Restrictions.ge(Mission.PROP_DATEFIN, debut);

        Criterion missionNonCliente = Restrictions.ne(Mission.PROP_TYPEMISSION, typeMissionDAO.get(5l));
        linkMissionNonClientBetweenTwoDates.add(Restrictions.and(missionNonCliente, Restrictions.or(Restrictions.and(debutSup, endLess), Restrictions.and(debutless, endSup))));
        return linkMissionNonClientBetweenTwoDates.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionNonClientBetweenTwoDatesForCollab(Date, Date, Collaborator)}
     */
    @Override
    public List<Mission> findMissionNonClientBetweenTwoDatesForCollab(Date debut, Date fin, Collaborator collaborateur) {
        Criteria linkMissionForCollaboratorBetweenTwoDates = getCriteria();
        // date de debut de mission supperieur date debut
        Criterion debutSup = Restrictions.ge(Mission.PROP_DATEDEBUT, debut);
        // date debut mission inferieur date fin
        Criterion endLess = Restrictions.le(Mission.PROP_DATEDEBUT, fin);
        // date debut mission inferieur date debut
        Criterion debutless = Restrictions.le(Mission.PROP_DATEDEBUT, debut);
        // date fin mission supperieur date debut
        Criterion endSup = Restrictions.ge(Mission.PROP_DATEFIN, debut);
        Criterion collab = Restrictions.eq(PROP_COLLABORATEUR, collaborateur.getId());
        // liste = collab ET ((debutSup ET endless) OU (debutLess ET endSup))
        Criterion missionNonCliente = Restrictions.ne(Mission.PROP_TYPEMISSION, typeMissionDAO.get(5l));
        linkMissionForCollaboratorBetweenTwoDates.add(Restrictions.and(missionNonCliente, Restrictions.and(collab, Restrictions.or(Restrictions.and(debutSup, endLess), Restrictions.and(debutless, endSup)))));
        //
        return linkMissionForCollaboratorBetweenTwoDates.list();
    }

    /**
     * {@linkplain MissionDAO#findMissionByTypeCollaboratorStatusAndCurrentPeriod(TypeMission, StatutCollaborateur, Date)}
     */
    @Override
    public List<Mission> findMissionByTypeCollaboratorStatusAndCurrentPeriod(TypeMission typeMission, StatutCollaborateur collaboratorStatus, Date lastDayOfMonth) {
        final String alias = "m";
        Criteria criteria = getCurrentSession().createCriteria(Mission.class, alias);

        criteria.createAlias(alias + "." + PROP_COLLABORATEUR, PROP_COLLABORATEUR, JoinType.INNER_JOIN);
        criteria.createAlias(alias + "." + PROP_CLIENT, PROP_CLIENT, JoinType.LEFT_OUTER_JOIN);
        criteria.createAlias(alias + "." + PROP_COMMANDES, PROP_COMMANDES, JoinType.LEFT_OUTER_JOIN);

        criteria.add(Restrictions.eq(alias + "." + PROP_TYPEMISSION, typeMission));
        criteria.add(Restrictions.eq(PROP_COLLABORATEUR + "." + Collaborator.PROP_STATUT, collaboratorStatus));
        criteria.add(Restrictions.eq(PROP_COLLABORATEUR + "." + Collaborator.PROP_ENABLED, true));

        Date start = new DateTime(lastDayOfMonth).minusMonths(2).toDate();
        criteria.add(Restrictions.between(alias + "." + PROP_DATEDEBUT, start, lastDayOfMonth));


        return criteria.list();
    }
}
