package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.Utils;
import com.amilnote.project.metier.domain.utils.enumerations.ClientExcelEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * Service permettant la génération de tout type de document (Excel, PDF...)
 *
 * @author kimbert
 */
@Service("DocumentService")
public class DocumentServiceImpl implements DocumentService {

    private static Logger logger = LogManager.getLogger(DocumentServiceImpl.class);

    private static final String CSV_FOOTER_LEFT = "Signature :";
    private static final String CSV_FOOTER_CENTER = "Signature Directeur :";

    private static final String FROM = "* du ";
    private static final String TO = " au ";

    private static final String COLLABS_EN_MISSION = "LISTE DES COLLABORATEURS EN MISSION CLIENTE";
    private static final String STT_EN_MISSION = "LISTE DES SOUS-TRAITANTS EN MISSION CLIENTE";

    private static final String TYPE_MISSIONCLIENT_CODE = "MI";

    /**
     * The Collaborator service.
     */
    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;
    /**
     * The Tva service.
     */
    @Autowired
    TvaService tvaService;
    /**
     * The Commande service.
     */
    @Autowired
    CommandeService commandeService;
    /**
     * The Mission service.
     */
    @Autowired
    private MissionService missionService;

    @Autowired
    private CalculFraisService calculFraisService;

    /**
     * Retourne une feuille excel correctement formatée pour les factures
     *
     * @param wb un Workbook
     * @return sheet
     */
    public static Sheet createSheetFacture(Workbook wb) {
        Sheet sheet = wb.createSheet();
        sheet.setFitToPage(true);
        sheet.setAutobreaks(true);
        sheet.setHorizontallyCenter(true);

        // print setup
        HSSFPrintSetup mySheetPS = (HSSFPrintSetup) sheet.getPrintSetup();
        mySheetPS.setLandscape(true);
        mySheetPS.setPaperSize(HSSFPrintSetup.A4_PAPERSIZE);
        mySheetPS.setFitWidth((short) 1); // prendre toute la page sur la longueur
        mySheetPS.setFitHeight((short) 0);

        // footer
        sheet.getFooter().setLeft(CSV_FOOTER_LEFT);
        sheet.getFooter().setCenter(CSV_FOOTER_CENTER);

        return sheet;
    }

    /**
     * Retourne une feuille excel correctement formatée pour les frais
     *
     * @param wb un Workbook
     * @return sheet
     */
    public static Sheet createSheetFrais(Workbook wb) {
        return createSheetFacture(wb);
    }

    @Override
    public int createLine(double nbJoursParMission, Float tjm, int rowNumber, Sheet mySheet, String fullName, String missionName, String client) {
        int lRowNumber = rowNumber;
        if (nbJoursParMission > 0) {
            //calcul du total à facturer
            double total = nbJoursParMission * tjm;

            // Création de la ligne
            lRowNumber++;
            Row myRow = mySheet.createRow(lRowNumber);

            // Ajouter des données dans les cellules
            myRow.createCell(0).setCellValue(lRowNumber - 1);
            myRow.createCell(1).setCellValue(fullName);
            myRow.createCell(2).setCellValue(missionName);
            myRow.createCell(3).setCellValue(client);
            myRow.createCell(4).setCellValue(tjm);
            myRow.createCell(5).setCellValue(nbJoursParMission);
            myRow.createCell(6).setCellValue(total);
        }
        return lRowNumber;
    }

    @Override
    public int createLinesByMissions(Collaborator collaborateur, int year, int rowNumber, Sheet mySheet) {
        //on recupere son nom et son prenom
        String nom = collaborateur.getNom();
        String prenom = collaborateur.getPrenom();
        Float tjm;
        double nbJoursParMission;
        String missionName;
        String client;

        List<Mission> allMission = collaborateur.getMissions();
        for (Mission mission : allMission) { //et pour chaque mission
            //si elle est à l'état validé et est une mission cliente,

            Calendar calDebut = Calendar.getInstance();
            calDebut.setTime(mission.getDateDebut());
            int anneeDebut = calDebut.get(Calendar.YEAR);

            Calendar calFin = Calendar.getInstance();
            calFin.setTime(mission.getDateFin());
            int anneeFin = calFin.get(Calendar.YEAR);


            if (mission.getTypeMission().getCode().equals("MI") && year >= anneeDebut && year <= anneeFin) {
                missionName = mission.getMission();
                tjm = mission.getTjm();
                nbJoursParMission = 0;
                client = !isNull(mission.getClient()) ? mission.getClient().getNom_societe() : "";

                List<LinkEvenementTimesheet> timesheets = mission.getLinkEvenementTimesheetLine();

                //calcul du nombre de jours à facturer en additionnant chaque timesheet
                // de la mission (du mois courant)
                for (LinkEvenementTimesheet timesheet : timesheets) {

                    Absence tmpAbsence = timesheet.getAbsence();
                    Calendar calTS = Calendar.getInstance();
                    calTS.setTime(timesheet.getDate());

                    nbJoursParMission = calTS.get(Calendar.YEAR) == year && !Utils.isAbsenceVAOrSO(tmpAbsence) ?
                            nbJoursParMission + 0.5 : nbJoursParMission;
                }

                /* affichage uniquement des lignes si pour le mois en cours  :
                            - le collaborateur a une mission cliente validée
                            - le rapport d'activité
                */
                rowNumber = createLine(nbJoursParMission, tjm, rowNumber, mySheet, nom + " " + prenom,
                        missionName, client);
            }
        }
        return rowNumber;
    }

    @Override
    public int createLinesByRA(Collaborator collaborateur, int month, int year, int rowNumber, Sheet mySheet) {
        //on récupère ses RA
        List<RapportActivites> ras = collaborateur.getRapportActivites();

        for (RapportActivites ra : ras) { //et pour chaque RA
            //si c'est le RA du mois courant
            Calendar calRA = Calendar.getInstance();
            calRA.setTime(ra.getMoisRapport());
            if (calRA.get(Calendar.MONTH) == month && calRA.get(Calendar.YEAR) == year &&
                    (ra.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE) ||
                            ra.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE))) {
                //alors on regarde chacune de ses missions
                List<Mission> allMission = collaborateur.getMissions();
                for (Mission mission : allMission) { //et pour chaque mission
                    rowNumber = createLineByDaysToInvoice(collaborateur, mission, month, year, rowNumber, mySheet);
                }
            }
        }
        return rowNumber;
    }

    @Override
    public int createLineByDaysToInvoice(Collaborator collaborateur, Mission mission, int month, int year, int rowNumber, Sheet mySheet) {
        String nom = collaborateur.getNom();
        String prenom = collaborateur.getPrenom();

        //si c'est une mission cliente,
        if ("MI".equals(mission.getTypeMission().getCode())) {
            String missionName = mission.getMission();
            float tjm = mission.getTjm();

            double nbJoursParMission = 0;
            String client = !isNull(mission.getClient()) ? mission.getClient().getNom_societe() : "";
            List<LinkEvenementTimesheet> timesheets = mission.getLinkEvenementTimesheetLine();

            //calcul du nombre de jours à facturer en additionnant chaque timesheet
            // de la mission (du mois courant)
            for (LinkEvenementTimesheet timesheet : timesheets) {

                Absence tmpAbsence = timesheet.getAbsence();
                Calendar calTS = Calendar.getInstance();
                calTS.setTime(timesheet.getDate());

                if (calTS.get(Calendar.MONTH) == month && calTS.get(Calendar.YEAR) == year
                        && !Utils.isAbsenceVAOrSO(tmpAbsence)) {
                    nbJoursParMission = nbJoursParMission + 0.5;
                }
            }

            /* affichage uniquement des lignes si pour le mois en cours  :
                - le collaborateur a une mission cliente validée
                - le rapport d'activité
             */
            rowNumber = createLine(nbJoursParMission, tjm, rowNumber, mySheet, nom + " " + prenom, missionName, client);
        }
        return rowNumber;
    }

    @Override
    public String updateAbsenceDates(String abs, String absenceDateDeb, String absenceDateFin) {
        if (!abs.isEmpty()) {
            // Insert line return inside cell
            abs += "\r\n";
        }
        abs += FROM + absenceDateDeb;
        if (!absenceDateDeb.equals(absenceDateFin)) {
            abs += TO + absenceDateFin + ".";
        }
        return abs;
    }

    @Override
    public List<Object[]> generateListeFrais(Date dateDebut, Date dateFin, String typeDetail) {

        Map<Long, Object[]> mapAvanceFrais = new HashMap<>();
        Map<Long, Map<Long, Object[]>> mapFraisDetail = new HashMap<>();
        Map<Long, Map<Long, Float>> mapForfaitDetail = new HashMap<>();
        Map<Long, Integer> mapAbsence = new HashMap<>();

        List<Object[]> listTotal = new ArrayList<>();
        List<Object[]> listFrais = new ArrayList<>();
        List<Object[]> listAvanceFrais = new ArrayList<>();

        try {
            //recuperation des objets frais autre que voiture et avance
            listFrais.addAll(calculFraisService.findFraisByDate(dateDebut, dateFin, "Autre"));
            //recuperation des objet frais voiture
            listFrais.addAll(calculFraisService.findFraisByDate(dateDebut, dateFin, "Voiture"));
            //recuperation des objets frais Avance
            listAvanceFrais.addAll(calculFraisService.findFraisByDate(dateDebut, dateFin, "Avance"));


            //calcul forfait du collaborateur
            List<Object[]> listForfait = new ArrayList<>();
            List<Object[]> listAbsence = new ArrayList<>();

            // pour chaque absence un matin, on retire un jour de forfait repas
            listAbsence.addAll(calculFraisService.findAbsence(dateDebut, dateFin));
            for (Object[] o : listAbsence) {
                if (((Integer) o[1] == 0 && (Long) o[3] == 1)) {
                    if (mapAbsence.containsKey((Long) o[0])) {
                        Long collab = (Long) o[0];

                        int i = mapAbsence.get((Long) o[0]);
                        mapAbsence.put((Long) o[0], i + 1);
                    } else {
                        mapAbsence.put((Long) o[0], 1);
                    }
                }
            }

            //liste forfait repas
            listForfait.addAll(calculFraisService.findForfaitByDate(dateDebut, dateFin, "repas"));
            // liste forfait mensuel
            listForfait.addAll(calculFraisService.findForfaitByDate(dateDebut, dateFin, "mensuel"));
            //liste forfait autre
            listForfait.addAll(calculFraisService.findForfaitByDate(dateDebut, dateFin, "autre"));

            mapAvanceFrais.putAll(calculFraisService.doCalculAvance(listAvanceFrais)); //calcul frais autre
            mapFraisDetail.putAll(calculFraisService.doCalculFraisDetail(listFrais)); // calcul forfait
            mapForfaitDetail.putAll(calculFraisService.doCalculForfaitsDetail(listForfait, mapFraisDetail, mapAbsence)); // calcul avance frais
            listTotal.addAll(calculFraisService.doCalculDetail(typeDetail, mapFraisDetail, mapForfaitDetail, mapAvanceFrais));//Frais totaux
        } catch (Exception e) {
            logger.error(DocumentServiceImpl.class + ": Error => " + e.getMessage());
        }
        return listTotal;
    }

    @Override
    public int createClientsFileRows(ClientExcelEnum choixExcel, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index) {
        // On converti le collaborateurJson en collaborateur.
        Collaborator collaborateur = collaboratorService.get(collaboratorAsJson.getId());

        // si l'utilisateur a cliqué sur "Collaborators en mission cliente" ou "Collaborators en mission cliente (Sous-Traitants)
        if (ClientExcelEnum.CLIENTS_PAR_COLLAB != choixExcel && ClientExcelEnum.CLIENTS_PAR_STT != choixExcel) {

            // on récupère l'index de la dernière ligne où a eu lieu l'écriture, pour reprendre au bon endroit par la suite
            index = createCollaboratorsOnClientMissionsCells(collaborateur, collaboratorAsJson, myRow, mySheet, index);
        }

        // si l'utilisateur a cliqué sur "Clients du Mois par Collaborator" ou "Clients du Mois par Collaborator (Sous-Traitant)
        else if (ClientExcelEnum.CLIENTS_PAR_COLLAB == choixExcel || ClientExcelEnum.CLIENTS_PAR_STT == choixExcel) {

            // on récupère l'index de la dernière ligne où a eu lieu l'écriture, pour reprendre au bon endroit par la suite
            index = createClientsPerCollaboratorCells(collaborateur, collaboratorAsJson, myRow, mySheet, index);
        }

        for (int i = 0; i < 7; i++) {
            mySheet.autoSizeColumn(i, true);
        }
        mySheet.createFreezePane(3, 2); // this will freeze first 2 rows(titres) and 3 columns (id,nom,prénom)

        // on renvoie l'index pour savoir à quelle ligne reprendre pour le prochain collaborateur
        return index;
    }

    @Override
    public void prepareClientFile(ClientExcelEnum choixExcel, Font font, Sheet mySheet, Row myRow, Workbook wb, CellStyle style) {

        mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) 24));

        style.setAlignment(CellStyle.ALIGN_CENTER);
        font.setBold(true);
        font.setFontHeight((short) (16 * 20));
        style.setFont(font);

        Date monthAndYear = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_MMMM_YYYY);
        String monthAndYearString = " - ".concat(dateFormat.format(monthAndYear)).toUpperCase();

        if (ClientExcelEnum.SOUSTRAITANTS_EN_MISSION == choixExcel || ClientExcelEnum.CLIENTS_PAR_STT == choixExcel) {
            Utils.createCellule(myRow, wb, 0, STT_EN_MISSION + monthAndYearString, null, style);
        } else {
            Utils.createCellule(myRow, wb, 0, COLLABS_EN_MISSION + monthAndYearString, null, style);
        }

        // Création de la deuxième ligne avec les titres des colonnes
        myRow = mySheet.createRow(1);
        // Numéro
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        font = wb.createFont();
        font.setBold(true);
        style.setFont(font);

        Utils.createCellule(myRow, wb, 0, "ID", null, style);
        // Nom
        Utils.createCellule(myRow, wb, 1, "Nom", null, style);
        // Prenom
        Utils.createCellule(myRow, wb, 2, Constantes.COLUMN_EXCEL_FIRST_NAME, null, style);


        if (ClientExcelEnum.CLIENTS_PAR_COLLAB != choixExcel && ClientExcelEnum.CLIENTS_PAR_STT != choixExcel) {

            // Type de Mission
            Utils.createCellule(myRow, wb, 3, "Type de Mission", null, style);
            // Mission
            Utils.createCellule(myRow, wb, 4, Constantes.COLUMN_EXCEL_MISSION, null, style);
            // Fin de mission
            Utils.createCellule(myRow, wb, 5, "Fin de mission", null, style);
            // Client
            Utils.createCellule(myRow, wb, 6, Constantes.COLUMN_EXCEL_CLIENT, null, style);
        } else {
            for (int i = 1, y = 3; i < 6; i++, y++) {
                Utils.createCellule(myRow, wb, y, Constantes.COLUMN_EXCEL_CLIENT + " " + i, null, style);
            }
        }
    }

    @Override
    public int createCollaboratorsOnClientMissionsCells(Collaborator collaborateur, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index) {

        Date today = new Date();
        SimpleDateFormat formater = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);

        List<Mission> listeMission = missionService.findAllMissionsNotFinishForCollaborator(collaborateur,
                Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR);

        for (Mission mission : listeMission) {
            //Si c'est une mission cliente et qu'elle n'est pas finie
            if (mission.getDateFin().after(today) && !"SI".equals(mission.getTypeMission().getCode()) && !"AC".equals(mission.getTypeMission().getCode())
                    && !"FO".equals(mission.getTypeMission().getCode()) && !"MN".equals(mission.getTypeMission().getCode())) {
                // Création de la ligne i
                myRow = mySheet.createRow(index);

                // ajout nom du COLLAB
                // premiere case de la ligne i du collab i
                myRow.createCell(0).setCellValue(index - 1);
                myRow.createCell(1).setCellValue(collaboratorAsJson.getNom());
                myRow.createCell(2).setCellValue(collaboratorAsJson.getPrenom());
                myRow.createCell(3).setCellValue(mission.getTypeMission().getTypeMission());
                myRow.createCell(4).setCellValue(mission.getMission());
                myRow.createCell(5).setCellValue(formater.format(mission.getDateFin()));
                if (mission.getClient() != null) {
                    myRow.createCell(6).setCellValue(mission.getClient().getNom_societe());
                } else {
                    myRow.createCell(6).setCellValue("Non Renseigné");
                }
                index = index + 1;
            }
        }
        return index;
    }

    @Override
    public int createClientsPerCollaboratorCells(Collaborator collaborateur, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        Date debutMois = calendar.getTime();

        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        Date finMois = calendar.getTime();

        List<LinkEvenementTimesheet> evenementsMissionClient = new ArrayList<>();
        List<String> listeClients = new ArrayList<>();
        List<String> listeClientsNonDupliques = new ArrayList<>();

        try {
            // récupère tous les évenements du mois en cours d'un collaborateur
            evenementsMissionClient = linkEvenementTimesheetService.findEventsBetweenDates(collaborateur, debutMois, finMois);
        } catch (JsonProcessingException jsonError) {
            logger.error(DocumentServiceImpl.class + ": Json Processing error => " + jsonError.getMessage());
        }

        // pour chaque evenement du mois en cours, vérifie s'il s'agit d'une mission cliente et l'ajoute dans une liste
        for (LinkEvenementTimesheet evenement : evenementsMissionClient) {
            if ((evenement.getMission() != null && TYPE_MISSIONCLIENT_CODE.equals(evenement.getMission().getTypeMission().getCode()) && evenement.getAbsence() == null)
                    || (evenement.getMission() != null && TYPE_MISSIONCLIENT_CODE.equals(evenement.getMission().getTypeMission().getCode()) && evenement.getAbsence() != null && !Utils.isAbsenceVAOrSO(evenement.getAbsence()))) {

                listeClients.add(evenement.getMission().getClient().getNom_societe());
            }
        }

        // filtre la liste des clients en retirant tous les doublons
        listeClientsNonDupliques = listeClients.stream().distinct().collect(Collectors.toList());

        if (listeClientsNonDupliques.size() > 0) {
            // l'éctiture commence a la 4e colonne;
            int clientCellule = 3;

            myRow = mySheet.createRow(index);

            // ajout nom du COLLAB
            // premiere case de la ligne i du collab i
            myRow.createCell(0).setCellValue(index - 1);
            myRow.createCell(1).setCellValue(collaboratorAsJson.getNom());
            myRow.createCell(2).setCellValue(collaboratorAsJson.getPrenom());

            // pour chaque client différent, on ajoute une colonne
            for (String nomClient : listeClientsNonDupliques) {
                myRow.createCell(clientCellule).setCellValue(nomClient);
                clientCellule++;
            }
            index++;
        }

        return index;
    }

    @Override
    public void autoSizeColumns(Workbook workbook) {

        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            short numRowsTodo = 3;
            if (sheet.getPhysicalNumberOfRows() > numRowsTodo) {
                for (int j = 0; j < numRowsTodo; j++) {
                    Row row = sheet.getRow(j);
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        int columnIndex = cell.getColumnIndex();
                        sheet.autoSizeColumn(columnIndex);
                    }
                }
            }
        }
    }

    @Override
    public String getCheminFichier(DateTime date, String cheminMonthODF, String numODF, TypeFacture.InvoiceType typeFacture) throws NamingException {

        String chemin;
        //SBE_AMNOTE-183 chemin où l'on enregistre l'excel si on ne se trouve pas dans le cas de soumission des factures
        String odf = null;
        String fichier = "";
        String ficExtension = "";
        String ficTimeStamp = Utils.moisIntToString(date.getMonthOfYear() - 1) + "_" + date.getYear();
        String nomBaseFichier = "";

        //determination de l'extension suivant le type de facture

        ficExtension = ".xls";
        nomBaseFichier = Parametrage.getContext(Constantes.TEMP_FOLDER_FILES);
        if (numODF == null) {
            switch (typeFacture) {
                case FRABLE:
                    fichier = "Recapitulatif_Factures_" + ficTimeStamp;
                    break;
                case FRABLESST:
                    fichier = "Recapitulatif_Factures_SST_" + ficTimeStamp;
                    break;
                case FMAIN:
                    fichier = "Recapitulatif_Factures_A_La_Main_" + ficTimeStamp;
                    break;

                default:
                    break;
            }

        } else {

            fichier = "Factures_soumises_" + ficTimeStamp + numODF;

        }


        if (cheminMonthODF == null) {
            chemin = nomBaseFichier + fichier + ficExtension;
        } else {
            chemin = nomBaseFichier + "/" + cheminMonthODF + "/" + fichier + ficExtension;
        }
        return chemin;
    }

    @Override
    public void sortListFactures(List<Facture> listeFactures) {

        Collections.sort(listeFactures, new Comparator<Facture>() {
            @Override
            public int compare(Facture o1, Facture o2) {
                if (o1.getNumFacture() < o2.getNumFacture()) {
                    return -1;
                } else if (o1.getNumFacture() == o2.getNumFacture()) {
                    return 0;
                } else return 1;
            }
        });
    }

    @Override
    public void creerDossiersPourFichier(String cheminFichier) throws IOException {

        Path directoryPath = (new File(cheminFichier).getParentFile()).toPath(); //on récupère les dossiers parents

        try {
            Files.createDirectories(directoryPath); //s'il en manque on les crée
        } catch (Exception ex) {
            logger.error("Erreur lors de la creation du dossier:" + ex.getMessage());
        }
    }

    @Override
    public float calculFraisTJMElementFacture(Facture facture) {

        List<ElementFacture> listeElementFacture = elementFactureService.findByFacture(facture);
        float fraisTJM = 0;

        if (!listeElementFacture.isEmpty()) {
            for (ElementFacture elementFacture : listeElementFacture) {
                if (elementFacture.getQuantiteElement() != 0) {
                    fraisTJM += elementFacture.getMontantElement();
                }
            }
        }
        return fraisTJM;
    }

    @Override
    public String createTitle(String[] arrayName, String[] monthNames) {

        boolean isActive = isActive(arrayName);
        boolean isMonth = false;
        String mois = null;

        Pair<Boolean, String> pair = checkMonth(arrayName, monthNames);
        isMonth = pair.getFirst();
        if (pair.getSecond() != null) {
            mois = pair.getSecond();
        }

        StringBuilder title = new StringBuilder();

        if (isActive && isMonth) {
            title.append(Constantes.HEADER_EXCEL_MONTHLY_ACTIVES_ORDER);
            title.append(mois + " ");
        } else if (isActive && !isMonth) {
            title.append(Constantes.HEADER_EXCEL_YEARLY_ACTIVES_ORDER);
        } else if (!isActive && isMonth) {
            title.append(Constantes.HEADER_EXCEL_MONTHLY_STOPPED_ORDER);
            title.append(mois + " ");
        } else if (!isActive && !isMonth) {
            title.append(Constantes.HEADER_EXCEL_YEARLY_STOPPED_ORDER);
        }

        return title.toString();
    }

    @Override
    public boolean isActive(String[] arrayName) {

        boolean isActive = false;

        for (String string : arrayName) {
            if (string.toLowerCase().contains("cours")) {
                isActive = true;
                break;
            } else if (string.toLowerCase().contains("terminees")) {
                isActive = false;
                break;
            }
        }
        return isActive;
    }

    @Override
    public Pair<Boolean, String> checkMonth(String[] arrayName, String[] months) {

        boolean isMonth = false;
        String mois = null;

        for (String string : arrayName) {
            for (int i = 0; i < months.length; i++) {
                if (string.toLowerCase().contains(months[i].toLowerCase())) {
                    isMonth = true;
                    mois = months[i];
                    break;
                }
            }
        }
        return new Pair<>(isMonth, mois);
    }
}
