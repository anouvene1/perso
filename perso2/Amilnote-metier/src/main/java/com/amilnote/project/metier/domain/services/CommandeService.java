/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;

/**
 * The interface Commande service.
 */
public interface CommandeService extends AbstractService<Commande> {

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pCommande the p commande as json
     * @param pMissionJson    the p mission json
     * @return string string
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    String createOrUpdateCommande(Commande pCommande, Mission pMissionJson) throws JsonProcessingException, IOException;

    public String createOrUpdateCommande(CommandeAsJson pCommandeAsJson, MissionAsJson pMissionAsJson)throws JsonProcessingException, IOException;

    /**
     * Suppression de la commande
     *
     * @param pCommande the p commande
     * @return string string
     */
    String deleteCommande(Commande pCommande);

    /**
     * Find all order by nom asc list.
     *
     * @return the list
     */
    List<Commande> findAllOrderByNomAsc();

    /**
     * Find by id commande.
     *
     * @param pIdCommande the p id commande
     * @return the commande
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    Commande findById(Long pIdCommande);

    /**
     * retrouver toutes les commandes et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<CommandeAsJson> findAllOrderByNomAscAsJson();

    /**
     * Gets list commande facture.
     *
     * @param listLinkEvent the list link event
     * @return the list commande facture
     */
    List<Commande> getListCommandeFacture(List<LinkEvenementTimesheet> listLinkEvent);

    /**
     * Trouver toutes les commandes encore actives jusqu'à la date de passage du cron
     * @return the list
     */
    List<Commande> findActivesCommandes();

    /**
     * Trouver les commandes pour le mois passé en paramètre
     * @param date date
     * @param code cpde
     * @return a list of {@link Commande}
     */
    List<Commande> findCommandesForMonth(String date, String code);

    /**
     * Trouver toutes les commandes en cours
     * @return a list of {@link Commande}
     */
    List<Commande> findStartedCommandes();

    /**
     * Trouver toutes les commandes terminées
     * @return a list of {@link Commande}
     */
    List<Commande> findStoppedCommandes();
    /**
     * Retourne la commande correspondant à une mission à une date voulue
     * @param mission l'identifiant de la mission correspondante
     * @param dateVoulue le mois voulu
     * @return la commande
     * @author clome
     */
    List<Commande> findByIdMissionByMonth(Mission mission, DateTime dateVoulue);

    /**
     * Retourne les commandes correspondant à une mission
     * @param mission l'identifiant de la mission correspondante
     * @return la commande
     * @author clome
     */
    List<Commande> findByIdMission(Mission mission);
}
