package com.amilnote.project.metier.domain.utils.enumerations;

public enum DomainNames {
    AMILTONE ("@amiltone.com"),
    NEWROAD ("@data-newroad.com");

    private final String domainName;

    DomainNames(final String domainName) {
        this.domainName = domainName;
    }

    @Override
    public String toString() {
        return this.domainName;
    }
}
