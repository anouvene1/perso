/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.DemandeDeplacementDAO;
import com.amilnote.project.metier.domain.entities.DemandeDeplacement;
import org.springframework.stereotype.Repository;

/**
 * The type Demande deplacement dao.
 */
@Repository("demandeDeplacementDAO")
public class DemandeDeplacementDAOImpl extends AbstractDAOImpl<DemandeDeplacement> implements DemandeDeplacementDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<DemandeDeplacement> getReferenceClass() {
        return DemandeDeplacement.class;
    }
}
