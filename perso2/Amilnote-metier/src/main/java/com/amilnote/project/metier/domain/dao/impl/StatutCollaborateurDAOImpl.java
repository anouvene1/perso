/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.StatutCollaborateurDAO;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * The type Statut collaborateur dao.
 */
@Repository("statutCollaborateurDAO")
public class StatutCollaborateurDAOImpl extends AbstractDAOImpl<StatutCollaborateur> implements StatutCollaborateurDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<StatutCollaborateur> getReferenceClass() {
        return StatutCollaborateur.class;
    }

    @Override
    public StatutCollaborateur findStatutCollaborateurByCode(String code) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(StatutCollaborateur.PROP_CODE, code));
        return (StatutCollaborateur) criteria.uniqueResult();
    }
}
