/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkTypeFraisCarac;

/**
 * The type Link type frais carac as json.
 */
public class LinkTypeFraisCaracAsJson {

    private Long id;
    private TypeFraisAsJson typeFrais;
    private CaracteristiqueAsJson carac;

    /**
     * Instantiates a new Link type frais carac as json.
     *
     * @param pId        the p id
     * @param pTypeFrais the p type frais
     * @param pCarac     the p carac
     */
    public LinkTypeFraisCaracAsJson(Long pId, TypeFraisAsJson pTypeFrais, CaracteristiqueAsJson pCarac) {
        super();
        id = pId;
        typeFrais = pTypeFrais;
        carac = pCarac;
    }

    /**
     * Instantiates a new Link type frais carac as json.
     *
     * @param pLinkTypeFraisCarac the p link type frais carac
     */
    public LinkTypeFraisCaracAsJson(LinkTypeFraisCarac pLinkTypeFraisCarac) {
        super();
        id = pLinkTypeFraisCarac.getId();
        typeFrais = pLinkTypeFraisCarac.getTypeFrais().toJson();
        carac = pLinkTypeFraisCarac.getCarac().toJson();
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets type frais.
     *
     * @return the type frais
     */
    public TypeFraisAsJson getTypeFrais() {
        return typeFrais;
    }

    /**
     * Sets type frais.
     *
     * @param pTypeFrais the p type frais
     */
    public void setTypeFrais(TypeFraisAsJson pTypeFrais) {
        typeFrais = pTypeFrais;
    }

    /**
     * Gets carac.
     *
     * @return the carac
     */
    public CaracteristiqueAsJson getCarac() {
        return carac;
    }

    /**
     * Sets carac.
     *
     * @param pCarac the p carac
     */
    public void setCarac(CaracteristiqueAsJson pCarac) {
        carac = pCarac;
    }


}
