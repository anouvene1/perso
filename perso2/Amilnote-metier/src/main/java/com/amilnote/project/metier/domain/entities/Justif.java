/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.JustifAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Justif.
 */
@Entity
@Table(name = "ami_justif")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Justif implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_DESCRIPTIF.
     */
    public static final String PROP_DESCRIPTIF = "descriptif";

    /**
     * The constant PROP_FICHIER.
     */
    public static final String PROP_FICHIER = "fichier";

    /**
     * The constant PROP_FRAIS.
     */
    public static final String PROP_FRAIS = "frais";

    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_COLLABORATEUR = "collaborateur";

    private static final long serialVersionUID = 2290863870942055022L;

    private Long id;

    private String descriptif;

    private String fichier;

    private List<Frais> frais = new ArrayList<Frais>();

    private Collaborator collaborator;

    /**
     * Constructeur par defaut de la classe TypeFrais
     */
    public Justif() {
    }

    /**
     * Constructeur de la classe TypeFrais
     *
     * @param pDescriptif the p descriptif
     * @param pFichier    the p fichier
     * @param pFrais      the p frais
     */
    public Justif(String pDescriptif, String pFichier, List<Frais> pFrais) {
        super();
        descriptif = pDescriptif;
        fichier = pFichier;
        setFrais(pFrais);
    }

    /*
     * GETTERS AND SETTERS
     */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets descriptif.
     *
     * @return the Poste
     */
    @Column(name = "nom_descriptif", nullable = false)
    public String getDescriptif() {
        return descriptif;
    }

    /**
     * Sets descriptif.
     *
     * @param pDescriptif the p descriptif
     */
    public void setDescriptif(String pDescriptif) {
        this.descriptif = pDescriptif;
    }

    /**
     * Gets fichier.
     *
     * @return the code
     */
    @Column(name = "fichier", nullable = false)
    public String getFichier() {
        return fichier;
    }

    /**
     * Sets fichier.
     *
     * @param pFichier the p fichier
     */
    public void setFichier(String pFichier) {
        this.fichier = pFichier;
    }

    /**
     * Gets frais.
     *
     * @return the frais
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "justifs")
    public List<Frais> getFrais() {
        return frais;
    }

    /**
     * Sets frais.
     *
     * @param pFrais the frais to set
     */
    public void setFrais(List<Frais> pFrais) {
        frais = pFrais;
    }

    /**
     * To json justif as json.
     *
     * @return the justif as json
     */
    public JustifAsJson toJson() {
        return new JustifAsJson(this);
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    @ManyToOne
    @JoinColumn(name = "id_collaborateur", nullable = true)
    public Collaborator getCollaborator() {
        return collaborator;
    }

    /**
     * Sets collaborateur.
     *
     * @param pCollaborateur the p collaborateur
     */
    public void setCollaborator(Collaborator pCollaborateur) {
        collaborator = pCollaborateur;
    }
}
