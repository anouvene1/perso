/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.DemandeDeplacement;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Mission;

import java.util.Date;

/**
 * The type Demande deplacement as json.
 *
 * @author AJakubiak
 */
public class DemandeDeplacementAsJson {

    private Long id;
    private Mission mission;
    private String codeProjet;
    private String motif;
    private Date dateDebut;
    private Date dateFin;
    private Etat etat;
    private int avance;
    private Date dateSoumission;
    private Date dateValidation;
    private String lieu;
    private String validationClient;
    private String commentaire;
    private Float nbJours;
    private Collaborator collaborateur;
    private String pdfDeplacement;

    /**
     * Constructeur par défaut de la classe DemandeDeplacementAsJson
     */
    public DemandeDeplacementAsJson() {
    }

    /**
     * Instantiates a new Demande deplacement as json.
     *
     * @param pDemandeDeplacement the p demande deplacement
     */
    public DemandeDeplacementAsJson(DemandeDeplacement pDemandeDeplacement) {
        this.setAvance(pDemandeDeplacement.getAvance());
        this.setCodeProjet(pDemandeDeplacement.getCodeProjet());
        this.setDateDebut(pDemandeDeplacement.getDateDebut());
        this.setDateFin(pDemandeDeplacement.getDateFin());
        this.setDateSoumission(pDemandeDeplacement.getDateSoumission());
        this.setDateValidation(pDemandeDeplacement.getDateValidation());
        this.setLieu(pDemandeDeplacement.getLieu());
        this.setEtat(pDemandeDeplacement.getEtat());
        this.setId(pDemandeDeplacement.getId());
        this.setMission(pDemandeDeplacement.getMission());
        this.setMotif(pDemandeDeplacement.getMotif());
        this.setValidationClient(pDemandeDeplacement.getValidationClient());
        this.setCommentaire(pDemandeDeplacement.getCommentaire());
        this.setCollaborateur(pDemandeDeplacement.getCollaborator());
        this.setNbJours(pDemandeDeplacement.getNbJours());
    }

    /**
     * Gets id.
     *
     * @return id de la demande de déplacement
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets mission.
     *
     * @return Mission associée au déplacement
     */
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(Mission mission) {
        this.mission = mission;
    }

    /**
     * Gets code projet.
     *
     * @return Code projet Client
     */
    public String getCodeProjet() {
        return codeProjet;
    }

    /**
     * Sets code projet.
     *
     * @param codeProjet the code projet
     */
    public void setCodeProjet(String codeProjet) {
        this.codeProjet = codeProjet;
    }

    /**
     * Gets motif.
     *
     * @return Motif du déplacement
     */
    public String getMotif() {
        return motif;
    }

    /**
     * Sets motif.
     *
     * @param motif the motif
     */
    public void setMotif(String motif) {
        this.motif = motif;
    }

    /**
     * Gets date debut.
     *
     * @return Date début du déplacement
     */
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param dateDebut the date debut
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return Date Fin du déplacement
     */
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param dateFin the date fin
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets etat.
     *
     * @return Etat (validé, soumis, refusé, annulé)
     */
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets avance.
     *
     * @return the avance
     */
    public int getAvance() {
        return avance;
    }

    /**
     * Sets avance.
     *
     * @param avance the avance
     */
    public void setAvance(int avance) {
        this.avance = avance;
    }

    /**
     * Gets date soumission.
     *
     * @return Date de soumission de la demande
     */
    public Date getDateSoumission() {
        return dateSoumission;
    }

    /**
     * Sets date soumission.
     *
     * @param dateSoumission the date soumission
     */
    public void setDateSoumission(Date dateSoumission) {
        this.dateSoumission = dateSoumission;
    }

    /**
     * Gets date validation.
     *
     * @return Date de validation de la demande par l'admin
     */
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param dateValidation the date validation
     */
    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    /**
     * Gets lieu.
     *
     * @return Lieu du déplacement
     */
    public String getLieu() {
        return lieu;
    }

    /**
     * Sets lieu.
     *
     * @param lieu the lieu
     */
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    /**
     * Gets validation client.
     *
     * @return the validationClient
     */
    public String getValidationClient() {
        return validationClient;
    }

    /**
     * Sets validation client.
     *
     * @param validationClient the validation client
     */
    public void setValidationClient(String validationClient) {
        this.validationClient = validationClient;
    }

    /**
     * Gets commentaire.
     *
     * @return Commentaire à propose de la demande
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets nb jours.
     *
     * @return the nbJours
     */
    public Float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param nbJours the nb jours
     */
    public void setNbJours(Float nbJours) {
        this.nbJours = nbJours;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public Collaborator getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborateur the collaborateur
     */
    public void setCollaborateur(Collaborator collaborateur) {
        this.collaborateur = collaborateur;
    }

    /**
     * Gets pdf deplacement.
     *
     * @return the pdfDeplacement
     */
    public String getPdfDeplacement() {
        return pdfDeplacement;
    }

    /**
     * Sets pdf deplacement.
     *
     * @param pdfDeplacement the pdf deplacement
     */
    public void setPdfDeplacement(String pdfDeplacement) {
        this.pdfDeplacement = pdfDeplacement;
    }
}
