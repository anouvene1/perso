/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.ami_factureDeCommande;

import java.util.Date;
import java.util.List;

/**
 * The interface Facturede commande service.
 */
public interface FacturedeCommandeService extends AbstractService<ami_factureDeCommande> {

    /**
     * Find all by date list.
     *
     * @param date the date
     * @return the list
     */
    List<ami_factureDeCommande> findAllByDate(Date date);

}
