/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.TypeFraisDAO;
import com.amilnote.project.metier.domain.dao.impl.TypeFraisDAOImpl;
import com.amilnote.project.metier.domain.entities.TVA;
import com.amilnote.project.metier.domain.entities.TypeFrais;
import com.amilnote.project.metier.domain.entities.json.TypeFraisAsJson;
import com.amilnote.project.metier.domain.services.TvaService;
import com.amilnote.project.metier.domain.services.TypeFraisService;
import com.amilnote.project.metier.domain.utils.TypeFraisForm;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Type frais service.
 */
@Service("typeFraisService")
public class TypeFraisServiceImpl extends AbstractServiceImpl<TypeFrais, TypeFraisDAOImpl> implements TypeFraisService {

    @Autowired
    private TypeFraisDAO typeFraisDAO;

    @Autowired
    private TvaService tvaService;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public TypeFraisDAOImpl getDAO() {
        return (TypeFraisDAOImpl) typeFraisDAO;
    }

    /**
     * {@linkplain TypeFraisService#getAllTypeFrais()}
     */
    @Override
    public List<TypeFrais> getAllTypeFrais() {
        return typeFraisDAO.getAllTypeFrais();
    }

    /**
     * {@linkplain TypeFraisService#getTypeFraisById(long)}
     */
    @Override
    public TypeFrais getTypeFraisById(long pId) {
        return typeFraisDAO.getTypeFraisById(pId);
    }

    /**
     * {@linkplain TypeFraisService#getAllTypeFraisAsJson()}
     */
    @Override
    public List<TypeFraisAsJson> getAllTypeFraisAsJson() {
        List<TypeFrais> lListTypeFrais = typeFraisDAO.getAllTypeFrais();
        Collections.sort(lListTypeFrais);
        List<TypeFraisAsJson> lTypeFraisAsJson = new ArrayList<>();

        for (TypeFrais lTypeFrais : lListTypeFrais) {
            lTypeFraisAsJson.add(new TypeFraisAsJson(lTypeFrais));
        }
        return lTypeFraisAsJson;
    }

    /**
     * {@linkplain TypeFraisService#updateAllTVAForFrais(TypeFraisForm)}
     */
    @Override
    public void updateAllTVAForFrais(TypeFraisForm pTypeFrais) {
        TVA tmpTVA = null;
        TypeFrais tmpTypeFrais = null;

        // --- Création de la session et de la transaction
        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        for (TypeFraisAsJson tmpTypeFraisAsJson : pTypeFrais.getListTypeFrais()) {
            // --- Récupération des entités grâce à leurs ID
            tmpTVA = tvaService.getTVAById(tmpTypeFraisAsJson.getTva().getId());
            tmpTypeFrais = get(tmpTypeFraisAsJson.getId());

            // --- Application de la nouvelle TVA au Frais
            tmpTypeFrais.setTva(tmpTVA);

            session.saveOrUpdate(tmpTypeFrais);
        }
        session.flush();
        transaction.commit();
    }

    /**
     * {@linkplain TypeFraisService#getTypeFraisByCode(String)}
     */
    @Override
    public TypeFrais getTypeFraisByCode(String pCode) {

        TypeFrais frais = null;
        List<TypeFrais> lListFrais = getAllTypeFrais();
        for (TypeFrais tempFrais : lListFrais) {
            if (tempFrais.getCode().equals(pCode)) {
                frais = tempFrais;
            }
        }
        return frais;
    }

}
