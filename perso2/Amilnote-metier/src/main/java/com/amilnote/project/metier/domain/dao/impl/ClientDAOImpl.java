/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ClientDAO;
import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The type Client dao.
 */
@Repository("clientDAO")
public class ClientDAOImpl extends AbstractDAOImpl<Client> implements ClientDAO {

    private static final Logger logger = LogManager.getLogger(ClientDAOImpl.class);

    @Autowired
    private ContactClientDAOImpl contactClientDAO;

    @Autowired
    private MissionDAOImpl missionDAO;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Client> getReferenceClass() {
        return Client.class;
    }

    /**
     * {@linkplain ClientDAO#findAllOrderByNomAsc()}
     */
    @Override
    public List<Client> findAllOrderByNomAsc() {
        Criteria lCritClient = getCriteria();
        lCritClient.addOrder(Order.asc(Client.PROP_NOM));
        return lCritClient.list();
    }

    /**
     * {@linkplain ClientDAO#updateOrCreateClient(ClientAsJson)}
     */
    @Override
    public String updateOrCreateClient(ClientAsJson pClientAsJson) throws Exception {
        String messageRetour = "";
        if (null == pClientAsJson) {
            throw new Exception("Le client ne doit pas être vide");
        }
        Client tmpClient = null;
        // Modification d'un client existant
        if (null != pClientAsJson.getId()) {
            tmpClient = this.get(pClientAsJson.getId());
            if (null == tmpClient) {
                messageRetour = "Erreur: L'identifiant du client est inconnu";
            }
            if (tmpClient != null) {
                if (null != pClientAsJson.getNom_societe()) {
                    tmpClient.setNom_societe(pClientAsJson.getNom_societe());
                }
                if (null != pClientAsJson.getAdresse()) {
                    tmpClient.setAdresse(pClientAsJson.getAdresse());
                }
                if (null != pClientAsJson.getAdresse_facturation()) {
                    tmpClient.setAdresseFacturation(pClientAsJson.getAdresse_facturation());
                }
                if (null != pClientAsJson.getContacts()) {
                    tmpClient.setContacts(pClientAsJson.getContacts());
                }
                if (null != pClientAsJson.getTva()) {
                    tmpClient.setTva(pClientAsJson.getTva());
                }
                if (null != pClientAsJson.getRibClient()) {
                    tmpClient.setRibClient(pClientAsJson.getRibClient());
                }
                if (null != pClientAsJson.getAuxiliaire()) {
                    tmpClient.setAuxiliaire(pClientAsJson.getAuxiliaire());
                }
                if (null != pClientAsJson.getLibelleComptable()) {
                    tmpClient.setLibelleComptable(pClientAsJson.getLibelleComptable());
                }
                messageRetour = "<h4>Modification effectuée !</h4> <p>Le client à été mis à jour avec succès.</p>";
            }
        } else {
            tmpClient = new Client(
                    pClientAsJson.getNom_societe(),
                    pClientAsJson.getAdresse(),
                    pClientAsJson.getAdresse_facturation(),
                    pClientAsJson.getTva(),
                    pClientAsJson.getRibClient(),
                    pClientAsJson.getAuxiliaire(),
                    pClientAsJson.getLibelleComptable(),
                    null
            );
            messageRetour = "<h4>Enregistrement effectué !</h4> <p>Le nouveau client à été créé avec succès.</p>";
        }
        // --- Création de la session et de la transaction
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        // --- Sauvegarde et Commit de la mise à jour
        session.save(tmpClient);
        session.flush();
        transaction.commit();
        return messageRetour;
    }

    /**
     * {@linkplain ClientDAO#deleteClient(Client)}
     */
    @Override
    public String deleteClient(Client pClient) {
        // --- Récupération du client
        Client client = this.get(pClient.getId());
        Date today = Calendar.getInstance().getTime();
        // --- Test de l'existance du client
        if (null != client) {
            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            //On supprime les contacts associés à ce client
            List<ContactClient> listContacts = contactClientDAO.findListEntitesByProp(ContactClient.PROP_CLIENT, client);
            List<Mission> listMissionsClient = missionDAO.findListEntitesByProp(Mission.PROP_CLIENT, client);
            List<Mission> missionsToUpdate = new ArrayList<Mission>();
            Boolean missionEnCours = false;
            StringBuilder listCollab = new StringBuilder();
            for (Mission mission : listMissionsClient) {
                if (mission.getDateFin().after(today)) {
                    missionEnCours = true;
                    listCollab.append("<p>" + mission.getCollaborateur().getMail() + ": " + mission.getMission() + "</p>");
                } else {
                    mission.setClient(null);
                    mission.setResp_client(null);
                    missionsToUpdate.add(mission);
                }
            }
            if (missionEnCours) {
                return "<p>Erreur: Une ou plusieurs mission en cours se déroulent chez ce client: " + listCollab
                        + "<p>Veuillez vérifier qu'il ne soit plus assigné à  aucune mission en cours avant de le supprimer.</p>";
            }
            for (Mission mission : missionsToUpdate) {
                session.save(mission);
            }
            for (ContactClient contact : listContacts) {
                session.delete(contact);
            }
            session.delete(client);
            // --- Sauvegarde et Commit de la mise à jour
            try {
                session.flush();
                transaction.commit();
            } catch (Exception e) {
                logger.error("[commit transaction] delete client id : {}", client.getId(), e);
                return "Impossible de supprimer le client. Vérifiez qu'aucun contact ne soit associé à une misison en cours.";
            }
            return "<h4>Suppression effectuée !</h4> <p>Le client à été supprimé avec succès.</p>";
        } else {
            return "Erreur client null";
        }
    }
}
