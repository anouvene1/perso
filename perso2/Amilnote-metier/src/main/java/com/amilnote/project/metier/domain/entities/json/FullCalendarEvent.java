/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import org.joda.time.DateTime;


/**
 * Représente un Event compatible avec FullCalendar
 */
public class FullCalendarEvent {
    private Long id = null;

    private Long title;

    private String start;

    private String end;

    private Boolean allDay = false;

    private AbsenceAsJson absence;

    private MissionAsJson mission;

    private int nbFrais;


    /**
     * Instantiates a new Full calendar event.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     */
    public FullCalendarEvent(LinkEvenementTimesheet pLinkEvenementTimesheet) {
        this.setId(pLinkEvenementTimesheet.getId());

        this.setTitle(pLinkEvenementTimesheet.getId());

        this.setStart(pLinkEvenementTimesheet.calculDateStart());

        this.setEnd(pLinkEvenementTimesheet.calculDateEnd());

        if (pLinkEvenementTimesheet.getAbsence() != null) {
            this.setAbsence(new AbsenceAsJson(pLinkEvenementTimesheet.getAbsence()));
        }
        if (pLinkEvenementTimesheet.getMission() != null) {
            this.setMission(new MissionAsJson(pLinkEvenementTimesheet.getMission()));
        }

        this.setNbFrais(pLinkEvenementTimesheet.getListFrais().size());
    }

    /**
     * Instantiates a new Full calendar event.
     */
    public FullCalendarEvent() {
    }

    /**
     * Constructor for holidays full calendar event.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     * @return the full calendar event
     */
    public FullCalendarEvent constructorForHolidays(LinkEvenementTimesheet pLinkEvenementTimesheet) {

        this.setStart(pLinkEvenementTimesheet.calculDateStart());
        this.setEnd(pLinkEvenementTimesheet.calculDateEnd());

        return this;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(long pId) {
        id = pId;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Long getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param pLong the p long
     */
    public void setTitle(Long pLong) {
        title = pLong;
    }


    /**
     * Gets start.
     *
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * Récupère une date au format DateTime et la transforme en String
     *
     * @param pStart the p start
     */
    public void setStart(DateTime pStart) {
        start = pStart.toString();
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public String getEnd() {
        return end;
    }

    /**
     * Récupère une date au format DateTime et la transforme en String
     *
     * @param pEnd the p end
     */
    public void setEnd(DateTime pEnd) {
        end = pEnd.toString();
    }

    /**
     * Gets all day.
     *
     * @return the all day
     */
    public Boolean getAllDay() {
        return allDay;
    }

    /**
     * Sets all day.
     *
     * @param pAllDay the p all day
     */
    public void setAllDay(Boolean pAllDay) {
        allDay = pAllDay;
    }

    /**
     * Gets absence.
     *
     * @return the absence
     */
    public AbsenceAsJson getAbsence() {
        return absence;
    }

    /**
     * Sets absence.
     *
     * @param pAbsence the p absence
     */
    public void setAbsence(AbsenceAsJson pAbsence) {
        absence = pAbsence;
    }


    /**
     * Gets mission.
     *
     * @return the idMission
     */
    public MissionAsJson getMission() {
        return mission;
    }


    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(MissionAsJson pMission) {
        mission = pMission;
    }

    /**
     * Gets nb frais.
     *
     * @return the nbFrais
     */
    public int getNbFrais() {
        return nbFrais;
    }

    /**
     * Sets nb frais.
     *
     * @param pNbFrais the nbFrais to set
     */
    public void setNbFrais(int pNbFrais) {
        nbFrais = pNbFrais;
    }


}
