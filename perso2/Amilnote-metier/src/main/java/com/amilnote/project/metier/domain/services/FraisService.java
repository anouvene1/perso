/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.utils.Pair;
import org.joda.time.DateTime;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The interface Frais service.
 */
public interface FraisService extends AbstractService<Frais> {

    /**
     * récupérer toutes les demandes d'avance de frais en cours
     *
     * @return list list
     */
    List<Frais> findFraisAvanceDeFrais();

    /**
     * trouver l'ensemble des frais enregistrés triés par date pour la mission passée en parametre
     *
     * @param pMission the p mission
     * @param pMois    the p mois
     * @param pAnnee   the p annee
     * @return list list
     */
    List<Object[]> findAllByMissionOrderByDateCriteria(Mission pMission, int pMois, int pAnnee);

    List<Object[]> findAllByMissionOrderByDate(Mission pMission, int pMois, int pAnnee);


    /**
     * trouver l'ensemble des frais enregistrés triés par date pour la mission passée en parametre
     *
     * @param pMission the p mission
     * @param date    the p date
     * @return list list
     */
    List<Object[]> findAllByMissionOrderByDateCriteria(Mission pMission, DateTime date);

    /**
     * trouver tous les justificatifs attachés à des frais pour la mission passées en parametre
     *
     * @param pMission the p mission
     * @return list list
     */
    List<byte[]> findJustifsByMission(Mission pMission);


    /**
     * creation d'un nouveau frais
     *
     * @param proofFile the p justif
     * @throws Exception save file errors
     */
    void saveNewFrais(MultipartFile proofFile) throws Exception;

    /**
     * suppression d'un ou de plusieurs frais
     *
     * @param pListFraisToDelete the p list frais to delete
     * @return string string
     */
    String deleteFrais(List<Long> pListFraisToDelete);

    /**
     * modification d'un frais
     *
     * @param pIdFrais         the p id frais
     * @param pMontant         the p montant
     * @param pLinkTFraisCarac the p link t frais carac
     * @param pLinkEvtTS       the p link evt ts
     * @param pARembourser     the p a rembourser
     * @param pCommentaire     the p commentaire
     * @param pNbKm            the p nb km
     * @param pJustif          the p justif
     * @return string string
     */
    String modifyFrais(Long pIdFrais, BigDecimal pMontant, LinkTypeFraisCarac pLinkTFraisCarac, LinkEvenementTimesheet pLinkEvtTS, BigDecimal pARembourser, String pCommentaire, Double pNbKm, String pJustif);

    /**
     * retourne le lien du justificatif d'un frais
     *
     * @param pIdFrais the p id frais
     * @return picture for frais
     */
    Pair<String, InputStream> getPictureForFrais(Long pIdFrais);

    /**
     * modifie l'etat des frais
     *
     * @param pListFrais the p list frais
     * @return string string
     */
    String modifyEtatFrais(List<Frais> pListFrais);

    /**
     * Retourne la liste des frais pour le collaborateur souhaité au format JSon
     *
     * @param pDateVoulue Date souhaitée
     * @return Liste des frais du collaborateur en fonction de la date
     */
    List<Frais> getAllFraisByMonth(DateTime pDateVoulue);

    /**
     * Retourne la liste des frais pour le collaborateur souhaité au format JSon
     *
     * @param collaborator Collaborateur souhaité
     * @return Liste des frais du collaborateur au format JSon
     */
    List<FraisAsJson> getFraisByCollaborator(Collaborator collaborator);

    /**
     * Retourne la liste des frais à l'état brouillon pour le collaborateur souhaité au format JSon
     *
     * @param collaborator Collaborateur souhaité
     * @return Lsite des frais du collaborateur au format JSon
     */
    List<FraisAsJson> getFraisBOByCollaborator(Collaborator collaborator);

    /**
     * Créer ou met à jour un frais
     *
     * @param pFrais Frais souhaité
     * @param pMission a {@link Mission} related to the frais
     * @return Message d'information
     * @throws Exception the exception
     */
    String createOrUpdateFrais(FraisAsJson pFrais, Mission pMission) throws Exception;

    /**
     * Méthode permettant de soumettre une demande d'avance de frais
     *
     * @param pIdFrais the p id frais
     * @throws Exception the exception
     */
    void soumissionDemandeAvance(Long pIdFrais) throws Exception;

    /**
     * Méthode permettant de retirer le justification de la liste des justificatifs liés au frais
     *
     * @param idFrais  ID du frais à traiter
     * @param justif ID du justificatif à retirer
     * @throws Exception the exception
     */
    void supprJustificatifToFrais(Long idFrais, Justif justif) throws Exception;

    /**
     * Méthode permettant de calculer le nombre de repas (hors forfait) pris pour le RA donné
     *
     * @param pMission Mission en cours de calcul
     * @param dateDeb  la date de début du mois en cours pour le RA
     * @param dateFin  la date de fin du mois en cours pour le RA
     * @return the integer
     */
    Integer findNbRepasFraisMidi(Mission pMission, DateTime dateDeb, DateTime dateFin);

    /**
     * Méthode permettant de récupérer les frais pour le RA en cours
     *
     * @param collaborator Collaborateur ayant soumis son RA
     * @param pDateRAVoulue Date du RA soumis
     * @param etat          L'état que l'on souhaite mettre sur les frais
     * @return true si l'update s'est bien fait, false sinon.
     */
    Boolean updateFraisEnCours(Collaborator collaborator, DateTime pDateRAVoulue, Etat etat);

    /**
     * Permet de trouver les frais lié à un justificatif si il y a.
     *
     * @param justif le justificatif en question
     * @return La liste de frais trouvée.
     */
    List<Frais> findFraisByJustif(Justif justif);

    /**
     * Permet de récupérer le pdf de l'ASF
     *
     * @param pId the p id
     * @return the file as fby id
     */
    File getFileASFbyId(Long pId);

    /**
     * Retourne les ASF à l'état soumis
     *
     * @return list list
     */
    List<Frais> findFraisAvanceDeFraisSO();

    /**
     * Change l'état de l'avance sur frais  et envoi du mail correspondant à l'action (validation, refus, solde)
     *
     * @param pIdFrais        the p id frais
     * @param pEtat           the p etat
     * @param pCommentaire    the p commentaire
     * @param pDateValidation the p date validation
     * @return string string
     * @throws Exception the exception
     */
    String actionValidationASF(Long pIdFrais, String pEtat, String pCommentaire, Date pDateValidation) throws Exception;

}
