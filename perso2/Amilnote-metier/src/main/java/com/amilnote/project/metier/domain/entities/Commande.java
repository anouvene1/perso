/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Commande.
 */
@Entity
@Table(name = "ami_commande")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Commande implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";
    /**
     * The constant PROP_DATEDEBUT.
     */
    public static final String PROP_DATEDEBUT = "dateDebut";
    /**
     * The constant PROP_DATEFIN.
     */
    public static final String PROP_DATEFIN = "dateFin";
    /**
     * The constant PROP_NB_JOURS.
     */
    public static final String PROP_NB_JOURS = "nbJours";
    /**
     * The constant PROP_TJM.
     */
    public static final String PROP_TJM = "tjm";
    /**
     * The constant PROP_FACTURE.
     */
    public static final String PROP_FACTURE = "idFacture";
    /**
     * The constant PROP_JUSTIFICATIF.
     */
    public static final String PROP_JUSTIFICATIF = "justificatif";
    /**
     * The constant PROP_MOYENS_PAIEMENT.
     */
    public static final String PROP_MOYENS_PAIEMENT = "id_moyen_paiement";
    /**
     * The constant PROP_CONDITION_PAIEMENT.
     */
    public static final String PROP_CONDITION_PAIEMENT = "condition_paiement";
    /**
     * The constant PROP_BUDGET.
     */
    public static final String PROP_BUDGET = "budget";
    /**
     * The constant PROP_NUMERO.
     */
    public static final String PROP_NUMERO = "numero";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant PROP_RESP_FACT.
     */
    public static final String PROP_RESP_FACT = "idResponsableFacturation";
    private static final long serialVersionUID = -329072377657745936L;
    private Long id;
    private Mission mission;
    private Date dateDebut;
    private Date dateFin;
    private Float nbJours;
    private Float montant;
    private String commentaire;
    private String numero;
    private String justificatif;
    private MoyensPaiement paiement;
    private String condition_paiement;
    private Float budget;
    private Etat etat;
    private Client client;
    private Long idResponsableFacturation;

	/* CONSTRUCTORS */

    /**
     * Instantiates a new Commande.
     */
    public Commande() {
        super();
    }

    /**
     * Instantiates a new Commande.
     *
     * @param pMission      the p mission
     * @param pDateDebut    the p date debut
     * @param pDateFin      the p date fin
     * @param pNbJours      the p nb jours
     * @param pMontant      the p montant
     * @param pCommentaire  the p commentaire
     * @param pNumero       the p numero
     * @param pJustificatif the p justificatif
     * @param pPaiement     the p paiement
     * @param pCondition    the p condition
     * @param pBudget       the p budget
     * @param pEtat         the p etat
     * @param pIdResponsableFacturation id
     */
    public Commande(Mission pMission, Date pDateDebut, Date pDateFin, Float pNbJours, Float pMontant, String pCommentaire, String pNumero, String pJustificatif, MoyensPaiement pPaiement, String pCondition, Float pBudget, Etat pEtat, Long pIdResponsableFacturation) {
        super();
        mission = pMission;
        dateDebut = pDateDebut;
        dateFin = pDateFin;
        nbJours = pNbJours;
        montant = pMontant;
        commentaire = pCommentaire;
        numero = pNumero;
        justificatif = pJustificatif;
        paiement = pPaiement;
        condition_paiement = pCondition;
        budget = pBudget;
        etat = pEtat;
        idResponsableFacturation = pIdResponsableFacturation;
    }

    /**
     * To json commande as json.
     *
     * @return the commande as json
     */
    public CommandeAsJson toJson() {
        return new CommandeAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_mission")
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(Mission mission) {
        this.mission = mission;
    }

    /**
     * Gets date debut.
     *
     * @return the date debut
     */
    @Column(name = "date_debut", nullable = true)
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param dateDebut the date debut
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return the date fin
     */
    @Column(name = "date_fin", nullable = true)
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param dateFin the date fin
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Gets nb jours.
     *
     * @return the nb jours
     */
    @Column(name = "nb_jours", nullable = true)
    public Float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param nbJours the nb jours
     */
    public void setNbJours(Float nbJours) {
        this.nbJours = nbJours;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "montant", nullable = true)
    public Float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param montant the montant
     */
    public void setMontant(Float montant) {
        this.montant = montant;
    }

    /**
     * Gets budget.
     *
     * @return the budget
     */
    @Column(name = "budget", nullable = true)
    public Float getBudget() {
        return budget;
    }

    /**
     * Sets budget.
     *
     * @param budget the budget
     */
    public void setBudget(Float budget) {
        this.budget = budget;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    @Column(name = "commentaire", nullable = true)
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    @Column(name = "numero", nullable = false)
    public String getNumero() {
        return numero;
    }

    /**
     * Sets numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Gets justificatif.
     *
     * @return the justificatif
     */
    @Column(name = "justificatif")
    public String getJustificatif() {
        return justificatif;
    }

    /**
     * Sets justificatif.
     *
     * @param justificatif the justificatif
     */
    public void setJustificatif(String justificatif) {
        this.justificatif = justificatif;
    }

    /**
     * Gets moyens paiement.
     *
     * @return the moyens paiement
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_moyen_paiement")
    public MoyensPaiement getMoyensPaiement() {
        return paiement;
    }

    /**
     * Sets moyens paiement.
     *
     * @param paiement the paiement
     */
    public void setMoyensPaiement(MoyensPaiement paiement) {
        this.paiement = paiement;
    }

    /**
     * Gets condition paiement.
     *
     * @return the condition paiement
     */
    @Column(name = "condition_paiement")
    public String getConditionPaiement() {
        return condition_paiement;
    }

    /**
     * Sets condition paiement.
     *
     * @param condition the condition
     */
    public void setConditionPaiement(String condition) {
        this.condition_paiement = condition;
    }


    /**
     * Gets etat.
     *
     * @return the etat
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "etat")
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_client")
    public Client getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Gets idResponsableFacturation
     * @return le Responsable Facturation
     */
    public Long getIdResponsableFacturation() {
        return idResponsableFacturation;
    }

    /**
     * Sets idResponsableFacturation
     * @param idResponsableFacturation id
     */
    public void setIdResponsableFacturation(Long idResponsableFacturation) {
        this.idResponsableFacturation = idResponsableFacturation;
    }
}
