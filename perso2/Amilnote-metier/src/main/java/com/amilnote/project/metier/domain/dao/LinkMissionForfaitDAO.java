/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.LinkMissionForfait;
import com.amilnote.project.metier.domain.entities.Mission;

import java.util.List;

/**
 * The interface Link mission forfait dao.
 */
public interface LinkMissionForfaitDAO extends LongKeyDAO<LinkMissionForfait> {

    /**
     * Retourne la liste des LinkMissionForfait pour la misison passée en paramètre
     *
     * @param pMission the p mission
     * @return list list
     */
    List<LinkMissionForfait> findByMission(Mission pMission);

    List<LinkMissionForfait> findLib();

    List<LinkMissionForfait> findNotLib();

    List<LinkMissionForfait> findNotLibByMission(Mission pMission);

    List<LinkMissionForfait> findLibByMission(Mission pMission);

}
