/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.RapportActivites;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * The interface Rapport activites service.
 */
public interface RapportActivitesService {

    /**
     * Sauvegarde du rapport d'activités généré suite à la tache de création du
     * pdf rapport d'activites
     *
     * @param pDateSoumission     the p date soumission
     * @param pPdfRapportActivite the p pdf rapport activite
     * @param collaborator      the p collaborateur
     * @param pDateRAVoulue       the p date ra voulue
     * @param pAvecFrais          the p avec frais
     * @return String string
     */
    String saveNewRACollaborator(Date pDateSoumission, String pPdfRapportActivite, Collaborator collaborator, DateTime pDateRAVoulue, boolean pAvecFrais);

    /**
     * Date du dernier rapport d'activités soumis
     *
     * @param collaborator the p collaborateur
     * @return Date date dernier ra soumis
     */
    Date getDateDernierRASoumis(Collaborator collaborator);

    /**
     * Retourne le nombre de RA Soumis/Valide existant pour le collaborateur dans le mois
     * donné
     *
     * @param collaborator Collaborateur concerné
     * @param pDateTime      Mois voulu
     * @return Nombre de RA existant
     */
    List<RapportActivites> getExistingRAForMonth(Collaborator collaborator, DateTime pDateTime);

    /**
     * Retourne le nombre de RA Brouillon existant pour le collaborateur dans le mois
     * donné
     *
     * @param collaborator Collaborateur concerné
     * @param pDateTime      Mois voulu
     * @return Nombre de RA existant
     */
    List<RapportActivites> getBrouillonRAForMonth(Collaborator collaborator, DateTime pDateTime);

    /**
     * Retourne la liste complète des RA filtrés selon l'état
     * S'il n'y a pas de filtre, retourne tous les RA
     *
     * @param pFiltre Filtre sur l'etat des RA
     * @return La liste des RA filtrée
     */
    List<RapportActivites> getAllRAByFiltre(String pFiltre);

    /**
     * Retourne la liste compléte des RA à l'état SOUMIS
     *
     * @return L 'ensemble des RA Soumis
     */
    List<RapportActivites> getAllRAEtatSO();

    /**
     * Retourne le RA lié à l'id voulu
     *
     * @param pId ID voulu
     * @return the r aby id
     */
    RapportActivites getRAbyId(Long pId);

    /**
     * Change l'état du RA en l'état souhaité et ajoute un commentaire
     *
     * @param idRA         Identifiant du RA à modifier
     * @param pEtatValide  Nouvel Etat
     * @param pCommentaire the p commentaire
     * @throws Exception the exception
     */
    void changeEtatRA(Long idRA, String pEtatValide, String pCommentaire) throws Exception;

    /**
     * Change l'état du RA en l'état souhaité
     *
     * @param idRA        Identifiant du RA à modifier
     * @param pEtatValide Nouvel Etat
     * @throws Exception the exception
     */
    void changeEtatRA(Long idRA, String pEtatValide) throws Exception;

    /**
     * Retourne le fichier PDF RA lié à l'ID
     *
     * @param pIdRA ID du RA souhaité
     * @return Fichier PDF du RA souhaité
     * @throws Exception the exception
     */
    File getFileRAbyId(Long pIdRA) throws Exception;

    /**
     * Permet de savoir si le RA existe dans le serveur
     * @param pIdRA est l'id du RA
     * @return un boolean
     */
    boolean raFileExists(Long pIdRA);

    /**
     * Retourne l'ensemble des RA existants
     *
     * @return Liste de tous les RA
     */
    List<RapportActivites> getAllRA();

    /**
     * Retourne l'ensemble des RA existants pour le collaborateur
     *
     * @param collaborator Collaborateur souhaité
     * @return Liste des RA du collaborateur
     */
    List<RapportActivites> getAllRAForCollaborator(Collaborator collaborator);


    List<RapportActivites> getAllRAForCollaboratorBetweenDate(Collaborator collaborator, Date dateDebut);

    /**
     * Retourne le nombre de jour travaillés par un collaborateur sur une mission durant un mois pris en argument
     *
     * @param collaborator le collaborateur
     * @param pMission la misison du collaborateur
     * @param dateVoulue le mois pour lequel on veut le nombre de jour passés sur la mission
     * @return le nombre de jours travaillés
     * @author clome
     */
    Float getNbJoursTravailles(Collaborator collaborator, Mission pMission, DateTime dateVoulue);

    /**
     * Persiste le nouveau PDF sur le rapport d'activité donné.
     * @param rapportActivites {@link RapportActivites}
     * @param pdf pdf
     */
    void changePdf(RapportActivites rapportActivites, String pdf);


    List<RapportActivites> getRACollaboratorMissionClienteForMonth(DateTime pDateTime) throws JsonProcessingException;

    boolean isRAWithMissionCliente(RapportActivites pRapportActivites) throws JsonProcessingException;
}
