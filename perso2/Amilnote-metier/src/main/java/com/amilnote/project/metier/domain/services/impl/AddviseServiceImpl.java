/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.AddviseFormateurDAO;
import com.amilnote.project.metier.domain.dao.AddviseLinkSessionCollaborateurDAO;
import com.amilnote.project.metier.domain.dao.AddviseSessionDAO;
import com.amilnote.project.metier.domain.dao.ModuleAddviseDAO;
import com.amilnote.project.metier.domain.dao.impl.AddviseSessionDAOImpl;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.pdf.PDFBuilderAddvise;
import com.amilnote.project.metier.domain.services.AddviseService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.DocumentException;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service("addviseService")
public class AddviseServiceImpl extends AbstractServiceImpl<AddviseSession, AddviseSessionDAOImpl> implements AddviseService {

    private static final Logger logger = LogManager.getLogger(AddviseServiceImpl.class);
    private static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private ModuleAddviseDAO moduleAddviseDAO;

    @Autowired
    private AddviseSessionDAO addviseSessionDAO;

    @Autowired
    private AddviseFormateurDAO addviseFormateurDAO;

    @Autowired
    private AddviseLinkSessionCollaborateurDAO addviseLinkSessionCollaboratorDAO;

    @Autowired
    private PDFBuilderAddvise pdfBuilderAddvise;

    @Autowired
    private MailService mailService;

    @Override
    public AddviseSessionDAOImpl getDAO() {
        return (AddviseSessionDAOImpl) addviseSessionDAO;
    }

    /**
     * Retourne tous les collaborateurs concernés par le programme Addvise
     *
     * @return List de Collaborator
     */
    @Override
    public List<Collaborator> findAddviseCollaborators() {
        List<Collaborator> collaborateurs = collaboratorService.findAllOrderByNomAscWithoutADMIN();
        collaborateurs.sort(Comparator.comparing(Collaborator::getDateEntree));
        prepareAddviseCollaborators(collaborateurs);
        return collaborateurs;
    }

    /**
     * Retourne la totalité des modules Addvise (M1, M2...) indépendamment des sessions elles-mêmes
     *
     * @return List de ModuleAddvise
     */
    @Override
    public List<ModuleAddvise> findAddviseAllModules() {
        return moduleAddviseDAO.findAll(Order.asc("id"));
    }

    /**
     * Retourne la totalité des formateurs Addvise
     *
     * @return List de AddviseFormateur
     */
    @Override
    public List<AddviseFormateur> findAddviseAllFormateurs() {
        return addviseFormateurDAO.findAll(Order.asc("nom"));
    }

    /**
     * Retourne toutes les sessions Addvise pour la page de saisie
     *
     * @return liste de AddviseSession
     */
    @Override
    public List<AddviseSession> findAddviseSessionsModifiables() {
        List<AddviseSession.EtatSession> etatsSession = Arrays.asList(AddviseSession.EtatSession.EN_COURS_DE_SAISIE, AddviseSession.EtatSession.CONFIRMEE);
        return addviseSessionDAO.findAllByEtatsAndDateOrderByDate(etatsSession, null, null);
    }

    /**
     * Permet l'ajout asynchrone d'un nouveau module (type M1, M2...)
     *
     * @param codeModule le code du nouveau module
     * @param nomModule  le nom du nouveau module
     * @return l'id du nouveau module créé
     */
    @Override
    public long addNewModule(String codeModule, String nomModule) throws InvalidPropertiesFormatException {
        ModuleAddvise existingModule = moduleAddviseDAO.findUniqEntiteByProp("codeModuleAddvise", codeModule);
        if (codeModule.isEmpty() || nomModule.isEmpty() || existingModule != null) {
            throw new InvalidPropertiesFormatException("Syntaxe invalide des arguments pour créer un module.");
        }

        ModuleAddvise newModuleAddvise = new ModuleAddvise();
        newModuleAddvise.setCodeModuleAddvise(codeModule);
        newModuleAddvise.setNomModuleAddvise(nomModule);

        moduleAddviseDAO.createOrUpdateModuleAddvise(newModuleAddvise);
        return newModuleAddvise.getId();
    }

    /**
     * Permet l'ajout asynchrone d'un nouveau formateur
     *
     * @param nomFormateur    le nom du nouveau formateur
     * @param prenomFormateur le prenom du nouveau formateur
     * @param emailFormateur  l'email du nouveau formateur
     * @return l'id du nouveau formateur créé
     */
    @Override
    public long addNewFormateur(String nomFormateur, String prenomFormateur, String emailFormateur)
            throws InvalidPropertiesFormatException {

        AddviseFormateur existingFormateur = addviseFormateurDAO.findUniqEntiteByProp("email", emailFormateur);
        String emailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (nomFormateur.isEmpty() || prenomFormateur.isEmpty() || !emailFormateur.matches(emailRegex)
                || existingFormateur != null) {
            throw new InvalidPropertiesFormatException("Syntaxe invalide des arguments pour créer un formateur.");
        }

        AddviseFormateur newFormateur = new AddviseFormateur();
        newFormateur.setNom(nomFormateur);
        newFormateur.setPrenom(prenomFormateur);
        newFormateur.setEmail(emailFormateur);

        addviseFormateurDAO.createOrUpdateAddviseFormateur(newFormateur);
        return newFormateur.getId();
    }

    /**
     * Permet l'ajout d'une nouvelle session Addvise
     *
     * @param idModule    long
     * @param dateStr     Date
     * @param idFormateur int
     * @throws ParseException the parse exception
     */
    @Override
    public void addNewSession(long idModule, String dateStr, int idFormateur) throws ParseException {
        //Convertit les données comme il faut selon les types souhaités
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        Date date = formatter.parse(dateStr);

        //Instancie les attributs de l'objet à créer
        ModuleAddvise moduleAddvise = moduleAddviseDAO.findById(idModule);
        AddviseFormateur addviseFormateur = addviseFormateurDAO.findById(idFormateur);

        //Crée l'entité à sauvegarder
        AddviseSession addviseSession = new AddviseSession();
        addviseSession.setModuleAddvise(moduleAddvise);
        addviseSession.setFormateur(addviseFormateur);
        addviseSession.setDate(date);
        addviseSession.setEtat(AddviseSession.EtatSession.EN_COURS_DE_SAISIE);

        //Sauvegarde de l'entité souhaité
        addviseSessionDAO.createOrUpdateAddviseSession(addviseSession);
    }

    /**
     * Permet de trouver une invitation par rapport au collaborateur et à la session souhaités
     *
     * @param idsCollabSession ids par paire (id collaborateur, id session)
     * @return AddviseLinkSessionCollaborateur
     */
    @Override
    public List<Long[]> findInvitationsEtatsByCollabSession(String idsCollabSession) {
        String[] tabIdsCollabSession = idsCollabSession.split(",");
        List<AddviseLinkSessionCollaborateur> invitations =
                addviseLinkSessionCollaboratorDAO.findAllByIdCollaboratorAndIdSession(tabIdsCollabSession);

        List<Long[]> etatInvitations = new ArrayList<>();
        for (AddviseLinkSessionCollaborateur invit : invitations) {
            etatInvitations.add(new Long[]{
                    invit.getCollaborateur().getId(),
                    (long) invit.getSession().getId(),
                    (long) invit.getEtatInvitation().ordinal()
            });
        }

        return etatInvitations;
    }


    /**
     * Permet de trier, filtrer, rechercher ou cacher les collaborateurs suivant les paramètres
     *
     * @param collaborateursAddvise Liste des collaborateurs à modifier
     * @param trier                 [alpha, date-entree, date-dernier-module]
     * @param cacher                [true, false]
     * @param chercher              un string pour le "nom prénom" du collaborateur
     * @param filtrer               "Tous" ou nom d'un module (M1, M2...)
     * @return Liste des collaborateurs arrangée
     */
    @Override
    public List<Collaborator> sortAndFilterCollaborators(
            List<Collaborator> collaborateursAddvise, String trier, String cacher, String chercher, String filtrer) {
        //Tris
        if (trier != null) {
            switch (trier) {
                case "alpha":
                    collaborateursAddvise.sort((c1, c2) -> c1.getNom().compareToIgnoreCase(c2.getNom()));
                    break;
                case "date-entree":
                    collaborateursAddvise.sort(Comparator.comparing(Collaborator::getDateEntree));
                    break;
                case "date-dernier-module": //Si le dernier module n'existe pas, on trie par la date d'entrée
                    collaborateursAddvise.sort(Comparator.comparing(c ->
                            c.getLastAddviseSession() != null ? c.getLastAddviseSession().getDate() : c.getDateEntree()));
                    break;
                default:
                    break;
            }
        }

        //Cacher les collaborateurs ayant eu une session depuis moins de 4 mois
        if (Boolean.parseBoolean(cacher)) {
            Date today = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.MONTH, -4);
            //On supprime pour les sessions de moins de 4 mois ou, si aucune session, les dates d'entrée récentes
            collaborateursAddvise.removeIf(c ->
                    (c.getLastAddviseSession() == null && c.getDateEntree().after(cal.getTime())) ||
                            (c.getLastAddviseSession() != null && c.getLastAddviseSession().getDate().after(cal.getTime())));
        }

        //Chercher uniquement les collaborateurs dont le "nom prénom" est dans la variable chercher
        if (chercher != null && !chercher.isEmpty()) {
            collaborateursAddvise.removeIf(c ->
                    !(c.getNom() + " " + c.getPrenom()).toUpperCase().contains(chercher.toUpperCase()));
        }

        //filtrer seulement avec le module souhaité si variable non vide
        if (filtrer != null && !filtrer.isEmpty()) {
            if (Integer.parseInt(filtrer) == 0) {
                collaborateursAddvise.removeIf(c -> c.getLastAddviseSession() != null);
            } else {
                collaborateursAddvise.removeIf(c -> c.getLastAddviseSession() == null ||
                        c.getLastAddviseSession().getModuleAddvise().getId() != Long.parseLong(filtrer));
            }
        }

        return collaborateursAddvise;
    }

    /**
     * Permet de récupérer les sessions déjà validées ou supprimées pour l'historique
     *
     * @param annulees     Booléen si on veut aussi les sessions supprimées
     * @param dateDebutStr the date debut as String
     * @param dateFinStr   the date fin as String
     * @return list
     */
    @Override
    public List<AddviseSession> findSessionsPassees(boolean annulees, String dateDebutStr, String dateFinStr)
            throws ParseException {

        List<AddviseSession.EtatSession> etatsSession = new ArrayList<>();
        etatsSession.add(AddviseSession.EtatSession.VALIDEE);
        if (annulees) {
            etatsSession.add(AddviseSession.EtatSession.SUPPRIMEE);
        }

        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date dateDebut = null;
        Date dateFin = null;

        if (dateDebutStr == null) {
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -3);
        } else {
            cal.setTime(df.parse(dateDebutStr));
        }
        dateDebut = cal.getTime();

        if (dateFinStr == null) {
            cal.setTime(new Date());
        } else {
            cal.setTime(df.parse(dateFinStr));
        }
        cal.add(Calendar.DAY_OF_MONTH, 1);
        dateFin = cal.getTime();

        return addviseSessionDAO.findAllByEtatsAndDateOrderByDate(etatsSession, dateDebut, dateFin);
    }

    /**
     * Permet de restaurer une session dans son état précédent éditable
     *
     * @param idSession l'id de session
     */
    @Override
    public void restoreSession(int idSession) {
        AddviseSession session = addviseSessionDAO.findById(idSession);
        if (session.getEtat() == AddviseSession.EtatSession.VALIDEE) {
            session.setEtat(AddviseSession.EtatSession.CONFIRMEE);
        } else if (session.getEtat() == AddviseSession.EtatSession.SUPPRIMEE) {
            session.setEtat(AddviseSession.EtatSession.EN_COURS_DE_SAISIE);
        }

        addviseSessionDAO.createOrUpdateAddviseSession(session);
    }

    /**
     * Permet de créer le PDF d'émargement du programme Addvise étant donné un id de session
     *
     * @param idSession id de la session concernée par la feuille d'émargement
     * @return le nom du fichier créé
     */
    @Override
    public byte[] printSessionToSign(int idSession) throws DocumentException, NamingException, IOException {
        AddviseSession addviseSession = addviseSessionDAO.findById(idSession);

        return pdfBuilderAddvise.createPDFAddvise(addviseSession);
    }

    @Override
    public ModuleAddvise getPreviousModule(long idModule) {
        ModuleAddvise mod;
        do {
            idModule--;
            mod = moduleAddviseDAO.findById(idModule);
        } while (mod == null && idModule >= 1);

        return mod;
    }

    @Override
    public AddviseLinkSessionCollaborateur manageInvitationByToken(String token, String url,
                                                                   String action) throws MessagingException, NamingException, EmailException, IOException {
        AddviseLinkSessionCollaborateur invitation = addviseLinkSessionCollaboratorDAO.findUniqEntiteByProp("token", token);


        if (invitation != null) {
            Collaborator currentUser = invitation.getCollaborateur();
            boolean sessionPassee = confirmerOuDeclinerInvitationParMail(url, action, invitation, currentUser);
            if (sessionPassee) {
                invitation = null;
            }
        }
        return invitation;
    }

    /**
     * Retourne les stats utiles à la modale de création de session Addvise
     *
     * @return tableau double de String
     */
    @Override
    public String[][] getStatsTable(List<Collaborator> collaborateursAddvise, String dateDebut,
                                    String dateFin) throws ParseException {
        List<AddviseSession> sessionsRealisees;
        // On récupère toutes les sessions réalisées
        sessionsRealisees = addviseSessionDAO.findListEntitesByProp(
                "etat", AddviseSession.EtatSession.VALIDEE, "date");
        // On récupère les modules issus de sessions déjà réalisées en gardant les doublons
        List<ModuleAddvise> modulesDejaRealises = sessionsRealisees.stream().map(AddviseSession::getModuleAddvise)
                .sorted(Comparator.comparing(ModuleAddvise::getId)).collect(Collectors.toList());

        //On initialise le tableau des stats
        int taille = modulesDejaRealises.stream().distinct().collect(Collectors.toList()).size();
        String[][] tableStats = new String[taille][4];
        for (int i = 0; i < taille; ++i) {
            //On récupère chaque code de module distinct
            String codeModule = modulesDejaRealises.stream().distinct().collect(Collectors.toList())
                    .get(i).getCodeModuleAddvise();

            //On regarde pour ce module combien il y a eu de sessions
            long nombreSessionsPourModule = modulesDejaRealises.stream()
                    .filter(module -> Objects.equals(module.getCodeModuleAddvise(), codeModule)).count();
            //On ajoute les nombres connus de l'historique non stockés en base
            if (i < 6) {
                for (Pair<Integer, int[]> nombreSessionParAnnee : AddviseSession.getNombreSessionsPassees()) {
                    nombreSessionsPourModule += nombreSessionParAnnee.getSecond()[i];
                }
            }

            // On prend la date de la dernière session pour le module courant
            Optional lastSessionByModuleOpt = sessionsRealisees.stream().filter(
                    session -> Objects.equals(session.getModuleAddvise().getCodeModuleAddvise(), codeModule))
                    .max(Comparator.comparingLong(session2 -> session2.getDate().getTime())); //dernière session d'un module
            String dateLastSessionByModule = "";
            if (lastSessionByModuleOpt.isPresent()) { //On prend la date de la dernière session d'un module si existe
                AddviseSession lastSessionByModule = (AddviseSession) lastSessionByModuleOpt.get();
                Format formatter = new SimpleDateFormat("dd/MM/yyyy");
                dateLastSessionByModule = formatter.format(lastSessionByModule.getDate());
            }

            // On trouve le nombre de collaborateurs devant effectuer théoriquement le module courant la prochaine fois
            int numeroModule = Integer.parseInt(codeModule.replaceAll("[^0-9]", ""));
            int nombreCollaborators = 0;
            for (Collaborator collab : collaborateursAddvise) {
                AddviseSession lastSession = collab.getLastAddviseSession();
                if (lastSession == null && numeroModule == 1) {
                    nombreCollaborators++;
                } else if (lastSession != null) {
                    ModuleAddvise dernierModule = lastSession.getModuleAddvise();
                    int numDernierModule = Integer.parseInt(dernierModule.getCodeModuleAddvise().replaceAll("[^0-9]", ""));
                    if (numDernierModule + 1 == numeroModule) {
                        nombreCollaborators++;
                    }
                }
            }

            //On remplit la ligne du tableau
            tableStats[i][0] = codeModule;
            tableStats[i][1] = String.valueOf(nombreSessionsPourModule);
            tableStats[i][2] = dateLastSessionByModule;
            tableStats[i][3] = String.valueOf(nombreCollaborators);
        }
        return tableStats;
    }

    @Override
    public List<Pair<String, Long>> getStatsHist(List<AddviseSession> sessionsPassees) {
        List<ModuleAddvise> modulesPasses = sessionsPassees.stream().map(AddviseSession::getModuleAddvise)
                .sorted(Comparator.comparing(ModuleAddvise::getId)).collect(Collectors.toList());

        List<Pair<String, Long>> result = new ArrayList<>();
        List<ModuleAddvise> modulesDistincts = modulesPasses.stream().distinct().collect(Collectors.toList());
        for (ModuleAddvise moduleAddvise : modulesDistincts) {
            Long count = modulesPasses.stream().filter(m -> m.getId().equals(moduleAddvise.getId())).count();
            result.add(new Pair<>(moduleAddvise.getCodeModuleAddvise(), count));
        }

        return result;
    }

    /**
     * A partir d'une liste exhaustive de collaborateurs, on supprime les non concernés par Addvise
     * et on trouve les dernières sessions réalisées pour chaque collaborateur concerné
     *
     * @param collaborateurs liste de tous les collaborateurs récupérés et à filtrer
     */
    private void prepareAddviseCollaborators(List<Collaborator> collaborateurs) {
        List<Collaborator> collaborateursToRemove = new ArrayList<>();
        for (Collaborator collab : collaborateurs) {
            if (!collab.isEnabled() || StatutCollaborateur.STATUT_DIRECTION.equals(collab.getStatut().getCode()) || StatutCollaborateur.STATUT_SOUS_TRAITANT.equals(collab.getStatut().getCode())) {
                collaborateursToRemove.add(collab);
            } else {
                applyLastAddviseSession(collab); //L'entité collaborateur connaît sa dernière session
                applyFutureModule(collab);
            }
        }
        collaborateurs.removeAll(collaborateursToRemove);
    }

    /**
     * A partir d'un collaborateur on trouve sa dernière session réalisée et l'entité en a la connaissance
     *
     * @param collaborateur Collaborator dont on veut trouver la dernière session réalisée
     */
    private void applyLastAddviseSession(Collaborator collaborateur) {
        Optional<AddviseSession> optional = collaborateur.getAddviseSessions().stream()
                .filter(session -> Objects.equals(session.getEtat(), AddviseSession.EtatSession.VALIDEE))
                .filter(session ->
                        session.getListeLinkSessionCollaborateur().stream().filter(
                                invit -> invit.getCollaborateur() == collaborateur).findFirst().get().getEtatInvitation()
                                .equals(AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE)
                )
                .max(Comparator.comparingLong(session2 -> session2.getDate().getTime()));
        optional.ifPresent(collaborateur::setLastAddviseSession);
    }

    /**
     * A partir d'un collaborateur on trouve le module qu'il doit faire ensuite
     *
     * @param collab Collaborator
     */
    private void applyFutureModule(Collaborator collab) {
        AddviseSession lastAdviseSession = collab.getLastAddviseSession();
        List<ModuleAddvise> modulesAddvise = moduleAddviseDAO.findAll(Order.asc("id"));

        if (lastAdviseSession == null) {
            collab.setFutureModule(modulesAddvise.get(0));
        } else {
            ModuleAddvise lastModuleAddvise = lastAdviseSession.getModuleAddvise();
            String codeLastModule = lastModuleAddvise.getCodeModuleAddvise();
            int numeroFutureModule = Integer.parseInt(codeLastModule.replaceAll("[^0-9]", "")) + 1;
            Optional<ModuleAddvise> futureModuleAddviseOpt = modulesAddvise.stream().filter(moduleAddvise -> Objects.equals(
                    numeroFutureModule, Integer.parseInt(moduleAddvise.getCodeModuleAddvise().replaceAll("[^0-9]", "")))
            ).findFirst();
            futureModuleAddviseOpt.ifPresent(collab::setFutureModule);
        }
    }

    /**
     * Permet de sauvegarder toutes les invitations pour chaque session et collaborateurs souhaités
     *
     * @param allInvitationsStr String composé des triplets "idSession, idCollab, etatInvit... idSession, idCollab..."
     * @param currentUser       the current user
     */
    @Override
    public void saveAllInvitations(String allInvitationsStr, String url, Collaborator currentUser) {
        String[] allInvitations = allInvitationsStr.split(",");
        List<AddviseLinkSessionCollaborateur> invitations = new ArrayList<>();

        for (int i = 0; i < allInvitations.length; i += 3) {
            int idSession = Integer.parseInt(allInvitations[i]);
            long idCollab = Long.parseLong(allInvitations[i + 1]);

            AddviseLinkSessionCollaborateur invit =
                    addviseLinkSessionCollaboratorDAO.findByIdCollaboratorAndIdSession(idCollab, idSession);
            if (invit == null) {
                invit = new AddviseLinkSessionCollaborateur();
                invit.setSession(addviseSessionDAO.findById(idSession));
                invit.setCollaborateur(collaboratorService.findById(idCollab));
            }

            AddviseLinkSessionCollaborateur.EtatInvitation etatInvitation =
                    AddviseLinkSessionCollaborateur.EtatInvitation.values()[Integer.parseInt(allInvitations[i + 2])];
            invit.setEtatInvitation(etatInvitation);

            sendMailAddviseOnSauvegarderToCollaborator(invit, url, currentUser);
            invitations.add(invit);

            addviseLinkSessionCollaboratorDAO.createOrUpdateAddviseLinkSessionCollaborateur(invit);

        }
        sendMailAddviseOnSauvegarderToRh(invitations, currentUser);
    }

    @Override
    public boolean confirmerOuDeclinerInvitationParMail(String url, String action,
                                                        AddviseLinkSessionCollaborateur invitation,
                                                        Collaborator currentUser) throws MessagingException, IOException, EmailException, NamingException {
        List<AddviseLinkSessionCollaborateur> invitations = new ArrayList<>();
        boolean sessionPassee = true;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        if (invitation.getSession().getDate().after(cal.getTime())) {
            switch (action) {
                case "confirmer":
                    invitation.setEtatInvitation(AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE);
                    break;
                case "decliner":
                    invitation.setEtatInvitation(AddviseLinkSessionCollaborateur.EtatInvitation.DECLINEE);
                    break;
                default:
                    break;
            }
            sendMailAddviseOnSauvegarderToCollaborator(invitation, url, currentUser);
            invitations.add(invitation);
            sendMailAddviseOnSauvegarderToRh(invitations, currentUser);
            sessionPassee = false;
        }

        invitation.setToken(null);
        addviseLinkSessionCollaboratorDAO.createOrUpdateAddviseLinkSessionCollaborateur(invitation);

        return sessionPassee;
    }

    /**
     * Confirme la session si elle ne l'était pas (toujours éditable), sinon on la valide (historique)
     *
     * @param idSession         id de la session Addvise
     * @param allInvitationsStr ensemble des invitations à modifier
     * @param currentUser       the current user
     * @return la session Addvise créée
     */
    @Override
    public AddviseSession validationSession(int idSession, String allInvitationsStr, String url,
                                            Collaborator currentUser) {
        AddviseSession addviseSession = addviseSessionDAO.findById(idSession);

        if (allInvitationsStr != null && !allInvitationsStr.isEmpty()) {
            saveAllInvitations(allInvitationsStr, url, currentUser);
        }

        if (addviseSession.getEtat() == AddviseSession.EtatSession.CONFIRMEE) {
            // Session passée, on la met dans l'historique
            addviseSession.setEtat(AddviseSession.EtatSession.VALIDEE);
            setTokenToNullBySession(addviseSession);
        } else {
            // Toujours éditable, alertes email de confirmation et formateur
            addviseSession.setEtat(AddviseSession.EtatSession.CONFIRMEE);
            List<Collaborator> destinataires = getDestinataires(addviseSession);
            sendMailAddviseOnActionSessionToCollaborators(destinataires, addviseSession, currentUser);
            sendMailAddviseOnActionSessionToRH(addviseSession, false);
        }


        addviseSessionDAO.createOrUpdateAddviseSession(addviseSession);
        return addviseSession;
    }

    /**
     * Modifie la session
     *
     * @param idSession         id de la session Addvise
     * @param idModule          id du module
     * @param idFormateur       id du formateur
     * @param allInvitationsStr ensemble des invitations à modifier
     * @param url               l'url
     * @param currentUser       l'utilisateur de la session
     * @return la session Addvise modifiée
     */

    @Override
    public AddviseSession editSession(int idSession, long idModule, String dateStr, int idFormateur,
                                      String allInvitationsStr, String url,
                                      Collaborator currentUser) throws ParseException {

        Map<String, Boolean> modifications = new HashMap<>();
        //recupérer la session, le module, le formateur
        AddviseSession addviseSession = addviseSessionDAO.findById(idSession);
        ModuleAddvise moduleAddvise = moduleAddviseDAO.findById(idModule);
        AddviseFormateur formateur = addviseFormateurDAO.findById(idFormateur);
        //formater la date
        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
        Date date = formater.parse(dateStr);
        String datePrecedenteStr = formater.format(addviseSession.getDate());
        //Verifier le type de modification
        if (addviseSession.getFormateur() != formateur) {
            modifications.put("formateur", true);
        } else {
            modifications.put("formateur", false);
        }
        if (addviseSession.getDate() != date) {
            modifications.put("date", true);
        } else {
            modifications.put("date", false);
        }
        if (!addviseSession.getModuleAddvise().getId().equals(moduleAddvise.getId())) {
            modifications.put("module", true);
        } else {
            modifications.put("module", false);
        }

        //Recuperer les personnes concerneés par la modification
        List<Collaborator> destinataires = addviseSession.getCollaborateurs();
        //Passer les invit à 'declinée' si le module change + set etatSesion 'Validée'
        if (modifications.get("module")) {
            addviseSession.setEtat(AddviseSession.EtatSession.EN_COURS_DE_SAISIE);

        }
        //set etatSesion 'Validée' si la date change
        if (modifications.get("date")) {
            addviseSession.setEtat(AddviseSession.EtatSession.EN_COURS_DE_SAISIE);
        }
        //Set la session
        addviseSession.setDate(date);
        addviseSession.setFormateur(formateur);
        addviseSession.setModuleAddvise(moduleAddvise);

        //Update la session
        addviseSessionDAO.createOrUpdateAddviseSession(addviseSession);


        //Evoyer des mails
        sendMailAddviseOnEditToCollaborators(destinataires, addviseSession, currentUser, modifications, url, datePrecedenteStr);
        //sendMailAddviseOnEditToSessionToRH(addviseSession, isDejaConfirmee);

        //Ajouter les nouvelles invitations
        if (allInvitationsStr != null && !allInvitationsStr.isEmpty()) {
            saveAllInvitations(allInvitationsStr, url, currentUser);
        }
        return addviseSession;
    }

    /**
     * Envoi un mail au collaborateur à l'action du bouton Modifier Modules Addvise
     */
    private void sendMailAddviseOnEditToCollaborators(List<Collaborator> collaborateurs,
                                                       AddviseSession addviseSession,
                                                       Collaborator currentUser, Map<String, Boolean> modifications,
                                                       String urlIntern, String datePrecedenteStr) {
        List<Collaborator> destinataires = new ArrayList<>();
        String nomModule = addviseSession.getModuleAddvise().getNomModuleAddvise();
        String formateur = addviseSession.getFormateur().getPrenom() + " " + addviseSession.getFormateur().getNom();
        //formater la date modifiée
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String dateModifieeStr = simpleDateFormat.format(addviseSession.getDate());
        //Recuperer l'url
        int urlIndex = urlIntern.indexOf("Administration");
        if (urlIndex > -1) {
            urlIntern = urlIntern.substring(0, urlIndex);
        }

        //Envoi message adapté
        if (modifications.get("module")) {
            destinataires = getDestinataires(addviseSession);
            String[] params = {addviseSession.getModuleAddvise().getNomModuleAddvise(), dateModifieeStr,
                    currentUser.getNom() + " " + currentUser.getPrenom()};
            mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiSuppressionModuleAddvise.object",
                    "email.envoiSuppressionModuleAddvise.message", params);
            for (Collaborator collaborateur : collaborateurs) {
                AddviseLinkSessionCollaborateur invitation = addviseSession.getListeLinkSessionCollaborateur().stream().filter(
                        invit -> invit.getCollaborateur().equals(collaborateur)).findFirst().get();
                invitation.setEtatInvitation(AddviseLinkSessionCollaborateur.EtatInvitation.DECLINEE);
                addviseLinkSessionCollaboratorDAO.createOrUpdateAddviseLinkSessionCollaborateur(invitation);

            }
        } else {
            if (modifications.get("formateur") && modifications.get("date") == false) {
                destinataires = getDestinataires(addviseSession);
                String[] params = {nomModule, dateModifieeStr, formateur, currentUser.getNom() + " " + currentUser.getPrenom()};
                mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiModifificationAddvise.object",
                        "email.envoiModificationAddvise.message", params);
            }
            ;
            if (modifications.get("date")) {
                for (Collaborator destinataire : collaborateurs) {
                    AddviseLinkSessionCollaborateur invitation = addviseSession.getListeLinkSessionCollaborateur().stream().filter(
                            invit -> invit.getCollaborateur().equals(destinataire)).findFirst().get();
                    if (invitation.getEtatInvitation() == AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE
                            || invitation.getEtatInvitation() == AddviseLinkSessionCollaborateur.EtatInvitation.ENVOYEE) {
                        //Récupérer les infos
                        Calendar cal = Calendar.getInstance();
                        String prenomInvite = invitation.getCollaborateur().getPrenom();
                        Date today = new Date();

                        destinataires.add(destinataire);

                        SecureRandom random = new SecureRandom();
                        StringBuilder token = new StringBuilder();
                        token.append(new BigInteger(400, random).toString(32));
                        invitation.setToken(token.toString());

                        cal.setTime(today);
                        cal.add(Calendar.DAY_OF_MONTH, +7);
                        Date dateLimite = cal.getTime();
                        String dateLimiteStr = simpleDateFormat.format(dateLimite);
                        String signature = currentUser.getNom() + " " + currentUser.getPrenom();
                        //Envoi du mail
                        String[] params = {nomModule, datePrecedenteStr, signature, token.toString(), prenomInvite,
                                formateur, dateLimiteStr, urlIntern, dateModifieeStr, Constantes.APPLICATION_URL};
                        mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiReportAddvise.object",
                                "email.envoiReportAddvise.message", params);
                        //Changer l'état de l'invitation
                        invitation.setEtatInvitation(AddviseLinkSessionCollaborateur.EtatInvitation.ENVOYEE);
                        addviseLinkSessionCollaboratorDAO.createOrUpdateAddviseLinkSessionCollaborateur(invitation);
                    }

                }
            }
        }
    }

    /**
     * "Supprime" (reste stockée) la session en cours en cas d'annulation (disponible dans l'historique)
     *
     * @param idSession         id de la session Addvise
     * @param allInvitationsStr ensemble des invitations à modifier
     * @param currentUser       the current user
     */
    @Override
    public void removeSession(int idSession, String allInvitationsStr, String url, Collaborator currentUser) {
        boolean isDejaConfirme = false;
        AddviseSession addviseSession = addviseSessionDAO.findById(idSession);

        if (addviseSession.getEtat().equals(AddviseSession.EtatSession.CONFIRMEE)) {
            isDejaConfirme = true;
        }

        if (allInvitationsStr != null && !allInvitationsStr.isEmpty()) {
            saveAllInvitations(allInvitationsStr, url, currentUser);
        }

        addviseSession.setEtat(AddviseSession.EtatSession.SUPPRIMEE);
        setTokenToNullBySession(addviseSession);
        addviseSessionDAO.createOrUpdateAddviseSession(addviseSession);
        addviseSession = addviseSessionDAO.findById(idSession);

        List<Collaborator> destinataires = getDestinataires(addviseSession);
        sendMailAddviseOnActionSessionToCollaborators(destinataires, addviseSession, currentUser);
        sendMailAddviseOnActionSessionToRH(addviseSession, isDejaConfirme);
    }

    /**
     * Créer une liste de collaborateurs destinataires à partir d'une session
     *
     * @param addviseSession la session
     */
    private List<Collaborator> getDestinataires(AddviseSession addviseSession) {

        List<Collaborator> collaborateurs = addviseSession.getCollaborateurs();
        List<Collaborator> recipients = new ArrayList<>();

        for (Collaborator collaborateur : collaborateurs) {
            AddviseLinkSessionCollaborateur invitation = addviseSession.getListeLinkSessionCollaborateur().stream().filter(invit -> invit.getCollaborateur().equals(collaborateur)).findFirst().get();
            if (invitation.getEtatInvitation() == AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE ||
                    invitation.getEtatInvitation() == AddviseLinkSessionCollaborateur.EtatInvitation.ENVOYEE) {
                recipients.add(collaborateur);
            }
        }
        return recipients;
    }

    private void setTokenToNullBySession(AddviseSession addviseSession) {
        List<Collaborator> collaborateurs = addviseSession.getCollaborateurs();

        for (Collaborator collaborateur : collaborateurs) {
            AddviseLinkSessionCollaborateur invitation;
            invitation = addviseSession.getListeLinkSessionCollaborateur().stream().filter(invit -> invit.getCollaborateur().equals(collaborateur)).findFirst().get();
            invitation.setToken(null);
        }
    }

    /**
     * Envoi un mail au collaborateur à l'action du bouton sauvegarder Modules Addvise
     *
     * @param invit       l'invitation du collaborateur concerné par l'envoi de mail
     * @param currentUser
     */
    private void sendMailAddviseOnSauvegarderToCollaborator(AddviseLinkSessionCollaborateur invit, String urlIntern,
                                                             Collaborator currentUser) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        if (invit.getSession().getDate().before(cal.getTime())) {
            return;
        }

        if (invit.getSession().getEtat() == AddviseSession.EtatSession.CONFIRMEE ||
                invit.getSession().getEtat() == AddviseSession.EtatSession.EN_COURS_DE_SAISIE) {

            AddviseLinkSessionCollaborateur.EtatInvitation etatInvitation = invit.getEtatInvitation();

            List<Collaborator> destinataires = new ArrayList<>();

            destinataires.add(invit.getCollaborateur());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            String dateStr = simpleDateFormat.format(invit.getSession().getDate());

            int urlIndex = urlIntern.indexOf("Administration");
            if (urlIndex > -1) {
                urlIntern = urlIntern.substring(0, urlIndex);
            }

            SecureRandom random = new SecureRandom();
            StringBuilder token = new StringBuilder();
            token.append(new BigInteger(400, random).toString(32));

            invit.setToken(token.toString());
            String nomModule = invit.getSession().getModuleAddvise().getNomModuleAddvise();
            String prenomInvite = invit.getCollaborateur().getPrenom();
            String formateur = invit.getSession().getFormateur().getPrenom() + " " + invit.getSession().getFormateur().getNom();
            Date today = new Date();
            cal.setTime(today);
            cal.add(Calendar.DAY_OF_MONTH, +7);
            Date dateLimite = cal.getTime();
            String dateLimiteStr = simpleDateFormat.format(dateLimite);

            String[] params = {nomModule, dateStr, currentUser.getNom() + " " + currentUser.getPrenom(), token.toString(), prenomInvite, formateur, dateLimiteStr, urlIntern, Constantes.APPLICATION_URL};

            switch (etatInvitation) {
                case ENVOYEE:
                    mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiInvitationAddvise.object",
                            "email.envoiInvitationAddvise.message", params);
                    break;
                case DECLINEE:
                    mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiDeclinaisonInvitationAddvise.object",
                            "email.envoiDeclinaisonInvitationAddvise.message", params);
                    invit.setToken(null);
                    break;
                case CONFIRMEE:
                    mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiConfirmationParticipationAddvise.object",
                            "email.envoiConfirmationParticipationAddvise.message", params);
                    if (invit.getSession().getEtat() == AddviseSession.EtatSession.CONFIRMEE) {
                        sendConfirmationSessionAddvise(destinataires, params);
                    }
                    invit.setToken(null);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Envoi un mail au collaborateur à l'action du bouton sauvegarder Modules Addvise
     *
     * @param invitations les invitations des collaborateurs concernés par la session Modules Addvise
     * @param currentUser
     */
    private void sendMailAddviseOnSauvegarderToRh(List<AddviseLinkSessionCollaborateur> invitations,
                                                  Collaborator currentUser) {

        Collaborator rh = new Collaborator();
        try {
            rh.setMail(Parametrage.getContext(Constantes.EMAIL_ADDVISE_CONTEXT));
        } catch (NamingException error) {
            logger.error("[send mail addvise] context parameter naming error", error);
        }
        List<Collaborator> destinataires = new ArrayList<>();
        destinataires.add(rh);

        String listeInvite = "";
        String listeDecline = "";
        String listeConfirme = "";

        boolean listInviteNotVide = false;
        boolean listDeclineNotVide = false;
        boolean listConfirmeNotVide = false;

        for (AddviseLinkSessionCollaborateur invit : invitations) {
            AddviseLinkSessionCollaborateur.EtatInvitation etatInvitation = invit.getEtatInvitation();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            String dateStr = simpleDateFormat.format(invit.getSession().getDate());
            switch (etatInvitation) {
                case ENVOYEE:
                    listInviteNotVide = true;
                    listeInvite += "<li>" + invit.getCollaborateur().getPrenom() + " " + invit.getCollaborateur().getNom() + " (" + invit.getSession().getModuleAddvise().getNomModuleAddvise() + " le " + dateStr + ")</li>";
                    break;
                case DECLINEE:
                    listDeclineNotVide = true;
                    listeDecline += "<li>" + invit.getCollaborateur().getPrenom() + " " + invit.getCollaborateur().getNom() + " (" + invit.getSession().getModuleAddvise().getNomModuleAddvise() + " le " + dateStr + ")</li>";
                    break;
                case CONFIRMEE:
                    listConfirmeNotVide = true;
                    listeConfirme += "<li>" + invit.getCollaborateur().getPrenom() + " " + invit.getCollaborateur().getNom() + " (" + invit.getSession().getModuleAddvise().getNomModuleAddvise() + " le " + dateStr + ")</li>";
                    break;
                default:
                    break;
            }
        }
        if (listInviteNotVide) {
            listeInvite = "<p>Invitations envoyées:<p/><ul>" + listeInvite + "</ul>";
        }
        if (listDeclineNotVide) {
            listeDecline = "<p>Invitations déclinées:<p/><ul>" + listeDecline + "</ul>";
        }
        if (listConfirmeNotVide) {
            listeConfirme = "<p>Invitations confirmées:<p/><ul>" + listeConfirme + "</ul>";
        }

        String params[] = {listeInvite, listeDecline, listeConfirme, currentUser.getNom() + " " + currentUser.getPrenom()};

        mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiConfirmationSauvegardeRH.object",
                "email.envoiConfirmationSauvegardeRH.message", params);
    }

    /**
     * Envoi un mail pour chaque collaborateur concerné par la session Modules Addvise supprimée ou confirmée
     *
     * @param destinataires  liste des collaborateurs concerné par l'envoi du Mail
     * @param addviseSession session Modules Addvise concené par l'action
     * @param currentUser
     */
    private void sendMailAddviseOnActionSessionToCollaborators(List<Collaborator> destinataires,
                                                                AddviseSession addviseSession,
                                                                Collaborator currentUser) {

        AddviseSession.EtatSession etatSession = addviseSession.getEtat();

        if (addviseSession.getDate().compareTo(new Date()) < 0) {
            return;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String dateStr = simpleDateFormat.format(addviseSession.getDate());

        String[] params = {addviseSession.getModuleAddvise().getNomModuleAddvise(), dateStr, currentUser.getNom() + " " + currentUser.getPrenom()};
        switch (etatSession) {
            case CONFIRMEE:
                sendConfirmationSessionAddvise(destinataires, params);
                break;
            case SUPPRIMEE:
                sendSuppressionSessionAddvise(destinataires, params);
                break;
            default:
                break;
        }
    }

    /**
     * Envoi un mail RH pour chaque participant  concerné par la session Modules Addvise supprimée ou confirmée hors collaborateurs
     *
     * @param addviseSession session Modules Addvise concené par l'action
     * @param isDejaConfirme indication sur l'état précedant le changement d'etat opéré par l'action
     */
    private void sendMailAddviseOnActionSessionToRH(AddviseSession addviseSession, boolean isDejaConfirme) {

        AddviseSession.EtatSession etatSession = addviseSession.getEtat();
        List<Collaborator> destinataires = new ArrayList<>();

        Collaborator rh = new Collaborator();
        try {
            rh.setMail(Parametrage.getContext(Constantes.EMAIL_ADDVISE_CONTEXT));
        } catch (NamingException error) {
            logger.error("[send mail addvise] context parameter naming error", error);
        }
        destinataires.add(rh);

        Collaborator formateur = new Collaborator();
        formateur.setMail(addviseSession.getFormateur().getEmail());

        if (addviseSession.getDate().compareTo(new Date()) < 0) {
            return;
        }

        String listeParticipants = "";

        for (Collaborator collaborateur : addviseSession.getCollaborateurs()) {
            AddviseLinkSessionCollaborateur invitation = addviseSession.getListeLinkSessionCollaborateur().stream()
                    .filter(invit -> invit.getCollaborateur().equals(collaborateur)).findFirst().get();

            if (invitation.getEtatInvitation() == AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE) {
                listeParticipants += "<li>" + collaborateur.getNom().toUpperCase() + " " + collaborateur.getPrenom() + "</li>";
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String dateStr = simpleDateFormat.format(addviseSession.getDate());

        String[] params = {addviseSession.getModuleAddvise().getNomModuleAddvise(), dateStr, listeParticipants};

        switch (etatSession) {
            case CONFIRMEE:
                destinataires.add(formateur);
                mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiConfirmationSessionRH.object",
                        "email.envoiConfirmationSessionRH.message", params);
                break;
            case SUPPRIMEE:
                if (isDejaConfirme) {
                    destinataires.add(formateur);
                }
                mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiSuppressionSessionRH.object",
                        "email.envoiSuppressionSessionRH.message", params);
                break;
            default:
                break;
        }
    }

    /**
     * Envoi un mail de Confirmation à une liste de detistataire
     *
     * @param destinataires indication sur l'état précedant le changement d'etat opéré par l'action
     */
    private void sendConfirmationSessionAddvise(List<Collaborator> destinataires, String[] params) {
        mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiConfirmationModuleAddvise.object",
                "email.envoiConfirmationModuleAddvise.message", params);
    }

    /**
     * Envoi un mail de Confirmation à une liste de detistataire
     *
     * @param destinataires indication sur l'état précedant le changement d'etat opéré par l'action
     */
    private void sendSuppressionSessionAddvise(List<Collaborator> destinataires, String[] params) {
        mailService.sendMailAddviseCollaborator(properties, destinataires, "email.envoiSuppressionModuleAddvise.object",
                "email.envoiSuppressionModuleAddvise.message", params);
    }
}
