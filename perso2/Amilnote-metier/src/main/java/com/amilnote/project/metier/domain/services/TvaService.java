/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.TVA;
import com.amilnote.project.metier.domain.entities.json.TVAAsJson;
import com.amilnote.project.metier.domain.utils.TVAForm;

import java.util.List;


/**
 * The interface Tva service.
 */
public interface TvaService extends AbstractService<TVA> {

    /**
     * Retourne toutes les TVA
     *
     * @return all tva
     */
    List<TVA> getAllTVA();

    /**
     * Retourne une TVA en fonction de l'id
     *
     * @param pId the p id
     * @return TypeFrais tva by id
     */
    TVA getTVAById(long pId);

    /**
     * Retourne toutes les TVA au format json
     *
     * @return String (Liste des TVA)
     */
    List<TVAAsJson> getAllTVAAsJson();

    /**
     * Retourne une TVA en fonction de son code
     *
     * @param code Code de la TVA souhaitée
     * @return TVA lié au code
     */
    TVA getTVAByCode(String code);

    /**
     * Mise à jour de toutes les TVA existantes
     *
     * @param pTvaForm the p tva form
     */
    void updateAllTVA(TVAForm pTvaForm);

    /**
     * Update tva value depending on clients intracommunautary number
     * @param commande Commande object
     * @param bill Facture object
     * @return intracommunautary tva rates id
     */

    long updateTVAIntracRate(Commande commande, Facture bill);
}
