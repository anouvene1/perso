/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by sbenslimane on 22/12/2016.
 */
@Entity
@Table(name = "ami_link_perimetre_mission")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LinkPerimetreMission implements Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_ID_PERIMETRE_MISSION.
     */
    public static final String PROP_ID_PERIMETRE_MISSION = "id_perimetre";
    /**
     * The constant PROP_ID_MISSION.
     */
    public static final String PROP_ID_MISSION = "id_mission";

    private Long id;
    private PerimetreMission perimetreMission;
    private Mission mission;

    /**
     * Constructeur
     */
    public LinkPerimetreMission() {
    }

    /**
     * Instantiates a new Link perimetre mission.
     *
     * @param id               the id
     * @param perimetreMission the perimetre mission
     * @param mission          the mission
     */
    public LinkPerimetreMission(Long id, PerimetreMission perimetreMission, Mission mission) {
        this.id = id;
        this.perimetreMission = perimetreMission;
        this.mission = mission;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets perimetre mission.
     *
     * @return idPerimetre perimetre mission
     */
    @ManyToOne
    @JoinColumn(name = "id_perimetre")
    public PerimetreMission getPerimetreMission() {
        return perimetreMission;
    }

    /**
     * Sets perimetre mission.
     *
     * @param perimetreMission the perimetre mission
     */
    public void setPerimetreMission(PerimetreMission perimetreMission) {
        this.perimetreMission = perimetreMission;
    }

    /**
     * Gets mission.
     *
     * @return Mission mission
     */
    @ManyToOne
    @JoinColumn(name = "id_mission")
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(Mission mission) {
        this.mission = mission;
    }

}
