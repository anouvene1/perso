/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Poste;

/**
 * The type Poste as json.
 */
public class PosteAsJson {


    private Long id;
    private String poste;
    private String code;
    private int etat;

    /**
     * Constructeur par defaut de la classe TypeFrais
     */
    public PosteAsJson() {
    }

    /**
     * Constructeur de la classe TypeFrais
     *
     * @param pPoste the p poste
     */
    public PosteAsJson(Poste pPoste) {
        super();
        this.setId(pPoste.getId());
        this.setPoste(pPoste.getPoste());
        this.setEtat(pPoste.getEtat());
        this.setCode(pPoste.getCode());
    }

	
	/* 
     * GETTERS AND SETTERS
	 */

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets poste.
     *
     * @return the Poste
     */
    public String getPoste() {
        return poste;
    }

    /**
     * Sets poste.
     *
     * @param pPoste the Poste to set
     */
    public void setPoste(String pPoste) {
        this.poste = pPoste;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * 1: actif
     * 0:inactif
     *
     * @return etat etat
     */
    public int getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(int pEtat) {
        etat = pEtat;
    }
}
