/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;

import java.util.List;

/**
 * The interface Client dao.
 */
public interface ClientDAO extends LongKeyDAO<Client> {

    /**
     * retrouver tous les client classés par nom (ascendant)
     *
     * @return une liste de client
     */
    List<Client> findAllOrderByNomAsc();


    /**
     * Mise à jour ou création d'un client dans la BDD
     *
     * @param pClientAsJson the p client as json
     * @return string string
     * @throws Exception the exception
     */
    String updateOrCreateClient(ClientAsJson pClientAsJson) throws Exception;

    /**
     * Suppression d'un client dans la BDD
     *
     * @param pClient the p client
     * @return string string
     */
    String deleteClient(Client pClient);

}
