/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;

import java.util.List;

/**
 * The type Absences form.
 */
public class AbsencesForm {

    private List<AbsenceAsJson> listAbsences;
    private String commentaire;

    /**
     * Instantiates a new Absences form.
     */
    public AbsencesForm() {
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the commentaire to set
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }

    /**
     * Gets list absences.
     *
     * @return the listAbsences
     */
    public List<AbsenceAsJson> getListAbsences() {
        return listAbsences;
    }

    /**
     * Sets list absences.
     *
     * @param pListAbsences the listAbsences to set
     */
    public void setListAbsences(List<AbsenceAsJson> pListAbsences) {
        listAbsences = pListAbsences;
    }
}
