/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.ClientDAOImpl;
import com.amilnote.project.metier.domain.dao.impl.ContactClientDAOImpl;
import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.amilnote.project.metier.domain.entities.json.ContactClientAsJson;
import com.amilnote.project.metier.domain.services.ContactClientService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Contact client service.
 */
@Service("contactClientService")
public class ContactClientServiceImpl extends AbstractServiceImpl<ContactClient, ContactClientDAOImpl> implements ContactClientService {

    @Autowired
    private ContactClientDAOImpl contactClientDAO;

    @Autowired
    private ClientDAOImpl clientDAO;

    private List<ContactClientAsJson> listClientsToAdd = new ArrayList<>();

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public ContactClientDAOImpl getDAO() {
        return contactClientDAO;
    }

    /**
     * {@linkplain ContactClientService#findById(Long)}
     */
    @Override
    public ContactClient findById(Long pIdContactClient) {
        return contactClientDAO.findUniqEntiteByProp(ContactClient.PROP_ID, pIdContactClient);
    }

    /**
     * {@linkplain ContactClientService#findAllOrderByNomAsc()}
     */
    @Override
    public List<ContactClient> findAllOrderByNomAsc() {
        return contactClientDAO.findAllOrderByNomAsc();
    }

    /**
     * {@linkplain ContactClientService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<ContactClientAsJson> findAllOrderByNomAscAsJson() {
        List<ContactClient> tmpListClient = this.findAllOrderByNomAsc();
        List<ContactClientAsJson> tmpListClientAsJson = new ArrayList<>();

        for (ContactClient tmpClient : tmpListClient) {

            tmpListClientAsJson.add(tmpClient.toJson());

        }
        return tmpListClientAsJson;
    }

    /**
     * {@linkplain ContactClientService#getListContactsToAdd()}
     */
    @Override
    public List<ContactClientAsJson> getListContactsToAdd() {
        return listClientsToAdd;
    }

    /**
     * {@linkplain ContactClientService#findById(Long)}
     */
    @Override
    public void setListForfaitsToAdd(List<ContactClientAsJson> listContactsToAdd) {
        this.listClientsToAdd = listContactsToAdd;
    }

    /**
     * {@linkplain ContactClientService#findAllContactsForClient(Client)}
     */
    @Override
    public List<ContactClientAsJson> findAllContactsForClient(Client pClient) {
        List<ContactClient> lListContactsClient = contactClientDAO.findListEntitesByProp(ContactClient.PROP_CLIENT, pClient, ContactClient.PROP_NOM);
        List<ContactClientAsJson> lListContactClientAsJson = new ArrayList<>();

        for (ContactClient lContactClient : lListContactsClient) {
            ContactClientAsJson contactAsJson = new ContactClientAsJson(lContactClient);
            if (lContactClient.getTelephone() != null) {
                contactAsJson.setTelephone(lContactClient.getTelephone());
            }
            lListContactClientAsJson.add(contactAsJson);
        }

        return lListContactClientAsJson;
    }

    /**
     * {@linkplain ContactClientService#findByMailForClient(String, Client)}
     */
    @Override
    public ContactClient findByMailForClient(String pMail, Client pClient) {

        List<ContactClient> lListContact = contactClientDAO.findListEntitesByProp(ContactClient.PROP_CLIENT, pClient, ContactClient.PROP_MAIL);

        for (ContactClient contact : lListContact) {
            //--- Contrôle d'égalité sans les espaces
            if (StringUtils.equals(contact.getMail().trim(), pMail.trim())) {
                return contact;
            }
        }

        return null;
    }

    /**
     * {@linkplain ContactClientService#createOrUpdtateContact(ContactClientAsJson, ClientAsJson)}
     */
    @Override
    @Transactional
    public ResponseEntity<ContactClientAsJson> createOrUpdtateContact(ContactClientAsJson contactClientAsJson, ClientAsJson clientAsJson) {
        ResponseEntity<ContactClientAsJson> response = null;

        if (clientAsJson != null) {
            // --- Retrieve client
            Client client = clientDAO.get(clientAsJson.getId());
            ContactClient contact = new ContactClient();
            if (null != contactClientAsJson.getId()) {
                contact = findUniqEntiteByProp(ContactClient.PROP_ID, contactClientAsJson.getId());
            }

            // --- Add informations about contact
            contact.setClient(client);
            contact.setMail(contactClientAsJson.getMail());
            contact.setNom(contactClientAsJson.getNom());
            contact.setPrenom(contactClientAsJson.getPrenom());
            contact.setRespClient(contactClientAsJson.getRespClient());
            contact.setRespFacturation(contactClientAsJson.getRespFacturation());
            if (contactClientAsJson.getTelephone() != null) {
                contact.setTelephone(contactClientAsJson.getTelephone());
            }
            contact.setEnabled(!contactClientAsJson.isEnabled());

            // --- Create/update
            try {
                contactClientDAO.saveOrUpdate(contact);
                if(null != findById(contactClientAsJson.getId())) { // Updated contact client
                    response =  new ResponseEntity<>(findById(contactClientAsJson.getId()).toJson(), HttpStatus.OK);
                } else { // New created contact client
                    response =  new ResponseEntity<>(null, HttpStatus.OK);
                }
            } catch(DataAccessException dataAccessException) {
                response =  new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            }
        }

        return response;
    }

    /**
     * {@linkplain ContactClientService#deleteContactClient(ContactClient)}
     */
    @Override
    public String deleteContactClient(ContactClient pContact) {
        return contactClientDAO.deleteContactClient(pContact);
    }

    /**
     * {@linkplain ContactClientService#findAllBillingManagersByCustomer(Client)}
     */
    @Override
    public List<ContactClient> findAllBillingManagersByCustomer(Client customer) {
        List<ContactClient> tempListContactsCustomer = contactClientDAO.findListEntitesByProp(ContactClient.PROP_CLIENT,customer,ContactClient.PROP_NOM);
        return tempListContactsCustomer.stream().filter(
                ContactClient::isRespFacturation
        ).collect(Collectors.toList());
    }
}
