/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;


import com.amilnote.project.metier.domain.entities.Parametre;
import com.amilnote.project.metier.domain.entities.json.ParametreAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;

/**
 * The interface Parametre service.
 */
public interface ParametreService extends AbstractService<Parametre> {

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param pParametreJson the p parametre json
     * @return int int
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    int createOrUpdateParametre(ParametreAsJson pParametreJson) throws JsonProcessingException, IOException;

    /**
     * Suppression de la commande
     *
     * @param pParametre the p parametre
     * @return string string
     */
    String deleteParametre(Parametre pParametre);

    /**
     * Find all order by nom asc list.
     *
     * @return the list
     */
    List<Parametre> findAllOrderByNomAsc();

    /**
     * Find by id parametre.
     *
     * @param pIdParametre the p id parametre
     * @return the parametre
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    Parametre findById(int pIdParametre);

    /**
     * retrouver toutes les commandes et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<ParametreAsJson> findAllOrderByNomAscAsJson();

}
