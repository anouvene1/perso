package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.*;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Encryption;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.STATUT_SOUS_TRAITANT;
import static com.amilnote.project.metier.domain.utils.Utils._generate;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.EQUAL;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.IN;


/**
 * The type Collaborator service.
 */
@Service("collaborateurService")
public class CollaboratorServiceImpl extends AbstractServiceImpl<Collaborator, CollaboratorDAO> implements CollaboratorService {

    private static final Logger logger = LogManager.getLogger(CollaboratorServiceImpl.class);

    @Autowired
    private CollaboratorDAO collaboratorDAO;

    @Autowired
    private AbsenceDAO absenceDAO;

    @Autowired
    private RapportActivitesDAO rapportActivitesDAO;

    @Autowired
    private PosteService postService;

    @Autowired
    private StatutCollaborateurDAO statutCollaborateurDAO;

    @Autowired
    private ModuleAddviseDAO moduleAddviseDAO;

    @Autowired
    private RaisonSocialeDAO raisonSocialeDAO;

    @Autowired
    private MissionService missionService;

    @Autowired
    private StatutCollaborateurService statutCollaborateurService;

    @Autowired
    private CiviliteService civiliteService;

    @Autowired
    private MailService mailService;


    @Override
    public CollaboratorDAO getDAO() {
        return collaboratorDAO;
    }

    /**
     * {@linkplain CollaboratorService#findByMail(String)}
     */
    @Override
    public Collaborator findByMail(String pMail) {
        return collaboratorDAO.findUniqEntiteByProp(Collaborator.PROP_MAIL, pMail);
    }

    /**
     * {@linkplain CollaboratorService#findByUserNameAndPassword(String, String)}
     */
    @Override
    public Collaborator findByUserNameAndPassword(String pMail, String pPassword) {
        return collaboratorDAO.findByUsernameAndPassword(pMail, pPassword);
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Collaborator> findAllOrderByNomAsc() {
        //with admin, without disabled
        return collaboratorDAO.findTravailleurs();
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscWithoutADMIN()}
     */
    @Override
    public List<Collaborator> findAllOrderByNomAscWithoutADMIN() {
        //without admin, without disabled
        return collaboratorDAO.findCollaborators(false, false);
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscWithoutADMINWithDisabled()}
     */
    @Override
    public List<Collaborator> findAllOrderByNomAscWithoutADMINWithDisabled() {
        //without admin, with disabled
        return collaboratorDAO.findCollaborators(false, true);
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsForAbsenceMonthAsJson(Date)}
     */
    @Override
    public List<CollaboratorAsJson> findCollaboratorsForAbsenceMonthAsJson(Date pDate) {
        List<Collaborator> collaboratorsList = collaboratorDAO.findCollaboratorsForAbsenceMonth(pDate);
        List<CollaboratorAsJson> collaboratorAsJsonList = new ArrayList<>();

        for (Collaborator collaborator : collaboratorsList) {
            collaboratorAsJsonList.add(collaborator.toJson());
        }
        return collaboratorAsJsonList;
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsForFraisAnnuelAsJson(int)}
     */
    @Override
    public List<CollaboratorAsJson> findCollaboratorsForFraisAnnuelAsJson(int yearFrais) {
        List<Collaborator> tmpListCollaborator = collaboratorDAO.findCollaboratorsForFraisAnnuel(yearFrais);
        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<>();

        for (Collaborator tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collaborator findCollaboratorByName(String firstname, String lastname) {
        return collaboratorDAO.findCollaboratorByName(firstname, lastname);
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsByResponsable(Collaborator)}
     */
    @Override
    public List<Collaborator> findCollaboratorsByResponsable(Collaborator responsable) {
        return collaboratorDAO.findCollaboratorsByResponsable(responsable);
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorByNameAsJSON(String, String)}
     */
    @Override
    public CollaboratorAsJson findCollaboratorByNameAsJSON(String firstname, String lastname) {
        return collaboratorDAO.findCollaboratorByName(firstname, lastname).toJson();
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsByResponsableAsJSON(Collaborator)}
     */
    @Override
    public List<CollaboratorAsJson> findCollaboratorsByResponsableAsJSON(Collaborator responsable) {
        List<Collaborator> collaborators = collaboratorDAO.findCollaboratorsByResponsable(responsable);
        List<CollaboratorAsJson> collaboratorsJSON = new ArrayList<>();

        for (Collaborator tmpCollab : collaborators) {
            if (tmpCollab.isEnabled() && !isAdminManager(tmpCollab)) {
                collaboratorsJSON.add(tmpCollab.toJson());
            }
        }

        return collaboratorsJSON;
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscAsJsonWithoutADMIN()}
     */
    @Override
    public List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMIN() {

        return convertCollaboratorListToAsJsonList(this.findAllOrderByNomAscWithoutADMIN());
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscAsJsonWithoutADMINWithDisabled()}
     */
    @Override
    public List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMINWithDisabled() {

        return convertCollaboratorListToAsJsonList(this.findAllOrderByNomAscWithoutADMINWithDisabled());
    }


    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllOrderByNomAscAsJson() {

        List<Collaborator> tmpListCollaborator = this.findAllOrderByNomAsc();
        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<>();
        for (Collaborator tmpCollaborator : tmpListCollaborator) {
            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    @Override
    public List<CollaboratorAsJson> findAllWithMissionOrderByNomAscAsJson() {
        List<Collaborator> tmpListCollaborator = collaboratorDAO.findTravailleurs();
        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<>();
        List<Mission> listMission;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date result = cal.getTime();
        DateTime date = new DateTime(result);

        for (Collaborator tmpCollaborator : tmpListCollaborator) {

            listMission = missionService.findMissionsClientesByMonthForCollaborator(tmpCollaborator, date);
            if (!listMission.isEmpty())
                tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteCollaborator(Collaborator collaborator) {

        // --- Récupération du collaborateur
        Collaborator collaborateur = this.get(collaborator.getId());

        // --- Test de l'existance du collaborateur
        if (null != collaborateur) {

            // BAB 15/06/2016 - [AMILNOTE-100] : on ne supprime plus un collaborateur lorsque on le desactive dans l'appli
            // on lui bloque juste l'acces à l'application je commente donc tout ce qui a ete fait ci-dessous :(

            //On vérifie que le collab ne soit pas manger d'un autre collab
            List<Collaborator> allCollab = collaboratorDAO.findTravailleurs();
            StringBuilder collabManager = new StringBuilder();
            Boolean isManager = false;
            for (Collaborator collab : allCollab) {
                if (collab.getManager() != null) {
                    if (collab.getManager().getId() == collaborateur.getId()) {
                        collabManager.append(collab.getMail() + " ");
                        isManager = true;
                    }
                }
            }

            if (isManager) {
                return "Attention: ce collaborateur ne peut pas être désactivé tant qu'il est associé à  un autre collaborateur en tant que manager (cf liste ci-dessous):"
                        + "<p>" + collabManager + "</p>";
            }

            // --- Création de la session et de la transaction
            Session session = collaboratorDAO.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // BAB 15/06/2016 - [AMILNOTE-100] : on set juste le flag en bdd enabled=false pour couper l'accès
            // on set la date de sortie a la date du jour
            collaborateur.setEnabled(false);
            collaborateur.setDateSortie(collaborator.getDateSortie());
            // on sauvegarde la modification en bdd
            session.save(collaborateur);

            // --- Sauvegarde et Commit de la mise à jour
            session.flush();
            transaction.commit();

            List<Mission> missions = collaborateur.getMissions();
            for (Mission mission : missions) {
                try {
                    missionService.updateEtatMission(mission.getId(), Etat.ETAT_ANNULE);
                } catch (Exception e) {
                    logger.error("[update mission state] mission id : {}", mission.getId(), e);
                }
            }

            return "Désactivation effectuée avec succès";
        } else {
            return "Erreur collaborateur null";
        }

    }


    /**
     * @param nomEmail email
     * @return true or false
     */
    @Override
    public boolean checkEmailExist(String nomEmail) {

        Boolean res;

        Collaborator collaborateur = collaboratorDAO.findByMail(nomEmail);
        if (collaborateur != null) {
            res = true;
        } else {
            res = false;
        }
        return res;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String updateOrCreateCollaborator(CollaboratorAsJson collaboratorAsJson) throws Exception {

        String messageRetour = "";

        if (null == collaboratorAsJson) {
            throw new Exception("Le collaborateur ne doit pas être vide");
        }

        Collaborator tmpCollaborator = null;
        String clearTmpPassword = "";

        // Modification d'un collaborateur existant
        if (null != collaboratorAsJson.getId()) {

            tmpCollaborator = this.get(collaboratorAsJson.getId());

            if (null == tmpCollaborator) {
                throw new Exception("L'identifiant du collaborateur est inconnu");
            }

            if (null != collaboratorAsJson.getEnabled()) {
                tmpCollaborator.setEnabled(collaboratorAsJson.getEnabled());
            }
            if (null != collaboratorAsJson.getMail()) {
                if (!checkEmailExist(collaboratorAsJson.getMail()) || null != collaboratorAsJson.getId()) {
                    tmpCollaborator.setMail(collaboratorAsJson.getMail());
                } else {
                    throw new Exception("Erreur dans l'adresse email: Adresse vide ou déjà existante en base.");
                }
            }
            if (null != collaboratorAsJson.getNom()) {
                tmpCollaborator.setNom(collaboratorAsJson.getNom());
            }
            if (null != collaboratorAsJson.getPrenom()) {
                tmpCollaborator.setPrenom(collaboratorAsJson.getPrenom());
            }
            if (null != collaboratorAsJson.getPassword()) {
                try {
                    tmpCollaborator.setPasswordAndEncode(collaboratorAsJson.getPassword());
                } catch (NoSuchAlgorithmException e) {
                    logger.error("[password encoding] not found algorithm", e);
                }
            }
            if (null != collaboratorAsJson.getPoste()) {
                tmpCollaborator.setPoste(postService.get(collaboratorAsJson.getPoste().getId()));
            }
            if (null != collaboratorAsJson.getTelephone()) {
                tmpCollaborator.setTelephone(collaboratorAsJson.getTelephone());
            }
            if (null != collaboratorAsJson.getManager()) {
                Collaborator tmpManager = this.get(collaboratorAsJson.getManager().getId());

                if (null == tmpManager) {
                    throw new Exception("L'identifiant de ce manager est inconnu");
                }

                tmpCollaborator.setManager(tmpManager);
            }
            if (null != collaboratorAsJson.getStatut()) {
                StatutCollaborateur newStatut = statutCollaborateurDAO.get(collaboratorAsJson.getStatut().getId());
                checkAndChangeStatut(tmpCollaborator, newStatut);
            }
            if (null != collaboratorAsJson.getDateEntree()) {
                tmpCollaborator.setDateEntree(collaboratorAsJson.getDateEntree());
            }
            if (null != collaboratorAsJson.getDateNaissance()) {
                tmpCollaborator.setDateNaissance(collaboratorAsJson.getDateNaissance());
            }

            if (null != collaboratorAsJson.getAdressePostale()) {
                tmpCollaborator.setAdressePostale(collaboratorAsJson.getAdressePostale());
            }

            if (null != collaboratorAsJson.getMailPerso()) {
                tmpCollaborator.setMailPerso(collaboratorAsJson.getMailPerso());
            }

            if (null != collaboratorAsJson.getRaisonSociale()) {
                tmpCollaborator.setRaisonSociale(raisonSocialeDAO.findByIdCollab(collaboratorAsJson.getId()));
                tmpCollaborator.getRaisonSociale().setRaisonSociale(collaboratorAsJson.getRaisonSociale().getRaisonSociale());
            }

            if (null != collaboratorAsJson.getStatutGeneral()) {
                tmpCollaborator.setStatutGeneral(collaboratorAsJson.getStatutGeneral());
            }
            tmpCollaborator.setExclusAddvise(collaboratorAsJson.getExclusAddvise());

            if (null != collaboratorAsJson.getCivilite()) {
                tmpCollaborator.setCivilite(civiliteService.get(collaboratorAsJson.getCivilite().getId()));
            } else {
                tmpCollaborator.setCivilite(civiliteService.get(Civilite.CIVILITE_INCONNU_ID));
            }

            tmpCollaborator.setAgency(Agency.getFromId(collaboratorAsJson.getAgency()));

            messageRetour = "1";
        } else {

            if (checkEmailExist(collaboratorAsJson.getMail())) {
                throw new Exception("Erreur dans l'adresse email: Adresse vide ou déjà existante en base.");
            }

            Collaborator manager = null;
            if (null != collaboratorAsJson.getManager()) {
                manager = this.getDAO().get(collaboratorAsJson.getManager().getId());
            }

            Date dateEmbauche = new Date();
            if (null != collaboratorAsJson.getDateEntree()) {
                dateEmbauche = collaboratorAsJson.getDateEntree();
            }

            Date dateSortie = null;
            tmpCollaborator = new Collaborator(
                    collaboratorAsJson.getNom(),
                    collaboratorAsJson.getPrenom(),
                    statutCollaborateurDAO.get(collaboratorAsJson.getStatut().getId()),
                    collaboratorAsJson.getEnabled(),
                    collaboratorAsJson.getTelephone(),
                    collaboratorAsJson.getMail(),
                    "",
                    null,
                    manager,
                    postService.get(collaboratorAsJson.getPoste().getId()),
                    true,
                    // BAB 16/06/2016 [AMILNOTE 100] : on ajoute la date d'embauche, de sortie=null,
                    // et la date d'anniversaire du collab a sa creation.
                    0, dateEmbauche, dateSortie, collaboratorAsJson.getDateNaissance(),
                    collaboratorAsJson.getAdressePostale(),
                    collaboratorAsJson.getMailPerso(),
                    collaboratorAsJson.getStatutGeneral(),
                    null,
                    civiliteService.get(collaboratorAsJson.getCivilite().getId()),
                    Agency.getFromId(collaboratorAsJson.getAgency())
            );

            if (collaboratorAsJson.getRaisonSociale() != null) {
                RaisonSociale raisTemp = new RaisonSociale();
                raisTemp.setRaisonSociale(collaboratorAsJson.getRaisonSociale().getRaisonSociale());
                raisTemp.setCollaborator(tmpCollaborator);
                tmpCollaborator.setRaisonSociale(raisTemp);
            }

            //String tmpPassword = pCollaborateurAsJson.getMail().split("@")[0];
            clearTmpPassword = _generate(11);
            String hashedTmpPassword = "";
            try {
                hashedTmpPassword = Encryption.encryptSHA(clearTmpPassword);
            } catch (NoSuchAlgorithmException e) {
                logger.error("[password encryption] algorithm not found", e);
            }

            try {
                //--- Encodage du mot de passe
                tmpCollaborator.setPasswordAndEncode(hashedTmpPassword);
            } catch (NoSuchAlgorithmException e) {
                logger.error("[password encode] algorithm not found", e);
            }
            messageRetour = "2";
        }

        // --- Création de la session et de la transaction
        Session session = collaboratorDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        session.saveOrUpdate(tmpCollaborator);

        if (collaboratorAsJson.getId() == null) {
            collaboratorAsJson.setId(getDAO().findUniqEntiteByProp(Collaborator.PROP_MAIL, tmpCollaborator.getMail()).getId());
        }
        session.flush();
        transaction.commit();

        //sauvegarde de la maj et commit raison sociale
        if (collaboratorAsJson.getRaisonSociale() != null) {
            tmpCollaborator.getRaisonSociale().setCollaborator(tmpCollaborator);
            Session session2 = raisonSocialeDAO.getCurrentSession();
            Transaction transaction2 = session.beginTransaction();
            session2.saveOrUpdate(tmpCollaborator.getRaisonSociale());

            if (collaboratorAsJson.getRaisonSociale() == null) {
                collaboratorAsJson.setRaisonSociale(tmpCollaborator.getRaisonSociale().toJson());
            }

            session2.flush();
            transaction2.commit();
        }

        // dans le cas d'une création, envoi du mail avec le mot de passe
        if (messageRetour.equals("2")) {
            String env = "";
            if (!Boolean.valueOf(Parametrage.getContext("env.prod"))) env = "[DEV]";
            mailService.sendMailCreationCollab(collaboratorAsJson, clearTmpPassword, env);
        }

        return messageRetour;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollaboratorAsJson findByIdAbsence(Long pIdAbsence) throws Exception {

        Absence absence = absenceDAO.get(pIdAbsence);

        if (null == absence) {
            throw new Exception("Identifiant n°" + pIdAbsence + " inconnu pour les absences");
        }

        return absence.getCollaborateur().toJson();
    }

    /**
     * {@linkplain CollaboratorService#findByIdRA(Long)}
     */
    @Override
    public CollaboratorAsJson findByIdRA(Long pId) throws Exception {

        RapportActivites rapportActivites = rapportActivitesDAO.get(pId);

        if (null == rapportActivites) {
            throw new Exception("Identifiant n°" + pId + " inconnu pour les rapport d'activités");
        }

        return rapportActivites.getCollaborateur().toJson();
    }

    @Override
    public List<Collaborator> findAllByStatutOrderByNom(String pStatut) {

        Objects.requireNonNull(pStatut);
        StatutCollaborateur statut = statutCollaborateurDAO.findUniqEntiteByProp(
                StatutCollaborateur.PROP_CODE, pStatut
        );

        Objects.requireNonNull(statut);
        List<Collaborator> listCollabs = getDAO().findListEntitesByProp(
                Collaborator.PROP_STATUT,
                statut,
                Collaborator.PROP_NOM
        );

        if (listCollabs.isEmpty()) {
            return listCollabs;
        }

        return listCollabs.stream().filter(Collaborator::isEnabled).collect(Collectors.toList());
    }

    /**
     * {@linkplain CollaboratorService#findAllByStatutOrderByNomAsJson(String)}
     */
    @Override
    public List<CollaboratorAsJson> findAllByStatutOrderByNomAsJson(String pStatut) throws Exception {
        //Recuperation d'un statut
        StatutCollaborateur statut =
                statutCollaborateurDAO.findUniqEntiteByProp(
                        StatutCollaborateur.PROP_CODE,
                        pStatut
                );

        if (null == statut) {
            throw new Exception("Statut (" + pStatut + ") inconnu.");
        }

        //Recuperation de la liste des managers triés par nom
        List<Collaborator> listManagers =
                this.getDAO().findListEntitesByProp(
                        Collaborator.PROP_STATUT,
                        statut,
                        Collaborator.PROP_NOM
                );

        List<CollaboratorAsJson> tmpListManagerAsJson = new ArrayList<>();

        for (Collaborator tmpManager : listManagers) {
            if (tmpManager.isEnabled()) {
                tmpListManagerAsJson.add(tmpManager.toJson());
            }
        }

        return tmpListManagerAsJson;
    }

    /**
     * Verifie si le changement de statut est possible et, si oui, effectue l'operation
     *
     * @param pCollaborator le collaborateur à tester
     * @param pNewStatut     le statut
     * @return bool
     * @throws Exception exception
     */
    private Boolean checkAndChangeStatut(Collaborator pCollaborator, StatutCollaborateur pNewStatut) throws Exception {
        //Si il n'y a pas de changement de statut on fais rien
        if (null == pNewStatut || null == pCollaborator) {
            throw new Exception("Le statut ou le collaborateur ne peuvent pas être vide");
        }

        if (pCollaborator.getStatut() == pNewStatut) {
            return false;
        }

        //Statut actuel du collaborateur
        String oldStatut = pCollaborator.getStatut().getCode();
        //Nouveau statut qu'on doit lui attribuer
        String newStatut = pNewStatut.getCode();

        //cas manager/admin --> collaborateur/DRH
        if (oldStatut.equals(StatutCollaborateur.STATUT_MANAGER) ||
                oldStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

            if (newStatut.equals(StatutCollaborateur.STATUT_COLLABORATEUR) ||
                    newStatut.equals(StatutCollaborateur.STATUT_DRH)) {

                if (pCollaborator.getCollaborateurs().size() > 0) {
                    throw new Exception("Changement statut impossible: ce manager (" + pCollaborator.getMail() + ") est toujours lié à des collaborateurs. ");
                }

            }

        }
        //cas collaborateur/DRH --> Manager/administrateur
        if (oldStatut.equals(StatutCollaborateur.STATUT_COLLABORATEUR) ||
                oldStatut.equals(StatutCollaborateur.STATUT_DRH)) {

            if (newStatut.equals(StatutCollaborateur.STATUT_MANAGER) ||
                    newStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

                //On récupère et supprime le collaborateur dans la liste de collaborateurs managés
                Collaborator manager = pCollaborator.getManager();

                //On annule le lien collaborateur --> manager
                if (null != manager) {
                    manager.getCollaborateurs().remove(pCollaborator);
                }

                pCollaborator.setManager(null);

            }

        }
        //cas admin --> Autre
        if (oldStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

            if (!newStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

                StatutCollaborateur statutAdmin =
                        statutCollaborateurDAO.findUniqEntiteByProp(
                                StatutCollaborateur.PROP_CODE,
                                StatutCollaborateur.STATUT_ADMIN);

                List<Collaborator> listAdmin =
                        this.getDAO().findListEntitesByProp(
                                Collaborator.PROP_STATUT,
                                statutAdmin);

                if (listAdmin.size() < 2) {
                    throw new Exception(
                            "Changement statut impossible : il doit y avoir au minimum un adiminstrateur dans la base de données"
                    );
                }

            }
        }

        //Le changement de statut est possible
        pCollaborator.setStatut(pNewStatut);

        return true;
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderWithDisabledByNomAscAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllOrderWithDisabledByNomAscAsJson() {

        List<Collaborator> tmpListCollaborator = collaboratorDAO.findCollaborators(true, true);
        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<>(tmpListCollaborator.size());

        for (Collaborator tmpCollaborator : tmpListCollaborator) {

            tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());

        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collaborator findById(Long aIdCollaborator) {
        return collaboratorDAO.get(aIdCollaborator);
    }

    @Override
    public List<ModuleAddvise> getAllModuleAddvise() {
        List<ModuleAddvise> listeModule = moduleAddviseDAO.findAll();
        listeModule.sort((module1, module2) -> {
            String[] codeM1 = module1.getCodeModuleAddvise().split("M");
            String[] codeM2 = module2.getCodeModuleAddvise().split("M");
            if (codeM1.length > 1 && codeM2.length > 1) {
                if (Integer.parseInt(codeM1[1]) > Integer.parseInt(codeM2[1])) {
                    return 5;
                } else if (Integer.parseInt(codeM1[1]) < Integer.parseInt(codeM2[1])) {
                    return -5;
                } else {
                    return 0;
                }
            } else {
                return module1.getCodeModuleAddvise().compareTo(module2.getCodeModuleAddvise());
            }
        });
        return listeModule;
    }

    @Override
    public boolean isCadre(Collaborator collaborator) {
        return "C".equals(collaborator.getStatutGeneral());
    }

    /**
     * {@linkplain CollaboratorService#findAllManagersAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllManagersAsJson() {
        List<CollaboratorAsJson> managersAsJsons = new ArrayList<>();
        StatutCollaborateur statutManager = statutCollaborateurDAO.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_MANAGER);
        Poste posteManager = postService.getPosteByCode(Poste.POSTE_MANAGER);
        List<Collaborator> managers = collaboratorDAO.findAllManagers(statutManager, posteManager);

        for (Collaborator collaborateur : managers) {
            if (collaborateur.isEnabled()) {
                managersAsJsons.add(collaborateur.toJson());
            }
        }

        return managersAsJsons;
    }

    /**
     * {@linkplain CollaboratorService#findAllDirectionAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllDirectionAsJson() {
        List<CollaboratorAsJson> directorsAsJsons = new ArrayList<>();
        StatutCollaborateur statutManager = statutCollaborateurDAO.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        Poste posteManager = postService.getPosteByCode(Poste.POSTE_DIRECTEUR);
        List<Collaborator> managers = collaboratorDAO.findAllManagers(statutManager, posteManager);

        for (Collaborator collaborateur : managers) {
            if (collaborateur.isEnabled()) {
                directorsAsJsons.add(collaborateur.toJson());
            }
        }

        return directorsAsJsons;
    }

    /**
     * {@linkplain CollaboratorService#findAllResponsablesTechniqueAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllResponsablesTechniqueAsJson() {
        List<CollaboratorAsJson> responsablesTechniqueAsJsons = new ArrayList<>();
        StatutCollaborateur statutResponsableTechnique = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_RESPONSABLE_TECHNIQUE);
        List<Collaborator> responsablesTechnique = collaboratorDAO.findAllResponsablesTechnique(statutResponsableTechnique);

        for (Collaborator collaborateur : responsablesTechnique) {
            if (collaborateur.isEnabled()) {
                responsablesTechniqueAsJsons.add(collaborateur.toJson());
            }
        }

        return responsablesTechniqueAsJsons;
    }

    /**
     * {@linkplain CollaboratorService#findAllChefDeProjetAsJson()}
     */
    @Override
    public List<CollaboratorAsJson> findAllChefDeProjetAsJson() {
        List<CollaboratorAsJson> chefDeProjetAsJson = new ArrayList<>();
        StatutCollaborateur statutChefDeProjet = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_CHEF_DE_PROJET);
        List<Collaborator> chefDeProjet = collaboratorDAO.findAllChefDeProjet(statutChefDeProjet);

        for (Collaborator collaborateur : chefDeProjet) {
            if (collaborateur.isEnabled()) {
                chefDeProjetAsJson.add(collaborateur.toJson());
            }
        }
        return chefDeProjetAsJson;
    }

    /**
     * {@linkplain CollaboratorService#isManager(Collaborator)}
     */
    @Override
    public boolean isManager(Collaborator currentUser) {
        StatutCollaborateur statutManager = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_MANAGER);
        return statutManager.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaboratorService#isResponsableTechnique(Collaborator)}
     */
    @Override
    public boolean isResponsableTechnique(Collaborator currentUser) {
        StatutCollaborateur statutResponsableTechnique = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_RESPONSABLE_TECHNIQUE);
        return statutResponsableTechnique.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaboratorService#isChefDeProjet(Collaborator)}
     */
    @Override
    public boolean isChefDeProjet(Collaborator currentUser) {
        StatutCollaborateur statutChefDeProjet = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_CHEF_DE_PROJET);
        return statutChefDeProjet.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaboratorService#findAllOrderByNomAscAsJsonWithoutADMINAndDir()}
     */
    @Override
    public List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMINAndDir() {

        List<Collaborator> tmpListCollaborator = findAllOrderByNomAscWithoutADMIN();
        List<CollaboratorAsJson> tmpListCollaboratorAsJson = new ArrayList<>();

        for (Collaborator tmpCollaborator : tmpListCollaborator) {
            if (!isDirecteur(tmpCollaborator) && !isAdminManager(tmpCollaborator))
                tmpListCollaboratorAsJson.add(tmpCollaborator.toJson());
        }
        return tmpListCollaboratorAsJson;
    }

    /**
     * {@linkplain CollaboratorService#isDirecteur(Collaborator)}
     */
    @Override
    public boolean isDirecteur(Collaborator currentUser) {
        StatutCollaborateur statutDirecteur = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        return statutDirecteur.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaboratorService#isDRH(Collaborator)}
     */
    @Override
    public boolean isDRH(Collaborator currentUser) {
        StatutCollaborateur statutDRH = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        return statutDRH.equals(currentUser.getStatut());
    }

    /**
     * Vérifie si le collaborateur est l'Admin Manager en fonction de son nom et prénom
     *
     * @param collaborateur collaborateur à tester
     * @return true or false
     */
    private boolean isAdminManager(Collaborator collaborateur) {
        return "MANAGER".equalsIgnoreCase(collaborateur.getNom()) && "ADMIN".equalsIgnoreCase(collaborateur.getPrenom());
    }

    @Override
    public boolean hasMissionById(Collaborator currentUser, Long missionId) {

        List<Long> missionIds = currentUser
                .getMissions().stream().map(Mission::getId).collect(Collectors.toList());

        return missionIds.contains(missionId);
    }

    private List<CollaboratorAsJson> convertCollaboratorListToAsJsonList(List<Collaborator> listCollaborators) {
        List<CollaboratorAsJson> listCollaboratorsAsJson = new ArrayList<>();
        for (Collaborator collaborator : listCollaborators) {
            listCollaboratorsAsJson.add(collaborator.toJson());
        }

        return listCollaboratorsAsJson;
    }


    /**
     * creation de la liste des manager
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    @Override
    public List<CollaboratorAsJson> addListManagers() {
        List<CollaboratorAsJson> listeManagers = new ArrayList<>();
        try {
            listeManagers.addAll(findAllManagersAsJson());
            listeManagers.addAll(findAllDirectionAsJson());
            listeManagers.addAll(findAllResponsablesTechniqueAsJson());
            listeManagers.addAll(findAllChefDeProjetAsJson());
        } catch (Exception e) {
            logger.error("[add managers]", e);
        }

        return listeManagers;
    }

    /**
     * {@linkplain CollaboratorService#findCollaboratorsByStatus(StatutCollaborateur...)}
     */
    @Override
    public List<Collaborator> findCollaboratorsByStatus(StatutCollaborateur... collaboratorsStatus) {
        return findBySearchCriterias(
                Collaborator.PROP_NOM,
                true,
                new SearchCriteria<>(Collaborator.PROP_STATUT, IN, collaboratorsStatus),
                new SearchCriteria<>(Collaborator.PROP_ENABLED, EQUAL, true)
        );
    }

    /**
     * {@linkplain CollaboratorService#findAllByStatusCode(String...)}
     */
    @Override
    public List<Collaborator> findAllByStatusCode(String... collaboratorsStatusCode) {
        List<StatutCollaborateur> statuses = statutCollaborateurService
                .findCollaboratorsStatusByCodes(collaboratorsStatusCode);
        return findCollaboratorsByStatus(statuses.toArray(new StatutCollaborateur[0]));
    }

    /**
     * {@linkplain CollaboratorService#isSubcontractor(Collaborator)}
     */
    @Override
    public boolean isSubcontractor(Collaborator currentUser) {
        return currentUser.getStatut().getCode().equals(STATUT_SOUS_TRAITANT);
    }

    /**
     * {@linkplain CollaboratorService#changeCollaboratorActivation(Long)}
     */
    @Override
    public boolean changeCollaboratorActivation(Long collaboratorId) {

        if (null == collaboratorId) {
            return false;
        }

        Session session = collaboratorDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Collaborator collaborator = collaboratorDAO.get(collaboratorId);
        collaborator.setEnabled(!collaborator.isEnabled());
        session.saveOrUpdate(collaborator);

        session.flush();
        transaction.commit();

        return true;
    }

    /**
     * Retreive the list of collaborators by their status and activated or not
     * {@linkplain CollaboratorService#retrieveCollaboratorByStatus(boolean, StatutCollaborateur...)}
     */
    @Override
    public List<Collaborator> retrieveCollaboratorByStatus(boolean enabled, StatutCollaborateur... status) {
        return collaboratorDAO.findCollaboratorByStatus(enabled, status);
    }
}
