/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.PerimetreMission;

/**
 * Created by sbenslimane on 22/12/2016.
 */
public class PerimetreMissionAsJson {

    private Long id;
    private String codePerimetre;
    private String nomPerimetre;

    /**
     * Instantiates a new Perimetre mission as json.
     */
    public PerimetreMissionAsJson() {
    }

    /**
     * Instantiates a new Perimetre mission as json.
     *
     * @param perimetre the perimetre
     */
    public PerimetreMissionAsJson(PerimetreMission perimetre) {
        this.setId(perimetre.getId());
        this.setCodePerimetre(perimetre.getCodePerimetre());
        this.setNomPerimetre(perimetre.getNomPerimetre());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets code perimetre.
     *
     * @return the codePerimetre
     */
    public String getCodePerimetre() {
        return codePerimetre;
    }

    /**
     * Sets code perimetre.
     *
     * @param codePerimetre the id to set
     */
    public void setCodePerimetre(String codePerimetre) {
        this.codePerimetre = codePerimetre;
    }

    /**
     * Gets nom perimetre.
     *
     * @return the nomPerimetre
     */
    public String getNomPerimetre() {
        return nomPerimetre;
    }

    /**
     * Sets nom perimetre.
     *
     * @param nomPerimetre the id to set
     */
    public void setNomPerimetre(String nomPerimetre) {
        this.nomPerimetre = nomPerimetre;
    }

}
