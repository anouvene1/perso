/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Type absence.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_type_absence")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class TypeAbsence implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPEABSENCE.
     */
    public static final String PROP_TYPEABSENCE = "type_absence";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant TYPE_AA.
     */
    public static final String TYPE_AA = "AA";
    /**
     * The constant TYPE_AE.
     */
    public static final String TYPE_AE = "AE";
    /**
     * The constant TYPE_CP.
     */
    public static final String TYPE_CP = "CP";
    /**
     * The constant TYPE_CS.
     */
    public static final String TYPE_CS = "CS";
    /**
     * The constant TYPE_RTT.
     */
    public static final String TYPE_RTT = "RTT";
    /**
     * The constant TYPE_PAT.
     */
    public static final String TYPE_PAT = "PAT";
    /**
     * The constant TYPE_MA.
     */
    public static final String TYPE_MA = "MA";
    /**
     * The constant TYPE_MT.
     */
    public static final String TYPE_MT = "MT";
    /**
     * The constant TYPE_CN.
     */
    public static final String TYPE_CN = "CN";
    /**
     * The constant TYPE_FC.
     */
    public static final String TYPE_FC = "FC";
    private static final long serialVersionUID = -1197857111510107844L;
    private Long id;
    private String typeAbsence;
    private String code;
    private int etat;

    private Set<Absence> absences;

    /**
     * Instantiates a new Type absence.
     *
     * @param pTypeAbsence the p type absence
     * @param pCode        the p code
     * @param pEtat        the p etat
     * @param pAbsences    the p absences
     */
    public TypeAbsence(String pTypeAbsence, String pCode, int pEtat, Set<Absence> pAbsences) {
        super();
        this.typeAbsence = pTypeAbsence;
        code = pCode;
        etat = pEtat;
        this.absences = pAbsences;
    }

    /**
     * Instantiates a new Type absence.
     */
    public TypeAbsence() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets type absence.
     *
     * @return the type absence
     */
    @Column(name = "type_absence", nullable = false)
    public String getTypeAbsence() {
        return typeAbsence;
    }

    /**
     * Sets type absence.
     *
     * @param pTypeAbsence the p type absence
     */
    public void setTypeAbsence(String pTypeAbsence) {
        this.typeAbsence = pTypeAbsence;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets etat.
     *
     * @return etat etat
     */
    @Column(name = "etat", nullable = false, columnDefinition = "TINYINT")
    public int getEtat() {
        return etat;
    }

    /**
     * Récupère un int, 0 inactif, 1 actif
     *
     * @param pEtat the etat to set
     */
    public void setEtat(int pEtat) {
        this.etat = pEtat;
    }

    /**
     * Gets absences.
     *
     * @return la liste des TimesheetLines avec cette absence
     */
    @OneToMany(mappedBy = "typeAbsence")
    public Set<Absence> getAbsences() {
        return absences;
    }

    /**
     * Sets absences.
     *
     * @param pAbsences the p absences
     */
    public void setAbsences(Set<Absence> pAbsences) {
        this.absences = pAbsences;
    }

}
