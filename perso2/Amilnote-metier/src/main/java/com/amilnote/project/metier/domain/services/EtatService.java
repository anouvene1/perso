/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Etat;

import java.util.List;

/**
 * The interface Etat service.
 */
public interface EtatService extends AbstractService<Etat> {

    /**
     * Retourne tous les Types de frais
     *
     * @return List all etat
     */
    List<Etat> getAllEtat();

    /**
     * Retourne un type de frais en fonction de l'id
     *
     * @param pId the p id
     * @return Etat etat by id
     */
    Etat getEtatById(int pId);

    /**
     * Retourne tous les états au format json
     *
     * @return String (Liste d'état)
     */
    String getAllEtatAsJson();

    /**
     * Retourne la liste des états pour le domaine choisi (frais, absence...)
     *
     * @param pListeEtat the p liste etat
     * @return Liste etat domaine
     */
    List<Etat> getEtatDomaine(String[] pListeEtat);

    /**
     * Retourne la liste des états pour le domaine choisi (frais, absence...) au format JSON
     *
     * @param pListeEtat the p liste etat
     * @return String (Liste d'état)
     */
    String getEtatDomaineAsJson(String[] pListeEtat);

    /**
     * Retourne l'état correspondant au code souhaité
     *
     * @param pEtat Code de l'état souhaité
     * @return Entité Etat lié  au code
     */
    Etat getEtatByCode(String pEtat);
}
