/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ParametreAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Parametre.
 */
@Entity
@Table(name = "ami_params")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Parametre implements java.io.Serializable {

    /**
     * The constant PROP_ID_PARAMETRE.
     */
    public static final String PROP_ID_PARAMETRE = "id";
    /**
     * The constant PROP_LIBELLE_PARAMETRE.
     */
    public static final String PROP_LIBELLE_PARAMETRE = "libelle";
    /**
     * The constant PROP_VALEUR_PARAMETRE.
     */
    public static final String PROP_VALEUR_PARAMETRE = "valeur";
    private static final long serialVersionUID = -329072377657745936L;
    private int id;
    private String libelle;
    private String valeur;


	/* CONSTRUCTORS */

    /**
     * Instantiates a new Parametre.
     */
    public Parametre() {
        super();
    }

    /**
     * Instantiates a new Parametre.
     *
     * @param pLibelle the p libelle
     * @param pValeur  the p valeur
     */
    public Parametre(String pLibelle, String pValeur) {
        super();
        libelle = pLibelle;
        valeur = pValeur;
    }

    /**
     * To json parametre as json.
     *
     * @return the parametre as json
     */
    public ParametreAsJson toJson() {
        return new ParametreAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets libelle.
     *
     * @return the libelle
     */
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle.
     *
     * @param lib the lib
     */
    public void setLibelle(String lib) {
        this.libelle = lib;
    }

    /**
     * Gets valeur.
     *
     * @return the valeur
     */
    @Column(name = "valeur")
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets valeur.
     *
     * @param val the val
     */
    public void setValeur(String val) {
        this.valeur = val;
    }

}
