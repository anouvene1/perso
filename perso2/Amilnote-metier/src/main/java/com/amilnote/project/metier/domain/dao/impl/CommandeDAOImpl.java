/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.CommandeDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.ElementFactureService;
import com.amilnote.project.metier.domain.services.EtatService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The type Commande dao.
 */
@Repository("commandeDAO")
public class CommandeDAOImpl extends AbstractDAOImpl<Commande> implements CommandeDAO {

    private static final Logger logger = LogManager.getLogger(CommandeDAOImpl.class);

    @Autowired
    private MissionDAOImpl missionDAO;

    @Autowired
    private FactureDAOImpl factureDAO;

    @Autowired
    private EtatService etatService;

    @Autowired
    private ElementFactureService elementService;

    @Override
    protected Class<Commande> getReferenceClass() {
        return Commande.class;
    }

    @Override

    /**
     * _________________________ [JNA][AMNOTE 165] _________________________
     * DANS CETTE FONCTION ON TROUVE LA CREATION ET MODIFICATION D'UNE COMMANDE
     * ON TROUVE EGALEMENT LA CREATION DE LA FACTURE LIEE A CETTE COMMANDE
     * ON CREE LA FACTURE SEULEMENT DANS LE CAS D'UNE CREATION DE COMMANDE
     * CAR SI ON MET JUSTE A JOUR UNE COMMANDE EXISTANTE, LA FACTURE EXISTE DEJA
     *
     *  {@linkplain CommandeDAO#createOrUpdateCommande(CommandeAsJson, MissionAsJson)}
     */
    public String createOrUpdateCommande(CommandeAsJson commandeJson, MissionAsJson missionJson) throws IOException {
        String msgRetour = "";
        //[JNA][AMNOTE 141] Création d'une facture à partir d'une commande
        FactureAsJson factureJson = new FactureAsJson(); // On crée une nouvelle facture
        //[JNA][AMNOTE 166] On met l'état de la facture à "généré" par défaut
        Etat etat = etatService.getEtatByCode("GN");
        factureJson.setEtat(etat);
        // [JNA][AMNOTE 142] On prérempli la facture avec les élément de la commande
        if (missionJson.getClient() != null) {
            factureJson.setIdClient(missionJson.getClient().getId().intValue()); // on ajoute à la facture l'id du client
        } else {
            factureJson.setIdClient(0); // on ajoute à la facture l'id du client
        }
        String mode;
        if (missionJson != null) {
            // --- Récupération de la mission
            Mission mission = missionDAO.get(missionJson.getId());
            Commande commande = new Commande();
            if (null != commandeJson.getId()) { // Si l'ID n'est pas null on récupère la commande
                commande = findUniqEntiteByProp(Commande.PROP_ID, commandeJson.getId());
                mode = "update";
            } else {
                mode = "insert";
            }
            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            // --- Ajout des informations concernant la commande
            commande.setMission(mission);
            if (commandeJson.getDateDebut() != null) {
                commande.setDateDebut(commandeJson.getDateDebut());
            } else {
                commande.setDateDebut(null);
            }
            if (commandeJson.getDateFin() != null) {
                commande.setDateFin(commandeJson.getDateFin());
            } else {
                commande.setDateFin(null);
            }
            if (commandeJson.getNbJours() != null) {
                commande.setNbJours(commandeJson.getNbJours());
            }
            commande.setNumero(commandeJson.getNumero());
            // --- Test sur l'objet commandeJson pour remplir l'objet commande qui lui rempli la BD
            if (commandeJson.getMontant() != 0) {
                //[JNA][AMNOTE 150] Si le montant a changé
                if (commande.getMontant() != null) {
                    if (!String.valueOf(commandeJson.getMontant()).equals(String.valueOf(commande.getMontant()))) {
                        // On met à jour le montant
                        commande.setMontant(commandeJson.getMontant());
                        // Et on met à jour le budget avec le nouveau montant
                        commande.setBudget(commandeJson.getMontant());
                    }
                } else {
                    commande.setMontant(commandeJson.getMontant());
                }

            } else {
                commande.setMontant((float) (0.0));
            }
            // --- Test sur l'objet commandeJson pour remplir l'objet commande qui lui rempli la BD
            if (commandeJson.getCommentaire() != null) {
                commande.setCommentaire(commandeJson.getCommentaire());
            } else {
                commande.setCommentaire(null);
            }
            // --- [JNA][AMNOTE 142] Test si justificatif commande
            if (commandeJson.getJustificatif() != null) {
                commande.setJustificatif(commandeJson.getJustificatif());
            } else {
                commande.setJustificatif(null);
            }
            // --- [JNA][AMNOTE 160] Sauvegarde moyen de paiement
            if (commandeJson.getMoyensPaiement() != null) {
                commande.setMoyensPaiement(commandeJson.getMoyensPaiement());
            } else {
                commande.setMoyensPaiement(null);
            }
            if (commandeJson.getEtat() != null) {
                commande.setEtat(commandeJson.getEtat());
            } else {
                Etat etatCommande = etatService.getEtatByCode("CO");
                commande.setEtat(etatCommande);
            }
            // --- [JNA][AMNOTE 160] Sauvegarde moyen de paiement
            if (commandeJson.getConditionPaiement() != null) {
                commande.setConditionPaiement(commandeJson.getConditionPaiement());
            } else {
                commande.setConditionPaiement(null);
            }
            if (commandeJson.getClient() != null) {
                commande.setClient(commandeJson.getClient());
            } else {
                commande.setClient(null);
            }
            // [JNA][AMNOTE 150] Si la buget est vide ou à 0 on l'initialise avec le montant total
            if (commande.getBudget() == null || commande.getBudget() == 0) {
                commande.setBudget(commande.getMontant());
            } else if (commandeJson.getBudget() != null && commandeJson.getBudget() != 0) {
                commande.setBudget(commandeJson.getBudget());
            }
            // [AMNT-598] Sauvegarde du Reponsable Facturation
            if (commandeJson.getIdResponsableFacturation() != null) {
                commande.setIdResponsableFacturation(commandeJson.getIdResponsableFacturation());
            } else {
                commande.setIdResponsableFacturation(null);
            }


            // --- Sauvegarde et Commit de la mise à jour
            session.save(commande);
            session.flush();
            transaction.commit();
            commandeJson.setId(commande.getId());
            if (mode.equals("insert")) {
                FactureAsJson factureAsJson = new FactureAsJson();
                factureDAO.createOrUpdateFactureAVANTREFONTE(factureAsJson, commande, false);
            }
            msgRetour = "ok";
        } else {
            msgRetour = "error";
        }
        return msgRetour;
    }

    /**
     * {@linkplain CommandeDAO#deleteCommande(Commande)}
     */
    @Override
    public String deleteCommande(Commande commande) {
        // SI la commande n'est pas null
        if (commande != null) {

            //[JNA][AMNOTE 165] On récupère la liste des factures de cette commande
            List<Facture> listeFactureDeLaCommande = factureDAO.findByCommande(commande);
            // Pour la liste de facture
            for (Facture facture : listeFactureDeLaCommande) {
                // On récupère la liste des élement de la facture
                List<ElementFacture> listeElementsDeLaFacture = elementService.findByFacture(facture);
                // Si la liste n'est pas vide
                if (!listeElementsDeLaFacture.isEmpty()) {
                    // On boucle sur le liste des élements de cette facture
                    for (ElementFacture elem : listeElementsDeLaFacture) {
                        // On supprime les éléments de cette facture
                        elementService.deleteElementFacture(elem);
                    }
                }
                // On supprime la facture
                factureDAO.deleteFacture(facture);
            }
            // On peut maintenant supprimer la commande
            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            Facture facture = new Facture();

            // --- Sauvegarde et Commit de la mise à jour
            session.delete(commande);

            if (facture.getId() != 0) { // Si on a récupéré une facture
                session.delete(facture); // On supprime cette facture
            }
            session.flush();
            transaction.commit();
            return "Suppression effectuée avec succès";
        }
        return "Aucune suppression n'a été effectuée";
    }

    /**
     * {@linkplain CommandeDAO#findCommandes()}
     */
    @Override
    public List<Commande> findCommandes() {
        return getCriteria().list();
    }

    /**
     * {@linkplain CommandeDAO#findActivesCommandes()}
     */
    @Override
    public List<Commande> findActivesCommandes() {
        List<Commande> commandes = new ArrayList<>();
        Criteria criteria = getCriteria();
        Etat etatCommence = etatService.getEtatByCode(Etat.ETAT_COMMENCE);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date dateFinDeMois = calendar.getTime();
        Criterion criterion = Restrictions.or(
                Restrictions.eq(Commande.PROP_ETAT, etatCommence), Restrictions.ge(Commande.PROP_DATEFIN, dateFinDeMois));
        criteria.add(criterion);
        commandes.addAll(criteria.list());
        return commandes;
    }

    /**
     * {@linkplain CommandeDAO#findCommandesForMonth(DateTime, DateTime, Etat)}
     */
    @Override
    public List<Commande> findCommandesForMonth(DateTime debut, DateTime fin, Etat etat) {
        Date dateDebut = new Date(debut.getMillis());
        Date dateFin = new Date(fin.getMillis());
        Criteria criteria = getCriteria();
        Criterion criterion = Restrictions.and(Restrictions.eq(Commande.PROP_ETAT, etat), Restrictions.between(Commande.PROP_DATEDEBUT, dateDebut, dateFin));
        criteria.add(criterion);

        criteria.addOrder(Order.desc(Commande.PROP_DATEFIN));

        return criteria.list();
    }

    /**
     * {@linkplain CommandeDAO#findStartedCommandes(Etat)}
     */
    @Override
    public List<Commande> findStartedCommandes(Etat etatCommencee) {
        Criteria criteria = getCriteria();
        Criterion criterion = Restrictions.eq(Commande.PROP_ETAT, etatCommencee);
        criteria.add(criterion);

        criteria.addOrder(Order.desc(Commande.PROP_DATEFIN));

        return criteria.list();
    }

    /**
     * {@linkplain CommandeDAO#findStoppedCommandes(Etat)}
     */
    @Override
    public List<Commande> findStoppedCommandes(Etat etatTerminee) {
        Criteria criteria = getCriteria();
        Criterion criterion = Restrictions.eq(Commande.PROP_ETAT, etatTerminee);
        criteria.add(criterion);

        criteria.addOrder(Order.desc(Commande.PROP_DATEFIN));

        return criteria.list();
    }

    /**
     * {@linkplain CommandeDAO#findByIdMissionByMonth(Mission, DateTime)}
     */
    @Override
    public List<Commande> findByIdMissionByMonth(Mission mission, DateTime dateVoulue) {
        Criteria criteria = getCriteria();

        Etat etatTermine = etatService.getEtatByCode(Etat.ETAT_TERMINE);
        criteria.add(Restrictions.ne(Commande.PROP_ETAT, etatTermine));

        criteria.add(Restrictions.eq(Commande.PROP_MISSION, mission));

        DateTime lDebutPeriode = dateVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = dateVoulue.dayOfMonth().withMaximumValue();
        criteria.add(Restrictions.ge(Commande.PROP_DATEFIN, lDebutPeriode.toDate()));
        criteria.add(Restrictions.le(Commande.PROP_DATEDEBUT, lFinPeriode.toDate()));

        return (List<Commande>) criteria.list();
    }

    /**
     * {@linkplain CommandeDAO#findByIdMission(Mission)}
     */
    @Override
    public List<Commande> findByIdMission(Mission mission) {
        Criteria criteria = getCriteria();

        criteria.add(Restrictions.eq(Commande.PROP_MISSION, mission));

        return (List<Commande>) criteria.list();
    }
}
