/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Link evenement timesheet as json.
 */
public class LinkEvenementTimesheetAsJson {
    private Long id = null;

    private MissionAsJson mission;

    private AbsenceAsJson absence;

    private int momentJournee;

    private Date date;

    private Date start;

    private Date end;

    private List<FraisAsJson> listFraisAsJson = new ArrayList<>();

    /**
     * Instantiates a new Link evenement timesheet as json.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     */
    public LinkEvenementTimesheetAsJson(LinkEvenementTimesheet pLinkEvenementTimesheet) {
        id = pLinkEvenementTimesheet.getId();
        //On récupère la mission sous la forme d'un Json.
        if (null != pLinkEvenementTimesheet.getMission()) {
            mission = pLinkEvenementTimesheet.getMission().toJson();
        }

        if (null != pLinkEvenementTimesheet.getAbsence()) {
            absence = pLinkEvenementTimesheet.getAbsence().toJson();
        }
        momentJournee = pLinkEvenementTimesheet.getMomentJournee();
        date = pLinkEvenementTimesheet.getDate();
        start = pLinkEvenementTimesheet.calculDateStart().toDate();
        end = pLinkEvenementTimesheet.calculDateEnd().toDate();
    }

    /**
     * Instantiates a new Link evenement timesheet as json.
     */
    public LinkEvenementTimesheetAsJson() {
    }

    /**
     * Construct for frais.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     */
    public void constructForFrais(LinkEvenementTimesheet pLinkEvenementTimesheet) {
        id = pLinkEvenementTimesheet.getId();
        //On récupère la mission sous la forme d'un Json.
        setAbsence(((pLinkEvenementTimesheet.getAbsence() != null) ?
            new AbsenceAsJson(pLinkEvenementTimesheet.getAbsence())
            : null));
        momentJournee = pLinkEvenementTimesheet.getMomentJournee();
        date = pLinkEvenementTimesheet.getDate();

        this.setListFraisAsJson(pLinkEvenementTimesheet.getListFrais());

    }

    /**
     * Constructor for holidays link evenement timesheet as json.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     * @return the link evenement timesheet as json
     */
    public LinkEvenementTimesheetAsJson ConstructorForHolidays(LinkEvenementTimesheet pLinkEvenementTimesheet) {

        this.setStart(pLinkEvenementTimesheet.calculDateStart().toDate());
        this.setEnd(pLinkEvenementTimesheet.calculDateEnd().toDate());

        return this;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public MissionAsJson getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(MissionAsJson pMission) {
        mission = pMission;
    }


    /**
     * Gets moment journee.
     *
     * @return the moment journee
     */
    public int getMomentJournee() {
        return momentJournee;
    }

    /**
     * Sets moment journee.
     *
     * @param pMomentJournee the p moment journee
     */
    public void setMomentJournee(int pMomentJournee) {
        momentJournee = pMomentJournee;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param pDate the p date
     */
    public void setDate(Date pDate) {
        date = pDate;
    }

    /**
     * Gets start.
     *
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * Sets start.
     *
     * @param pStart the start to set
     */
    public void setStart(Date pStart) {
        start = pStart;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public Date getEnd() {
        return end;
    }

    /**
     * Sets end.
     *
     * @param pEnd the end to set
     */
    public void setEnd(Date pEnd) {
        end = pEnd;
    }

    /**
     * Gets list frais as json.
     *
     * @return the listFraisAsJson
     */
    public List<FraisAsJson> getListFraisAsJson() {
        return listFraisAsJson;
    }

    /**
     * Sets list frais as json.
     *
     * @param pListFrais the p list frais
     */
    public void setListFraisAsJson(List<Frais> pListFrais) {

        for (Frais frais : pListFrais) {

            FraisAsJson tmp = new FraisAsJson(frais);
            this.getListFraisAsJson().add(tmp);

        }
    }

    /**
     * Gets absence.
     *
     * @return the absence
     */
    public AbsenceAsJson getAbsence() {
        return absence;
    }

    /**
     * Sets absence.
     *
     * @param pAbsence the absence to set
     */
    public void setAbsence(AbsenceAsJson pAbsence) {
        absence = pAbsence;
    }


}
