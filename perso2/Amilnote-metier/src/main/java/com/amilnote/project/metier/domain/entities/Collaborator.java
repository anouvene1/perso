/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Collaborateur.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_collaborateur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//TODO will: tester la serialisation/deserialisation json avec jackson
public class Collaborator extends AbstractTravailleur<CollaboratorAsJson> {

    /**
     * The constant PROP_NOM.
     */
    public static final String PROP_NOM = "nom";

    /**
     * The constant PROP_PRENOM.
     */
    public static final String PROP_PRENOM = "prenom";

    /**
     * The constant PROP_TEL.
     */
    public static final String PROP_TEL = "telephone";

    /**
     * The constant PROP_MAIL.
     */
    public static final String PROP_MAIL = "mail";

    /**
     * The constant PROP_PASSWORD.
     */
    public static final String PROP_PASSWORD = "password";

    /**
     * The constant PROP_STATUT.
     */
    public static final String PROP_STATUT = "statut";

    /**
     * The constant PROP_ENABLED.
     */
    public static final String PROP_ENABLED = "enabled";

    /**
     * The constant PROP_MANAGER.
     */
    public static final String PROP_MANAGER = "manager";

    /**
     * The constant PROP_POSTE.
     */
    public static final String PROP_POSTE = "poste";

    /**
     * The constant PROP_NONLOCKED.
     */
    public static final String PROP_NONLOCKED = "accountNonLocked";

    /**
     * The constant PROP_NBATTEMPTS.
     */
    public static final String PROP_NBATTEMPTS = "nbAttempts";

    /**
     * The constant PROP_ADRESS.
     */
    public static final String PROP_ADRESS = "adresse_postale";

    /**
     * The constant PROP_MAIL_PERSO.
     */
    public static final String PROP_MAIL_PERSO = "mail_perso";

    /**
     * The constant PROP_STATUT_GENERAL.
     */
    public static final String PROP_STATUT_GENERAL = "statut_general";

    /**
     * The constant PROP_RAISON_SOCIALE.
     */
    public static final String PROP_RAISON_SOCIALE = "raison_sociale";

    public static final String COLLABORATORS_FR_LOWER_CASE = "collaborateurs";

    public static final String SUBCONTRACTORS_FR_SNAKE_CASE = "sous_traitants";

    private static final long serialVersionUID = -1319603069615603284L;

    private Collaborator manager;

    private List<Justif> justificatifs = new ArrayList<>();

    private Date dateEntree;

    private Date dateSortie;

    private Date dateNaissance;

    private String mailPerso;

    private String statutGeneral;

    private List<AddviseSession> addviseSessions;

    private AddviseSession lastAddviseSession;

    private ModuleAddvise futureModule;

    private boolean exclusAddvise;

    private RaisonSociale raisonSociale;

    private Civilite civilite;

    private Agency agency;

    /**
     * Constructeur par défaut de la classe Collaborateur
     */
    public Collaborator() {
        super();
        addviseSessions = new ArrayList<>();
    }

    /**
     * Constructeur de la classe Collaborateur
     *
     * @param pNom              the p nom
     * @param pPrenom           the p prenom
     * @param pStatut           the p statut
     * @param pEnabled          the p enabled
     * @param pTel              the p tel
     * @param pMail             the p mail
     * @param pPassword         the p password
     * @param pMissions         the p missions
     * @param pManager          the p manager
     * @param pPoste            the p poste
     * @param pAccountNonLocked the p account non locked
     * @param pNbAttempts       the nb attempts
     * @param pDateEntree       the p date entree
     * @param pDateSortie       the p date sortie
     * @param pDateNaissance    the p date naissance
     * @param adressePostale    the adresse postale
     * @param mailPerso         the mail perso
     * @param statutGeneral     the statut general
     * @param pRaisonSociale    the p raison sociale
     * @param civilite          civilite
     * @param agency            agency
     */
    public Collaborator(
        String pNom,
        String pPrenom,
        StatutCollaborateur pStatut,
        Boolean pEnabled,
        String pTel,
        String pMail,
        String pPassword,
        List<Mission> pMissions,
        Collaborator pManager,
        Poste pPoste,
        Boolean pAccountNonLocked,
        int pNbAttempts,
        Date pDateEntree,
        Date pDateSortie,
        Date pDateNaissance,
        String adressePostale,
        String mailPerso,
        String statutGeneral,
        RaisonSociale pRaisonSociale,
        Civilite civilite,
        Agency agency) {

        super(pNom, pPrenom, pStatut, pMail, pPassword, pTel, pEnabled, pMissions, null, null, pPoste, pAccountNonLocked, pNbAttempts, adressePostale, civilite, agency);

        this.dateEntree = pDateEntree;
        this.dateSortie = pDateSortie;
        this.dateNaissance = pDateNaissance;
        this.mailPerso = mailPerso;
        this.statutGeneral = statutGeneral;
        this.manager = pManager;
        this.raisonSociale = pRaisonSociale;
    }

    /**
     * Gets raisonSociale.
     *
     * @return the raisonSociale
     */
    @OneToOne(mappedBy = "collaborator", fetch = FetchType.LAZY)
    public RaisonSociale getRaisonSociale() {
        return raisonSociale;
    }

    /**
     * Sets missions.
     *
     * @param pRaisonSociale the p raison sociale
     */
    public void setRaisonSociale(RaisonSociale pRaisonSociale) {
        raisonSociale = pRaisonSociale;
    }

    /**
     * Parse se Collaborateur au format json
     *
     * @return un collaborateur json
     */
    @Override
    public CollaboratorAsJson toJson() {
        return new CollaboratorAsJson(this);
    }

    /**
     * Gets justificatifs.
     *
     * @return the justificatifs
     */
    @OneToMany(fetch = FetchType.LAZY )
    @JoinColumn(name="id_collaborateur")
    public List<Justif> getJustificatifs() {
        return justificatifs;
    }

    /**
     * Sets justificatifs.
     *
     * @param pJustificatifs the p justificatifs
     */
    public void setJustificatifs(List<Justif> pJustificatifs) {
        justificatifs = pJustificatifs;
    }

    /**
     * Gets date naissance.
     *
     * @return date de naissance
     */
    @Column(name = "date_naissance")
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * to set date de naissance
     *
     * @param dateNaissance the date naissance
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Gets date sortie.
     *
     * @return dateSortie date sortie
     */
    @Column(name = "date_sortie")
    public Date getDateSortie() {
        return dateSortie;
    }

    /**
     * set dateSortie
     *
     * @param dateSortie the date sortie
     */
    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    /**
     * Gets date entree.
     *
     * @return date d'entree
     */
    @Column(name = "date_entree")
    public Date getDateEntree() {
        return dateEntree;
    }

    /**
     * set la date d'entree
     *
     * @param dateEntree the date entree
     */
    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    /**
     * Gets mail perso.
     *
     * @return l 'adresse postale
     */
    @Column(name = "mail_perso")
    public String getMailPerso() {
        return mailPerso;
    }

    /**
     * set la date d'entree
     *
     * @param mailPerso the mail perso
     */
    public void setMailPerso(String mailPerso) {
        this.mailPerso = mailPerso;
    }

    /**
     * Gets statut general.
     *
     * @return le statut general
     */
    @Column(name = "statut_general")
    public String getStatutGeneral() {
        return statutGeneral;
    }



    /**
     * set la date d'entree
     *
     * @param statutGeneral the statut general
     */
    public void setStatutGeneral(String statutGeneral) {
        this.statutGeneral = statutGeneral;
    }

    /**
     * Gets raisonSociale.
     *
     * @return the manager
     */
    @ManyToOne
    @JoinColumn(name = "id_manager", nullable = true)
    public Collaborator getManager() {
        return manager;
    }

    /**
     * Sets manager.
     *
     * @param pManager the manager to set
     */
    public void setManager(Collaborator pManager) {
        manager = pManager;
    }


    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "collaborateurs")
    public List<AddviseSession> getAddviseSessions() {
        return addviseSessions;
    }

    public void setAddviseSessions(List<AddviseSession> addviseSessions) {
        this.addviseSessions = addviseSessions;
    }

    public void addAddviseSession(AddviseSession session) {
        this.addviseSessions.add(session);
    }

    /**
     * Est exclus ou non du programme Addvise (par exemple : collaborateur parti loin)
     *
     * @return boolean
     */
    @Column(name = "exclus_addvise")
    public boolean getExclusAddvise() {
        return exclusAddvise;
    }

    public void setExclusAddvise(boolean exclusAddvise) {
        this.exclusAddvise = exclusAddvise;
    }

    @Transient
    public AddviseSession getLastAddviseSession() {
        return lastAddviseSession;
    }

    public void setLastAddviseSession(AddviseSession lastAddviseSession) {
        this.lastAddviseSession = lastAddviseSession;
    }

    @Transient
    public ModuleAddvise getFutureModule() {
        return futureModule;
    }

    public void setFutureModule(ModuleAddvise futureModule) {
        this.futureModule = futureModule;
    }

    @Transient
    public String toString() {
        return getNom().toUpperCase() + " " + getPrenom();
    }
}
