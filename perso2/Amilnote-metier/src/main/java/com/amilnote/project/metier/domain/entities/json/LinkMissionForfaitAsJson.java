/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkMissionForfait;


/**
 * The type Link mission forfait as json.
 */
public class LinkMissionForfaitAsJson {


    private Long id;

    private ForfaitAsJson forfait;

    private float montant;

    private String libelle;


    /**
     * Constructeur par defaut de la classe LinkMissionForfait
     */
    public LinkMissionForfaitAsJson() {
    }

    ;

    /**
     * Instantiates a new Link mission forfait as json.
     *
     * @param linkMissionForfait the link mission forfait
     */
    public LinkMissionForfaitAsJson(LinkMissionForfait linkMissionForfait) {
        super();
        setId(linkMissionForfait.getId());
        setForfait(new ForfaitAsJson(linkMissionForfait.getForfait()));
        setMontant(linkMissionForfait.getMontant());
        setLibelle(linkMissionForfait.getLibelle());
    }

    /**
     * Gets forfait.
     *
     * @return the forfait
     */
    public ForfaitAsJson getForfait() {
        return forfait;
    }

    /**
     * Sets forfait.
     *
     * @param pForfait the forfait to set
     */
    public void setForfait(ForfaitAsJson pForfait) {
        forfait = pForfait;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the montant to set
     */
    public void setMontant(float pMontant) {
        montant = pMontant;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets the libellé
     *
     * @return libellé
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle
     *
     * @param libelle the libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
