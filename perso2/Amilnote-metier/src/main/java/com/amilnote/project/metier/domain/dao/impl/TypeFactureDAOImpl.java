/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.TypeFactureDAO;
import com.amilnote.project.metier.domain.entities.TypeFacture;
import org.springframework.stereotype.Repository;

@Repository("typeFactureDAO")
public class TypeFactureDAOImpl extends AbstractDAOImpl<TypeFacture> implements TypeFactureDAO {

    @Override
    protected Class<TypeFacture> getReferenceClass() {
        return TypeFacture.class;
    }
}
