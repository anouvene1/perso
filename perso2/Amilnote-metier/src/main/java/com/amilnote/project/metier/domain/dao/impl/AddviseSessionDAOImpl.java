/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.AddviseSessionDAO;
import com.amilnote.project.metier.domain.entities.AddviseSession;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository("addviseSessionDAO")
public class AddviseSessionDAOImpl extends AbstractDAOImpl<AddviseSession> implements AddviseSessionDAO {

    @Override
    protected Class<AddviseSession> getReferenceClass() {
        return AddviseSession.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int createOrUpdateAddviseSession(AddviseSession addviseSession) {
        saveOrUpdate(addviseSession);
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AddviseSession findById(int id) {
        return (AddviseSession) currentSession().get(AddviseSession.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AddviseSession> findAllByEtatsAndDateOrderByDate(
        List<AddviseSession.EtatSession> etatsSession, Date dateDebut, Date dateFin) {
         Criteria criteria = currentSession().createCriteria(AddviseSession.class)
            .add(Restrictions.in("etat", etatsSession));
         if (dateDebut != null) {
            criteria.add(Restrictions.ge("date", dateDebut));
         }
         if (dateFin != null) {
             criteria.add(Restrictions.le("date", dateFin));
         }
        criteria.addOrder(Order.asc("date"));
        return (List<AddviseSession>) criteria.list();
    }
}
