/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.ami_factureDeCommande;

import java.sql.Date;

/**
 * The type Ami facture de commande as json.
 */
public class ami_factureDeCommandeAsJson {

    private int id_facture;
    private int num_facture;
    private String nom_societe;
    private String collaborateur;
    private String mission;
    private String numero;
    private float quantite;
    private float prix;
    private float montant;
    private String libelle;
    private Date mois_prestations;
    private int etat;

    /**
     * Instantiates a new Ami facture de commande as json.
     */
    public ami_factureDeCommandeAsJson() {
    }

    /**
     * Instantiates a new Ami facture de commande as json.
     *
     * @param facture the facture
     */
    public ami_factureDeCommandeAsJson(ami_factureDeCommande facture) {

        this.id_facture = facture.getId_facture();
        this.num_facture = facture.getNum_facture();
        this.nom_societe = facture.getNom_societe();
        this.collaborateur = facture.getCollaborateur();
        this.mission = facture.getMission();
        this.numero = facture.getNumero();
        this.quantite = facture.getQuantite();
        this.prix = facture.getPrix();
        this.montant = facture.getMontant();
        this.libelle = facture.getLibelle();
        this.mois_prestations = facture.getMois_prestations();
        this.etat = facture.getEtat();
    }

    /**
     * Gets id facture.
     *
     * @return the id facture
     */
    public int getId_facture() {
        return id_facture;
    }

    /**
     * Sets id facture.
     *
     * @param id_facture the id facture
     */
    public void setId_facture(int id_facture) {
        this.id_facture = id_facture;
    }

    /**
     * Gets num facture.
     *
     * @return the num facture
     */
    public int getNum_facture() {
        return num_facture;
    }

    /**
     * Sets num facture.
     *
     * @param num_facture the num facture
     */
    public void setNum_facture(int num_facture) {
        this.num_facture = num_facture;
    }

    /**
     * Gets nom societe.
     *
     * @return the nom societe
     */
    public String getNom_societe() {
        return nom_societe;
    }

    /**
     * Sets nom societe.
     *
     * @param nom_societe the nom societe
     */
    public void setNom_societe(String nom_societe) {
        this.nom_societe = nom_societe;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public String getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborateur the collaborateur
     */
    public void setCollaborateur(String collaborateur) {
        this.collaborateur = collaborateur;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param quantite the quantite
     */
    public void setQuantite(float quantite) {
        this.quantite = quantite;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param prix the prix
     */
    public void setPrix(float prix) {
        this.prix = prix;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param montant the montant
     */
    public void setMontant(float montant) {
        this.montant = montant;
    }

    /**
     * Gets libelle
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle
     * @param libelle the libellé
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets mois prestations.
     *
     * @return the mois prestations
     */
    public Date getMois_prestations() {
        return mois_prestations;
    }

    /**
     * Sets mois prestations.
     *
     * @param mois_prestations the mois prestations
     */
    public void setMois_prestations(Date mois_prestations) {
        this.mois_prestations = mois_prestations;
    }
}
