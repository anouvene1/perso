/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;

import java.util.List;

/**
 * The type Deplacements form.
 */
public class DeplacementsForm {

    private List<DemandeDeplacementAsJson> listDeplacements;
    private String commentaire;

    /**
     * Instantiates a new Deplacements form.
     */
    public DeplacementsForm() {
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the p commentaire
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }

    /**
     * Gets list deplacements.
     *
     * @return the list deplacements
     */
    public List<DemandeDeplacementAsJson> getListDeplacements() {
        return listDeplacements;
    }

    /**
     * Sets list deplacements.
     *
     * @param pListDeplacements the p list deplacements
     */
    public void setListDeplacements(List<DemandeDeplacementAsJson> pListDeplacements) {
        listDeplacements = pListDeplacements;
    }
}
