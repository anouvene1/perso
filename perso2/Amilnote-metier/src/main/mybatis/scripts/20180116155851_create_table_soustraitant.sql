--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create table soustraitant
-- Migration SQL that makes the change goes here.
CREATE TABLE `ami_sous_traitant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `raison_sociale` varchar(100) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `enabled` tinyint(11) NOT NULL,
  `id_statut` int(11) NOT NULL DEFAULT '3',
  `id_prescripteur` int(11) DEFAULT NULL COMMENT 'id du manager qui demande la sous-traitance',
  `id_poste` int(11) NOT NULL DEFAULT '0',
  `accountNonLocked` tinyint(1) NOT NULL DEFAULT '1',
  `nbAttempts` int(1) DEFAULT '0',
  `adresse_postale` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail_UNIQUE` (`mail`),
  KEY `id_statut` (`id_statut`),
  KEY `ami_stt_fk_id_prescripteur_idx` (`id_prescripteur`),
  KEY `ami_stt_poste_idx` (`id_poste`),
  CONSTRAINT `ami_stt_fk_id_prescripteur` FOREIGN KEY (`id_prescripteur`) REFERENCES `ami_sous_traitant` (`id`),
  CONSTRAINT `ami_stt_ibfk_1` FOREIGN KEY (`id_statut`) REFERENCES `ami_ref_type_statut` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ami_stt_poste` FOREIGN KEY (`id_poste`) REFERENCES `ami_poste` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- //@UNDO
-- SQL to undo the change goes here.
drop table ami_sous_traitant;

