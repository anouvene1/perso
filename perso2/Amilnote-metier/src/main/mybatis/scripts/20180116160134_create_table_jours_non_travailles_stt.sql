--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create table jours_non_travailles_stt
-- Migration SQL that makes the change goes here.
CREATE TABLE `ami_jours_non_travailles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `nb_jours` float NOT NULL,
  `id_ref_absence` int(11) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `date_soumission` datetime DEFAULT NULL,
  `date_validation` datetime DEFAULT NULL,
  `id_stt` int(11) NOT NULL COMMENT 'id du sous traitant lie au jour non travaille',
  `commentaire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_ref_absence` (`id_ref_absence`),
  KEY `ami_jnt_fk_id_etat_idx` (`id_etat`),
  KEY `ami_jnt_fk_id_stt_idx` (`id_stt`),
  CONSTRAINT `ami_jnt_fk_id_stt` FOREIGN KEY (`id_stt`) REFERENCES `ami_sous_traitant` (`id`),
  CONSTRAINT `ami_jnt_fk_id_etat` FOREIGN KEY (`id_etat`) REFERENCES `ami_ref_etat` (`id`),
  CONSTRAINT `ami_jnt_ibfk_1` FOREIGN KEY (`id_ref_absence`) REFERENCES `ami_ref_type_absence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- //@UNDO
-- SQL to undo the change goes here.
drop table ami_jours_non_travailles;

