package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ModuleAddviseDAO;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 * SHA AMNOTE-203 12/01/2017.
 */
@Repository("ModuleAddviseDAO")
class ModuleAddviseDAOImplTest extends AbstractDAOImpl<ModuleAddvise> implements ModuleAddviseDAO {

    /**
     * retrouver la classe modele liée au DAO
     *
     * @return la classe liée
     */
    @Override
    protected Class<ModuleAddvise> getReferenceClass() {
        return ModuleAddvise.class;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ModuleAddvise findById(Long id) {
        return (ModuleAddvise) currentSession().get(ModuleAddvise.class, id);
    }

    /**
     * @return
     */
    @Override
    public List<ModuleAddvise> findAll() {
        return (List<ModuleAddvise>) getCriteria().list();
    }

    /**
     * @param pModuleAddvise
     * @return
     */
    @Override
    @Transactional
    public int deleteModuleAddvise(ModuleAddvise pModuleAddvise) {

        delete(pModuleAddvise);
        return 0;
    }

    /**
     * @param pModuleAddvise
     * @return
     */
    @Override
    @Transactional
    public int createOrUpdateModuleAddvise(ModuleAddvise pModuleAddvise) {

        saveOrUpdate(pModuleAddvise);
        return 0;
    }
}
