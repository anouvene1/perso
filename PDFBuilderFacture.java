/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.apache.commons.mail.EmailException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * The type Pdf builder facture.
 */
@Service("PDFBuilderFacture")
public class PDFBuilderFacture extends PDFBuilder {

    protected static Logger logger = LogManager.getLogger(PDFBuilderFacture.class);

    public static String PDF_FACTURES_CONTEXT_ROOT = "dossierfacturesExcel";

    private static String repertoireFacture = "";

    private final static String AMILTONE_LOGO_URL = "/imagesPDF/logo-amiltone-facture.png";

    private PdfPCell pdfPCell = new PdfPCell();

    /**
     * Customized Facture Font
     */
    private static final BaseColor FACTURE_HEADER_COLOR = new BaseColor(0xFF, 0xFF, 0xFF);
    private static final Font FACTURE_FONT_HEADER = FontFactory.getFont(FontFactory.HELVETICA, 10);
    private static final Font FACTURE_FONT_FOOTER = FontFactory.getFont(FontFactory.HELVETICA, 8);
    private static final Font FACTURE_FONT_FOOTER_MIN = FontFactory.getFont(FontFactory.HELVETICA, 7);
    private static final Font FONT_RIB = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 9);
    private static final Font FONT_REGLEMENT = FontFactory.getFont(FontFactory.HELVETICA, 9);

    private Font fontBlackHeader;

    @Autowired
    private MissionService missionService;

    @Autowired
    private TvaService tvaService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private ContactClientService contactClientService;

    @Autowired
    private TypeFactureService typeFactureService;

    @Autowired
    private FileService fileService;

    public PDFBuilderFacture() {
        this.fontBlackHeader = FACTURE_FONT_HEADER;
        this.fontBlackHeader.setColor(BaseColor.BLACK);
    }

    /**
     * Création du PDF de l'ordre de mission
     *
     * @param dateArchivage   the date voulue
     * @param mission         the mission
     * @param facture         the facture
     * @param elementsFacture the elements facture
     * @param idContact       the id contact
     * @param archive         permet de savoir si on est en mode archive d'un ODF (0) ou en visualisation (1)
     * @param numeroODF       the numero odf
     * @param client          the client
     * @return fileName string
     * @throws DocumentException the document exception
     * @throws IOException       local/network file/folder read/write problems
     */
    public String createPdfFacture(Mission mission, Facture facture, List<ElementFacture> elementsFacture, int idContact,
                                   boolean archive, String numeroODF, DateTime dateArchivage, Client client)
            throws IOException, DocumentException {

        logger.info("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");

        // création des fonts
        FontFactory.registerDirectories();
        SimpleDateFormat mois = new SimpleDateFormat("MMMM");
        SimpleDateFormat annee = new SimpleDateFormat("YYYY");
        DateFormat formatFullDate = DateFormat.getDateInstance(DateFormat.FULL);

        // On nomme le fichier avec l'id de la facture
        String pdfFileName = "Fac_" + facture.getNumFacture() + Constantes.EXTENSION_FILE_PDF;

        FileOutputStream fileOutputStream;

        try {
            // On récupère le chemin du context pour le dossier des facture
            repertoireFacture = Parametrage.getContext(PDF_FACTURES_CONTEXT_ROOT);

            String facturesDirectoryPath = repertoireFacture;
            if (!archive && numeroODF != null) { // Si on est dans le cas d'archivage de facture
                facturesDirectoryPath = facture.getPdf();
            } else { // Sinon on prévisualise
                // [OZE] 01-12-2016 Gestion du dossier de listes à imprimer
                if (numeroODF != "SpecialImpression") { // Si on reçois le code d'impression 'SpecialImpression' on ne change pas notre chemin
                    // sinon on ajoute au chemin 'temporaire'
                    facturesDirectoryPath = facturesDirectoryPath + "temporaire/" + pdfFileName;
                }
            }

            fileService.createFolder(facturesDirectoryPath);

            fileOutputStream = new FileOutputStream(facturesDirectoryPath); // Création du fichier
        } catch (Exception e) {
            logger.error("[create pdf]", e);
            return "Erreur fichier";
        }

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;

        lWriter = newWriter(lDocument, fileOutputStream);
        TableHeader lEvent = new TableHeader();

        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);

        pdfContent(lDocument, facture, formatFullDate, dateArchivage, mission, idContact,
                elementsFacture,
                fileOutputStream, lWriter, true, client);

        logger.info("-------------------------------------------------------------- TEST DMA 0---------------------------------------------------------");
        logger.info("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");

        return pdfFileName;
    }

    /**
     * Create PDF of all invoices (Factures)
     *
     * @param today       the date voulue
     * @param factureList the liste facture
     * @param withCharges the frais ou pas
     * @return an array of string, where in the first part [0] the directory path of the file and in the second part [1] the file name  {fileDirectoryPath, fileName}
     * @throws IOException        the io exception
     * @throws SchedulerException the scheduler exception
     * @throws EmailException     the email exception
     * @throws MessagingException the messaging exception
     * @throws DocumentException  the document exception
     */
    public String[] createBigPdfFacture(List<Facture> factureList, boolean withCharges, DateTime today) throws Exception, IOException, SchedulerException, EmailException, MessagingException, DocumentException {

        logger.info("-------------------------------------------------------------- creation PDF ---------------------------------------------------------");

        // création des fonts
        FontFactory.registerDirectories();
        SimpleDateFormat mois = new SimpleDateFormat(Constantes.DATE_FORMAT_MMMM);
        SimpleDateFormat annee = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY);
        DateFormat formatFullDate = DateFormat.getDateInstance(DateFormat.FULL);

        // SBE_AMNOTE-194 On nomme le fichier avec  son type(factures de frais ou de commande),l'année et le mois en cours
        String pdfFileName;
        if (withCharges) {
            pdfFileName = "AllFacFrais" + annee.format(today.toDate()) + "_" + mois.format(today.toDate()) + Constantes.EXTENSION_FILE_PDF;
        } else {
            pdfFileName = "AllFacCommande" + annee.format(today.toDate()) + "_" + mois.format(today.toDate()) + Constantes.EXTENSION_FILE_PDF;
        }

        // On récupère le chemin du context pour le dossier des facture
        String pdfDirectoryPath = Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT) + "temporaire/" + "ToutesLesFacturesActuelles/";
        ;

        fileService.createFolder(pdfDirectoryPath);

        FileOutputStream fileOutputStream;
        fileOutputStream = new FileOutputStream(pdfDirectoryPath + "/" + pdfFileName); // Création du fichier

        // Appliquer les preferences et construction des metadata.
        Document lDocument = newDocument();
        PdfWriter lWriter;

        lWriter = newWriter(lDocument, fileOutputStream);
        TableHeader lEvent = new TableHeader();

        lWriter.setPageEvent(lEvent);
        prepareWriter(lWriter);
        buildPdfMetadata(lDocument);

        List<ElementFacture> elementsFacture;
        Commande commande;
        Mission mission;
        lDocument.open();
        for (Facture facture : factureList) {
            // Génération des élements permettant de reconstituer une facture
            elementsFacture = elementFactureService.findByFacture(facture);
            mission = facture.getMission();
            commande = facture.getCommande();
            Client client;
            if (commande != null) {
                client = commande.getClient();
            } else {
                client = mission.getClient();
            }

            //on verifie l'adresse de facturation
            if (facture.getAdresseFacturation() == null && client != null) {
                facture.setAdresseFacturation(client.getAdresseFacturation());
            }

            pdfContent(lDocument, facture, formatFullDate, today, mission, facture.getContact(),
                    elementsFacture,
                    fileOutputStream, lWriter, false, client);
            lDocument.newPage();
        }
        lDocument.close();
        fileOutputStream.close();

        logger.debug("-------------------------------------------------------------- fin creation PDF ---------------------------------------------------------");

        return new String[]{pdfDirectoryPath, pdfFileName};
    }

    private void pdfContent(Document lDocument, Facture facture, DateFormat formatFullDate, DateTime today, Mission mission, int idContact,
                            List<ElementFacture> elementsFacture, FileOutputStream fileOutputStream, PdfWriter lWriter, boolean closeDoc, Client client)
            throws DocumentException, IOException {

        //SBE_AMNOTE-194 Si on veut faire qu'une seul facture et non une liste
        if (closeDoc == true) {
            lDocument.open();
        }

        TypeFacture typeFacture = facture.getTypeFacture();

        // On crée le haut de page avec la date et le logo
        if (typeFacture.getCode() != TypeFacture.TYPEFACTURE_FACTURE_FRAIS && facture.getDateFacture() != null) {
            lDocument.add(hautPage("Lyon, le " + formatFullDate.format(facture.getDateFacture())));
        } else {
            DateTime date = new DateTime();
            lDocument.add(hautPage("Lyon, le " + formatFullDate.format(date.toDate())));
        }
        // On crée la partie client, adresse
        lDocument.add(createTableAdresse(idContact, facture));

        // On met le num facture
        PdfPTable lTableAdresse = new PdfPTable(2); // Table de 2 colonnes
        lTableAdresse.setHorizontalAlignment(Element.ALIGN_RIGHT);
        lTableAdresse.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableAdresse.setWidthPercentage(100); // prend 100% de la page
        lTableAdresse.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableAdresse.addCell(new Phrase("")); // espace à gauche
        // on crée une couleur gris claire
        BaseColor color = new BaseColor(158, 158, 158);
        lTableAdresse.getDefaultCell().setBackgroundColor(color); // On assigne la couleur
        lTableAdresse.getDefaultCell().setFixedHeight(20);// On met une hauteur
        lTableAdresse.getDefaultCell().setBorderWidthRight(1);// On met les bords
        lTableAdresse.getDefaultCell().setBorderWidthTop(1);
        lTableAdresse.getDefaultCell().setBorderWidthLeft(1);
        lTableAdresse.getDefaultCell().setBorderWidthBottom(1);
        if (facture.getIsAvoir()) {
            lTableAdresse.addCell(new Phrase("Avoir N° " + facture.getNumFacture(), FontFactory.getFont(FontFactory.HELVETICA, 10))); // Numéro d avoir
        } else {
            lTableAdresse.addCell(new Phrase("Facture N° " + facture.getNumFacture(), FontFactory.getFont(FontFactory.HELVETICA, 10))); // Numéro de facture
        }
        lTableAdresse.setSpacingAfter(10); // espace en dessous
        lDocument.add(lTableAdresse); // On ajoute au document

        // On crée la partie infos : N° TVA, Siret
        lDocument.add(createTableInfos(client));
        // On rempli la facture avec la Commande, la Facture et les élements de la facture
        lDocument.add(createTableCommande(facture, mission, elementsFacture));
        // On crée la partie des mentions légales

        lDocument.add(createTableMentions(facture, lWriter, lDocument));

        // SBE_AMNOTE-194 fermeture du document pdf (pareil si on veut faire qu'une seul facture
        if (closeDoc == true) {
            lDocument.close();
            fileOutputStream.close();
        }
    }

    /**
     * Créer la table contenant le logo et la date
     *
     * @param date à afficher dans la facture
     * @return PdfPTable pdf p table
     */
    public PdfPTable hautPage(String date) {

        URL lUrl = getClass().getResource(AMILTONE_LOGO_URL);
        Image lImg = null;
        try {
            lImg = Image.getInstance(lUrl);
        } catch (BadElementException | IOException e) {
            logger.error("[pdf image]", e);
        }

        PdfPTable lTableHeader = new PdfPTable(2);
        lTableHeader.setTotalWidth(500);
        lTableHeader.setLockedWidth(true);

        lTableHeader.getDefaultCell().setFixedHeight(40);
        lTableHeader.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableHeader.getDefaultCell().setBackgroundColor(FACTURE_HEADER_COLOR);
        lTableHeader.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableHeader.addCell(lImg);

        lTableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableHeader.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableHeader.getDefaultCell().setBackgroundColor(FACTURE_HEADER_COLOR);
        lTableHeader.addCell(new Phrase(date, this.fontBlackHeader));

        lTableHeader.setSpacingAfter(30);

        return lTableHeader;
    }

    /**
     * Créer la table contenant les informations client
     *
     * @param idContact Contact de la facture à traiter
     * @param facture   Facture à traiter
     * @return PdfPTable pdf p table
     * @throws DocumentException the document exception
     */
    public PdfPTable createTableAdresse(int idContact, Facture facture) throws DocumentException {
        PdfPTable lTableAdresse = new PdfPTable(3);
        lTableAdresse.setTotalWidth(555);
        lTableAdresse.setWidths(new int[]{100, 100, 170});
        lTableAdresse.setHorizontalAlignment(Element.ALIGN_RIGHT);
        lTableAdresse.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableAdresse.setWidthPercentage(30);
        lTableAdresse.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);

        lTableAdresse.setWidthPercentage(60);
        lTableAdresse.addCell(new Phrase(""));// Espace à gauche
        lTableAdresse.addCell(new Phrase("Client : ", this.fontBlackHeader));

        Commande commande = facture.getCommande();

        try {

            // [CLO AMNOTE-308] REFONTE
            if (commande != null) {
                lTableAdresse.addCell(new Phrase(commande.getClient().getNom_societe(), this.fontBlackHeader)); // Société cliente
            } else {
                lTableAdresse.addCell(new Phrase(facture.getMission().getClient().getNom_societe(), this.fontBlackHeader)); // Société cliente
            }
            // REFONTE FIN

        } catch (Exception e) {
            throw e;
        }
        if (idContact != 0) { // Si on a sélectionné un contact pour la facture
            lTableAdresse.addCell(new Phrase("")); // Espace à gauche
            lTableAdresse.addCell(new Phrase("A l'attention de ", this.fontBlackHeader));
            ContactClient contact = contactClientService.findById(Long.valueOf(idContact)); // On récupère le contact
            lTableAdresse.addCell(new Phrase("" + contact.getNom() + " " + contact.getPrenom(), this.fontBlackHeader)); // On affiche nom et prénom
        }
        lTableAdresse.addCell(new Phrase(""));// Espace à gauche
        lTableAdresse.addCell(new Phrase("Adresse : ", this.fontBlackHeader));

        String aParser = null;
        String monAdresse = null;

        Client client;
        if (commande != null) {
            client = commande.getClient();
        } else {
            client = facture.getMission().getClient();
        }

        //[JNA][AMNOTE 159]
        // Si l'adresse de la facture n'est pas vide
        try {
            if (facture.getAdresseFacturation() != null) {
                String[] part = facture.getAdresseFacturation().split(",");
                String adresse = part[0];
                String ville = part[1];
                monAdresse = adresse + '\n' + ville.trim();
            }
        } catch (Exception e) {
            logger.error(e.getMessage() + facture.getId());
        }


        // On met l'adresse sur le PDF
        lTableAdresse.addCell(new Phrase(monAdresse, this.fontBlackHeader)); // Adresse de facturation


        lTableAdresse.setSpacingAfter(10);
        return lTableAdresse;
    }

    /**
     * Créer la table contenant les informations TVA et Siret
     *
     * @param client Client de la facture à traiter
     * @return PdfPTable pdf p table
     */
    public PdfPTable createTableInfos(Client client) {
        PdfPTable lTableAdresse = new PdfPTable(2);
        lTableAdresse.setHorizontalAlignment(Element.ALIGN_RIGHT);
        lTableAdresse.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableAdresse.setWidthPercentage(100);
        lTableAdresse.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableAdresse.getDefaultCell().setFixedHeight(20);
        lTableAdresse.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableAdresse.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableAdresse.addCell(new Phrase("")); // espace à gauche


        lTableAdresse.getDefaultCell().setBorderWidthRight(1);
        lTableAdresse.getDefaultCell().setBorderWidthTop(1);
        lTableAdresse.getDefaultCell().setBorderWidthLeft(1);
        BaseColor color = new BaseColor(158, 158, 158);
        lTableAdresse.getDefaultCell().setBackgroundColor(color);
        lTableAdresse.addCell(new Phrase("Notre N° TVA Intrac : FR23538949108", this.fontBlackHeader)); // espace à gauche

        lTableAdresse.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        lTableAdresse.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableAdresse.addCell(new Phrase("N° Siret : 53894910800020", this.fontBlackHeader));

        String tvaTemp = client.getTva();
        // [CLO AMNOTE 28/04/2017] Modif du if si tvaTemp==null, on le transforme en "". ATTENTION BDD : vérifier de ne pas trop mettre de null dans ami_client.tva
        if (tvaTemp == null) {
            tvaTemp = "";
        }
        if (tvaTemp != "") {
            lTableAdresse.getDefaultCell().setBorderWidthRight(1);
            lTableAdresse.getDefaultCell().setBorderWidthTop(1);
            lTableAdresse.getDefaultCell().setBorderWidthLeft(1);
            lTableAdresse.getDefaultCell().setBorderWidthBottom(1);
            lTableAdresse.getDefaultCell().setBackgroundColor(color);
            lTableAdresse.addCell(new Phrase("N° TVA Intrac du client : " + client.getTva(), this.fontBlackHeader));
        } else {
            lTableAdresse.getDefaultCell().setBorderWidthRight(1);
            lTableAdresse.getDefaultCell().setBorderWidthTop(1);
            lTableAdresse.getDefaultCell().setBorderWidthLeft(1);
            lTableAdresse.getDefaultCell().setBorderWidthBottom(1);
            lTableAdresse.getDefaultCell().setBackgroundColor(color);
            lTableAdresse.addCell(new Phrase("N° TVA Intrac du client : ", this.fontBlackHeader));
        }
        lTableAdresse.setSpacingAfter(10);

        return lTableAdresse;
    }

    /**
     * Créer la table contenant les informations de la facture
     *
     * @param facture         Facture à traiter
     * @param mission         Mission à traiter
     * @param elementsFacture the elements facture
     * @return PdfPTable pdf p table
     * @throws DocumentException the document exception
     */
    public PdfPTable createTableCommande(Facture facture,
                                         Mission mission,
                                         List<ElementFacture> elementsFacture) throws DocumentException {

        final String EXPENSE_MONTH_OF = "Frais du mois de ";
        final String SERVICE_MONTH_OF = "Prestations du mois de ";

        SimpleDateFormat mois = new SimpleDateFormat("MMMM");
        SimpleDateFormat annee = new SimpleDateFormat("yyyy");
        DecimalFormat montantFormat = new DecimalFormat("0.00");
        montantFormat.setRoundingMode(RoundingMode.HALF_UP);
        DecimalFormat quantiteFormat = new DecimalFormat("0.00");

        String missionMonth = null;
        String missionYear = null;

        if(!mission.getLinkEvenementTimesheetLine().isEmpty()) {
            missionMonth = mois.format(mission.getLinkEvenementTimesheetLine().get(0).getDate()).replaceAll("[\\d]", "");
            missionYear  = annee.format(mission.getLinkEvenementTimesheetLine().get(0).getDate());
        }

        final String servicesMonth = mois.format(facture.getMoisPrestations());
        final String servicesYear = annee.format(facture.getMoisPrestations());

        PdfPTable lTableCommande = new PdfPTable(4);
        lTableCommande.setSpacingBefore(20);

        lTableCommande.setTotalWidth(555);
        lTableCommande.setWidths(new int[]{200, 65, 65, 65});
        lTableCommande.setWidthPercentage(100);

        PdfPCell cell = lTableCommande.getDefaultCell();
        cell.setBorderWidthTop(1);
        cell.setBorderWidthLeft(1);

        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

        // Header
        BaseColor color = new BaseColor(115, 194, 251);
        cell.setBackgroundColor(color);
        cell.setBorderWidthRight(0f);
        cell.setFixedHeight(17f);

        lTableCommande.addCell(new Phrase(Constantes.COLUMN_EXCEL_DESIGNATION, this.fontBlackHeader));
        lTableCommande.addCell(new Phrase(Constantes.COLUMN_EXCEL_QUANTITY, this.fontBlackHeader));
        lTableCommande.addCell(new Phrase(Constantes.COLUMN_EXCEL_PU, this.fontBlackHeader));
        lTableCommande.addCell(new Phrase(Constantes.COLUMN_EXCEL_PRE_TAX_AMOUNT_EUROS, this.fontBlackHeader));

        // Row 1
        cell.setBackgroundColor(BaseColor.WHITE);
        cell.setPaddingTop(5f);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);

        // Fixe default min-height of colunms
        cell.setMinimumHeight(20f);

        if (facture.getCommande() != null) {
            lTableCommande.addCell(new Phrase("Commande : " + facture.getCommande().getNumero(), this.fontBlackHeader));
        } else {
            lTableCommande.addCell(new Phrase("Suivant proposition commerciale", this.fontBlackHeader));
        }

        addEmptyCells(lTableCommande, 3);

        // Row 2
        cell.setPaddingTop(0f);
        TypeFacture typeFacture = facture.getTypeFacture();

        PdfPCell pdfPCell = null;

        if (facture.getMoisPrestations() == null) {
            if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                pdfPCell = addCellPaddingTopBottom(EXPENSE_MONTH_OF + missionMonth + " " + missionYear);
            } else {
                pdfPCell = addCellPaddingTopBottom(SERVICE_MONTH_OF + missionMonth + " " + missionYear);
            }
        } else {
            if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
                pdfPCell = addCellPaddingTopBottom(EXPENSE_MONTH_OF + servicesMonth + " " + servicesYear);
            } else {
                pdfPCell = addCellPaddingTopBottom(SERVICE_MONTH_OF + servicesMonth + " " + servicesYear);
            }
        }

        lTableCommande.addCell(pdfPCell);
        addEmptyCells(lTableCommande, 3);

        // Rows 3 : Frais or Prestations lines
        // Case of "FACTURE DE FRAIS", Add title "Frais - <nom_collaborateur>"
        if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) {
            lTableCommande.addCell(new Phrase("Frais - " + mission.getCollaborateur()));
        } else {
            if (facture.getCommentaire() != null) {
                lTableCommande.addCell(new Phrase("" + facture.getCommentaire(), this.fontBlackHeader));
            } else {
                lTableCommande.addCell(new Phrase("", this.fontBlackHeader));
            }
        }

        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        if (facture.getQuantite() == Float.valueOf("0.0") && facture.getPrix() == Float.valueOf("0.0") && facture.getMontant() == Float.valueOf("0.0")) {
            addEmptyCells(lTableCommande, 3);
        } else {
            lTableCommande.addCell(new Phrase("" + quantiteFormat.format(facture.getQuantite()), this.fontBlackHeader));
            lTableCommande.addCell(new Phrase("" + quantiteFormat.format(facture.getPrix()), this.fontBlackHeader));
            lTableCommande.addCell(new Phrase("" + montantFormat.format(facture.getMontant()), this.fontBlackHeader));
        }

        // Increase the padding-bottom of the cell if invoice title contains several lines
        if(facture.getCommentaire().contains("\n") || facture.getCommentaire().contains("\r")) {
            cell.setMinimumHeight(5f);
            addEmptyCells(lTableCommande, 4);
        }

        // The case we have one or several line(s) of "Prestation" invoice
        float montantTotal = facture.getMontant();

        if (!elementsFacture.isEmpty()) {
            for (ElementFacture elem : elementsFacture) {
                cell.setMinimumHeight(25f);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                lTableCommande.addCell(new Phrase("" + elem.getLibelleElement(), this.fontBlackHeader)); // libellé

                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

                if (elem.getQuantiteElement() == null || 0.0f == elem.getQuantiteElement()) {
                    lTableCommande.addCell(new Phrase("", this.fontBlackHeader));
                } else {
                    lTableCommande.addCell(new Phrase("" + quantiteFormat.format(elem.getQuantiteElement()), this.fontBlackHeader)); // Quantité
                }
                if (elem.getPrixElement() == null || 0.0f == elem.getPrixElement()) {
                    lTableCommande.addCell(new Phrase("", this.fontBlackHeader));
                } else {
                    lTableCommande.addCell(new Phrase("" + quantiteFormat.format(elem.getPrixElement()), this.fontBlackHeader)); // Prix
                }
                if (elem.getMontantElement() == null || 0.0f == elem.getMontantElement()) {
                    lTableCommande.addCell(new Phrase("", this.fontBlackHeader));
                } else {
                    lTableCommande.addCell(new Phrase("" + montantFormat.format(elem.getMontantElement()), this.fontBlackHeader)); // Montant
                    montantTotal = montantTotal + elem.getMontantElement();
                }

                addEmptyRow(elem.getLibelleElement(), lTableCommande);
            }
        }

        // Add empty row
        cell.setBorderWidthBottom(1);
        cell.setFixedHeight(20f);
        addEmptyCells(lTableCommande, 4);

        // Fixed height columns HT, TVA, TTC
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setFixedHeight(17f);
        cell.setPaddingBottom(0f);

        // Total HT
        lTableCommande.addCell(new Phrase(""));
        lTableCommande.addCell(new Phrase(""));
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(1f);
        cell.setBorderWidthBottom(1f);
        lTableCommande.addCell(new Phrase("Total H.T.", this.fontBlackHeader));
        lTableCommande.addCell(new Phrase("" + montantFormat.format(montantTotal), this.fontBlackHeader));

        // Total TVA
        Float tva = facture.getTVA().getMontant() / 100;
        Float tva2 = facture.getTVA().getMontant();
        Float montantTva = montantTotal * tva;

        cell.setBorder(Rectangle.NO_BORDER);
        addEmptyCells(lTableCommande, 2);
        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(1);
        cell.setBorderWidthBottom(1);

        if (tva2 - Math.round(tva2) != 0)
            lTableCommande.addCell(new Phrase("TVA " + tva2 + "%", this.fontBlackHeader));
        else
            lTableCommande.addCell(new Phrase("TVA " + Math.round(tva2) + "%", this.fontBlackHeader));
        lTableCommande.addCell(new Phrase("" + montantFormat.format(montantTva), this.fontBlackHeader));

        // Total TTC
        cell.setBorder(Rectangle.NO_BORDER);
        addEmptyCells(lTableCommande, 2);

        cell.setBorderWidthRight(0);
        cell.setBorderWidthLeft(1);
        cell.setBorderWidthBottom(1);
        lTableCommande.addCell(new Phrase("Total T.T.C.", this.fontBlackHeader));
        lTableCommande.addCell(new Phrase("" + montantFormat.format(montantTva + montantTotal), this.fontBlackHeader));

        lTableCommande.setSpacingAfter(10);

        // Add border left to the last cells
        List<PdfPRow> pdfRows = lTableCommande.getRows();
        for(PdfPRow pdfRow: pdfRows) {
            pdfRow.getCells()[pdfRow.getCells().length - 1].setBorderWidthRight(1f);
        }
        return lTableCommande;
    }

    /**
     * Add padding top and bottom to a specific cell
     * @param sentence String of cell
     * @return PdfPCell PDF cell
     */
    private PdfPCell addCellPaddingTopBottom(String sentence) {
        PdfPCell cell = new PdfPCell(new Phrase(sentence, this.fontBlackHeader));

        cell.setPaddingTop(18f);
        cell.setPaddingBottom(38f);

        cell.setBorderWidthTop(0);
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthLeft(1);
        cell.setBorderWidthRight(0);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return cell;
    }

    /**
     * Add empty row
     * @param title The cell Title
     * @param lTableCommande The PDF Table
     */
    private void addEmptyRow(String title, PdfPTable lTableCommande) {
        // Increase the padding-bottom of the cell if invoice title contains several lines
        if(title.trim().length() > 57) {
            lTableCommande.getDefaultCell().setMinimumHeight(5f);
            addEmptyCells(lTableCommande, 4);
        }
    }

    /**
     * Add empty cells
     * @param lTableCommande The PDF table
     * @param cellsNumber Number of celles to add
     */
    private void addEmptyCells(PdfPTable lTableCommande, int cellsNumber) {
        for(int i=0; i<cellsNumber; i++ ) {
            lTableCommande.addCell(new Phrase(""));
        }
    }

    /**
     * Créer la table contenant les informations sur les mentions légales
     *
     * @param facture   the facture
     * @param pWriter   the p writer
     * @param pDocument the p document
     * @return PdfPTable pdf p table
     */
    public PdfPTable createTableMentions(Facture facture, PdfWriter pWriter, Document pDocument) {

        final String REGULATION_BY = "Règlement par ";
        final String PAYMENT_VIR_45_DAY = "virement 45 jours fin de mois";
        // Création de la table
        PdfPTable lTableMentions = new PdfPTable(1);
        // On prends la taille du contenu du document
        PdfContentByte cb = pWriter.getDirectContent();

        lTableMentions.setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableMentions.setWidthPercentage(100);
        lTableMentions.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

        float llx = 36;
        float lly = 115;
        float urx = 559;
        float ury = 87;
        Rectangle rect = new Rectangle(llx, lly, urx, ury);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(0.5f);
        rect.setBorderColor(BaseColor.BLACK);
        cb.rectangle(rect);

        Commande commande = facture.getCommande();
        TypeFacture typeFacture = facture.getTypeFacture();

        if (typeFacture.getCode().equals(TypeFacture.TYPEFACTURE_FACTURE_FRAIS)) { // [CLO AMNOTE-400] Pour les factures de frais, on a toujours des conditions de paiement par virement à réception
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                    new Phrase((REGULATION_BY + "virement à réception"), FONT_REGLEMENT),
                    (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                    pDocument.bottom() + 84, 0);

            if (facture.getRIBFacture() != null) {
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        new Phrase((facture.getRIBFacture()), FONT_RIB),
                        (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                        pDocument.bottom() + 71, 0);
            }
        } else {
            if (commande != null) {
                if (!commande.getConditionPaiement().equals("") && commande.getConditionPaiement() != null && commande.getMoyensPaiement().getIdPaiement() != 0) {
                    // [JNA][AMNOTe 168] Si la facture est vierge elle concerne des frais TOUJOURS payés "à réception"
                    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                            new Phrase((REGULATION_BY + commande.getConditionPaiement() + " " + commande.getMoyensPaiement().getLibellePaiement()), FONT_REGLEMENT),
                            (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                            pDocument.bottom() + 84, 0);
                } else {
                    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                            new Phrase((REGULATION_BY + PAYMENT_VIR_45_DAY), FONT_REGLEMENT),
                            (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                            pDocument.bottom() + 84, 0);
                }

                if (facture.getRIBFacture() != null && !commande.getConditionPaiement().equals("chèque")) {
                    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                            new Phrase((facture.getRIBFacture()), FONT_RIB),
                            (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                            pDocument.bottom() + 71, 0);
                }
            } else {
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        new Phrase((REGULATION_BY + PAYMENT_VIR_45_DAY), FONT_REGLEMENT),
                        (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                        pDocument.bottom() + 84, 0);

                if (facture.getRIBFacture() != null) {
                    ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                            new Phrase((facture.getRIBFacture()), FONT_RIB),
                            (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                            pDocument.bottom() + 71, 0);
                }
            }
        }

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("Tout retard de paiement engendre une pénalité égale à 3 fois la base du taux d'interêt légal en vigeur (C.Com L441-6, al. 3 modifié)"), FACTURE_FONT_FOOTER_MIN),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + 58, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("Aucun escompte ne sera accordé en cas de règlement anticipé"), FACTURE_FONT_FOOTER_MIN),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + 45, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("Siège Social : Immeuble le Président - 76 boulevard du 11 novembre 1918 - 69100 Villeurbanne"), FACTURE_FONT_FOOTER),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + 32, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("Tél : 04 37 70 89 95   Fax : 04 78 17 03 77"), FACTURE_FONT_FOOTER),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + 19, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("SAS au capital de 30 000€ - Code APE 7112B"), FACTURE_FONT_FOOTER),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + 6, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                new Phrase(("Déclaration d'activité enregistrée sous le numéro 82 69 12318 69 auprès du préfet de région Rhône-Alpes"), FACTURE_FONT_FOOTER),
                (pDocument.right() - pDocument.left()) / 2 + pDocument.leftMargin(),
                pDocument.bottom() + -7, 0);

        return lTableMentions;
    }

    /**
     * Inner class to add a table as header.
     */
    class TableHeader extends PdfPageEventHelper {
        /**
         * The header text.
         */
        String header;
        /**
         * The template with the total number of pages.
         */
        PdfTemplate total;

        /**
         * Allows us to change the content of the header.
         *
         * @param pHeader The new header String
         */
        public void setHeader(String pHeader) {
            this.header = pHeader;
        }

        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         *
         * @see PdfPageEventHelper#onOpenDocument(PdfWriter,
         * Document)
         */
        @Override
        public void onOpenDocument(PdfWriter pWriter, Document pDocument) {
            total = pWriter.getDirectContent().createTemplate(10, 15);
            total.setColorFill(BaseColor.WHITE);
        }

        /**
         * Adds a header to every page
         *
         * @see PdfPageEventHelper#onEndPage(PdfWriter,
         * Document)
         */
        @Override
        public void onEndPage(PdfWriter pWriter, Document pDocument) {

        }

        /**
         * Fills out the total number of pages before the document is closed.
         *
         * @see PdfPageEventHelper#onCloseDocument(PdfWriter,
         * Document)
         */
        @Override
        public void onCloseDocument(PdfWriter pWriter, Document pDocument) {
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(pWriter.getPageNumber() - 1)), 2, 2, 0);
        }
    }

}
