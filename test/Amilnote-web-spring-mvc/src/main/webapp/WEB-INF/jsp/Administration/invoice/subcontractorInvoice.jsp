<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="tab-pane" id="tabFacturableSS">
    <sec:authorize access="hasAnyAuthority('DRH')">
        <c:choose>
            <c:when test="${forcerFactureOK}">
                <div>
                    <br/>
                    <h4 style="color: red" align="center">
                        Certains RA n'ont pas été validé et bloquent le processus de facturation. Vous pouvez
                        dès à présent
                        forcer ces factures pour continuer le processus en cliquant sur le bouton "Forcer les
                        factures" plus bas.
                    </h4>
                    <br/>
                </div>
            </c:when>
        </c:choose>
    </sec:authorize>

    <br/>
    <br/>
    <div class="col-md-6 text-right" style="font-size: xx-large; float:right;">
        ${moisActuel} ${anneeActuel}<br/>
    </div>
    <br/>
    <div class="col-md-3">
        <a href="${administration}${facturesValidees}/null" class="btn btn-success">Consulter les factures
            archivées</a>
    </div>

    <div class="col-md-12 text-center section-border">
        <form class="form-inline" method="post" action="${administration}${factures}/factures#tabFacturableSS">
            <div class="form-group">
                <label for="monthExtract" id="lblMonthPickerSS">Date voulue :</label>
                <input class="form-control monthPicker" name="monthYearExtract" id="monthExtract">
            </div>
            <button class="btn btn-primary" type="submit" name="action"
                    id="btnMonthPickerSS" value=""> Valider</button>
        </form>
        <br/><br/><br/>

        <%-- [CLO][AMNOTE 308] Tableau des facturables - REFONTE --%>
        <legend class="text-center">
            <h3>Facturables sous-traitants</h3>
            <p class="headerLabel">Factures sous-traitant correspondant chacune à un ordre de mission et, sauf
                manque, à un bon de commande.</p>
            <br/>
        </legend>
        <fieldset>
            <div id="toolbarFacturablesSS">
                <!--SBE_AMNOTE-183 POSSIBILITE DE SOUMETTRE LES FACTURES SEULEMENT POUR LES DRH -->
                <sec:authorize access="hasAnyAuthority('DRH')">
                    <c:set var="verifSoumission" scope="session" value="0"/>
                    <c:forEach items="${listFacturablesSS}" var="item">
                        <c:if test="${item.etat.id != 2 && item.etat.id != 10}">
                            <c:set var="verifSoumission" scope="session" value="1"/>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${verifSoumission==1}">
                            <c:if test="${not empty listFacturablesSS}">
                                <c:set var="nbsFacturablesSansNum" scope="session" value="0"/>
                                <c:forEach items="${listFacturablesSS}" var="facture">
                                    <c:if test="${facture.numFacture==0}">
                                        <c:set var="nbsFacturablesSansNum" scope="session" value="1"/>
                                    </c:if>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${nbsFacturablesSansNum==0}">
                                        <a class="btn btn-danger"
                                           onclick="soumissionFactures(1,'sous_traitants')">Soumettre l'ordre
                                            de facturation des commandes </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a class="btn btn-info"
                                           onclick="openModalNumerotation(1,'sous_traitants')">Numéroter les
                                            factures de commande </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${not empty listFacturablesSS}">
                                <a class="btn btn-default" disabled="true"
                                   onclick="soumissionFactures(1,'sous_traitants')">Soumettre
                                    l'ordre de facturation des commandes </a>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </sec:authorize>

                <!--SBE_AMNOTE-183 POSSIBILITE DE VALIDER LES FACTURES SEULEMENT POUR LA DIRECTION -->
                <sec:authorize access="hasAnyAuthority('DIR')">
                    <c:if test="${not empty listFacturablesSS}">
                        <c:set var="verifExistSoumission" scope="session" value="0"/>
                        <c:forEach items="${listFacturablesSS}" var="item">
                            <c:if test="${item.etat.id == 2}">
                                <c:set var="verifExistSoumission" scope="session" value="1"/>
                            </c:if>
                        </c:forEach>

                        <!--affichage du boutton valider si il existe des facture soumises-->
                        <c:choose>
                            <c:when test="${verifExistSoumission==1}">
                                <a class="btn btn-success" onclick="createExcels(1,'sous_traitants')">Valider
                                    l'ordre de facturation
                                    des commandes </a>
                                <a class="btn btn-danger" onclick="refusFactures(1,'sous_traitants')">Refuser
                                    l'ordre de facturation
                                    des commandes</a>
                            </c:when>
                        </c:choose>
                    </c:if>
                </sec:authorize>

                <c:set var="verifListeVide" scope="session" value="0"/>
                <c:forEach items="${listFacturablesSS}" var="item">
                    <c:if test="${item.etat.id != 10}">
                        <c:set var="verifListeVide" scope="session" value="1"/>
                    </c:if>
                </c:forEach>

                <c:choose>
                    <%-- SBE_AMNOTE-175 Si la liste n'est pas vide, le bouton est clicable, sinon non --%>
                    <c:when test="${verifListeVide==1}">
                        <a href="${administration}/factures/genererExcel/sous_traitants/FRABLESST/${dateVoulueRefonte}"
                           id="exportFacturablesSS"
                           class="exportExcelFacturesCommande btn btn-primary"> Exporter </a>

                        <a href="${administration}/factures/csv/${FRABLESSTCSV}/${dateVoulueRefonte}"
                           id="exportFMainCSV"
                           class="btn btn-primary"> Export comptable </a>

                        <a href="${administration}/factures/SaveAllFacturesPDF/sous_traitants/${FRABLE}/${dateVoulueRefonte}"
                           id="pdfFacturablesSS"
                           class="btn btn-warning"> Telecharger PDF complet </a>
                    </c:when>

                    <c:otherwise>
                        <a id="margeEnBasBoutton" class="exportExcelFacturesCommande btn btn-default"
                           disabled="true">
                            Exporter </a>
                    </c:otherwise>
                </c:choose>
            </div>
            <table class="table"
                   data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbarFacture"
                   data-pagination="true"
                   data-page-size=20
                   data-page-list="[10, 20, 30, 40,50]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-pagination-detail-h-align="left"
                   data-show-multi-sort="true"
                   data-sort-priority='[{"sortName": "nom","sortOrder":"asc"}{"sortName": "id", "sortOrder":"asc"}]'
                   id="idBootstrapTable3"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditCollabSansFacture">
                <thead>
                <tr>
                    <th data-field="id" data-sortable="true" data-align="center">N°</th>
                    <th data-field="nom" data-sortable="true" data-align="center">Client</th>
                    <th data-field="prenom" data-sortable="true" data-align="center">Collaborateur</th>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                    <th data-field="nbjours" data-sortable="true" data-align="center">Quantité</th>
                    <th data-field="tjm" data-sortable="true" data-align="center">P.U.</th>
                    <th data-field="prestations" data-sortable="true" data-align="center">Prestations</th>
                    <th data-field="total" data-sortable="true" data-align="center">Total</th>
                    <th data-field="editer" data-sortable="true" data-align="center">Éditer</th>
                    <th data-field="Supprimer" data-sortable="true" data-align="center">Supprimer</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${listFacturablesSS}" var="facture">
                    <tr>
                        <td><c:out value="${facture.numFacture}"/></td>
                        <c:choose>
                            <c:when test="${facture.commande!=null}">
                                <td><c:out value="${facture.commande.client.nom_societe}"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><c:out value="${facture.mission.client.nom_societe}"/></td>
                            </c:otherwise>
                        </c:choose>
                        <td><c:out value="${facture.mission.collaborateur}"/></td>
                        <td><c:out value="${facture.mission.mission}"/></td>
                        <c:choose>
                            <c:when test="${facture.commande!=null}">
                                <td><c:out value="${facture.commande.numero}"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><span class="manqueCommande"> Suivant proposition commerciale</span></td>
                            </c:otherwise>
                        </c:choose>
                        <td><c:out value="${mapFactureQuantiteMontantSS[facture.id].first}"/></td>
                        <td><c:out value="${facture.prix}"/></td>
                        <td><c:out value="${mapFactureElementMontantSS[facture.id]}"/></td>
                        <td><c:out value="${mapFactureQuantiteMontantSS[facture.id].second}"/></td>

                        <c:choose>
                            <c:when test="${facture.etat.id == 2}">
                                <td><a href="${administration}${afficher}/${facture.id}"
                                       disabled="true" target="_blank">
                                    <a disabled="true"></a>
                                    <span class="glyphicon glyphicon-edit" disabled="true"></span>
                                </a></td>

                                <td><span class="glyphicon glyphicon-ban-circle"></span></td>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    <a href="${administration}${editFacture}/<c:out value="${facture.id}"/>">
                                        <span class="glyphicon glyphicon-edit "></span>
                                    </a>
                                </td>
                                <td>
                                    <a href="${administration}${deleteFacture}/<c:out value="${facture.id}"/>"
                                       data-confirm="Etes-vous certain de vouloir supprimer la facture ${facture.numFacture} ?">
                                        <span class="glyphicon glyphicon-remove "></span>
                                    </a>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </fieldset>
    </div>

    <!-- Table : missions without invoice for subcontractor -->
    <c:set var="listMissionsWithoutInvoice" value="${listMissionsWithoutInvoiceSubcontractor}" scope="request"/>
    <jsp:include page="missionWithoutInvoiceTable.jsp"/>

</div>
