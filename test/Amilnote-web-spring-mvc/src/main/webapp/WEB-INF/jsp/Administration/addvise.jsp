<%--
  ~ ©Amiltone 2017
--%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!-- Ressources complémentaires -->
<link rel="stylesheet" type="text/CSS" href="${contextPath}/resources/css/addvise.css"/>
<script type="text/javascript" src="${contextPath}/resources/js/Addvise/fixed_table_rc.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/Addvise/addvise.js"></script>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Gestion du programme Addvise</li>
</ol>

<!-- Flash messages -->

<c:if test="${not empty succes}">
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p class="addvise-flash-success">
                ${succes}
        </p>
    </div>
</c:if>
<c:if test="${not empty sessionAdded and sessionAdded eq true}">
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p class="addvise-flash-success">La session a été créée.</p>
    </div>
</c:if>
<c:if test="${errorDate}">
    <div class="alert alert-danger alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p class="addvise-flash-error">Une session existe déjà pour cette date avec ce formateur.</p>
    </div>
</c:if>

<div class="alert alert-danger checkInvitDate">
    <span class="close close-alert">&times;</span>
    <p class="addvise-flash-error">Les invitations ne peuvent être envoyées une fois la session passée.</p>
</div>


<!-- Contenu pour le programme Addvise -->
<div id="div-addvise-gestion">
    <h3>Gestion des modules Addvise</h3>

    <div id="toolbar">
        <!-- menu-->
        <a type="button" data-backdrop="static" class="btn btn-lg btn-default btn-action bluck"
           id="btn-addvise-modal-creation-session">
            <span class="glyphicon glyphicon-plus"></span> Créer session
        </a>
        <a href="${ administration }/addvise-hist" type="button" data-backdrop="static"
           class="btn btn-lg btn-default btn-action purpleHaze" id="btn-addvise-modal-historique-session">
            <span class="glyphicon glyphicon-time"></span> Afficher historique
        </a>
    </div>
    <form class="navbar-form navbar-right inline-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Recherche" id="addvise-chercher" name="chercher">
        </div>
    </form>
    <!-- Table de saisie des sessions -->
    <div id="scrolltab">
        <table id="table-addvise-gestion"
               class="table table-responsive table-container"
               data-toggle="table"
               data-toolbar="#toolbar">
            <thead id="thead-addvise-gestion">
            <tr>
                <th id="firstColHeader">
                    <div class="cell-addvise-1"></div>
                    <div class="addvise-division-col">Collaborateur</div>
                    <select class="form-control" style="width:auto;" id="addvise-cacher">

                        <option value="0">Tous</option>
                        <option value="1">> 4 mois</option>

                        <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                        </c:forEach>
                    </select>

                    <img class="sort-alphabetically-white"
                         src="${contextPath}/resources/images/sort-alphabetically.png">

                    <img class="icone_date addvise-tri" id="addvise-tri-alpha"
                         src="${contextPath}/resources/images/sort-down-triangular-symbol.png">

                    <img class="sort-clock"
                         src="${contextPath}/resources/images/clock.png">

                    <img class="icone_date addvise-tri" id="addvise-tri-date-entree"
                         src="${contextPath}/resources/images/sort-down-triangular-symbol.png">

                </th>
                <th>
                    <div class="addvise-multiple-header cell-addvise-2 last-session-addvise">
                        <div addvise-division-col>Dernière session réalisée</div>
                        <div class="addvise-multiple-col">
                            <div class="form-group">
                                <select class="form-control" style="width:auto;" id="addvise-filtrer">
                                    <option value="">Tous</option>
                                    <option value="0" <c:if
                                        test="${previousModuleId == 0}"> selected="selected" </c:if> >Aucun
                                    </option>
                                    <c:forEach items="${listeModules}" var="module">
                                        <option value="${module.id}" <c:if
                                            test="${previousModuleId == module.id}"> selected="selected" </c:if>>
                                                ${module.codeModuleAddvise} : ${module.nomModuleAddvise}
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="date">Date
                                <img class="icone_date addvise-tri" id="addvise-tri-date-dernier-module"
                                     src="${contextPath}/resources/images/sort-down-triangular-symbol.png">
                            </div>
                        </div>
                    </div>
                </th>
                <c:forEach items="${listeSessionsModifiables}" var="session">
                    <th class="session-modif session-etat-${session.etat} session-id-${session.id}">
                        <div class="cell-addvise">
                            <div>
                                ${session.moduleAddvise.codeModuleAddvise} le <span
                                class="cell-addvise-dateSession-jour session-id-${session.id}"><fmt:formatDate
                                value="${session.date}" pattern="dd"/></span>/<span
                                class="cell-addvise-dateSession-mois session-id-${session.id}"><fmt:formatDate
                                value="${session.date}" pattern="MM"/></span>/<span
                                class="cell-addvise-dateSession-annee session-id-${session.id}"><fmt:formatDate
                                value="${session.date}" pattern="yyyy"/></span> à <span
                                class="cell-addvise-dateSession-heure session-id-${session.id}"><fmt:formatDate
                                value="${session.date}" pattern="HH"/></span>:<span
                                class="cell-addvise-dateSession-minutes session-id-${session.id}"><fmt:formatDate
                                value="${session.date}" pattern="mm"/></span>

                            </div>
                            <div>${session.formateur.nom} ${session.formateur.prenom}</div>
                        </div>

                        <div>
                            <form action="Administration/addvise/validateSession" method="post"
                                  class="addvise-form-session form-confirm-session">
                                <a type="button" data-backdrop="static" class="btn btn-sm btn-confirm-session"
                                   data-etatsession="${session.etat}" data-idsession="${session.id}">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </a>
                                <input type="hidden" name="idSession" value="${session.id}"/>
                            </form>
                            <form action="Administration/addvise/removeSession" method="post"
                                         class="addvise-form-session form-remove-session">
                            <a type="button" data-backdrop="static" class="btn btn-sm btn-remove-session">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                            <input type="hidden" name="idSession" value="${session.id}"/>
                        </form>
                            <!-- Bouton modifier-->
                            <div style="display: none" >
                                <option id="selected-option-module"
                                        selected
                                        value="${session.moduleAddvise.id}"> ${session.moduleAddvise.codeModuleAddvise} : ${session.moduleAddvise.nomModuleAddvise}
                                </option>
                                <option id="selected-option-formateur"
                                        selected
                                        value="${session.formateur.id}">${session.formateur.nom} ${session.formateur.prenom}
                                </option>
                            </div>
                                <a type="button" data-backdrop="static"
                                   class="btn btn-sm btn-link btn-addvise-modal-modification-session"
                                   data-toggle="modal"
                                   data-idsession="${session.id}"
                                   data-idmodule="${session.moduleAddvise.id}"
                                   data-idformateur="${session.formateur.id}"
                                   data-jour="<fmt:formatDate  value="${session.date}" pattern="dd"/>"
                                   data-mois="<fmt:formatDate  value="${session.date}" pattern="MM"/>"
                                   data-annee="<fmt:formatDate  value="${session.date}" pattern="yyyy"/>"
                                   data-heure="<fmt:formatDate  value="${session.date}" pattern="HH"/>"
                                   data-minute="<fmt:formatDate  value="${session.date}" pattern="mm"/>"
                                >

                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            <!-- Bouton print-->
                            <form action="Administration/addvise/printSessionToSign" method="post" target="_blank"
                                  class="addvise-form-session form-print-session">
                                <button type="submit" data-backdrop="static" data-etatsession="${session.etat}"
                                        class="btn btn-sm btn-link btn-print-session">
                                    <span class="glyphicon glyphicon-print"></span>
                                </button>
                                <input type="hidden" name="idSession" value="${session.id}"/>
                            </form>
                            <span class="countConfirmSpan">0</span>
                            <img class="icone_2 countConfirm" src="${contextPath}/resources/images/addvise/checked.gif">
                        </div>

                        <!-- Confirmation de session -->
                        <div class="modal fade" id="modale-confirm-session-${session.id}" tabindex="-1" role="dialog"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Confirmer la session ?</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="modal-body-confirm">
                                            <p class="modal-confirm-session">Voulez-vous confirmer cette session ?</p>
                                            <p class="modal-confirm-session">Si vous le faites, les collaborateurs et le
                                                formateur
                                                seront informés que la session aura lieu. Les changements effectués
                                                seront sauvegardés.</p>
                                        </div>
                                        <div class="modal-body-validate">
                                            <p class="modal-validate-session">Voulez-vous valider et clôturer la session
                                                ? Les changements effectués seront sauvegardés.</p>
                                        </div>
                                        <div>
                                            <p class="missingInvit">
                                                    <c:set var="firstIt" value="0"/>
                                                <c:forEach items="${session.listeLinkSessionCollaborateur}" var="invit">
                                                <c:if test="${invit.etatInvitation == 'ENVOYEE'}">
                                                <c:if test="${firstIt == 0}">
                                                <strong class="redWarning">Attention</strong>,
                                                les collaborateurs suivants avaient des invitations en
                                                attente de réponse (confirmez si vous avez bien changé leur état) :
                                                    <c:set var="firstIt" value="1"/>
                                                </c:if>

                                            <li> ${invit.collaborateur.nom} ${invit.collaborateur.prenom}</li>
                                            </c:if>
                                            </c:forEach>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-success btn-modale-confirm-session">
                                            Oui
                                        </button>
                                        <button type="button" id="btn-cancel-${session.id}" class="btn btn-danger"
                                                data-dismiss="modal">
                                            Non
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                </c:forEach>
            </tr>
            </thead>
            <tbody>
            <c:set var="count" value="0"/>
            <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                <c:set var="count" value="${count + 1}"/>
                <tr class="addvise-row">
                    <!-- collaborateur -->
                    <td class="firstCol">
                        <div class="cell-addvise-1">
                            <span class="tooltip-addvise exclus-addvise-${collaborateur.exclusAddvise}"
                                  data-toggle="tooltip" data-placement="right" title="Entrée :
                                <fmt:formatDate value="${collaborateur.dateEntree}" pattern="dd/MM/yyyy"/>">
                                        ${collaborateur.nom} ${collaborateur.prenom}
                            </span>
                        </div>
                    </td>
                    <td class="contentCol">
                        <div class="addvise-multiple-col cell-addvise-2 exclus-addvise-${collaborateur.exclusAddvise}">
                            <!-- module -->
                            <div>${collaborateur.lastAddviseSession.moduleAddvise.codeModuleAddvise}</div>
                            <!-- date -->
                            <div>
                                <fmt:formatDate value="${collaborateur.lastAddviseSession.date}"
                                                pattern="dd/MM/yyyy"/>
                            </div>
                        </div>
                    </td>
                    <c:forEach items="${listeSessionsModifiables}" var="session">
                        <td class="contentCol">
                            <div class="cell-addvise">
                                <c:if test="${collaborateur.futureModule == session.moduleAddvise}">
                                <div class="invitation-addvise"
                                     data-collab="${collaborateur.id}" data-session="${session.id}">
                                    <div class="icones_session">
                                        <span class="icone_0 icone">
                                        <img class="icone_0" src="${contextPath}/resources/images/addvise/info.gif">
                                        </span>

                                        <span class="icone_1 icone">
                                        <img class="icone_1" src="${contextPath}/resources/images/addvise/send.gif">
                                        </span>

                                        <span class="icone_2 icone">
                                        <img class="icone_2" src="${contextPath}/resources/images/addvise/checked.gif">
                                        </span>

                                        <span class="icone_3 icone">
                                            <img class="icone_3"
                                                 src="${contextPath}/resources/images/addvise/unchecked.gif">
                                        </span>
                                    </div>
                                    </c:if>
                                </div>
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div id="nb-collab">
            <span class="addvise-nb-collab">Nombre de collaborateurs : </span>
            <span class="addvise-nb-collab">${count}</span>
        </div>
    </div>
    <div id="validate-addvise-session">
        <form id="addvise-validate-sessions" action="Administration/addvise/saveAllInvitations" method="post">
            <button type="button" id="btn-validate-addvise-session" class="btn btn-lg btn-success">
                <span class="glyphicon glyphicon-save"></span> Sauvegarder
            </button>
        </form>
    </div>

    <div id="addvise-legende">
        <div class="addvise-legend-bloc">
            <div class="legende">
                <img class="img-legende" src="${contextPath}/resources/images/addvise/info.gif"> Potentiel
            </div>
            <img class="img-legende" src="${contextPath}/resources/images/addvise/send.gif"> Envoyé
        </div>
        <div class="addvise-legend-bloc">
            <div class="legende">
                <img class="img-legende" src="${contextPath}/resources/images/addvise/checked.gif"> Confirmé
            </div>
            <div class="legende">
                <img class="img-legende" src="${contextPath}/resources/images/addvise/unchecked.gif"> Décliné
            </div>
        </div>
        <div class="addvise-legend-bloc">
            <div class="legende">
                <div class="img-legende img-legende-bleu"></div>
                Session non confirmée
            </div>
            <div class="legende">
                <div class="img-legende img-legende-verte"></div>
                Session confirmée
            </div>
        </div>
    </div>
</div>

<!-- Modale de création de session -->
<div class="modal fade" id="addvise-modal-creation-session" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Création d'une nouvelle session Addvise</h4>
            </div>
            <div class="modal-body" id="addvise-modal-creation-session-body">
                <!-- Contenu de la modale -->
                <div id="addvise-fields-creation-session"> <!-- Créer une session Addvise -->
                    <h4>Créer une nouvelle session :</h4>
                    <form class="addvise-session-form" action="Administration/addvise/addNewSession" method="post" >
                        <select id="addvise-field-creation-modification-module" class="form-control" name="module">
                            <c:forEach items="${listeModules}" var="module">
                                <option value="${module.id}">${module.codeModuleAddvise} : ${module.nomModuleAddvise}</option>
                            </c:forEach>
                        </select>
                        <input id="addvise-field-creation-session-date"
                               class="datepicker-creation-session form-control"
                               placeholder="Date" name="date">
                        <select id="addvise-field-creation-session-formateur" class="form-control" name="formateur">
                            <c:forEach items="${listeFormateurs}" var="formateur">
                                <option value="${formateur.id}">${formateur.nom} ${formateur.prenom}</option>
                            </c:forEach>
                        </select>
                        <button type="submit" id="addvise-create-session"
                                class="btn btn-success addvise-create-button">
                            Créer
                        </button>
                    </form>
                </div>
                <div id="addvise-fields-creation-module"> <!-- créer un nouveau module -->
                    <h4>Ajouter un nouveau module (M1, M2...) :</h4>
                    <input type="text" id="addvise-field-creation-module-code" class="form-control"
                           placeholder="Code"/>
                    <input type="text" id="addvise-field-creation-module-nom" class="form-control"
                           placeholder="Nom"/>
                    <button type="submit" id="addvise-create-module"
                            class="btn btn-success addvise-create-button">
                        Ajouter
                    </button>
                    <div class="addvise-msg"></div>
                </div>
                <div id="addvise-fields-creation-formateur"> <!-- créer un formateur -->
                    <h4 class="nouveau-formateur">Ajouter un nouveau formateur :</h4>
                    <input type="text" id="addvise-create-formateur-nom" class="form-control" placeholder="Nom"/>
                    <input type="text" id="addvise-create-formateur-prenom" class="form-control"
                           placeholder="Prénom"/>
                    <input type="email" id="addvise-create-formateur-email" class="form-control"
                           placeholder="e-mail"/>
                    <button type="button" id="addvise-create-formateur"
                            class="btn btn-success addvise-create-button">
                        Ajouter
                    </button>
                    <div class="addvise-msg"></div>
                </div>
                <div id="addvise-fields-creation-help">
                    <h4>Aide à la création d'une session :</h4>
                    <table id="addvise-fields-creation-help-table text-center"
                           class="table table-bordered table-hover border">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Nombre de sessions réalisées</th>
                            <th>Dernière session</th>
                            <th>Nombre de collaborateurs devant faire ce module</th>
                        </tr>
                        </thead>
                        <c:forEach items="${aideCreationStats}" var="row">
                            <tr>
                                <c:forEach items="${row}" var="data">
                                    <td>${data}</td>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<!-- Modales suppression des sessions -->

<div id="modalesAddvise">
    <div class="modal fade" id="modale-remove-session" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Supprimer la session ?</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-remove-session">Voulez-vous supprimer cette session ? Les changements effectués
                        seront
                        sauvegardés.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-modale-remove-session" class="btn btn-success">Oui</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modale de création de session -->
<div class="modal fade" id="addvise-modal-modification-session" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modification d'une nouvelle session Addvise</h4>
            </div>
            <div class="modal-body" id="addvise-modal-modification-session-body">
                <!-- Contenu de la modale -->
                <div id="addvise-fields-modification-session"> <!-- Modifier une session Addvise -->
                    <h4>Modifier une session :</h4>
                    <form class="addvise-session-form" id="form-edit" action="Administration/addvise/editSession" method="post">
                        <select id="addvise-field-modification-session-module" class="form-control" name="module">
                            <c:forEach items="${listeModules}" var="module">
                                <option value="${module.id}">${module.codeModuleAddvise} : ${module.nomModuleAddvise}</option>
                            </c:forEach>
                        </select>
                        <input id="addvise-field-modification-session-date"
                               class="datepicker-creation-session form-control"
                               placeholder="Date" name="date">

                        <select id="addvise-field-modification-session-formateur" class="form-control" name="formateur">
                            <c:forEach items="${listeFormateurs}" var="formateur">
                                <option value="${formateur.id}">${formateur.nom} ${formateur.prenom}</option>
                            </c:forEach>
                        </select>
                        <button type="submit" id="btn-modale-edit-session"
                                class="btn btn-success addvise-create-button" name="btn-modale-edit-session">
                            Modifier
                        </button>
                    </form>
                </div>
                <!--<div id="addvise-fields-creation-module"> //créer un nouveau module
                    <h4>Ajouter un nouveau module (M1, M2...) :</h4>
                    <input type="text" id="addvise-field-creation-module-code" class="form-control"
                           placeholder="Code"/>
                    <input type="text" id="addvise-field-creation-module-nom" class="form-control"
                           placeholder="Nom"/>
                    <button type="button" id="addvise-create-module" class="btn btn-success addvise-create-button">
                        Ajouter
                    </button>
                    <div class="addvise-msg"></div>
                </div>
                <div id="addvise-fields-creation-formateur"> //créer un formateur
                    <h4 class="nouveau-formateur">Ajouter un nouveau formateur :</h4>
                    <input type="text" id="addvise-create-formateur-nom" class="form-control" placeholder="Nom"/>
                    <input type="text" id="addvise-create-formateur-prenom" class="form-control"
                           placeholder="Prénom"/>
                    <input type="email" id="addvise-create-formateur-email" class="form-control"
                           placeholder="e-mail"/>
                    <button type="button" id="addvise-create-formateur"
                            class="btn btn-success addvise-create-button">
                        Ajouter
                    </button>
                    <div class="addvise-msg"></div>
                </div>
                <div id="addvise-fields-creation-help">
                    <h4>Aide à la création d'une session :</h4>
                    <table id="addvise-fields-creation-help-table text-center"
                           class="table table-bordered table-hover border">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Nombre de sessions réalisées</th>
                            <th>Dernière session</th>
                            <th>Nombre de collaborateurs devant faire ce module</th>
                        </tr>
                        </thead>
                        <c:forEach items="${aideCreationStats}" var="row">
                            <tr>
                                <c:forEach items="${row}" var="data">
                                    <td>${data}</td>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                    </table>
                </div>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>


