<%--
  Created by IntelliJ IDEA.
  User: alosat
  Date: 27/06/2017
  Time: 09:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="resources/css/jquery-ui.css" rel="stylesheet" />
<link href="resources/css/jquery.ui.theme.css" rel="stylesheet" />
<link href="resources/css/timelineScheduler.css" rel="stylesheet" />
<link href="resources/css/timelineScheduler.styling.css" rel="stylesheet" />
<link href="resources/css/calendar.css" rel="stylesheet" />
<script src="resources/js/Administration/moment.min.js"></script>
<script src="resources/js/Administration/timelineScheduler.js"></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Consultations des disponibilités</li>
</ol>

<div class="col-md-12">
    <form:form cssClass="form-inline" method="POST" action="Administration/consultationDisponibilites" id="filtreForm">
        <sec:authorize access="hasAnyAuthority('DIR, CP, RT')">
            <div class="form-group filtreResponsable">
                <select name="filtreResponsable"
                    id="filtreResponsable"
                    class="selectpicker form-control show-tick"
                    data-live-search="true"
                    data-header="Choisir un responsable"
                    multiple
                    data-max-options="1"
                    data-none-selected-text="Aucun responsable"
                    data-hide-disabled="true"
                    onchange="changeResponsable()">
                    <optgroup label="Directeurs">
                        <c:forEach items="${listeDirecteurs}" var="directeur">
                            <option value="${directeur.id}">${directeur.prenom} ${directeur.nom}</option>
                        </c:forEach>
                    </optgroup>
                    <optgroup label="Managers">
                         <c:forEach items="${listeManagers}" var="manager">
                            <option value="${manager.id}">${manager.prenom} ${manager.nom}</option>
                        </c:forEach>
                    </optgroup>
                    <optgroup label="Responsables Technique">
                        <c:forEach items="${listeResponsables}" var="responsable">
                            <option value="${responsable.id}">${responsable.prenom} ${responsable.nom}</option>
                        </c:forEach>
                    </optgroup>
                </select>
            </div>
        </sec:authorize>
            <div class="form-group filtreCollaborateur">
                <select name="filtreCollaborateur"
                        id="filtreCollaborateur"
                        class="form-control selectpicker show-tick"
                        data-live-search="true" data-header="Choisir un collaborateur"
                        multiple
                        data-max-options="1"
                        data-none-selected-text="Aucun collaborateur"
                        data-hide-disabled="true">
                    <optgroup label="Liste des consultants">
                    <c:forEach items="${listeCollaborateursFixe}" var="collaborateur">
                        <option  value="${collaborateur.id}" data-id-manager="${collaborateur.manager.id}">${collaborateur.prenom} ${collaborateur.nom}</option>
                    </c:forEach>
                    </optgroup>
                </select>
            </div>

    </form:form>
    <div class="col col-md-6"><a onclick="resetFilters()" class="annuleFiltre">Annuler filtre</a></div>
</div>

<div class="calendar col-md-12" style="padding-bottom: 35px">
</div>

<script type="text/javascript">
    <%-- Définition des parametres en fraçais, comme le nom des mois, des jours etc) --%>
    moment.locale("fr");
    <%-- Defini la date de début à l'initialisation--%>
    var today = moment().startOf('day');

    var Calendar = {
        <%-- Definicion d'une periode --%>
        Periods: [

            {
                Name: '1 month',
                Label: '1 m<span class="toto">ois</span>',
                TimeframePeriod: (60 * 24 * 1),
                TimeframeOverall: (60 * 24 * 30),
                TimeframeHeaders: [
                    'MMM',
                    'Do'
                ],
                Classes: 'period-1month'
            },
            {
                Name: '3 month',
                Label: '3 m<span class="toto">ois</span>',
                TimeframePeriod: (60 * 24 * 3),
                TimeframeOverall: (60 * 24 * 90),
                TimeframeHeaders: [
                    'MMM',
                    'Do'
                ],
                Classes: 'period-1month'
            },
            {
                Name: '6 month',
                Label: '6 m<span class="toto">ois</span>',
                TimeframePeriod: (60 * 24 * 7),
                TimeframeOverall: (60 * 24 * 180),
                TimeframeHeaders: [
                    'MMM',
                    'Do'
                ],
                Classes: 'period-1month'
            },
            {
                Name: '1 year',
                Label: '1 an',
                TimeframePeriod: (60 * 24 * 14 ),
                TimeframeOverall: (60 * 24 * 360),
                TimeframeHeaders: [
                    'MMM',
                    'Do'
                ],
                Classes: 'period-1month'
            }
        ],

        <%-- exemple de définition d'un item --%>
        Items: [
            <c:forEach items="${listeMissions}" var="mission">
                {
                    id: ${mission.id},
                    name: '<div></div>',
                    sectionID: ${mission.collaborateur.id},
                    start: moment("${mission.dateDebut}"),
                    end: moment("${mission.dateFin}"),
                    classes: 'item-status-two'

                },
            </c:forEach>
            <c:forEach items="${listeAbsencesSoumises}" var="absence">
                {
                    id: ${absence.id},
                    name: '<div></div>',
                    sectionID: ${absence.collaborateur.id},
                    start: moment("${absence.dateDebut}"),
                    end: moment("${absence.dateFin}"),
                    classes: 'item-status-three'
                },
            </c:forEach>
            <c:forEach items="${listeAbsencesValidees}" var="absence">
            {
                id: ${absence.id},
                name: '<div></div>',
                sectionID: ${absence.collaborateur.id},
                start: moment("${absence.dateDebut}"),
                end: moment("${absence.dateFin}"),
                classes: 'item-status-one'
            },
          </c:forEach>
            <%--<c:forEach items="${listeJoursFeries}" var="jourFerie">--%>
            <%--{--%>
                <%--<c:forEach items="${listeCollaborateurs}" var="collaborateur">--%>
                <%--id:${collaborateur.id},--%>
                <%--name: '<div></div>',--%>
                <%--sectionID : ${collaborateur.id},--%>
                <%--start : moment("${jourFerie.dateDabut}"),--%>
                <%--end : moment("${jourFerie.dateFin}"),--%>
                <%--classes:'item-status-four'--%>
                <%--</c:forEach>--%>
            <%--},--%>
            <%--</c:forEach>--%>
        ],

        Sections: [
            <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                {
                    id: ${collaborateur.id},
                    name: '${collaborateur.prenom} ${collaborateur.nom}'
                },
            </c:forEach>
        ],

        Init: function () {
            TimeScheduler.Options.GetSections = Calendar.GetSections;
            TimeScheduler.Options.GetSchedule = Calendar.GetSchedule;
            TimeScheduler.Options.Start = today;
            TimeScheduler.Options.Periods = Calendar.Periods;
            TimeScheduler.Options.SelectedPeriod = '1 month';
            TimeScheduler.Options.SelectedCollaborator = "";
            TimeScheduler.Options.Element = $('.calendar');

            TimeScheduler.Options.AllowDragging = false;
            TimeScheduler.Options.AllowResizing = false;

            TimeScheduler.Options.Events.ItemClicked = Calendar.Item_Clicked;
            TimeScheduler.Options.Events.ItemDropped = Calendar.Item_Dragged;
            TimeScheduler.Options.Events.ItemResized = Calendar.Item_Resized;

            TimeScheduler.Options.Events.ItemMovement = Calendar.Item_Movement;
            TimeScheduler.Options.Events.ItemMovementStart = Calendar.Item_MovementStart;
            TimeScheduler.Options.Events.ItemMovementEnd = Calendar.Item_MovementEnd;

            TimeScheduler.Options.Text.NextButton = '&nbsp;';
            TimeScheduler.Options.Text.PrevButton = '&nbsp;';

            TimeScheduler.Options.MaxHeight = 100;

            TimeScheduler.Init();
        },

        GetSections: function (callback) {
            callback(Calendar.Sections);
        },

        GetSchedule: function (callback, start, end) {
            callback(Calendar.Items);
        },

        Item_Clicked: function (item) {
            console.log(item);
        },

        Item_Dragged: function (item, sectionID, start, end) {
            var foundItem;

            for (var i = 0; i < Calendar.Items.length; i++) {
                foundItem = Calendar.Items[i];

                if (foundItem.id === item.id) {
                    foundItem.sectionID = sectionID;
                    foundItem.start = start;
                    foundItem.end = end;

                    Calendar.Items[i] = foundItem;
                }
            }

            TimeScheduler.Init();
        },


        Item_Movement: function (item, start, end) {
            var html;

            html =  '<div>';
            html += '   <div>';
            html += '       Start: ' + start.format('Do MMM YYYY HH:mm');
            html += '   </div>';
            html += '   <div>';
            html += '       End: ' + end.format('Do MMM YYYY HH:mm');
            html += '   </div>';
            html += '</div>';

            $('.realtime-info').empty().append(html);
        },

        Item_MovementStart: function () {
            $('.realtime-info').show();
        },

        Item_MovementEnd: function () {
            $('.realtime-info').hide();
        }
    };

    $(document).ready(Calendar.Init);

</script>

<script type="text/javascript">

    var idManager = "${idManager}";
    var idCollaborateur = "${idCollaborateur}";

    $(document).ready(function () {
        removeSpace();
        addSelectedOnResponsable(idManager);
        addSelectedOnCollab(idCollaborateur);

        $('#filtreCollaborateur').on('change', function() {
            var selectedCollaborateur = $(this).val();
            TimeScheduler.Options.SelectedCollaborator = selectedCollaborateur;
            TimeScheduler.Init();
        });
    });

    function addSelectedOnResponsable(idManager) {
        if (idManager) {
            $('#filtreResponsable').val(idManager);

            var index = $("#filtreResponsable").find("option[value='" + idManager + "']").index();
            $('#filtreForm .filtreResponsable ul').find("li[data-original-index='" + index + "']").addClass("selected");

            var manager = $('#filtreForm .filtreResponsable ul li[class=selected] span').html();
            $('#filtreForm .filtreResponsable ul li[class=selected] a').attr('aria-selected', "true");
            $('#filtreForm > div.form-group.filtreResponsable > div > button').attr('title', manager);
            $('#filtreForm .filtreResponsable > div > button > span.filter-option.pull-left').html(manager);

        } else {
            $('#selectResponsable ul.dropdown-menu').find("li").removeClass("selected");
        }

        changeResponsable();
    }

    function addSelectedOnCollab(idCollaborateur) {
        if (idCollaborateur) {

            $('#filtreCollaborateur').val(idCollaborateur);

            var indexCollab = $("#filtreCollaborateur").find("option[value='" + idCollaborateur + "']").index();
            $('#filtreForm div.form-group.filtreCollaborateur ul').find("li[data-original-index='" + indexCollab + "']").addClass("selected");

            var collaborateur = $('#filtreForm .filtreCollaborateur ul li[class=selected] span').html();
            $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "true");
            $('.filtreCollaborateur span.filter-option.pull-left').html(collaborateur);
            $('.filtreCollaborateur > div > button').attr('title', collaborateur);

        } else {
            $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");
        }
    }

    function changeResponsable() {

        $('#filtreCollaborateur').val("");
        $('.filtreCollaborateur span.filter-option.pull-left').html("Aucun Collaborateur");
        $('.filtreCollaborateur > div > button').attr('title', 'Aucun Collaborateur');
        $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "false");
        $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");

        var selectedVal = $("#filtreResponsable").find("option:selected").val();
        var i = 0;

        if (selectedVal != null) {
            $("#filtreCollaborateur option").filter(function () {
                var equal = $(this).attr('data-id-manager') == selectedVal;
                if (equal) {
                    $('#filtreForm .filtreCollaborateur ul').find("li[data-original-index='" + i + "']").show();
                } else {
                    $('#filtreForm .filtreCollaborateur ul').find("li[data-original-index='" + i + "']").hide();
                }
                i++;
            });
        } else {
            $('#filtreForm .filtreCollaborateur ul li').show();
        }
    }

    function removeSpace() {
        if ($('#filtreResponsable').is(':visible')) {
            $('#filtreForm').addClass('grid-display-dir-cp');
            $('#filtreResponsable').addClass('grid-col-1');
            $('#filtreCollaborateur').addClass('grid-col-2');
            $('#valider').addClass('grid-col-3');
        } else {
            $('#filtreForm').addClass('grid-display-man-rt');
            $('#filtreCollaborateur').addClass('grid-col-1');
            $('#valider').addClass('grid-col-2');
        }
    }
    function annulerFiltre() {
        window.location.href = window.location.pathname;
    }

    function resetFilters() {
        TimeScheduler.Options.SelectedPeriod = '1 month';
        TimeScheduler.Options.SelectedCollaborator = "";
        TimeScheduler.Init();

        // reset filter manager
        $('#filtreResponsable').val("");
        $('.filtreResponsable span.filter-option.pull-left').html("Aucun Responsable");
        $('.filtreResponsable > div > button').attr('title', 'Aucun Responsable');
        $('.filtreResponsable > div > button').addClass('bs-placeholder');
        $('#filtreForm .filtreResponsable ul li[class=selected] a').attr('aria-selected', "false");
        $('#filtreForm .filtreResponsable ul').find("li").removeClass("selected");
        $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "false");

        //reset filter collaborator
        $('#filtreCollaborateur').val("");
        $('.filtreCollaborateur span.filter-option.pull-left').html("Aucun Collaborateur");
        $('.filtreCollaborateur > div > button').attr('title', 'Aucun Collaborateur');
        $('.filtreCollaborateur > div > button').addClass('bs-placeholder');
        $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");
        $('#filtreForm .filtreCollaborateur ul li').show();

        $('.time-sch-section-row').show();
        $('.time-sch-section-container').show();
    }
</script>
