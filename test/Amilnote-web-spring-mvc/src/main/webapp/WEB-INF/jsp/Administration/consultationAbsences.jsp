<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
 --%>

<!-- Création d'une variable avec la date now() -->
<jsp:useBean id="today" class="java.util.Date"/>

<script type="text/javascript" src='resources/js/modalCommentaire.js'></script>
<script type="text/javascript" src='resources/js/Administration/validationAbsence.js'></script>
<script type="text/javascript" src='resources/js/Administration/consultationAbsence.js'></script>
<script type="text/javascript" src='resources/js/librairie.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Consultation des absences</li>
</ol>

<!-- variables à ajouter aux GETs/POSTs pour les redirections -->
<c:set var="qString"
       scope="session"
       value="${pageContext.request.queryString}"/>

<c:set var="qStringWithQMark"
       scope="session"
       value="${not empty qString?'?'+=qString:''}"/>

<!-- to use with GETs -->
<c:set var="refererReqParam"
       scope="session"
       value="?refererUri=${requestScope['javax.servlet.forward.request_uri']}${qStringWithQMark}"/>

<!-- to use with POSTs -->
<c:set var="refererPostReqParam"
       scope="session"
       value="${requestScope['javax.servlet.forward.request_uri']}${qStringWithQMark}"/>

<div id="idTableContainer">

    <h3>Gestion des absences - ${moisDuFiltre} ${anneeDuFiltre}</h3>
    <a href="${administration}${consultationAbsences}/null${qStringWithQMark}" id="lienFiltre">Toutes</a>
    <a href="${administration}${consultationAbsences}/VA${qStringWithQMark}" id="lienFiltre">Validées</a>
    <a href="${administration}${consultationAbsences}/SO${qStringWithQMark}" id="lienFiltre">Soumises</a>
    <a href="${administration}${consultationAbsences}/RE${qStringWithQMark}" id="lienFiltre">Refusées</a>

    <FORM id="formFiltre" method="GET">
        <div class="form-group" style="margin-top:27px">
            <input name="monthYearExtract" id="monthExtract" class="monthPicker form-control" placeholder="MM/AAAA"
                   autocomplete="off"
                   value="${monthYearExtract}">
            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </FORM>

    <!-- Tableau avec la liste des absences
            data-height="700" -->
    <div id="mini-scrolltab" style="height: calc(100% - 375px);">
        <table data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbar"
               data-pagination="true"
               data-page-size=10
               data-page-list="[10, 25, 50, 100, ${listAbsences.size()}]"
               data-pagination-v-align="top"
               data-pagination-h-align="right"
               data-pagination-detail-h-align="left"
               id="idBootstrapTable"
               data-maintain-selected="true"
               data-cookie="true"
               data-cookie-id-table="saveIdConsultAbs">
            <!-- En tête du tableau et configuration de bootstrapTable -->
            <thead>
            <tr>
                <th data-field="state" data-checkbox="true" data-formatter="stateFormatter"></th>
                <th class="col-md-1" data-field="id" data-visible="false">id</th>
                <th class="col-md-1" data-field="code" data-visible="false">code</th>
                <th class="col-md-3" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                <th class="col-md-3" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <!-- <th class="col-md-1" data-sortable="true" data-align="center">Email</th> -->
                <th class="col-md-2" data-field="type" data-sortable="true" data-align="center">Type</th>
                <th class="col-md-0" data-field="duree" data-sortable="true" data-align="center">Durée</th>
                <th class="col-md-2" data-field="debut" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                    data-align="center">Début
                </th>
                <th class="col-md-2" data-field="fin" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                    data-align="center">Fin
                </th>
                <th class="col-md-1" data-field="etat" data-sortable="true" data-align="center">État</th>
                <th class="col-md-0" data-align="center">Détails</th>
                <th class="col-md-0" data-align="center">Annuler</th>
                <th class="col-md-0" data-field="valider" data-align="center">Valider</th>
                <th class="col-md-0" data-field="refuser" data-align="center">Refuser</th>
            </tr>
            </thead>
            <!-- Corps du tableau -->
            <tbody>
            <!-- Boucle sur la liste des absences pour afficher chaque ligne -->

            <c:forEach items="${listAbsences}" var="absence">
                <c:if test="${absence.etat.code!='BR'}">
                    <tr>
                        <td></td> <!-- checkbox -->
                        <td>${absence.id}</td>
                        <td>${absence.etat.code}</td>
                        <td>${absence.collaborateur.prenom}</td>
                        <td>${absence.collaborateur.nom}</td>
                        <!-- <td>${absence.collaborateur.mail}</td> -->
                        <td>${absence.typeAbsence.typeAbsence}</td>
                        <td>${absence.nbJours}</td>

                        <td><fmt:formatDate value="${absence.dateDebut}" pattern="dd/MM/yyyy HH:mm"/></td>
                        <td><fmt:formatDate value="${absence.dateFin}" pattern="dd/MM/yyyy HH:mm"/></td>
                        <td>${absence.etat.etat}</td>
                        <!-- Bouton pour ouvrir la fenêtre modale avec le calendrier -->
                        <td>
                            <a onclick="eventClikOnBtnShow( this )"
                               data-absenceid="${absence.id}"
                               data-datedebut="${absence.dateDebut}"
                               data-typeabsence="${absence.typeAbsence.code}"
                               data-toggle="tooltip"
                               data-placement="left"
                               class="btnShowListEvents"
                                <c:choose>
                                    <%--Tooltip absence comment when onmouseover--%>
                                    <c:when test="${ fn:trim(absence.commentaire) != '' }">
                                        data-original-title="${absence.commentaire}"
                                    </c:when>
                                    <c:otherwise>
                                        data-original-title="Voir détails"
                                    </c:otherwise>
                                </c:choose>
                            >
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                        </td>
                        <!-- Bouton pour annuler l'absence -->
                        <td>
                            <a class="openModalCommentaire" data-title="Annulation de l'absence"
                               data-href="${administration}${annulationAbsences}/${absence.id}">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                            </a>
                        </td>

                        <!-- Bouton pour accepter l'absence -->
                        <td>
                            <c:choose>
                                <c:when test="${ absence.etat.code!='SO' }">
                                    -
                                </c:when>
                                <c:otherwise>
                                    <a href="${administration}${validationsAbsences}/${absence.id}${VA}${refererReqParam}"
                                       onclick="clickAndDisable(this);">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </td>

                        <!-- Bouton pour refuser l'absence -->
                        <td>
                            <c:choose>
                                <c:when test="${ absence.etat.code!='SO' }">
                                    -
                                </c:when>
                                <c:otherwise>
                                    <a
                                            class="openModalCommentaire"
                                            data-title="Refus de l'absence"
                                            data-href="${administration}${validationsAbsences}/${absence.id}${RE}"
                                            onclick="clickAndDisable(this);"
                                    >
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>

        <!-- boutons de validation multiple -->
        <div class="navbar-form navbar-left">
            <input
                    class="btn btn-default btnActionSelection"
                    value="Valider la sélection"
                    data-action="${administration}${validationsMultiplesAbsences}${VA}"
                    data-refuri="${refererPostReqParam}"
            >
            <input
                    class="btn btn-default btnActionSelection"
                    value="Refuser la sélection"
                    data-action="${administration}${validationsMultiplesAbsences}${RE}"
                    data-refuri="${refererPostReqParam}"
            >
        </div>
    </div>
</div>

<!-- Fenetre modale pour afficher le calendrier de détail de l'absence sélectionnée -->
<div class="modal fade" id="idModalShowAbsence" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Détails de l'absence</h3>
            </div>
            <div class="modal-body">
                <div id="calendarAbsence"></div>
                <div id="absence-comment"></div>
            </div>
            <div class="modal-footer"></div>

        </div>
    </div>
</div>