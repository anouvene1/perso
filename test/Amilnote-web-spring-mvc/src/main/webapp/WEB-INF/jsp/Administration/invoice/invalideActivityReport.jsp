<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="tab-pane" id="tabRAnonValide">
    <div id="divRANonSoumis">

        <%-- [CLO][AMNOTE 308] Tableau des RA non validés - REFONTE --%>
        <p></p>
        <legend class="text-center">
            <h3>RA non validés</h3>
            <p class="headerLabel">Futures factures qui pourront être facturées une fois que le collaborateur
                correpondant aura validé son RA.</p>
            <br/>
        </legend>
        <fieldset>

            <c:set var="verifListeRANonSoumisVide" scope="session" value="0"/>
            <c:forEach items="${listFacturesRANonSoumis}" var="itemRA">
                <c:if test="${itemRA.etat.id != 10}">
                    <c:set var="verifListeRANonSoumisVide" scope="session" value="1"/>
                </c:if>
            </c:forEach>
            <c:choose>
                <%-- SBE_AMNOTE-175 Si la liste n'est pas vide, le bouton est clicable, sinon non --%>
                <c:when test="${verifListeRANonSoumisVide==1}">
                    <a href="${administration}/factures/genererExcel/travailleurs/RANonSoumis/${dateVoulueRefonte}"
                       id="exportRANonSoumis"
                       class="exportExcelRANonSoumis btn btn-primary"> Exporter </a>
                </c:when>
                <c:otherwise>
                    <a id="margeEnBasBoutton" class="exportExcelRANonSoumis btn btn-default" disabled="true">
                        Exporter </a>
                </c:otherwise>
            </c:choose>

            <sec:authorize access="hasAnyAuthority('DRH')">
                <c:choose>
                    <c:when test="${forcerFactureOK}">
                        <a class="btn btn-danger" onclick="forcerFactures()"> Forcer les factures </a>
                    </c:when>
                    <c:otherwise>
                        <a class="btn btn-default" disabled="true"> Forcer les factures </a>
                    </c:otherwise>
                </c:choose>
            </sec:authorize>

            <table class="table"
                   data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbarCollabSansCommande"
                   data-pagination="true"
                   data-page-size=20
                   data-page-list="[10, 20, 30, 40,50]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-pagination-detail-h-align="left"
                   data-sort-name="nom"
                   data-sort-order="asc"
                   id="idBootstrapTable5"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditCollabSansFacture">
                <thead>
                <tr>
                    <th data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur</th>
                    <th data-field="client" data-sortable="true" data-align="center">Client</th>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${listFacturesRANonSoumis}" var="facture">
                    <c:if test="${facture.mission.collaborateur.enabled}">
                        <tr>
                            <td><c:out value="${facture.mission.collaborateur}"/></td>
                            <c:choose>
                                <c:when test="${facture.commande!=null}">
                                    <td><c:out value="${facture.commande.client.nom_societe}"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td><c:out value="${facture.mission.client.nom_societe}"/></td>
                                </c:otherwise>
                            </c:choose>
                            <td><c:out value="${facture.mission.mission}"/></td>
                            <c:choose>
                                <c:when test="${facture.commande!=null}">
                                    <td><c:out value="${facture.commande.numero}"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td><span class="manqueCommande">Suivant proposition commerciale</span></td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </fieldset>
        <br/><br/>
    </div>
</div>