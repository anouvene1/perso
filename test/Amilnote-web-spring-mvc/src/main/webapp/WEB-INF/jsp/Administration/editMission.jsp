<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript">
    var pWithForfaitFromController = ${withForfait};
    var urlForfait = "Administration/editCollaborateurForfait/" + ${collaborateur.id};
    var urlEditMission = "Administration/editCollaborateur/" + ${collaborateur.id} +"/editMission";
    var pEditMission = ${!empty mission.id};
    var urlAddForfait = "Administration/addForfait/" + ${collaborateur.id};
    var urlRemoweForfait = "Administration/removeForfait/" + ${collaborateur.id};
    var idCollaborator = ${collaborateur.id};
</script>
<script type="text/javascript" src='resources/js/Administration/editMission.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<section id="messageUtilisateur" class="alert alert-warning" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<ol class="breadcrumb">
    <li><a href="${ welcome }" onclick="clearCookies()">Accueil</a></li>
    <li><a href="${ welcome }" onclick="clearCookies()">Administration</a></li>
    <c:choose>
        <c:when test="${collaborateur.statut.id=='7'}">
            <li><a href="${ administration }${ travailleurs }?type=idTableContainerSS">Consultants</a></li>
            <li><a
                    href="${administration}${editTravailleur}/sous_traitant/${ collaborateur.id }/null" onclick="clearCookies()">Édition
                du sous-traitant</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="${ administration }${ travailleurs }?type=idTableContainer">Consultants</a></li>
            <li><a
                    href="${administration}${editTravailleur}/collaborateur/${ collaborateur.id }/null" onclick="clearCookies()">Édition
                du collaborateur</a></li>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${ !empty mission.id }">
            <li class="active">Édition de la mission</li>
        </c:when>
        <c:otherwise>
            <li class="active">Création de la mission</li>
        </c:otherwise>
    </c:choose>
</ol>

<div id="idProfilContainer">
    <form:form class="form-horizontal" method="POST"
               action="${administration}/${collaborateur.id}/${saveMission}"
               commandName="mission" name="formMission"
               onsubmit="return testForm(this)">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center">
                <c:choose>
                    <c:when test="${empty mission.id }">
                        Création d'une mission
                    </c:when>
                    <c:otherwise>
                        Modification de la mission :   ${ mission.mission }
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${!empty collaborateur.id}">
                        <c:choose>
                            <c:when test="${typeTravailleur=='sous_traitant'}">
                                pour :   ${ collaborateur.mail }
                            </c:when>
                            <c:otherwise>
                                pour :   ${ collaborateur.mail }
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${typeTravailleur=='sous_traitant'}">
                                Création du sous-traitant :   ${ collaborateur.mail }
                            </c:when>
                            <c:otherwise>
                                Création du collaborateur :   ${ collaborateur.mail }
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>

            </legend>
            <form:hidden id="idMission" path="id" value="${ id }"/>
            <form:hidden path="collaborateur"/>
            <c:if test="${not empty error}">
                <div class="errorblock">
                        ${error}
                </div>
            </c:if>
            <c:if test="${ empty mission.id && collaborateur.statut.id != 7}">
                <!-- checkbox ajout forfait si on est en création -->
                <div class="form-group" align="center">
                    <label class="col-md-6 control-label" style="font-size:large">Mission avec forfait ?</label>
                    <div class="col-md-1 ">
                        <div class="checkbox">
                            <label>
                                <input class="check-box CheckBoxClass" id="idWithForfait" type="checkbox">
                            </label>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idNomMiss" id="nom">Nom<span
                        style="color:red">*</span></label>
                <div class="col-md-4">

                    <form:input path="mission" cssClass="form-control input-md"
                                data-validation-length="2-50" data-validation="length" id="nomMission"
                                name="nomMission"/>

                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idDateDebutMiss">Date
                    de début<span style="color:red">*</span></label>
                <div class="col-md-4">

                    <form:input type="text"
                                path="dateDebut"
                                class="form-control input-md datepicker"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                                id="dateDebut"
                                name="dateDebut"
                                autocomplete="off"/>

                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idDateFinMiss">Date
                    de fin<span style="color:red">*</span></label>
                <div class="col-md-4">

                    <form:input type="text"
                                path="dateFin"
                                class="form-control input-md datepicker"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                                id="dateFin"
                                name="dateFin"
                                autocomplete="off"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="description">Description de la mission<span
                        style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:textarea id="description"
                                   path="description"
                                   placeholder="Description"
                                   class="form-control input-md"
                                   required="required"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="duree">Durée hebdomadaire (heures)</label>
                <div class="col-md-4">
                    <c:choose>
                        <c:when test="${mission.heuresHebdo !=0}">
                            <form:input id="duree" path="heuresHebdo" type="text"
                                        placeholder="Durée hebdomadaire de la mission" class="form-control input-md"
                                        data-validation="number" data-validation-allowing="float"
                                        data-validation-decimal-separator=","
                                        value="${mission.heuresHebdo}"
                                        onkeyup="remplaceVirgule(this)"/>
                        </c:when>
                        <c:otherwise>
                            <form:input id="duree" path="heuresHebdo" type="text"
                                        placeholder="Durée hebdomadaire de la mission" class="form-control input-md"
                                        data-validation="number" data-validation-allowing="float"
                                        data-validation-decimal-separator=","
                                        value="36.5"
                                        onkeyup="remplaceVirgule(this)"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Type
                    de mission</label>
                <div class="col-md-4">
                    <form:select name="listTypesMission" class="form-control" path="typeMission.id"
                                 id="listTypesMission" onchange="changeTypeMission()">
                        <c:forEach items="${typesMission}" var="typeMission">
                            <form:option id="typeMissionSelected"
                                         value="${typeMission.id}">${typeMission.typeMission}</form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>

            <div id="divClient">
                <!-- Mission client -->
                <!-- CheckBoxes perimetre mission -->
                <!-- création des checkboxes du choix des périmètres mission -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Périmètre(s) mission</label>
                    <div class="col-md-4">
                        <label style="display: block;">
                            <input class="check-box perimetreMission" id="perimetreMission_1"
                                   name="perimetreMission_1"
                                   value="1"
                                   type="checkbox"/> Métier de l'informatique et administratif
                        </label>
                        <label style="display: block;">
                            <input class="check-box perimetreMission" id="perimetreMission_2"
                                   name="perimetreMission_2"
                                   value="2"
                                   type="checkbox"/> Métier de l'industrie
                        </label>
                        <label style="display: block;">
                            <input class="check-box perimetreMission" id="perimetreMission_3"
                                   name="perimetreMission_3"
                                   value="3"
                                   type="checkbox"/> Activité commerciale / Déplacement professionnel
                        </label>
                        <c:forEach items="${listIdPerimetreMissionChecked}" var="idChecked">
                            <script>
                                document.getElementById('perimetreMission_${idChecked}').checked = true;
                            </script>
                        </c:forEach>
                    </div>
                </div>

                <!-- Choix du client  -->
                <div class="form-group missionCliente">
                    <label class="col-md-4 control-label" for="listClients">Client</label>
                    <div class="col-md-4">
                        <form:select name="listClients" class="form-control" path="client.id" id="listClients"
                                     placeholder="Sélectionner un client">
                            <c:if test="${clientSelectedJson != null}">
                                <form:option
                                        value="${clientSelectedJson.id}">${clientSelectedJson.nom_societe}</form:option>
                            </c:if>
                            <c:forEach items="${listClients}" var="client">
                                <c:if test="${clientSelectedJson.id != client.id}">
                                    <form:option value="${client.id}">${client.nom_societe}</form:option>
                                </c:if>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>

                <!-- TJM  -->
                <div class="form-group missionCliente">
                    <label class="col-md-4 control-label" for="tjm">TJM<span style="color: red;">*</span></label>
                    <div class="col-md-4">
                        <form:input id="tjm" path="tjm" type="text"
                                    placeholder="tjm de la mission" class="form-control input-md valid"
                                    data-validation="number" data-validation-allowing="float"
                                    data-validation-decimal-separator=","
                                    value="${tjm}"
                                    onkeyup="remplaceVirgule(this)"
                                    required="required"/>
                    </div>
                </div>

                <!-- Choix responsable client  -->
                <div class="form-group missionCliente">
                    <label class="col-md-4 control-label" for="respClient">Responsable client</label>
                    <div class="col-md-4">
                        <form:select name="listContacts" class="form-control" path="responsableClient.id"
                                     id="listContactsClient">
                            <form:option value="-1">-</form:option>
                            <c:forEach items="${listContactsClient}" var="contact">
                                <form:option value="${contact.id}">${contact.nom} ${contact.prenom}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>

                <!-- Choix du suivi  -->
                <div class="form-group missionCliente">
                    <label class="col-md-4 control-label" for="suivi">Suivi</label>
                    <div class="col-md-4">
                        <form:select id="suivi" class="form-control" path="suivi">
                            <form:option value="Mensuel">Mensuel</form:option>
                            <form:option value="Trimestriel">Trimestriel</form:option>
                        </form:select>
                    </div>
                </div>

                <!-- Choix du responsable de mission  -->
                <div class="form-group missionCliente" id="choixManager">
                    <label class="col-md-4 control-label">Responsable Mission</label>
                    <div class="col-md-4">
                        <form:select name="manager" class="form-control" path="manager.id"
                                     id="listeManagers">

                            <c:forEach items="${listeManagers}" var="manager">
                                <form:option value="${manager.id}">${manager.nom} ${manager.prenom}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>
            </div>

            <!--Choix agence pour activité commerciale -->
            <div class="form-group" id="choixAgence">
                <label class="col-md-4 control-label">Choix de l'agence</label>
                <div class="col-md-4">
                    <form:select name="agence" class="form-control" path="agence.id" id="agence"
                                 placeholder="Sélectionner une agence" itemLabel="listeAgence">
                        <c:forEach items="${listeAgence}" var="agenceTemp"><%-- les agences d'Amiltone --%>
                            <option
                                    value="${agenceTemp.id}"
                                ${(agenceTemp.id eq idAgence) ? "selected" : "" }>
                                    ${agenceTemp.adresse} ${agenceTemp.codePostal} ${agenceTemp.ville}
                            </option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>

            <div id="divOutilsInterne">
                <div class="form-group fieldsOutilsInterne">
                    <label class="col-md-4 control-label">Outils interne</label>
                    <div class="col-md-4">

                        <form:select name="outilsInterne" class="form-control" path="outilsInterne.id" id="outilsInterne">
                            <c:forEach items="${outilsInternes}" var="outilsInterne">
                                <form:option id="outilsInterneSelected" value="${outilsInterne.id}">${outilsInterne.nom}</form:option>
                            </c:forEach>
                        </form:select>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <c:choose>
                        <c:when test="${ !empty mission.id }">
                            <input id="validationCollaborateur" name="validationCollaborateur" type="checkbox">
                        </c:when>
                        <c:otherwise>
                            <input id="validationCollaborateur" name="validationCollaborateur" type="hidden" value="on">
                            <input id="validationCollaborateur" name="validationCollaborateur" type="checkbox" checked readonly disabled>
                        </c:otherwise>
                    </c:choose>
                    <label for="validationCollaborateur"> Faire valider l'ordre de mission par le collaborateur</label>
                </div>
            </div>

            <!-- Button (Double) -->
            <c:choose>
                <c:when test="${collaborateur.statut.id==7}">
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                            <a class="btn btn-danger"
                               onclick="clearCookies()"
                               href="${administration}${editTravailleur}/sous_traitant/${ collaborateur.id }/null"
                               role="button">Retour</a>
                            <input type="submit" id="buttonSubmiMission" onclick="clearCookies()"
                                   value="Valider Mission" class="btn btn-success"/>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                            <a class="btn btn-danger"
                               onclick="clearCookies()"
                               href="${administration}${editTravailleur}/collaborateur/${ collaborateur.id }/null"
                               role="button">Retour</a>
                            <input type="submit" id="buttonSubmiMission" onclick="clearCookies()"
                                   value="Valider Mission" class="btn btn-success"/>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </fieldset>
    </form:form>
</div>

<!-- si nouvelle mission avec forfait -->
<c:if test="${ empty mission.id && withForfait}">
    <!-- Forfait input-->
    <div class="form-group" id=forfaitform>
        <form:form class="form-horizontal" method="POST" commandName="forfait">
            <fieldset>
                <!-- Form Name -->
                <legend class="text-center"> Création d'un forfait à ajouter</legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="listForfaits">
                        Choix du forfait</label>
                    <div class="col-md-4">
                        <form:select name="listForfaits" class="form-control" path="id" id="idListForfaits" onchange="getSelectedForfait(this)">
                            <c:forEach items="${listForfaits}" var="tmpForfait">
                                <form:option id="tmpForfaitCode" name="${tmpForfait.nom}"
                                             value="${tmpForfait.code}">${tmpForfait.nom} - ${tmpForfait.frequence.frequence}</form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </div>

                <div class="form-group" id="montantForfait" style="display: block">
                    <label class="col-md-4 control-label" for="montant"> Montant (€)
                    </label>
                    <div class="col-md-4">
                        <form:hidden id="montant" path="montant" value="${montant}"/>
                        <form:input id="idMontant" name="montant" path="montant"
                                    class="form-control input-md" data-validation="number"
                                    data-validation-allowing="float"
                                    data-validation-decimal-separator=","
                                    onkeyup="remplaceVirgule(this)"/>
                    </div>
                </div>

                <div class="form-group" id="libelleForfaitLibre" style="display: none">
                    <label class="col-md-4 control-label" for="libelle"> Libellé
                    </label>
                    <div class="col-md-4">
                        <form:input id="libelle" path="libelle" type="text"
                                    class="form-control input-md valid"/>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-8">

                        <input id="buttonSubmitForfait" value="Ajouter Forfait" class="btn btn-success"/>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
    <div id="idTableContainer">
        <h4>Liste des forfaits à ajouter à la mission:</h4>

        <table data-toggle="table"
               data-height="520"
               data-striped="true"
               data-pagination="true"
               id="idBootstrapTable"
               data-cookie="true"
               data-cookie-id-table="saveIdEditMission">
            <thead>
            <tr>
                <th data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <th data-field="frequence" data-sortable="true" data-align="center">Fréquence</th>
                <th data-field="montant" data-sortable="true" data-align="center">Montant</th>
                <th data-align="center">Suppression</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${listForfaitsLIBJsonToAdd}" var="linkForfaitLib">
                <tr>
                    <td>LIBRE - ${linkForfaitLib.libelle}</td>
                    <td>${linkForfaitLib.forfait.frequence.frequence}</td>
                    <td>-</td>
                    <td><a
                            onclick="removeForfait('${linkForfaitLib.forfait.code}','${linkForfaitLib.montant}')">
                        <span class="glyphicon glyphicon-remove 1"></span>
                    </a></td>
                </tr>
            </c:forEach>
            <c:forEach items="${listForfaitsNonLIBJsonToAdd}" var="forfait">
                <tr>
                    <td>${forfait.forfait.nom}</td>
                    <td>${forfait.forfait.frequence.frequence}</td>
                    <td> ${forfait.montant}</td>
                    <td><a
                            onclick="removeForfait('${forfait.forfait.code}','${forfait.montant}')">
                        <span class="glyphicon glyphicon-remove" 2></span>
                        </a></td>
                </tr>
            </c:forEach>
            <c:forEach items="${listForfaitsJsonToAdd}" var="linkForfaitNotLib">
                <tr>
                    <c:choose>
                        <c:when test="${linkForfaitNotLib.forfait.code=='LIB'}">
                            <td>${linkForfaitNotLib.forfait.nom} - ${forfait.libelle}</td>
                        </c:when>
                        <c:otherwise>
                            <td>${linkForfaitNotLib.forfait.nom}</td>
                        </c:otherwise>
                    </c:choose>
                    <td>${linkForfaitNotLib.forfait.frequence.frequence}</td>
                    <td>${forfait.montant}</td>
                    <td><a
                            onclick="removeForfait('${linkForfaitNotLib.forfait.code}','${forfait.montant}') ">
                        <span class="glyphicon glyphicon-remove" 3></span>
                    </a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</c:if>

<!-- si edit mission ou nouvelle mission avec frais -->
<c:if test="${!empty mission.id  && collaborateur.statut.id != 7}">
    <fieldset>
        <div id="idTableContainer">
            <h4>Liste des forfaits :</h4>
            <c:if test="${raValide=='disabled'}">
                <div style="color: red"> Au moins un RA a déjà été validé avec cette mission, impossible de modifier les forfaits. Veuillez dévalider ce(s) RA(s)
                    ou créer un nouvel ODM si un forfait doit être ajouté ou supprimé seulement pour les prochains mois.</div>
            </c:if>
            <div id="toolbar" >
                <a
                        href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}/${mission.id}${editForfait}"
                        class="btn btn-success" ${raValide}> Nouveau forfait </a>
            </div>
            <table data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbar"
                   data-pagination="true"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditMission2">
                <thead>
                <tr>
                    <th data-field="nom" data-sortable="true" data-align="center">Nom</th>
                    <th data-field="frequence" data-sortable="true" data-align="center">Fréquence</th>
                    <th data-field="montant" data-sortable="true" data-align="center">Montant</th>
                    <th data-align="center">Suppression</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listForfaitsLib}" var="linkForfaitLib">
                    <tr>
                        <td>LIBRE - ${linkForfaitLib.libelle}</td>
                        <td>${linkForfaitLib.forfait.frequence.frequence}</td>
                        <td>-</td>
                        <td>
                            <c:if test="${raValide!='disabled'}">
                            <a href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}/${mission.id}${deleteForfait}/${linkForfaitLib.id}">
                                </c:if>
                                <span class="glyphicon glyphicon-remove "></span>
                            </a></td>
                    </tr>
                </c:forEach>
                <c:forEach items="${listForfaitsNotLib}" var="linkForfaitNotLib">
                    <tr>
                        <td>${linkForfaitNotLib.forfait.nom}</td>
                        <td>${linkForfaitNotLib.forfait.frequence.frequence}</td>
                        <td> ${linkForfaitNotLib.montant}</td>
                        <td>
                            <c:if test="${raValide!='disabled'}">
                            <a href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}/${mission.id}${deleteForfait}/${linkForfaitNotLib.id}">
                                </c:if>
                                <span class="glyphicon glyphicon-remove "></span>
                            </a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </fieldset>
</c:if>
