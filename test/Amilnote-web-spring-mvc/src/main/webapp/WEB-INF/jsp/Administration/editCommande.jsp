<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<script type="text/javascript" src='resources/js/Administration/editCommande.js'></script>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${administration}${gestionCommandes}/CO">Commandes</a></li>
    <li class="active">Édition de la commande</li>
</ol>


<div id="idProfilContainer">
    <form:form class="form-horizontal" method="POST"
               action="${administration}${saveCommande}"
               commandName="commande" enctype="multipart/form-data">
        <%--onsubmit="testForm(this)"--%>
        <fieldset>
            <legend class="text-center">
                <c:choose>
                    <c:when test="${duplicate == true}">
                        Duplication de la commande <i>${numero}</i>
                        <input hidden id="duplicatedCommandeId" name="duplicatedCommandeId" value="${idCommande}"/>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${commande.id != null}">
                                Edition de la commande <i>${commande.numero}</i>
                                <form:hidden path="id" value="${ commande.id }"/>
                            </c:when>
                            <c:otherwise>
                                Création d'une commande
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>

                </c:choose>
            </legend>

                <%-- [JNA][AMNOTE 53] Liste Collaborateurs --%>
            <div class="form-group">
                <label class="col-md-4 control-label" for="listeCollaborateurs">Consultant<span
                    style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:select name="mission.collaborateur" class="form-control" path="mission.collaborateur.id"
                                 id="listeCollaborateurs" placeholder="Sélectionner un collaborateur"
                                 itemLabel="listeCollaborateurs" onchange="loadMission(this)" required="true">
                        <c:choose>
                            <c:when test="${collab.id != null}"> <%-- Si le collab n'est pas null --%>

                                <c:forEach items="${listeCollaborateurs}"
                                           var="collaborateur"><%-- on regarde tous les collab --%>
                                    <c:choose>
                                        <c:when
                                            test="${collaborateur.id == collab.id}"> <%-- Si le collab correspond à celui selectionné --%>
                                            <form:option selected="selected"
                                                         value="${collab.id}">${collab.nom} ${collab.prenom}</form:option>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option
                                                value="${collaborateur.id}">${collaborateur.nom} ${collaborateur.prenom}</form:option>
                                        </c:otherwise>
                                    </c:choose>

                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <form:option value="0">Sélectionner un collaborateur</form:option>
                                <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                                    <form:option
                                        value="${collaborateur.id}">${collaborateur.nom} ${collaborateur.prenom}</form:option>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </form:select>

                </div>
            </div>

                <%-- [JNA][AMNOTE 53] Liste des missions du colllab choisis --%>
            <div class="form-group">
                <label class="col-md-4 control-label" for="listeMissions">Mission<span
                    style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:select name="mission" class="form-control" path="mission.id" id="listeMissions"
                                 placeholder="Sélectionner une mission" onchange="changeClient(this)" required="true">
                        <c:choose>

                            <c:when test="${empty listeMissionsCollab}"> <%-- Si la liste des missions Collab est vide --%>
                                <form:option
                                    value="0">Sélectionner une mission</form:option> <%-- On doit selectionner une mission --%>
                                <c:forEach items="${listeMissions}" var="mission">
                                    <form:option
                                        value="${mission.id}">${mission.mission}</form:option> <%-- parmis toutes les missions --%>
                                </c:forEach>
                            </c:when>

                            <c:otherwise> <%-- Sinon on a que les missions du collab --%>
                                <c:forEach items="${listeMissionsCollab}" var="mission">
                                    <c:choose>
                                        <c:when test="${mission.id == selectedMission.id}">
                                            <form:option
                                                value="${mission.id}" selected="true">${mission.mission}</form:option> <%-- parmis toutes les missions --%>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option
                                                value="${mission.id}">${mission.mission}</form:option> <%-- parmis toutes les missions --%>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:otherwise>

                        </c:choose>

                    </form:select>

                </div>
            </div>

            <div id="mission selected" value="0"></div>

                <%-- [SHA][AMNOTE-206] Liste des clients pour la facture --%>
            <div class="form-group">
                <label class="col-md-4 control-label" for="choixClient">Client<span
                    style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:select name="choixClient" class="form-control" path="client.id" id="choixClient"
                                 placeholder="Sélectionner un client" required="true" onchange="changeRespFacturation(this.value)">
                        <form:option class="clientSelect" id="client_0" value="0">Sélectionner un client</form:option>
                        <c:forEach items="${listeClients}" var="clientTmp">
                            <c:choose>
                                <c:when test="${clientTmp.id == selectedClient.id}">
                                    <form:option class="clientSelect" id="client_${clientTmp.id}" selected="true"
                                                 value="${clientTmp.id}">${clientTmp.nom_societe}</form:option>
                                </c:when>
                                <c:otherwise>
                                    <form:option class="clientSelect" id="client_${clientTmp.id}"
                                                 value="${clientTmp.id}">${clientTmp.nom_societe}</form:option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
                <%-- [SBE][AMNT-526] Liste des responsables des clients choisis --%>
            <c:choose>
                <c:when test="${empty listeResponsablesFact}">
                    <div class="form-group" id="choixRespFacturation" hidden="true">
                        <label class="col-md-4 control-label">Responsable facturation<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form name="respFacturation" class="form-control" id="FormresponsableFacturation"
                                  placeholder="Sélectionner un responsable facturation" required="true">
                                <select id="responsableFacturation" class="form-control" name="choixResp" onchange="setValueIdResp()">
                                </select>
                            </form>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group" id="choixRespFacturation">
                        <label class="col-md-4 control-label">Responsable facturation<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form name="respFacturation" class="form-control" id="FormresponsableFacturation"
                                  placeholder="Sélectionner un responsable facturation" required="true">
                                <select id="responsableFacturation" class="form-control" name="choixResp" onchange="setValueIdResp()">
                                    <c:forEach items="${listeResponsablesFact}" var="resp">
                                        <c:choose>
                                            <c:when test="${resp.id == selectedidRespo}">
                                            <option id="respFact_${resp.id}"
                                                value="${resp.id}" selected="true">${resp.nom} ${resp.prenom}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option id="respFact_${resp.id}"
                                                        value="${resp.id}">${resp.nom} ${resp.prenom}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </form>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>


            <div class="form-group">
                <label class="col-md-4 control-label" for="numero">Numéro de commande<span
                    style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="numero" path="numero" type="text"
                                placeholder="numero de la commande client" class="form-control input-md"
                                required="true" value="${numero}"/>
                </div>
                    <%----%>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="numero">Libellé<span
                    style="color:red">*</span></label>
                <div class="col-md-4">
                    <textarea id="commentaire" name="commentaire" type="text" placeholder="libellé de la commande client"
                              class="form-control input-md"
                              required="true">${commentaire}</textarea>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="dateDebut">Date
                    de début<span
                        style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input type="text"
                                path="dateDebut"
                                class="form-control input-md datepicker"
                                id="dateDebut"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                                name="dateDebut" value="${dateDebut}"
                                autocomplete="off"/>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="dateFin">Date
                    de fin<span
                        style="color:red">*</span></label>
                <div class="col-md-4">

                    <form:input type="text"
                                path="dateFin"
                                class="form-control input-md datepicker"
                                id="dateFin"
                                data-validation="date"
                                data-validation-format="yyyy-mm-dd"
                                name="dateFin" value="${dateFin}"
                                autocomplete="off"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="montant">Montant(€ H.T)</label>
                <div class="col-md-4">
                    <form:input id="montant" path="montant" type="number" name="montant"
                                value="${commande.montant}"
                                placeholder="Montant de la commande" class="form-control input-md" step="any"

                    />
                </div>
            </div>
            <div class="col-md-6 col-md-offset-4">
                <c:if test="${commande.id != null}">
                    <c:if test="${commande.budget != null || commande.budget != 0.0}">
                        <p>Il vous reste un budget de ${commande.budget} €.</p>
                    </c:if>
                </c:if>
            </div>

            <div class="form-group">
                <label class="col-md-2 col-md-offset-1 control-label" for="conditionPaiement">Conditions de
                    paiement<span style="color:red">*</span> :</label>
                <div class="col-md-2">
                    <form:select name="conditionPaiement" class="form-control" path="conditionPaiement"
                                 id="conditionPaiement" placeholder="Sélectionner un moyen de payement"
                                 itemLabel="conditionPaiement" required="true">
                        <c:forEach items="${conditionPaiementCommande}" var="condition">
                            <form:option value="${condition}">${condition}</form:option>
                        </c:forEach>
                    </form:select>
                </div>
                <div class="col-md-3">
                    <form:select name="moyensPaiement" class="form-control" path="moyensPaiement.idPaiement"
                                 id="moyensPaiement" placeholder="Sélectionner un moyen de payement"
                                 itemLabel="moyensPaiement" required="true">
                        <c:forEach items="${listePaiements}" var="paiement"><%-- les contacts du client --%>
                            <c:choose>
                                <%--[JNA][AMNOTE 142] Lorsqu'on rencontre le contact on le sélectionne --%>
                                <c:when test="${commande.moyensPaiement.libellePaiement == paiement.libellePaiement}">
                                    <form:option selected="selected"
                                                 value="${commande.moyensPaiement.idPaiement}">${commande.moyensPaiement.libellePaiement}</form:option>
                                </c:when>
                                <c:otherwise>
                                    <form:option
                                        value="${paiement.idPaiement}">${paiement.libellePaiement}</form:option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </div>
            </div>

            <c:if test="${commande.justificatif != null}">
                <div class="form-group">
                    <label class="col-md-4 control-label">Nom du justificatif</label>
                    <div class="col-md-4">
                        <div class="col-md-10">
                            <a href="${administration}${download}/${commande.id}" target="_blank">
                                <span class="glyphicon glyphicon-eye-open" title="Visualiser" style="font-size:1.3em">&nbsp;${commande.justificatif}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="form-group">
                <label class="col-md-4 control-label" for="exampleInputFile">Fichier(s) justificatif(s) (format accepté
                    : pdf)
                    <c:if test="${commande.justificatif == null}">
                        <span style="color:red">*</span>
                    </c:if>
                </label>
                <div class="col-md-4">
                    <input type="file" id="exampleInputFile" name="fichier" accept=".pdf, application/pdf">
                </div>
            </div>

            <form:hidden id="justifCommande" path="justificatif" name="justifCommande"
                         value="${ commande.justificatif }"/>

            <!-- Button (Double) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <a class="btn btn-danger"
                       href="${administration}${gestionCommandes}/CO"
                       role="button">Retour</a>
                    <input type="submit" id="button1id"
                           value="Valider" class="btn btn-success"/>
                </div>
            </div>

            <!-- value id responsable -->
            <c:choose>
                <c:when test="${selectedidRespo==0}">
                    <input id="valueIdResp" name="TheValueIdResp" hidden="true" value=""/>
                </c:when>
                <c:otherwise>
                    <input id="valueIdResp" name="TheValueIdResp" hidden="true" value="${selectedidRespo}"/>
                </c:otherwise>
            </c:choose>
        </fieldset>
    </form:form>
</div>
