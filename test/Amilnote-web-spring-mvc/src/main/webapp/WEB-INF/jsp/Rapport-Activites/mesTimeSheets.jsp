<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript">
    //Récupéré depuis le controller
    var pPeriodHistoMaxTimeSheetAsMonth = ${periodHistoMaxTimeSheetAsMonth};
    var pWithSaturdayFromController = ${withSaturday};
    var moisChoisisController = ${moisChoisis};
</script>
<script type="text/javascript" src='resources/js/Timesheet/modalEditMesTimesheets.js'></script>
<script type="text/javascript" src='resources/js/Timesheet/modalValidAbsences.js'></script>
<script type="text/javascript" src='resources/js/Timesheet/mesTimesheets.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden="hidden">
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<section id="messageUtilisateurErreur" class="alert alert-danger" role="alert" hidden="hidden">
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li class="active">Mon rapport d'activités</li>
</ol>

<article class="block-left panel panel-default">

    <div class="panel panel-success" id="list-missionsAndAbsences">
        <!-- Default panel contents -->
        <div class="panel-heading bluck">
            <h2 class="panelTitle-groupItem">Vos missions :</h2>
        </div>
        <div class="panel-body">
            <ul id="listGroupMission" class="list-group">
                <li class="list-group-item"></li>
            </ul>
        </div>
        <sec:authorize access="hasAnyAuthority('DRH', 'AD', 'DIR', 'CP', 'MA', 'RT', 'CO')">
            <div class="panel-heading">
                <h2 class="panelTitle-groupItem">Statuts des absences :</h2>
            </div>
            <div class="panel-body">
                <ul id="listGroupTypeAbsence" class="list-group">
                    <li class="list-group-item"></li>
                </ul>
            </div>

            <div class="panel-heading">
                <h2 class="panelTitle-groupItem">Paramétrage :</h2>
            </div>
        </sec:authorize>
        <div class="panel-body parametrage">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-10">Afficher les samedis :</div>
                        <div class="col-sm-2 text text-right"><input id="idWithSaturday" type="checkbox"></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-10">Permettre l'édition des jours fériés :</div>
                        <div class="col-sm-2 text text-right"><input id="idCheckBoxEditHoliday" type="checkbox"></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</article>

<!-- Calendar and actions buttons -->
<article class="block-right panel panel-default">
    <a type="button" data-backdrop="static" class="btn btn-lg btn-default btn-action purpleHaze" id="btn-checkRA"
       style="margin-left:5%;">
        <span class="glyphicon glyphicon-eye-open"></span> Visualiser rapport d'activités
    </a>
    <sec:authorize access="hasAnyAuthority('DRH', 'AD', 'DIR', 'CP', 'MA', 'RT', 'CO', 'STT')">
        <a type="button" data-backdrop="static" class="btn btn-lg btn-default btn-action skywalkerBlue" id="btn-RASansFrais">
            <span class="glyphicon glyphicon-share"></span> Soumettre RA sans frais
        </a>
    </sec:authorize>
    <a type="button" data-backdrop="static" class="btn btn-lg btn-default btn-action vaderBlue" id="btn-sendRA">
        <span class="glyphicon glyphicon-check"></span> Validation finale du RA
    </a>
    <sec:authorize access="hasAnyAuthority('DRH', 'AD', 'DIR', 'CP', 'MA', 'RT', 'CO')">
        <a type="button" class="btn btn-lg btn-default btn-action bluck" id="btnValidAbsencesRequest"
           style="float:right;margin-right:15%;">
            <span class="glyphicon glyphicon-ok"></span> Demander validation des absences
        </a>
    </sec:authorize>

    <!-- CALENDAR -->
    <section id="calendar"></section>
</article>

<!-- Modal of activity report validation -->
<div class="modal fade" id="warning-sending-RA" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Soumission du rapport d'activités</h4>
            </div>
            <div class="modal-body" id="warning-sending-RA-body"></div>
            <div class="modal-footer">
                <h4> Voulez-vous continuer ?</h4>
                <button type="button" id="sending-RA" class="btn btn-success dismiss" data-dismiss="modal"
                        onMouseOver="testPresenceVide()">Valider
                </button>
                <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal to create or update missions / absences -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="page-header" id="myModalLabelTitle">
                    <h3>
                        Propriété de la sélection
                        <small id="modalEditSubTitle">Pour la période :</small>
                    </h3>
                </div>
            </div>
            <div class="modal-body">
                <div class="panel panel-default" id="modalEditBody" class="wrapper">
                    <div class="modalWithScrolling"></div>
                </div>

                <!-- Boutons des Tabs -->
                <ul id="modalEditTabHeader" role="presentation" class="nav nav-tabs nav-justified">

                    <li role="presentation" class="active">
                        <a href="#modalEditTabEditMission" role="tab" data-toggle="tab" aria-expanded="true">Mission
                            : </a>
                    </li>

                    <li role="presentation" class="">
                        <a href="#modalEditTabEditAbsence" role="tab" data-toggle="tab" aria-expanded="true">Absence
                            : </a>
                    </li>

                </ul>

                <!-- Contenu des Tabs -->
                <div id="modalEditTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="modalEditTabEditMission">

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Choix de la mission pour les demi-journées sélectionnées :</span>
                            <div id="modalEditMissionSelect"><select class="selectpicker"></select></div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="modalEditTabEditAbsence">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon2">Choix de l'absence pour les demi-journées sélectionnées :</span>
                            <div id="modalEditAbsenceSelect">
                                <select class="selectpicker"></select>
                            </div>
                        </div>
                    </div>

                </div>

                <ul id="idMessageUtilisateur" class="alert" role="alert"></ul>

                <!-- Actions on the selected missions or absences -->
                <div class="modal-footer modalFooterCustom">
                    <button disabled type="button"
                            class="btn btn-default"
                            aria-label="Left Align"
                            id="btnDelSelectionModal">
                        <span class="glyphicon glyphicon-trash glyphicon-position" aria-hidden="true"></span>
                        Supprimer la sélection
                    </button>

                    <button disabled type="button"
                            class="btn btn-default"
                            aria-label="Left Align"
                            id="btnUpdateSelectionModal">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        <span class="btnUpdateSelectionModal">Enregistrer la sélection</span>
                    </button>

                    <button type="button"
                            class="btn btn-default"
                            data-dismiss="modal"
                            id="btnCancelSelectModal">
                        <span class="glyphicon glyphicon-remove glyphicon-position" aria-hidden="true"></span>
                        Annuler
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal to request absences validation -->
<div class="modal fade" id="modalValidAbsences" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <H3>Validation des absences</H3>
            </div>
            <div class="modal-body modalWithScrolling"></div>
            <div class="modal-footer">
                <div class="form-group" id="formCommentaire">
                    <label class="control-label" for="comment">Commentaire : </label>
					<textarea name="commentaire" data-validation="length" data-validation-length="max255"
                              class="form-control" rows="5" id="comment"></textarea>
                </div>
                <span class="label label-danger" id="idMessageUtilisateur2"></span>

                <button type="button" class="btn btn-default" aria-label="Left Align" id="btnValidAbsencesModal">
					<span class="glyphicon glyphicon-ok" aria-hidden="true">
					</span>
                    Soumettre la sélection
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal" >
					<span class="glyphicon glyphicon-remove" aria-hidden="true">
					</span>
                    Annuler
                </button>

            </div>
        </div>
    </div>
</div>

<!-- Boites de dialogue -->
<div id="dialogEditionAbsence">
    <p id="textDialogAbsence"></p>
</div>
