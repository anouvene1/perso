<%--
  ~ ©Amiltone 2017
  --%>

<jsp:useBean id="constants" class="com.amilnote.project.metier.domain.utils.Constantes" scope="session"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page contentType="text/html; charset=UTF-8" %>


<c:if test="${! empty error }">
    <div class="errorBlock">${error}</div>
</c:if>
<c:if test="${testChangementMdp}">
    <div id="idTooltipWelcome" class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <Strong> Bonjour ${prenomNomCollab} !</Strong> Vous n'avez toujours pas modifié votre mot de passe. Vous pouvez
        y accéder en cliquant sur l'onglet 'Mon profil'.
    </div>
</c:if>
<article id="recap" class="panel panel-default">
    <h1><%=constants.recapMois%>
    </h1>

    <ul class="list-group">
        <li class="list-group-item">Dernier rapport d'activités soumis : <span class="badge">${dateDernierRA}</span>
        </li>
        <li class="list-group-item">Dernière absence soumise : <span class="badge">${dateDerniereAbs}</span></li>
        <li class="list-group-item">Nombre d'absences au statut brouillon : <span class="badge">${nbAbsBrouillon}</span>
        </li>
        <li class="list-group-item">Nombre d'absences en attente de validation manager : <span
            class="badge">${nbAbsAttente}</span></li>
        <li class="list-group-item">Nombre d'ordres de mission à valider : <span
            class="badge">${nbMissionAValider}</span></li>
        <!-- <li class="list-group-item">Nombre de rapports d'activités en attente de validation manager : <span class="badge">${nbRapportAttente}</span></li> -->
        <!-- <li class="list-group-item">LDAP : ${testLDAP}</li> -->
    </ul>
    <sec:authorize access="hasAnyAuthority('DRH', 'AD')">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title welcomeTitleAdmin">Administration</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">Demandes d'absences en attente de votre validation : <span
                        class="badge">${nbAbsAValider}</span></li>
                    <li class="list-group-item">Ordres de mission en attente de votre validation : <span
                        class="badge">${nbDOMAValider}</span></li>
                    <!-- <li class="list-group-item">Nombre de rapports d'activités en attente de votre validation : <span class="badge">${nbRapportAValider}</span></li> -->
                </ul>
            </div>
        </div>
    </sec:authorize>

</article>
