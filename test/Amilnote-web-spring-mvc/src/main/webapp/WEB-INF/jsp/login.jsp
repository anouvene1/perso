<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Connexion to Amilnote</title>
    <link rel="icon" type="image/x-icon"
          href="resources/images/favicon.ico"/>
    <link rel="stylesheet" type="text/CSS" href="resources/css/menu-navigation.css"/>

    <link rel="stylesheet" type="text/CSS"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/CSS" href="resources/css/custom.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript" src='resources/js/Login/login.js'></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/x64-core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/components/sha512.js"></script>
</head>

<body onload="resetCookieLogin()">
<jsp:include page="template/header-login.jsp"/>
<c:if test="${not empty error}">
    <div class="errorblock"> ${error} </div>
</c:if>

<div id="loginForm" class="panel panel-default">
    <form class="form-horizontal" action="<c:url value='j_spring_security_check' />" method='POST'>

        <!-- Logo -->
        <div class="form-group">
            <div class="col-sm-12">
                <a id="amilnote" title='retour accueil application amilnote' href="${login}">
                    <img alt="Accueil application amilnote" title="Accueil application amilnote"
                         src="resources/images/amilnote-logo.png"/>
                </a>
            </div>
        </div>

        <!-- Email -->
        <div class="form-group">
            <Label for="email" class="col-sm-3 col-md-3 control-label">E-mail <i class="obligated">*</i></Label>
            <div class="col-sm-8 col-md-7">
                <input id="email" class="form-control" type="email" placeholder="Entrez votre adresse mail" required
                       name='j_username' pattern="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|" autofocus/>

            </div>
        </div>

        <!-- Password -->
        <div class="form-group">
            <label for="passwordToHash" class="col-sm-3 col-md-3 control-label text-nowrap">Mot de passe <i class="obligated">*</i></label>
            <div class="col-sm-8 col-md-7">
                <input id="passwordToHash" class="form-control" type='password' placeholder="Entrez votre mot de passe"
                       name='passwordToHash'/>
                <input name='j_password' id="password" hidden/>
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-md-offset-3 col-sm-8 col-md-7">
                <input id="submit" name="submit" type="submit" value="Connexion" class="btn btn-default purpleHaze"
                       onClick="hashAndSubmit()"/>
            </div>
        </div>

        <!-- Forgotten password -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-md-offset-3 col-sm-8 col-md-7 text-right">
                <a title="mot de passe oublié ?"
                   id="forgottenMDP" href="mail"
                   onCLick="createCookieLogin()">Mot de passe oublié ?</a>
            </div>
        </div>
    </form>
</div>

<jsp:include page="template/footer.jsp"/>
</body>

</html>
