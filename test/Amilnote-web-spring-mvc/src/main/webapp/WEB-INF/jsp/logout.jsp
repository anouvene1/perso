<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Connexion to Amilnote</title>
    <link rel="icon" type="image/x-icon"
          href="resources/images/favicon.ico"/>
    <link rel="stylesheet" type="text/CSS" href="resources/css/menu-navigation.css"/>

    <link rel="stylesheet" type="text/CSS"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/CSS" href="resources/css/custom.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<body>

<jsp:include page="template/header-login.jsp"/>

<c:if test="${not empty error}">
    <div class="errorblock">
            ${error}<br>
            ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
    </div>
</c:if>

<form id="loginForm" class="panel panel-default" action="" method='POST'>
    <a id="amilnote" title='retour accueil application amilnote' href="${login}"><img alt="Accueil application amilnote"
                                                                                      title="accueil application amilnote"
                                                                                      src="resources/images/amilnote-logo.png"/></a>

</form>
<jsp:include page="template/footer.jsp"/>
</body>

</html>
