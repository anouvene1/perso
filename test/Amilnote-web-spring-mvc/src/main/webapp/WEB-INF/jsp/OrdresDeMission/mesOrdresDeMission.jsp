<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
  ~ ©Amiltone 2017
  --%>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<script type="text/javascript" src='resources/js/modalCommentaire.js'></script>
<script>
    $(function () {
        $('#idBootstrapTable').bootstrapTable(); // init via javascript

        //resize columns with window
        $(window).resize(function () {
            $('#idBootstrapTable').bootstrapTable('resetView');
        });
    });
</script>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Validation des ordres de mission</li>
</ol>

<div id="idTableContainer">
    <h3>Validation des Ordres de mission</h3>

    <div id="mini-scrolltab">
        <table
            data-toggle="table"
            data-height="520"
            data-search="true"
            data-striped="true"
            data-pagination="true"
            data-page-size=25
            data-page-list="[10, 25, 50, 100, ${listMission.size()}]"
            data-pagination-v-align="top"
            data-pagination-h-align="right"
            data-pagination-detail-h-align="left"
            data-id-field="idBootstrapTable"
            data-maintain-selected="true"
            data-cookie="true"
            data-cookie-id-table="saveIdMesODM">
            <!-- En tête du tableau et configuration de bootstrapTable -->

            <!-- <th data-field="state" data-checkbox="true"></th> -->
            <thead style="text-align:center">
            <tr>
                <th data-field="mission" data-sortable="true" data-align="center" data-valign="middle">Mission</th>
                <th data-field="visualisation" data-align="center" data-valign="middle">Visualisation</th>
                <th data-field="valider" data-align="center" data-valign="middle">Valider</th>
                <th data-field="refuser" data-align="center" data-valign="middle">Refuser</th>
            </tr>
            </thead>

            <!-- Corps du tableau -->
            <tbody>
            <!-- Boucle sur la liste des deplacements pour afficher chaque ligne -->

            <c:forEach items="${listMission}" var="mission" varStatus="loop">
                <tr>
                    <td>${mission.mission}</td>

                    <!--bouton oeil-->
                    <td>
                        <c:choose>
                            <c:when test="${!empty mission.pdfODM}">
                                <a href="${mesOrdresDeMission}${validationODM}/${mission.id}${download}"
                                   target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="glyphicon glyphicon-eye-close"></span>
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <!-- Bouton pour accepter l'odm -->
                    <td>
                        <c:choose>
                            <c:when test="${mission.etat.code=='SO'}">
                                <a href="${mesOrdresDeMission}${validationODM}/${mission.id}${VA}">
                                    <span class="glyphicon glyphicon-ok"></span>
                                </a>
                            </c:when>
                            <c:otherwise>
                                -
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <!-- Bouton pour refuser l'odm -->
                    <td><c:choose>
                        <c:when test="${mission.etat.code=='SO'}">
                            <a class="openModalCommentaire"
                               data-title="Refus de l'ordre de mission"
                               data-href="${mesOrdresDeMission}${validationODM}/${mission.id}${RE}">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
