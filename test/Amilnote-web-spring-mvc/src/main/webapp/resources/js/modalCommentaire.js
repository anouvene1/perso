 /*
 * ©Amiltone 2017
 */

$(function () {

    var $modalCommentaire = $("#modalCommentaire");
    var $formModalCommentaire = $modalCommentaire.find("form");
    var $modalTitleH3 = $modalCommentaire.find(".modal-header h3");

    //le bouton qui ouvre la fenetre modale de commentaire doit se trouver dans un tableau BootstrapTable
    var $bootstrapTable = $("#idBootstrapTable");

    //Fonction d'ouverture
    var functionOpenModal = function (elem) {
        var hrefElem = $(elem).data("href");
        var title = $(elem).data("title");
        var inputHidden = $formModalCommentaire.find("input");
        $modalTitleH3.html(title);
        $formModalCommentaire.attr("action", hrefElem);
        inputHidden.attr('value', hrefElem.split('=')[1]);

        $modalCommentaire.modal({backdrop: 'static'});
    }

    //On attache la fonction à tous les boutons de la page
    $(".openModalCommentaire").click(function (e) {
        functionOpenModal(this);
    });

    //Si l'utilisateur change de page dans le tableau: 
    //-  des nouveaux boutons apparaissent, il faut attacher l'event à ces nouveaux boutons
    if (null != $bootstrapTable) {
        $bootstrapTable.on("page-change.bs.table", function (e) {
            $(".openModalCommentaire").click(function (e) {
                functionOpenModal(this);
            });
        });

        $bootstrapTable.on("sort.bs.table", function (e) {
            $(".openModalCommentaire").click(function (e) {
                functionOpenModal(this);
            });
        });
    }

    //Si la fentere modale est fermée, on supprime les actions et le titre
    $modalCommentaire.on('hide.bs.modal', function (e) {

        $formModalCommentaire.attr("action", "#");
        $modalTitleH3.html("");
    });


});
