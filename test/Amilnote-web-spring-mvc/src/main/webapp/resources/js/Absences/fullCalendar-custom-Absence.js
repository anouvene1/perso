/*
 * ©Amiltone 2017
 */

$(function () {

    //Chargement de la classe Colors avec en paramètre une  palette de couleurs 'absence'
    var lColors = new Colors(getColorsList('absence'));

    var lPanelAbsences = new PanelAbsences($("#list-group-typeAbsences"));

    lPanelAbsences.colors = lColors;

    var lAllTypesAbsences = getListTypeAbsence();

    //On assoccie une couleur à chaque type d'absence
    lColors.addList(lAllTypesAbsences, "typeAbsence");

    //Construction du panneau d'absence à gauche
    lPanelAbsences.buildList(lAllTypesAbsences, "typeAbsence");

    var lModalEditAbsence = new ModalEditAbsence($('#modalEditView'));

    //Construction du calendrier
    $("#calendar").fullCalendar({
        that: this,
        header: {
            left: 'title',
            right: 'today prev,next'
        },
        hiddenDays: [0, 6],      //on cache le dimanche
        height: 500,
        aspectRatio: 2,
        defaultView: 'month',
        editable: false,			//Les events ne peuvent pas être déplacé
        selectable: true,			//On peut séléctionner un jour ou une place de jour
        fixedWeekCount: false,
        selectHelper: true,
        defaultTimedEventDuration: '04:00:00',	//Si un event issue de eventSource n'a pas de propriété end, l'assigne automatiquement.
        eventSources: [
            // A chaque fois que les events sont rechargés ( fullCalendar('refetchEvents'), changements de mois, ... )
            // Appelle la fonction getListEvents(start, end) avec le jour min et max de la vue en cours.
            function (start, end, timezone, callback) {
                var events = getListEvents(start, end);
                callback(events);
            },
            getListHolidays()	//Liste des jours fériés
        ],
        select: function (start, end, jsEvent) { //Quand sélection d'une journée ou d'une plage.
            if (end == null)
                end = start;
            //On récupère la liste 
            var listEventsSelected = $('#calendar').fullCalendar('clientEvents', function (e) {
                return (e.start >= start && e.end <= end );
            });
            var lNbAbsence = findByItemNameNotNullInListEvents(listEventsSelected, "absence").length;
            var lNbFerie = findByItemNameNotNullInListEvents(listEventsSelected, "ferie").length;
            var $modalWarning = $("#modalWarning");
            if (lNbAbsence == 0 && lNbFerie == 0) {
                lModalEditAbsence.initWithoutAbsence(null, lAllTypesAbsences, start, end);
                lModalEditAbsence.show();
            } else {
                $modalWarning.find(".modal-body").empty();
                $modalWarning.find(".modal-body").append("Pour modifier la ou les absences présentes sur la séléction, cliquez directement dessus.");
                $modalWarning.modal({
                    backdrop: 'static',
                    keyboard: false
                });
                //alert("Pour modifier la ou les absences présentes sur la séléction, cliquez directement dessus.");
            }

        },
        eventClick: function (event) { // Click sur un event
            var $modalWarning = $("#modalWarning");
            if (event.ferie != null) {
                $modalWarning.find(".modal-body").empty();
                $modalWarning.find(".modal-body").append("Vous avez cliqué sur un jour férié.");
                $modalWarning.modal({
                    backdrop: 'static',
                    keyboard: false
                });
                //alert("Vous avez cliqué sur un jour férié.");
                return false;
            } else {
                if (event.absence != null) {
                    var listEventsWithSameAbsence = $('#calendar').fullCalendar('clientEvents', function (e) {
                        return (e.absence != null && e.absence.id == event.absence.id && e.ferie == null);
                    });
                    lModalEditAbsence.initWithAbsence(event.absence, listEventsWithSameAbsence, lAllTypesAbsences,
                        event.absence.dateDebut, event.absence.dateFin);
                    lModalEditAbsence.show();
                } else {
                    lModalEditAbsence.initWithoutAbsence($.makeArray(event), lAllTypesAbsences, event.start, event.end);
                    lModalEditAbsence.show();
                }
            }
            return false;
        },
        eventDataTransform: function (data) {
            if (data.absence != null) {
                data.title = data.absence.typeAbsence;
            } else if (data.mission != null) {
                data.title = data.mission.mission;
            }
            return data;
        },
        eventRender: function (event, element, view) {
            var color = lColors.getValue(event.title);
            element.css({
                backgroundColor: color
            });

            var lDateString = moment(event.start).format('YYYY-MM-DD');
            var lFcDay = $(document).find('.fc-day[data-date="' + lDateString + '"]');

            if (event.ferie != null) {
                //$(element).height('60px');
                element.css('backgroundColor', getColorsList('férié'));

                lFcDay.css('background-color', getColorsList('férié'));
            }
            if (event.absence != null) {
                element.addClass("absence" + event.absence.id);
            }
        },
        eventAfterRender: function (pEvent, pElement, view) {
            addCustomClassForEventPM(pEvent, pElement, $('#calendar').fullCalendar('clientEvents'));
        },
        eventMouseover: function (event, jsEvent, view) {
            if (event.absence != null) {
               var $listEventAbsence = $(document).find('.absence' + event.absence.id);
                $listEventAbsence.addClass("hover");
            }
            return false;
        },
        eventMouseout: function (event, jsEvent, view) {
            if (event.absence != null) {
                $listEventAbsence = $(document).find('.absence' + event.absence.id);
                $listEventAbsence.removeClass("hover");
            }
        }
    });
});

function PanelAbsences(pListGroupTypeAbsence) {
    var that = this;
    this.listGroupTypeAbsence = pListGroupTypeAbsence;
    this.colors = "";

    this.buildList = function (pList, itemName) {
        that.listGroupTypeAbsence.empty();
        $.each(pList, function (key, val) {
            var color = that.colors.getValue(val[itemName]);
            var li = $('<li>').attr({
                'class': 'list-group-item'
            }).html(val[itemName]);
            var span = $('<span>').attr({
                'class': 'badge'
            }).css({
                backgroundColor: color
            }).html(" ");

            li.append(span);
            that.listGroupTypeAbsence.append(li);
        });
    }
}

/**
 * Modal absences edit
 * @param $modal The modal
 * @constructor Constructor
 */
function ModalEditAbsence($modal) {
    var that = this;
    this.$that = $modal;

    this.idSubTitle = "#modalEditViewSubTitle";
    this.$subTitle = that.$that.find(that.idSubTitle);
    this.idBody = "#modalEditViewBody"
    this.$idBody = that.$that.find(that.idBody);


    this.$btnSendTypeAbsenceChange = that.$that.find("#sendTypeAbsenceChange");
    that.$btnSendTypeAbsenceChange.click(function () {
        that.saveNewTypeAbsence()
    });

    this.$selectTypeAbsence = that.$that.find("select"); // liste déroulante pour choisir le type d'absence
    that.$selectTypeAbsence.change(function () { // quand choix d'un type d'absence :  activer le bouton  $btnSendTypeAbsenceChange
        that.activeOrNotSubmitButton();
    });
    this.$labelSelectAbsence = that.$that.find('#modalEditAbsenceLabelSelect');
    this.$btnDeleteAbsence = that.$that.find("#sendTypeAbsenceDelete");
    this.$spanWarningNbFrais = that.$that.find('#modalEditViewSpanWarning');

    //variables de classe
    this.absence = null;
    this.listEvents = null;
    this.allTypeAbsence = null;
    this.start = null;
    this.end = null;
    this.nbFraisTotal = null;
    this.formatDateForTableEvents = 'LLL';
    this.$listCheckBox = null;
    this.listCheckModalFunction = [];
    this.initFenetre = function () {

        //Initialisation de la fenêtre
        that.$subTitle.empty();
        that.$idBody.empty();
        that.$selectTypeAbsence.empty();
        that.$btnSendTypeAbsenceChange.prop('disabled', true);
        that.$labelSelectAbsence.empty();
        that.$spanWarningNbFrais.toggle(false);

        //ajout du parser date personnalisé au plugin $.tablesorter
        addDateParser(that.formatDateForTableEvents);

    }

    this.initWithAbsence = function (pAbsence, pListEvents, pAllTypeAbsence, pStart, pEnd) {
        that.initFenetre();

        that.absence = pAbsence;
        that.listEvents = pListEvents;
        that.allTypeAbsence = pAllTypeAbsence;
        that.start = pStart;
        that.end = pEnd;
        that.nbFraisTotal = 0;

        that.$labelSelectAbsence.html("Modification de l'absence :");

        var lTxtPeriode = moment(that.absence.dateDebut).format('DD/MM/YYYY') + " - " + moment(that.absence.dateFin).format('DD/MM/YYYY');

        // On enlève le type de l'absence présente de la liste du menu déroulant.
        var listePourMenuDeroulant = $.grep(that.allTypeAbsence, function (val) {
            return (val.typeAbsence != that.absence.typeAbsence);
        });
        that.buildList(listePourMenuDeroulant, "typeAbsence"); // construction de la liste déroulante

        //Construction du tableau pour le titre de la modal
        // - Initialisation des données
        var dataAbsence = [
            {"1": "Absence numéro : ", "11": that.absence.id},
            {"2": "Période : ", "21": lTxtPeriode},
            {"3": "TypeAbsence : ", "31": that.absence.typeAbsence}
        ];
        // - Insertion des données dans la div that.idSubTitle
        $.jsontotable(dataAbsence, {id: that.idSubTitle, header: false, className: 'table'});

        //Activitation du bouton suppression
        that.$btnDeleteAbsence.prop('disabled', false);

        //Construction du tableau avec la liste des demi-journées d'absences
        var dataEvents = [];
        // - Ajout de l'entête 
        dataEvents.push({"1": "Date de début", "2": "Date de fin"});
        // -  Ajout du corps du tableau : une ligne par demi-journée d'absence et comptabilisation du nombre de frais total
        $.each(that.listEvents, function (key, val) {
            dataEvents.push({
                "1": moment(val.start).format(that.formatDateForTableEvents),
                "2": moment(val.end).format(that.formatDateForTableEvents)
            });
        });
        // -  Insertion des données du tableau
        $.jsontotable(dataEvents, {id: that.idBody, header: true, className: 'table table-responsive'});

        //On tri le tableau sur la première colonne en ASC avec le parser personnalisé "sortDateParser"
        that.$idBody.find(".table").tablesorter({
            headers: {0: {sorter: 'sortDateParser'}},
            sortList: [[0, 0]]
        });

        // Une absence est présente, on active le bouton
        that.$btnDeleteAbsence.prop('disabled', false);

        //Ajout de la fonction de test à that.listCheckModalFunction
        // - Elle sera testé à l'appel de la function that.checkModal()
        that.listCheckModalFunction.push(that.checkSelectNotEmpty);
    }

    this.initWithoutAbsence = function (pListEvents, pAllTypeAbsence, pStart, pEnd) {

        that.initFenetre();
        that.absence = null;
        if (pListEvents == null)
            that.listEvents = initListEventsWithDates(pStart, pEnd);
        else
            that.listEvents = pListEvents;

        that.allTypeAbsence = pAllTypeAbsence;
        that.start = pStart;
        that.end = pEnd;
        that.nbFraisTotal = 0;

        that.initFenetre();

        that.$labelSelectAbsence.html("Nouvelle absence :");
        // On désactive le bouton de suppression ( Il n'y a pas d'absence à supprimer dans la fenêtre modal )
        that.$btnDeleteAbsence.prop('disabled', true);
        // la liste du menu déroulant doit contenir toutes les absences
        var listePourMenuDeroulant = that.allTypeAbsence;
        // construction de la liste déroulante
        this.buildList(listePourMenuDeroulant, "typeAbsence");
        // Désactive le bouton d'envoie ( il se réactivera quand changement sur la liste ( voir initialisation des elements au début de la fonction ) )
        that.$btnSendTypeAbsenceChange.prop('disabled', true);

        // Construction du tableau pour le titre de la modal
        // - Insertion des données
        var lTxtPeriode = formatDateForUser(that.start) + " - " + formatDateForUser(that.end);
        var dataAbsence = [
            {"1": "Période : ", "21": lTxtPeriode}
        ];
        // - Ajout du tableau dans la div that.idSubTitle
        $.jsontotable(dataAbsence,
            {id: that.idSubTitle, header: false, className: 'table'});

        // Construction du tableau avec la liste des demi-journée choisies pour l'ajout de l'absence
        // - Ajout de l'entête du tableau
        var dataEvents = [];
        dataEvents.push({"1": "Date de début", "2": "Date de fin", "3": "Nombre de Frais", "4": "Selectionné"});
        // - Ajout du corps du tableau : une ligne par demi-journée séléctionnés  et comptabilisation du nombre de frais total
        $.each(that.listEvents, function (key, val) {
            that.nbFraisTotal = that.nbFraisTotal + val.nbFrais;
            dataEvents.push({
                "1": moment(val.start).format(that.formatDateForTableEvents),
                "2": moment(val.end).format(that.formatDateForTableEvents),
                "3": val.nbFrais,
                "4": "<input  type='checkbox'  value='" + val.localId + "' checked='checked'>"
            });
        });

        if (that.nbFraisTotal > 0) that.$spanWarningNbFrais.toggle(true);
        // - Insertion du tableau dans la div id=that.idBody
        $.jsontotable(dataEvents, {id: that.idBody, header: true, className: 'table table-responsive'});// configuration
        // - a chaque click sur un checkBox, on met à jour la liste des events on appel countAndUpdateCheckBox()
        that.$listCheckBox = that.$idBody.find('tr input[type="checkbox"]');
        that.$listCheckBox.click(function () {
            that.activeOrNotSubmitButton();
        });
        // - tri le tableau sur la première colonne en ASC avec le parser personnalisé "sortDateParser"
        that.$idBody.find(".table").tablesorter({
            headers: {0: {sorter: 'sortDateParser'}},
            sortList: [[0, 0]]
        });
        //Ajout des fonctions de test à that.listCheckModalFunction
        // - Elles seront testés à l'appel de la function that.checkModal()
        that.listCheckModalFunction.push(that.checkCountCheckBox);
        that.listCheckModalFunction.push(that.checkSelectNotEmpty);
    }
    this.show = function () {
        that.$that.modal('show');
    }
    this.buildList = function (pList, itemName) {
        var $lSelect = that.$selectTypeAbsence
        $lSelect.empty();
        $.each(pList, function (key, val) { 		 	//Pour chaque item dans la liste, on construit une option :
            var input = $("<option>").attr({	 	// - <option value="N">Nom type absence</option>
                'value': val.id,
            });
            input.html(val[itemName]);
            $lSelect.append(input);
            $lSelect.val("");
        });

    }
    this.saveNewTypeAbsence = function () {

        var $modalWarning = $("#modalWarning");
        if (that.checkModal()) {
            if (that.absence != null) { //On veut modifier une absence existante
                $modalWarning.find(".modal-body").empty();
                $modalWarning.find(".modal-body").append("Vous devez voir avec le manager.");
                $modalWarning.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            } else {//On veut créer une absence.
                //On récupère les cases décochéés
                var $listUncheckBoxInTab = that.$idBody.find('tr input[type="checkbox"]:not(:checked)');
                // la liste des demi-journées séléctionnées par l'utilisateur
                var lListEvents = that.listEvents;
                //Pour chaque case décochée, on supprime la demi-journée qui a le même localId dans listEvents
                $.each($listUncheckBoxInTab, function (key, pCheckBoxVal) {
                    lListEvents = $.grep(lListEvents, function (pEventsVal) {
                        // Retourne à lListEvents QUE ceux qui ont un localId différent
                        return (pEventsVal.localId != pCheckBoxVal.value);
                    });
                });
                var res = saveNewAbsence(lListEvents, that.$selectTypeAbsence.val(), that.start, that.end);
                setMessageUtilisateur($('#messageUtilisateur'), res);
            }
        }

        $('#calendar').fullCalendar('refetchEvents');
        that.$that.modal("hide");
    }

    this.checkCountCheckBox = function () {
        var lNBCheckBoxIsCheck = that.$idBody.find('tr input[type="checkbox"]:checked');
        if (lNBCheckBoxIsCheck.length == 0)
            return false;
        else
            return true;
    }
    this.checkSelectNotEmpty = function () {
        var lSelectVal = that.$that.find("select").val();
        if (lSelectVal != null)
            return true;
        else
            return false;
    }
    //A chaque fois qu'on veut tester l'etat de la fenêtre modal, on passe par cette fonction
    this.checkModal = function () {
        var lCheck = true;
        //Pour chaque fonction de test dans that.listCheckModalFunction
        $.each(that.listCheckModalFunction, function (key, func) {
            //Si la fonction return faux : la fenêtre modal n'est pas conforme
            lCheck = func(); // on stock le resultat
            if (!lCheck) return false; // si égal à faux, on sort (  pas besoin de tester les fonctions suivantes )
        });
        return lCheck;
    }
    // Si la fenêtre modal est conforme ( checkModal = true/false ), activer ou non le bouton "envoyer"
    this.activeOrNotSubmitButton = function () {
        if (that.checkModal()) {
            that.$btnSendTypeAbsenceChange.prop('disabled', false);
        } else {
            that.$btnSendTypeAbsenceChange.prop('disabled', true);
        }
    }
}

function saveNewAbsence(pListEvents, pIdTypeAbsence, pStart, pEnd) {

    var lAjaxRequestRes;
    $.each(pListEvents, function (key, val) {
        val['idTypeAbsence'] = pIdTypeAbsence;
        val['absenceStart'] = pStart;
        val['absenceEnd'] = pEnd;
        delete val['localId'];
    });
    //Création et lancement de la requèete Ajax
    $.ajax({
        url: 'Absences/update',
        type: 'POST',
        dataType: 'text',
        contentType: 'application/json; charset=utf-8',
        async: false,
        data: JSON.stringify(pListEvents),
        success: function (data) {
            var lModalPDF = new ModalPdf($('#modalPDF'));
            lModalPDF.init(data);
            lAjaxRequestRes = "statut : " + data;
        }
    });

    return lAjaxRequestRes;
}
function initListEventsWithDates(pStart, pEnd) {
    var listEvents = [];
    var lStart = moment(pStart).local();
    var lEnd = moment(pEnd).local();
    var lId = 0;
    while (!lStart.isSame(lEnd, 'day')) {
        lId++;
        var tmpEventAM = {};
        tmpEventAM["start"] = moment(lStart).hours(8).format();
        tmpEventAM["end"] = moment(lStart).hours(12).format();
        tmpEventAM["localId"] = lId; //On initialise un id temporaire
        tmpEventAM["nbFrais"] = 0;
        listEvents.push(tmpEventAM);
        lId++;
        var tmpEventPM = {};
        tmpEventPM["start"] = moment(lStart).hours(13).format();
        tmpEventPM["end"] = moment(lStart).hours(17).format();
        tmpEventPM["localId"] = lId;//On initialise un id temporaire
        tmpEventPM["nbFrais"] = 0;
        listEvents.push(tmpEventPM);

        var lStart = moment(lStart).add(1, 'days');

    }
    return listEvents;

}
function ModalPdf($thatModal) {
    var that = this;
    this.$that = $thatModal;

    this.show = function () {
        that.$that.modal('show');
    }
    this.init = function (data) {
        that.$that.find('img').attr({'src': data});
        that.show();
    }
}


