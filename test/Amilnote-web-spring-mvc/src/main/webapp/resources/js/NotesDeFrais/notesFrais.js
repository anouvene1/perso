/*
 * ©Amiltone 2017
 */

//----------------------------------------------------------------------------//
//envoi de l'id du frais pour récupérer son image et l'afficher en grand
//----------------------------------------------------------------------------// 
function sendRowForPicture(elem) {
    var idToSend = elem; //$(elem).closest("tr").children("td").first().html();
    $.ajax({
        type: 'POST',
        data: JSON.stringify(idToSend),
        contentType: 'application/json; charset=utf-8',
        mimetype: 'application/json',
        processData: false,
        url: "mesNotesDeFrais/getImageJustif",
        success: function (data, textStatus, jqXHR) {
            if (data.indexOf("Erreur") >= 0) {
                $('#img-full .modal-body').html("<div ID='myCtrl' ><p> Le fichier est introuvable... </p></div>");
            }
            var resContentType = jqXHR.getResponseHeader('content-type');
            if(resContentType.startsWith('image/')) {
                $('#img-full .modal-body').css("height", "60vh");
                $('#img-full .modal-body').html("<img id='pj_img' width=100% height=100% " +
                    "src = '" + data + "' />");
            } else {
                $('#img-full .modal-body').css("height", "60vh");
                $('#img-full .modal-body').html("<OBJECT ID='myCtrl' WIDTH=100% HEIGHT=100% " +
                    "DATA='" + data + "'>Impossible d'afficher la pièce jointe. Cette option est incompatible avec votre navigateur...</OBJECT>");
            }
            window.parent.$("#img-full").modal();
        },

        error: function (response) {
            var $modalWarning = $("#modalWarning");
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append(response);
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            //alert(response);
        },
        cache: false
    });
};
