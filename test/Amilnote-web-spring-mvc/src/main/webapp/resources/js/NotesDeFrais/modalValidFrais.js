/*
 * ©Amiltone 2017
 */

function modalValidFrais($modalValidFrais) {
    var that = this;
    this.$that = $modalValidFrais;

    this.$header = that.$that.find('.modal-header');
    this.$body = that.$that.find('.modal-body');
    this.$footer = that.$that.find('.modal-footer');

    this.idTable = "idTable";
    this.$table;

    this.messageUtilisateur = that.$that.find('#idMessageUtilisateur');
    this.$idRemplacerButton = that.$that.find('#idRemplacerButton');
    this.$idAdditionnerButton = that.$that.find('#idAdditionnerButton');

    that.$idRemplacerButton.click(function () {
        that.submitRemplacerModal();
    });

    that.$idAdditionnerButton.click(function () {
        that.submitAdditionnerModal();
    });

    this.init = function () {
        that.$body.empty();
        that.$body.append($('<table>').prop('id', that.idTable));

        var selection = document.getElementById("mission").value;

        that.$body.append($('<p>valeur select : ' + selection + '</p><p>Un Forfait de Transport pour cette mission existe déjà.</p><p>Souhaitez vous <i>remplacer</i> le forfait avec ce frais ou <i>l\'additionner</i> ?</p>'));

        that.$table = that.$body.find('#' + that.idTable);

        that.$that.modal({backdrop: 'static'});

    }

    this.submitRemplacerModal = function () {
        //On met la valeur du champ fraisForfait à FRF (Frais Remplacant Forfait)
        document.forms["formulaire"].elements["fraisForfait"].value = "FRF";
        document.forms["formulaire"].submit();
    }

    this.submitAdditionnerModal = function () {
        //On met la valeur du champ fraisForfait à FRF (Frais Additionnant Forfait)
        document.forms["formulaire"].elements["fraisForfait"].value = "FAF";
        document.forms["formulaire"].submit();
    }
}
