/*
 * ©Amiltone 2017
 */

$(function () {

    $('#idBootstrapTable').bootstrapTable(); // init via javascript
    initBootstrapTable(7, "");

    // Resize columns with window
    $(window).resize(function () {
        $('#idBootstrapTable').bootstrapTable('resetView');
        initBootstrapTable(7, 'resetView');

        $(".fixed-table-container").css({"height":"auto", "padding-bottom": "0"});
    });

    // Date picker
    var from = '${dateVoulue}'.split("-");
    var f = new Date(from[2], from[1] - 1, from[0]);
    $('.monthPicker').datepicker('setDate', new Date(f));

    activerTab();
});

function initBootstrapTable(iter, option) {
    for(let i=1; i<=iter; i++) {
        if(option === '') {
            $('#idBootstrapTable'+i).bootstrapTable();
        } else {
            $('#idBootstrapTable'+i).bootstrapTable(option);
        }
    }
}

/**
 * Activation de la bonne tab :
 * si aucune tab n'est active et aucun "hash" (#hash) dans l'url,
 * activer la tab par défaut. Sinon, activer le hash.
 */
function activerTab() {
    var menuFacture = $("#menuFacture");

    // vérifie si un hash existe, afin de retourner a la bonne page après traitement de facture
    var hash = document.getElementById('hash').innerHTML;
    var hashRet = document.getElementById('hashRet').innerHTML;
    var hashUrl = window.location.hash;

    // [AMA][AMN 576] si le hash existe, on affiche le menu correspondant
    // [AMA][AMN 576] hash correspond a une potentielle facture traitée (créée, ou éditée)
    if (hash) {
        $(hash).addClass("active");
        highlightMenu(hash);
        return;
    } else if (hashRet) { // [AMA][AMN 576] hashRet est un hash qui est généré en cas d'appuie sur le bouton "Retour" lors de la création ou l'édition
        $(hashRet).addClass("active");
        highlightMenu(hashRet);
        return;
    } else if (hashUrl)       // si le hash a directement été entré dans l'url (ex : bouton Valider pour changer la date voulue)
    {
        $(hashUrl).addClass("active");
        highlightMenu(hashUrl);
        return;
    }

    // [AMA][AMN 576] si aucune facture ne vient d'être traité, aucun hash récupéré
    var tabDefautHash = '#tabFacturable'; // [AMA][AMN 576] hash par défaut au cas où c'est le premier chargement de la page
    var tabDefaut = $(tabDefautHash);
    var activeTabs = $("div.active");

    // console.log("active tabs are :", activeTabs);
    if (activeTabs.length < 1) {
        tabDefaut.addClass("active");
        highlightMenu(tabDefautHash);
    }
}

function highlightMenu(hash) {
    var link = $('a[href$=' + hash + ']');
    // console.log("link is :", link);
    link.closest('li').addClass('active');
    link.closest('.dropdown').addClass('active');
}

// Update mission list for a given collaborator
function loadMission(idCollab, previousMonth, idButton, suffix) {
    let selectMission = $("#listMissionsClientesDuMoisVoulu" + suffix);

    // get collaborator id
    let temp = idCollab.value.split(" ");
    let id = temp[0];

    $.ajax({
        type: 'GET',
        url: 'Administration/loadMissionCourantes/' + id + '/' + previousMonth,
        dataType: 'json',
        success: function (json) {
            $.each(json, function (index, elem) {
                let missionOptionId = 'mission' + suffix + '_' + elem.id;
                selectMission.append('<option id="' + missionOptionId + '" value="' + elem.id + '">' + elem.mission + '</option>');
                if (index === 0) {
                    //Update order list according to selected mission
                    loadCommandes(document.getElementById(missionOptionId), idButton, suffix);
                }
            });
        }
    });

    selectMission.html(''); // empty
    $('#listCommandesDuMoisVouluMission' + suffix).html('');
}


// Update command list for a given mission
function loadCommandes(idMission, idButton, suffix) {

    let orderSelect = $('#listCommandesDuMoisVouluMission' + suffix);
    // Enable selection of orders
    orderSelect.attr('disabled', false);

    let id = idMission.value; // on récupère l'ID de la mission sélectionnée

    $.ajax({
        type: 'GET',
        url: 'Administration/loadCommandesDeMission/' + id,
        dataType: 'json',
        success: function (listeCommande) {

            orderSelect.empty();
            $.each(listeCommande, function (index, order) {
                let orderOptionId = 'order' + suffix + '_' + order.id;
                let dateDebut = getDateFormat(order.dateDebut);
                let dateFin = getDateFormat(order.dateFin);
                orderSelect.append('<option id="' + orderOptionId + '" value="' + order.id + '">[du ' + dateDebut + ' au ' + dateFin + '] n° ' + order.numero + '</option>');
            });

            // Select last order
            orderSelect.find(' option:last').prop('selected', true);
            disableButton(idButton, listeCommande);
        }
    });
}

/**
 * Disable button for creation when commands list is empty
 *
 * @param button id of button
 * @param listeCommande list of commands
 */
function disableButton(button, listeCommande) {
    button = '#' + button;
    if (listeCommande.length > 0) {
        $(button).attr('disabled', false);
    } else {
        $(button).attr('disabled', true);
    }

}

// [AMNT-597] formatte une date en TimeStamp en un affichage classique
function getDateFormat(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var date = new Date();
    date.toLocaleDateString();

    return [day, month, year].join('/');
}

function createExcels(typeFacture, typeDetravailleur) {
    $("#typeFacture").attr({value: typeFacture});
    $("#typeDeTravailleur").attr({value: typeDetravailleur});
    $("#modalValidationFacture").modal({
        backdrop: 'static'
    });
}

function openModalNumerotation(typeFacture, typeDetravailleur) {
    $("#typeFacture2").attr({value: typeFacture});
    $("#typeDeTravailleur2").attr({value: typeDetravailleur});
    $("#modalGenerationNumero").modal({
        backdrop: 'static'
    });
}

function soumissionFactures(typeFacture, typeDetravailleur) {
    $("#typeFacture3").attr({value: typeFacture});
    $("#typeDeTravailleur3").attr({value: typeDetravailleur});
    $("#soumissionFactures").modal({
        backdrop: 'static'
    });
}

function refusFactures(typeFacture, typeDetravailleur) {
    $("#typeFacture4").attr({value: typeFacture});
    $("#typeDeTravailleur4").attr({value: typeDetravailleur});
    $("#refusFactures").modal({
        backdrop: 'static'
    });
}

function forcerFactures() {
    // $("#forcerFactures").attribute({})
    $("#forcerFactures").modal({
        backdrop: 'static'
    });
}

function showLoading() {
    $("#idModalLoading").modal({
        backdrop: 'static'
    });
}

/**
 * Javascript function to retrieve a file through an API asynchronously (ajax) then display / hide indications element in case of no result http code 204 or in case of loading, in progress
 * @param requestUrl  the http url to get the file
 * @param DOMElementsIdsForInfosDisplaying here it is a list of ids of elements of the DOM which will allow to display information of state of the request, no result / load in progress, etc.
 */
function ajaxRequestGetFile(requestUrl, DOMElementsIdsForInfosDisplaying) {
    // Ici on affiche l'element 'traitement en cours' et on cache le bouton qui a appelé la fonction
    if (document.getElementById(DOMElementsIdsForInfosDisplaying.LOADING_displayer_id) != null) {
        document.getElementById(DOMElementsIdsForInfosDisplaying.LOADING_displayer_id).style.display = "block";
    }
    if (document.getElementById(DOMElementsIdsForInfosDisplaying.CALLER_BTN_id) != null) {
        document.getElementById(DOMElementsIdsForInfosDisplaying.CALLER_BTN_id).style.display = "none";
    }
    // requête ajax
    var xhr = new XMLHttpRequest();
    xhr.open('GET', requestUrl, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function (e) {
        switch (xhr.status) { // Codes http et traiements
            case 200: // SUCCESS
                try {
                    // Pour récuperer le nom du fichier dans le header
                    var filename = "";
                    var disposition = xhr.getResponseHeader('Content-Disposition');
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    filename = (matches != null) ? matches[1].replace(/['"]/g, '') : "document";
                    // Un objet Blob représente un objet, semblable à un fichier, qui est immuable et qui contient des données brutes.
                    var blob = new Blob([this.response], {type: this.contentType});

                    // On va créer un lien de téléchargement puis enclure le fichier dans le lien

                    // version Chrome
                    if (typeof window.chrome !== 'undefined') {
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = filename;
                        link.click();
                    }

                    // version Internet Explorer
                    else if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        window.navigator.msSaveBlob(blob, filename);
                    }

                    // version Firefox
                    else {
                        var file = new File([this.response], filename, {type: this.contentType});
                        window.open(URL.createObjectURL(file));
                    }
                } catch (e) {
                    alert(e);
                }

                break;
            case 204: // NO CONTENT  la requête s'est bien executé mais aucun résultat retourné, exemple aucune facture correspondant
                // Affichage en cas de résultat null
                if (document.getElementById(DOMElementsIdsForInfosDisplaying.NO_CONTENT_displayer_id) != null) {
                    document.getElementById(DOMElementsIdsForInfosDisplaying.NO_CONTENT_displayer_id).style.display = "block";
                } else {
                    alert("Aucun résultat trouvé");
                }
                break;
            default:
                break;
        }
    }
    // On cache l'element 'traitement en cours une fois que la requete ait été réalisée, puis on réaffiche le bouton qui a aappelé la fonction
    xhr.onloadend = function (e) {
        if (document.getElementById(DOMElementsIdsForInfosDisplaying.LOADING_displayer_id) != null) {
            document.getElementById(DOMElementsIdsForInfosDisplaying.LOADING_displayer_id).style.display = "none";
        }
        if (document.getElementById(DOMElementsIdsForInfosDisplaying.CALLER_BTN_id) != null) {
            document.getElementById(DOMElementsIdsForInfosDisplaying.CALLER_BTN_id).style.display = "block";
        }
    }
    xhr.send();
}
