var urlDelate = "Administration/deleteCollaborateur/";

function handleClick(cb) {
    if (cb.checked) {
        $(location).attr('href', "${ administration }${ collaborateurs }/true");
    }
    else {
        $(location).attr('href', "${ administration }${ collaborateurs }/false");
    }
}

function annulerFiltre() {
    window.location.href = window.location.pathname;
}

$(function() {
    $('.monthPicker').change(function () {
        window.location.href = window.location.pathname + "?monthYearExtract=" + this.value;
    });
});

