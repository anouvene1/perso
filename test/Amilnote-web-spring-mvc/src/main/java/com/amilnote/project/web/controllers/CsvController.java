package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.TypeFacture;
import com.amilnote.project.metier.domain.services.DocumentCSVService;
import com.amilnote.project.metier.domain.services.FileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;


/**
 * Handler focused on the CSV files generation only
 */
@Controller
@RequestMapping("/csv")
public class CsvController extends AbstractController {

    private static final String EXTRACT_MONTH_YEAR = "monthYearExtract";
    private static final String APPLICATION_CSV = "application/csv";
    private static final String CACHE_CONTROL = "must-revalidate, post-check=0, pre-check=0";

    @Autowired
    DocumentCSVService documentCSVService;

    @Autowired
    FileService fileService;

    private static Logger logger = LogManager.getLogger(CsvController.class);

    /**
     * Generates a CSV file containing all collaborators expenses for a given month
     *
     * @param request the {@link HttpServletRequest}
     * @param model   the {@link Model}
     * @return an {@link ResponseEntity} representing the file itself
     */
    @RequestMapping(value = "/fraiscollaborateurs", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<byte[]> fraisCollaborateurs(HttpServletRequest request, Model model) {

        init(model);

        ResponseEntity<byte[]> responseEntity;
        String pathOfFileToDownload;
        String dateParameter = request.getParameter(EXTRACT_MONTH_YEAR);
        byte[] fileContent = new byte[]{};
        HttpHeaders headers = new HttpHeaders();

        try {

            pathOfFileToDownload = documentCSVService.createCSVFileFrais(dateParameter, TypeFacture.TYPEFACTURE_FACTURE_FRAIS_COLLAB_CSV, null);
            fileContent = fileService.readFileByPath(pathOfFileToDownload);
            File file = new File(pathOfFileToDownload);

            headers.setContentType(MediaType.parseMediaType(APPLICATION_CSV));
            headers.setContentDispositionFormData(file.getName(), file.getName());
            headers.setCacheControl(CACHE_CONTROL);

        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        responseEntity = new ResponseEntity<>(fileContent, headers, HttpStatus.OK);

        return responseEntity;

    }


}
