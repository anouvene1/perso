/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.AddviseLinkSessionCollaborateur;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.AddviseService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Encryption;
import com.amilnote.project.metier.domain.utils.Parametrage;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * The type Mail controller.
 */
@Controller
public class MailController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(MailController.class);

    @Autowired
    CollaboratorService collaboratorService;

    @Autowired
    AddviseService addviseService;

    @Autowired
    MailService mailService;


    /**
     * Mail forfait string.
     *
     * @param request  the request
     * @param model    the model
     * @param subject  the subject
     * @param message  the message
     * @param redirect the redirect
     * @return the string
     */
    @RequestMapping(value = "mailAdministrateur", method = RequestMethod.POST)
    public String mailForfait(HttpServletRequest request, Model model, @RequestParam String subject, @RequestParam String message,
                              final RedirectAttributes redirect) {
        init(request, model);

        // récupération du mail du user en session
        UserDetails lUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String lMail = lUserDetails.getUsername();

        /*
         * préparation du mail d'envoi
         */
        try {
            SimpleEmail email = new SimpleEmail();

            email.setHostName(Parametrage.getContext(Constantes.HOSTNAME_ADMIN_EMAIL_CONTEXT));
            email.setAuthentication(Parametrage.getContext(Constantes.AUTHENTICATION_ADMIN_ADDRESS_CONTEXT),
                    Parametrage.getContext(Constantes.AUTHENTICATION_ADMIN_PASSWORD_CONTEXT));

            email.setSmtpPort(25);

            email.addTo(Parametrage.getContext(Constantes.EMAIL_ADMIN_ADDRESS_CONTEXT));
            email.setFrom(lMail, lMail);

            email.setSubject(subject);
            email.setCharset("UTF-8");
            email.setMsg(message);

            email.send();

            redirect.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "<h4>Votre mail a bien été transmis.</h4><p> <br> Votre demande sera traitée dans les plus brefs délais.</p>");

        } catch (EmailException e) {
            log.error("envoi de mail échoué", e);
            redirect.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "<h4>L'envoi du mail a échoué.</h4><p> Contactez votre administrateur.<br>L'opération a été annulée.</p>");
        } catch (NamingException e) {
            log.error("envoi de mail échoué: parametres email invalide", e);
            redirect.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "<h4>L'envoi du mail a échoué.</h4><p> Contactez votre administrateur.<br>L'opération a été annulée.</p>");
        }

        return "redirect:" + NAV_NOTEDEFRAIS;
    }

    /**
     * Mail string.
     *
     * @param request  the request
     * @param model    the model
     * @param response the response
     * @return the string
     * @throws IOException the io exception
     */
    @RequestMapping(value = "/mail", method = RequestMethod.GET)
    public String mail(HttpServletRequest request, Model model, HttpServletResponse response) throws IOException {
        init(request, model);

        return NAV_GESTIONMAIL;
    }

    /**
     * Reset mdp oublie string.
     *
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return the string
     */
    @RequestMapping(value = "resetMdpOublie", method = {RequestMethod.GET, RequestMethod.POST})
    public String resetMdpOublie(HttpServletRequest request, Model model, RedirectAttributes ra) {
        init(request, model);

        String mail = request.getParameter("mail");
        Collaborator collaborateur = collaboratorService.findByMail(mail);

        // si on a trouvé le collaborateur avec son mail, on réinitialisera le mdp automatiquement
        if (collaborateur != null) {
            String newMdp = com.amilnote.project.web.utils.AmiltoneUtils._generate(11);

            String hashedNewMDP = "";

            try {
                hashedNewMDP = Encryption.encryptSHA(newMdp);
            } catch (NoSuchAlgorithmException e) {
                logger.error(e.getMessage());
            }

            CollaboratorAsJson collabJson = new CollaboratorAsJson();
            String resultat = "";
            try {
                collabJson.setId(collaborateur.getId());
                collabJson.setPassword(hashedNewMDP);
                resultat = collaboratorService.updateOrCreateCollaborator(collabJson);
                if ("1".equals(resultat)) {
                    ra.addFlashAttribute(NAME_NAV_TEMP_REINITMDP, true);
                    ra.addFlashAttribute(NAME_NAV_REINITIALISE_MDP, "Sous réserve d'email valide, un nouveau mot de passe vous sera envoyé.");

                    String env = "";
                    if (!Boolean.valueOf(Parametrage.getContext("env.prod"))) env = "[DEV]";
                    mailService.sendMailPasswordReset(collaborateur, env, newMdp);
                } else {
                    ra.addFlashAttribute(NAME_NAV_TEMP_REINITMDP, false);
                    ra.addFlashAttribute(NAME_NAV_ERREUR_MDP, "Erreur lors du changement de mot de passe en base. Veuillez réessayer ultérieurement.");
                }
            } catch (Exception e) {
                logger.error("[reset mdp] collaborator id : {}", collaborateur.getId(), e);
                ra.addFlashAttribute(NAME_NAV_TEMP_REINITMDP, false);
                ra.addFlashAttribute(NAME_NAV_ERREUR_MDP, "Erreur lors du changement de mot de passe en base. Veuillez réessayer ultérieurement.");
            }
        } else {
            ra.addFlashAttribute(NAME_NAV_TEMP_REINITMDP, false);
            ra.addFlashAttribute(NAME_NAV_ERREUR_MDP, "Ce nom d'utilisateur n'existe pas.");
        }

        return "redirect:/mail";
    }

    @RequestMapping(value = "addviseReponseMail/{action}/{token}", method = RequestMethod.GET)
    public String addviseReponseMail(@PathVariable("token") String token, @PathVariable("action") String action, HttpServletRequest request, Model model, HttpServletResponse response) throws MessagingException, IOException, EmailException, NamingException {
        init(request, model);
        String url = request.getRequestURL().toString();

        AddviseLinkSessionCollaborateur invitation = addviseService.manageInvitationByToken(token, url, action);

        request.setAttribute("invitation", invitation);

        return NAV_REPONSE_MAIL_ADDVISE;
    }


}
