/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.EtatAsJson;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderAvanceFrais;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Controller des Notes de frais
 *
 * @author GCornet
 * @author KEsprit
 */
@Controller
@RequestMapping("/mesNotesDeFrais")
public class FraisController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(FraisController.class);

    @Autowired
    private FraisService fraisService;

    @Autowired
    private TypeFraisService typeFraisService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private RapportActivitesService raService;

    @Autowired
    private LinkEvenementTimesheetService linkEvementTimesheetService;

    @Autowired
    private JustifService justifService;

    @Autowired
    private PDFBuilderRA pdfBuilder;

    @Autowired
    private PDFBuilderAvanceFrais pdfBuilderFrais;

    @Autowired
    private EtatService etatService;

    @Autowired
    private AmiltoneUtils utils;

    @Autowired
    private CollaboratorService collabService;

    @Autowired
    private FileService fileService;

    /**
     * Notes de frais string.
     *
     * @param pFiltre the p filtre
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return the string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "frais/{pFiltre}", method = {RequestMethod.GET, RequestMethod.POST})
    public String notesDeFrais(@PathVariable("pFiltre") String pFiltre, HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        session.setAttribute(NAME_NAV_FRAIS_NOTESDEFRAIS, NAV_FRAIS_NOTESDEFRAIS);
        Collaborator collaborateur = utils.getCurrentCollaborator();
        List<FraisAsJson> list = fraisService.getFraisByCollaborator(collaborateur);
        for (FraisAsJson frais : list) {
            TypeFrais type = typeFraisService.getTypeFraisByCode(frais.getTypeFrais());
            frais.setTypeFrais(type.getTypeFrais());
        }

        if (!"null".equals(pFiltre)) {
            List<FraisAsJson> listTemp = new ArrayList<>();
            for (FraisAsJson frais : list) {
                if (frais.getEtat().getCode().equals(pFiltre)) {
                    listTemp.add(frais);
                }
            }
            list = listTemp;
        }
        model.addAttribute("listFrais", list);
        model.addAttribute("listTypeFrais", typeFraisService.getAllTypeFraisAsJson());
        model.addAttribute("listMission", missionService.findAllMissionsNotFinishForCollaborator(
                utils.getCurrentCollaborator(), Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_SELECTOR_FRAIS));

        return NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS;
    }

    /***
     * Méthode permettant l'édition d'un frais existant
     *
     * @param id      the id
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Redirection vers une autre page
     * @throws Exception the exception
     */
    @RequestMapping(value = {"editFrais/{idFrais}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String editFrais(@PathVariable("idFrais") Long id, HttpServletRequest request, Model model, HttpSession session) throws Exception {
        init(request, model);
        session.setAttribute(NAME_NAV_FRAIS_NOTESDEFRAIS, NAV_FRAIS_NOTESDEFRAIS);
        Collaborator currentCollab = utils.getCurrentCollaborator();
        if (null != id) {
            Frais fraisToEdit = fraisService.get(id);
            if (null != fraisToEdit) {
                LinkEvenementTimesheet eve = fraisToEdit.getLinkEvenementTimesheet();
                if (currentCollab.getId().equals(eve.getCollaborator().getId())) {
                    model.addAttribute("modeCreation", false);
                    if (fraisToEdit.getNbNuit() != null) {
                        model.addAttribute("nbNuit", fraisToEdit.getNbNuit());
                    }
                    FraisAsJson fraisJson = fraisToEdit.toJSon();

                    if (fraisJson.getTypeFrais().equals(TypeFrais.TYPEFRAIS_ABONNEMENTTRANSPORTS))
                        fraisJson.setMontant(fraisJson.getMontant().multiply(new BigDecimal("2")));

                    fraisJson.setIdMission(fraisToEdit.getLinkEvenementTimesheet().getMission().getId());
                    model.addAttribute("typeFrais", fraisToEdit.getTypeFrais().getTypeFrais());
                    model.addAttribute("typeAvance", fraisToEdit.getTypeAvance());
                    model.addAttribute("fraisForfait", fraisToEdit.getFraisForfait());
                    model.addAttribute("typeDeFrais", fraisToEdit.getTypeFrais().getCode());
                    model.addAttribute("fraisToEdit", fraisJson);
                    model.addAttribute("listTypeFrais", typeFraisService.getAllTypeFraisAsJson());
                    //Récupération de la mission du frais
                    model.addAttribute("mission", fraisToEdit.getLinkEvenementTimesheet().getMission().getMission());
                    model.addAttribute("id_mission", fraisToEdit.getLinkEvenementTimesheet().getMission().getId());
                    //Récupération des justificatifs liés au frais
                    model.addAttribute("listJustifs", fraisToEdit.getJustifs());

                    fraisToEdit.getLinkEvenementTimesheet().getMission().getListMissionForfaits();
                    //Vérification de la présence d'un forfait déplacement
                    //dans le cas d'un frais de type : Avion, Train, Voiture, Location, Taxi, Transport (Bus/Navette)
                    String typeDeFraisJava = fraisToEdit.getTypeFrais().getCode();
                    if (TypeFrais.TYPEFRAIS_VOITURE.equals(typeDeFraisJava) || TypeFrais.TYPEFRAIS_TRAIN.equals(typeDeFraisJava)
                            || TypeFrais.TYPEFRAIS_AVION.equals(typeDeFraisJava) || TypeFrais.TYPEFRAIS_LOCATION.equals(typeDeFraisJava)
                            || TypeFrais.TYPEFRAIS_TAXI.equals(typeDeFraisJava) || TypeFrais.TYPEFRAIS_TRANSPORT.equals(typeDeFraisJava)) {

                        List<LinkMissionForfait> listMissionForfait = fraisToEdit.getLinkEvenementTimesheet().getMission().getListMissionForfaits();
                        Boolean test = false;
                        for (LinkMissionForfait missionForfait : listMissionForfait) {
                            if (Forfait.CODE_DEPLACEMENT.equals(missionForfait.getForfait().getCode())) {
                                model.addAttribute("testFraisForfait", true);
                                test = true;
                            }
                        }
                        if (!test) {
                            model.addAttribute("testFraisForfait", false);
                        }
                    } else {
                        model.addAttribute("testFraisForfait", false);
                    }


                    return NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS;
                } else {
                    return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
                }
            }
        }
        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
    }

    /***
     * Méthode permettant l'ajout d'un nouveau frais
     *
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return Redirection vers une autre page
     */
    @RequestMapping(value = {"editFrais", "editFrais/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newFrais(HttpServletRequest request, Model model, RedirectAttributes ra) {
        init(request, model);

        String typeDeFrais = request.getParameter("typeFrais");
        String idMission = request.getParameter("idMission");

        if (!collabService.hasMissionById(utils.getCurrentCollaborator(), Long.valueOf(idMission))) {
            idMission = null; // causera le retour de "Mission non conforme"
        }

        TypeFrais tmpTypeFrais = typeFraisService.findUniqEntiteByProp("code", typeDeFrais);
        StringBuffer erreur = new StringBuffer();

        if (null == typeDeFrais) {
            erreur.append("Type de frais non conforme.\n");
        }
        if (null == idMission) {
            erreur.append("Mission non conforme.");
        }

        if (null != typeDeFrais && null != idMission) {

            FraisAsJson fraisToCreate = new FraisAsJson();
            fraisToCreate.setTypeFrais(typeDeFrais);
            fraisToCreate.setIdMission((long) Integer.parseInt(idMission));
            model.addAttribute("modeCreation", true);
            model.addAttribute("typeDeFrais", typeDeFrais);
            model.addAttribute("typeFrais", tmpTypeFrais.getTypeFrais());
            model.addAttribute("fraisForfait", "");
            model.addAttribute("fraisToEdit", fraisToCreate);
            List<MissionAsJson> listMission = missionService.findAllMissionsCollaborator(utils.getCurrentCollaborator());
            model.addAttribute("listMissions", listMission);
            Mission mission = missionService.get((long) Integer.parseInt(idMission));
            model.addAttribute("mission", mission.getMission());
            model.addAttribute("id_mission", idMission);

            //Vérification de la présence d'un forfait déplacement
            //dans le cas d'un frais de type : Avion, Train, Voiture, Location, Taxi, Transport (Bus/Navette)
            if (TypeFrais.TYPEFRAIS_VOITURE.equals(typeDeFrais) || TypeFrais.TYPEFRAIS_TRAIN.equals(typeDeFrais)
                    || TypeFrais.TYPEFRAIS_AVION.equals(typeDeFrais) || TypeFrais.TYPEFRAIS_LOCATION.equals(typeDeFrais)
                    || TypeFrais.TYPEFRAIS_TAXI.equals(typeDeFrais) || TypeFrais.TYPEFRAIS_TRANSPORT.equals(typeDeFrais)) {
                Boolean test = false;
                List<LinkMissionForfait> listMissionForfait = mission.getListMissionForfaits();
                for (LinkMissionForfait missionForfait : listMissionForfait) {
                    if (Forfait.CODE_DEPLACEMENT.equals(missionForfait.getForfait().getCode())) {
                        model.addAttribute("testFraisForfait", true);
                        test = true;
                    }
                }
                if (!test) {
                    model.addAttribute("testFraisForfait", false);
                }
            } else {
                model.addAttribute("testFraisForfait", false);
            }

            return NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS;
        }

        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, erreur.toString());
        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
    }

    /***
     * Méthode permettant l'association d'un ou plusieurs frais à un ou
     * plusieurs justificatifs
     *
     * @param request the request
     * @param model   the model
     * @return Redirection vers une autre page
     */
    @RequestMapping(value = {"assocFraisJustifs", "assocFraisJustifs/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String assocFraisJustifs(HttpServletRequest request, Model model) {
        init(request, model);

        Collaborator collaborateur = utils.getCurrentCollaborator();
        List<Justif> listJustifs = new ArrayList<>();
        if (null != collaborateur) {
            listJustifs = justifService.getAllJustifForCollaborator(collaborateur);
        }
        model.addAttribute("listJustifs", listJustifs);
        return NAV_NOTEDEFRAIS + NAV_FRAIS_ASSOCIATION_JUSTIFS;
    }

    /**
     * Retourne le fichier correspondant au justificatif souhaité
     *
     * @param idFichier id du fichier a récupérer
     * @param request   the request
     * @param response  the response
     * @param model     the model
     * @throws Exception the exception
     */
    @RequestMapping(value = "download/{idFichier}", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadFichier(@PathVariable("idFichier") Long idFichier, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idFichier) {
            Justif justif = justifService.get(idFichier);

            //[ALV] [AMNOTE-224] Interdire de visualiser la note de frais si on n'est pas le collaborateur concerné
            //Les rôles supérieurs aux collaborateurs ont toujours l'accès
            Collaborator collaborateur = utils.getCurrentCollaborator();
            if (StatutCollaborateur.STATUT_COLLABORATEUR.equals(collaborateur.getStatut().getCode()) &&
                    collaborateur != justif.getCollaborator()) {
                throw new AccessDeniedException("Les collaborateurs non concernés n'ont pas accès à ce fichier.");
            }

            //Récupération de l'extension du fichier
            String extension = justif.getFichier().substring(justif.getFichier().lastIndexOf(".") + 1);


            // Récupération du nom du justificatif - sans l'extension
            String fileName = (justif.getFichier()).substring(0, justif.getFichier().lastIndexOf("."));

            //--- Récupération du fichier
            File lFile = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + justif.getFichier());

            // permet de savoir si le fichier qui sera lu a été décompressé ou pas
            Boolean unzipped = false;

            // --- Vérification de l'existance du fichier
            if (!lFile.exists()) {

                // si le fichier n'existe pas en .PDF, vérifier qu'il existe en .ZIP
                if (fileService.fileWasZipped(lFile)) {
                    lFile = fileService.unZipFile(new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + File.separator + fileName + Constantes.EXTENSION_FILE_ZIP));
                } else {
                    throw new Exception("Fichier '" + lFile.getPath() + "'" + ": introuvable");
                }
            }


            //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
            httpResponseConstruct(extension, response, lFile);

            // si le justificatif affiché est issu d'un .ZIP, il est supprimé pour ne garder que la version compressée (gain de place serveur)
            // ainsi on ne supprime pas par erreur les anciens justificatifs .PDF conservés dans le serveur
            if (fileService.fileWasZipped(lFile)) {
                fileService.deleteFile(lFile);
            }
        }
    }

    /**
     * Gets image justif.
     *
     * @param pIdJustif the p id justif
     * @param response  the response
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     * @throws Exception               the exception
     */
    @RequestMapping(value = "getImageJustif", method = {RequestMethod.GET, RequestMethod.POST})
    public void getImageJustif(@RequestBody String pIdJustif, HttpServletResponse response) throws Exception {

        //Récupération de l'extension du fichier
        Justif justif = justifService.get(Long.valueOf(pIdJustif));

        File file = new File(Parametrage.getContext("dossierFrais") + justif.getFichier());

        if (!file.exists()) {

            if (fileService.fileWasZipped(file)) {

                String fileName = (justif.getFichier()).substring(0, justif.getFichier().lastIndexOf("."));
                file = fileService.unZipFile(new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + File.separator + fileName + Constantes.EXTENSION_FILE_ZIP));
            } else {
                response.getWriter().print("Erreur: Fichier '" + file.getPath() + "'" + ": introuvable");
                return;
            }
        }

        String typeMime = AmiltoneUtils.getFileMIMEType(file);

        InputStream stream = new FileInputStream(file);
        response.setContentType(typeMime);
        response.getWriter().print(
                "data:" + typeMime + ";base64,"
                        + Base64.getEncoder().encode(IOUtils.toByteArray(stream)));
        stream.close();

        // si le justificatif affiché est issu d'un .ZIP, il est supprimé pour ne garder que la version compressée (gain de place serveur)
        // ainsi on ne supprime pas par erreur les anciens justificatifs .PDF conservés dans le serveur
        if (fileService.fileWasZipped(file)) {
            fileService.deleteFile(file);
        }

    }

    /***
     * Méthode permettant l'association d'un frais en particulier à un ou
     * plusieurs justificatifs
     *
     * @param idFrais the id frais
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Redirection vers une autre page
     * @throws Exception the exception
     */
    @RequestMapping(value = {"assocFraisJustifs/{idFrais}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String assocFraisJustifs(@PathVariable("idFrais") Long idFrais, HttpServletRequest request, Model model, HttpSession session) throws Exception {
        init(request, model);

        model.addAttribute("assocJustif", true);

        Collaborator collaborateur = utils.getCurrentCollaborator();
        List<Justif> listJustifsColl = new ArrayList<>();
        if (null != collaborateur) {
            listJustifsColl = justifService.getAllJustifForCollaborator(collaborateur);
            Frais item = fraisService.get(idFrais);
            if (item != null) {
                List<Justif> listJustifsFrai = item.getJustifs();
                listJustifsColl.removeAll(listJustifsFrai);
            }
        }
        model.addAttribute("listJustifs", listJustifsColl);

        return NAV_NOTEDEFRAIS + NAV_FRAIS_ASSOCIATION_JUSTIFS;
    }

    /**
     * Méthode permettant de sauvegarder un frais
     *
     * @param nomFichier = Descriptif du ou des justificatifs donnés
     * @param fichiers   the fichiers
     * @param request    the request
     * @param model      the model
     * @param frais      Frais à sauvegarder
     * @param pRedir     the p redir
     * @return Redirection vers une autre page
     */
    @RequestMapping(value = "saveFrais", method = RequestMethod.POST)
    public String saveFrais(
            @RequestParam(value = "nomFichier", required = false) String nomFichier,
            @RequestParam(value = "fichier", required = false) MultipartFile fichiers,
            HttpServletRequest request, Model model, FraisAsJson frais, RedirectAttributes pRedir) {

        init(request, model);

        Mission mission = missionService.get(frais.getIdMission());

        boolean creation = frais.getId() == null;

        if (null != mission && null != fichiers) {
            try {
                // --- Récupération du collaborateur courant
                Collaborator collaborateur = utils.getCurrentCollaborator();

                Optional<RapportActivites> existingValideRAforMonth = raService.getExistingRAForMonth(collaborateur, new DateTime(frais.getDateEvenement()))
                        .stream()
                        .filter(ra -> Etat.ETAT_SOUMIS_CODE.equals(ra.getEtat().getCode()))
                        .findFirst();

                if (existingValideRAforMonth.isPresent()) {
                    DateTime dateExistingRA = new DateTime(existingValideRAforMonth.get().getMoisRapport());
                    String errorMessage = String.format(
                            "Le rapport d'activité pour le mois %s/%s est soumis, impossible d'ajouter un frais sur cette période. Merci de demander une annulation du rapport d'activité avant de réessayer.",
                            dateExistingRA.getMonthOfYear(),
                            dateExistingRA.getYear()
                    );
                    pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage); //Affichage de l'erreur
                    return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
                }

                //empecher la modif de frais déjà soumis, validé ou soldé
                if (frais.getId() != null) {
                    String test = verifEtatfrais(frais, pRedir);
                    if (test != null) return test;
                }

                //On récupère le premier valeur du tableau pour voir si le fichier a passé le test ou non
                //Prend les extensions (sans '.') et les types mimes :
                boolean verification = false;
                if (creation || !fichiers.isEmpty()) { //Verification que si on est en creation du frais (fichier obligatoire) ou un fichier a été envoyé en mode edition
                    if (!(frais.getTypeFrais().equals("VO") || frais.getTypeFrais().equals("ASF"))) {
                        verification = true;
                    } else {
                        //Avance sur frais ou voiture personnelle avec fichier
                        if (!fichiers.isEmpty()) {
                            verification = true;
                        }
                        //Avance sur frais permanente
                        if (frais.getTypeFrais().equals("ASF") && frais.getTypeAvance() != null) {
                            if (frais.getTypeAvance().equals("on")) {
                                verification = true;
                            }
                        }
                    }
                }
                if (verification) {
                    try {
                        fileService.isValidToUpload(fichiers, UploadFile.MAX_SIZE_JUSTIF_FRAIS, FileFormatEnum.values());
                        fileService.isValidImageContent(fichiers);
                    } catch (Exception e) {
                        pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, e.getMessage()); //Affichage de l'erreur
                        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
                    }
                }
                if (frais.getMontant() != null) {
                    if (frais.getMontant().floatValue() < 0) {
                        String errorMessage = String.format("Impossible d'ajouter ce frais, Le montant indiqué est inférieur à 0");
                        pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage); //Affichage de l'erreur
                        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
                    }
                }

                // sauvegarder la note de frais
                String message = fraisService.createOrUpdateFrais(frais, mission);

                // si le message contient "succes" il faudra l'afficher en vert
                if (message.contains("succès")) {
                    String formatDeb = "yyyy-MM-01";
                    SimpleDateFormat formater = new SimpleDateFormat(formatDeb);
                    Date dateDeb = frais.getDateEvenement();
                    String dateString = formater.format(dateDeb);
                    dateDeb = formater.parse(dateString);

                    Timestamp time = new Timestamp(dateDeb.getTime());
                    Boolean fraisCurrentMonth = false;
                    List<RapportActivites> listRA = raService.getAllRAForCollaborator(collaborateur);
                    for (RapportActivites ra : listRA) {
                        if (ra.getMoisRapport().equals(time)) {
                            Etat etat = ra.getEtat();
                            if (etat.getId() == 1 || etat.getId() == 5 || etat.getId() == 4) { //Message de rappel si on a soumis le RA sans les frais ou si il a été refusé/annulé
                                pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "<h4>Ajout/Modification effectuée !</h4> <h5>Pensez à valider votre RA final pour soumettre les frais!</h5>");
                                fraisCurrentMonth = true;
                                break;
                            }
                        }
                    }
                    if (!fraisCurrentMonth) {
                        pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, message);
                    }

                    // ajouter la pièce justificative.
                    if (fichiers != null && !fichiers.isEmpty()) {
                        // par défaut la description du justificatif est le nom de la pièce justificative.
                        String filename = fichiers.getOriginalFilename();

                        // supprimer la pièce justificative déjà associée si on n'est pas en mode création
                        if (!creation) {
                            List<Justif> pjs = justifService.findJustifByIdFrais(frais.getId());
                            // il ne devrait y en avoir qu'une
                            for (Justif pj : pjs) {
                                justifService.deleteJustif(pj);
                            }
                        }


                        if (null != collaborateur) {
                            // si l'utilisateur a fourni un descriptif, ce dernier remplace la description par défaut.
                            if (!nomFichier.trim().isEmpty()) {
                                filename = nomFichier;
                            }
                            String path = Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS);
                            justifService.saveNewJustifAndApplyToFrais(path, filename, fichiers, frais.getId(), collaborateur);
                        }
                    }

                } else {
                    pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, message);
                }
            } catch (Exception ex) {
                pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, ex.getMessage());
                if (frais.getId() == null) {
                    pRedir.addAttribute("typeFrais", frais.getTypeFrais());
                }
                return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS + (frais.getId() == null ? "" : "/" + frais.getId());
            }
        }

        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
    }

    /**
     * Méthode permettant de supprimer un frais
     *
     * @param request the request
     * @param model   the model
     * @param id      ID du frais à supprimer
     * @param ra      the ra
     * @return Redirection vers la page des frais
     */
    @RequestMapping(value = "deleteFrais/{idFrais}", method = {RequestMethod.POST, RequestMethod.GET})
    public String deleteFrais(HttpServletRequest request, Model model, @PathVariable("idFrais") Long id, RedirectAttributes ra) {
        init(request, model);

        try {
            // --- Vérification de l'ID
            if (null == id) {
                throw new NullArgumentException("id is null");
            }

            List<Long> lListFraisToDelete = new ArrayList<Long>();
            Frais frais = fraisService.get(id);

            //empecher la modif de frais déjà soumis, validé ou soldé
            if (frais.getId() != null) {
                String test = verifEtatfrais(frais.toJSon(), ra);
                if (test != null) return test;
            }

            Collaborator collaborateur = utils.getCurrentCollaborator();

            if (!collaborateur.equals(frais.getLinkEvenementTimesheet().getCollaborator())) {
                throw new AccessDeniedException("deletion denied");
            }

            // suppression du frais
            lListFraisToDelete.add(id);
            fraisService.deleteFrais(lListFraisToDelete);

            // suppression des justificatifs liés au frais
            frais.getJustifs().stream().forEach(x -> justifService.deleteJustif(x));

            DateTime dateRAVoulue = new DateTime(frais.getLinkEvenementTimesheet().getDate());

        } catch (Exception e) {
            logger.error("[Deleting fees]", e);
        }

        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
    }

    /**
     * Méthode permettant de supprimer un justificatif
     *
     * @param request  the request
     * @param model    the model
     * @param idJustif ID du justificatif à supprimer
     * @param ra       the ra
     * @return Redirection vers la page des frais
     */
    @RequestMapping(value = "deleteJustif/{id}", method = {RequestMethod.POST, RequestMethod.GET})
    public String deleteJustif(HttpServletRequest request, Model model, @PathVariable("id") Long idJustif, RedirectAttributes ra) {
        init(request, model);
        String idFrais = "";
        // --- Vérification de l'ID
        try {
            if (null != idJustif) {
                Justif justif = justifService.get(idJustif);

                Boolean FraisValide = false;

                //On vérifie qu'aucun frais n'est lié sinon erreur
                List<Frais> listFrais = fraisService.findFraisByJustif(justif);

                if (listFrais.size() > 0) {
                    //Bloquer la suppression d'un justificatif si ce dernier est lié à un frais dans un état autre que brouillon
                    for (Frais f : listFrais) {
                        idFrais = Long.toString(f.getId());
                        Etat e = f.getEtat();
                        String cd = e.getCode();
                        if (cd.compareToIgnoreCase(Etat.ETAT_BROUILLON_CODE) != 0) {
                            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Le justificatif est lié à un Frais validé. Veuillez d'abord supprimer le Frais avant de supprimer le justificatif.");
                            FraisValide = true;
                            break;
                        }
                    }
                }

                if (!FraisValide) {
                    String result = justifService.deleteJustif(justif);
                    if (StringUtils.equals(result, "OK")) {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Le justificatif a été supprimé avec succès.");
                    } else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, result);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Erreur lors de la suppression du frais", e);
        }
        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS + "/" + idFrais;
    }

    /**
     * Méthode permettant de sauvegarder un justificatif
     * ATTENTION : Dans le cadre d'un justificatif sans lien avec un frais, le idFrais = null
     * et est géré dans le JustifService
     *
     * @param nomFichier the nom fichier
     * @param fichiers   the fichiers
     * @param idFrais    the id frais
     * @param request    the request
     * @param model      the model
     * @param frais      the frais
     * @return Redirection vers une autre page
     * @throws Exception the exception
     */
    @RequestMapping(value = "saveJustif", method = RequestMethod.POST)
    public String saveJustif(@RequestParam("nomFichier") String nomFichier, @RequestParam("fichier") MultipartFile[] fichiers,
                             @RequestParam("idFrais") Long idFrais, HttpServletRequest request, Model model, FraisAsJson frais) throws Exception {
        init(request, model);

        if (fichiers.length > 0) {
            // avant d'ajouter tous les fichiers,
            // contrôler le format de chaque pièce justificative et rejeter en bloc si l'une d'elles n'est pas au format jpeg, png ou pdf.
            for (MultipartFile fic : fichiers) {
                String typ = fic.getContentType();
                if (!typ.contentEquals("image/jpeg") && !typ.contentEquals("image/png") && !typ.contentEquals("application/pdf")) {
                    model.addAttribute("error", "Le format du justificatif {0} ne fait pas partie des formats autorisés (jpeg, png ou pdf).".replaceAll("[{]0[}]", fic.getOriginalFilename()));
                    model.addAttribute("idFrais", idFrais);
                    return NAV_NOTEDEFRAIS + NAV_FRAIS_NEW_JUSTIF;
                }
            }

            // ajouter toutes les pièces.
            for (MultipartFile fic : fichiers) {
                // par défaut la description du justificatif est le nom de la pièce justificative.
                String filename = fic.getOriginalFilename();
                // --- Récupération du collaborateur courant
                Collaborator collaborateur = utils.getCurrentCollaborator();
                if (null != collaborateur) {
                    // si l'utilisateur a fourni un descriptif, ce dernier remplace la description par défaut.
                    if (!nomFichier.trim().isEmpty()) {
                        // mais s'il y a plus d'une pièce justificative, alors le nom de la pièce justificative est concaténée au descriptif.
                        filename = nomFichier + (fichiers.length > 1 ? " - " + filename : "");
                    }
                    String path = Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS);
                    justifService.saveNewJustifAndApplyToFrais(path, filename, fic, idFrais, collaborateur);
                }
            }

            if (idFrais == null) {
                return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
            } else {
                return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS + "/" + idFrais;
            }

        } else {
            // pas de pièce justificative ? => Erreur
            model.addAttribute("error", "Veuillez sélectionner un justificatif à ajouter.");
            model.addAttribute("idFrais", idFrais);
            return NAV_NOTEDEFRAIS + NAV_FRAIS_NEW_JUSTIF;
        }
    }

    /**
     * Méthode permettant de supprimer un frais
     *
     * @param request  the request
     * @param model    the model
     * @param idFrais  ID du frais à supprimer
     * @param idJustif the id justif
     * @return Redirection vers la page des frais
     * @throws Exception the exception
     */
    @RequestMapping(value = "retirerJustif/{idFrais}/{idJustif}", method = {RequestMethod.POST, RequestMethod.GET})
    public String retirerJustif(HttpServletRequest request, Model model, @PathVariable("idFrais") Long idFrais,
                                @PathVariable("idJustif") Long idJustif) throws Exception {
        init(request, model);

        Justif justif = justifService.get(idJustif);

        fraisService.supprJustificatifToFrais(idFrais, justif);
        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_EDIT_FRAIS + "/" + idFrais;
    }

    /**
     * Méthode permettant de soumettre une demande d'avance de frais
     *
     * @param request the request
     * @param model   the model
     * @param idFrais ID du frais à soumettre
     * @param ra      the ra
     * @return Redirection vers la page des frais
     * @throws Exception the exception
     */
    @RequestMapping(value = "soumettreAvance/{idFrais}", method = {RequestMethod.POST, RequestMethod.GET})
    public String soumettreAvance(HttpServletRequest request, Model model, @PathVariable("idFrais") Long idFrais,
                                  RedirectAttributes ra) throws Exception {
        init(request, model);

        Etat newEtat = etatService.findUniqEntiteByProp(Etat.PROP_CODE, "SO");
        EtatAsJson etatJson = new EtatAsJson(newEtat);
        Frais frais = fraisService.get(idFrais);
        FraisAsJson fraisJson = frais.toJSon();
        fraisJson.setIdMission(frais.getLinkEvenementTimesheet().getMission().getId());
        fraisJson.setMission(frais.getLinkEvenementTimesheet().getMission().getMission());
        fraisJson.setDateEvenement(frais.getLinkEvenementTimesheet().getDate());
        fraisJson.setEtat(etatJson);
        fraisJson.setTypeAvance(frais.getTypeAvance());
        fraisJson.setMontant(frais.getMontant());

        pdfBuilderFrais.createPDFAvanceFrais(frais.getLinkEvenementTimesheet().getMission().getCollaborateur(), fraisJson);
        fraisService.soumissionDemandeAvance(idFrais);


        return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null";
    }

    /**
     * Verif presence mission string.
     *
     * @param request   the request
     * @param model     the model
     * @param pDate     the p date
     * @param idMission the id mission
     * @return -1 si le jour sélectionné et vide d'evenement, 0 si le jour sélectionné est une absence, 1 si c'est une mission
     */
    @RequestMapping(value = "verifPresenceMission/{date}/{idMission}", method = {RequestMethod.POST})
    public
    @ResponseBody
    String verifPresenceMission(HttpServletRequest request, Model model, @PathVariable("date") String pDate, @PathVariable("idMission") int idMission) {
        String retour = "-1";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long dateCherche = 0;
        try {
            dateCherche = format.parse(pDate).getTime();
        } catch (ParseException e) {
            logger.error("[check mission] parse date : {}", pDate, e);
        }

        Mission mission = missionService.get((long) idMission);

        try {
            List<LinkEvenementTimesheet> listEvent = linkEvementTimesheetService.findEventsBetweenDates(mission.getCollaborateur(), pDate, pDate);
            int nbEvent = listEvent.size();
            LinkEvenementTimesheet event;
            int i = 0;
            while (i < nbEvent) {
                event = listEvent.get(i);
                long date = event.getDate().getTime();
                if (date == dateCherche) {
                    if (event.getAbsence() != null && event.getAbsence().getNbJours() > 0.5) { //Cas d'une absence sur le jour sélectionné supérieur à une demi-journée
                        retour = "0";
                        i = nbEvent;
                    } else {    //Cas d'un jour sélectionné Mission
                        retour = "1";
                        i = nbEvent;
                    }
                }
                i++;
            }
        } catch (JsonProcessingException e) {
            logger.error("[check mission] json processing", e);
        }

        return retour;
    }


    //Fonction SBE pour verifier l'état avant de modifier ou supprimer un frais
    public String verifEtatfrais(FraisAsJson frais, RedirectAttributes pRedir) {
        //empecher la modif de frais déjà soumis, validé ou soldé
//        Collaborator collaborateur = utils.getCurrentCollaborator(collaborateurService);
        Frais ee = fraisService.get(frais.getId());
        /*List<FraisAsJson> listFr = fraisService.getFraisByCollaborateur(collaborateur);
        FraisAsJson fraisTemp=new FraisAsJson();
        for(FraisAsJson fr: listFr)
        {
            if(fr.getId().equals(frais.getId()))
            {
                fraisTemp=fr;
            }
        }*/
        frais.setEtat(ee.getEtat().toJSon());

        if (frais.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)) {
            String errorMessage = String.format("Erreur, le frais ne peut pas être modifié ou supprimé car il est déjà soumis.");
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage); //Affichage de l'erreur
            return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
        }
        if (frais.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)) {
            String errorMessage = String.format("Erreur, le frais ne peut pas être modifié ou supprimé car il est déjà validé.");
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage); //Affichage de l'erreur
            return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
        }
        if (frais.getEtat().getCode().equals(Etat.ETAT_SOLDE)) {
            String errorMessage = String.format("Erreur, le frais ne peut pas être modifié ou supprimé car il est déjà soldé.");
            pRedir.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage); //Affichage de l'erreur
            return "redirect:/" + NAV_NOTEDEFRAIS + NAV_FRAIS_NOTESDEFRAIS + "/null"; //Redirection
        }
        return null;
    }

    /**
     * Permet de binder pour convertir les objets Date pour les formulaires
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
