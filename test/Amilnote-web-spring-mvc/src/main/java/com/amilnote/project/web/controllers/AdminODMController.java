/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.pdf.PDFBuilderODM;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.metier.domain.utils.ODMForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Controller de la validation des Ordres de Mission ODM
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminODMController extends AbstractController {

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private PDFBuilderODM pdfBuilderODM;

    @Autowired
    private MailService mailService;


    /**
     * Méthode permettant de lister les odm à valider coté admin
     *
     * @param session the session
     * @param request the request
     * @param model   the model
     * @param ra      the ra
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"validationODM"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String validationOrdresDeMission(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra) throws JsonProcessingException {

        init(request, model);
        List<MissionAsJson> listMissionBR = missionService.getAllMissionEtatBR();
        for (MissionAsJson mission : listMissionBR) {
            Collaborator collab = collaboratorService.findByMail(mission.getCollaborateur());
            mission.setCollaborateur(collab.getNom() + " " + collab.getPrenom());
            File pdfODM = missionService.getFileODMId(mission.getId());
            if (pdfODM != null) {
                mission.setpdfODM(pdfODM.getPath());
            }
        }

        model.addAttribute("listMission", listMissionBR);
        return NAV_ADMIN + NAV_ADMIN_VALID_ODM;
    }

    /**
     * Méthode permettant de valider l'ordre de mission coté direction (on rempli le champ de validation dans le pdf de l'odm)
     *
     * @param session   the session
     * @param request   the request
     * @param model     the model
     * @param ra        the ra
     * @param idMission the id mission
     * @param action    the action
     * @return string
     * @throws Exception the exception
     */
    @RequestMapping(value = {"validationODM/{idMission}/{action}"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String validationOrdreDeMission(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                                           @PathVariable("idMission") Long idMission,
                                           @PathVariable("action") String action
    ) throws Exception {

        init(request, model);
        if (null != idMission) {
            String msgRetour = missionService.updateEtatMission(idMission, action); //action = refusé ou action = validé
            if (!msgRetour.equals("")) {
                if (action.equals("SO")) {
                    Mission mission = missionService.get(idMission);
                    String retourPDF = pdfBuilderODM.createPDFODM(mission.getCollaborateur(), mission.toJson());
                    if (!retourPDF.contains("Erreur")) {

                        // on envoie un mail au collab pour qu'il valide son ODM
                        mailService.sendMailAdminMissionOrder(mission);

                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Validation effectuée. Un EMail a été envoyé au collaborateur pour qu'il valide également son ordre de mission.");
                    } else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour du pdf");
                        logger.error("Erreur dans la mise a jour du pdf pour mission "+ idMission +" et collaborateur "+mission.getCollaborateur());
                    }
                }
            } else {
                ra.addFlashAttribute("messageErreur", properties.get("error.actionInconnue", action));

            }
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_VALID_ODM;
    }

    /**
     * Retourne le fichier PDF souhaité
     *
     * @param idMission ID de la mission à afficher
     * @param request   the request
     * @param response  the response
     * @param model     the model
     * @param session   the session
     * @param ra        the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationODM/{idMission}/download", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void visualisationOrdreDeMission(@PathVariable("idMission") Long idMission, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                                     RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idMission) {

            //--- Récupération du fichier PDF
            File lFile = missionService.getFileODMId(idMission);

            //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
            httpResponseConstruct("pdf", response, lFile);
        }
    }

    /**
     * Validations multiples odm string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @param action  the action
     * @param ra      the ra
     * @param odmForm the odm form
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsMultiplesODM/{action}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validationsMultiplesODM(
        HttpServletRequest request,
        Model model,
        HttpSession session,
        @PathVariable("action") String action,
        RedirectAttributes ra,
        @ModelAttribute("ODMForm") ODMForm odmForm

    ) throws Exception {

        String msgRetour = "";
        if (odmForm.getListMissions() != null) {
            for (MissionAsJson tmpMissionAsJson : odmForm.getListMissions()) {
                msgRetour = missionService.updateEtatMission(tmpMissionAsJson.getId(), action);

                if (!msgRetour.equals("") && action.equals("SO")) {
                    Mission mission = missionService.get(tmpMissionAsJson.getId());
                    String retourPDF = pdfBuilderODM.createPDFODM(mission.getCollaborateur(), mission.toJson());
                    if (!retourPDF.contains("Erreur")) {
                        String[] paramsForMsgProperties = {
                            mission.getMission()
                        };
                        // on envoie un mail au collab pour qu'il valide son ODM
                        mailService.sendMailAdminMissionOrder(mission);

                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Validation efectuée avec succès. Un Email a été envoyé aux collaborateurs pour qu'ils valident leur ordre de mission");
                    } else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour du pdf");
                    }
                }
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément sélectionné");
        }
        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_VALID_ODM;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

}
