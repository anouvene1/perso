/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.utils;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.utils.Pair;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe pour toutes les méthodes communes aux controllers de web-spring-mvc
 *
 * @author LSouai
 */
@Component
public class AmiltoneUtils {

    /**
     * dd/MM/yyyy
     */
    public SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    private static final Logger logger = LogManager.getLogger(AmiltoneUtils.class);

    private CollaboratorService collaboratorService;

    @Autowired
    public AmiltoneUtils(CollaboratorService collaboratorService) {
        this.collaboratorService = collaboratorService;
    }

    /**
     * Méthode permettant de générer une chaine de caractères aléatoire.
     *
     * @param length la taille de la chaine voulu
     * @return String la chaine de caractères aléatoire
     */
    public static String _generate(int length) {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder pass = new StringBuilder(length);
        for (int x = 0; x < length; x++) {
            int i = (int) (Math.random() * chars.length());
            pass.append(chars.charAt(i));
        }
        return pass.toString();
    }

    /**
     * Gets current collaborateur.
     *
     * @return the current collaborateur
     */
    public Collaborator getCurrentCollaborator() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = userDetails.getUsername();
        return collaboratorService.findByMail(mail);
    }

    /**
     * Gets current collaborateur.
     *
     * @param pCollaboratorService {@link CollaboratorService}
     * @return the current collaborateur
     * @deprecated utiliser à la place la version sans parametre {@link com.amilnote.project.web.utils.AmiltoneUtils#getCurrentCollaborator()}
     */
    @Deprecated
    public Collaborator getCurrentCollaborator(CollaboratorService pCollaboratorService) {
        return getCurrentCollaborator();
    }

    /**
     * Prepare data frais list.
     *
     * @param pNomMission            the p nom mission
     * @param pMontant               the p montant
     * @param pJustif                the p justif
     * @param mapHasForfaitTransport the map has forfait transport
     * @param pTypeFrais             the p type frais
     * @return the list
     * @deprecated ne plus utiliser cette methode qui a des parametres qu'elle n'utilise pas. utiliser plutôt {@link com.amilnote.project.web.utils.AmiltoneUtils#prepareDataFrais(java.math.BigDecimal, org.springframework.web.multipart.MultipartFile)}
     */
    @Deprecated
    public List<Object> prepareDataFrais(String pNomMission, BigDecimal pMontant,
                                         MultipartFile pJustif, Map<String, Pair<BigDecimal, Boolean>> mapHasForfaitTransport, Long pTypeFrais) {
        return prepareDataFrais(pMontant, pJustif);
    }

    /**
     * Prepare data frais list.
     *
     * @param pMontant the p montant
     * @param pJustif  the p justif
     * @return the list
     */
    public List<Object> prepareDataFrais(BigDecimal pMontant, MultipartFile pJustif) {
        List<Object> lListToPrepareFrais = new ArrayList<>();

        lListToPrepareFrais.add(pMontant);
        lListToPrepareFrais.add(pJustif);

        return lListToPrepareFrais;
    }

    /**
     * prend un fichier et retourne un type MIME, e.g. application/pdf, application/png, text/plain etc...
     * Par défaut si le type du fichier n'est pas trouvé, retourne application/octet-stream
     *
     * @param file fichier
     * @return type MIME du fichier
     */
    public static String getFileMIMEType(File file) {

        String filetype = null;
        try {
            Tika tika = new Tika();
            filetype = tika.detect(file);
            return filetype;
        } catch (IOException e) {
            // silent fail, si Tika n'arrive pas à déterminer le fichier
            // on va essayer autrement dans la suite de la fonction...
            logger.error("[tika error] get mime type", e);
        }

        String filename = file.getName();
        String extension = filename.substring(filename.lastIndexOf(".") + 1);

        if (StringUtils.equals("jpg", extension) || StringUtils.equals("jpeg", extension)) {
            return "image/jpeg";
        }

        if (StringUtils.equals("png", extension)) {
            return "image/png";
        }

        if (StringUtils.equals("pdf", extension)) {
            return "application/pdf";
        }

        return "application/octet-stream";
    }
}
