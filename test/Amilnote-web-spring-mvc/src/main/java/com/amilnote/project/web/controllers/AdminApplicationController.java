/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Parametre;
import com.amilnote.project.metier.domain.entities.json.TVAAsJson;
import com.amilnote.project.metier.domain.entities.json.TypeFraisAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.services.DocumentService;
import com.amilnote.project.metier.domain.services.LinkEvenementTimesheetService;
import com.amilnote.project.metier.domain.services.ParametreService;
import com.amilnote.project.metier.domain.services.TvaService;
import com.amilnote.project.metier.domain.services.TypeFraisService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.TVAForm;
import com.amilnote.project.metier.domain.utils.TypeFraisForm;
import com.amilnote.project.metier.domain.utils.enumerations.ClientExcelEnum;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.concatenateObjectsToString;

/**
 * Controller de la gestion de l'application
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminApplicationController extends AbstractController {


    private static final String MUST_REVALIDATE = "must-revalidate, post-check=0, pre-check=0";
    private static final String REDIRECT = "redirect:/";

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;
    @Autowired
    private TvaService tvaService;
    @Autowired
    private TypeFraisService typeFraisService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private ParametreService parametreService;
    @Autowired
    private FileService fileService;
    @Autowired
    private DocumentExcelService documentExcelService;


    // ---- GESTION APPLICATION ----//

    /**
     * Gérer l'application Amilnote
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "gestionApplication", method = {RequestMethod.GET, RequestMethod.POST})
    public String gestionApplication(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        List<TVAAsJson> lListTva = tvaService.getAllTVAAsJson();
        List<TypeFraisAsJson> lListTypeFrais = typeFraisService.getAllTypeFraisAsJson();
        Parametre parametre = parametreService.findById(1);

        model.addAttribute("listTva", lListTva);
        model.addAttribute("listTypeFrais", lListTypeFrais);
        model.addAttribute("budget", parametre);

        return NAV_ADMIN + NAV_ADMIN_GESTIONAPPLICATION;
    }

    /**
     * Gérer les tableurs Excel
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "gestionTableurs", method = {RequestMethod.GET, RequestMethod.POST})
    public String gestionTableurs(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        return NAV_ADMIN + NAV_ADMIN_GESTION_TABLEURS;
    }

    /**
     * Création et restitution des tableurs excel
     *
     * @param request   the request
     * @param model     the model
     * @param session   the session
     * @param typeExcel the type excel
     * @return response entity
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "tableurExcel/{typeExcel}", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<byte[]> tableurExcel(HttpServletRequest request, Model model, HttpSession session,
                                               @PathVariable("typeExcel") String typeExcel) throws JsonProcessingException {
        init(request, model);

        //pour le cas des facturations
        String pDate = request.getParameter(Constantes.MONTH_YEAR_EXTRACT);
        Calendar pCal = Calendar.getInstance();
        if (pDate != null && !pDate.isEmpty()) {
            Integer pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
            Integer pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
            pCal.set(pYear, pMonth, 1);
        }


        String revenuesDate = request.getParameter(Constantes.MONTH_EXTRACT_REVENUES);
        Calendar revenuesCal = Calendar.getInstance();
        if (revenuesDate != null && !revenuesDate.isEmpty()) {
            Integer revenuesYear = Integer.valueOf(revenuesDate.substring(revenuesDate.length() - 4));
            Integer revenuesMonth = Integer.valueOf(revenuesDate.substring(0, 2)) - 1;
            revenuesCal.set(revenuesYear, revenuesMonth, 1);
        }

        //Pour le cas des frais mensuelle
        String fDate = request.getParameter(Constantes.MONTH_YEAR_EXTRACT2);
        Calendar fCal = Calendar.getInstance();
        Calendar fCalFin=Calendar.getInstance();
        if (fDate != null && !fDate.isEmpty()) {
            Integer fYear = Integer.valueOf(fDate.substring(fDate.length() - 4));
            Integer fMonth = Integer.valueOf(fDate.substring(0, 2)) - 1;
            fCal.set(fYear, fMonth, 1);

            Integer nbjour=fCal.getActualMaximum(Calendar.DAY_OF_MONTH);
            fCalFin.set(fYear,fMonth,nbjour);
        }
        //Pour le cas des frais annuelle
        String faDate = request.getParameter(Constantes.MONTH_YEAR_EXTRACT3);
        Calendar faCal = Calendar.getInstance();
        if (faDate != null && !faDate.isEmpty()) {
            Integer faYear = Integer.valueOf(faDate.substring(faDate.length() - 4));
            Integer faMonth = Integer.valueOf(faDate.substring(0, 2)) - 1;
            faCal.set(faYear, faMonth, 1);
        }
        //Pour le cas des abscences par mois
        String absDate = request.getParameter(Constantes.MONTH_YEAR_EXTRACT4);
        Calendar absCal = Calendar.getInstance();
        if (absDate != null && !absDate.isEmpty()) {
            Integer absYear = Integer.valueOf(absDate.substring(absDate.length() - 4));
            Integer absMonth = Integer.valueOf(absDate.substring(0, 2)) - 1;
            absCal.set(absYear, absMonth, 1);
        }
        //Pour le cas des jours de presences par mois
        String presDate = request.getParameter(Constantes.MONTH_YEAR_EXTRACT5);
        Calendar presCal = Calendar.getInstance();
        if (presDate != null && !presDate.isEmpty()) {
            Integer presYear = Integer.valueOf(presDate.substring(presDate.length() - 4));
            Integer presMonth = Integer.valueOf(presDate.substring(0, 2)) - 1;
            presCal.set(presYear, presMonth, 1);
        }

        byte[] contents = new byte[]{0};
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(FileFormatEnum.XLS.toString()));
        //Récupération de l'action (année ou mois) et de la date choisie pour extract facturation
        String action = request.getParameter("action");
        switch (typeExcel) {
            case "revenues":
                try {
                    boolean isAnnualRevenues = ANNUAL_REVENUES_EXCEL_TYPE.equals(action);
                    String excelFilePath = documentExcelService.createExcelRevenues(typeExcel, revenuesCal.getTime(), isAnnualRevenues);
                    File excelFile = new File(excelFilePath);
                    contents = fileService.readFileByPath(excelFilePath);

                    String excelFileName = concatenateObjectsToString(
                            isAnnualRevenues ? ANNUAL_REVENUES_EXCEL_TYPE : MONTHLY_REVENUES_EXCEL_TYPE,
                            UNDERSCORE,
                            isAnnualRevenues ? "" : revenuesCal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase() + UNDERSCORE,
                            revenuesCal.get(Calendar.YEAR),
                            EXTENSION_FILE_XLS
                    );

                    headers.setContentDispositionFormData(excelFileName, excelFileName);
                    headers.setCacheControl(MUST_REVALIDATE);

                    if (excelFile.exists()) {
                        fileService.deleteFile(excelFile);
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;

            case "absencesMois":
                try {
                    boolean isSousTraitant = Constantes.SOUSTRAITANT_CODE.equals(action);
                    String retour = documentExcelService.createExcelAbsenceMois(new File(typeExcel), absCal.getTime(),isSousTraitant);

                    File fic = new File(retour);

                    if (fic.exists() && fic.isFile()) {
                        Path path = fic.toPath();
                        contents = Files.readAllBytes(path);
                    }

                    String nomFichier = typeExcel + "_" + absCal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + Constantes.EXTENSION_FILE_XLS;
                    headers.setContentDispositionFormData(nomFichier, nomFichier);
                    headers.setCacheControl(MUST_REVALIDATE);


                    // supprime le fichier excel créé dans le serveur (n'est pas réutilisé, il est écrasé a chaque appel)
                    if (fic.exists()){ fileService.deleteFile(fic);}

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;

            //SBE_11/09/16_AMNOTE_170: ajout d'un case pour traiter dynamiquement les frais
            //Gestion des frais detaille ou pas
            case "frais":
                try {
                    boolean bAction = "MoisSimple".equals(action);
                    String pathFile;
                    Date dateDebut;
                    Date dateFin;
                    dateDebut=fCal.getTime();
                    dateFin=fCalFin.getTime();

                    if (!bAction) {
                        pathFile = documentExcelService.createExcelFraisMois(new File(typeExcel), dateDebut,dateFin);
                    } else {
                        pathFile = documentExcelService.createExcelFraisMoisSimple(new File(typeExcel), dateDebut,dateFin);
                    }

                    File file = new File(pathFile);

                    if (file.exists() && file.isFile()) {
                        Path path = file.toPath();
                        contents = Files.readAllBytes(path);
                    }

                    //Définition du nom de fichier en fonction de la date et du type d'extract
                    String nomFichier = typeExcel + action + "_";
                    nomFichier += fCal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    nomFichier += fCal.get(Calendar.YEAR) + Constantes.EXTENSION_FILE_XLS;
                    headers.setContentDispositionFormData(nomFichier, nomFichier);
                    headers.setCacheControl(MUST_REVALIDATE);

                    // supprime le fichier excel créé dans le serveur (n'est pas réutilisé, il est écrasé a chaque appel)
                    if (file.exists()) {fileService.deleteFile(file);}

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;

            //Gestion des frais sur l'année
            case "fraisAn":

                try {
                    boolean bAction = "Simple".equals(action);
                    String pathFile = null;
                    if (!bAction) {
                        pathFile = documentExcelService.createExcelFraisAnnuel(new File(typeExcel), faCal.get(Calendar.YEAR));
                    } else {
                        pathFile = documentExcelService.createExcelFraisAnnuelSimple(new File(typeExcel), faCal.get(Calendar.YEAR));
                    }
                    File file = new File(pathFile);
                    if (file.exists() && file.isFile()) {
                        Path path = file.toPath();
                        contents = Files.readAllBytes(path);
                    }

                    //Définition du nom de fichier en fonction de la date et du type d'extract
                    String nomFichier = typeExcel + action + "_";
                    nomFichier += faCal.get(Calendar.YEAR) + Constantes.EXTENSION_FILE_XLS;
                    headers.setContentDispositionFormData(nomFichier, nomFichier);
                    headers.setCacheControl(MUST_REVALIDATE);

                    // supprime le fichier excel créé dans le serveur (n'est pas réutilisé, il est écrasé a chaque appel)
                    if (file.exists()) {fileService.deleteFile(file);}

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;

            //SBE_11/09/16_AMNOTE_170: ajout d'un case pour traiter dynamiquement les presences
            case "presenceMois":
                try {
                    boolean isSousTraitant = "SST".equals(action);
                    String retour = documentExcelService.createExcelPresenceMois(new File(typeExcel), presCal.get(Calendar.YEAR), isSousTraitant);

                    File fic = new File(retour);

                    if (fic.exists() && fic.isFile()) {
                        Path path = fic.toPath();
                        contents = Files.readAllBytes(path);
                    }

                    String nomFichier = typeExcel + "_" + presCal.get(Calendar.YEAR) + Constantes.EXTENSION_FILE_XLS;
                    headers.setContentDispositionFormData(nomFichier, nomFichier);
                    headers.setCacheControl(MUST_REVALIDATE);

                    // supprime le fichier excel créé dans le serveur (n'est pas réutilisé, il est écrasé a chaque appel)
                    if (fic.exists()) {fileService.deleteFile(fic);}

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;
            case "clients":
                try {

                    ClientExcelEnum actionEnum = ClientExcelEnum.getClientExcelType(action);

                    String retour = documentExcelService.createExcelClients(new File(typeExcel), actionEnum);
                    File fic = new File(retour);

                    if (fic.exists() && fic.isFile()) {
                        Path path = fic.toPath();
                        contents = Files.readAllBytes(path);
                    }

                    headers.setContentDispositionFormData(typeExcel + Constantes.EXTENSION_FILE_XLS, typeExcel + Constantes.EXTENSION_FILE_XLS);
                    headers.setCacheControl(MUST_REVALIDATE);

                    // supprime le fichier excel créé dans le serveur (n'est pas réutilisé, il est écrasé a chaque appel)
                    if (fic.exists()) {fileService.deleteFile(fic);}
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
                break;
            default:
                break;
        }

        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }

    /**
     * Gérer l'application Amilnote
     *
     * @param request   the request
     * @param model     the model
     * @param session   the session
     * @param typeFrais the type frais
     * @param ra        the ra
     * @return Redirection vers la même page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "saveTVAForTypeFrais",
        method = {RequestMethod.GET, RequestMethod.POST})
    public String saveTVAForTypeFrais(HttpServletRequest request, Model model, HttpSession session, TypeFraisForm typeFrais, RedirectAttributes ra) throws JsonProcessingException {
        init(request, model);

        typeFraisService.updateAllTVAForFrais(typeFrais);

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTIONAPPLICATION;
    }

    /**
     * Gérer l'application Amilnote
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @param tvaForm the tva form
     * @param ra      the ra
     * @return Redirection vers la même page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "saveTVA", method = {RequestMethod.GET, RequestMethod.POST})
    public String saveTVA(HttpServletRequest request, Model model, HttpSession session, TVAForm tvaForm, RedirectAttributes ra) throws JsonProcessingException {
        init(request, model);

        tvaService.updateAllTVA(tvaForm);

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTIONAPPLICATION;
    }

    /**
     * Gérer l'application Amilnote
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @param ra      the ra
     * @return Redirection vers la même page
     * @throws IOException the io exception
     */
    @RequestMapping(value = "saveBudget", method = {RequestMethod.GET, RequestMethod.POST})
    public String saveBudget(HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra) throws IOException {
        init(request, model);
        String valeurParam = request.getParameter("budget");
        int idParam = Integer.parseInt(request.getParameter("id"));
        Parametre parametre = parametreService.findById(idParam);
        parametre.setValeur(valeurParam);
        parametreService.createOrUpdateParametre(parametre.toJson());

        return REDIRECT + NAV_ADMIN + NAV_ADMIN_GESTIONAPPLICATION;
    }

    /**
     * Retourne tous les evenements avec l'id absence passé dans l'url
     *
     * @param request   the request
     * @param model     the model
     * @param session   the session
     * @param idAbsence the id absence
     * @return String Les evenenements "as json" parsés en String
     * @throws Exception the exception
     */
    @RequestMapping(value = "getListEventsWithAbsenceId/{idAbsence}", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getListEventsWithAbsenceId(HttpServletRequest request, Model model, HttpSession session,
                                             @PathVariable("idAbsence") Long idAbsence) throws Exception {
        if (null == idAbsence) {
            throw new Exception("idAbsence null");
        }
        return linkEvenementTimesheetService.getListEventWithAbsenceIdAsJson(idAbsence);
    }

    /**
     * Permet de binder pour convertir les objets Date pour les formulaires
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


}
