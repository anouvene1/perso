/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.LinkEvenementTimesheetService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Gère toutes les Url /.../event/...
 */
@Controller
@RequestMapping(value = {"**/Event/**"})
public class EventController extends AbstractController {

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private AmiltoneUtils utils;

    /**
     * Gets events.
     *
     * @param start the start
     * @param end   the end
     * @return the events
     * @throws JsonProcessingException the json processing exception
     */
    @PostMapping(value = {"/getEvents/{start}/{end}"}, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getEvents(@PathVariable String start, @PathVariable String end) throws JsonProcessingException {

        String lLinkEvenementTimesheet = linkEvenementTimesheetService.findEventsBetweenDatesAsJson(utils.getCurrentCollaborator(collaboratorService), start, end);
        return lLinkEvenementTimesheet;
    }

    /**
     * Gets holidays.
     *
     * @param start the start
     * @param end   the end
     * @return the holidays
     */
    @RequestMapping(value = {"/getHolidays/{start}/{end}"}, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getHolidays(@PathVariable String start, @PathVariable String end) {

        String lListHolidays = linkEvenementTimesheetService.getHolidaysJson(start, end);
        return lListHolidays;
    }

    /**
     * Update event string.
     *
     * @param pEvents the p events
     * @return the string
     * @throws IOException the io exception
     */
    @RequestMapping(value = {"Mission/update"}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String updateEvent(@RequestBody String pEvents) throws IOException {

        String lEtatModif = linkEvenementTimesheetService.updateMissionOnLinkEvenementTimesheet(pEvents, utils.getCurrentCollaborator());
        if (lEtatModif.isEmpty()) {
            return "Modification réussie";
        } else {
            return lEtatModif;
        }

    }

    /**
     * Update absence event string.
     *
     * @param pEvents the p events
     * @return the string
     * @throws IOException the io exception
     */
    @RequestMapping(value = {"Absence/update"},
        method = RequestMethod.POST,
        consumes = "application/json",
        produces = "application/json; charset=utf-8")

    @ResponseBody
    public String updateAbsenceEvent(@RequestBody String pEvents) throws IOException {

        String lEtatModif = linkEvenementTimesheetService.updateAbsenceOnLinkEvenementTimesheet(pEvents, utils.getCurrentCollaborator(collaboratorService));
        if (lEtatModif.isEmpty()) {
            return "Modification réussie";
        } else {
            return lEtatModif;
        }
    }

    /**
     * Del event string.
     *
     * @param pEvents the p events
     * @return the string
     * @throws IOException the io exception
     */
    @RequestMapping(value = {"supprime"},
        method = RequestMethod.POST,
        consumes = "application/json",
        produces = "application/json; charset=utf-8")

    @ResponseBody
    public String delEvent(@RequestBody String pEvents) throws IOException {

        String lEtatModif = linkEvenementTimesheetService.delLinkEvenementTimesheet(pEvents, utils.getCurrentCollaborator(collaboratorService));
        if (lEtatModif.isEmpty()) {
            return "Suppression réussie";
        } else {
            return lEtatModif;
        }

    }

}
