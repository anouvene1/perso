package com.amilnote.project.web.DTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Classe créée pour permettre le renvoi d'un objet Commande en Json pour la creation d'une facture de frais
 * L'objet CommandeAsJson ne permet pas d'être serialisé ( org.springframework.http.converter.HttpMessageNotWritableException )
 * Seuls les attributs utiles ont été repris, possibilité d'en ajouter d'autres selon les besoins
 * {@linkplain com.amilnote.project.web.controllers.AdminFactureController#loadCommande(HttpServletRequest, long)} ()}
 */
public class CommandeDTO {

    private Long id;
    private Date dateDebut;
    private Date dateFin;
    private String numero;

    public CommandeDTO(Long id, Date dateDebut, Date dateFin, String numero) {
        this.id = id;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.numero = numero;
    }

    public CommandeDTO() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
