/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.RapportActivitesAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.RAForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Controller de la Gestion des RA
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminRAController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(AdminRAController.class);

    private static final String REDIRECT = "redirect:/";

    @Autowired
    private PDFBuilderRA pdfBuilderRA;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private MailService mailService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private FactureService factureService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private FileService fileService;

    private static final String PDF_EXTENSION = "pdf";

    private static final String MULTIPLE_RA_FILE_NAME = "RA_Multiple";

    private static final String FIRSTPAGE_CHOICE = "firstpage";
    private static final String ALLPAGES_CHOICE = "allpages";

    // ---- GESTION DES RA ----//

    /**
     * Consultation de l'ensemble des rapports d'activités quelques soit leurs état et leurs date
     *
     * @param pFiltre    the p filtre
     * @param FiltreDate the date filtre
     * @param request    the request
     * @param model      the model
     * @param session    the session
     * @return Page de consultation des RA
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "consultationRA/{pFiltre}/{FiltreDate}", method = {RequestMethod.GET, RequestMethod.POST})
    public String consultationRA(@PathVariable("pFiltre") String pFiltre, @PathVariable("FiltreDate") String FiltreDate, HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        //Récupération du filtre sur le mois
        String mois = request.getParameter("mois");
        String annee = request.getParameter("annee");
        String pDate = FiltreDate;
        Calendar pCal = Calendar.getInstance();
        pCal.setTime(new Date());
        Integer pYear;
        Integer pMonth;
        if (!pDate.equals("null") && !FiltreDate.equals("TO")) {
            pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
            pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
        } else {
            //si aucun filtre on l'initialise avec le mois et l'année courant
            pYear = pCal.get(Calendar.YEAR);
            pMonth = pCal.get(Calendar.MONTH);
        }
        mois = String.valueOf(pMonth);
        annee = String.valueOf(pYear);

        // Récupération des RA filtrés sur leur Etat
        List<RapportActivites> listRA = rapportActivitesService.getAllRAByFiltre(pFiltre);


        //Liste temporaire pour stocker les RA si il y a un filtre sur le mois
        List<RapportActivites> listTemp = new ArrayList<RapportActivites>();


        for (RapportActivites ra : listRA) {
            //On set a null tous les pdf introuvable pour le bouton visualisaation RA

            if (!rapportActivitesService.raFileExists(ra.getId())) {
                ra.setPdfRapportActivite(null);
            }
            //S'il y a un filtre sur le mois on ne récupère que les RA qui datent de ce mois
            if (FiltreDate.equals("TO")) listTemp.add(ra); //Permets de voir tout les RA existant
            else if ("null".equals(pFiltre) || ra.getEtat().getCode().equals(pFiltre)) {
                if (moisRAIsOk(mois, ra) && anneeRAIsOk(annee, ra)) {
                    listTemp.add(ra);
                }
            }
        }
        listRA = listTemp;

        // [AMA][AMNT-579] On sépare les RA de collaborateurs et des sous-traitants
        List<RapportActivites> listRAcollab = new ArrayList<>();  // la liste des RA des collaborateurs
        List<RapportActivites> listRAstt = new ArrayList<>();    // la liste des RA des sous-traitants

        for (RapportActivites ra : listRA) {
            if (StatutCollaborateur.STATUT_SOUS_TRAITANT.equals(ra.getCollaborateur().getStatut().getCode())) {
                listRAstt.add(ra);
            } else {
                listRAcollab.add(ra);
            }
        }


        try {
            model.addAttribute("listRA", listRA); // tous les RA
            model.addAttribute("listRAcollab", listRAcollab); //RA des collaborateurs
            model.addAttribute("listRAstt", listRAstt); // RA des Sous-traitants

            model.addAttribute("repertoireRA", Parametrage.getContext("dossierPDFRA"));
        } catch (NamingException e) {
            logger.error("[RA consultation] context parameter naming error", e);
        }

        return NAV_ADMIN + NAV_ADMIN_CONSULTATIONRA;
    }


    /**
     * Mois ra is ok boolean.
     *
     * @param mois the mois
     * @param ra   the ra
     * @return the boolean
     */
    public boolean moisRAIsOk(String mois, RapportActivites ra) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ra.getMoisRapport());
        String moisRA = Integer.toString(calendar.get(Calendar.MONTH));

        return moisRA.equals(mois) || "-1".equals(mois);
    }

    /**
     * Annee ra is ok boolean.
     *
     * @param annee the annee
     * @param ra    the ra
     * @return the boolean
     */
    public boolean anneeRAIsOk(String annee, RapportActivites ra) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ra.getMoisRapport());
        String anneeRA = Integer.toString(calendar.get(Calendar.YEAR));

        return anneeRA.equals(annee) || "-1".equals(annee);
    }

    /**
     * Consultation de la liste des soumissions des RA du mois
     *
     * @param pFiltre the p filtre
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return Page de liste des soumissions des RA
     */
    @RequestMapping(value = "listeSoumissionRA/{pFiltre}", method = {RequestMethod.GET, RequestMethod.POST})
    public String listeSoumissionRA(@PathVariable("pFiltre") String pFiltre, HttpServletRequest request, Model model, HttpSession session) {
        init(request, model);

        List<Collaborator> listCollab = collaboratorService.findAllOrderByNomAscWithoutADMIN();
        List<RapportActivites> listeSoumisNonSoumis = new ArrayList<RapportActivites>();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-01");
        Date date = new Date();
        String dateString = formater.format(date);
        String dateS = "";
        Etat etat = new Etat();
        etat.setCode("BR");
        etat.setEtat("Brouillon");

        //On boucle sur tous les collaborateurs
        for (Collaborator collab : listCollab) {
            List<RapportActivites> listRA = collab.getRapportActivites();
            boolean collabEnr = false;
            int i = 0;
            collab.setNom(collab.getNom().trim());
            collab.setPrenom(collab.getPrenom().trim());
            collab.setMail(collab.getMail().trim());
            int nbRA = listRA.size();
            //Pour chaque collaborateur, on boucle sur tous les RA
            //On récupère le RA du mois si il existe, sinon on enregistre un RA à l'Etat "brouillon"
            while (i < nbRA && !collabEnr) {
                RapportActivites ra = new RapportActivites();
                ra = listRA.get(i);
                dateS = formater.format(ra.getMoisRapport());
                if (dateS.equals(dateString)) {
                    if (ra.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)) {
                        listeSoumisNonSoumis.add(ra);
                        collabEnr = true;
                    } else if (!ra.getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE)) {    //Si le RA du mois est à l'état Validé ou Refusé on  ne l'affiche pas (on n'affiche que brouillon/soumis)
                        collabEnr = true;
                    }
                }
                i++;
            }

            if (!"SO".equals(pFiltre)) {
                if (!collabEnr && nbRA != 0) {
                    i--;
                    RapportActivites ra = new RapportActivites();
                    ra = listRA.get(i);
                    ra.setEtat(etat);
                    listeSoumisNonSoumis.add(ra);
                } else if (!collabEnr && nbRA == 0) {
                    RapportActivites ra = new RapportActivites();
                    ra.setCollaborateur(collab);
                    ra.setCommentaire("");
                    ra.setDateSoumission(date);
                    ra.setDateValidation(date);
                    ra.setEtat(etat);
                    ra.setId(new Long("1"));
                    ra.setLinkEvenementTimesheets(null);
                    ra.setMoisRapport(date);
                    ra.setPdfRapportActivite("");
                    listeSoumisNonSoumis.add(ra);
                }
            }
        }

        if ("BR".equals(pFiltre)) {
            List<RapportActivites> listTemp = new ArrayList<RapportActivites>();
            for (RapportActivites ra : listeSoumisNonSoumis) {
                if (pFiltre.equals(ra.getEtat().getCode())) {
                    listTemp.add(ra);
                }
            }
            listeSoumisNonSoumis = listTemp;
        }
        model.addAttribute("listeSoumis", listeSoumisNonSoumis);

        return NAV_ADMIN + NAV_ADMIN_LISTESOUMISRA;
    }

    /**
     * Envoi mail rappel ra string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return the string
     * @throws NamingException the naming exception
     */
    @RequestMapping(value = "listeSoumissionRA/Rappel", method = {RequestMethod.POST})
    public String envoiMailRappelRA(HttpServletRequest request, Model model, HttpSession session) throws NamingException {

        mailService.envoiMailRappelRA();
        return NAV_ADMIN + NAV_ADMIN_LISTESOUMISRA;
    }

    /**
     * Méthode permettant de valider, de refuser ou d'annuler une validation RA en fonction de son ID
     *
     * @param etatRetour  Ordre de validation ou de suppression
     * @param session     the session
     * @param request     the request
     * @param model       the model
     * @param ra          the ra
     * @param commentaire the commentaire
     * @return Redirection vers la même page
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsRA/{etatRetour}", method = {RequestMethod.GET, RequestMethod.POST})
    public String validOrRefuseRA(
            @PathVariable("etatRetour") String etatRetour,
            HttpSession session,
            HttpServletRequest request,
            Model model,
            RedirectAttributes ra,
            String commentaire)
            throws Exception {

        init(request, model);

        Etat etatSoumis = etatService.getEtatByCode(Etat.ETAT_SOUMIS_CODE);
        Etat etatValide = etatService.getEtatByCode(Etat.ETAT_VALIDE_CODE);

        //[JNA][AMNOTE 169] Récupération des infos en POST
        String pDate = request.getParameter("filtreDate");
        String filtre = request.getParameter("filtreRA");
        Long id = Long.valueOf(request.getParameter("idRA"));
        String dateExist = "null";
        String dateTemp = request.getParameter("dateFiltre");
        if (dateTemp != null && !dateTemp.equals("")) {
            dateExist = dateTemp;
        }


        if (!StringUtils.isBlank(commentaire)) {
            commentaire = new String(commentaire.getBytes(), Charset.forName("UTF-8"));
        }

        if (null != id) {


            CollaboratorAsJson collaboratorAsJson = collaboratorService.findByIdRA(id);

            RapportActivitesAsJson rapportActivitesAsJson = rapportActivitesService.getRAbyId(id).toJson();

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");

            // Paramètres donnés à la propriété messageSources
            String[] paramsForMsgProperties = {
                    dateFormat.format(rapportActivitesAsJson.getMoisRapport()),
                    (!StringUtils.isBlank(commentaire) ? commentaire : "Aucun commentaire"),
            };

            if (etatRetour.equals(NAME_NAV_ADMIN_ANNULE)) {
                rapportActivitesService.changeEtatRA(id, Etat.ETAT_ANNULE, commentaire);

                mailService.sendMailActivityReportCanceled(dateFormat.format(rapportActivitesAsJson.getMoisRapport()), commentaire, collaboratorAsJson);

                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Dévalidation du rapport d'activités effectué");

                // [CLO AMNOTE-308] REFONTE - mise à jour facture correspondante
                Collaborator collaborateur = collaboratorService.findByMail(collaboratorAsJson.getMail());
                DateTime dateRAVoulue = new DateTime(rapportActivitesAsJson.getMoisRapport());
                List<Mission> missions = missionService.findMissionsClientesByMonthForCollaborator(collaborateur, dateRAVoulue);

                ArrayList<Mission> missionsDesFacturesSoumises = new ArrayList<>();

                for (Mission mission : missions) {
                    Facture facture = factureService.findByMissionByMonth(mission, dateRAVoulue);
                    if (facture != null) {
                        if (facture.getEtat() != etatSoumis && facture.getEtat() != etatValide) {
                            facture.setRaValide(-1);
                            factureService.createOrUpdateFacture(facture);
                        } else {
                            missionsDesFacturesSoumises.add(facture.getMission());
                        }
                    }
                }
                // fin refonte

                if (!missionsDesFacturesSoumises.isEmpty()) {
                    String errorMessage = "Le rapport d'activité mais la (les) facture(s) pour la (les) mission(s) ";
                    for (Mission mission : missionsDesFacturesSoumises) {
                        errorMessage += "\"" + mission.getMission() + "\", ";
                    }
                    errorMessage += " a (ont) déjà été soumise(s) : elle(s) ne sera (seront) donc pas modifiée(s).";
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, errorMessage);
                }

            } else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Action non prise en charge (" + etatRetour + ")");
            }
        }

        if (pDate.equals("")) {
            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CONSULTATIONRA + "/" + filtre + "/" + dateExist;
        } else {
            return "redirect:/" + NAV_ADMIN + NAV_ADMIN_CONSULTATIONRA + "/" + filtre + "/" + dateExist;
        }

    }

    /**
     * Retourne le fichier PDF correspondant au RA souhaité
     *
     * @param idRA     ID du RA à visualiser
     * @param request  the request
     * @param response the response
     * @param model    the model
     * @param session  the session
     * @param ra       the ra
     * @throws Exception the exception
     */
    @RequestMapping(value = "validationsRA/{idRA}/download", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    void visualisationRA(@PathVariable("idRA") Long idRA, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                         RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idRA) {

            //--- Récupération du fichier PDF lié au RA
            File lFileRA = rapportActivitesService.getFileRAbyId(idRA);
            if (lFileRA != null) {
                String nomFichier = lFileRA.getPath();
                RapportActivites RA = rapportActivitesService.getRAbyId(idRA);
                if (RA.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE) || RA.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)) {
                    nomFichier = nomFichier.replace("_SOCIAL", "");
                }

                lFileRA = new File(nomFichier);

                //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
                httpResponseConstruct(PDF_EXTENSION, response, lFileRA);

                if (fileService.fileWasZipped(nomFichier)) {
                    fileService.deleteFile(lFileRA);
                }
            }
        }
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(value = "creationPdfMultiplesRA/{page}", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<byte[]> creationPdfMultiplesRA(
            HttpServletRequest request,
            Model model,
            HttpSession session,
            RedirectAttributes ra,
            @PathVariable String page,
            @ModelAttribute("RAForm") RAForm raForm)
            throws Exception {

        byte[] contents = new byte[]{0};
        init(model);

        String msgRetour = "";
        Date today = new Date();
        String nomFichier = "";
        SimpleDateFormat full = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm);
        String pathFile = "";

        List<RapportActivites> liste = new ArrayList<>();
        if (raForm.getListRA() != null) {

            for (RapportActivitesAsJson rapportActivitesAsJson : raForm.getListRA()) {
                RapportActivites rapportActivites = rapportActivitesService.getRAbyId(rapportActivitesAsJson.getId());
                liste.add(rapportActivites);
            }
            liste.sort(new Comparator<RapportActivites>() {
                @Override
                public int compare(RapportActivites ra1, RapportActivites ra2) {
                    String nom1 = ra1.getCollaborateur().getNom();
                    String nom2 = ra2.getCollaborateur().getNom();

                    if (nom1.equals(nom2)) {
                        return ra1.getMoisRapport().compareTo(ra2.getMoisRapport());
                    }

                    return nom1.compareToIgnoreCase(nom2);
                }
            });

            Collaborator collaborateur = liste.get(0).getCollaborateur();

            // la PDF du RA avec frais doit avoir le même nom que celui utilisé pour la visualisation du RA (meme contenu)
            SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_MMYYYY);
            String dateRA = dateFormat.format(liste.get(0).getMoisRapport());


            // si on créé un PDF avec plusieurs RA on donne un nom à part pour mieux l'identifier
            if (liste != null && liste.size() >= 2) {
                switch (page) {
                    case (ALLPAGES_CHOICE):
                        nomFichier = MULTIPLE_RA_FILE_NAME + Constantes.EXTENSION_FILE_PDF;
                        pathFile = pdfBuilderRA.createSeveralPdfRA(liste, nomFichier, Constantes.RA_TYPE_PDF);
                        break;
                    case (FIRSTPAGE_CHOICE):
                        nomFichier = MULTIPLE_RA_FILE_NAME + Constantes.EXTENSION_FILE_PDF;
                        pathFile = pdfBuilderRA.createSeveralPdfRA(liste, nomFichier, Constantes.RA_SANS_FRAIS_TYPE_PDF);
                        break;
                }
            } else {
                switch (page) {
                    case (FIRSTPAGE_CHOICE):
                        nomFichier = MULTIPLE_RA_FILE_NAME + "-" + full.format(today) + Constantes.EXTENSION_FILE_PDF;
                        pathFile = pdfBuilderRA.createSeveralPdfRA(liste, nomFichier, Constantes.RA_SANS_FRAIS_TYPE_PDF);
                        break;
                    case (ALLPAGES_CHOICE):
                        nomFichier = MULTIPLE_RA_FILE_NAME + "-" + collaborateur.getId() + "-" + collaborateur.getNom() + "_" + dateRA + Constantes.EXTENSION_FILE_PDF;
                        pathFile = pdfBuilderRA.createSeveralPdfRA(liste, nomFichier, Constantes.RA_TYPE_PDF);
                        break;
                    default:
                }
            }

            File file = new File(pathFile);

            if (file.exists() && file.isFile()) {
                Path path = file.toPath();
                contents = Files.readAllBytes(path);

                fileService.deleteFile(file);
            }
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, msgRetour);
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément séléctionné");
        }

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate");
        headers.setContentDispositionFormData(nomFichier, nomFichier);


        ResponseEntity<byte[]> reponse = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);

        return reponse;

    }

}
