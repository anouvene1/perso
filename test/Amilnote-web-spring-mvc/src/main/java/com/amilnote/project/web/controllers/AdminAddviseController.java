/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.AddviseSession;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import com.amilnote.project.metier.domain.services.AddviseService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.itextpdf.text.DocumentException;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

/**
 * ALV AMNOTE-228 Controller de la gestion des modules Addvise
 */
@Controller
@RequestMapping("/Administration")
public class AdminAddviseController extends AbstractController {

    @Autowired
    private AddviseService addviseService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private AmiltoneUtils utils;

    /**
     * Initialise les données et l'affichage de la vue par défaut du programme Addvise
     *
     * @param request Request
     * @param model   Model
     * @return la route vers la vue
     * @throws ParseException the parse exception
     */
    @RequestMapping(value = "addvise", method = {RequestMethod.GET})
    public String moduleAddvise(HttpServletRequest request, Model model) throws ParseException {
        init(request, model);

        List<Collaborator> collaborateursAddvise = addviseService.findAddviseCollaborators();
        request.setAttribute("aideCreationStats", addviseService.getStatsTable(collaborateursAddvise, null, null)); //avant tris/filtres

        String trier = request.getParameter("trier");
        String cacher = request.getParameter("cacher");
        String chercher = request.getParameter("chercher");
        String filtrer = request.getParameter("filtrer");
        collaborateursAddvise = addviseService.sortAndFilterCollaborators(collaborateursAddvise,
                                                                           trier, cacher, chercher, filtrer);

        request.setAttribute("listeCollaborateurs", collaborateursAddvise);
        request.setAttribute("listeModules", addviseService.findAddviseAllModules());
        request.setAttribute("listeFormateurs", addviseService.findAddviseAllFormateurs());
        request.setAttribute("listeSessionsModifiables", addviseService.findAddviseSessionsModifiables());

        return NAV_ADMIN + NAV_ADMIN_ADDVISE;
    }

    /**
     * Initialise les données et l'affichage de la vue de l'historique du programme Addvise
     *
     * @param request Request
     * @param model   Model
     * @return la route vers la vue
     * @throws ParseException the parse exception
     */
    @RequestMapping(value = "addvise-hist", method = {RequestMethod.GET})
    public String moduleAddviseHistorique(HttpServletRequest request, Model model) throws ParseException {
        init(request, model);

        List<Collaborator> collaborateursAddvise = addviseService.findAddviseCollaborators();

        String trier = request.getParameter("trier");
        String chercher = request.getParameter("chercher");
        String filtrer = request.getParameter("filtrer");
        collaborateursAddvise = addviseService.sortAndFilterCollaborators(collaborateursAddvise,
                                                                           trier, "false", chercher, filtrer);

        String annulees = request.getParameter("annulees");
        String dateDebut = request.getParameter("date-debut");
        String dateFin = request.getParameter("date-fin");

        List<AddviseSession> sessionsPassees = addviseService.findSessionsPassees(
            Boolean.parseBoolean(annulees), dateDebut, dateFin);

        request.setAttribute("listeCollaborateurs", collaborateursAddvise);
        request.setAttribute("listeModules", addviseService.findAddviseAllModules());
        request.setAttribute("listeSessionsPassees", sessionsPassees);
        request.setAttribute("stats", addviseService.getStatsHist(sessionsPassees));

        return NAV_ADMIN + NAV_ADMIN_ADDVISE_HIST;
    }

    /**
     * Sauvegarde les invitations aux sessions à la soumission du tableau de saisie
     *
     * @param request            Request
     * @param model              Model
     * @param redirectAttributes Redirect Attributes
     * @return la route vers la redirection
     */
    @RequestMapping(value = "addvise/saveAllInvitations", method = {RequestMethod.POST})
    public String saveAllInvitations(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
        init(request, model);

        Collaborator currentUser = utils.getCurrentCollaborator(collaboratorService);
        String allInvitationsStr = request.getParameter("allInvitations");
        addviseService.saveAllInvitations(allInvitationsStr, request.getRequestURL().toString(), currentUser);

        redirectAttributes.addFlashAttribute("succes", "Votre sauvegarde a été effectuée avec succès. Un mail a été envoyé aux collaborateurs concernés par les changements effectués pour les sessions futures.");

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE;
    }

    /**
     * Confirme la session si elle ne l'était pas (toujours éditable), sinon on la valide (historique)
     *
     * @param redirectAttributes Redirect Attributes
     * @param request            Request
     * @param model              Model
     * @return la route vers la redirection
     */
    @RequestMapping(value = "addvise/validateSession", method = {RequestMethod.POST})
    public String validateSession(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
        init(request, model);

        String allInvitationsStr = request.getParameter("allInvitations");
        int idSession = Integer.parseInt(request.getParameter("idSession"));
        Collaborator currentUser = utils.getCurrentCollaborator(collaboratorService);
        AddviseSession addviseSession = addviseService.validationSession(idSession, allInvitationsStr, request.getRequestURL().toString(), currentUser);

        if (addviseSession.getEtat() == AddviseSession.EtatSession.CONFIRMEE) {
            redirectAttributes.addFlashAttribute("succes", "Votre sauvegarde a été effectuée avec succès. Un mail a été envoyé aux personnes concernées.");
        } else {
            redirectAttributes.addFlashAttribute("succes", "Votre sauvegarde a été effectuée avec succès. La session a rejoint l'historique.");
        }

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE;
    }

    /**
     * "Supprime" (reste stockée) la session en cours en cas d'annulation (disponible dans l'historique)
     *
     * @param redirectAttributes Redirect Attributes
     * @param request            Request
     * @param model              Model
     * @return la route vers la redirection
     */
    @RequestMapping(value = "addvise/removeSession", method = {RequestMethod.POST})
    public String removeSession(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
        init(request, model);

        String allInvitationsStr = request.getParameter("allInvitations");
        int idSession = Integer.parseInt(request.getParameter("idSession"));
        Collaborator currentUser = utils.getCurrentCollaborator(collaboratorService);
        addviseService.removeSession(idSession, allInvitationsStr, request.getRequestURL().toString(), currentUser);

        redirectAttributes.addFlashAttribute("succes", "La session a été supprimée avec succès. Un mail a été envoyé aux personnes concernées.");

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE;
    }

    @RequestMapping(value = "addvise/addNewSession", method = {RequestMethod.POST})
    public String addNewSession(HttpServletRequest request, Model model,
                                RedirectAttributes redirectAttributes) throws ParseException {
        init(request, model);
        boolean errorDate = false;
        boolean sessionAdded = false;

        long idModule = Long.parseLong(request.getParameter("module"));
        String date = request.getParameter("date");
        int idFormateur = Integer.parseInt(request.getParameter("formateur"));
        ModuleAddvise previousModule = null;

        try {
            addviseService.addNewSession(idModule, date, idFormateur);
            previousModule = addviseService.getPreviousModule(idModule);
            sessionAdded = true;
        } catch (DataIntegrityViolationException e) {
            errorDate = true;
        }

        long previousModuleId;
        if (previousModule == null) {
            previousModuleId = 0;
        } else {
            previousModuleId = previousModule.getId();
        }

        redirectAttributes.addFlashAttribute("errorDate", errorDate);
        redirectAttributes.addFlashAttribute("sessionAdded", sessionAdded);

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE + "?filtrer=" + previousModuleId;
    }

    /**
     * "Supprime" (reste stockée) la session en cours en cas d'annulation (disponible dans l'historique)
     *
     * @param redirectAttributes Redirect Attributes
     * @param request            Request
     * @param model              Model
     * @return la route vers la redirection
     */
    @RequestMapping(value = "addvise/editSession", method = {RequestMethod.POST})
    public String editSession(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
        init(request, model);
        ModuleAddvise previousModule = null;
        boolean errorDate = false;
        long idModule = Long.parseLong(request.getParameter("module"));
        String date = request.getParameter("date");
        int idFormateur = Integer.parseInt(request.getParameter("formateur"));
        String allInvitationsStr = request.getParameter("allInvitations");
        Collaborator currentUser = utils.getCurrentCollaborator(collaboratorService);
        int idSession = Integer.parseInt(request.getParameter("idSession"));

        try {
            addviseService.editSession(idSession, idModule, date, idFormateur, allInvitationsStr, request.getRequestURL().toString(), currentUser);
            previousModule = addviseService.getPreviousModule(idModule);
            redirectAttributes.addFlashAttribute("succes", "La session a été modifiée avec succès. Un mail a été envoyé aux personnes concernées.");
        } catch (ParseException e) {
            errorDate = true;
        }

        long previousModuleId;
        if (previousModule == null) {
            previousModuleId = 0;
        } else {
            previousModuleId = previousModule.getId();
        }

        redirectAttributes.addFlashAttribute("errorDate", errorDate);

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE + "?filtrer=" + previousModuleId;
    }

    /**
     * Permettre à nouveau une session d'être éditée (en cours si précédemment supprimée, confirmée si validée)
     *
     * @param redirectAttributes Redirect Attributes
     * @param request            Request
     * @param model              Model
     * @return la route vers la redirection
     */
    @RequestMapping(value = "addvise/restoreSession", method = {RequestMethod.POST})
    public String restoreSession(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
        init(request, model);

        addviseService.restoreSession(Integer.parseInt(request.getParameter("idSession")));
        redirectAttributes.addFlashAttribute("succes", "La session a bien été restaurée dans la partie Gestion des modules Addvise.");

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_ADDVISE_HIST;
    }

    /**
     * Crée une nouvelle session à la soumission du formulaire
     *
     * @param request Request
     * @param model   Model
     * @return la route vers la redirection
     */


    /**
     * Récupère en aynchrone l'état de l'invitation correspondant à l'id collaborateur + id session en paramètres
     *
     * @param request Request
     * @param model   Model
     * @return l'état énumérable correspondant ou null
     */
    @RequestMapping(value = "addvise/getInvitationsByCollabSession", method = {RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<Long[]> getInvitationsByCollabSession(HttpServletRequest request, Model model) {
        init(request, model);

        String idsCollabSession = request.getParameter("idsCollabSession");

        List<Long[]> listeInvits = addviseService.findInvitationsEtatsByCollabSession(idsCollabSession);

        Long[] etatsLong = new Long[listeInvits.size() * 3];
        for (int i = 0; i < listeInvits.size(); ++i) {
            etatsLong[i * 3] = listeInvits.get(i)[0];
            etatsLong[i * 3 + 1] = listeInvits.get(i)[1];
            etatsLong[i * 3 + 2] = listeInvits.get(i)[2];
        }

        return new ResponseEntity<>(etatsLong, HttpStatus.OK);
    }

    /**
     * Permet la sauvegarde d'un nouveau module en retournant un statut HTTP et l'id créé
     *
     * @param request Request
     * @param model   Model
     * @return Une réponse HTTP avec statut + id créé
     */
    @RequestMapping(value = "addvise/addNewModule", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<Long> addNewModule(HttpServletRequest request, Model model) {
        init(request, model);

        String codeModule = request.getParameter("codeModule");
        String nomModule = request.getParameter("nomModule");
        try {
            Long id = addviseService.addNewModule(codeModule, nomModule);
            return new ResponseEntity<>(id, HttpStatus.OK); //id retourné
        } catch (InvalidPropertiesFormatException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST); //mauvaise syntaxe des paramètres
        } catch (HibernateException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); //erreur d'Hibernate
        }
    }

    /**
     * Permet la sauvegarde d'un nouveau formateur en retournant un statut HTTP et l'id créé
     *
     * @param request Request
     * @param model   Model
     * @return Une réponse HTTP avec statut + id créé
     */
    @RequestMapping(value = "addvise/addNewFormateur", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<Long> addNewFormateur(HttpServletRequest request, Model model) {
        init(request, model);

        String nomFormateur = request.getParameter("nomFormateur");
        String prenomFormateur = request.getParameter("prenomFormateur");
        String emailFormateur = request.getParameter("emailFormateur");
        try {
            Long id = addviseService.addNewFormateur(nomFormateur, prenomFormateur, emailFormateur);
            return new ResponseEntity<>(id, HttpStatus.OK); //id retourné
        } catch (InvalidPropertiesFormatException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST); //mauvaise syntaxe des paramètres
        } catch (HibernateException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); //erreur d'Hibernate
        }
    }

    /**
     * Permet l'impression de la feuille d'émargement (ouverture d'un PDF)
     *
     * @param request Request
     * @param model   Model
     * @return Un document PDF
     * @throws DocumentException the document exception
     * @throws NamingException   the naming exception
     * @throws IOException       the io exception
     */
    @RequestMapping(value = "addvise/printSessionToSign", method = {RequestMethod.POST})
    public ResponseEntity<byte[]> printSessionToSign(HttpServletRequest request, Model model)
        throws DocumentException, NamingException, IOException {

        init(request, model);
        int idSession = Integer.parseInt(request.getParameter("idSession"));

        byte[] fileContent = addviseService.printSessionToSign(idSession);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        return new ResponseEntity<>(fileContent, headers, HttpStatus.OK);
    }
}

