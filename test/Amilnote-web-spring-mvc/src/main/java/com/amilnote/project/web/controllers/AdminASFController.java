/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.RapportActivites;
import com.amilnote.project.metier.domain.entities.json.EtatAsJson;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderAvanceFrais;
import com.amilnote.project.metier.domain.pdf.PDFBuilderRA;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.FraisService;
import com.amilnote.project.metier.domain.services.MissionService;
import com.amilnote.project.metier.domain.services.RapportActivitesService;
import com.amilnote.project.metier.domain.utils.AvanceFraisForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.itextpdf.text.DocumentException;
import org.apache.commons.mail.EmailException;
import org.joda.time.DateTime;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Controller de la gestion des avances de frais
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminASFController extends AbstractController {

    /**
     * The Succes.
     */
    public static final String SUCCES = "succès";

    @Autowired
    private FraisService fraisService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private PDFBuilderAvanceFrais pdfBuilderFrais;

    @Autowired
    private PDFBuilderRA pdfBuilder;

    @Autowired
    private RapportActivitesService raService;

    @Autowired
    private MissionService missionService;

    // ---- GESTION DES NOTES DE FRAIS ----//

    /**
     * Méthode permettant de lister toutes les avances de frais avec un filtre sur les mois et sur l'état de l'ASF
     * Menu "consultation des avances de frais"
     *
     * @param pFiltre the p filtre
     * @param dateFilter the date filtre
     * @param request the request
     * @param model   the model
     * @param session the session
     * @throws JsonProcessingException JsonProcessingException
     * @return string
     */
    @GetMapping("noteDeFrais/{pFiltre}/{FiltreDate}")
    public String noteDeFrais(@PathVariable("pFiltre") String pFiltre, @PathVariable("FiltreDate") String dateFilter, HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        session.setAttribute(NAME_NAV_ADMIN_NOTEDEFRAIS, NAV_ADMIN_NOTEDEFRAIS);

        List<Frais> tmpListAvanceFrais = fraisService.findFraisAvanceDeFrais();
        List<FraisAsJson> tmpFraisJson = new ArrayList<>();
        List<FraisAsJson> listTemp = new ArrayList<>();

        Integer pYear;
        Integer pMonth;

        String pDate = dateFilter;
        Calendar pCal = Calendar.getInstance();

        if (!pDate.equals("null")  && !dateFilter.equals("TO")) {
            pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
            pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
        }
        else
        {
            //si aucun filtre on l'initialise avec le mois et l'année courant
            pYear = pCal.get(Calendar.YEAR);
            pMonth = pCal.get(Calendar.MONTH);
        }
        String mois = String.valueOf(pMonth);
        String annee = String.valueOf(pYear);

        for (Frais frais : tmpListAvanceFrais) {
            FraisAsJson fraisJson = frais.toJSon();
            fraisJson.setCollab(frais.getLinkEvenementTimesheet().getMission().getCollaborateur().getNom() + " " + frais.getLinkEvenementTimesheet().getMission().getCollaborateur().getPrenom());
            tmpFraisJson.add(fraisJson);
        }

        for (FraisAsJson frais : tmpFraisJson) {
            //S'il y a un filtre sur le mois on ne récupère que les RA qui datent de ce mois
            if ("null".equals(pFiltre) || frais.getEtat().getCode().equals(pFiltre)) {
                if (dateFilter.equals("TO")) listTemp.add(frais); //Permets de voir tout les RA existant
                else if (moisASFIsOk(mois, frais) && anneeASFIsOk(annee, frais)) {
                    associateASFPdfIfMonthOk(frais);
                    listTemp.add(frais);
                }
            }
        }

        tmpFraisJson = listTemp;
        model.addAttribute("listeNoteDeFrais", tmpFraisJson);
        return NAV_ADMIN + NAV_ADMIN_NOTEDEFRAIS;
    }

    private boolean moisASFIsOk(String mois, FraisAsJson frais) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(frais.getDateEvenement());
        String moisFrais = Integer.toString(calendar.get(Calendar.MONTH));

        return moisFrais.equals(mois) || "-1".equals(mois);
    }

    private boolean anneeASFIsOk(String annee, FraisAsJson frais) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(frais.getDateEvenement());
        String anneeFrais = Integer.toString(cal.get(Calendar.YEAR));

        return anneeFrais.equals(annee) || "-1".equals(annee);
    }

    private void associateASFPdfIfMonthOk(FraisAsJson frais) {
        File lFileASF = fraisService.getFileASFbyId(frais.getId());
        if (lFileASF != null) {
            frais.setPdfAvanceFrais(lFileASF.getPath());
        } else {
            frais.setPdfAvanceFrais("null");
        }
    }

    /**
     * Méthode permettant de lister les avances sur frais à l'état Soumis.
     * Menu "Validation des avances de frais"
     *
     * @param request the request
     * @param model   the model
     * @return string
     */
    @GetMapping("validNoteDeFrais")
    public String validationsNoteDeFrais(HttpServletRequest request, Model model) {
        init(request, model);
        String mois = request.getParameter("mois");
        List<Frais> listFrais = fraisService.findFraisAvanceDeFraisSO();
        List<Frais> listTemp = new ArrayList<>();
        if (mois != null && !mois.isEmpty()) {
            for (Frais frais : listFrais) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(frais.getLinkEvenementTimesheet().getDate());
                String moisFrais = Integer.toString(calendar.get(Calendar.MONTH));

                if (moisFrais.equals(mois) || "-1".equals(mois)) {
                    listTemp.add(frais);
                }
            }
            listFrais = listTemp;
        }

        model.addAttribute("listNotesFrais", listFrais);
        return NAV_ADMIN + NAV_ADMIN_VALIDATIONNOTEDEFRAIS;
    }

    /**
     * Méthode permettant la validation ou le solde d'une demande d'avance de frais
     *
     * @param id          Id du collaborateur à modifier.
     * @param action      Validation/Solde
     * @param request     the request
     * @param model       the model
     * @param ra          Permet de cacher les paramètres de l'URL de redirection
     * @param commentaire Champs commentaire
     * @param idNoteDeFraisParam idNoteDeFraisParam
     * @param response response
     * @param session session
     * @return Redirection vers une autre page
     * @throws Exception the exception
     */
    @GetMapping("validNoteDeFrais/{idNoteDeFrais}/{action}")
    public String validationNoteDeFrais(@PathVariable("idNoteDeFrais") Long id,
                                        @PathVariable("action") String action,
                                        @RequestParam(value = "idNoteFrais", required = false) Long idNoteDeFraisParam,
                                        HttpSession session,
                                        HttpServletRequest request,
                                        Model model,
                                        HttpServletResponse response,
                                        RedirectAttributes ra,
                                        String commentaire
    ) throws Exception {
        init(model);

        //si on a un idNoteFrais en parametre, on écrase la valeur qui vient du pathVariable
        if (idNoteDeFraisParam != null) {
            id = idNoteDeFraisParam;
        }

        if (commentaire != null) {
            commentaire = new String(commentaire.getBytes(), StandardCharsets.UTF_8);
        }
        else {
            commentaire = "";
        }

        Date today = new Date();

        Etat newEtat = etatService.findUniqEntiteByProp(Etat.PROP_CODE, action);
        EtatAsJson etatJson = new EtatAsJson(newEtat);
        Frais frais = fraisService.findUniqEntiteByProp("id", id);
        FraisAsJson fraisJson = frais.toJSon();
        fraisJson.setIdMission(frais.getLinkEvenementTimesheet().getMission().getId());
        fraisJson.setMission(frais.getLinkEvenementTimesheet().getMission().getMission());
        fraisJson.setDateEvenement(frais.getLinkEvenementTimesheet().getDate());
        fraisJson.setEtat(etatJson);
        fraisJson.setTypeAvance(frais.getTypeAvance());
        fraisJson.setMontant(frais.getMontant());

        if (action.equals("VA")) {
            fraisJson.setDateValidation(today);
        }
        else if (action.equals("SD")) {

            // BAB 25/05/2016 [AMILNOTE 101] : on recupere la date de solde saisie par l'utilisateur et non today
            String lDateSolde = request.getParameter("dateSolde");
            lDateSolde = lDateSolde.replace("-", "");
            lDateSolde = lDateSolde.trim();
            GregorianCalendar gCal = new GregorianCalendar();
            gCal.set(Calendar.YEAR, Integer.parseInt(lDateSolde.substring(0, 4)));
            gCal.set(Calendar.MONTH, Integer.parseInt(lDateSolde.substring(4, 6)) - 1);
            gCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(lDateSolde.substring(6, 8)));
            if (!lDateSolde.isEmpty()) {
                fraisJson.setDateSolde(gCal.getTime());
            }
            fraisJson.setDateValidation(frais.getDateValidation());
            // BAB 25/05/2016 [AMILNOTE 101] : on ajoute la mention soldé le avec la date du jour lors du solde de l'asfpe
            if (fraisJson.getCommentaire() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                fraisJson.setCommentaire(fraisJson.getCommentaire().concat(" (soldée le " + sdf.format(today).concat(")")));
            }



        }

        Mission lMission = missionService.get(fraisJson.getIdMission());
        String retour = fraisService.createOrUpdateFrais(fraisJson, lMission);

        if (retour.contains(SUCCES)) {
            if (action.equals("VA") || action.equals("SD")) {
                pdfBuilderFrais.createPDFAvanceFrais(frais.getLinkEvenementTimesheet().getMission().getCollaborateur(), fraisJson);
                calculAndPersistNewPdf(frais, fraisJson);
            }
            String retourAction = "";
            //Envoi du mail au collaborateur
            retourAction = fraisService.actionValidationASF(id, action, commentaire, today);

            if (retourAction.contains(SUCCES)) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retourAction);
            }
            else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans le changement d'état du frais ou dans l'envoi du mail");
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur lors de la mise à jour du frais");
        }

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_NOTEDEFRAIS + "/null/null";
    }


    /**
     * [AMNOTE-457]: dans le cas d'un solde d'une avance sur frais permanente, si il y a déjà un RA soumis le
     // même mois, on remplace le pdf du RA de ce mois avec le nouveau pdf.
     * @param frais
     * @param fraisJson
     * @throws MessagingException
     * @throws DocumentException
     * @throws EmailException
     * @throws SchedulerException
     * @throws IOException
     */
    private void calculAndPersistNewPdf(Frais frais, FraisAsJson fraisJson) throws MessagingException, DocumentException, EmailException, SchedulerException, IOException {

        Optional<RapportActivites> existingValideRAforMonth = raService.getExistingRAForMonth(
            frais.getLinkEvenementTimesheet().getMission().getCollaborateur(),
            new DateTime(fraisJson.getDateSolde())
        )
            .stream()
            .filter(rapportActivites -> Etat.ETAT_SOUMIS_CODE.equals(rapportActivites.getEtat().getCode()))
            .findFirst();

        if(existingValideRAforMonth.isPresent()) {
            RapportActivites rapportActivites = existingValideRAforMonth.get();

            String nomFichier = pdfBuilder.createAndSavePDF(
                rapportActivites.getCollaborateur(),
                new DateTime(rapportActivites.getMoisRapport()),
                "social"
            );
            rapportActivites.setPdfRapportActivite(nomFichier);
            raService.changePdf(rapportActivites, nomFichier);
        }
    }

    /**
     * Méthode permettant la validation/ le refus multiple de demandes d'avance de frais
     *
     * @param action          the action
     * @param ra              the ra
     * @param avanceFraisForm permet d'avoir la liste des frais sur lesquels on doit effectuer l'action
     * @return string
     * @throws Exception the exception
     */
    @GetMapping("validMultipleNoteDeFrais/{action}")
    public String validationsMultiplesNoteDeFrais(
        @PathVariable("action") String action,
        RedirectAttributes ra,
        @ModelAttribute("avanceFraisForm") AvanceFraisForm avanceFraisForm

    ) throws Exception {

        String msgRetour = "";

        Date today = new Date();
        if (avanceFraisForm.getListFrais() != null) {
            for (FraisAsJson tmpFraisAsJson : avanceFraisForm.getListFrais()) {

                Frais frais = fraisService.findUniqEntiteByProp("id", tmpFraisAsJson.getId());
                tmpFraisAsJson = new FraisAsJson(frais);
                tmpFraisAsJson.setIdMission(frais.getLinkEvenementTimesheet().getMission().getId());
                tmpFraisAsJson.setMission(frais.getLinkEvenementTimesheet().getMission().getMission());

                if (action.equals("VA")) {
                    tmpFraisAsJson.setDateValidation(today);
                }

                Mission lMission = missionService.get(tmpFraisAsJson.getIdMission());
                String retour = fraisService.createOrUpdateFrais(tmpFraisAsJson, lMission);

                if (retour.contains(SUCCES)) {
                    if (action.equals("VA")) {
                        pdfBuilderFrais.createPDFAvanceFrais(frais.getLinkEvenementTimesheet().getMission().getCollaborateur(), tmpFraisAsJson);
                    }

                    if (avanceFraisForm.getCommentaire() != null) {
                        msgRetour = fraisService.actionValidationASF(tmpFraisAsJson.getId(), action, avanceFraisForm.getCommentaire(), today);
                    }
                    else {
                        msgRetour = fraisService.actionValidationASF(tmpFraisAsJson.getId(), action, "", today);
                    }

                    if (msgRetour.contains(SUCCES)) {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, msgRetour);
                    }
                    else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, properties.get("error.actionInconnue", action));
                    }
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour du frais");
                }
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Aucun élément séléctionné");
        }

        return "redirect:/" + NAV_ADMIN + NAV_ADMIN_NOTEDEFRAIS + "/null/null";
    }

    /**
     * Retourne le fichier PDF correspondant a l'avance de frais
     *
     * @param idNoteDeFrais the id note de frais
     * @param request       the request
     * @param response      the response
     * @param model         the model
     * @param session       the session
     * @param ra            the ra
     * @throws Exception the exception
     */
    @GetMapping("validNoteDeFrais/{idNoteDeFrais}/download")
    public
    @ResponseBody
    void visualisationNoteDeFrais(@PathVariable("idNoteDeFrais") Long idNoteDeFrais, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session,
                                  RedirectAttributes ra) throws Exception {
        init(request, model);

        //--- Vérification de l'ID
        if (null != idNoteDeFrais) {

            //--- Récupération du fichier PDF lié au RA
            File lFileASF = fraisService.getFileASFbyId(idNoteDeFrais);

            if (lFileASF != null) {
                //--- Construction de l'entête de la réponse HTTP et copie dans la réponse HTTP
                httpResponseConstruct("pdf", response, lFileASF);
            }
        }
    }
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


}
