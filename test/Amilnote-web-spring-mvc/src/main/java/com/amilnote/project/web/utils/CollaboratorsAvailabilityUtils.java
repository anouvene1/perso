package com.amilnote.project.web.utils;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.CollaborateurAsJson;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.web.controllers.DisponibiliteCollaborateurController;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CollaboratorsAvailabilityUtils {
    private static final Logger logger = LogManager.getLogger(DisponibiliteCollaborateurController.class);

    private static final String SEVEN_AM = "07";
    private static final String EIGHT_AM = "08";
    private static final String ELEVEN_AM = "11";
    private static final String TWELVE_AM = "12";
    private static final String ONE_PM = "13";
    private static final String FOUR_PM = "16";
    private static final String FIVE_PM = "17";
    private static final String SIX_PM = "18";

    private static final String SUBMITTED_ABSENCE = "soumise";
    private static final String VALIDATED_ABSENCE = "valide";
    private static final String MIDNIGHT = "00:00:00.0";
    private static final String MIDNIGHT_PLUS_ONE_SECOND = "00:00:01";
    private static final String MIDDAY_MINUS_TWO_SECONDS = "11:59:58";
    private static final String MIDDAY_MINUS_ONE_SECOND = "11:59:59";
    private static final String MIDDAY = "12:00:00.0";
    private static final String MIDNIGHT_MINUS_ONE_SECOND = "23:59:59";
    private static final String MIDDAY_PLUS_ONE_SECOND = "12:00:01";

    private static final DateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);

    public String[] upperAndLowerBoundsDate() {
        SimpleDateFormat simpleDateFormatEn = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();

        String startDate = null;
        String endDate = null;

        calendar.add(Calendar.MONTH, -6);
        int firstDayOfMonth = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, firstDayOfMonth);
        startDate = formatDate(startDate, simpleDateFormatEn, calendar);

        calendar.add(Calendar.MONTH, +12);
        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
        endDate = formatDate(endDate, simpleDateFormatEn, calendar);

        return new String[]{startDate, endDate};
    }

    /**
     * Divide collaborators missions according to absences
     *
     * @param collaborators   a list of {@link CollaborateurAsJson}
     * @param missions         a list of {@link Mission}
     * @param submittedAbsences a list of {@link Absence}
     * @param validatedAbsences a list of {@link Absence}
     * @return new {@link MutableTriple}
     * @see Triple
     */
    public Triple<List<Mission>, List<Absence>, List<Absence>> cutMissionsForCollaboratorsForAbsences(
            List<CollaborateurAsJson> collaborators,
            List<Mission> missions,
            List<Absence> submittedAbsences,
            List<Absence> validatedAbsences
    ) {
        List<Mission> resultsListMission = new ArrayList<>();
        List<Absence> resultsListSubmittedAbsences = new ArrayList<>();
        List<Absence> resultsListValidatedAbsences = new ArrayList<>();

        for (CollaborateurAsJson collaborator : collaborators) {
            cutMissionsForOneCollaborator(
                    missions,
                    submittedAbsences,
                    validatedAbsences,
                    resultsListMission,
                    resultsListSubmittedAbsences,
                    resultsListValidatedAbsences,
                    collaborator
            );
        }

        return new MutableTriple<>(resultsListMission, resultsListSubmittedAbsences, resultsListValidatedAbsences);
    }

    /**
     * Divide missions for one collaborator according to absences
     *
     * @param missions                    a list of {@link Mission}
     * @param submittedAbsences            a list of {@link Absence}
     * @param validatedAbsences            a list of {@link Absence}
     * @param resultsListMission          a list of {@link Mission}
     * @param resultsListSubmittedAbsences a list of {@link Absence}
     * @param resultsListValidatedAbsences a list of {@link Absence}
     * @param collaborator               a {@link CollaborateurAsJson}
     */
    private void cutMissionsForOneCollaborator(
        List<Mission> missions,
        List<Absence> submittedAbsences,
        List<Absence> validatedAbsences,
        List<Mission> resultsListMission,
        List<Absence> resultsListSubmittedAbsences,
        List<Absence> resultsListValidatedAbsences,
        CollaborateurAsJson collaborator
    ) {
        List<Mission> missionsForCollaborator = new ArrayList<>();
        List<Absence> submittedAbsencesForCollaborator = new ArrayList<>();
        List<Absence> validatedAbsencesForCollaborator = new ArrayList<>();
        List<Mission> finalMissionList = new ArrayList<>();

        for (Mission mission : missions) {
            if (collaborator.getId().equals(mission.getCollaborateur().getId())) {
                missionsForCollaborator.add(mission);
            }
        }
        sortMissions(missionsForCollaborator);

        // Get all collaborator submitted absences
        for (Absence absence : submittedAbsences) {
            if (collaborator.getId().equals(absence.getCollaborateur().getId())) {
                submittedAbsencesForCollaborator.add(absence);
            }
        }
        sortAbsences(submittedAbsencesForCollaborator);

        // Get all collaborator validated absences
        for (Absence absence : validatedAbsences) {
            if (collaborator.getId().equals(absence.getCollaborateur().getId())) {
                validatedAbsencesForCollaborator.add(absence);
            }
        }
        sortAbsences(validatedAbsencesForCollaborator);

        Map<String, Absence> allAbsencesForCollaborator = addAbsencesToMap(submittedAbsencesForCollaborator, validatedAbsencesForCollaborator);
        addCutMissionsToList(
                missionsForCollaborator,
                submittedAbsencesForCollaborator,
                validatedAbsencesForCollaborator,
                finalMissionList,
                allAbsencesForCollaborator
        );
        resultsListSubmittedAbsences.addAll(submittedAbsencesForCollaborator);
        resultsListValidatedAbsences.addAll(validatedAbsencesForCollaborator);

        resultsListMission.addAll(finalMissionList);
        missionsForCollaborator.clear();
        finalMissionList.clear();
        submittedAbsencesForCollaborator.clear();
        validatedAbsencesForCollaborator.clear();
        allAbsencesForCollaborator.clear();
    }

    /**
     * Add divided missions by an absence
     *
     * @param missionsForCollaborator         a list of {@link Mission}
     * @param submittedAbsencesForCollaborator a list of {@link Absence}
     * @param validatedAbsencesForCollaborator a list of {@link Absence}
     * @param finalMissionList                 a list of {@link Mission}
     * @param allAbsencesForCollaborator      a {@link Map}
     */
    private void addCutMissionsToList(List<Mission> missionsForCollaborator,
                                      List<Absence> submittedAbsencesForCollaborator,
                                      List<Absence> validatedAbsencesForCollaborator,
                                      List<Mission> finalMissionList,
                                      Map<String, Absence> allAbsencesForCollaborator) {
        if (!allAbsencesForCollaborator.isEmpty()) {
            List<Map.Entry<String, Absence>> sortedAbsences = sortAllAbsencesByDateDebut(allAbsencesForCollaborator);

            List<Map.Entry<String, Absence>> withoutDuplicatedAbsencesForCollaborator = deleteDuplicatedAbsences(sortedAbsences);

            // Split missions depends on the absence dates
            cutMissionForCollaborateurOnAllAbsences(missionsForCollaborator, finalMissionList, withoutDuplicatedAbsencesForCollaborator);

            // Clean collaborators absences list before moving on the next collaborator
            submittedAbsencesForCollaborator.clear();
            validatedAbsencesForCollaborator.clear();

            for (Map.Entry<String, Absence> absenceEntry : withoutDuplicatedAbsencesForCollaborator) {
                if (absenceEntry.getKey().contains(SUBMITTED_ABSENCE)) {
                    submittedAbsencesForCollaborator.add(absenceEntry.getValue());
                } else {
                    validatedAbsencesForCollaborator.add(absenceEntry.getValue());
                }
            }
        } else {
            finalMissionList.addAll(missionsForCollaborator);
        }
    }

    /**
     * Divide all missions with absences
     *
     * @param missionsForCollaborator                  a list of {@link Mission}
     * @param finalMissionList                          a list of {@link Mission}
     * @param withoutDuplicatedAbsencesForCollaborator a {@link List}
     */
    private void cutMissionForCollaborateurOnAllAbsences(List<Mission> missionsForCollaborator,
                                                         List<Mission> finalMissionList,
                                                         List<Map.Entry<String, Absence>> withoutDuplicatedAbsencesForCollaborator) {
        for (Mission mission : missionsForCollaborator) {
            for (Map.Entry<String, Absence> absenceEntry : withoutDuplicatedAbsencesForCollaborator) {
                if (mission.getDateFin().after(absenceEntry.getValue().getDateDebut())
                        && mission.getDateDebut().before(absenceEntry.getValue().getDateDebut())
                ) {
                    // Copy mission with the start/end dates and add it to the mission list
                    Mission clonedMission = (Mission) SerializationUtils.clone(mission);
                    mission.setDateDebut(absenceEntry.getValue().getDateFin());

                    // Substract one day to the current absence start date
                    Calendar c = Calendar.getInstance();
                    c.setTime(absenceEntry.getValue().getDateDebut());

                    String startAbsenceTime = dateFormat.format(c.getTime());
                    String startAbsenceHour = startAbsenceTime.substring(11, 19);
                    if(startAbsenceHour.equals(MIDDAY_PLUS_ONE_SECOND)) {
                        c.add(Calendar.DATE, -1);
                    }

                    String startAbsenceDateToString = dateFormat.format(c.getTime());
                    try {
                        Date  startAbsenceDate = convertStringToTimestampForDateDebutAbsence(startAbsenceDateToString);
                        clonedMission.setDateFin(startAbsenceDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    finalMissionList.add(clonedMission);
                }
            }
            finalMissionList.add(mission);
        }
    }

    /**
     * Delete duplicated(submitted or validated) absences in the same interval
     * @param sortedAbsences a {@link List}
     * @return a {@link List}
     */
    private List<Map.Entry<String, Absence>> deleteDuplicatedAbsences(List<Map.Entry<String, Absence>> sortedAbsences) {
        List<Map.Entry<String, Absence>> mapList = new ArrayList<>();

        for (Map.Entry<String, Absence> entry : sortedAbsences) {
            int size = mapList.size();
            if (!mapList.isEmpty()) {
                int index = size - 1;
                String lastKey = mapList.get(index).getKey();
                Absence lastInsertedAbsence = mapList.get(index).getValue();
                manageDuplicatedAbsences(lastKey, entry, lastInsertedAbsence, mapList, index);
            } else {
                mapList.add(entry);
            }
        }
        return mapList;
    }

    private List<Map.Entry<String, Absence>> manageDuplicatedAbsences(
            String lastKey,
            Map.Entry<String,
            Absence> entry,
            Absence lastInsertedAbsence,
            List<Map.Entry<String, Absence>> mapList,
            int index) {
        if (lastKey.contains(SUBMITTED_ABSENCE) && entry.getKey().contains(VALIDATED_ABSENCE)) {
            if (lastInsertedAbsence.getDateFin().before(entry.getValue().getDateDebut()) &&
                    lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) ||
                    lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) &&
                            lastInsertedAbsence.getDateFin().after(entry.getValue().getDateDebut())) {
                mapList.remove(index);
                mapList.add(entry);
            } else {
                mapList.add(entry);
            }
        } else if (lastKey.contains(VALIDATED_ABSENCE) && entry.getKey().contains(SUBMITTED_ABSENCE)) {
            if (lastInsertedAbsence.getDateDebut().before(entry.getValue().getDateDebut()) &&
                    lastInsertedAbsence.getDateFin().before(entry.getValue().getDateDebut())) {
                mapList.add(entry);
            }
        } else {
            mapList.add(entry);
        }

        return mapList;
    }

    /**
     * Add absence to a map
     *
     * @param submittedAbsencesForCollaborator a list of {@link Absence}
     * @param validatedAbsencesForCollaborator a list of {@link Absence}
     * @return a {@link Map}
     */
    private Map<String, Absence> addAbsencesToMap(List<Absence> submittedAbsencesForCollaborator,
                                                  List<Absence> validatedAbsencesForCollaborator) {
        Map<String, Absence> allAbsencesForCollaborator = new HashMap<>();

        if (!submittedAbsencesForCollaborator.isEmpty()) {
            for (int i = 0; i < submittedAbsencesForCollaborator.size(); i++) {
                Absence absence = submittedAbsencesForCollaborator.get(i);
                allAbsencesForCollaborator.put(SUBMITTED_ABSENCE+ i, absence);
            }
        }

        if (!validatedAbsencesForCollaborator.isEmpty()) {
            for (int i = 0; i < validatedAbsencesForCollaborator.size(); i++) {
                Absence absence = validatedAbsencesForCollaborator.get(i);
                allAbsencesForCollaborator.put(VALIDATED_ABSENCE + i, absence);
            }
        }

        return allAbsencesForCollaborator;
    }

    /**
     * Sort an absences list map
     *
     * @param unsortedMap a {@link Map}
     * @return a {@link List}
     */
    private static List<Map.Entry<String, Absence>> sortAllAbsencesByDateDebut(Map<String, Absence> unsortedMap) {

        // List map conversion
        List<Map.Entry<String, Absence>> mapList = new LinkedList<>(unsortedMap.entrySet());

        // Sort the list with a custom comparator
        mapList.sort(Comparator.comparing(absence -> (absence.getValue().getDateDebut())));

        return mapList;
    }

    /**
     * Update all absence dates fot the timeline
     *
     * @param absences a list of {@link Mission}
     * @throws ParseException parseException
     */
    public void updateDatesForTimelineSchedulerForAbsences(List<Absence> absences) throws ParseException {
        for (Absence absence : absences) {
            updateDatesForTimelineScheduler(absence);
        }
    }

    /**
     * Update all mission dates for the timeline
     *
     * @param missions a list of {@link Mission}
     * @throws ParseException parseException
     */
    public void updateDatesForTimelineSchedulerForMissions(List<Mission> missions) throws ParseException {
        for (Mission mission : missions) {
            updateDatesForTimelineScheduler(mission);
        }
    }

    /**
     * Update all dates to display an absence
     *
     * @param absence a list of {@link Absence}
     * @throws ParseException parseException
     */
    public void updateDatesForTimelineScheduler(Absence absence) throws ParseException {

        String startDate;
        String endDate;

        Date dateDebut;
        Date dateFin;

        startDate = absence.getDateDebut().toString();
        endDate = absence.getDateFin().toString();

        dateDebut = convertStringToTimestampForDateDebutAbsence(startDate);
        dateFin = convertStringToTimestampForDateFinAbsence(endDate);

        absence.setDateDebut(dateDebut);
        absence.setDateFin(dateFin);

    }

    /**
     * Update the dates to display missions
     *
     * @param mission a {@link Mission}
     * @throws ParseException parseException
     */
    public void updateDatesForTimelineScheduler(Mission mission) throws ParseException {

        String startDate;
        String endDate;

        Date dateDebut;
        Date dateFin;

        startDate = mission.getDateDebut().toString();
        endDate = mission.getDateFin().toString();

        dateDebut = convertStringToTimestampForDateDebutMission(startDate);
        dateFin = convertStringToTimestampForDateFinMission(endDate);

        // When absence ends in the same time as the end of mission
        if (dateDebut.before(dateFin)) {
            mission.setDateDebut(dateDebut);
            mission.setDateFin(dateFin);
        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateDebut);
            calendar.set(Calendar.SECOND, 1);

            mission.setDateDebut(dateDebut);
            mission.setDateFin(calendar.getTime());
        }
    }


    /**
     * Return string date
     *
     * @param pDate            date
     * @param simpleDateFormat dateformat
     * @param calendar         calendar
     * @return formatted date
     */
    public String formatDate(String pDate, SimpleDateFormat simpleDateFormat, Calendar calendar) {

        if (pDate == null) {
            pDate = simpleDateFormat.format(calendar.getTime());
            String year = pDate.substring(0, 4);
            String month = pDate.substring(5, 7);
            String day = pDate.substring(8, 10);

            return day + "/" + month + "/" + year;

        } else {
            String year = pDate.substring(6, 10);
            String month = pDate.substring(3, 5);
            String day = pDate.substring(0, 2);

            return year + "-" + month + "-" + day;
        }
    }

    /**
     * Convert a date in timestamp for the start date of mission
     *
     * @param date date String date to convert
     * @return date Timestamp date
     * @throws ParseException parseException
     */
    private Date convertStringToTimestampForDateDebutMission(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        String time;
        String hour = date.substring(11, 19);

        // To avoid confusion between 00:00:00 and 12:00:00
        // Mission will start at midnight or midday if absence is not included in the day
        switch (hour) {
            // Mission hour equals 00:00:00 or 00:00:01
            case MIDNIGHT:
            case MIDNIGHT_PLUS_ONE_SECOND:
                time = MIDNIGHT;
                break;
            // Mission hour equals 11:59:58 or 11:59:59 or 12:00:00
            case MIDDAY_MINUS_TWO_SECONDS:
            case MIDDAY_MINUS_ONE_SECOND:
            case MIDDAY:
                time = MIDDAY_MINUS_ONE_SECOND;
                break;
            // Mission hour equals 23:59:59
            case MIDNIGHT_MINUS_ONE_SECOND:
                // Add one second to go to the next day
                date = addDay(date);
                time = MIDNIGHT;
                break;
            // To Avoid exception if one case is not managed
            // However a display bug could probably appear
            default:
                time = date.substring(11, 19);
                break;
        }

        stringBuilder.append(date, 0, 10);
        stringBuilder.append(" " + time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Convert a date in timestamp for the end date of mission
     *
     * @param date date
     * @return date
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateFinMission(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        String time;
        String hour = date.substring(11, 19);

        switch (hour) {
            case MIDNIGHT:
            case MIDNIGHT_PLUS_ONE_SECOND:
                time = MIDNIGHT;
                break;
            case MIDDAY_MINUS_ONE_SECOND:
            case MIDDAY:
                time = MIDDAY_MINUS_TWO_SECONDS;
                break;
            case MIDNIGHT_MINUS_ONE_SECOND:
                date = addDay(date);
                time = MIDNIGHT;
                break;
            default:
                time = date.substring(11, 19);
                break;
        }

        stringBuilder.append(date, 0, 10);
        stringBuilder.append(" " + time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        logger.info("Date mission: " + stringToDate + "\nTimeStamp: " + new Timestamp(stringToDate.getTime()));


        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Convert a date in timestamp for the start absence date
     *
     * @param date date
     * @return date
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateDebutAbsence(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        // Time variable to set the new time that will be necessary to create the timeline
        String time;
        String hour = date.substring(11, 19);

        String substractedHour = hour.substring(0, 2);

        switch (substractedHour){
            case SEVEN_AM:
            case EIGHT_AM:
                time = MIDNIGHT_PLUS_ONE_SECOND;
                break;
            case TWELVE_AM:
            case ONE_PM:
                time = MIDDAY_MINUS_ONE_SECOND;
                break;
            default:
                time = hour;
                break;
        }

        stringBuilder.append(date, 0, 11);
        stringBuilder.append(time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Convert a date in timestamp for the end absence date
     *
     * @param date date
     * @return date Date to convert in timestamp
     * @throws ParseException ParseException
     */
    private Date convertStringToTimestampForDateFinAbsence(String date) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        // Time variable to set the new time that will be necessary to create the timeline
        String time;
        String hour = date.substring(11, 19);
        String substractedHour = hour.substring(0, 2);

        switch (substractedHour){
            case ELEVEN_AM:
            case TWELVE_AM:
                time = MIDDAY_MINUS_TWO_SECONDS;
                break;
            case FOUR_PM:
            case FIVE_PM:
            case SIX_PM:
                time = MIDNIGHT_MINUS_ONE_SECOND;
                break;
            default:
                time = hour;
                break;
        }

        stringBuilder.append(date, 0, 11);
        stringBuilder.append(time);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);
        Date stringToDate = simpleDateFormat.parse(stringBuilder.toString());

        return new Timestamp(stringToDate.getTime());
    }

    /**
     * Format a datetime and convert it to string
     *
     * @param date date
     * @return dateTime.toString()
     * @throws ParseException ParseException
     */
    private String addDay(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_yyyy_MM_dd_HH_mm_ss);
        Date stringToDate = simpleDateFormat.parse(date);
        DateTime dateTime = new DateTime(stringToDate);
        // Add one second to go to the next day
        // Example of hour in the format 23:59:59
        dateTime = dateTime.plusSeconds(1);
        return dateTime.toString();
    }

    /**
     *
     * Merge absences if they are overlapped in time
     *
     * @param absences       a list of {@link Absence}
     * @param collaborators a list of {@link CollaborateurAsJson}
     * @return mergedAbsences a list of {@link Absence}
     */
    public List<Absence> mergeAbsences(List<Absence> absences, List<CollaborateurAsJson> collaborators) {

        List<Absence> absencesForCollaborateur = new ArrayList<>();
        List<Absence> mergedAbsences = new ArrayList<>();
        for (CollaborateurAsJson collaborator : collaborators) {
            if (!absencesForCollaborateur.isEmpty()) {
                absencesForCollaborateur.clear();
            }
            // Add all collaborator absences
            if (absencesForCollaborateur.isEmpty()) {
                for (Absence absence : absences) {
                    if (absence.getCollaborateur().getId().equals(collaborator.getId())) {
                        absencesForCollaborateur.add(absence);
                    }
                }
                sortAbsences(absencesForCollaborateur);

                // Merge absences
                for (Absence absence : absencesForCollaborateur) {
                    addMergedAbsencesToMergedList(mergedAbsences, absence);
                }
            }
        }

        return mergedAbsences;
    }

    /**
     * Merge the following absences in the timeline
     *
     * @param absences       a list of {@link Absence}
     * @param collaborators a list of {@link CollaborateurAsJson}
     * @return a list of {@link Absence}
     */
    public List<Absence> mergeMultipleAbsences(List<Absence> absences, List<CollaborateurAsJson> collaborators) {

        List<Absence> resultsAbsences = new ArrayList<>();

        for (CollaborateurAsJson collaborator : collaborators) {

            List<Absence> absencesForCollaborateur = new ArrayList<>();

            mergeMultipleAbsencesForOneCollaborateur(absences, resultsAbsences, collaborator, absencesForCollaborateur);

            if (!absencesForCollaborateur.isEmpty()) {
                absencesForCollaborateur.clear();
            }

        }
        return resultsAbsences;
    }

    /**
     * Merge two absences if the end and start dates follow one another
     *
     * @param absences                 a list of {@link Absence}
     * @param resultsAbsences          a list of {@link Absence}
     * @param collaborator            a {@link CollaborateurAsJson}
     * @param absencesForCollaborateur a list of {@link Absence}
     */
    private void mergeMultipleAbsencesForOneCollaborateur(List<Absence> absences,
                                                          List<Absence> resultsAbsences,
                                                          CollaborateurAsJson collaborator,
                                                          List<Absence> absencesForCollaborateur) {
        if (absencesForCollaborateur.isEmpty()) {

            for (Absence absence : absences) {
                if (absence.getCollaborateur().getId().equals(collaborator.getId())) {
                    absencesForCollaborateur.add(absence);
                }
            }
            sortAbsences(absencesForCollaborateur);

            for (Absence absence : absencesForCollaborateur) {
                addMergeToList(resultsAbsences, absence);
            }
        }
    }

    /**
     * Add absences to a list after treatment
     *
     * @param resultsAbsences a list of {@link Absence}
     * @param absence         a {@link Absence}
     */
    private void addMergeToList(List<Absence> resultsAbsences, Absence absence) {
        if (!resultsAbsences.isEmpty()) {
            int index = resultsAbsences.size() - 1;
            Absence lastInsertedAbsence = resultsAbsences.get(index);

            if (lastInsertedAbsence.getCollaborateur().getId().equals(absence.getCollaborateur().getId())) {
                if (checkDates(lastInsertedAbsence, absence)) {
                    absence.setDateDebut(lastInsertedAbsence.getDateDebut());
                    resultsAbsences.remove(index);
                    resultsAbsences.add(absence);
                } else {
                    resultsAbsences.add(absence);
                }
            } else {
                resultsAbsences.add(absence);
            }
        } else {
            resultsAbsences.add(absence);
        }
    }

    /**
     * Add merged absences to a final absences list
     *
     * @param mergedAbsences a list of {@link Absence}
     * @param absence        a {@link Absence}
     */
    private void addMergedAbsencesToMergedList(List<Absence> mergedAbsences, Absence absence) {
        // Merged list size > 0, add default absence
        // Compare last inserted absence with the one passed in parameter
        if (!mergedAbsences.isEmpty()) {
            int size = mergedAbsences.size();
            int index = size - 1;
            Absence lastInsertedAbsence = mergedAbsences.get(index);
            // If the absences concern the same collaborator, check the dates and merge them if needed
            if (lastInsertedAbsence.getCollaborateur().getId().equals(absence.getCollaborateur().getId())) {
                // Case 1 - Absence starts before the second one and it ends before:
                // Set inserted absence date to the start date of the second absence,
                // Delete the first absence of the liste and add the second absence.
                if (lastInsertedAbsence.getDateDebut().before(absence.getDateDebut())
                        && lastInsertedAbsence.getDateFin().after(absence.getDateDebut())
                        && lastInsertedAbsence.getDateFin().before(absence.getDateFin())) {
                    absence.setDateDebut(lastInsertedAbsence.getDateDebut());
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // Case 2
                    // - inserted absence starts after the second one,
                    // - its end date finish after the end of the second one,
                    // - its start date is before the end date of the second absence:
                    // Set inserted absence date to the end date of the second absence,
                    // Delete first absence in the list and add the second.
                } else if (lastInsertedAbsence.getDateDebut().after(absence.getDateDebut())
                        && lastInsertedAbsence.getDateFin().after(absence.getDateFin())
                        && lastInsertedAbsence.getDateDebut().before(absence.getDateFin())) {
                    absence.setDateFin(lastInsertedAbsence.getDateFin());
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // Case 3 - inserted absence is nested in the second absence
                    // Delete the first one of the list and add the secon one.
                } else if (lastInsertedAbsence.getDateDebut().after(absence.getDateDebut())
                        && lastInsertedAbsence.getDateFin().before(absence.getDateFin())) {
                    mergedAbsences.remove(index);
                    mergedAbsences.add(absence);
                    // Case 4 - inserted absence is not in the same interval as the second ,
                    // Add the second absence

                    // Case 5 - unlike the 3 rd case, we do nothing
                } else if (lastInsertedAbsence.getDateDebut().before(absence.getDateDebut())
                        && lastInsertedAbsence.getDateFin().before(absence.getDateDebut())) {
                    mergedAbsences.add(absence);
                }
            } else {
                mergedAbsences.add(absence);
            }
        } else {
            mergedAbsences.add(absence);
        }
    }

    /**
     * Check if the end and start dates of two absences follow each other
     *
     * @param lastAbsence      a {@link Absence}
     * @param absenceToCompare a {@link Absence}
     * @return true or false
     */
    private boolean checkDates(Absence lastAbsence, Absence absenceToCompare) {

        DateTime dateFin = new DateTime(lastAbsence.getDateFin());
        DateTime dateDebut = new DateTime(absenceToCompare.getDateDebut());

        if (dateFin.getDayOfMonth() == dateDebut.getDayOfMonth() && dateFin.getMonthOfYear() == dateDebut.getMonthOfYear()) {
            return true;
            // Knowing that the absence ends at 18:00:00, we add 14 hours to go to the nest day at 08:00:00
        } else if (dateFin.plusHours(14).getDayOfMonth() == dateDebut.getDayOfMonth()
                && dateFin.plusHours(14).getMonthOfYear() == dateDebut.getMonthOfYear()) {
            DateTime dateModif = new DateTime(dateFin.plusHours(14));
            return dateDebut.getHourOfDay() - dateModif.getHourOfDay() == 0;
        } else {
            return false;
        }
    }

    /**
     * Sort collaborator absences list according to the start dates
     *
     * @param absencesForCollaborateur a list of {@link Absence}
     */
    private void sortAbsences(List<Absence> absencesForCollaborateur) {
        Collections.sort(absencesForCollaborateur, Comparator.comparing(Absence::getDateDebut));
    }

    /**
     * Sort collaborator missions list according to the start dates
     *
     * @param missionsForCollaborator a list of {@link Mission}
     */
    private void sortMissions(List<Mission> missionsForCollaborator) {
        Collections.sort(missionsForCollaborator, Comparator.comparing(Mission::getDateDebut));
    }
}
