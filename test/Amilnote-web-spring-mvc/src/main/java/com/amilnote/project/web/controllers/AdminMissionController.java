package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.*;
import com.amilnote.project.metier.domain.pdf.PDFBuilderODM;
import com.amilnote.project.metier.domain.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controller de la Gestion des mission:
 * Gestion des Missions ( editor, modification, save )
 * Created by Cédric Leguay on 27/02/2019.
 */
@Controller
@RequestMapping("/Administration")
public class AdminMissionController extends AbstractController {

    private static final String PARAMETER_WITH_FORFAIT = "withForfaitUserChoice";
    private static final String PARAMETER_ID_CLIENT_SELECTED = "idClientSelected";

    private static final String PROP_MISSIONTOEDIT = "currentEditMission";

    private static final String VALUE_ERROR = "Erreur";
    private static final String VALUE_MISSION = "mission";
    private static final String VALUE_LISTFORFAITS = "listForfaits";
    private static final String VALUE_COLLABORATOR = "collaborateur";

    private static final String REDIRECT = "redirect:/";
    private static final String NULL = "/null";

    @Autowired
    private CollaboratorService collaboratorService;
    @Autowired
    private AgenceService agenceService;
    @Autowired
    private ForfaitService forfaitService;
    @Autowired
    private OutilsInterneService outilsInterneService;
    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;
    @Autowired
    private MailService mailService;
    @Autowired
    private FactureService factureService;
    @Autowired
    protected MissionService missionService;
    @Autowired
    protected ClientService clientService;
    @Autowired
    protected ContactClientService contactClientService;
    @Autowired
    protected TypeMissionService typeMissionService;
    @Autowired
    protected LinkMissionForfaitService linkMissionforfaitService;
    @Autowired
    protected EtatService etatService;
    @Autowired
    protected PDFBuilderODM pdfBuilderODM;

    /**
     * To edit a mission
     *
     * @param session  the current session
     * @param request  the request
     * @param model    the model
     * @param idMiss   if of the mission we want to edit
     * @param idCollab id of the current collaborator
     * @return redirection to another page
     */
    @RequestMapping(value = {"editCollaborateur/{idCollaborateur}/editMission/{idMission}"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String editMission(HttpSession session, HttpServletRequest request, Model model,
                              @PathVariable("idMission") Long idMiss,
                              @PathVariable("idCollaborateur") Long idCollab) {
        init(request, model);
        Collaborator currentCollaborator = collaboratorService.get(idCollab);

        if (null != currentCollaborator && null != idMiss) {

            Mission mission = missionService.get(idMiss);

            if (null != mission && mission.getCollaborateur().getId().equals(currentCollaborator.getId())) {
                boolean lWithForfait = false;
                if (model.containsAttribute(PARAMETER_WITH_FORFAIT)) {
                    lWithForfait = (boolean) model.asMap().get(PARAMETER_WITH_FORFAIT);
                }

                // To update the contacts' list according to the selected client in the drop-down list
                Long idClientSelected = 0L;
                List<ContactClientAsJson> listContacts;
                List<ContactClientAsJson> listContactsClient = new ArrayList<>();
                List<ContactClientAsJson> listContactsFacturation = new ArrayList<>();
                Client clientSelected = new Client();
                if (model.containsAttribute(PARAMETER_ID_CLIENT_SELECTED)) {
                    idClientSelected = (Long) model.asMap().get(PARAMETER_ID_CLIENT_SELECTED);
                    clientSelected = clientService.get(idClientSelected);
                    listContacts = contactClientService.findAllContactsForClient(clientSelected);
                    for (ContactClientAsJson contact : listContacts) {
                        if (contact.getRespClient()) {
                            listContactsClient.add(contact);
                        }
                        if (contact.getRespFacturation()) {
                            listContactsFacturation.add(contact);
                        }
                    }

                } else if (mission.getClient() != null) {
                    idClientSelected = mission.getClient().getId();
                    clientSelected = clientService.get(idClientSelected);
                    listContacts = contactClientService.findAllContactsForClient(clientSelected);
                    for (ContactClientAsJson contact : listContacts) {
                        if (contact.getRespClient()) {
                            listContactsClient.add(contact);
                        }
                        if (contact.getRespFacturation()) {
                            listContactsFacturation.add(contact);
                        }
                    }
                }
                String lServletPath = request.getServletPath().substring(1);
                request.setAttribute("origRequestURL", lServletPath);
                model.addAttribute("listeManagers", collaboratorService.addListManagers());
                model.addAttribute("withForfait", lWithForfait);

                ClientAsJson clientJson = clientSelected.toJson();
                model.addAttribute("clientSelectedJson", clientJson);
                model.addAttribute("listContactsClient", listContactsClient);
                model.addAttribute("listContactsFacturation", listContactsFacturation);
                model.addAttribute(PARAMETER_ID_CLIENT_SELECTED, idClientSelected);

                MissionAsJson tmpMissionAsJson = mission.toJson();
                tmpMissionAsJson.setListMissionForfait(mission.getListMissionForfaits());
                tmpMissionAsJson.setCommandes(mission.getCommandes());

                session.setAttribute(PROP_MISSIONTOEDIT, tmpMissionAsJson);

                // Adding the perimeter's list
                List<PerimetreMission> lPerimetreMission = missionService.getAllPerimetreMission();
                List<PerimetreMissionAsJson> lPerimetreMissionJson = new ArrayList<>();
                if (!lPerimetreMission.isEmpty()) {
                    for (PerimetreMission perimettreMission : lPerimetreMission) {
                        lPerimetreMissionJson.add(perimettreMission.toJson());
                    }
                }
                model.addAttribute("listPerimetreMissionJson", lPerimetreMissionJson);

                // Passing the checkboxes' list
                List<Long> listIdPerimetreMission = new ArrayList<>();
                for (LinkPerimetreMission linkPerimetreMission : mission.getListPerimetreMission()) {
                    listIdPerimetreMission.add(linkPerimetreMission.getPerimetreMission().getId());
                }
                model.addAttribute("listIdPerimetreMissionChecked", listIdPerimetreMission);
                model.addAttribute(VALUE_MISSION, tmpMissionAsJson);
                model.addAttribute(VALUE_COLLABORATOR, currentCollaborator);

                List<TypeMissionAsJson> listtypemission = typeMissionService.getAllTypeMissionAsJson();

                Long idAgence;
                List<Agence> listeAgence = agenceService.getAgenceChoixODM();
                if (tmpMissionAsJson.getAgence() != null) {
                    idAgence = tmpMissionAsJson.getAgence().getId();
                } else {
                    idAgence = agenceService.getAgenceSiegeSocial().getId();
                }

                String raValide = "";
                if (linkEvenementTimesheetService.isRaValideWithMission(mission, idCollab)) {
                    raValide = "disabled";
                }
                model.addAttribute("raValide", raValide);
                model.addAttribute("typesMission", listtypemission);
                model.addAttribute("idAgence", idAgence);
                model.addAttribute("listeAgence", listeAgence);
                model.addAttribute(VALUE_LISTFORFAITS, forfaitService.getAllForfaitsAsJson());
                model.addAttribute("listForfaitsLib", linkMissionforfaitService.findLibByMission(mission));
                model.addAttribute("listForfaitsNotLib", linkMissionforfaitService.findNotLibByMission(mission));
                model.addAttribute("listClients", clientService.findAllOrderByNomAscAsJson());
                model.addAttribute("outilsInternes", outilsInterneService.getAllOutilsInterneAsJson());
                return NAV_ADMIN + NAV_ADMIN_EDITMISSION;
            } else {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
            }
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
    }

    /**
     * Allows to visualize the pdf file of the work order
     *
     * @param idMiss   the id miss
     * @param idColl   the id coll
     * @param response the response
     * @param model    the model
     * @throws Exception the exception
     */
    @RequestMapping(value = {"editCollaborateur/{idCollaborateur}/editMission/{idMission}/download"}, method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void visualisationMission(HttpServletResponse response, Model model,
                                     @PathVariable("idMission") Long idMiss,
                                     @PathVariable("idCollaborateur") Long idColl) throws Exception {
        init(model);
        // Verifying the ID
        if (null != idMiss) {
            // Obtaining the PDF file
            File lFileODM = missionService.getFileODMId(idMiss);
            if (lFileODM != null) {
                // Constructing the header of the HTTP answer
                httpResponseConstruct("pdf", response, lFileODM);
            }
        }
    }

    /**
     * Retrieves the selected client's ID to update the contact's list
     *
     * @param model    the model
     * @param ra       the ra
     * @param idCollab the id collab
     * @param idClient the id client
     * @param idMiss   the id miss
     * @return string
     */
    @RequestMapping(value = {"editCollaborateur/{idCollaborateur}/editMission/{idMission}/getContacts/{idClient}"}, method = {RequestMethod.GET})
    public String editMissionAddListContact(Model model, RedirectAttributes ra,
                                            @PathVariable("idCollaborateur") Long idCollab,
                                            @PathVariable("idClient") Long idClient,
                                            @PathVariable("idMission") Long idMiss) {
        init(model);
        ra.addFlashAttribute(PARAMETER_ID_CLIENT_SELECTED, idClient);
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + idCollab + NAV_ADMIN_EDITMISSION + "/" + idMiss;
    }

    /**
     * Method to create a new mission
     *
     * @param request the http servlet request
     * @param model the model
     * @param collaboratorId the new mission will be related to this collaborator
     * @return redirection to another page
     */
    @RequestMapping(value = {"editCollaborateur/{idCollaborateur}/editMission"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newMission(HttpServletRequest request, Model model, @PathVariable("idCollaborateur") Long collaboratorId) {

        init(request, model);
        boolean lWithForfait = false;

        if (model.containsAttribute(PARAMETER_WITH_FORFAIT))
            lWithForfait = (boolean) model.asMap().get(PARAMETER_WITH_FORFAIT);

        // To update the contacts' list according to the selected client in the drop-down list
        Long idClientSelected = 0L;
        List<ContactClientAsJson> listContacts;
        List<ContactClientAsJson> listContactsClient = new ArrayList<>();
        List<ContactClientAsJson> listContactsFacturation = new ArrayList<>();
        Client clientSelected = new Client();

        if (model.containsAttribute(PARAMETER_ID_CLIENT_SELECTED)) {
            idClientSelected = (Long) model.asMap().get(PARAMETER_ID_CLIENT_SELECTED);
            clientSelected = clientService.get(idClientSelected);
            listContacts = contactClientService.findAllContactsForClient(clientSelected);
            for (ContactClientAsJson contact : listContacts) {
                if (contact.getRespClient()) {
                    listContactsClient.add(contact);
                }
                if (contact.getRespFacturation()) {
                    listContactsFacturation.add(contact);
                }
            }
        }

        String lServletPath = request.getServletPath().substring(1);
        request.setAttribute("origRequestURL", lServletPath);
        model.addAttribute("listeManagers", collaboratorService.addListManagers());
        model.addAttribute("withForfait", lWithForfait);

        ClientAsJson clientJson = clientSelected.toJson();
        model.addAttribute("clientSelectedJson", clientJson);
        model.addAttribute("listContactsClient", listContactsClient);
        model.addAttribute("listContactsFacturation", listContactsFacturation);
        model.addAttribute(PARAMETER_ID_CLIENT_SELECTED, idClientSelected);

        // Adding the perimeter's list
        List<PerimetreMission> lPerimetreMission = missionService.getAllPerimetreMission();
        List<PerimetreMissionAsJson> lPerimetreMissionJson = new ArrayList<>();
        if (!lPerimetreMission.isEmpty()) {
            for (PerimetreMission perimettreMission : lPerimetreMission) {
                lPerimetreMissionJson.add(perimettreMission.toJson());
            }
        }
        model.addAttribute("listPerimetreMissionJson", lPerimetreMissionJson);
        model.addAttribute("listForfaitsLIBJsonToAdd", forfaitService.getListForfaitsLIBToAdd(collaboratorId));
        model.addAttribute("listForfaitsNonLIBJsonToAdd", forfaitService.getListForfaitsNonLIBToAdd(collaboratorId));

        MissionAsJson mission = new MissionAsJson();
        List<TypeMissionAsJson> listtypemission = typeMissionService.getAllTypeMissionAsJson();

        Long idAgence = agenceService.getAgenceSiegeSocial().getId();
        List<Agence> listeAgence = agenceService.getAgenceChoixODM();
        model.addAttribute("idAgence", idAgence);
        model.addAttribute("listeAgence", listeAgence);
        model.addAttribute("typesMission", listtypemission);
        model.addAttribute("outilsInternes", outilsInterneService.getAllOutilsInterneAsJson());

        Collaborator collaborateurWithMission = collaboratorService.get(collaboratorId);
        if (null != collaborateurWithMission) {
            model.addAttribute(VALUE_MISSION, mission);
            model.addAttribute(VALUE_COLLABORATOR, collaborateurWithMission);
            model.addAttribute("forfait", new LinkMissionForfaitAsJson());
            model.addAttribute(VALUE_LISTFORFAITS, forfaitService.getAllForfaitsAsJson());
            model.addAttribute("listClients", clientService.findAllOrderByNomAscAsJson());

            return NAV_ADMIN + NAV_ADMIN_EDITMISSION;
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
    }

    /***
     * Add a new mission with packages when the "withForfait" box is checked
     * The idClient allows to memorize the selected client and display any linked contact
     *
     * @param model       the model
     * @param ra          the redirect attribute
     * @param idCollab    the id of the collaborator currently edited
     * @param withForfait the checkbox conditioning the display of the packages (forfaits)
     * @param idClient    the client's id
     * @return a redirection towards an other page
     */
    @RequestMapping(value = {"editCollaborateurForfait/{idCollaborateur}/editMission/{withForfait}/{idClient}"}, method = {RequestMethod.GET})
    public String newMissionWithForfait(Model model, RedirectAttributes ra,
                                        @PathVariable("idCollaborateur") Long idCollab,
                                        @PathVariable("withForfait") String withForfait,
                                        @PathVariable("idClient") Long idClient) {

        init(model);
        ra.addFlashAttribute(PARAMETER_WITH_FORFAIT, "true".equals(withForfait));
        if (idClient != 0) {
            ra.addFlashAttribute(PARAMETER_ID_CLIENT_SELECTED, idClient);
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + idCollab + NAV_ADMIN_EDITMISSION;
    }

    /**
     * To save  or edit a new mission
     *
     * @param session         current session
     * @param request         the request
     * @param model           the model
     * @param missionJson     the mission we wante to save
     * @param ra              to hide the redirection url parameters
     * @param idCollaborateur id of the current collaborator
     * @return redirection towards an other page
     * @throws Exception the Exception
     */
    @RequestMapping(value = {"/{idCollaborateur}/saveMission", "{idCollaborateur}/saveMission/"}, method = RequestMethod.POST)
    public String saveMission(HttpSession session, HttpServletRequest request, Model model, MissionAsJson missionJson, RedirectAttributes ra,
                              @PathVariable("idCollaborateur") Long idCollaborateur) throws Exception {
        init(request, model);
        String missionReturn;
        String bundleReturn;
        String pdfReturn;
        String checkboxValidationODM;

        checkboxValidationODM = request.getParameter("validationCollaborateur");
        Collaborator collaboratorWithMission = collaboratorService.get(idCollaborateur);

        List<LinkMissionForfaitAsJson> listforfaitToAdd = forfaitService.getListForfaitsLIBToAdd(idCollaborateur);
        for (LinkMissionForfaitAsJson lmf : forfaitService.getListForfaitsNonLIBToAdd(idCollaborateur)) {
            listforfaitToAdd.add(lmf);
        }

        if (null == collaboratorWithMission) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur collaborateur is null");
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
        }

        if (missionJson.getDateDebut().after(missionJson.getDateFin())) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "La date de début doit être inférieure à la date de fin.");
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + (missionJson.getId() != null ? "/" + missionJson.getId() : "");
        }

        if (missionJson.getResponsableClient() != null && missionJson.getResponsableClient().getId() == -1) {
            missionJson.setResponsableClient(null);
        }
        if (missionJson.getClient() != null && missionJson.getClient().getId() == null) {
            missionJson.setClient(null);
        }
        if (missionJson.getTypeMission().getId() != 5) {
            missionJson.setSuivi("");
        }
        if (missionJson.getTypeMission().getId() != 9) {
            missionJson.setOutilsInterne(null);
        }

        // When we create a mission, we pass its status to "draft" so it can be validated by the direction and the collaborator
        if (isValidationCollaborateurChecked(checkboxValidationODM)) {
            Etat etatBR = etatService.getEtatByCode(Etat.ETAT_BROUILLON_CODE);
            missionJson.setEtat(new EtatAsJson(etatBR));
        }

        if (missionJson.getId() != null) {
            Mission mission = missionService.get(missionJson.getId());

            if (((mission.getDateFin()).compareTo(missionJson.getDateFin()) < 0)
                    && ((mission.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)) || (mission.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)))) {
                mailService.sendMailMissionExtension(mission);
            }
        }
        Collaborator collab = collaboratorService.get(collaboratorWithMission.getId());
        missionReturn = missionService.updateMission(missionJson, collab);

        String typeTravailleur = "/collaborateur/";
        if (collaboratorWithMission.getStatut().getId().equals(7L))
            typeTravailleur = "/sous_traitant/";

        if (missionReturn == "ok") {

            // We retrieve the mission which was just updated
            MissionAsJson missionUptoDate = missionService.get(missionJson.getId()).toJson();

            // We create the link between mission/package if any package is created at the same time as the mission
            if (listforfaitToAdd.size() > 0) {
                for (int i = 0; i < listforfaitToAdd.size(); i++) {
                    LinkMissionForfaitAsJson linkMissionforfaitAsJson = listforfaitToAdd.get(i);
                    if (linkMissionforfaitAsJson.getForfait() != null) {
                        bundleReturn = linkMissionforfaitService.addLinkMissionForfait(missionUptoDate, linkMissionforfaitAsJson);
                        if (bundleReturn != "ok") {
                            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la création du forfait");
                            return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + (missionJson.getId() != null ? "/" + missionJson.getId() : "");
                        }
                    }
                }
                missionUptoDate.setListMissionForfaitAsJson(listforfaitToAdd);
            }

            // We retrieve the mission we are currently editing to update to packages' list
            MissionAsJson missionSession = (MissionAsJson) session.getAttribute(PROP_MISSIONTOEDIT);
            if (missionSession != null) {
                missionUptoDate.setListMissionForfaitAsJson(missionSession.getListMissionForfait());

                if (missionSession.getCommandes() != null)
                    missionUptoDate.setCommandesAsJson(missionSession.getCommandes());
            }
            session.setAttribute(PROP_MISSIONTOEDIT, missionUptoDate);
            Collaborator collabPdf = collaboratorService.get(collaboratorWithMission.getId());

            // Retrieving the checkboxes' list
            List<LinkPerimetreMissionAsJson> lListLinkPerimetreMissionJson = new ArrayList<>();
            for (PerimetreMission perimettreMission : missionService.getAllPerimetreMission()) {
                if (request.getParameter("perimetreMission_" + perimettreMission.getId()) != null) {
                    LinkPerimetreMissionAsJson lLinkPerimetreMisson = new LinkPerimetreMissionAsJson();
                    lLinkPerimetreMisson.setMission(missionJson);
                    lLinkPerimetreMisson.setPerimetreMission(perimettreMission.toJson());
                    lListLinkPerimetreMissionJson.add(lLinkPerimetreMisson);
                }
            }
            missionJson.setPerimetreMissionAsJson(lListLinkPerimetreMissionJson);
            missionUptoDate.setPerimetreMissionAsJson(lListLinkPerimetreMissionJson);

            pdfReturn = pdfBuilderODM.createPDFODM(collabPdf, missionUptoDate);

            // Creation of a new invoice at the validation of a new work order
            factureService.createFactureAfterCron(missionService.get(missionJson.getId()));

            if (pdfReturn.contains(VALUE_ERROR)) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Impossible d'écrire dans le répertoire de destination");
                forfaitService.clearForfaitsMaps(idCollaborateur);
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + (missionJson.getId() != null ? "/" + missionJson.getId() : "");
            } else if (isValidationCollaborateurChecked(checkboxValidationODM)) {
                mailService.sendMailODMCreation(collabPdf);

                missionService.createOrUpdateLinkPerimetreMission(missionJson);

                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Création/Modification effectuée avec succès. Un Email a été envoyé à la direction pour valider l'ordre de mission.");
            } else {
                missionService.createOrUpdateLinkPerimetreMission(missionJson);
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Création/Modification effectuée avec succès.");
            }
            forfaitService.clearForfaitsMaps(idCollaborateur);
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR + typeTravailleur + collaboratorWithMission.getId() + NULL;
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la création de la mission");
            forfaitService.clearForfaitsMaps(idCollaborateur);
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR + typeTravailleur + collaboratorWithMission.getId() + NULL;
        }
    }

    /**
     * allows to know if the checkbox for the work order validation of the collaborator is checked or not
     *
     * @param checkboxValidationODM the checkbox's value
     * @return true or false
     */
    private boolean isValidationCollaborateurChecked(String checkboxValidationODM) {
        return "on".equals(checkboxValidationODM);
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
