/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderDeplacement;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.web.utils.AmiltoneUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Controller des demandes de Deplacements
 *
 * @author AJakubiak
 */
@Controller
@RequestMapping("/Deplacements")
public class DeplacementController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(DeplacementController.class);

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private AmiltoneUtils utils;

    @Autowired
    private PDFBuilderDeplacement pdfBuilderDeplacement;

    @Autowired
    private MissionService missionService;

    @Autowired
    private DemandeDeplacementService demandeDeplacementService;

    @Autowired
    private MailService mailService;

    @Autowired
    private FileService fileService;

    /**
     * New deplacement string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = {"editDeplacement", "editDeplacement/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String newDeplacement(HttpServletRequest request, Model model, HttpSession session) throws Exception {
        init(request, model);

        Collaborator collaborateur = utils.getCurrentCollaborator(collaboratorService);

        model.addAttribute("demandeDeplacement", new DemandeDeplacementAsJson());
        model.addAttribute("listMission", collaborateur.getMissions());

        return NAV_EDIT_DEPLACEMENT;
    }


    /**
     * Save deplacement string.
     *
     * @param request                  the request
     * @param response                 the response
     * @param model                    the model
     * @param demandeDeplacementAsJson the demande deplacement as json
     * @param ra                       the ra
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = {"saveDeplacement"}, method = {RequestMethod.POST})
    public String saveDeplacement(HttpServletRequest request, HttpServletResponse response, Model model, DemandeDeplacementAsJson demandeDeplacementAsJson, RedirectAttributes ra) throws Exception {
        init(request, model);

        if (demandeDeplacementAsJson.getDateDebut().after(demandeDeplacementAsJson.getDateFin())) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "La date de début doit être inférieure à la date de fin.");
            return "redirect:/" + NAV_EDIT_DEPLACEMENT;
        }

        Collaborator collaborateur = utils.getCurrentCollaborator(collaboratorService);
        String idMission = request.getParameter("idMission");
        Mission mission = missionService.get((long) Integer.parseInt(idMission));
        Etat etat = new Etat();
        etat.setCode(Etat.ETAT_SOUMIS_CODE);
        etat.setEtat(Etat.ETAT_SOUMIS);
        etat.setId(2);
        Date date = new Date();

        demandeDeplacementAsJson.setMission(mission);
        demandeDeplacementAsJson.setDateSoumission(date);
        demandeDeplacementAsJson.setEtat(etat);
        demandeDeplacementAsJson.setCollaborateur(collaborateur);

        if (null != mission) {
            try {
                // sauvegarder la demande de deplacement dans la BDD
                String res = demandeDeplacementService.saveNewDemandeDeplacement(demandeDeplacementAsJson);
                //La sauvegarde est un succès
                if (res == "2") {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "Demande de déplacement créée avec succès. Vous allez recevoir une copie par mail.");

                    //Ecriture et envoi par mail du PDF
                    String repertoire = Parametrage.getContext(Constantes.CONTEXT_FOLDER_DEPLACEMENT);
                    String nomFichier = "";
                    nomFichier = pdfBuilderDeplacement.createPDFDemandeDeplacement(collaborateur, demandeDeplacementAsJson);
                    mailService.sendMailMovement(collaborateur, repertoire, nomFichier);

                    // une fois le mail envoyé, le fichier est converti au format ZIP puis le format PDF est effacé
                    fileService.convertFileToZip(nomFichier, Constantes.CONTEXT_FOLDER_DEPLACEMENT);
                    fileService.deleteFile(nomFichier, Constantes.CONTEXT_FOLDER_DEPLACEMENT);
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Problème dans la création de la demande");
                }


            } catch (Exception ex) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, ex.getMessage());
            }
        }
        return "redirect:/" + NAV_WELCOME;
    }

    /**
     * Verif presence mission periode string.
     *
     * @param request        the request
     * @param model          the model
     * @param pDateDebutDepl the p date debut depl
     * @param pDateFinDepl   the p date fin depl
     * @param idMission      the id mission
     * @return the string
     */
    @RequestMapping(value = "/verifPresenceMissionPeriode/{dateDebut}/{dateFin}/{idMission}", method = {RequestMethod.POST})
    public
    @ResponseBody
    String verifPresenceMissionPeriode(HttpServletRequest request, Model model, @PathVariable("dateDebut") String pDateDebutDepl, @PathVariable("dateFin") String pDateFinDepl, @PathVariable("idMission") int idMission) {
        String retour = "-1";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long dateDebutDeplacement = 0;
        long dateFinDeplacement = 0;
        try {
            dateDebutDeplacement = format.parse(pDateDebutDepl).getTime();
            dateFinDeplacement = format.parse(pDateFinDepl).getTime();
        } catch (ParseException e) {
            logger.error("[check mission presence] parse dates between {} : {}", pDateDebutDepl, pDateFinDepl, e);
        }

        Mission mission = missionService.get((long) idMission);
        long dateDebutMission = mission.getDateDebut().getTime();
        long dateFinMission = mission.getDateFin().getTime();

        if (dateDebutDeplacement >= dateDebutMission && dateFinDeplacement <= dateFinMission) {
            retour = "1";
        } else {
            retour = "-1";
        }
        return retour;
    }


    /**
     * Permet de binder pour convertir les objets Date pour les formulaires
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

}
