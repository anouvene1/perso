/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.web.utils.AmiltoneUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;


/**
 * Controller des Rapports d'activités
 *
 * @author GCornet
 * @author KEsprit
 * @author KImbert
 */
@Controller
@RequestMapping("/Profil")
public class GestionProfilController extends AbstractController {

    @Autowired
    private AmiltoneUtils utils;

    @Autowired
    private CollaboratorService collaboratorService;

    // ---- GESTION APPLICATION ----//

    /**
     * Gérer l'application Amilnote
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @return string
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "gestionProfil")
    public String gestionProfil(HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        init(request, model);

        model.addAttribute("prenomNomCollab", utils.getCurrentCollaborator(collaboratorService).getPrenom() + " " + utils.getCurrentCollaborator(collaboratorService).getNom());
        model.addAttribute("mail", utils.getCurrentCollaborator(collaboratorService).getMail());

        return NAV_GESTIONPROFIL;
    }

    /**
     * Reset mdp string.
     *
     * @param request the request
     * @param model   the model
     * @param session the session
     * @param ra      the ra
     * @return the string
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    @RequestMapping(value = "resetMdp", method = {RequestMethod.GET, RequestMethod.POST})
    public String resetMdp(HttpServletRequest request, Model model, HttpSession session, RedirectAttributes ra) throws NoSuchAlgorithmException {
        init(request, model);
        String ancienMDP = request.getParameter("ancien");
        String nouveauMDP = request.getParameter("nouveau");
        String confirmationMDP = request.getParameter("confirmation");
        //ce hash = hash quand le client envois une requette avec rien dans le champ mot de passe (SHA-512)
        String emptyPWHash = "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e";

        //On regarde si des données ont bien été entrées
        if (!ancienMDP.isEmpty() && !nouveauMDP.isEmpty() && !confirmationMDP.isEmpty() && !nouveauMDP.equals(emptyPWHash)) {
            //On compare le nouveau mdp et l'ancien afin d'être sur qu'il est différent
            if (!nouveauMDP.equals(ancienMDP)) {
                //On compare le nouveau mdp et sa confirmation
                if (confirmationMDP.equals(nouveauMDP)) {
                    String secret = "";
                    Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder(secret, 185000, 512);

                    //On vérifie que l'ancien MDP est bien le bon
                    Collaborator collabId = collaboratorService.get(utils.getCurrentCollaborator(collaboratorService).getId());
                    String ancienMdpBase = collabId.getPassword();

                    if (pbkdf2PasswordEncoder.matches(ancienMDP, ancienMdpBase)) {
                        CollaboratorAsJson collaborateur = new CollaboratorAsJson();
                        collaborateur.setId(utils.getCurrentCollaborator(collaboratorService).getId());
                        collaborateur.setPassword(nouveauMDP);

                        //Enregistrement du nouveau password.
                        try {
                            collaboratorService.updateOrCreateCollaborator(collaborateur);
                        } catch (Exception e) {
                            ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, true);
                            ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, e.getMessage());
                        }

                        //Si tout s'est bien passé
                        ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, false);
                        ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, "Modification effectuée avec succès");
                    } else {
                        ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, true);
                        ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, "Erreur sur l'ancien mot de passe.");
                    }
                } else {
                    ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, true);
                    ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, "Le nouveau mot de passe et la confirmation du mot de passe ne correspondent pas.");
                }
            } else {
                ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, true);
                ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, "Le nouveau mot de passe est identique à l'ancien.");
            }
        } else {
            ra.addFlashAttribute(NAME_NAV_PROFIL_ERRORMDP, true);
            ra.addFlashAttribute(NAME_NAV_PROFIL_MESSMDP, "Les 3 champs doivent être renseignés.");
        }

        return "redirect:/" + NAV_GESTIONPROFIL;
    }
}
