package com.amilnote.project.web.configuration;

import com.amilnote.project.metier.domain.services.impl.AbsenceServiceImpl;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.enumerations.LevelEnvironment;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.apache.logging.log4j.core.config.plugins.Plugin;

import javax.naming.NamingException;
import java.net.URI;

@Plugin(name = "LoggerConfigurationFactory", category = ConfigurationFactory.CATEGORY)
@Order(50)
public class LoggerConfigurationFactory extends ConfigurationFactory {

    private static final Logger logger = LogManager.getLogger(LoggerConfigurationFactory.class);

    /**
     * Static method for creating log4j2 configuration programmatically
     *
     * @param name    {@link String}
     * @param builder {@link ConfigurationBuilder}
     * @return log4j2 configuration {@link Configuration}
     */
    static Configuration createConfiguration(final String name, ConfigurationBuilder<BuiltConfiguration> builder) {


        // Builder config
        builder.setConfigurationName(name);
        builder.setStatusLevel(Level.TRACE);

        // Pattern Layout
        LayoutComponentBuilder layoutBuilder = builder.newLayout("PatternLayout")
                .addAttribute("pattern", "%d{yyyy-MM-dd HH:mm:ss} [%thread]  %-5p - %c - %m%n");

        // Env variable
        Level envLevel = getEnvironmentLogLevel();
        String catalinaLogDir = System.getProperty("catalina.home") + "/logs";
        FilterComponentBuilder levelFilter = builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT,
                Filter.Result.DENY).addAttribute("level", envLevel);

        // Root Logger
        RootLoggerComponentBuilder rootLogger = builder.newRootLogger(Level.DEBUG);

        // Log to console if not prod
        if (!isProd()) {
            // Appender Config - Console
            AppenderComponentBuilder consoleAppender = builder.newAppender("console", "CONSOLE")
                    .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);
            consoleAppender.add(layoutBuilder);
            consoleAppender.add(levelFilter);
            builder.add(consoleAppender);

            // Logger - Console
            rootLogger.add(builder.newAppenderRef("console"))
                    .addAttribute("level", envLevel);
        }

        // Appender Config - RollingFile
        // Roll over log file by size and unit of time
        ComponentBuilder triggeringPolicy = builder.newComponent("Policies")
                .addComponent(builder.newComponent("TimeBasedTriggeringPolicy").addAttribute("interval", "1"))
                .addComponent(builder.newComponent("SizeBasedTriggeringPolicy").addAttribute("size", "100M"));

        AppenderComponentBuilder rollingfileAppender = builder.newAppender("rollingFile", "RollingFile")
                .addAttribute("fileName", catalinaLogDir + "/amilnote.log")
                .addAttribute("filePattern", catalinaLogDir + "/archive/$${date:yyyy-MM}/amilnote-%d{yyyy-MM-dd}.%i.log.gz")
                .add(layoutBuilder)
                .addComponent(triggeringPolicy);

        rollingfileAppender.add(levelFilter);
        builder.add(rollingfileAppender);

        // Logger - RollingFile
        rootLogger.add(builder.newAppenderRef("rollingFile"))
                .addAttribute("level", envLevel);
        builder.add(rootLogger);

        return builder.build();
    }

    /**
     * Returns log4j2 custom configuration
     *
     * @param loggerContext {@link LoggerContext}
     * @param source        {@link ConfigurationSource}
     * @return log4j2 configuration {@link Configuration}
     */
    @Override
    public Configuration getConfiguration(final LoggerContext loggerContext, final ConfigurationSource source) {
        return getConfiguration(loggerContext, source.toString(), null);
    }

    /**
     * Returns log4j2 custom configuration, used at the initialization of log4j2
     *
     * @param loggerContext  {@link LoggerContext}
     * @param name           {@link String}
     * @param configLocation {@link LoggerContext}
     * @return log4j2 configuration {@link Configuration}
     */
    @Override
    public Configuration getConfiguration(final LoggerContext loggerContext, final String name,
                                          final URI configLocation) {
        ConfigurationBuilder<BuiltConfiguration> builder = newConfigurationBuilder();
        return createConfiguration(name, builder);
    }

    /**
     * Returns the possible types for Configuration
     *
     * @return array of types {@link String}
     */
    @Override
    protected String[] getSupportedTypes() {
        return new String[]{"*"};
    }

    /**
     * Returns the minimum level logged according to the environment
     *
     * @return minimum level logged {@link Level}
     */
    private static Level getEnvironmentLogLevel() {

        switch (LevelEnvironment.getLevel(checkEnvironment())) {
            case DEV:
                return Level.DEBUG;
            case INT:
                return Level.INFO;
            default:
                return Level.WARN;
        }
    }

    /**
     * Returns level of the environment
     *
     * @return {@link String}
     */
    private static String checkEnvironment() {
        try {
            return String.valueOf(Parametrage.getContext(Constantes.ENVIRONMENT));
        } catch (NamingException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    /**
     * Returns true if the application runs in Production environment
     *
     * @return {@link Boolean}
     */
    private static Boolean isProd() {
        try {
            return Boolean.valueOf(Parametrage.getContext(Constantes.ENV_PROD));
        } catch (NamingException e) {
            return false;
        }
    }
}
