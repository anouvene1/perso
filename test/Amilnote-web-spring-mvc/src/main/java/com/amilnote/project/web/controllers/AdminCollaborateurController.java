/*
 * ©Amiltone 2017
 */

package com.amilnote.project.web.controllers;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.*;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import com.amilnote.project.metier.domain.utils.enumerations.DomainNames;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.*;


/**
 * Controller de la Gestion des collaborateurs:
 * Gestion des Collaborateurs ( Collaborator + Mission + Forfait + Commande )
 * collaborateurs
 * Created by azicaro on 05/08/2016.
 */
@Controller
@RequestMapping("/Administration")
public class AdminCollaborateurController extends AbstractTravailleurController {

    private static final Logger logger = LogManager.getLogger(AdminCollaborateurController.class);

    private static final String VALUE_ERROR = "Erreur";
    private static final String VALUE_MISSION = "mission";
    private static final String VALUE_LISTFORFAITS = "listForfaits";
    private static final String VALUE_COLLABORATOR = "collaborateur";

    private static final String PARAMETER_IDCOLLAB = "idCollab";

    @Autowired
    private CollaboratorService collaboratorService;
    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;
    @Autowired
    private MailService mailService;
    @Autowired
    private FraisService fraisService;
    @Autowired
    private StatutCollaborateurService statutCollaborateurService;
    @Autowired
    private FactureService factureService;
    @Autowired
    private ForfaitService forfaitService;
    @Autowired
    private DocumentExcelService documentExcelService;
    @Autowired
    private CiviliteService civiliteService;

    // ---- GESTION DES COLLABORATEURS ----//
    AbstractTravailleurService getService() {
        return collaboratorService;
    }

    /***
     * Allows to add or edit a collaborator according to the presence of an id. It is possible to filter the expenses reports
     *
     * @param typeTravailleur   type of the user currently edited
     * @param idTravailleur     the id of user currently edited
     * @param pFiltre           the filter
     * @param session           the session
     * @param request           the request
     * @param model             the model
     * @param response          the response
     * @param ra                allows to hide the URL's parameters during the redirection
     * @return a redirection towards an other page
     * @throws Exception the exception
     */
    @GetMapping("editTravailleur/{typeTravailleur}/{idTravailleur}/{pFiltre}")
    public String editCollaborateur(HttpSession session, HttpServletRequest request, Model model, HttpServletResponse response, RedirectAttributes ra,
                                    @PathVariable("typeTravailleur") String typeTravailleur,
                                    @PathVariable("idTravailleur") Long idTravailleur,
                                    @PathVariable("pFiltre") String pFiltre) throws Exception {
        init(model);

        String mois = request.getParameter("mois");
        String annee = request.getParameter("annee");
        String pDate = request.getParameter("monthYearExtract");

        if (pDate != null && !pDate.isEmpty()) {
            Integer pYear = Integer.valueOf(pDate.substring(pDate.length() - 4));
            Integer pMonth = Integer.valueOf(pDate.substring(0, 2)) - 1;
            mois = String.valueOf(pMonth);
            annee = String.valueOf(pYear);
        }

        List<LinkMissionForfaitAsJson> tempLIB = forfaitService.getListForfaitsLIBToAdd();
        List<LinkMissionForfaitAsJson> tempNonLIB = forfaitService.getListForfaitsNonLIBToAdd();

        tempLIB.clear();
        tempNonLIB.clear();
        forfaitService.setListForfaitsLIBToAdd(tempLIB);
        forfaitService.setListForfaitsNonLIBToAdd(tempNonLIB);

        if (idTravailleur != null) {

            Collaborator tmpCollaborateur = collaboratorService.get(idTravailleur);
            if (tmpCollaborateur != null) {
                CollaboratorAsJson collaboratorAsJson = tmpCollaborateur.toJson();

                String codePoste = collaboratorAsJson.getStatut().getCode();
                // The manager's list is displayed only to the collaborators
                if (codePoste.equals(STATUT_COLLABORATEUR) || codePoste.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                    // Generation of the manager and admin's list to supply the drop-down list of the manager's choice
                    List<CollaboratorAsJson> tmpListManagerAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_MANAGER);
                    List<CollaboratorAsJson> tmpListManagerDRHAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_DRH);
                    List<CollaboratorAsJson> tmpListManagerDIRAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_DIRECTION);
                    tmpListManagerDRHAsJson.addAll(tmpListManagerDIRAsJson);

                    String codeManager;
                    for (CollaboratorAsJson collab : tmpListManagerDRHAsJson) {
                        codeManager = collab.getPoste().getCode();
                        if (codeManager.equals(Poste.POSTE_MANAGER) || codeManager.equals(Poste.POSTE_DIRECTEUR)) {
                            tmpListManagerAsJson.add(collab);
                        }
                    }
                    Collections.reverse(tmpListManagerAsJson);
                    model.addAttribute("listManagers", tmpListManagerAsJson);
                }
                // Retrieves the expenses' list which are not in a "draft" state according to the filters upon the state and the month
                List<FraisAsJson> listFrais = fraisService.getFraisByCollaborator(tmpCollaborateur);
                List<FraisAsJson> tmpListFrais = new ArrayList<>();
                for (FraisAsJson frais : listFrais) {
                    if (!frais.getEtat().getCode().equals(Etat.ETAT_BROUILLON_CODE)) {
                        TypeFrais type = typeFraisService.getTypeFraisByCode(frais.getTypeFrais());
                        frais.setTypeFrais(type.getTypeFrais());

                        if ("null".equals(pFiltre) || frais.getEtat().getCode().equals(pFiltre)) {
                            if ((mois == null || mois.isEmpty() || moisIsOk(mois, frais)) &&
                                    (annee == null || annee.isEmpty() || anneeIsOk(annee, frais))) {
                                tmpListFrais.add(frais);
                            }
                        }
                    }
                }
                listFrais = tmpListFrais;
                model.addAttribute("listFrais", listFrais);
                session.removeAttribute(PROP_MISSION_TO_EDIT);

                List<MissionAsJson> listMission = missionService.findAllMissionsCollaborator(tmpCollaborateur, Mission.PROP_DATEDEBUT);
                List<MissionAsJson> tmpListMission = new ArrayList<>();
                for (MissionAsJson mission : listMission) {
                    File lFile = missionService.getFileODMId(mission.getId());
                    if (lFile != null) {
                        mission.setpdfODM(lFile.getPath());
                    } else {
                        mission.setpdfODM("null");
                    }
                    tmpListMission.add(mission);
                }
                List<PosteAsJson> listPostes = new ArrayList<>();
                List<StatutCollaborateurAsJson> listStatus = new ArrayList<>();
                if (typeTravailleur.equals(VALUE_COLLABORATOR)) {
                    StatutCollaborateurAsJson st = statutCollaborateurService.get(7L).toJson();
                    PosteAsJson psSS = postService.getPosteByCode(Poste.POSTE_SOUS_TRAITANT).toJson();

                    List<PosteAsJson> listPostesTemp = postService.getAllPosteAsJson();
                    List<StatutCollaborateurAsJson> listStatusTemp = statutCollaborateurService.getAllStatutAsJson();

                    for (StatutCollaborateurAsJson stas : listStatusTemp) {
                        if (!stas.getId().equals(st.getId()))
                            listStatus.add(stas);
                    }
                    for (PosteAsJson paj : listPostesTemp) {
                        if (!paj.getId().equals(psSS.getId()))
                            listPostes.add(paj);
                    }
                } else {
                    listPostes.add(postService.getPosteByCode(Poste.POSTE_SOUS_TRAITANT).toJson());
                    listStatus.add(statutCollaborateurService.get(7l).toJson()); //7 représente le statut sous-traitant
                }

                List<Civilite> listCivilite = civiliteService.findAllOrderById();
                List<Agency> listAgencies = Arrays.asList(Agency.values());
                TreeMap<ModuleAddvise, String> mapModuleDate = new TreeMap<>();

                model.addAttribute("mapModuleCollab", mapModuleDate);
                model.addAttribute("listMissions", tmpListMission);
                model.addAttribute("listPostes", listPostes);
                model.addAttribute("listStatuts", listStatus);
                model.addAttribute("codeManager", StatutCollaborateur.STATUT_MANAGER);
                model.addAttribute("codeAdmin", STATUT_ADMIN);
                model.addAttribute("typeTravailleur", typeTravailleur);
                model.addAttribute(VALUE_COLLABORATOR, collaboratorAsJson);
                model.addAttribute("listCivilite", listCivilite);
                model.addAttribute("listAgencies", listAgencies);
                model.addAttribute("listDomainNames", DomainNames.values());

                return NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR;
            }
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS;
    }

    private boolean moisIsOk(String mois, FraisAsJson frais) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(frais.getDateEvenement());
        String moisFrais = Integer.toString(calendar.get(Calendar.MONTH));

        return moisFrais.equals(mois) || "-1".equals(mois);
    }

    private boolean anneeIsOk(String annee, FraisAsJson frais) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(frais.getDateEvenement());
        String anneeFrais = Integer.toString(cal.get(Calendar.YEAR));

        return anneeFrais.equals(annee) || "-1".equals(annee);
    }

    /***
     * Adding a new collaborator
     *
     * @param typeTravailleur   the type or the user to be created
     * @param model             the mod
     * @param session           the session
     * @return a redirection towards an other page
     * @throws Exception the exception
     */
    @GetMapping("editTravailleur/{typeTravailleur}")
    public String newCollaborateur(Model model, HttpSession session,
                                   @PathVariable("typeTravailleur") String typeTravailleur) throws Exception {
        init(model);

        session.removeAttribute(PROP_MISSION_TO_EDIT);

        // Creation of the manager and admin's list to supply the drop-down of the manager's choice
        List<CollaboratorAsJson> tmpListManagerAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_MANAGER);
        List<CollaboratorAsJson> tmpListManagerDRHAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_DRH);
        List<CollaboratorAsJson> tmpListManagerDIRAsJson = collaboratorService.findAllByStatutOrderByNomAsJson(StatutCollaborateur.STATUT_DIRECTION);
        tmpListManagerDRHAsJson.addAll(tmpListManagerDIRAsJson);

        for (CollaboratorAsJson collab : tmpListManagerDRHAsJson) {
            if (collab.getPoste().getCode().equals(Poste.POSTE_MANAGER) || collab.getPoste().getCode().equals(Poste.POSTE_DIRECTEUR)) {
                tmpListManagerAsJson.add(collab);
            }
        }

        List<PosteAsJson> listPostes = new ArrayList<>();
        List<StatutCollaborateurAsJson> listStatus = new ArrayList<>();
        if (typeTravailleur.equals(VALUE_COLLABORATOR)) {
            StatutCollaborateurAsJson st = statutCollaborateurService.get(7L).toJson();
            PosteAsJson psSS = postService.getPosteByCode(Poste.POSTE_SOUS_TRAITANT).toJson();

            List<PosteAsJson> listPostesTemp = postService.getAllPosteAsJson();
            List<StatutCollaborateurAsJson> listStatusTemp = statutCollaborateurService.getAllStatutAsJson();

            for (StatutCollaborateurAsJson stas : listStatusTemp) {
                if (!stas.getId().equals(st.getId()))
                    listStatus.add(stas);
            }
            for (PosteAsJson paj : listPostesTemp) {
                if (!paj.getId().equals(psSS.getId()))
                    listPostes.add(paj);
            }
        } else {
            listPostes.add(postService.getPosteByCode(Poste.POSTE_SOUS_TRAITANT).toJson());
            listStatus.add(statutCollaborateurService.get(7L).toJson()); //7 corresponds to a subcontractor
        }

        List<Civilite> listCivilite = civiliteService.findAllOrderById();
        List<Agency> listAgencies = Arrays.asList(Agency.values());

        Collections.reverse(tmpListManagerAsJson);
        model.addAttribute("listManagers", tmpListManagerAsJson);
        model.addAttribute(VALUE_COLLABORATOR, new CollaboratorAsJson());
        model.addAttribute("listPostes", listPostes);
        model.addAttribute("listStatuts", listStatus);
        model.addAttribute("listDomainNames", DomainNames.values());
        model.addAttribute("listCivilite", listCivilite);
        model.addAttribute("listAgencies", listAgencies);

        return NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR;
    }

    /**
     * Saving a collaborator
     *
     * @param request             the request
     * @param model               the model
     * @param collaboratorAsJson the collaborator to save
     * @param ra                  the ra
     * @return a redirection towards the collaborators list page
     * @throws Exception the exception
     */
    @PostMapping("saveCollaborateur")
    public String saveCollaborateur(HttpServletRequest request, Model model, CollaboratorAsJson collaboratorAsJson,
                                    RedirectAttributes ra)
            throws Exception {

        init(model);
        String leType = "";

        if (null != collaboratorAsJson) {
            collaboratorAsJson.setEnabled(true); // The collaborator is necessarily activated during the creation process

            String exclusAddviseInt = request.getParameter("exclus_addvise");
            collaboratorAsJson.setExclusAddvise(Objects.equals(exclusAddviseInt, "on"));
            if (collaboratorService.checkEmailExist(collaboratorAsJson.getMail()) && collaboratorAsJson.getId() == null) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "l'email existe déjà en base");
            } else {
                String res = collaboratorService.updateOrCreateCollaborator(collaboratorAsJson);
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, res);
            }
            // Checks whether a collaborator or a subcontractor has been created
            if (collaboratorAsJson.getRaisonSociale() != null && collaboratorAsJson.getStatut().getId() == 7)
                leType = "?type=idTableContainerSS";
            else
                leType = "?type=idTableContainer";
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS + leType;
    }

    /**
     * To delete an existing collaborator or subcontractor we just deactivate the account but we suppress all the related events,
     * absences, travels and we delete the email address and phone number.
     *
     * To reactivate the collaborator or subcontractor, return a response body for AJAX treatment
     *
     * @param workerId  the id of the collaborator to delete
     * @param request   the request
     * @param model     the model
     * @param ra        the ra
     * @return a redirection towards the collaborators list page
     * @throws Exception Exception
     */
    @GetMapping("changeActivationStatusWorkers/{workerId}")
    public String changeActivationStatusWorkers(@PathVariable("workerId") Long workerId, HttpServletRequest request,
                                       HttpServletResponse response,
                                       Model model,
                                       RedirectAttributes ra) throws Exception {
        init(request, model);

        // Disable workers treatment
        if(!request.getParameter("type").isEmpty()) {
            if (request.getParameter(PARAMETER_IDCOLLAB) == null && request.getParameter("idCollabSS") == null) {
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS + "?type=" + request.getAttribute("type");
            }

            Collaborator tmpCollaborateur = collaboratorService.get(workerId);
            if (tmpCollaborateur != null) {

                String retourCollab = "";
                String retourDelete = "";

                if (request.getParameter("dteSortie") != null) {
                    Date dateSortie = DateUtil.parseYYYYMMDDDate(String.valueOf(request.getParameter("dteSortie")));
                    tmpCollaborateur.setDateSortie(dateSortie);
                }

                retourDelete = collaboratorService.deleteCollaborator(tmpCollaborateur);
                if (retourDelete.contains("succès")) {

                    CollaboratorAsJson collabJson = tmpCollaborateur.toJson();
                    collabJson.setEnabled(false);
                    collabJson.setTelephone("");
                    collabJson.setManager(null);

                    try {
                        retourCollab = collaboratorService.updateOrCreateCollaborator(collabJson);
                    } catch (Exception e) {
                        logger.error("[save collaborator] collab", e);
                    }
                    if ("1".equals(retourCollab)) {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retourDelete);

                        mailService.sendMailDesactivationCollab(tmpCollaborateur);
                    } else {
                        ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à  jour du collaborateur.");
                    }
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, retourDelete);
                }
            } else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Collaborator introuvable");
            }

            String typeOfTableContainer = request.getParameter("type");

            // Return the view
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS + "?type=" + typeOfTableContainer;
        }
        // Reactivate a collaborator or subcontractor treatment
        else {
            response.setContentType(Constantes.APPLICATION_JSON);
            PrintWriter out =  response.getWriter();
            // Return the result of update
            out.write(String.valueOf(collaboratorService.changeCollaboratorActivation(workerId)));
            return null;
        }
    }

    // ---- GESTION DES MISSIONS ----//

    /**
     * Check if there is already a mission with this name
     *
     * @param request        the request
     * @param session        the current session
     * @param idCollaborator collaborator
     * @param nomMission     checked name of the mission
     * @return boolean
     * TODO faire un MissionController pour gérer les opérations sur les missions!
     */
    @PostMapping("/{idCollaborator}/nomMissionExist/{nomMission}")
    @ResponseBody
    public Boolean nomMissionExist(HttpServletRequest request, HttpSession session,
                                   @PathVariable("idCollaborator") Long idCollaborator,
                                   @PathVariable("nomMission") String nomMission) {

        // Retrieving the collaborator to check
        Collaborator collaboratorToEdit = collaboratorService.get(idCollaborator);
        // Checking inside the DB if there is already a mission with the same name
        Mission mission = missionService.findByNameForCollab(nomMission, collaboratorToEdit);

        // If there is already a mission with the same name, the method returns true
        if (mission != null && mission.getCollaborateur().getId().equals(idCollaborator)) {
            return true;
        }
        return false;
    }

    /**
     * During a new mission creation, add bundle (forfait) to the bundle temporary list linked to the creation of a new mission
     *
     * @param forfaitCode    the bundle code (LIB,TCM, REP, DEP, TEL,LOG)
     * @param label          the bundle caption
     * @param amount         the bundle amount if it's exist
     * @param customerId     customer id
     * @param collaboratorId current collaborator's id
     * @return redirection to another page
     */
    @RequestMapping(value = {"addForfait/{idCollaborateur}/{forfaitCode}/{montant}/{idClient}/{libelle}"},
            method = {RequestMethod.POST, RequestMethod.GET})
    public String addForfait(@PathVariable("forfaitCode") String forfaitCode,
                             @PathVariable("libelle") String label,
                             @PathVariable("montant") String amount,
                             @PathVariable("idClient") Long customerId,
                             @PathVariable("idCollaborateur") Long collaboratorId) {

        List<LinkMissionForfaitAsJson> tempListLIB = forfaitService.getListForfaitsLIBToAdd(collaboratorId);
        List<LinkMissionForfaitAsJson> tempListNonLIB = forfaitService.getListForfaitsNonLIBToAdd(collaboratorId);

        LinkMissionForfaitAsJson linkMissionForfaitAsJson = new LinkMissionForfaitAsJson();
        Forfait forfaitToAdd = forfaitService.findForfaitByCode(forfaitCode);

        // Differentiation between the two kinds of packages
        if (forfaitCode.contains(Forfait.CODE_LIBRE)) {
            linkMissionForfaitAsJson.setMontant(0f); // montant nul
            linkMissionForfaitAsJson.setForfait(forfaitToAdd.toJson());
            linkMissionForfaitAsJson.setLibelle(label);
            tempListLIB.add(linkMissionForfaitAsJson);
        } else {
            linkMissionForfaitAsJson.setMontant(Float.valueOf(amount));
            linkMissionForfaitAsJson.setForfait(forfaitToAdd.toJson());
            linkMissionForfaitAsJson.setLibelle("");
            tempListNonLIB.add(linkMissionForfaitAsJson);
        }
        forfaitService.setListForfaitsLIBToAdd(collaboratorId, tempListLIB);
        forfaitService.setListForfaitsNonLIBToAdd(collaboratorId, tempListNonLIB);

        Collaborator currentCollaborator = collaboratorService.get(collaboratorId);
        return REDIRECT + NAV_ADMIN + "/editCollaborateurForfait/" + currentCollaborator.getId()
                + NAV_ADMIN_EDITMISSION + "/true/" + customerId;
    }

    /**
     * During a new mission creation, delete bundle (forfait) from temporary list
     *
     * @param session         the current session
     * @param request         the request
     * @param model           the model
     * @param ra              to hide the redirection url parameters
     * @param forfaitCode     the bundle code (LIB,TCM, REP, DEP, TEL,LOG)
     * @param montant         the bundle amount if it's exist
     * @param idClient        customer id
     * @param idCollaborateur collaborator id
     * @return redirection to another page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"removeForfait/{idCollaborateur}/{forfaitCode}/{montant}/{idClient}"},
            method = {RequestMethod.POST, RequestMethod.GET})
    public String removeForfait(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                                @PathVariable("forfaitCode") String forfaitCode,
                                @PathVariable("montant") String montant,
                                @PathVariable("idClient") Long idClient,
                                @PathVariable("idCollaborateur") Long idCollaborateur) throws JsonProcessingException {
        init(request, model);

        List<LinkMissionForfaitAsJson> listForfaitsNonLib = forfaitService.getListForfaitsNonLIBToAdd(idCollaborateur);
        List<LinkMissionForfaitAsJson> listForfaitsLib = forfaitService.getListForfaitsLIBToAdd(idCollaborateur);
        float amount = Float.valueOf(montant);

        if(amount == 0.0){
            forfaitService.removeLibForfait(listForfaitsLib, forfaitCode);
        }else{
            forfaitService.removeNonLibForfait(listForfaitsNonLib, forfaitCode, amount);
        }

        Collaborator currentCollaborator = collaboratorService.get(idCollaborateur);
        return REDIRECT + NAV_ADMIN + "/editCollaborateurForfait/" + currentCollaborator.getId() + NAV_ADMIN_EDITMISSION
                + "/true/" + idClient;
    }

    // ---- GESTION DES FORFAITS ----//

    /**
     * Add a new package (forfait)
     *
     * @param session  the session
     * @param request  the request
     * @param model    the model
     * @param ra       to hide the redirection url parameters
     * @param idMiss   mission's id
     * @param idCollab id of the current collaborator
     * @return return the bundle edition page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(
            value = {"editCollaborateur/{idCollaborateur}/editMission/{idMission}/editForfait"},
            method = {RequestMethod.GET, RequestMethod.POST})
    public String newForfait(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                             @PathVariable("idMission") Long idMiss,
                             @PathVariable("idCollaborateur") Long idCollab) throws JsonProcessingException {
        init(request, model);
        Collaborator currentCollaborator = collaboratorService.get(idCollab);

        if (null != currentCollaborator && null != idMiss) {

            Mission mission = missionService.get(idMiss);
            if (linkEvenementTimesheetService.isRaValideWithMission(mission, idCollab)) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Vous ne pouvez pas accéder à cette page");
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS;
            }

            if (null != mission && mission.getCollaborateur().getId().equals(currentCollaborator.getId())) {

                MissionAsJson tmpMissionAsJson = mission.toJson();
                LinkMissionForfaitAsJson linkMissionForfaitAsJson = new LinkMissionForfaitAsJson();
                tmpMissionAsJson.setListMissionForfait(mission.getListMissionForfaits());
                tmpMissionAsJson.setCommandes(mission.getCommandes());

                session.setAttribute(PROP_MISSION_TO_EDIT, tmpMissionAsJson);
                model.addAttribute(VALUE_MISSION, tmpMissionAsJson);
                model.addAttribute(VALUE_COLLABORATOR, currentCollaborator);
                model.addAttribute("linkMissionForfait", linkMissionForfaitAsJson);
                model.addAttribute(VALUE_LISTFORFAITS, forfaitService.getAllForfaitsAsJson());

                return NAV_ADMIN + NAV_ADMIN_EDITLINKFORFAIT;
            }
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
    }

    /**
     * To save a package (forfait)
     *
     * @param session                  the session
     * @param request                  the request
     * @param model                    the model
     * @param linkMissionforfaitAsJson mission linked to the bundle
     * @param ra                       to hide the redirection url parameters
     * @param idCollab                 id of the current collaborator
     * @return redirection to the mission's edition pge
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(
            value = {"/{idCollaborateur}/saveForfait", "/{idCollaborateur}/saveForfait/"},
            method = {RequestMethod.POST, RequestMethod.GET})
    public String saveForfait(HttpSession session, HttpServletRequest request, Model model,
                              LinkMissionForfaitAsJson linkMissionforfaitAsJson, RedirectAttributes ra,
                              @PathVariable("idCollaborateur") Long idCollab) throws JsonProcessingException {
        init(request, model);

        String retour;
        String retourPdf = "";
        Collaborator collaboratorWithMission = collaboratorService.get(idCollab);
        MissionAsJson missionToSession = (MissionAsJson) session.getAttribute(PROP_MISSION_TO_EDIT);
        if (missionToSession.getClient() != null && missionToSession.getClient().getId() == null) {
            missionToSession.setClient(null);
        }
        if (missionToSession.getResponsableClient() != null && missionToSession.getResponsableClient().getId() == null) {
            missionToSession.setResponsableClient(null);
        }

        List<LinkMissionForfaitAsJson> listForfait = missionToSession.getListMissionForfait();

        if (null != linkMissionforfaitAsJson) {

            retour = linkMissionforfaitService.addLinkMissionForfait(missionToSession, linkMissionforfaitAsJson);
            listForfait.add(linkMissionforfaitAsJson);
            missionToSession.setListMissionForfaitAsJson(listForfait);

            session.setAttribute(PROP_MISSION_TO_EDIT, missionToSession);

            if (retour.equals("ok")) {
                try {
                    Collaborator collab = collaboratorService.get(collaboratorWithMission.getId());
                    missionService.updateEtatMission(missionToSession.getId(), Etat.ETAT_REFUSE);
                    missionToSession.setEtat(new EtatAsJson(etatService.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE)));
                    retourPdf = pdfBuilderODM.createPDFODM(collab, missionToSession);
                } catch (Exception e) {
                    logger.error("[save forfait]", e);
                }
                if (retourPdf.contains(VALUE_ERROR)) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour de l'ordre de mission");
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, "2");
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + "/"
                            + missionToSession.getId();
                }
            } else {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "erreur dans la création du forfait");
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + "/"
                        + missionToSession.getId();
            }
        } else {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "erreur dans la création du forfait");
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
        }
    }

    /**
     * During a mission edition, delete a bundle link to a mission
     *
     * @param session         the current session
     * @param request         the request
     * @param model           the model
     * @param ra              to hide the redirection url parameters
     * @param idForfait       the bundlle id
     * @param idMission       the id mission
     * @param idCollaborateur id of the current collaborator
     * @return Redirection to the mission's edition page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = {"editCollaborateur/{idCollaborateur}/editMission/{idMission}/deleteForfait/{idForfait}"}, method = {RequestMethod.POST,
            RequestMethod.GET})
    public String deleteForfait(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                                @PathVariable("idForfait") Long idForfait,
                                @PathVariable("idMission") Long idMission,
                                @PathVariable("idCollaborateur") Long idCollaborateur) throws JsonProcessingException {
        init(request, model);

        String retourPdf = "";
        Collaborator collaboratorWithMission = collaboratorService.get(idCollaborateur);
        MissionAsJson missionToSession = (MissionAsJson) session.getAttribute(PROP_MISSION_TO_EDIT);
        List<LinkMissionForfaitAsJson> listForfait = missionToSession.getListMissionForfait();

        if (missionToSession.getClient() != null && missionToSession.getClient().getId() == null) {
            missionToSession.setClient(null);
        }
        if (missionToSession.getResponsableClient() != null && missionToSession.getResponsableClient().getId() == null) {
            missionToSession.setResponsableClient(null);
        }
        Mission mission = missionService.get(idMission);
        if (linkEvenementTimesheetService.isRaValideWithMission(mission, idCollaborateur)) {
            ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Vous ne pouvez pas accéder à cette page");
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_TRAVAILLEURS;
        }
        if (null != missionToSession && null != collaboratorWithMission && null != idForfait) {
            for (LinkMissionForfaitAsJson linkforfait : listForfait) {
                if (linkforfait.getId().equals(idForfait)) {
                    listForfait.remove(linkforfait);
                    break;
                }
            }
            missionToSession.setListMissionForfaitAsJson(listForfait);
            linkMissionforfaitService.deleteLinkMissionForfait(idForfait);
            session.setAttribute(PROP_MISSION_TO_EDIT, missionToSession);

            try {
                Collaborator collab = collaboratorService.get(collaboratorWithMission.getId());
                missionService.updateEtatMission(missionToSession.getId(), Etat.ETAT_REFUSE);
                missionToSession.setEtat(new EtatAsJson(etatService.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE)));
                retourPdf = pdfBuilderODM.createPDFODM(collab, missionToSession);
            } catch (Exception e) {
                logger.error("[delete forfait]", e);
            }

            if (retourPdf.contains(VALUE_ERROR)) {
                ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Erreur dans la mise à jour de l'ordre de mission");
                return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + "/"
                        + missionToSession.getId();
            }
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITCOLLABORATEUR + "/" + collaboratorWithMission.getId() + NAV_ADMIN_EDITMISSION + "/"
                    + missionToSession.getId();
        } else {
            return REDIRECT + NAV_ADMIN + NAV_ADMIN_COLLABORATEURS;
        }
    }

    /**
     * Deletes a mission
     *
     * @param session         current session
     * @param request         the request
     * @param model           the model
     * @param ra              to hide the redirection url parameters
     * @param id              the id of mission we want to delete
     * @param idCollaborateur id of the current collaborator
     * @return redirection to the edition collaborator page
     * @throws JsonProcessingException the json processing exception
     */
    @RequestMapping(value = "deleteMission/{idMission}/{idCollaborateur}", method = {RequestMethod.GET, RequestMethod.POST})
    public String deleteMission(HttpSession session, HttpServletRequest request, Model model, RedirectAttributes ra,
                                @PathVariable("idMission") Long id,
                                @PathVariable("idCollaborateur") Long idCollaborateur) throws JsonProcessingException {
        init(request, model);

        String retour;
        Collaborator collaboratorWithMission = collaboratorService.get(idCollaborateur);

        String typeTravailleur = "/collaborateur/";
        if (collaboratorWithMission.getStatut().getId().equals(7L))
            typeTravailleur = "/sous_traitant/";

        if (null != id) {
            Mission tmpMission = missionService.get(id);
            if (null != tmpMission) {
                DateTime pDateDebut = new DateTime();
                DateTime dateOFFdebut = pDateDebut.dayOfMonth().withMinimumValue();
                DateTime dateOFFfin = pDateDebut.dayOfMonth().withMaximumValue();
                if (!factureService.findByMissionBetweenDates(tmpMission, dateOFFdebut, dateOFFfin).isEmpty()) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, "Impossible de supprimer cette ordre de mission car des factures liées à cette mission existent." +
                            " Veuillez supprimer ces factures avant de supprimer l'ordre de mission.");
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR + typeTravailleur + collaboratorWithMission.getId() + NULL;
                }
                retour = missionService.deleteMission(tmpMission);
                if (retour.contains("succès")) {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_SUCCESS, retour);
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR + typeTravailleur + collaboratorWithMission.getId() + NULL;
                } else {
                    ra.addFlashAttribute(FLASHATTRIBUTE_KEY_ERROR, retour);
                    return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR + typeTravailleur + collaboratorWithMission.getId() + NULL;
                }
            }
        }
        return REDIRECT + NAV_ADMIN + NAV_ADMIN_EDITTRAVAILLEUR;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Downloads an XLSX file with any active subcontractor or collaborator
     *
     * @param type    :    The user's type
     * @param session session
     * @param request request
     * @param model   model
     * @param ra      ra
     * @return : The response entity with the Excel's file bytes
     * @throws IOException : if the service's function encounter a problem during the writing process in the BAOStream
     */
    @GetMapping("exportConsultants/{type}")
    @ResponseBody
    public ResponseEntity<ByteArrayResource> exportConsultants(
            @PathVariable("type") String type,
            HttpSession session,
            HttpServletRequest request,
            Model model,
            RedirectAttributes ra) throws IOException {

        init(model);

        Map<String, String> varToStatus = new HashMap<>();
        varToStatus.put("collab", STATUT_COLLABORATEUR);
        varToStatus.put("stt", StatutCollaborateur.STATUT_SOUS_TRAITANT);

        if (!varToStatus.containsKey(type)) {
            return ResponseEntity.badRequest().body(new ByteArrayResource(new byte[]{0}));
        }

        class CollabTableEntry implements DocumentExcelService.TableEntry {

            private final Collaborator collab;

            private CollabTableEntry(Collaborator collab) {
                this.collab = collab;
            }

            @Override
            public List<String> getColumnValues() {
                List<String> columns = new ArrayList<>();
                columns.add(collab.getNom());
                columns.add(collab.getPrenom());
                columns.add(collab.getMail());
                return columns;
            }
        }

        class SttTableEntry implements DocumentExcelService.TableEntry {

            private final Collaborator collab;

            private SttTableEntry(Collaborator collab) {
                this.collab = collab;
            }

            @Override
            public List<String> getColumnValues() {
                List<String> columns = new ArrayList<>();
                columns.add(collab.getNom());
                columns.add(collab.getPrenom());
                columns.add(collab.getMail());
                return columns;
            }
        }

        String statutChoisi = varToStatus.get(type);

        List<StatutCollaborateur> collaboratorsStatus = statutCollaborateurService.findCollaboratorsStatusByCodes(
                STATUT_ADMIN,
                STATUT_CHEF_DE_PROJET,
                STATUT_COLLABORATEUR,
                STATUT_DIRECTION,
                STATUT_DRH,
                STATUT_MANAGER,
                STATUT_RESPONSABLE_TECHNIQUE
        );

        List<Collaborator> listCollabs = collaboratorService.findCollaboratorsByStatus(
                collaboratorsStatus.toArray(new StatutCollaborateur[0])
        );

        byte[] data = null;
        String titreFichier = null;
        List<DocumentExcelService.TableEntry> lEntries;
        switch (statutChoisi) {
            case STATUT_COLLABORATEUR:
                lEntries = listCollabs.stream()
                        .map(c -> new CollabTableEntry(c))
                        .collect(Collectors.toList());
                titreFichier = "export_collaborateurs.xlsx";
                data = documentExcelService.createExcelFromTableEntries(
                        "Liste des collaborateurs actifs",
                        Arrays.asList("Nom", "Prénom", "Mail Pro."),
                        lEntries
                );
                break;
            case StatutCollaborateur.STATUT_SOUS_TRAITANT:
                lEntries = listCollabs.stream()
                        .map(c -> new SttTableEntry(c))
                        .collect(Collectors.toList());
                titreFichier = "export_sous_traitants.xlsx";
                data = documentExcelService.createExcelFromTableEntries(
                        "Liste des sous-traitants actifs",
                        Arrays.asList("Nom", "Prénom", "Mail Pro."),
                        lEntries
                );
                break;
            default:
                break;
        }
        if (data == null) {
            throw new NullPointerException("Le fichier est vide");
        }

        ByteArrayResource resource = new ByteArrayResource(data);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + titreFichier)
                .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(data.length)
                .body(resource);
    }

}