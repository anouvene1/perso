/*
 ~ ©Amiltone 2017
 */
$(document).ready(function () {
    fixTableHeaders();
    datepickerTri();

    manageTrisAndFilters();

    manageSessions();
    countCollab();

    $(".ft_scroller").scrollLeft($("#table-addvise-gestion").width()); //scroll to right
    $(".tooltip-entree").tooltip();
});

function fixTableHeaders() {

    var arrColModal = [{width: 320, align: 'center'}];

    //Pour l'historique
    $(".session-hist").each(function () {
        arrColModal.push({width: 200, align: 'center'});
    });
    if ($(".last-session-addvise-hist".length)) {
        arrColModal.push({width: 150, align: 'center'});
    }

    $("#table-addvise-gestion").fxdHdrCol({
        fixedCols: 1,
        width: "100%",
        height: "80%",
        colModal: arrColModal
    });
    $(".cell-addvise-1").css("width", "350px");
    $(".cell-addvise-2").css("width", "200px");
    $(".ft_cwrapper").css("height", "auto");
}

// Traitements aynchrones : nouveau module (M10...) et nouveau formateur

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function manageTrisAndFilters() {
    var trier = getUrlParameter("trier");
    var annulees = getUrlParameter("annulees");
    var chercher = getUrlParameter("chercher");
    var filtrer = getUrlParameter("filtrer");
    var dateDebut = getUrlParameter("date-debut");
    var dateFin = getUrlParameter("date-fin");

    if (annulees == "true") {
        $("#addvise-annulees").prop("checked", true);
    }
    if (chercher) {
        $("#addvise-chercher").val(chercher);
    }
    if (dateDebut) {
        createDatePicker($("#datepicker01"), dateDebut.replace('%2F', '/'));
    }
    if (dateFin) {
        createDatePicker($("#datepicker02"), dateFin.replace('%2F', '/'));
    }
    $("#addvise-filtrer").val(filtrer);

    if (!trier || trier == "date-entree") {
        $("#addvise-tri-date-entree").css("opacity", 1);
    } else if (trier == "alpha") {
        $("#addvise-tri-alpha").css("opacity", 1);
    } else { //dernier module
        $("#addvise-tri-date-dernier-module").css("opacity", 1);
    }

    var refresh = function () {
        //Redirection avec le nouvel url
        var url;
        if (window.location.href.indexOf("?") >= 0) {
            url = window.location.href.substr(0, window.location.href.indexOf('?') + 1);
        } else {
            url = window.location.href + "?";
        }

        if (trier) {
            url += "trier=" + trier + "&";
        }
        if (annulees) {
            url += "annulees=" + annulees + "&";
        }
        if (chercher) {
            url += "chercher=" + chercher + "&";
        }
        if (dateDebut) {
            url += "date-debut=" + dateDebut + "&";
        }
        if (dateDebut) {
            url += "date-fin=" + dateFin + "&";
        }
        if (filtrer) {
            url += "filtrer=" + filtrer;
        } else {
            url = url.slice(0, -1); //suppression du dernier & inutile
        }

        window.location.href = url;
    };

    $("#addvise-tri-alpha").click(function () {
        if (trier != "alpha") {
            trier = "alpha";
            refresh();
        }
    });

    $("#addvise-tri-date-entree").click(function () {
        if (trier != "date-entree") {
            trier = "date-entree";
            refresh();
        }
    });

    $("#addvise-tri-date-dernier-module").click(function () {
        if (trier != "date-dernier-module") {
            trier = "date-dernier-module";
            refresh();
        }
    });

    $("#addvise-filtrer").bind('change', function () {
        filtrer = $("#addvise-filtrer").val();
        refresh();
    });

    $("#addvise-annulees").bind('change', function () {
        annulees = this.checked;
        refresh();
    });

    $("#addvise-chercher").keypress(function (e) {
        if (event.keyCode == 13) { //disable ENTER
            e.preventDefault();
            var collabName = $("#addvise-chercher").val();
            if (chercher != collabName) {
                chercher = collabName;
                refresh();
            }
        }
    });

    $("#btnMonthPicker").click(function () {
        var date1 = $("#datepicker01").val();
        var date2 = $("#datepicker02").val();
        if (date1 != dateDebut || date2 != dateFin) {
            dateDebut = date1.replace('/', '%2F');
            dateFin = date2.replace('/', '%2F');
            refresh();
        }
    });
}

function manageSessions() {
    $(".btn-restore-session-hist").click(function () {
        var btn = $(this);
        $("#modale-restore-session").modal({backdrop: 'static'});

        $("#btn-modale-restore-session").click(function () {
            btn.closest(".form-restore-session").submit();
        });
    });
}

function countCollab() {
    $(".session-hist").each(function (i, el) {
        var idSession = $(el).find("input[name='idSession']").val();
        var count = $(".invitation-addvise-hist.session-id-"+idSession).find(".icone_2").length / 2;
        $(el).find(".countConfirmSpan").text(count);
    });
}

function datepickerTri() {
    var actualDate = new Date();
    var today = formatDateStr(actualDate);
    actualDate.setMonth(actualDate.getMonth() - 3);
    var previous = formatDateStr(actualDate);

    createDatePicker($("#datepicker01"), previous);
    createDatePicker($("#datepicker02"), today);
}

function formatDateStr(myDate) {
    var month = myDate.getMonth()+1;
    var day = myDate.getDate();
    return (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + myDate.getFullYear();
}

var createDatePicker = function (myDatePicker, myDateStr) {
    $(myDatePicker).datepicker({
        format: 'dd/mm/yyyy',
        defaultDate: myDateStr,
        changeMonth: true,
        changeYear: true
    });
    $(myDatePicker).val(myDateStr);
};
