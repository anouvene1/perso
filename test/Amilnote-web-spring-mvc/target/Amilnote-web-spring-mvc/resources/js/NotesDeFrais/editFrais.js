/*
 * ©Amiltone 2017
 */

$(document).ready(function () {
// [.change] Une fonction de JQuery à exécuter chaque fois que l'événement est déclenché.
    $("select[name='typeFrais']").change(function () {
        $("#caracFraisGroup1").fadeOut();
        $("#caracFraisGroup2").fadeOut();
        $("#caracFraisGroup3").fadeOut();
        var selected = $("select[name='typeFrais']").val();
        //--- Test des ID des différents frais
        if (selected == "1" || selected == "5" || selected == "6" || selected == "7" || selected == "8" || selected == "15") {
// [.fadeIn();] Fonction pour afficher le contenu du champ Compétence
// [.fadeOut();] Fonction pour cacher le contenu du champ Compétence
            $("#caracFraisGroup1").fadeIn();
        }
        //--- Element à afficher pour le frais Voiture
        if (selected == "1") {
            $("#caracFraisGroup2").fadeIn();
        }
        //--- Element(s) à afficher pour le frais Hotel
        if (selected == "12") {
            $("#caracFraisGroup3").fadeIn();
        }
    });

    $('#btnModalValidFrais').click(function () {

        var $modalValidFrais = $('#modalValidFrais');

        var lModalValidFrais = new modalValidFrais($modalValidFrais);
        lModalValidFrais.init();

    });
});
