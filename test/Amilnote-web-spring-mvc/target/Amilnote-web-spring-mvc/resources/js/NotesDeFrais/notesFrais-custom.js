/*
 * ©Amiltone 2017
 */

//variables globales
//map json récupérées du controller
var mapForfaitsMissions;
var mapFraisMissions;
var mapHistoFraisMissions;
var mapTypeFrais;
var JsonListMissionsEventsFrais;
//tableau pour stocker les noms des mission liées a l'utilisateur courant
var tabMission = [];

//jours travaillés mission
var workedDays = [];

//lignes de frais a supprimer
var listRowsToDelete = [];


//liste des jours mission
var listDatesMissions;
//map json des totaux par mission
var mapTotauxMissions = [];

//mission sélectionnée dans la liste
var currentMission;

//constantes
var constantAccordion1, constantAccordion2;


$(document).ready(function () {
    Array.prototype.clone = function () {
        return this.slice(0);
    };

    var $tabFiltreMission = $("#idTabFiltreMission");
    var $tabFiltreEtat = $("#idTabFiltreEtat");
    var $tabFrais = $("#tableauFrais");


    initTableauFiltreMission($tabFiltreMission, JsonListMissionsEventsFrais);
    initTableauFrais($tabFrais, getListFrais(JsonListMissionsEventsFrais));

    $tabFiltreMission.on('check.bs.table', function (e, row) {
        if (null != row.id) {
            $tabFrais.bootstrapTable("filterBy", {"mission": row.mission});
        } else {
            $tabFrais.bootstrapTable("filterBy", "");
        }
    });

});

function initTableauFiltreMission($table, listMission) {

    var lListMission = listMission.slice(0);
    $table.bootstrapTable({
        data: lListMission,
        cache: false,
        clickToSelect: true,
        height: 240,
        showHeader: false,
        singleSelect: true,
        columns: [{
            field: 'mission',
            title: 'Nom de la mission',
            align: 'center',
            valign: 'middle',
        }, {
            field: 'state',
            align: 'right',
            radio: true,
        }]

    });
    $table.bootstrapTable('insertRow', {
        index: 9999,
        row: {"mission": "Toutes les missions"}
    });

}
function getListFrais(pJsonListMissionsEventsFrais) {

    var listFrais = [];
    $.each(pJsonListMissionsEventsFrais, function (i, miss) {

        if (null != miss.listLinkEvenementTimesheetAsJson) {
            $.each(miss.listLinkEvenementTimesheetAsJson, function (j, event) {

                if (null != event.listFraisAsJson) {
                    $.each(event.listFraisAsJson, function (k, frais) {

                        frais["mission"] = miss.mission;
                        frais["date"] = event.date;
                        frais["momentJournee"] = event.momentJournee;

                        listFrais.push(frais);

                    });
                }

            });
        }

    });
    return listFrais;
}
function initTableauFrais($table, pJsonListMissionsEventsFrais) {


    $table.bootstrapTable({
        data: pJsonListMissionsEventsFrais,
        cache: false,
        striped: true,
        clickToSelect: true,
        height: 400,
        sortName: 'dateEvenement',
        sortOrder: 'desc',
        toolbar: 'custom-toolbar',
        search: 'tue',
        columns: [{
            field: 'state',
            checkbox: true
        }, {
            field: 'mission',
            title: 'Mission',
            align: 'center',
            valign: 'middle',
            sortable: true,
        }, {
            field: 'date',
            title: 'Date',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter: function (value, row, index) {
                var lDate = moment(value).local().format("DD/MM/YYYY");
                return lDate;

            }
        }, {
            field: 'momentJournee',
            title: 'Matin/Apres-midi',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter: function (value, row, index) {
                if (value == 0) {
                    return "Matin";
                } else {
                    return "Après-midi";
                }
            }
        }, {
            field: "listLinkFraisTFCAsJson",
            title: 'Type',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter: function (value, row, index) {
                if (null != value[0]) {
                    if (null != value[0].typeFraisCarac) {
                        return value[0].typeFraisCarac.typeFrais.typeFrais;
                    }
                }
            }
        }, {
            field: "montant",
            title: 'Montant',
            align: 'center',
            valign: 'middle',
            sortable: true,
        }, {
            field: "hasJustif",
            title: 'Justif',
            align: 'center',
            valign: 'middle',
            sortable: true,
        }, {
            field: "etat",
            title: 'Etat',
            align: 'center',
            valign: 'middle',
            sortable: true,
            formatter: function (value, row, index) {
                if (null != value)
                    return value.etat;
            }
        }, {
            field: "action",
            title: 'Action',
            align: 'center',
            valign: 'middle',
            clickToSelect: false,
            formatter: function (value, row, index) {
                return [
                    '<a class="edit ml10" href="javascript:void(0)" title="Detail">',
                    '<i class="glyphicon glyphicon-search"></i>',
                    '</a>'
                ].join('');
            },
            events: {
                'click .edit': function (e, value, row, index) {
                    var modalEdit = new ModalEditNotesFrais($("#form-depense"), JsonListMissionsEventsFrais, row);
                    modalEdit.init();

                }
            }

        }]

    });
}
	
	

	
