/*
 * ©Amiltone 2017
 */

// List group mission: Toggle glyphicon-eye-open / glyphicon-eye-close
$(function(){
    // glyphicon-eye-open
    $("#container").on("shown.bs.collapse", ".collapse", function () {
        var $li = $(this).closest("li.list-group-item");
        if($(this).hasClass("in")) {
            $li.find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }
    });

    /// glyphicon-eye-close
    $("#container").on("hidden.bs.collapse", ".collapse", function () {
        var $li = $(this).closest("li.list-group-item");
        $li.find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
    });
});

$(function () {
    var WITHFERIE = false;
    var WITHSATURDAY = pWithSaturdayFromController;
    var URLWITHSATURDAY = "Rapport-Activites/mesTimeSheetsWithSaturday/";
    var MOISCHOISI = moisChoisisController;
    var avecFrais = "oui";
    var $modalLoading = $("#idModalLoading");

    $('#idWithSaturday').prop('checked', WITHSATURDAY);
    $('#idCheckBoxEditHoliday').change(function (e) {
        WITHFERIE = $(this).is(":checked");
    });

    // Récupérée depuis l'attribut du model (voir mesTimesheets.jsp)
    // - Correspond à l'historique maximum qu'on veut pouvoir afficher sur le
    // calendrier
    var lPeriodHistoMaxTimeSheetAsMonth = pPeriodHistoMaxTimeSheetAsMonth;
    var lMinimumMonthViewOnFullCalendar = new moment().subtract(
        lPeriodHistoMaxTimeSheetAsMonth, 'months');

    // pWithSaturday = Récupérée depuis l'attribut du model (voir
    // mesTimesheets.jsp)
    // - Correspond à la présence ou non de Samedi dans les évenements, avec une
    // date supérieur à now() - lPeriodHistoMaxTimeSheetAsMonth
    // - créer un paramètre pour afficher ou non le samedi dans le calendrier
    // (le dimanche n'est jamais affiché)
    var hiddenDaysForFullCalendar = (pWithSaturdayFromController ? [0] : [0, 6]);

    //A chaque changement d'état de la checkbox d'affichage des samedis, on redirige 
    //vers la même url que l'on concatène avec la valeur booléenne inverse 
    //de la variable WITHSATURDAY.
    $('#idWithSaturday').change(function (e) {
        MOISCHOISI = $('#calendar').fullCalendar('getDate');
        $(location).attr('href', URLWITHSATURDAY + (WITHSATURDAY ? "false" : "true") + "/" + MOISCHOISI);
    });

    var $divWithSaturday = $('#idWithSaturday');
    $divWithSaturday.attr('');
    var lDisabled = true;
    var $btnSoumissionRA = $("#btn-sendRA");
    var $btnRASansFrais = $("#btn-RASansFrais");
    var $btnValiderRA = $("#sending-RA");
    var btnGroupChoixMission = $('.btnGroup-choixMission');

    // on récupère la liste des absences
    var listTypeAbsence = getListTypeAbsence();

    // On récupère la liste des missions
    var listMission = getListMissions();
    var listEtatsAbsence = getEtatsAbsence();
    var lColorsEtatAbsence = new Colors(getColorsList("absenceEtat"));

    // Initialisation du panneau des absences
    var lPanelAbsence = new PanelAbsences($("#listGroupTypeAbsence"), lColorsEtatAbsence);

    // construction du panneau des absences ( Accepte des listes en paramètre :
    // ["mes Absences"] = une liste d'1 item )
    lPanelAbsence.buildList(listEtatsAbsence);

    // On instancie un objet couleur et on lui donne la palette de couleur
    // "Mission" de getColorsList
    var lColorsMission = new Colors(getColorsList("mission"));

    // Pour chaque mission dans liste mission : lui associer une couleur issue
    // de la palette getColorsList("Mission")
    // - récuperer la couleur en faisant lColorsMission.getValue( [nom de la
    // mission])
    lColorsMission.addList(listMission, "mission");

    // on instancie le panel avec la liste des missions en donnant l'objet du
    // panel et la liste des couleurs
    var lPanelMission = new PanelMission($('#listGroupMission'), lColorsMission);

    // on construit le panel en lui donnant la liste des missions
    lPanelMission.buildList(listMission);

    // On instancie la fenêtre modal d'édition de mission
    var lModal = new ModalEdit($('#modalEdit'), WITHSATURDAY);

    $('#calendar').fullCalendar({
        header: {
            left: 'title',
            right: 'today prev,next'
        },
        hiddenDays: hiddenDaysForFullCalendar,
        height: 500,
        aspectRatio: 2,
        defaultView: 'month',
        lazyFetching: false,
        editable: false,
        selectable: true,
        fixedWeekCount: false,
        selectHelper: false,
        defaultTimedEventDuration: '04:00:00',
        timezone: 'local',
        // A chaque fois que les events sont rechargés (
        // fullCalendar('refetchEvents'), changements de mois,
        // ... )
        // Appelle la fonction getListEvents(start, end) avec le
        // jour min et max de la vue en cours.
        events: function (start, end, timezone, callback) {
            eventCalendar(start, end, timezone, callback);
        },

        // Chaque évenement ajouter dans fullCalendar passe ici,
        // permet de customiser la balise element.
        eventRender: function (event, element, view) {
            eventRenderCalendar(event, element, view);
        },

        eventClick: function (event) {
            eventClickCalendar(event);
        },
        select: function (start, end) { // Quand sélection d'une journée ou d'une région.
            selectCalendar(start, end);
        },

        viewRender: function (view, element) {
            viewRenderCalendar(view, element);
        },

        eventMouseover: function (event, jsEvent, view) {
            if (null != event.absence && event.absence.etat.code != "RE" && event.absence.etat.code != "AN") {
                var listElementAbsence = $('#calendar').find('[data-idAbsence =' + event.absence.id + ']');
                listElementAbsence.css({backgroundColor: getColorsList("eventSelection")});
            } else {
                var eventElement = $('#calendar').find('[data-idEvent =' + event._id + ']');
                eventElement.css({backgroundColor: getColorsList("eventSelection")});
            }
        },
        eventMouseout: function (event, jsEvent, view) {
            if (null != event.absence) {
                var listElementAbsence = $('#calendar').find('[data-idAbsence =' + event.absence.id + ']');
                listElementAbsence.css({
                    backgroundColor: lColorsEtatAbsence.getValue(event.absence.etat.code)
                });
            } else if (null != event.mission) {
                var eventElement = $('#calendar').find('[data-idEvent =' + event._id + ']');
                eventElement.css({
                    backgroundColor: lColorsMission.getValue(event.mission.mission)
                });
            } else if (null != event.ferie) {
                var eventElement = $('#calendar').find('[data-idEvent =' + event._id + ']');
                eventElement.css({
                    backgroundColor: getColorsList("férié")
                });
            } else {
                var eventElement = $('#calendar').find('[data-idEvent =' + event._id + ']');
                eventElement.css({
                    backgroundColor: getColorsList("vide")
                });
            }
        },
        // on affiche ou cache des éléments en fonction de
        // l'état de FullCalendar (isLoading = true/false)
        loading: function (isLoading, view) {
            if (isLoading == true) {
                $('#calendar').find('.fc-view-container')
                    .hide();
                $('#calendar').find('.fc-toolbar').hide();
                $('#PDFRA').hide();
            } else {
                $('#calendar').find('.fc-view-container')
                    .show();
                $('#calendar').find('.fc-toolbar').show();
                $('#PDFRA').show();
            }
        }
    });

    function eventCalendar(start, end, timezone, callback){
        var tmpDebut = moment(start);
        tmpDebut = tmpDebut.add(15, "days");

        var tmpFin = moment(end);
        tmpFin = tmpFin.subtract(15, "days");

        while (start.month() != tmpDebut.month()) {
            start.add(1, "day");
        }

        while (end.month() != tmpFin.month()) {
            end.subtract(1, "day");
        }

        var lEventsMissions = getListEvents(start, end);
        var lEventsHolidays = getListHolidays(start, end);
        lEventsHolidays = delHolidaysIfMissionInSameHalfDay(lEventsHolidays, lEventsMissions);
        var allEvents = $.merge(lEventsMissions, lEventsHolidays);

        var tmp = moment(start).add(15, "days");
        listMission = getListMissions((tmp.month() + 1), tmp.year());

        var lListMissionValidee = $.grep(listMission, function (mission) {
            const VALIDEE = "VA";
            const SOUMIS = "SO";
            const BROUILLON = "BR";
            //  var lMissionEtat = mission.etat.code;
            var retour=false;
            if(mission.etat!=null)
                retour=(mission.etat.code==VALIDEE || mission.etat.code==SOUMIS || mission.etat.code==BROUILLON);
            return retour;
        });

        // on retourne QUE les missions disponibles sur le mois en cours de visualisation
        var lListMissionDisponible = $.grep(lListMissionValidee, function (mission) {
            var lMissionStart = moment(mission.dateDebut);
            var lMissionEnd = moment(mission.dateFin).endOf('days');

            var startMonth = moment([tmp.year(), tmp.month(), 1]);
            var endMonth = moment(startMonth).endOf('month');

            return ((lMissionStart <= startMonth && lMissionEnd >= endMonth)
                || (lMissionStart.month() == startMonth.month() && lMissionStart.year() == startMonth.year())
                || (lMissionEnd.month() == endMonth.month() && lMissionEnd.year() == endMonth.year()));
        });

        lPanelMission.buildList(lListMissionDisponible);
        initListEventsWithDates(allEvents, start, end, WITHSATURDAY, moment(start).add(15, "days").month());

        var today = moment();

        for (let event of allEvents){
            event.start= moment(event.start).utc().utcOffset(moment().utcOffset()-dstInFrance(today), 'minutes');
            event.end= moment(event.end).utc().utcOffset(moment().utcOffset()-dstInFrance(today), 'minutes');
        }
        callback(allEvents);
    }

    function eventRenderCalendar(event, element, view) {
        var lColor = "";
        element.attr("data-idEvent", event._id);
        if (null == event.ferie && null == event.absence && null == event.mission) {
            // On récupère la couleur par défaut pour les jours vide
            lColor = getColorsList("vide");

        } else if (null != event.ferie) { // si l'évenement récupéré est ferié

            // On récupère la couleur par défaut pour les jours fériés
            lColor = getColorsList("férié");
            element.find('.fc-title').html(event.ferie);

        } else if (null != event.absence) {
            // On récupère la couleur qui correspond au code de son état
            lColor = lColorsEtatAbsence.getValue(event.absence.etat.code);
            // Ajout d'un data-attribut personnalisé avec l'id de l'absence
            element.attr("data-idAbsence", event.absence.id);
            if (event.absence.etat.code == "BR") {
                element.find('.fc-title').html(event.absence.typeAbsence.typeAbsence + " (Brouillon)");
            }
            else {
                element.find('.fc-title').html(event.absence.typeAbsence.typeAbsence);
            }
            element.addClass('absenceEtat' + event.absence.etat.code);

        } else if (null != event.mission) {
            // On récupère la couleur associé à cette mission
            lColor = lColorsMission.getValue(event.mission.mission);
            element.find('.fc-title').html(event.mission.typeMission.typeMission);
        }
        element.css({backgroundColor: lColor});
    }

    function eventClickCalendar(event) {
        var currentView = $('#calendar').fullCalendar('getView');
        lDisabled = getTimeSheetIsEditable(currentView.intervalStart);

        if (disabledOrNotEvent(lDisabled, $.makeArray(event))) {
            return false;
        }

        var currentViewMonth = currentView.intervalStart.month();
        if (currentViewMonth != event.start.month())
            return false;

        var array;
        var lListMissionDisponible;
        var lAbsenceId;
        var lStart, lEnd;

        if (event.absence != null && event.absence.etat.code != "RE") {

            lAbsenceId = event.absence.id;
            lStart = event.absence.dateDebut;
            lEnd = event.absence.dateFin;

            var lListMissionValidee = $.grep(listMission, function (mission) {
                const VALIDEE = "VA";
                const SOUMIS = "SO";
                const BROUILLON = "BR";

                var retour=false;
                if(mission.etat!=null)
                    retour=(mission.etat.code==VALIDEE || mission.etat.code==SOUMIS || mission.etat.code==BROUILLON);
                return retour;
            });

            lListMissionDisponible = $.grep(lListMissionValidee, function (mission) {
                var lMissionStart = moment(mission.dateDebut);

                var lMissionEnd = moment(mission.dateFin).endOf('days');

                return (lMissionStart <= event.absence.dateDebut && lMissionEnd >= event.absence.dateFin);
            });

            var listEventWithSameAbsence = $('#calendar').fullCalendar('clientEvents', function (e) {
                return (e.absence != null && e.absence.id == lAbsenceId);
            });

            array = $.makeArray(listEventWithSameAbsence);

        } else {
            // on retourne QUE les missions disponibles sur la periode selectionnée
            lStart = event.start;
            lEnd = event.end;


            var lListMissionValidee = $.grep(listMission, function (mission) {
                const VALIDEE = "VA";
                const SOUMIS = "SO";
                const BROUILLON = "BR";
                var retour=false;
                if(mission.etat!=null)
                    retour=(mission.etat.code==VALIDEE || mission.etat.code==SOUMIS || mission.etat.code==BROUILLON);
                return retour;
            });

            lListMissionDisponible = $.grep(lListMissionValidee, function (mission) {
                var lMissionStart = moment(mission.dateDebut);
                var lMissionEnd = moment(mission.dateFin).endOf('days');

                return (lMissionStart <= event.start && lMissionEnd >= event.end);
            });

            array = $.makeArray(event);
        }

        lModal.init(
            array,
            lStart,
            lEnd,
            lListMissionDisponible,
            listTypeAbsence,
            isRangeOfDay = false,
            absenceId = lAbsenceId,
            WITHFERIE
        );

        return false;
    }

    function selectCalendar(start, end) {
        var currentView = $('#calendar').fullCalendar('getView');
        var currentViewMonth = currentView.intervalStart.month();

        var today = moment();
        var lStart= moment(start).utc().utcOffset(moment().utcOffset()-dstInFrance(today), 'minutes');
        var lEnd= moment(end).utc().utcOffset(moment().utcOffset()-dstInFrance(today), 'minutes');

        if (( lStart.month() < currentViewMonth && lEnd.month() < currentViewMonth )
            || lStart.month() > currentViewMonth && lEnd.month() > currentViewMonth) {
            return false;
        }

        if (lStart.month() < currentViewMonth) {
            lStart = currentView.intervalStart;
        } else if (lStart.month() > currentViewMonth) {
            return false;
        }

        if (lEnd.month() > currentViewMonth) {
            lEnd = moment(currentView.intervalEnd.endOf('days')).subtract(1, 'days');
        }

        var listEventsSelected = $('#calendar').fullCalendar('clientEvents');
        listEventsSelected = $.grep(listEventsSelected, function (event) {
            return (event.start >= lStart && event.end <= lEnd);
        });

        lDisabled = getTimeSheetIsEditable(currentView.intervalStart);

        if (disabledOrNotEvent(lDisabled, listEventsSelected)) {
            return false;
        }

        //Récupération de la liste des absences en brouillon sélectionnées
        var listEventsSelectedAbsence = $.grep(listEventsSelected, function (event) {
            return (null != event.absence && "BR" == event.absence.etat.code );
        });

        // Récupération de la liste des absences sélectionnées (sauf les "refusées" et "commencé")
        var eventsWithAnyAbsence = $.grep(listEventsSelected, function (event){
            return (null != event.absence
                && event.absence.etat.code != "RE"
                && !(event.absence.etat.code == "CO"));
        });

        // Récupération de la liste des absences annulées sélectionnées
        var eventsWithCanceledAbsence = $.grep(listEventsSelected, function (event) {
            return (null != event.absence
                && event.absence.etat.code === "AN");
        });

        // Vérification de la présence unique d'absence (brouillon) dans la sélection
        if (( listEventsSelectedAbsence.length == listEventsSelected.length ) && listEventsSelected.length > 0) {
            showDialogAbsence("Édition impossible", "Cette absence est un brouillon : veuillez la sélectionner pour l'éditer ou la supprimer.", 1);
        }
        // Vérifie s'il y a des absences annulées parmi les demi-journées sélectionnées (les absences annulées doivent être supprimées manuellement pour être modifiées)
        else if (eventsWithCanceledAbsence.length != 0 && eventsWithCanceledAbsence.length <= listEventsSelected.length) {
            showDialogAbsence("Édition impossible", "Votre sélection contient des absences annulées. <br> Veuillez cliquer sur ces absences et les supprimer afin d'éditer ces demi-journées.")
        }
        // Vérifie s'il y a d'autres types d'absence (hors refusé) parmi les demi-journées sélectionnées
        else if (eventsWithAnyAbsence.length != 0 && eventsWithAnyAbsence.length <= listEventsSelected.length) {
            showDialogAbsence("Édition impossible", "La période sélectionnée contient des absences. <br> Pour éditer une absence, veuillez la sélectionner. <br> Pour éditer d'autres jours, veuillez sélectionner une période sans absence.");
        }
        else {
            $('#calendar').fullCalendar('clientEvents', function (e) {
                return (e.ferie != null);
            });

            // on retourne QUE les missions disponible sur la periode selectionnée
            var lListMissionValidee = $.grep(listMission, function (mission) {
                const VALIDEE = "VA";
                const SOUMIS = "SO";
                const BROUILLON = "BR";
                //  var lMissionEtat = mission.etat.code;
                var retour=false;
                if(mission.etat!=null)
                    retour=(mission.etat.code==VALIDEE || mission.etat.code==SOUMIS || mission.etat.code==BROUILLON);
                return retour;
            });
            var lListMissionDisponible = $.grep(lListMissionValidee, function (mission) {
                var lMissionStart = moment(mission.dateDebut);
                var lMissionEnd = moment(mission.dateFin).add(-moment().utcOffset() + dstInFrance(today), 'minutes').endOf('days');

                var isListEventValidWithMission = [];
                listEventsSelected.forEach((event)=>{
                    isListEventValidWithMission.push(lMissionStart <= event.start && lMissionEnd >= event.end);
                });

                return isListEventValidWithMission.every((element)=>{
                   return element === true;
                });
            });

            var tmpListEvents = extend(listEventsSelected);
            // Initialisation de la fenêtre modale
            lModal.init(
                tmpListEvents,
                lStart,
                lEnd,
                lListMissionDisponible,
                listTypeAbsence,
                isRangeOfDay = true,
                absenceId = null,
                WITHFERIE
            );
        }
        return false;
    }

    function viewRenderCalendar(view, element){
        lDisabled = getTimeSheetIsEditable(view.intervalStart);

        // Alerte sur la validation des Frais.
        // Si on créé le RA, alerte 1 et on passe le RA à l'état Brouillon.
        // Si on a déjà le RA à l'état BR on le passe à l'état soumis et on met l'alerte 2
        test = getTimeSheetIsPresent(view.intervalStart);
        if (test == "none") {
            //Cas où il n'y a pas de RA
            $btnRASansFrais.removeAttr('disabled');
            $btnSoumissionRA.removeAttr('disabled');
        }
        else if (test == "SO") {
            $btnRASansFrais.attr('disabled', 'disabled');
            $btnSoumissionRA.attr('disabled', 'disabled');
        }
        else {
            $btnRASansFrais.attr('disabled', 'disabled');
            $btnSoumissionRA.removeAttr('disabled');
        }

        var $buttonPrev = $('#calendar .fc-prev-button');
        if (view.intervalStart.isSame(lMinimumMonthViewOnFullCalendar, 'month')) {
            $buttonPrev.addClass("fc-state-disabled");
        } else {
            $buttonPrev.removeClass("fc-state-disabled");
        }
    }



    // Lunch and init the modal of absences validation request
    $('body').on('click', '#btnValidAbsencesRequest', function () {
        var lModalValidAbsences = new modalValidAbsences($('#modalValidAbsences'));
        lModalValidAbsences.init();
    });

    $('body').on('click', '#btn-sendRA', function () {
        //desactiver bouton de soumission si le RA à déjà été soumis
        avecFrais = "oui";
        testBeforeSendRAPdf($('#calendar'), $("#warning-sending-RA"), WITHSATURDAY, avecFrais);
    });

    $('body').on('click', '#btn-RASansFrais', function () {
        //desactiver bouton de soumission si le RA à déjà été soumis
        avecFrais = "non";
        testBeforeSendRAPdf($('#calendar'), $("#warning-sending-RA"), WITHSATURDAY, avecFrais);
    });

    $('body').on('click', '#sending-RA', function () {
        $('#btn-sendRA').attr("disabled", "disabled");
        $('#btn-RASansFrais').attr("disabled", "disabled");

        // Insert text when loading
        $modalLoading.find(".modal-footer").append("Cela peut prendre quelques instants.");

        $modalLoading.modal({
            backdrop: 'static',
            keyboard: false
        });

        setTimeout(function(){
            var currentView = $('#calendar').fullCalendar("getView").intervalStart;
            sendRAPDF(currentView.format(), avecFrais);
        }, 5000);
    });

    $('body').on('click', '#btn-checkRA', function () {
        var currentView = $('#calendar').fullCalendar("getView").intervalStart;
        checkRAPDF(currentView.format());
    });

    if (MOISCHOISI != "null" && MOISCHOISI != null) {
        $('#calendar').fullCalendar('gotoDate', MOISCHOISI);
    }
});

function disabledOrNotEvent(disabled, pListEvents) {
    var currentView = $('#calendar').fullCalendar('getView');
    var currentViewMonth = currentView.intervalStart.month();

    var listEventsNotInCurrentViewMonth = $.grep(pListEvents, function (event) {
        return ( event.start.month() != currentViewMonth );
    });

    // Si les RA sont soumis ET ( le mois afficher dans fullcalendar et avant now() OU le meme que now() )
    if (disabled) {
        return true;
        //Sinon on doit pouvoir modifier que les evenements du mois affiché dans fullCalendar
    } else if (pListEvents.length == 0) {
        return true;
    } else if (listEventsNotInCurrentViewMonth.length > 0) {
        return true;
    } else {
        return false;
    }
}

function delHolidaysIfMissionInSameHalfDay(pListHolidays, pListMissions) {
    var lResList = [];
    $.each(pListHolidays, function (key, holiday) {
        var eventsWithSameHalfDay = $.grep(pListMissions, function (eventMission) {
            return (eventMission.start == holiday.start) && (eventMission.end == holiday.end);
        });
        if (eventsWithSameHalfDay.length == 0) {
            lResList.push(pListHolidays[key]);
        }

    });
    return lResList;
}

function PanelAbsences($pListGroupTypeAbsence, pColors) {
    var that = this;
    this.$that = $pListGroupTypeAbsence;
    this.color = pColors;

    this.buildList = function (pList, itemName) {
        that.$that.empty();
        $.each(pList, function (key, val) {
            var li = $('<li>').attr({
                'class': 'list-group-item'
            }).html(val.etat);
            var span = $('<span>').attr({
                'class': 'badge'
            }).css({
                backgroundColor: that.color.getValue(val.code)
            }).html(" ");

            li.append(span);
            that.$that.append(li);
        });
    }
}

function PanelMission($pListGroupMission, pColors) {
    var that = this;
    this.$that = $pListGroupMission;
    this.colors = pColors;
    //Construit la liste dans la division $pListGroupMission avec les couleurs pColors
    this.buildList = function (pList) {
        that.$that.empty();

        $.each(pList, function (key, val) {

            //Récupération de la couleur
            var color = that.colors.getValue(val["mission"]);
            var $li = buildLiMission(val, color);

            that.$that.append($li);
        });
    }
    return that;
}

/**
 * Construit l'element liste pour la mission passée en paramètre
 * @param jsonMission
 * @param color
 * @returns un élement liste
 */
function buildLiMission(jsonMission, color) {
    //Id de la div cachée
    var idCollapse = "id" + jsonMission.id;
    var formatStr = "DD/MM/YYYY";

    //Création de l'element liste
    var $li = $('<li>').attr({
        'class': 'list-group-item'
    });

    // Création de l'icone pour afficher la div avec une margin left
    var $a = $("<a>").attr({
        "data-toggle": "collapse",
        "href": "#" + idCollapse,
        "aria-expanded": "false",
        "aria-controls": "collapseExample",
    }).css({
        "margin-right": "10px",
        "color": "#515151"
    });

    // Création icone
    var $glyphicon = $("<span>").attr({
        "class": "glyphicon glyphicon-menu-down",
        "aria-hidden": "true"
    }).css({
        "margin-right": "10px"
    });

    //Création du badge pour la légende
    var $span = $('<span>').attr({
        'class': 'badge'
    }).css({
        backgroundColor: color,
        "float": "right"
    }).html(" ");

    //Texte pour la liste des forfaits
    var forfaits = "Liste des forfaits : <br>";
    if (jsonMission.listMissionForfait.length > 0) {
        var n = 0;
        $.each(jsonMission.listMissionForfait, function (key, tempLinkForfaitMission) {
            if (tempLinkForfaitMission.montant !== 0) {
                forfaits += "  -  " + tempLinkForfaitMission.forfait.nom + " : " + tempLinkForfaitMission.montant + "€<br>";
                n += 1;
            }
        });
        if (n === 0) {
            forfaits += "  - Pas de forfait pour cette mission."
        }
    } else {
        forfaits += "  - Pas de forfait pour cette mission."
    }

    //Texte pour la liste des frais
    var frais = "Liste des frais :<br />";
    if (jsonMission.listFrais.length > 0) {
        $.each(jsonMission.listFrais, function (key, item) {
            if (item.hasOwnProperty('nombreKm')) {
                frais += "  -  " + item.typeFrais + " le " + moment(item.dateEvenement).format(formatStr) + " : " + item.nombreKm + " km<br/>";
            } else {
                frais += "  -  " + item.typeFrais + " le " + moment(item.dateEvenement).format(formatStr) + " : " + item.montant + " €<br/>";
            }
        });
    } else {
        frais += "  - Pas de frais pour ce mois-ci."
    }

    //Text des détails de la mission
    var divContent =
        "Periode :"
        + "<br>"
        + "  - " + moment(jsonMission.dateDebut).subtract(moment().utcOffset()-dstInFrance(moment()), 'minutes').format(formatStr)
        + "  - " + moment(jsonMission.dateFin).subtract(moment().utcOffset()-dstInFrance(moment()), 'minutes').format(formatStr)
        + "<br><br>"
        + forfaits
        + "<br />"
        + frais;

    //Divs qui s'affichent quand click sur l'icone
    var $div = $("<div>").attr({
        "class": "collapse",
        "id": idCollapse
    }).append(
        "<br> " + divContent
    );

    $a.append($glyphicon);
    $a.append(jsonMission.mission);
    $a.append($span);
    $li.append($a);
    $li.append($div);

    return $li;
}

function sendRAPDF(currentView, varFrais) {
    window.location.href = window.location.href.slice(0,window.location.href.indexOf('/Rapport-Activites')) +
        '/Rapport-Activites/soumissionRA/' + currentView + "/" + varFrais;
}

/**
 * Permet de visualiser le RA en cours
 * @param currentView
 */
function checkRAPDF(currentView) {
    var dateCal = $('#calendar').fullCalendar('getView').intervalStart.format('DD/MM/YYYY');
    window.open("Rapport-Activites/visualisationRA/" + currentView, "_blank");
}

/***
 * Méthode permettant d'afficher une fenêtre de dialogue
 * @param pTitle Titre de la boite de dialogue
 * @param pMessage Message de la boite de dialogue
 */
function showDialogAbsence(pTitle, pMessage) {

    $("#textDialogAbsence").html(pMessage);
    $("#dialogEditionAbsence").dialog({
        title: pTitle,
        html: pMessage,
        position: {my: "center", at: "center", of: $("#calendar")},
        modal: true,
        draggable: false,
        resizable: false,
        show: {effect: "fade", duration: 300},
        open: function () {
            $(".ui-widget-header").css('background', '#CE3089');
            $(".ui-dialog-titlebar-close").hide();
        },
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }
    });
}

/***
 * Méthode de génération de la boite de dialogue de
 * confirmation de la validation du RA
 * @param $calendar Calendrier contenant les timesheets
 * @param $modalPdfRA ...
 * @param pWithSaturday Présence des samedis
 */
function testBeforeSendRAPdf($calendar, $modalPdfRA, pWithSaturday, avecFrais) {
    var presentEventsEmpty = false;
    var presentEventsAbsence = false;
    var absencesNonSoumises = false;

    var currentView = $calendar.fullCalendar("getView");
    var listEvents = $calendar.fullCalendar("clientEvents");

    //var listEventsWithEmptyDays = initListEventsWithDates(listEvents, currentView.intervalStart , currentView.intervalEnd.subtract(1,"days") , pWithSaturday);

    if (avecFrais == "oui") {
        $("#warning-sending-RA-body").empty();
        $("#warning-sending-RA-body").append("<p>Votre rapport pour le mois de " + currentView.intervalStart.format("MMMM YYYY") + " va être soumis à votre manager. Vous recevrez une copie par e-mail.</p><br>");
    }
    else {
        $("#warning-sending-RA-body").empty();
        $("#warning-sending-RA-body").append("<p>Votre rapport pour le mois de " + currentView.intervalStart.format("MMMM YYYY") + " va être soumis <u>sans</u> les frais. <p><b><u><i>Pensez bien à faire la validation complète avant le 5 du mois suivant !</i></u></b></p></p><br>");
    }

    // Récupération de la liste des jours vide du mois
    var listEventsEmpty = $.grep(listEvents, function (event) {
        var dateCouranteEnd = event.end;
        var dateCourante = dateCouranteEnd._d;

        return ((dateCourante.toDateString().split([" "])[0] != "Sat") && (null == event.mission && null == event.absence && null == event.ferie) );
    });

    // Récupération de la liste des absences non valides du mois
    var listEventsWithAbsence = $.grep(listEvents, function (event) {
        return (null != event.absence && "VA" != event.absence.etat.code && "CO" != event.absence.etat.code);
    });

    // Récupération de la liste des absences non soumises
    var listEventsWithAbsenceNonSoumise = $.grep(listEvents, function (event) {
        return (null != event.absence && "VA" != event.absence.etat.code && "SO" != event.absence.etat.code && "CO" != event.absence.etat.code );
    });

    // Stockage de la présence ou non de jours vides et/ou d'absences non valides

    presentEventsEmpty = ( listEventsEmpty.length > 0 );
    presentEventsAbsence = ( listEventsWithAbsence.length > 0 );
    absencesNonSoumises = ( listEventsWithAbsenceNonSoumise.length > 0 );

    var lListMessages = [];
    var $lUl = $('<ul>').addClass('list-group');

    // Présence de jours vide dans le mois
    if (true == presentEventsEmpty) {
        //Dans le cas où les jours vides sont dans une mission enregistré, on affiche le message et on bloque la soumission
        var testBeforeJourDemarrage = beforeJourDemarrage(listEventsEmpty[listEventsEmpty.length - 1].start);
        if (testBeforeJourDemarrage == "ActionCommercial") {
            document.getElementById("sending-RA").setAttribute('disabled', 'disabled');
            lListMessages.push("Impossible de soummettre le RA, vérifiez avec la DRH qu'une mission vous a été attribuée pour ce mois-ci.");
        }
        else if (!testBeforeJourDemarrage) {
            document.getElementById("sending-RA").setAttribute('disabled', 'disabled');
            lListMessages.push("Attention, Vous n'avez pas rempli tout le mois. Veuillez bien remplir chaque jours avant validation !");
        }
        //Dans le cas où les jours vides sont des jours correspondants à des jours avant l'embauche, on laisse la soumission.
        else {
            document.getElementById("sending-RA").removeAttribute('disabled');
        }
    }
    else {
        document.getElementById("sending-RA").removeAttribute('disabled');
    }
    // Présence d'absence non validée dans le mois
    if (true == presentEventsAbsence) {
        lListMessages.push("Attention présence absence(s) non validée(s) .");
    }
    //Présence d'absence non soumise
    if (true == absencesNonSoumises) {
        document.getElementById("sending-RA").setAttribute('disabled', 'disabled');
        lListMessages.push("Attention, vous avez des absences non soumises pour ce mois-ci. Veuillez demandez la validation de vos absences !");
    }

    // Création des messages
    $.each(lListMessages, function (key, msg) {
        var $lIl = $('<li>').addClass('list-group-item ' + 'list-group-item-warning').html(msg);
        $lUl.append($lIl);
    });

    // Ajout des messages à la boite de dialogue
    $("#warning-sending-RA-body").append($lUl);

    //Affichage de la boite de dialogue
    $modalPdfRA.modal({backdrop: 'static'});
}
