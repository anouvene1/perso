/*
 * ©Amiltone 2017
 */
function annulerFiltre() {
    window.location.href = window.location.pathname;
}

// Checkbox data formatter
function stateFormatter(value, row, index) {
    if (row.code !== "SO") {
        return {
            disabled: true,
            checked: false
        };
    }
    return value;
}

nomFichier = document.location.href.substring(document.location.href.lastIndexOf("/") + 1);

if (nomFichier == "VA") {
    nomFichier = "(validées)";
}
else if (nomFichier == "SO") {
    nomFichier = "(soumises)";
}
else if (nomFichier == "RE") {
    nomFichier = "(refusées)";
}
else {
    nomFichier = "";
}
document.getElementById("filtre").innerHTML = "Consultation des demandes de déplacement " + nomFichier;

$(function () {
    var $bootstrapTable = $("#idBootstrapTable");
    var $modalLoading = $("#idModalLoading");

    $(".btnActionSelection").click(function (e) {
        //Action qu'on veut attacher au formulaire temporaire
        var action = $(this).data("action");

        //Liste des cases cochées
        var selectedDepl = $bootstrapTable.bootstrapTable("getSelections");

        var $tmpForm = $("<form>", {
            "action": action,
            "method": "POST"
        });

        //Pour chaque absences cochées
        //	- création input
        //	- ajout de l'input au form
        $.each(selectedDepl, function (key, val) {
            if (val.code === 'SO') {
                $("<input>", {
                    "name": "listDeplacements[" + key + "].id",
                    "value": val.id
                }).appendTo($tmpForm);
            }
        });
        $modalLoading.find(".modal-footer").append("Cela peut prendre quelques instants.")
        $modalLoading.modal({
            backdrop: 'static',
            keyboard: false
        });
        $tmpForm.appendTo('body').submit();
    });

    $('.monthPicker').change(function () {
        window.location.href = window.location.pathname + "?monthYearExtract=" + this.value;
    });
});

	

