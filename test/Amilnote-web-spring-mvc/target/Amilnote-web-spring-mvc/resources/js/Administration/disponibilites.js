// =======================
// Gestion des selects
// =======================
$(document).ready(function () {
    // removeSpace();
    addSelectedOnResponsable(idManager);
    addSelectedOnCollab(idCollaborateur);

    $('#filtreCollaborateur').on('change', function() {
        var selectedCollaborateur = $(this).val();
        TimeScheduler.Options.SelectedCollaborator = selectedCollaborateur;
        TimeScheduler.Init();
    });
});

function addSelectedOnResponsable(idManager) {
    if (idManager) {
        $('#filtreResponsable').val(idManager);

        var index = $("#filtreResponsable").find("option[value='" + idManager + "']").index();
        $('#filtreForm .filtreResponsable ul').find("li[data-original-index='" + index + "']").addClass("selected");

        var manager = $('#filtreForm .filtreResponsable ul li[class=selected] span').html();
        $('#filtreForm .filtreResponsable ul li[class=selected] a').attr('aria-selected', "true");
        $('#filtreForm > div.form-group.filtreResponsable > div > button').attr('title', manager);
        $('#filtreForm .filtreResponsable > div > button > span.filter-option.pull-left').html(manager);

    } else {
        $('#selectResponsable ul.dropdown-menu').find("li").removeClass("selected");
    }

    changeResponsable();
}

function addSelectedOnCollab(idCollaborateur) {
    if (idCollaborateur) {

        $('#filtreCollaborateur').val(idCollaborateur);

        var indexCollab = $("#filtreCollaborateur").find("option[value='" + idCollaborateur + "']").index();
        $('#filtreForm div.form-group.filtreCollaborateur ul').find("li[data-original-index='" + indexCollab + "']").addClass("selected");

        var collaborateur = $('#filtreForm .filtreCollaborateur ul li[class=selected] span').html();
        $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "true");
        $('.filtreCollaborateur span.filter-option.pull-left').html(collaborateur);
        $('.filtreCollaborateur > div > button').attr('title', collaborateur);

    } else {
        $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");
    }
}

function changeResponsable() {

    $('#filtreCollaborateur').val("");
    $('.filtreCollaborateur span.filter-option.pull-left').html("Aucun Collaborateur");
    $('.filtreCollaborateur > div > button').attr('title', 'Aucun Collaborateur');
    $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "false");
    $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");

    var selectedVal = $("#filtreResponsable").find("option:selected").val();
    var i = 0;

    if (selectedVal != null) {
        $("#filtreCollaborateur option").filter(function () {
            var equal = $(this).attr('data-id-manager') == selectedVal;
            if (equal) {
                $('#filtreForm .filtreCollaborateur ul').find("li[data-original-index='" + i + "']").show();
            } else {
                $('#filtreForm .filtreCollaborateur ul').find("li[data-original-index='" + i + "']").hide();
            }
            i++;
        });
    } else {
        $('#filtreForm .filtreCollaborateur ul li').show();
    }
}

function annulerFiltre() {
    window.location.href = window.location.pathname;
}

function resetFilters() {
    TimeScheduler.Options.SelectedPeriod = '1 month';
    TimeScheduler.Options.SelectedCollaborator = "";
    TimeScheduler.Init();

    // reset filter manager
    $('#filtreResponsable').val("");
    $('.filtreResponsable span.filter-option.pull-left').html("Aucun Responsable");
    $('.filtreResponsable > div > button').attr('title', 'Aucun Responsable');
    $('.filtreResponsable > div > button').addClass('bs-placeholder');
    $('#filtreForm .filtreResponsable ul li[class=selected] a').attr('aria-selected', "false");
    $('#filtreForm .filtreResponsable ul').find("li").removeClass("selected");
    $('#filtreForm .filtreCollaborateur ul li[class=selected] a').attr('aria-selected', "false");

    // reset filter collaborator
    $('#filtreCollaborateur').val("");
    $('.filtreCollaborateur span.filter-option.pull-left').html("Aucun Collaborateur");
    $('.filtreCollaborateur > div > button').attr('title', 'Aucun Collaborateur');
    $('.filtreCollaborateur > div > button').addClass('bs-placeholder');
    $('#filtreForm .filtreCollaborateur ul').find("li").removeClass("selected");
    $('#filtreForm .filtreCollaborateur ul li').show();

    $('.time-sch-section-row').show();
    $('.time-sch-section-container').show();
}

// =======================
// Gestion du calendar
// =======================

// Définition des parametres en fraçais, comme le nom des mois, des jours etc)
moment.locale("fr");

// Définir la date de début à l'initialisation
var today = moment().startOf('day');

var Calendar = {
    // Definition d'une periode
    Periods: [
        {
            Name: '1 month',
            Label: '1 m<span class="toto">ois</span>',
            TimeframePeriod: (60 * 24),
            TimeframeOverall: (60 * 24 * 30),
            TimeframeHeaders: [
                'MMM',
                'Do'
            ],
            Classes: 'period-1month'
        },
        // {
        //     Name: '3 month',
        //     Label: '3 m<span class="toto">ois</span>',
        //     TimeframePeriod: (60 * 24 * 3),
        //     TimeframeOverall: (60 * 24 * 90),
        //     TimeframeHeaders: [
        //         'MMM',
        //         'Do'
        //     ],
        //     Classes: 'period-1month'
        // },
        // {
        //     Name: '6 month',
        //     Label: '6 m<span class="toto">ois</span>',
        //     TimeframePeriod: (60 * 24 * 7),
        //     TimeframeOverall: (60 * 24 * 180),
        //     TimeframeHeaders: [
        //         'MMM',
        //         'Do'
        //     ],
        //     Classes: 'period-1month'
        // },
        // {
        //     Name: '1 year',
        //     Label: '1 an',
        //     TimeframePeriod: (60 * 24 * 14 ),
        //     TimeframeOverall: (60 * 24 * 360),
        //     TimeframeHeaders: [
        //         'MMM',
        //         'Do'
        //     ],
        //     Classes: 'period-1month'
        // }
    ],

    // Exemple de définition d'un item
    Items: [
        ...lstMissions,
        ...lstAbsencesSoumises,
        ...lstAbsencesValidees
    ],

    Sections: [
        ...lstCollaborateurs
    ],

    Init: function () {
        TimeScheduler.Options.GetSections = Calendar.GetSections;
        TimeScheduler.Options.GetSchedule = Calendar.GetSchedule;
        TimeScheduler.Options.Start = today;
        TimeScheduler.Options.Periods = Calendar.Periods;
        TimeScheduler.Options.SelectedPeriod = '1 month';
        TimeScheduler.Options.SelectedCollaborator = "";
        TimeScheduler.Options.Element = $('.calendar');

        TimeScheduler.Options.AllowDragging = false;
        TimeScheduler.Options.AllowResizing = false;

        TimeScheduler.Options.Events.ItemClicked = Calendar.Item_Clicked;
        TimeScheduler.Options.Events.ItemDropped = Calendar.Item_Dragged;
        TimeScheduler.Options.Events.ItemResized = Calendar.Item_Resized;

        TimeScheduler.Options.Events.ItemMovement = Calendar.Item_Movement;
        TimeScheduler.Options.Events.ItemMovementStart = Calendar.Item_MovementStart;
        TimeScheduler.Options.Events.ItemMovementEnd = Calendar.Item_MovementEnd;

        TimeScheduler.Options.Text.NextButton = '&nbsp;';
        TimeScheduler.Options.Text.PrevButton = '&nbsp;';

        TimeScheduler.Options.MaxHeight = 100;

        TimeScheduler.Init();
    },

    GetSections: function (callback) {
        callback(Calendar.Sections);
    },

    GetSchedule: function (callback, start, end) {
        callback(Calendar.Items);
    },

    Item_Clicked: function (item) {
        console.log(item);
    },

    Item_Dragged: function (item, sectionID, start, end) {
        var foundItem;

        for (var i = 0; i < Calendar.Items.length; i++) {
            foundItem = Calendar.Items[i];

            if (foundItem.id === item.id) {
                foundItem.sectionID = sectionID;
                foundItem.start = start;
                foundItem.end = end;

                Calendar.Items[i] = foundItem;
            }
        }

        TimeScheduler.Init();
    },

    Item_Movement: function (item, start, end) {
        var html;

        html =  '<div>';
        html += '   <div>';
        html += '       Start: ' + start.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '   <div>';
        html += '       End: ' + end.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '</div>';

        $('.realtime-info').empty().append(html);
    },

    Item_MovementStart: function () {
        $('.realtime-info').show();
    },

    Item_MovementEnd: function () {
        $('.realtime-info').hide();
    }
};

$(document).ready(Calendar.Init);
// ========== Fin Calendar

