/*
 * ©Amiltone 2017
 */

nomFichier = document.location.href.substring(document.location.href.lastIndexOf("/") + 1);

if (nomFichier == "SO") {
    nomFichier = "(Soumis)";
}
else if (nomFichier == "AN") {
    nomFichier = "(Annulés)";
}
else if (nomFichier == "BR") {
    nomFichier = "(Brouillons)";
}
else {
    nomFichier = "";
}


function annulerFiltre() {
    var dir = window.location.pathname.split('/');
    var res = dir[0] + "/";
    for (var i = 1; i < dir.length - 2; i++)
        res += dir[i] + "/";
    window.location.href = res + "null/TO";
}

function showCollab(){
    $('#tabRAstt').css('display', 'none');
    $('#tabRAcollab').css('display','contents');
}

function showSTT(){
    $('#tabRAstt').css('display', 'contents');
    $('#tabRAcollab').css('display','none');
}

$(function () {
    $('.monthPicker').change(function () {
        var dir = window.location.pathname.split('/');
        var res = dir[0] + "/";
        for (var i = 1; i < dir.length - 1; i++)
            res += dir[i] + "/";

        var monthYear = this.value.split("/");
        window.location.href = res + monthYear[0] + "_" + monthYear[1];
    });

    var $table = $('#table'),
        $button = $('#button');
    $button.click(function () {
        alert('getSelections: ' + JSON.stringify($table.bootstrapTable('getSelections')));
    });

    document.getElementById("filtre").innerHTML = "Consultation des rapports d'activités " + nomFichier;

    // Navigation filter
    var dir = window.location.pathname.split('/');
    document.getElementById("lienFiltre1").setAttribute("href", document.getElementById("lienFiltre1") + "/" + dir[dir.length - 1]);
    document.getElementById("lienFiltre2").setAttribute("href", document.getElementById("lienFiltre2") + "/" + dir[dir.length - 1]);
    document.getElementById("lienFiltre3").setAttribute("href", document.getElementById("lienFiltre3") + "/" + dir[dir.length - 1]);
    document.getElementById("lienFiltre5").setAttribute("href", document.getElementById("lienFiltre5") + "/" + dir[dir.length - 1]);

    var filtre;
    switch (dir[dir.length - 2]) {
        case "BR":
            filtre = "lienFiltre2";
            break;
        case "SO":
            filtre = "lienFiltre3";
            break;
        case "AN":
            filtre = "lienFiltre5";
            break;
        default:
            filtre = "lienFiltre1";
    }

    var filtreElement = document.getElementById(filtre);
    Object.assign(filtreElement.style, {fontWeight:'bold', fontSize:'14px', textDecoration:'underline'});

    showCollab();
});

