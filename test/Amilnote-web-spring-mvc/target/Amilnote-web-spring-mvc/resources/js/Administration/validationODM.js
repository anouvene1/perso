/*
 * ©Amiltone 2017
 */

$(function () {
    var $bootstrapTable = $("#idBootstrapTable");
    var $modalLoading = $("#idModalLoading");

    $(".btnActionSelection").click(function (e) {

        //Action qu'on veut attacher au formulaire temporaire
        var action = $(this).data("action");
        //Liste des cases cochées
        var selectedODM = $bootstrapTable.bootstrapTable("getSelections");

        var $tmpForm = $("<form>", {
            "action": action,
            "method": "POST"
        });
        //Pour chaque absences cochées
        //	- création input
        //	- ajout de l'input au form
        $.each(selectedODM, function (key, val) {
            $("<input>", {
                "name": "listMissions[" + key + "].id",
                "value": val.id
            }).appendTo($tmpForm);
        });

        $modalLoading.find(".modal-footer").append("Cela peut prendre quelques instants.")
        $modalLoading.modal({
            backdrop: 'static',
            keyboard: false
        });
        $tmpForm.appendTo('body').submit();
    });

});

function removeOut() {
    $('#oModalPDF').modal('toggle');
}
