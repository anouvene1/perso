/*
 * ©Amiltone 2017
 */

// [JNA][AMNOTE 142] Crée une nouvelle ligne à la facture pour ajouter un frais
function loadFrais() {
    var idtva = "tva_"+ document.getElementById('TVA').value;
    var tvaMontant = document.getElementById(idtva).getAttribute("name");
    var tva = Number(tvaMontant)/100;
    var frais = $("#frais"); // On définit div des frais
    var nb = 0;
    for (var i = 1; i < 100; i++) {
        if ($('.ligneFrais#' + i).length === 0) {
            nb = i;
            break;
        }
    }
    $('#frais').append('<div id="' + nb + '" class="form-group ligneFrais"><label class="col-md-1 control-label" for="libelle' + nb + '">Libellé :</label>' +
        '<div class="col-md-3"><input required id="libelle' + nb + '" type="text" name="libelleElementFacture" placeholder="Libellé" class="form-control input-md" value = ""/></div>' +
        '<div class="col-md-3"><input id="quantite' + nb + '" type="number" name="quantiteElementFacture" placeholder="Quantité" onchange="loadMontantFraisNotSave(' + nb + ')" class="form-control input-md" value = ""  step="any"/></div>' +
        '<div class="col-md-3"><input id="pu' + nb + '" type="number" name="puElementFacture" placeholder="Prix unitaire" onchange="loadMontantFraisNotSave(' + nb + ')" class="form-control input-md" value = "" step="any"/></div>' +
        '<div class="col-md-1"><input id="montant' + nb + '" type="number" name="montantElementFacture" placeholder="0" class="form-control input-md montant" value = "" onchange="loadTotal()"  readOnly="true"/></div>' +
        '<input value="" name="idElementFacture" type="hidden">' +
        '<div class="col-md-1"><button type="button" class="btn btn-default" ><span onclick="deletInput(' + nb + ',' + tva + ')" class="glyphicon glyphicon-remove text-danger" style="font-size: 20px;"></span></button></div></div>'
    );
}

function changeAvoir() {
    var etat = document.getElementById('idIsAvoir').checked;
    if(etat)
    {
        if(document.getElementById('idlabelTitre').innerHTML=='Création d\'une facture'){
            document.getElementById('idlabelTitre').innerHTML='Création d\'un avoir';
        } else {
            document.getElementById('idlabelTitre').innerHTML = 'Edition de l\'avoir';
        }
        document.getElementById('btnVisualiser').innerHTML = 'Visualisez votre avoir';

        document.getElementById('labelNumFacture').innerHTML = 'N° d\'avoir :';
    }
    else
    {
        if(document.getElementById('idlabelTitre').innerHTML=='Création d\'un avoir'){
            document.getElementById('idlabelTitre').innerHTML='Création d\'une facture';
        } else {
            document.getElementById('idlabelTitre').innerHTML = 'Edition de la facture';
        }
        document.getElementById('btnVisualiser').innerHTML = 'Visualisez votre facture';

        document.getElementById('labelNumFacture').innerHTML = 'N° de facture :';
    }
}

// [JNA][AMNOTE 142] Suppression d'une ligne de frais
function deletInput(id, tva) { // On récupère l'id de la ligne
    $("#" + id).empty(); // On vide la div
    $("#" + id).remove(); // On supprime la div
    loadTotal(tva); // On recalcul le total de la facture
}

// [JNA][AMNOTE 142] Suppression d'une ligne d'un frais enregistrer en BD
function deletInputFrais(id, tva) { // On récupère l'id de la ligne
    var alerteUtil = $("#messageUtilisateur"); // On définit la liste a remplir
    $.ajax({
        type: 'GET',
        url: 'Administration/deleteFrais/' + id,
        dataType: 'json',
        success: function (json) {
            alerteUtil.append('<p>' + json + '</p>');
        }
    });
    $("#" + id).empty(); // On vide la div
    $("#" + id).remove(); // On supprime la div
    loadTotal(tva); // On recalcul le total de la facture
}

// [JNA][AMNOTE 142] Calcul automatique montant de la commande
function loadMontant() {
    var quantité = $("#nbJours").val(); // On récupère la quantité
    var prix = $("#tjm").val(); // On récupère le PU
    var ht = quantité * prix; // Calcul du total HT de la commande
    $("#montant").val(ht); // Mise à jour de la valeur du montant de la commande
    loadTotal(); // On recalcul le montant total de la facture
}

// [SBE][AMNOTE 186] Calcul automatique montant du frais pour les frais que l'on vient de créer
function loadMontantFraisNotSave(nb) {
    var qteId = "#" + "quantite" + nb;
    var prixId = "#" + "pu" + nb;
    var mtnId = "#" + "montant" + nb;
    var quantité = $(qteId).val(); // On récupère la quantité
    var prix = $(prixId).val(); // On récupère le PU
    var ht = quantité * prix; // Calcul du total HT de la prestation
    $(mtnId).val(ht); // Mise à jour de la valeur du montant de la prestation
    loadTotal(); // On recalcul le montant total de la facture
}

// [SBE][AMNOTE 186] Calcul automatique montant du frais pour les frais qui sont recuperer en base
function loadMontantFraisSave(nb) {
    var qteId = "#" + "quantiteFraisSave" + nb;
    var prixId = "#" + "puFraisSave" + nb;
    var mtnId = "#" + "montantFraisSave" + nb;
    var quantité = $(qteId).val(); // On récupère la quantité
    var prix = $(prixId).val(); // On récupère le PU
    var ht = quantité * prix; // Calcul du total HT de la prestation
    $(mtnId).val(ht); // Mise à jour de la valeur du montant de la prestation
    loadTotal(); // On recalcul le montant total de la facture
}


// [JNA][AMNOTE 142] Calcul automatique montant facture
function loadTotal() {
    var idtva = "tva_"+ document.getElementById('TVA').value;
    var tvaMontant = document.getElementById(idtva).getAttribute("name");
    var tva = Number(tvaMontant)/100;
    var total = 0;
    $(".montant").each(function () { // Pour tous les élément ayant la classe "montant"
        total = total * 1 + $(this).val() * 1;// On récupère la valeur et on calcul le total
    });
    $("#ht").val(roundTwoDecimals(total)); // Mise à jour de la valeur des input
    var tvaMontant = total * tva; // on recalcul la TVA
    $("#tva").val(roundTwoDecimals(tvaMontant)); // On affiche la TVA
    $("#total").val(roundTwoDecimals(total + tvaMontant));// On affiche le montant TTC
}

// [JNA][AMNOTE 158] Submit du form pour visualiser la facture
function visuPdfFacture() {
    var form = document.getElementById("editionFacture");
    form.setAttribute("action", "Administration/visualisez");
    form.setAttribute("target", "_blank");

    var rempli = 0; // vérifie que tous les libellés sont remplis

    var lignesFraisJson = [];

    $('.ligneFrais').each(function (index, element) {
        var id = $(element).attr("id");
        if ($("#libelle" + id).val() != "") { // Si le champs est bien rempli
            rempli = 0; // On garde le compteur à 0
        } else { // Sinon
            rempli = 1; // Le compteur est à 1
        }
        var ligneFrais = {};
        $(element).find('input').each(function (idxInput, inputElt) {

            if ($(inputElt).attr('name') === 'idElementFacture') {
                ligneFrais.id = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'libelleElementFacture') {
                ligneFrais.libelle = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'quantiteElementFacture') {
                ligneFrais.quantite = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'puElementFacture') {
                ligneFrais.pu = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'montantElementFacture') {
                ligneFrais.montant = $(inputElt).val();
            }

        });
        lignesFraisJson.push(ligneFrais);

    });

    if (rempli == 0) {
        $("#listeJsonElementsFacture").val(JSON.stringify(lignesFraisJson));
        form.submit();
    }
}


//AMNOTE 290 sléection de la TVA
function choixdeTva() {
    var idtvabis = document.getElementById('TVA').value;
    var idtva = "tva_"+idtvabis;
    var etat = document.getElementById(idtva).getAttribute("name");
    document.getElementById("hiddenTva").setAttribute("value",idtvabis);
    document.getElementById('tauxtva').innerHTML = "T.V.A ("+Number(etat)/100+")";
    loadTotal();
    if( document.getElementById('montant') != null)
        document.getElementById('montant').setAttribute("onchange","loadTotal("+Number(etat)/100)+")";


}

// Used to round a number to two decimals
// toFixed has a problem when rounding a number finishing by 5, do not use !
function roundTwoDecimals(value) {
    return Math.round(value*100)/100;
}


// [JNA][AMNOTE 158] Submit du form pour save la facture
function saveFacture() {
    var form = document.getElementById("editionFacture");
    form.setAttribute("action", "Administration/saveFacture");
    form.setAttribute("target", "");
    var rempli = 0; // vérifie que tous les libellés sont remplis

    var lignesFraisJson = [];

    $('.ligneFrais').each(function (index, element) {
        var id = $(element).attr("id");
        if ($("#libelle" + id).val() != "") { // Si le champs est bien rempli
            rempli = 0; // On garde le compteur à 0
        } else { // Sinon
            rempli = 1; // Le compteur est à 1
        }
        var ligneFrais = {};
        $(element).find('input').each(function (idxInput, inputElt) {

            if ($(inputElt).attr('name') === 'idElementFacture') {
                ligneFrais.id = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'libelleElementFacture') {
                ligneFrais.libelle = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'quantiteElementFacture') {
                ligneFrais.quantite = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'puElementFacture') {
                ligneFrais.pu = $(inputElt).val();
            }
            if ($(inputElt).attr('name') === 'montantElementFacture') {
                ligneFrais.montant = $(inputElt).val();
            }

        });
        lignesFraisJson.push(ligneFrais);

    });

    if (rempli == 0) {
        $("#listeJsonElementsFacture").val(JSON.stringify(lignesFraisJson));

        form.submit();
    }

}


