/*
 * ©Amiltone 2017
 */

$(function () {
    var $bootstrapTable = $("#idBootstrapTable");
    //var $commentaireSelection = $("#idCommentaireSelection");
    var $modalLoading = $("#idModalLoading");
    //Quand la fenêtre modale a fini de s'ouvrir
    $("#idModalShowAbsence").on('shown.bs.modal', function () {
        //On initialise l'apparence du calendrier
        $("#calendarAbsence").fullCalendar('render');
    });

    $(".btnActionSelection").click(function (e) {

        //Action qu'on veut attacher au formulaire temporaire
        var action = $(this).data("action");
        //Liste des cases cochées
        var selectedAbsences = $bootstrapTable.bootstrapTable("getSelections");

        var $tmpForm = $("<form>", {
            "action": action,
            "method": "POST"
        });
        //Pour chaque absences cochées
        //	- création input
        //	- ajout de l'input au form
        $.each(selectedAbsences, function (key, val) {

            $("<input>", {
                name : "listAbsences[" + key + "].id",
                value : val.id,
                type : "hidden"
            }).appendTo($tmpForm);
        });

        var refUri = $(this).data("refuri");
        if(refUri) {
            $("<input>", {
                type: 'hidden',
                name: 'refererUri',
                value: refUri
            }).appendTo($tmpForm);
        }

        $modalLoading.find(".modal-footer").append("Cela peut prendre quelques instants.")
        $modalLoading.modal({
            backdrop: 'static',
            keyboard: false
        });
        $tmpForm.appendTo('body').submit();
    });

});

/**
 * Appeler quand l'utilisateur click sur le bouton détails du tableau
 * l'evenement est ajouté directement sur la balise html du bouton car certains boutons n'apparaissent que
 * lors du changement de page dans le tableau.
 * @param elem
 * @param listAbsences
 */
function eventClikOnBtnShow(elem) {

    var $divCalendar = $("#calendarAbsence");
    //Recuperation des data-attributs
    var idAbsence = $(elem).data("absenceid");
    var dateDebutAbsence = $(elem).data("datedebut");
    var typeAbsence = $(elem).data("typeabsence");

    if (null != idAbsence) {
        //Initialisation du calendrier
        getListEventsWithAbsenceIdAndInitFullCalendar($divCalendar, idAbsence, dateDebutAbsence, typeAbsence);
        //initCalendar( $divCalendar , idAbsence , dateDebutAbsence , typeAbsence);
        //Affichage de la popup
        $("#idModalShowAbsence").modal("show");
    }


}
/**
 * Fonction qui initialise fullcalendar dans la div passée en paramètre, avec une source d'evenements personnalisée
 * @param $div
 * @param absenceWithListEvents
 */
function initCalendar($divCalendar, dateDebutAbsence, typeAbsence, data) {
    //On détruis le calendrier existant
    $divCalendar.fullCalendar('destroy');
    //Initialisation d'un nouveau
    $divCalendar.fullCalendar({
        header: {
            left: 'title',
            right: '',
            center: ''
        },
        events: data,
        defaultView: 'month',
        editable: false,
        defaultDate: dateDebutAbsence,
        hiddenDays: [0],
        selectable: false,
        fixedWeekCount: false,
        selectHelper: false,
        defaultTimedEventDuration: '04:00:00',
        eventRender: function (event, element, view) {
            //On change le titre affiché sur la page
            element.html(event.title);
        },
        eventDataTransform: function (data) {
            return {
                title: typeAbsence,
                start: new moment(data.start),
                end: new moment(data.end)
            };
        },
    });
}

/**
 * Retourne la liste des evenements avec l'idAbsence passé en paramètre
 * Si success : appelle la méthode initCalendar
 * Si error : affiche l'erreur
 * @param $divCalendar
 * @param idAbsence
 * @param dateDebutAbsence
 * @param typeAbsence
 */

function getListEventsWithAbsenceIdAndInitFullCalendar($divCalendar, idAbsence, dateDebutAbsence, typeAbsence) {

    $.ajax({
        url: 'Administration/getListEventsWithAbsenceId/' + idAbsence,
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        async: false,
        success: function (data) {
            initCalendar($divCalendar, dateDebutAbsence, typeAbsence, data);
        },
        error: function (xhr, status, error) {
            $divCalendar.empty();
            $divCalendar.html(xhr.responseText)
        },
        fail: function (xhr, status, error) {
            $divCalendar.empty();
            $divCalendar.html(xhr.responseText)
        },
        statusCode: {

            901: function () {
                top.location.href = "login";
            },
            902: function () {
                top.location.href = "login";
            }
        }
    });

}
