/*
 * ©Amiltone 2017
 */
const MANAGERS_LIST_ID = "listeManagers";

function getSelectedForfait(sel) {
    var selectedForfaitOption = $('#idListForfaits').find('option:selected');

    if ($('#idListForfaits').val().indexOf('LIB') !== -1) {

        $('#libelle').attr("value", selectedForfaitOption.attr("name"));

        $('#libelleForfaitLibre').attr('style', 'display: block');
        $('#montantForfait').attr('style', 'display: none');
    }
    else {
        $('#libelleForfaitLibre').attr('style', 'display: none');
        $('#montantForfait').attr('style', 'display: block');
    }
}

function nomMissionValid() {
    if (document.getElementById('idMission').value == "") {
        if (!nomMissionExist(document.getElementById('nomMission').value)) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

$("#idMontant").attr('required', '');

function remplaceVirgule(input) {
    input.value = input.value.replace(',', '.');
}

function testForm(form) {
    var $modalWarning = $("#modalWarning");

    var nomMission = document.getElementById('nomMission').value;
    if(nomMission.indexOf("/") > -1) {
        $modalWarning.find(".modal-body").empty();
        $modalWarning.find(".modal-body").append("Nom de mission invalide : les '/' ne sont pas autorisés.");
        $modalWarning.modal({
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }

    var valid = nomMissionValid();
    if (!valid) {
        $modalWarning.find(".modal-body").empty();
        $modalWarning.find(".modal-body").append("Une mission portant le même nom existe déjà pour ce collaborateur. \nVeuillez choisir un nom de mission unique.");
        $modalWarning.modal({
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }
    else {
        if ((form.listTypesMission.value == 5) && (form.listClients.value == "")) {

            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Vous devez sélectionner un client.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            return false;
        }
        if (form.listTypesMission.value == 5 && $('#tjm').val() <= 0) {
            $('#tjm').focus();
            return false;
        }
    }
}

$(function() {
    var URLWITHForfait = urlForfait;
    var URLEDITMISSION = urlEditMission;
    var addForfaitWithUrl = urlAddForfait;

    // A chaque changement d'état de la checkbox on affiche ou non les champs pour remplir le forfait.

    var WITHFORFAIT = pWithForfaitFromController;
    var editMission = pEditMission;
    var idClientSelected = document.getElementById("listClients").value;

    if (editMission) {
        var idMission = document.getElementById("idMission").value;
    } else {
        var idMission = null;
    }

    if (idMission == null) {
        $('#idWithForfait').change(function (e) {
            storeValues(document.formMission);
            if ((idClientSelected != "undefined") && (idClientSelected != "")) {
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + (WITHFORFAIT ? "false" : "true") + "/" + idClientSelected);
            } else
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + (WITHFORFAIT ? "false" : "true") + "/" + 0);
        });

        $("#listClients").change(function (e) {
            if (document.getElementById("listClients").value != "") {
                storeValues(document.formMission);
                $(location).attr('href', URLWITHForfait + "/editMission" + "/" + document.getElementById("idWithForfait").checked + "/" + document.getElementById("listClients").value);
            }
        });

    } else {
        $("#listClients").change(function (e) {
            if (document.getElementById("listClients").value != "") {
                storeValues(document.formMission);
                $(location).attr('href', URLEDITMISSION + "/" + idMission + "/getContacts/" + document.getElementById("listClients").value);
            }
        });
    }

    $('#idWithForfait').prop('checked', WITHFORFAIT);

    if (WITHFORFAIT) {
        $('#forfaitform').show();
    } else {
        $('#forfaitform').hide();
    }


    $("#buttonSubmitForfait").click(function (e) {
        var tmpForfaitCode = document.getElementById('idListForfaits').value;
        var tempForfaitMontant = document.getElementById('idMontant').value;
        var libelle = $("#libelle").val();
        var percent = '%';
        // symbole pourcentage encodé car caractère mal interprété dans les url
        var percentEncoded = "%25";

        if (libelle === "") {
            libelle = "0";
        }

        if (libelle.indexOf(percent) !== -1) {
            libelle = libelle.replace(/\%/g, percentEncoded);
        }


        storeValues(document.formMission);
        if ((idClientSelected != "undefined") && (idClientSelected != "")) {
            window.location.href =  addForfaitWithUrl + "/" + tmpForfaitCode + "/" + tempForfaitMontant  + "/" + idClientSelected  + "/" + libelle;
        } else {
            window.location.href = addForfaitWithUrl + "/" + tmpForfaitCode + "/" + tempForfaitMontant  + "/" + 0 + "/" + libelle;
        }
    });

    $("#suppr").click(function (e) {
        storeValues(document.formMission);
    });

    // on set les valeurs enregistrées dans le cookie
    if (nomMission = getCookie("nomMission"))
        document.formMission.nomMission.value = nomMission;
    if (dateDebut = getCookie("dateDebut"))
        document.formMission.dateDebut.value = dateDebut;
    if (dateFin = getCookie("dateFin"))
        document.formMission.dateFin.value = dateFin;
    if (listTypesMission = getCookie("listTypesMission"))
        document.getElementById('listTypesMission').value = listTypesMission;
    if (outilsInterne = getCookie("outilsInterne"))
        document.getElementById('outilsInterne').value = outilsInterne;
    if (description = getCookie("description"))
        document.getElementById('description').value = description;
    if (duree = getCookie("duree"))
        duree = duree;
    if (listClients = getCookie("listClients"))
        document.getElementById('listClients').value = listClients;
    if (listContactsClient = getCookie("listContactsClient"))
        document.getElementById('listContactsClient').value = listContactsClient;
    if (suivi = getCookie("suivi"))
        document.getElementById('suivi').value = suivi;
    if (tjm = getCookie("tjm"))
        document.getElementById('tjm').value = tjm;
    if (listeManagers = getCookie(MANAGERS_LIST_ID))
        document.getElementById(MANAGERS_LIST_ID).value = listeManagers;
    /*
     SHA AMNOTE-196 05/01/2017
     restitution des cookies des chechboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        var perimetre = getCookie("perimetreMission-" + e.value);
        if (perimetre) {
            $("#perimetreMission_"+e.value).prop("checked", perimetre == "true");
        }
    });

    // On affiche les infos pour la mission cliente et formation
    loadTypeMission();

    // Date picker calendar
    $(".datepicker, #ui-datepicker-div").datepicker({
        showOn: "both",
        dateFormat: 'yy-mm-dd'
    }).on("click", function() {
        $(".ui-datepicker-calendar").css("display", "block");
    });
});

function setCookie(name, value) {
    var today = new Date();
    var expiry = new Date(today.getTime() + 60 * 1000); // plus 1 min
    document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
}

function deleteCookie(name) {
    var today = new Date();
    var expired = new Date(today.getTime() - 24 * 3600 * 1000); // less 24 hours
    document.cookie = name + "=null; path=/; expires=" + expired.toGMTString();
}

function clearCookies() {
    /*
     SHA AMNOTE-196 05/01/2017
     suppression des cookies des chechboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        deleteCookie("perimetreMission-" + e.value);
    });

    deleteCookie("nomMission");
    deleteCookie("dateDebut");
    deleteCookie("dateFin");
    deleteCookie("listTypesMission");
    deleteCookie("outilsInterne");
    deleteCookie("description");
    deleteCookie("duree");
    deleteCookie("listClients");
    deleteCookie("listContactsClient");
    deleteCookie("suivi");
    deleteCookie("tjm");
    deleteCookie(MANAGERS_LIST_ID);
}

function storeValues(form) {
    /*
     SHA AMNOTE-196 05/01/2017
     création des cookies des chechboxes du choix des périmètres mission
     */
    $('.perimetreMission').each(function (i, e) {
        setCookie("perimetreMission-" + e.value, e.checked);
    });

    setCookie("nomMission", form.nomMission.value);
    setCookie("dateDebut", form.dateDebut.value);
    setCookie("dateFin", form.dateFin.value);
    setCookie("listTypesMission", document.getElementById('listTypesMission').value);
    setCookie("outilsInterne", document.getElementById('outilsInterne').value);
    setCookie("description", document.getElementById('description').value);
    setCookie("duree", document.getElementById("duree").value);
    setCookie("listClients", document.getElementById("listClients").value);
    setCookie("listContactsClient", document.getElementById("listContactsClient").value);
    setCookie("suivi", document.getElementById("suivi").value);
    setCookie("tjm", form.tjm.value);
    setCookie(MANAGERS_LIST_ID, document.getElementById(MANAGERS_LIST_ID).value);
    return true;
}

function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

function loadTypeMission() {
    var idMission = $("#listTypesMission").val();
    checkPerimetresByDefault(idMission);

    /*Ids type mission:
     * 1=Travaux internes
     * 2=ActivitéCommerciale
     * 4=Formation
     * 5=Mission cliente
     * 6=Web Factory
     * 7=Mobile Factory
     * 8=Data Factory
     * 9=Soft Factory
     */

    var divClient = $("#divClient");
    var divOutilsInterne = $("#divOutilsInterne");
    var inputActiviteCommerciale = $('#perimetreMission_3');
    var fieldsMissionCliente = $(".form-group.missionCliente");
    var fieldsOutilsInterne = $(".form-group.fieldsOutilsInterne");
    var divAgence = $("#choixAgence");

    switch(idMission) {
        case '2':
            divAgence.removeAttr('hidden');
            divAgence.show();

            divOutilsInterne.attr('hidden', 'true');
            divClient.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '4':
            divClient.removeAttr('hidden'); //on restore la visibilité
            fieldsMissionCliente.show();
            inputActiviteCommerciale.prop("checked", false);
            inputActiviteCommerciale.parent().hide();
            fieldsMissionCliente.hide(); //tous les champs propres aux missions clientes sont cachés

            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '5':
            divClient.removeAttr('hidden'); //on restaure la visibilité
            fieldsMissionCliente.show();
            inputActiviteCommerciale.parent().show();

            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            break;

        case '9':
            divOutilsInterne.removeAttr('hidden');
            fieldsOutilsInterne.show();

            divClient.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            break;

        default:
            divClient.attr('hidden', 'true');
            divOutilsInterne.attr('hidden', 'true');
            divAgence.attr('hidden', 'true');
            fieldsOutilsInterne.hide();
            document.getElementById('listClients').value = null;
            document.getElementById('listContactsClient').value = null;
            document.getElementById('suivi').value = null;
            document.getElementById('tjm').value = 0;
            document.getElementById(MANAGERS_LIST_ID).value = null;
            break;
    }
}
function changeTypeMission() {
    $(".check-box.perimetreMission").prop("checked", false);
    loadTypeMission();
}

function checkPerimetresByDefault(idMission) {
    // On coche les cases masquées des périmètres de mission selon le type de mission
    switch (idMission) {
        case "1": //Travaux internes
        case "6": //idem pour Web Factory
        case "7": //idem pour Mobile Factory
        case "8": //idem pour Data Factory
        case "9": //idem pour Soft Factory
            $('#perimetreMission_1').prop("checked", true);
            break;
        case "2":
            $('#perimetreMission_3').prop("checked", true);
            break;
        default: //Cases à cocher non masquées pour Mission Cliente et Formations
            break;
    }
}

function removeForfait(pCode, pMontant) {
    var montant = pMontant;
    var code = pCode;
    var idClientSelected = document.getElementById("listClients");
    var urlWithRemoveForfait = urlRemoweForfait;

    if ((idClientSelected.value != "undefined") && (idClientSelected.value != "")) {
        window.location.href = urlWithRemoveForfait + "/" + code + "/" + montant + "/" + idClientSelected.value;
    } else {
        window.location.href = urlWithRemoveForfait + "/" + code + "/" + montant + "/" + 0;
    }
}
