function annulerFiltre() {
    window.location.href = window.location.pathname;
}

function exporterCommandeExcel() {
    $.ajax({
        url: "Administration/gestionCommandes/",
        type: "POST",
        success: function (fileName) {
        },
        error : function (xhr, status, error) {
            if (!$("#msgErreurExport_" + target).length) {
                //On affiche le message d'erreur
                var messageErreur = $("#msgErreurExport").clone();
                messageErreur.attr('id', 'msgErreurExport_' + target);
                messageErreur.prependTo(button.closest("fieldset"));
                messageErreur.removeAttr("hidden");
                messageErreur.find(".close").click(function () {
                    messageErreur.remove();
                });
            }
        }
    });
}

$(document).ready(function() {
    $('.monthPicker').change(function () {
        window.location.href = window.location.pathname + "?monthYearExtract=" + this.value;
    });

    $(".exportExcelCommandes").click(function () {
        exporterCommandeExcel();
    });
});
