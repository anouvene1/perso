<%--
  ~ ©Amiltone 2017
  --%>

<%--
  Created by IntelliJ IDEA.
  User: sbenslimane
  Date: 16/11/2016
  Time: 15:45
  To change this template use File | Settings | File Templates.
--%>
<link rel="stylesheet" href="resources/css/facturesValidees.css">


<%-- SBE_AMNOTE_154 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${administration}${factures}/null">Gestion des factures</a></li>
    <li class="active">Factures validées</li>
</ol>

<div id="idTableContainer">
    <fieldset>
        <c:if test="${not empty mapOdfFacture}">
            <a class="btn btn-info" href="${administration}${saveFactureArchivee}/${lAnnee}/${leMois}">Télécharger factures archivées PDF ${leMois} ${lAnnee}</a>
        </c:if>

        <legend class="text-center">
            <h3>Factures archivées</h3></span>
            <h3>${leMois} ${lAnnee}</h3>
            <br/>
        </legend>

        <form class="text-center" method="post" action="${administration}${facturesValidees}/facturesArchivees">
            <label for="monthExtract">Date voulue : </label>
            <input name="monthYearExtract" id="monthExtract" class="monthPicker" placeholder="MM/AAAA" autocomplete="off" value="${monthYearExtract}">
            <button class="btn btn-action vaderBlue btn-xs" type="submit" name="action" value="">Valider</button>
        </form>
    </fieldset>

    <div id="mini-scrolltab">

        <fieldset class="tabODF">
            <c:forEach items="${mapOdfFacture}" var="map">
                <div><h4>${map.key}</h4></div>
                <table data-toggle="table"
                       data-height="520"
                       data-search="true"
                       data-striped="true"
                       data-toolbar="#toolbarCommande"
                       data-pagination="true"
                       data-pagination-v-align="top"
                       data-pagination-h-align="left"
                       data-page-size=100
                       id="idBootstrapTable"
                       data-cookie="true"
                       data-cookie-id-table="saveIdEditCollabSansFacture">
                    <thead>

                    <c:choose>
                        <c:when test="${mapOdfTypeFacture.get(map.key) != 2}">
                            <tr>
                                <th data-field="id" data-sortable="true" data-align="center">N°</th>
                                <th data-field="client" data-sortable="true" data-align="center">Client</th>
                                <th data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur</th>
                                <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                                <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                                <th data-field="quantite" data-sortable="true" data-align="center">Quantité</th>
                                <th data-field="pu" data-sortable="true" data-align="center">P.U.</th>
                                <th data-field="total" data-sortable="true" data-align="center">Total</th>
                                <th data-field="visualisation" data-sortable="true" data-align="center">Visualisation</th>
                            </tr>
                            </thead>

                            <tbody>

                            <c:forEach items="${map.value}" var="facturesArchivees">
                                <tr>
                                    <td>${facturesArchivees.numero}</td>
                                    <td>${facturesArchivees.client}</td>
                                    <td>${facturesArchivees.collaborateur}</td>
                                    <td>${facturesArchivees.mission}</td>
                                    <td>${facturesArchivees.numCommande}</td>
                                    <td>${facturesArchivees.quantite}</td>
                                    <td>${facturesArchivees.prix}</td>
                                    <td>${facturesArchivees.montant}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${facturesArchivees.cheminPDF!= null}">
                                                <a href="${administration}/afficher/${facturesArchivees.id}" target="_blank"><span
                                                    class="glyphicon glyphicon-eye-open "></span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="glyphicon glyphicon-eye-close "></span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <th data-field="id" data-sortable="true" data-align="center">N°</th>
                                <th data-field="client" data-sortable="true" data-align="center">Client</th>
                                <th data-field="collaborateur" data-sortable="true" data-align="center">Collaborateur</th>
                                <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                                <th data-field="commande" data-sortable="true" data-align="center">Commande</th>
                                <%-- <th data-field="total" data-sortable="true" data-align="center">Total H.T.</th> --%>
                                <th data-field="total" data-sortable="true" data-align="center">Total T.T.C.</th>
                                <th data-field="visualisation" data-sortable="true" data-align="center">Visualisation</th>
                            </tr>
                            </thead>

                            <tbody>

                            <c:forEach items="${map.value}" var="facturesArchivees">
                                <c:set var="montantTTC" value="${facturesArchivees.montant}"/>
                                <tr>
                                    <td>${facturesArchivees.numero}</td>
                                    <td>${facturesArchivees.client}</td>
                                    <td>${facturesArchivees.collaborateur}</td>
                                    <td>${facturesArchivees.mission}</td>
                                    <td>${facturesArchivees.numCommande}</td>
                                    <td>${facturesArchivees.montant * 1.2}</td>
                                    <%--<td>${montantTTC * 1.2}</td> --%>
                                    <td>
                                        <c:choose>
                                            <c:when test="${facturesArchivees.cheminPDF!= null}">
                                                <a href="${administration}/afficher/${facturesArchivees.id}" target="_blank"><span
                                                    class="glyphicon glyphicon-eye-open "></span></a>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="glyphicon glyphicon-eye-close "></span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </c:forEach>
        </fieldset>
    </div>
</div>
