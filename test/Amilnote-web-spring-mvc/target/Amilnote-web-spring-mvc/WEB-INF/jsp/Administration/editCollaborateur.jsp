<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>
<script type="text/javascript" src='resources/js/NotesDeFrais/notesFrais.js'></script>
<script type="text/javascript" src='resources/js/Administration/editCollaborateur.js'></script>

<!-- Edition ou Ajout d'un collaborateur -->
<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <c:choose>
         <c:when test="${typeTravailleur=='sous_traitant'}">
            <li><a href="${ administration }${ travailleurs }?type=idTableContainerSS">Consultants</a></li>
         </c:when>
        <c:otherwise>
            <li><a href="${ administration }${ travailleurs }?type=idTableContainer">Consultants</a></li>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${typeTravailleur=='sous_traitant'}">
            <c:choose>
                <c:when test="${!empty collaborateur.id}">
                    <li class="active">Édition d'un sous-traitant</li>
                </c:when>
                <c:otherwise>
                    <li class="active">Création d'un sous-traitant</li>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${!empty collaborateur.id}">
                    <li class="active">Édition d'un collaborateur</li>
                </c:when>
                <c:otherwise>
                    <li class="active">Création d'un collaborateur</li>
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
</ol>

<%--@elvariable id="collaborateur" type="Collaborateur"--%>
<form:form id="idFormColl" class="form-horizontal" method="POST"
           commandName="collaborateur"
           action="${ administration }${saveCollaborateur}">

    <div id="idProfilContainer">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center">

                <c:choose>
                    <c:when test="${!empty collaborateur.id}">
                        <c:choose>
                            <c:when test="${typeTravailleur=='sous_traitant'}">
                                Modification du sous-traitant :   ${ collaborateur.mail }
                            </c:when>
                            <c:otherwise>
                                Modification du collaborateur :   ${ collaborateur.mail }
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${typeTravailleur=='sous_traitant'}">
                                Création du sous-traitant :   ${ collaborateur.mail }
                            </c:when>
                            <c:otherwise>
                                Création du collaborateur :   ${ collaborateur.mail }
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>

            </legend>
            <form:hidden path="id"/>
            <!-- Text input -->
            <c:choose>
                <c:when test="${typeTravailleur=='sous_traitant'}">
                    <!-- pour les sous-traitants on peut mettre n'importe quel type de mail -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idEmailColl">Email<span style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idEmailColl" path="mail" type="email"
                                        placeholder="" class="form-control input-md"
                                        data-validation="custom"
                                        data-validation-regexp="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|" />
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idEmailColl">Email<span style="color:red">*</span></label>
                        <div class="col-md-2">
                            <input id="idFirstPartEmail" class="form-control input-md" type="text" data-validation-regexp="[a-z]{1}[a-z]" data-validation="custom"/>
                        </div>
                        <div class="col-md-2">
                            <select id="idSecondPartEmail" name="domainName" class="form-control">
                                <c:forEach var="domainName" items="${listDomainNames}">
                                    <option value="<c:out value='${domainName}'/>"><c:out value="${domainName}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <!-- Contains full email address like test@amiltone.com -->
                        <form:hidden id="idEmailColl" path="mail"/>
                    </div>
                </c:otherwise>
            </c:choose>

            <div class="form-group">
                <label class="col-md-4 control-label" for="listCivilite">Civilité</label>
                <div class="col-md-4">
                    <form:select name="listCivilite" class="form-control" path="civilite.id"
                                 items="${listCivilite}" itemLabel="civilite" itemValue="id">
                    </form:select>
                </div>
            </div>

            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idNomColl">Nom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idNomColl" path="nom" type="text" placeholder=""
                                class="form-control input-md" data-validation-length="2-50"
                                data-validation="length"/>
                </div>
            </div>

            <!-- Text input -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idPrenomColl">Prenom<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="idPrenomColl" path="prenom" type="text"
                                placeholder="" class="form-control input-md"
                                data-validation-length="2-50" data-validation="length"/>
                </div>
            </div>

            <!-- Text input -->
            <c:choose>
                <c:when test="${typeTravailleur=='sous_traitant'}">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idRaisonSocialeColl">Raison sociale<span style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idRaisonSocialeColl" path="raisonSociale.raisonSociale" type="text"
                                        placeholder="" class="form-control input-md"
                                        data-validation-length="1-500" data-validation="length"/>
                        </div>
                    </div>
                </c:when>
            </c:choose>

            <!-- SBE_185 adresse Postal -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idAdresseColl">Adresse</label>
                <div class="col-md-4">
                    <form:input id="idAdresseColl" path="adressePostale" type="text" placeholder=""
                                class="form-control input-md" data-validation-length="0-500"
                                data-validation="length"/>
                </div>
            </div>

            <!-- SBE_185 Mail personnel -->
            <c:choose>
                <c:when test="${typeTravailleur=='collaborateur'}">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idEmailPersoColl">Email personnel</label>
                        <div class="col-md-4">
                            <form:input id="idEmailPersoColl" path="mailPerso" type="email"
                                        placeholder="" class="form-control input-md"
                                        data-validation="custom"
                                        data-validation-regexp="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+|"
                                        data-validation-help="Vous devez entrer une adresse mail en .fr ou .com"/>
                        </div>
                    </div>
                </c:when>
            </c:choose>

            <!-- Telephone -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="idTelColl">Téléphone</label>
                <div class="col-md-4">
                    <form:input id="idTelColl" path="telephone" type="tel"
                                class="form-control input-md" data-validation="custom"
                                data-validation-regexp="^0[1-79][0-9]{8}$|^$"
                                data-validation-help="Veuillez entrer un numéro de téléphone valide en 10 chiffres."/>
                </div>
            </div>

            <c:choose>
                <c:when test="${typeTravailleur=='collaborateur'}">
                    <!-- date d'embauche -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idDateEmbaucheColl">Date d'embauche<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idDateEmbaucheColl" path="dateEntree" type="text"
                                        class="form-control input-md datepicker" data-validation="date" autocomplete="off"/>
                        </div>
                    </div>

                    <!-- date de naissance -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idDateAnnivColl">Date de naissance<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idDateAnnivColl" path="dateNaissance" type="text"
                                        class="form-control input-md datepicker" data-validation="date" autocomplete="off"/>
                        </div>
                    </div>
                </c:when>
            </c:choose>

            <!-- Select poste -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="listPostes">Choix
                    du poste</label>
                <div class="col-md-4">
                    <form:select name="listPostes" class="form-control" path="poste.id"
                                 items="${listPostes}" itemLabel="poste" itemValue="id">
                    </form:select>
                </div>
            </div>

            <!-- Select droit -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="listStatuts">Choix
                    du droit</label>
                <div class="col-md-4">
                    <form:select name="listStatuts" class="form-control"
                                 path="statut.id" items="${listStatuts}" itemLabel="statut"
                                 itemValue="id">
                    </form:select>
                </div>
            </div>

            <!-- Select manager -->
            <div class="form-group">
                <!-- Si le collaborateur n'est pas un manager ou un admin
                ( codeManager et codeAdmin viennent d'attributs issues du controller -->

                <!-- Si l'attribut existe on affiche la liste -->
                <c:if test="${not empty listManagers}">
                    <c:choose>
                        <c:when test="${typeTravailleur=='sous_traitant'}">
                            <label class="col-md-4 control-label" for="listManagers">Choix du prescripteur</label>
                        </c:when>
                        <c:otherwise>
                            <label class="col-md-4 control-label" for="listManagers">Choix du manager</label>
                        </c:otherwise>
                    </c:choose>
                    <div class="col-md-4">
                        <select name="manager.id" class="form-control">
                            <c:forEach var="optionManager" items="${listManagers}">

                                <option value="${optionManager.id}"
                                    <c:if test="${optionManager.id == collaborateur.manager.id}">
                                        <c:out value='selected="selected"'/>
                                    </c:if>>
                                        ${optionManager.prenom} ${optionManager.nom} -
                                        ${optionManager.mail}</option>

                            </c:forEach>
                        </select>
                    </div>
                </c:if>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="listAgencies">Choix de l'agence</label>
                <div class="col-md-4">
                    <form:select name="listAgencies" class="form-control" path="agency"
                                 items="${listAgencies}" itemLabel="agency" itemValue="id">
                    </form:select>
                </div>
            </div>

            <!-- Choix statut cadre -->
            <c:choose>
                <c:when test="${typeTravailleur=='collaborateur'}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cadre</label>
                        <c:set var="verifCadre" scope="session" value="C"/>

                        <div class="col-md-4">
                            <label for="radios-0"> <form:radiobutton path="statutGeneral"
                                                                     value="C"
                                                                     checked="${(collaborateur.statutGeneral == verifCadre)? \"checked\" : \'\' }"/>
                                Oui
                            </label>
                            <label for="radios-0"> <form:radiobutton path="statutGeneral"
                                                                     value="NC"
                                                                     checked="${(collaborateur.statutGeneral != verifCadre)? \"checked\" : \'\' }"/>
                                Non
                            </label>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <form:input path="statutGeneral" value="NC" type="hidden"/>
                </c:otherwise>

            </c:choose>
            <c:choose>
                <c:when test="${typeTravailleur=='collaborateur'}">
                    <div class="form-group" id="checkbox-addvise">
                        <div>
                            <c:choose>
                                <c:when test="${collaborateur.exclusAddvise}">
                                    <input type="checkbox" id="addvise-annulees" name="exclus_addvise" checked="checked"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" id="addvise-annulees" name="exclus_addvise"/>
                                </c:otherwise>
                            </c:choose>
                            <label> Sortir le collaborateur du programme Addvise</label>
                        </div>
                    </div>
                </c:when>
            </c:choose>
            <br><br>

            <!-- Boutons actions -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <c:choose>
                        <c:when test="${typeTravailleur=='collaborateur'}">
                            <a class="btn btn-danger" href="${administration}${travailleurs}?type=idTableContainer"
                                role="button">Retour</a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-danger" href="${administration}${travailleurs}?type=idTableContainerSS"
                               role="button">Retour</a>
                        </c:otherwise>
                    </c:choose>
                    <button id="idSubmitCollaborateur" type="submit"
                            name="submitCollaborateur" class="btn btn-success" >Valider
                    </button>
                </div>
            </div>
        </fieldset>
    </div>
</form:form>

<fieldset>
    <!-- Form Name -->
    <legend class="text-center"><br>Liste des missions</legend>

    <div id="idTableContainer">
        <div>
            <a
                href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}"
                class="btn btn-success"
                <c:if test="${empty collaborateur.id }"> disabled="true" </c:if>>Nouvelle
                mission </a>
        </div>
        <div id="collab-scrolltab">
            <table data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbar"
                   data-pagination="true"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-page-size=100
                   id="idBootstrapTable"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditCollab">
                <thead>
                <tr>
                    <th data-field="mission" data-sortable="true" data-align="center">Mission</th>
                    <th data-field="dateDebut" data-sortable="true" data-align="center"
                        data-sorter="ddMMyyyyCustomSorter">Date de début
                    </th>
                    <th data-field="dateFin" data-sortable="true" data-align="center"
                        data-sorter="ddMMyyyyCustomSorter">Date de fin
                    </th>
                    <th data-align="center">Édition</th>
                    <th data-align="center">Visualiser</th>
                    <th data-align="center">Suppression</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listMissions}" var="mission">
                    <tr>
                        <td>${mission.mission}</td>
                        <td><fmt:formatDate value="${mission.dateDebut}"
                                            pattern="dd/MM/yyyy"/></td>
                        <td><fmt:formatDate value="${mission.dateFin}"
                                            pattern="dd/MM/yyyy"/></td>
                        <td><a
                            href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}/${mission.id}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${mission.pdfODM !='null' }">
                                    <a href="${administration}${editCollaborateur}/${collaborateur.id}${editMission}/${mission.id}${download}"
                                       target="_blank">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-eye-close"></span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td><a onclick="eventClikDelate(urlDelate + ${mission.id} + '/' +${collaborateur.id})">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</fieldset>

<c:if test="${typeTravailleur == 'collaborateur'}">
    <script> collaboratorEmailVerif(); </script>
</c:if>

<fieldset>
    <legend class="text-center"> Liste des notes de frais</legend>
    <div id="idTableContainer">

        <a href="${administration}${editTravailleur}/${typeTravailleur}/${collaborateur.id}/null" id="lienFiltre">Toutes</a>
        <a href="${administration}${editTravailleur}/${typeTravailleur}/${collaborateur.id}/VA" id="lienFiltre">Validées</a>
        <a href="${administration}${editTravailleur}/${typeTravailleur}/${collaborateur.id}/SO" id="lienFiltre">Soumises</a>
        <a href="${administration}${editTravailleur}/${typeTravailleur}/${collaborateur.id}/SD" id="lienFiltre">Soldées</a>

        <FORM id="formFiltre" method="GET">
            <div class="form-group" style="margin-top:27px">
                <input name="monthYearExtract"
                       id="monthExtract"
                       class="monthPicker form-control"
                       placeholder="MM/AAAA"
                       autocomplete="off"
                       value="${monthYearExtract}"
                >
                <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
            </div>
        </FORM>

        <div id="collab-scrolltab">
            <table data-toggle="table"
                   data-height="520"
                   data-search="true"
                   data-striped="true"
                   data-toolbar="#toolbar"
                   data-pagination="true"
                   data-pagination-v-align="top"
                   data-pagination-h-align="left"
                   data-page-size=25
                   id="idBootstrapTable"
                   data-cookie="true"
                   data-cookie-id-table="saveIdEditCollab2">
                <thead>
                <tr>
                    <th data-sortable="true" data-align="center">Type de frais</th>
                    <th data-sortable="true" data-align="center">Mission</th>
                    <th data-sortable="true" data-align="center">Dépense</th>
                    <th data-sortable="true" data-align="center" data-sorter="ddMMyyyyCustomSorter">Date</th>
                    <th data-sortable="true" data-align="center">État</th>
                    <th data-sortable="true" data-align="center">Motif</th>
                    <th data-sortable="true" data-align="center">Justificatif(s)</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listFrais}" var="frais">
                    <tr>
                        <!-- Type de Frais -->
                        <td>${frais.typeFrais}
                            <c:if test="${frais.fraisForfait == 'FRF' }"><br/> (- le forfait)</c:if>
                            <c:if test="${frais.fraisForfait == 'FAF' }"><br/> (+ le forfait)</c:if>
                            <c:if
                                test="${frais.typeAvance == 'PE' && frais.typeFrais == 'Avance sur frais'}"><br/>(Permanente)</c:if>
                            <c:if
                                test="${frais.typeAvance == 'PO' && frais.typeFrais == 'Avance sur frais'}"><br/>(Ponctuelle)</c:if>
                        </td>
                        <!-- Mission -->
                        <td>${frais.mission}</td>
                        <!-- Dépense -->
                        <c:if test="${frais.typeFrais == 'Voiture personnelle'}">
                            <td>${frais.nombreKm} km</td>
                        </c:if>
                        <c:if test="${frais.typeFrais != 'Voiture personnelle'}">
                            <td>${frais.montant} €</td>
                        </c:if>
                        <!--  Date -->
                        <td><fmt:formatDate value="${frais.dateEvenement}" pattern="dd/MM/yyyy"/></td>
                        <!-- Etat -->
                        <c:if test="${frais.etat.etat == 'Brouillon' && frais.typeFrais == 'Avance sur frais'}">
                            <td>
                                <form action="${mesNotesDeFrais}${soumettreAvance}/${frais.id}"
                                      style='margin-bottom:0px;'>
                                    <button class="btn btn-success">Soumettre</button>
                                </form>
                            </td>
                        </c:if>
                        <c:if test="${!(frais.etat.etat == 'Brouillon' && frais.typeFrais == 'Avance sur frais')}">
                            <td>${frais.etat.etat}</td>
                        </c:if>
                        <!-- Motif -->
                        <td>${frais.commentaire}</td>
                        <!-- Justificatif(s) -->
                        <c:if test="${frais.hasJustif == 0}">
                            <td>
                                -
                            </td>
                        </c:if>
                        <c:if test="${frais.hasJustif >=1}">
                            <td>
                                <c:forEach items="${frais.justifs}" var="justif">
                                    ${justif.descriptif}
                                    <a>
                                        <span class="glyphicon glyphicon-eye-open"
                                              onclick="sendRowForPicture(${justif.id})"></span>
                                    </a>
                                    <br>
                                </c:forEach>
                            </td>
                        </c:if>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</fieldset>

<div class="modal fade" id="warning-delete-Action" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Suppression d'une mission</h3>
            </div>
            <div class="modal-body" id="warning-delete-Mission-body">
                <h4>Êtes-vous sûr de vouloir supprimer cette mission ?</h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-Action"
                        class="btn btn-success dismiss" data-dismiss="modal">Oui
                </button>
                <button type="button" class="btn btn-danger dismiss"
                        data-dismiss="modal">Non
                </button>
            </div>
        </div>
    </div>
</div>

<!-- pop in pour la vue en grand des justificatifs -->
<section class="modal fade" id="img-full">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Votre justificatif</h4>
                <!-- <p>Pour firefox, merci de cocher "aperçu dans firefox" dans
                    Options>Applications</p>-->
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</section>
