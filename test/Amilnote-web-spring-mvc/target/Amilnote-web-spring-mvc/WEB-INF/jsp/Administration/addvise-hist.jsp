<%--
  ~ ©Amiltone 2017
--%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!-- Ressources complémentaires -->
<link rel="stylesheet" type="text/CSS" href="${contextPath}/resources/css/addvise.css"/>
<script type="text/javascript" src="${contextPath}/resources/js/Addvise/fixed_table_rc.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/Addvise/addvise-hist.js"></script>

<!-- Fil d'arianne -->
<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li><a href="${ administration }/addvise">Gestion du programme Addvise</a></li>
    <li class="active" id="filtre">Historique du programme Addvise</li>
</ol>

<!-- flash messages -->
<c:if test="${not empty succes}">
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <p class="addvise-flash-success">
                ${succes}
        </p>
    </div>
</c:if>

<!-- Contenu pour l'historique Addvise -->

<div id="div-addvise-gestion">
    <h3>Historique des modules Addvise</h3>
    <a href="${ administration }/addvise" type="button" data-backdrop="static"
       class="btn btn-lg btn-default btn-action purpleHaze">
        <span class="glyphicon glyphicon-pencil"></span> Retour à la gestion
    </a>

    <div id="checkbox">
        <input type="checkbox" id="addvise-annulees">
        <label>Afficher les sessions annulées</label>
    </div>
    <div id="addvise-tri-date">
        <p class="addvise-hist-datepicker">
            Période : du <input id="datepicker01" class="datepicker-hist form-control">
        </p>
        <p class="addvise-hist-datepicker"> au : <input id="datepicker02" class="datepicker-hist form-control"></p>
        <button class="btn btn-action vaderBlue" type="submit" name="action" id="btnMonthPicker" value="">Valider</button>
    </div>
    <div id="addvise-tri-modules">
        Durant cette période :
        <c:set var="count" value="0"/>
        <c:forEach items="${stats}" var="module">
            <c:set var="count" value="${count + module.second}"/>
            ${module.first} : ${module.second} |
        </c:forEach>
        Tous : ${count}
    </div>

    <div class="form-group">
        <form class="navbar-form navbar-right inline-form">
            <input type="search" class="form-control" placeholder="Recherche" id="addvise-chercher">
        </form>
    </div>

    <!-- Table de saisie des sessions -->
    <div id="scrolltab">
        <table id="table-addvise-gestion"
               class="table table-responsive table-container"
               data-toggle="table"
               data-toolbar="#toolbar">
            <thead id="thead-addvise-gestion">
            <tr>
                <!-- Collobalateurs -->
                <th id="firstColHeader">
                    <div class="cell-addvise-1"></div>
                    <div class="addvise-division-col">Collaborateur</div>
                    <img class="sort-alphabetically-white"
                         src="${contextPath}/resources/images/sort-alphabetically.png">
                    <img class="icone_date addvise-tri" id="addvise-tri-alpha"
                         src="${contextPath}/resources/images/sort-down-triangular-symbol.png">
                    <img class="sort-clock"
                         src="${contextPath}/resources/images/clock.png">
                    <img class="icone_date addvise-tri" id="addvise-tri-date-entree"
                         src="${contextPath}/resources/images/sort-down-triangular-symbol.png">
                </th>
                <!-- Sessions -->
                <c:forEach items="${listeSessionsPassees}" var="session">
                    <th class="session-hist cell-addvise-hist-${session.etat}">
                        <div class="cell-addvise-hist">
                            <div>${session.moduleAddvise.codeModuleAddvise}
                                le <fmt:formatDate value="${session.date}" pattern="dd/MM/yyyy"/> à <span
                                    class="cell-addvise-dateSession-heure session-id-${session.id}"><fmt:formatDate
                                    value="${session.date}" pattern="HH"/>:</span><span
                                    class="cell-addvise-dateSession-minutes session-id-${session.id}"><fmt:formatDate
                                    value="${session.date}" pattern="mm"/></span></div>
                            <div>${session.formateur.nom} ${session.formateur.prenom}</div>
                            <form action="Administration/addvise/restoreSession" method="post"
                                  class="addvise-form-session form-restore-session">
                                <a type="button" data-backdrop="static"
                                   class="btn btn-sm btn-link btn-restore-session-hist" data-toggle="modal">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <input type="hidden" name="idSession" value="${session.id}"/>
                            </form>
                            <span class="countConfirmSpan">0</span>
                            <img class="icone_2 countConfirm" src="${contextPath}/resources/images/addvise/checked.gif">
                        </div>
                    </th>
                </c:forEach>
                <th class="session-hist-dernier">
                    <div class="addvise-multiple-header cell-addvise-2 last-session-addvise">
                        <div>Dernière session réalisée</div>
                        <div class="addvise-multiple-col">
                            <div class="form-group">
                                <select class="form-control" style="width:auto;" id="addvise-filtrer">
                                    <option value="">Tous</option>
                                    <option value="0">Aucun</option>
                                    <c:forEach items="${listeModules}" var="module">
                                        <option value="${module.id}">${module.codeModuleAddvise}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="date">Date
                                <img class="icone_date addvise-tri" id="addvise-tri-date-dernier-module"
                                     src="${contextPath}/resources/images/sort-down-triangular-symbol.png">
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:set var="count" value="0"/>
            <c:forEach items="${listeCollaborateurs}" var="collaborateur">
                <c:set var="count" value="${count + 1}"/>
                <tr class="addvise-row">
                    <!-- collaborateur -->
                    <td class="firstCol">
                        <span class="tooltip-entree exclus-addvise-${collaborateur.exclusAddvise}"
                              data-toggle="tooltip" data-placement="right" title="Entrée :
                                <fmt:formatDate value="${collaborateur.dateEntree}" pattern="dd/MM/yyyy"/>">
                                        ${collaborateur.nom} ${collaborateur.prenom}
                        </span>
                    </td>
                    <c:forEach items="${listeSessionsPassees}" var="session">
                        <td class="contentCol">
                            <div class="cell-addvise-hist">
                                <div class="invitation-addvise-hist session-id-${session.id}">
                                    <c:forEach items="${session.listeLinkSessionCollaborateur}" var="invit">
                                        <c:if test="${invit.collaborateur == collaborateur}">
                                            <div class="icones_session">
                                                <c:if test="${invit.etatInvitation == 'CONFIRMEE'}">
                                                <span class="icone_2 icone icone-hist">
                                                    <img class="icone_2"
                                                         src="${contextPath}/resources/images/addvise/checked.gif">
                                                </span>
                                                </c:if>
                                                <c:if test="${invit.etatInvitation == 'DECLINEE'}">
                                                <span class="icone_3 icone icone-hist">
                                                    <img class="icone_3"
                                                         src="${contextPath}/resources/images/addvise/unchecked.gif">
                                                </span>
                                                </c:if>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </td>
                    </c:forEach>
                    <td class="contentCol">
                        <div class="addvise-multiple-col cell-addvise-2">
                            <!-- module -->
                            <div>${collaborateur.lastAddviseSession.moduleAddvise.codeModuleAddvise}</div>
                            <!-- date -->
                            <div>
                                <fmt:formatDate value="${collaborateur.lastAddviseSession.date}"
                                                pattern="dd/MM/yyyy"/>
                            </div>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div id="nb-collab">
            <span class="addvise-nb-collab">Nombre de collaborateurs : </span>
            <span class="addvise-nb-collab">${count}</span>
        </div>
    </div>
    <div id="addvise-legende">
        <div class="addvise-legend-bloc">
            <div class="legende">
                <img class="img-legende" src="${contextPath}/resources/images/addvise/checked.gif"> Confirmé
            </div>
            <div class="legende">
                <img class="img-legende" src="${contextPath}/resources/images/addvise/unchecked.gif"> Décliné
            </div>
        </div>
        <div class="addvise-legend-bloc">
            <div class="legende">
                <div class="img-legende img-legende-bleu"></div>
                Session validée
            </div>
            <div class="legende">
                <div class="img-legende img-legende-rouge"></div>
                Session annulée
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modale-restore-session" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modifier la session ?</h4>
            </div>
            <div class="modal-body">
                <p class="modal-remove-session">Voulez-vous modifier cette session ?
                    Si vous confirmez, la session retournera dans la page de gestion et ne sera plus dans
                    l'historique.</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-modale-restore-session" class="btn btn-success">Oui</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
            </div>
        </div>
    </div>
</div>

