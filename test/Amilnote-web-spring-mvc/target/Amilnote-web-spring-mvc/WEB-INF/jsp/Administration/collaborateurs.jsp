<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--
  ~ ©Amiltone 2017
  --%>
<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Collaborateurs</li>
</ol>

<h3>Liste des collaborateurs</h3>
<div id="toolbar2" class="row">
    <div class="col-md-3 offset-md-1">
        <a href="${administration}${collaborateurTypeMission}"
           class="btn btn-success">Type de mission par collaborateur</a>
    </div>
    <div id="checkboxDiv" class="col-md-4 " style="padding-left: 15px ; padding-right: 15px">
        <label>Afficher les collaborateurs désactivés : </label>
        <c:choose>
            <c:when test="${isChecked}">
                <input type="checkbox" onclick='handleClick(this, "collaborateurs", "");' checked>
            </c:when>
            <c:otherwise>
                <input type="checkbox" onclick='handleClick(this, "collaborateurs", "");'>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="col-md-3 offset-md-1">
        <input class="form-control col-md-3" type="text" id="myInput" onkeyup="filterCollaborators()"
               placeholder="Recherche d'un collaborateur">
    </div>
</div>
<div id="idTableContainer">
    <div id="toolbar">
        <a href="${administration}${editCollaborateur}"
           class="btn btn-success">Nouveau collaborateur</a>
    </div>
</div>

<div id="scrolltab">
    <table class="table"
           data-toggle="table"
           data-height="520"
           data-striped="true"
           data-toolbar="#toolbar"
           data-pagination="true"
           data-page-size=100
           data-page-list="[10, 25, 50, 100, ${listeCollaborateurs.size()}]"
           data-pagination-detail-v-align="top"
           data-pagination-v-align="top"
           data-pagination-detail-h-align="left"
           data-pagination-h-align="right"
           id="idBootstrapTable"
           data-cookie="true"
           data-cookie-id-table="saveIdCollab">
        <thead>
        <tr>
            <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center" data-search="true">Nom</th>
            <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center" data-search="true">
                Prénom
            </th>
            <th class="col-md-1" data-field="mail" data-sortable="true" data-align="center">Mail</th>
            <th class="col-md-1" data-field="droit" data-sortable="true" data-align="center">Droit</th>
            <th class="col-md-1" data-field="state" data-sortable="true" data-align="center">État</th>
            <th class="col-md-1" data-align="center">Édition</th>
            <th class="col-md-1" data-align="center">Désactiver</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${listeCollaborateurs}" var="collaborateur">
            <tr>
                <td>${collaborateur.nom}</td>
                <td>${collaborateur.prenom}</td>
                <td>${collaborateur.mail}</td>
                <td>${collaborateur.statut.statut}</td>
                <td>
                    <c:choose>
                        <c:when test="${collaborateur.enabled}">
                            Activé
                        </c:when>
                        <c:otherwise>
                            Désactivé
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${collaborateur.enabled}">
                            <a href="${administration}${editCollaborateur}/${collaborateur.id}/null">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${collaborateur.enabled}">
                            <a data-title="Désactivation" title="Désactiver le collaborateur" href=""
                               onclick="return openModalDateSortie(
                                       'warning-delete-Action',
                                       '${collaborateur.id}',
                                       'idCollab',
                                       'disabledCollabForm',
                                       'idTableContainer')">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <div class="modal fade" id="warning-delete-Action" tabindex="-1"
                 role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Désactivation d'un collaborateur</h3>
                        </div>
                        <div class="modal-body" id="warning-delete-Collaborateur-body">
                            <h4>Êtes-vous sûr de vouloir désactiver ce collaborateur ?</h4>
                            <h5>Attention : Cette action bloquera l'accès à l'application pour ce
                                collaborateur.</h5>
                            <br>
                            <form name="disabledCollabForm" id="disabledCollabForm" method="POST"
                                  action="${administration}${deleteCollaborateur}/${collaborateur.id}">
                                <label class="col-md-4 control-label" for="dteSortie">Date de sortie :</label>
                                <input id="dteSortie" name="dteSortie" required="required"
                                       data-validation-format="yyyy-mm-dd"
                                       data-validation="date"
                                       data-toggle="validator" type="text" class="form-control input-md datepicker"
                                       value="" autocomplete="off"/>
                                <span id="msgDateManquante" style="color:red; display:none">La date de sortie est obligatoire !</span>
                                <div class="modal-footer">
                                    <button type="submit" id="delete-Action" onclick="return validateFormDelete();"
                                            class="btn btn-success dismiss" data-dismiss="modal">Oui
                                    </button>
                                    <button type="button" class="btn btn-danger dismiss"
                                            data-dismiss="modal"
                                            onclick="document.getElementById('msgDateManquante').style.display='none'">
                                        Non
                                    </button>
                                </div>
                                <input type="hidden" id="idCollab" name="idCollab" value="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
        </tbody>
    </table>
</div>