<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--<%@ page contentType="text/html; charset=UTF-8" %>--%>
<%--<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<%--<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>--%>
<%--&lt;%&ndash;--%>
  <%--~ ©Amiltone 2017--%>
  <%--&ndash;%&gt;--%>

<%--<script>--%>
    <%--/*--%>
     <%--$(function() {--%>
     <%--var total = 0;--%>
     <%--$(".montant").each(function(){ // Pour tous les élément ayant la classe "montant"--%>
     <%--total = total*1 + $(this).val()*1;// On récupère la valeur et on calcul le total--%>
     <%--});--%>
     <%--$("#ht").val(total); // Mise à jour de la valeur des input--%>
     <%--var tvaMontant = total*${tva}; // on recalcul la TVA--%>
     <%--$("#tva").val(tvaMontant); // On affiche la TVA--%>
     <%--$("#total").val(total + tvaMontant);// On affiche le montant TTC--%>
     <%--});--%>
     <%--*/--%>

<%--</script>--%>


<%--<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>--%>
    <%--<button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>--%>
    <%--<p> -- </p>--%>
<%--</section>--%>
<%--<script type="text/javascript" src='resources/js/Administration/editFacture.js'></script>--%>

<%--<ol class="breadcrumb">--%>
    <%--<li><a href="${ welcome }">Accueil</a></li>--%>
    <%--<li><a href="${ welcome }">Administration</a></li>--%>
    <%--<li><a href="${administration}${factures}/null">Factures</a></li>--%>
    <%--<li class="active">Edition de la facture vierge</li>--%>
<%--</ol>--%>

<%--<div id="idProfilContainer">--%>
    <%--<form:form id="creationFactureVierge" class="form-horizontal" method="POST"--%>
               <%--action="" modelAttribute="facture">--%>
        <%--<fieldset>--%>
            <%--<legend class="text-center">--%>
                <%--<c:choose>--%>
                    <%--<c:when test="${facture.id != null && facture.id  != 0}">--%>
                        <%--Edition de la Facture N°<i>${facture.id}</i>--%>

                    <%--</c:when>--%>
                    <%--<c:otherwise>--%>
                        <%--Création d'une facture vierge--%>
                    <%--</c:otherwise>--%>
                <%--</c:choose>--%>
            <%--</legend>--%>
            <%--<form:hidden id="id" path="id" value="${ facture.id }"/>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 160] Date de facturation &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="dateFacturation">Lyon, le</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="dateFacturation"--%>
                                <%--path=""--%>
                                <%--type="text"--%>
                                <%--name="dateFacturation"--%>
                                <%--class="form-control input-md datepicker"--%>
                                <%--placeholder=" YYYY-MM-DD Date facturation"--%>
                                <%--pattern="^[0-9]{4}-[0-9]{2}-[0-9]{2}$"--%>
                                <%--TITLE="Le format de date n'est pas valide (YYYY-MM-DD)"--%>
                                <%--value="${ facture.dateFacture }"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>

            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="client">Client</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="client" path="" type="text" name="client"--%>
                                <%--placeholder="Client" class="form-control input-md"--%>
                                <%--value=""--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] Liste des contacts pour le client de la facture &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="contact">A l'intention de :</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="contact" path="" type="text" name="contact"--%>
                                <%--placeholder="Contact" class="form-control input-md"--%>
                                <%--value=""--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>


                <%--&lt;%&ndash;[JNA][AMNOTE 142] Adresse du client de la facture&ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="adresse">Adresse</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="adresse" path="" type="text" name="adresse"--%>
                                <%--placeholder="Adresse" class="form-control input-md"--%>
                                <%--value=""--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] Adresse de facturation si différente de l'adresse du client &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="adresse">Adresse de facturation si--%>
                    <%--différente</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="adresseFacturation" path="" type="text" name="adresseFacturation"--%>
                                <%--placeholder="Adresse" class="form-control input-md"--%>
                                <%--value="${facture.adresseFacturation}"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<br><br>--%>
                <%--&lt;%&ndash;[JNA][AMNOTE 142] N° TVA Amiltone&ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-4 col-md-offset-4 control-label" for="adresse">Notre N° TVA Intrac :</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="adresse" path="" type="text" name="adresse"--%>
                                <%--placeholder="Notre N° TVA Intrac" class="form-control input-md"--%>
                                <%--value="FR 235 389 491 08"--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] N° SIRET Amiltone &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-1 control-label" for="adresse">N° Siret :</label>--%>
                <%--<div class="col-md-3">--%>
                    <%--<form:input id="adresse" path="" type="text" name="adresse"--%>
                                <%--placeholder="N° Siret" class="form-control input-md"--%>
                                <%--value="53894910800020"--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>

                <%--</div>--%>

                    <%--&lt;%&ndash;[JNA][AMNOTE 142] N° TVA Client&ndash;%&gt;--%>
                <%--<label class="col-md-4 control-label" for="adresse">Votre N° TVA Intrac :</label>--%>
                <%--<div class="col-md-4">--%>
                    <%--<form:input id="adresse" path="" type="text" name="adresse"--%>
                                <%--placeholder="Votre N° TVA Intrac" class="form-control input-md"--%>
                                <%--value=""--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>

                <%--</div>--%>
            <%--</div>--%>

            <%--<br><br>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] Titre du tableau de la facture &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<label class="col-md-3 control-label">Désignation</label>--%>
                <%--<label class="col-md-3 control-label">Quantité</label>--%>
                <%--<label class="col-md-3 control-label">P.U.</label>--%>
                <%--<label class="col-md-2 control-label">Montant € HT</label>--%>
            <%--</div>--%>

            <%--<br><br>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] Num commande de la facture &ndash;%&gt;--%>
            <%--<div class="form-group">--%>
                <%--<div class="col-md-4">--%>
                    <%--<label class="col-md-4 control-label" for="commentaire">Commentaire :</label>--%>
                    <%--<div class="col-md-8">--%>
                         <%--<textarea id="commentaire" path="commentaire" name="commentaire"--%>
                                   <%--placeholder="Commentaire"--%>
                                   <%--class="form-control input-md">${facture.commentaire}</textarea>--%>
                    <%--</div>--%>
                <%--</div>--%>

                <%--<div class="col-md-3">--%>
                    <%--<form:input id="nbJours" path="" type="number" name="nbJours"--%>
                                <%--placeholder="NB jours" class="form-control input-md"--%>
                                <%--value="${facture.quantite}"--%>
                                <%--onchange="loadMontant(${tva})"--%>
                                <%--step="any"--%>
                    <%--/>--%>

                <%--</div>--%>
                <%--<div class="col-md-3">--%>
                    <%--<form:input id="tjm" path="" type="number" name="mission.tjm"--%>
                                <%--placeholder="TJM" class="form-control input-md"--%>
                                <%--value="${facture.prix}"--%>
                                <%--onchange="loadMontant(${tva})"--%>
                                <%--step="any"--%>
                    <%--/>--%>
                <%--</div>--%>
                <%--<div class="col-md-2">--%>
                    <%--<form:input id="montant" path="montant" type="number" name="montantFacture"--%>
                                <%--placeholder="Montant" class="form-control input-md montant"--%>
                                <%--value="${facture.montant}"--%>
                                <%--readOnly="true"--%>
                                <%--step="any"--%>
                    <%--/>--%>
                <%--</div>--%>
            <%--</div>--%>

                <%--&lt;%&ndash;[JNA][AMNOTE 142] Commentaire de la facture &ndash;%&gt;--%>

            <%--<br>--%>
                <%--&lt;%&ndash;[JNA][AMNOTE 142] Bouton frais &ndash;%&gt;--%>
            <%--<div class="form-group ajoutPrestations">--%>
                <%--<label class="col-md-2 control-label">Prestation du mois de :</label>--%>
                <%--<div class="col-md-3 date form_time">--%>
                    <%--<form:input id="moisPrestations"--%>
                                <%--path=""--%>
                                <%--type="text"--%>
                                <%--name="moisPrestations"--%>
                                <%--class="form-control input-md datepicker"--%>
                                <%--placeholder="Date de facturation"--%>
                                <%--data-validation="date"--%>
                                <%--data-validation-format="yyyy-mm-dd"--%>
                                <%--value="${ facture.moisPrestations }"--%>
                    <%--/>--%>
                <%--</div>--%>
                <%--<label class="col-md-2 control-label">Ajouter une prestation :</label>--%>
                <%--<input type="button" id="button1id"--%>
                       <%--value="Ajouter" class="btn btn-success" onclick="loadFrais(${tva})"/>--%>
            <%--</div>--%>

                <%--&lt;%&ndash; &lt;%&ndash;[JNA][AMNOTE 142] Div contenant tous les frais, rempli par le JS et/ou par frais existant &ndash;%&gt;--%>
                    <%--<div id="frais" class="form-group">--%>
                        <%--<c:forEach items="${listeFrais}" var="frais" varStatus="myIndex">&lt;%&ndash; On affiche les frais de la facture &ndash;%&gt;--%>

                            <%--<div id="${frais.idElementFacture}" class="form-group ligneFrais"><label class="col-md-1 control-label" for="libelle${myIndex.index + 1}">Libellé :</label>--%>
                            <%--<div class="col-md-3">--%>
                                <%--<form:input id="libelle${myIndex.index + 1}" path="" type="text" name="libelle${myIndex.index + 1}"--%>
                                            <%--placeholder="Libellé" class="form-control input-md"--%>
                                            <%--value = "${frais.libelleElement}"--%>
                                            <%--required = "true"--%>
                                <%--/>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-3">--%>
                                <%--<form:input id="quantite${myIndex.index + 1}" path="" type="number" name="quantite${myIndex.index + 1}"--%>
                                            <%--placeholder="Quantité" class="form-control input-md"--%>
                                            <%--value = "${frais.quantiteElement}"--%>
                                            <%--step="any"--%>
                                <%--/>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-3">--%>
                                <%--<form:input id="pu${myIndex.index + 1}" path="" type="number" name="pu${myIndex.index + 1}"--%>
                                            <%--placeholder="Prix unitaire" class="form-control input-md"--%>
                                            <%--value = "${frais.prixElement}"--%>
                                            <%--step="any"--%>
                                <%--/>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-1">--%>
                                <%--<form:input id="montant${myIndex.index + 1}" path="" type="number" name="montant${myIndex.index + 1}"--%>
                                            <%--placeholder="Montant" class="form-control input-md montant"--%>
                                            <%--value = "${frais.montantElement}"--%>
                                            <%--onchange="loadTotal(${tva})"--%>
                                            <%--step="any"--%>
                                <%--/>--%>
                            <%--</div>--%>
                                <%--<form:hidden name = "idFrais${myIndex.index + 1}" path="" value = "${frais.idElementFacture}" />--%>
                            <%--<div class="col-md-1">--%>
                                <%--<button type="button" class="btn btn-default" ><span onclick="deletInputFrais(${frais.idElementFacture},${tva})" class="glyphicon glyphicon-remove text-danger" style="font-size: 20px;"></span></button>--%>
                            <%--</div></div>--%>



                        <%--</c:forEach>--%>


                    <%--</div>--%>

            <%--<br><br>--%>
                <%--<div class="form-group">--%>
                    <%--<label class="col-md-1 control-label">RIB :</label>--%>

                    <%--<div class="col-md-5">--%>
                            <%--<form:select name="ribFacture" class="form-control" path="" id = "ribFacture" placeholder="Sélectionner un moyen de payement" itemLabel="conditionPaiement" required="true">--%>
                            <%--<c:forEach items="${listeRIB}" var="rib">--%>
                                <%--<form:option value="${rib}">${rib}</form:option>--%>
                            <%--</c:forEach>--%>
                            <%--</form:select>--%>
                    <%--</div>--%>

                    <%--<label class="col-md-4 control-label" for="ht">Total H.T :</label>--%>

                    <%--<div class="col-md-2">--%>
                            <%--<form:input id="ht" path="" type="text" name="ht"--%>
                                        <%--placeholder="Adresse" class="form-control input-md"--%>
                                        <%--value = "${commande.montant}"--%>
                                        <%--readOnly="true"--%>
                            <%--/>--%>
                    <%--</div>--%>
                <%--</div>--%>

            <%--&lt;%&ndash;[JNA][AMNOTE 142] Montant TVA&ndash;%&gt;--%>
                <%--<div class="form-group">--%>
                    <%--<label class="col-md-4 col-md-offset-6 control-label" for="tva">T.V.A (${tva}) :</label>--%>

                    <%--<div class="col-md-2">--%>
                        <%--<form:input id="tva" path="" type="text" name="tva"--%>
                                    <%--placeholder="Adresse" class="form-control input-md"--%>
                                    <%--value = "${commande.montant*tva}"--%>
                                    <%--readOnly="true"--%>
                        <%--/>--%>
                    <%--</div>--%>
                <%--</div>--%>

           <%--&lt;%&ndash; [JNA][AMNOTE 142] Montant Total TTC &ndash;%&gt;--%>
        <%--<div class="form-group">--%>
            <%--<label class="col-md-4 col-md-offset-6 control-label" for="total">Total T.T.C :</label>--%>

            <%--<div class="col-md-2">--%>
                    <%--<form:input id="total" path="" type="text" name="total"--%>
                                <%--placeholder="Adresse" class="form-control input-md"--%>
                                <%--value = "${commande.montant + commande.montant*(tva)}"--%>
                                <%--readOnly="true"--%>
                    <%--/>--%>
            <%--</div>--%>
        <%--</div>--%>

        <%--<!-- Button (Double) -->--%>
        <%--<br><br>--%>
        <%--<div class="form-group">--%>
            <%--<label class="col-md-4 control-label" for="button1id"></label>--%>
            <%--<div class="col-md-8">--%>
                <%--<a class="btn btn-danger"--%>
                    <%--href="${administration}${factures}"--%>
                    <%--role="button">Retour</a> <input type="submit" id="button1id"--%>
                    <%--value="Valider" class="btn btn-success" onclick="saveFacture()"/>--%>
                <%--<a class="btn btn-info"--%>
                        <%--onclick="visuPdfFacture()" target="_blank"--%>
                   <%-->Visualisez votre facture</a>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--&ndash;%&gt;--%>
        <%--</fieldset>--%>
    <%--</form:form>--%>
    <%--<script type="text/javascript">--%>
        <%--function checksubmit(inputAdresse, inputDate) {--%>
            <%--var inputAdresse = inputAdresse.value--%>
            <%--var inputDate = inputDate--%>
            <%--if (inputAdresse.contains("_")){--%>
                <%--if (inputDate!==null){--%>
                    <%--if (inputDate.charAt(4)==="-"){--%>
                        <%--if(inputDate.charAt(7)==="-"){--%>
                            <%--saveFacture()--%>
                        <%--}--%>
                        <%--return false--%>
                    <%--}--%>
                    <%--return false--%>
                <%--}--%>
                <%--return false--%>
            <%--}--%>
            <%--return false--%>
        <%--}--%>
    <%--</script>--%>
<%--</div>--%>
