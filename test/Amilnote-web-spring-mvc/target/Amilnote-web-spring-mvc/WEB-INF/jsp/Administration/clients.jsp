<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--
  ~ ©Amiltone 2017
  --%>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active">Clients</li>
</ol>

<div id="idTableContainer">
    <h3>Liste des clients</h3>

    <div id="toolbar">
        <a href="${administration}${editClient}"
           class="btn btn-success">Nouveau client</a>
    </div>
    <div id="scrolltab">
        <table class="table"
               data-toggle="table"
               data-height="520"
               data-search="true"
               data-striped="true"
               data-toolbar="#toolbar"
               data-pagination="true"
               data-page-size=50
               data-page-list="[10, 25, 50, 100, ${listClients.size()}]"
               data-pagination-v-align="top"
               data-pagination-h-align="right"
               data-pagination-detail-h-align="left"
               id="idBootstrapTable"
               data-cookie="true"
               data-cookie-id-table="saveIdClients">
            <thead>
            <tr>
                <th data-field="nom" data-sortable="true" data-align="center">Nom</th>
                <th data-field="adresse" data-sortable="true" data-align="center">Adresse de facturation</th>
                <th data-align="center">Liste des contacts</th>
                <th data-align="center">Édition</th>
                <th data-align="center">Supprimer</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${listClients}" var="client">
                <tr>
                    <td>${client.nom_societe}</td>
                    <td>${client.adresse_facturation}</td>
                    <td>
                        <c:forEach items="${client.contacts}" var="contact">
                            <div>
                                <span class="glyphicon glyphicon-user"> </span>
                                    ${contact.nom} ${contact.prenom}
                                <div style="font-size:13; font-style: italic; display:inline"> (${contact.mail})</div>
                            </div>
                        </c:forEach>
                    </td>
                    <td>
                        <a href="${administration}${editClient}/${client.id}">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a onclick="eventClikDelate(urlDelete + ${client.id})">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="warning-delete-Action" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Suppression d'un client</h3>
            </div>
            <div class="modal-body" id="warning-delete-Collaborateur-body">
                <h4>Êtes-vous sûr de vouloir supprimer ce client ?</h4>
                <h5><i>Attention, tous les contacts de ce client seront supprimés.<br>Vérifiez qu'ils ne soient pas
                    associés à une mission en cours.</i></h5>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-Action"
                        class="btn btn-success dismiss" data-dismiss="modal">Oui
                </button>
                <button type="button" class="btn btn-danger dismiss"
                        data-dismiss="modal">Non
                </button>
            </div>

        </div>
    </div>

</div>
<script src="resources/js/Administration/clients.js"></script>