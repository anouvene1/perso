<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- ~ ©Amiltone 2017 --%>

<script type="text/javascript" src='resources/js/Administration/pdfMultipleRA.js'></script>
<script type="text/javascript" src='resources/js/modalCommentaire.js'></script>
<script type="text/javascript" src='resources/js/Administration/validationRA.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <li class="active" id="filtre">Consultation des rapports d'activités</li>
</ol>

<div id="idTableContainer" style="margin-bottom: 40px;">
    <h3>Consultation des rapports d'activités</h3>

    <div id="menuRA" class="nav nav-tabs">
        <ul class="nav nav-pills col-md-12">
            <li class="nav-item col-md-2 text-center active">
                <a class="nav-link" href="#tabRAcollab" aria-pressed="true" data-toggle="tab" style="font-weight:bold ;font-size: small" onclick="showCollab()">RA
                    collaborateurs</a>
            </li>
            <li class="nav-item col-md-2 text-center">
                <a class="nav-link" href="#tabRAstt" data-toggle="tab" style="font-weight:bold ;font-size: small" onclick="showSTT()">RA
                    sous-traitants</a>
            </li>
        </ul>
    </div>

    <a href="${administration}${consultationRA}/null" id="lienFiltre1">Tous</a>
    <a href="${administration}${consultationRA}/BR" id="lienFiltre2">Brouillons</a>
    <a href="${administration}${consultationRA}/SO" id="lienFiltre3">Soumis</a>
    <a href="${administration}${consultationRA}/AN" id="lienFiltre5">Annulés</a>

    <form id="formFiltre" method="GET">
        <div class="form-group" style="margin-top:27px">
            <input name="monthYearExtract"
                   id="monthExtract"
                   class="monthPicker form-control"
                   placeholder="MM/AAAA"
                   autocomplete="off"
                   value="${monthYearExtract}">

            <a onclick="annulerFiltre()" class="annuleFiltre">Annuler filtre</a>
        </div>
    </form>

    <div id="mini-scrolltab">
        <div class="tab-pane active" id="tabRAcollab">
            <table class="table"
                   data-toggle="table"
                   data-height="520"
                   data-click-to-select="true"
                   data-search="true"
                   data-striped="true"
                   data-pagination="true"
                   data-page-size=50
                   data-page-list="[10, 25, 50, 100, ${listRA.size()}]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="right"
                   data-pagination-detail-h-align="left"
                   data-toolbar="#toolbar"
                   id="idBootstrapTable1"
                   data-maintain-selected="true"
                   data-cookie="true"
                   data-cookie-id-table="saveIdConsultRA"
                   onchange="changeCollabPdfBtnState()">
                <thead>
                <tr>
                    <th class="col-md-1" data-field="state" data-checkbox="true" data-align="center"></th>
                    <th class="col-md-1" data-field="id" data-visible="false">id</th>
                    <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                    <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                    <th class="col-md-1" data-field="mois" data-sortable="true" data-sorter="MMyyyyCustomSorter"
                        data-align="center">Mois concerné
                    </th>
                    <th class="col-md-1" data-field="date" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                        data-align="center">Date de soumission
                    </th>
                    <th class="col-md-1" data-field="etat" data-sortable="true" data-align="center">État</th>
                    <th class="col-md-1" data-field="commentaire" data-sortable="true" data-align="center">
                        Commentaire
                    </th>
                    <th class="col-md-1" data-align="center">Visualiser</th>
                    <th class="col-md-1" data-align="center">Annuler</th>
                </tr>
                </thead>
                <tbody onclick="changeCollabPdfBtnState()">
                <c:forEach items="${listRAcollab}" var="rapportActiviteCollab">
                    <tr>
                        <td></td>
                        <td>${rapportActiviteCollab.id}</td>
                        <td>${rapportActiviteCollab.collaborateur.prenom}</td>
                        <td>${rapportActiviteCollab.collaborateur.nom}</td>
                        <td>
                            <fmt:formatDate value="${rapportActiviteCollab.moisRapport}" pattern="MM/yyyy"/></td>
                        <td>
                            <fmt:formatDate value="${rapportActiviteCollab.dateSoumission}" pattern="dd/MM/yyyy"/>
                        </td>
                        <td>
                                ${rapportActiviteCollab.etat.etat}
                        </td>
                        <td>${rapportActiviteCollab.commentaire}</td>
                        <td>
                            <c:choose>
                                <c:when test="${! empty rapportActiviteCollab.pdfRapportActivite }">
                                    <a href="${administration}${validationsRA}/${rapportActiviteCollab.id}${download}"
                                       target="_blank">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-eye-close"></span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:if
                                test="${rapportActiviteCollab.etat.code != 'VA' && rapportActiviteCollab.etat.code != 'AN'}">
                                <a data-title="Annulation du rapport d'activité"
                                   onclick="annulerRA(${rapportActiviteCollab.id})" data-href="">
                                    <span class="glyphicon glyphicon-ban-circle"></span>
                                </a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <div class="navbar-form navbar-left" style="margin-bottom: 30px">

                <input
                    id="btnCreationMultiplePdfFirstPageCollab"
                    readonly="readonly"
                    class="btn btn-default btnCreationMultiplePdf"
                    value="Générer PDF sans frais"
                    disabled="disabled"
                    data-action="${administration}${creationPdfMultiplesRA}/firstpage"
                    data-raid=${rapportActiviteCollab.id}>

                <input
                    id="btnCreationMultiplePdfAllPagesCollab"
                    readonly="readonly"
                    class="btn btn-default btnCreationMultiplePdf"
                    value="Générer PDF avec frais soumis"
                    disabled="disabled"
                    style="width: 240px"
                    data-action="${administration}${creationPdfMultiplesRA}/allpages"
                    data-raid=${rapportActiviteCollab.id}>
            </div>
        </div>

        <div class="tab-pane" id="tabRAstt" style="display: none">
            <table class="table"
                   data-toggle="table"
                   data-height="520"
                   data-click-to-select="true"
                   data-search="true"
                   data-striped="true"
                   data-pagination="true"
                   data-page-size=50
                   data-page-list="[10, 25, 50, 100, ${listRAstt.size()}]"
                   data-pagination-v-align="top"
                   data-pagination-h-align="right"
                   data-pagination-detail-h-align="left"
                   data-toolbar="#toolbar"
                   id="idBootstrapTable2"
                   data-maintain-selected="true"
                   data-cookie="true"
                   data-cookie-id-table="saveIdConsultRA"
                   onchange="changeSttPdfBtnState()">
                <thead>
                <tr>
                    <th class="col-md-1" data-field="state" data-checkbox="true" data-align="center"></th>
                    <th class="col-md-1" data-field="id" data-visible="false">id</th>
                    <th class="col-md-1" data-field="prenom" data-sortable="true" data-align="center">Prénom</th>
                    <th class="col-md-1" data-field="nom" data-sortable="true" data-align="center">Nom</th>
                    <th class="col-md-1" data-field="mois" data-sortable="true" data-sorter="MMyyyyCustomSorter"
                        data-align="center">Mois concerné
                    </th>
                    <th class="col-md-1" data-field="date" data-sortable="true" data-sorter="ddMMyyyyCustomSorter"
                        data-align="center">Date de soumission
                    </th>
                    <th class="col-md-1" data-field="etat" data-sortable="true" data-align="center">État</th>
                    <th class="col-md-1" data-field="commentaire" data-sortable="true" data-align="center">Commentaire
                    </th>
                    <th class="col-md-1" data-align="center">Visualiser</th>
                    <th class="col-md-1" data-align="center">Annuler</th>
                </tr>
                </thead>
                <tbody onclick="changeSttPdfBtnState()">
                <c:forEach items="${listRAstt}" var="rapportActiviteSTT">
                    <tr>
                        <td></td>
                        <td>${rapportActiviteSTT.id}</td>
                        <td>${rapportActiviteSTT.collaborateur.prenom}</td>
                        <td>${rapportActiviteSTT.collaborateur.nom}</td>
                        <td>
                            <fmt:formatDate value="${rapportActiviteSTT.moisRapport}" pattern="MM/yyyy"/></td>
                        <td>
                            <fmt:formatDate value="${rapportActiviteSTT.dateSoumission}" pattern="dd/MM/yyyy"/>
                        </td>
                        <td>${rapportActiviteSTT.etat.etat}</td>
                        <td>${rapportActiviteSTT.commentaire}</td>
                        <td>
                            <c:choose>
                                <c:when test="${! empty rapportActiviteSTT.pdfRapportActivite }">
                                    <a href="${administration}${validationsRA}/${rapportActiviteSTT.id}${download}"
                                       target="_blank">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <span class="glyphicon glyphicon-eye-close"></span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:if
                                test="${rapportActiviteSTT.etat.code != 'VA' && rapportActiviteSTT.etat.code != 'AN'}">
                                <a data-title="Annulation du rapport d'activité"
                                   onclick="annulerRA(${rapportActiviteSTT.id})" data-href="">
                                    <span class="glyphicon glyphicon-ban-circle"></span>
                                </a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="navbar-form navbar-left" style="margin-bottom: 30px">

                <input
                    id="btnCreationMultiplePdfFirstPageStt"
                    readonly="readonly"
                    class="btn btn-default btnCreationMultiplePdf"
                    value="Générer PDF sans frais"
                    disabled="disabled"
                    data-action="${administration}${creationPdfMultiplesRA}/firstpage"
                    data-raid=${rapportActiviteSTT.id}>
            </div>
        </div>
    </div>
</div>

<div
    id="modalAnnulationRA"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <form action="${administration}${validationsRA}/AN">
                <div class="modal-header">
                    <h3>Annulation du rapport d'activité</h3>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Commentaire : </label><br>
                        <textarea name="commentaire" data-validation="length" data-validation-length="max255"
                                  class="form-control" rows="5"></textarea>
                        <input type="hidden" name="idRA" id="idRA" value="">
                        <input type="hidden" name="filtreRA" id="filtreRA" value="">
                        <input type="hidden" name="filtreDate" id="filtreDate" value="">
                    </div>
                </div>

                <div class="modal-footer">
                    <button id="idConfirmCreateExcels" type="submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>
                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>
                </div>
                <input id="dateFiltre" name="dateFiltre" hidden="true"></input>
            </form>
        </div>
    </div>
</div>
