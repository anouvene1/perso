<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%--
  ~ ©Amiltone 2017
  --%>

<script>
    function getSelectedForfait2(sel) {
        var selectedForfaitOption = $('#idListForfaits2').find('option:selected');

        if (selectedForfaitOption.attr("id").indexOf('LIB') !== -1) {

            $('#libelle').attr("value", selectedForfaitOption.attr("name"));

            $('#libelleForfaitLibre2').attr('style', 'display: block');
            $('#montantForfait2').attr('style', 'display: none');
        }
        else {
            $('#libelleForfaitLibre2').attr('style', 'display: none');
            $('#montantForfait2').attr('style', 'display: block');
        }
    }

    function remplaceVirgule(input) {
        input.value = input.value.replace(',', '.');
    }
</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">Accueil</a></li>
    <li><a href="${ welcome }">Administration</a></li>
    <c:choose>
        <c:when test="${collaborateur.statut.id=='7'}">
            <li><a href="${ administration }${ travailleurs }?type=idTableContainerSS">Travailleurs</a></li>
            <li><a
                href="${administration}${editTravailleur}/sous_traitant/${ collaborateur.id }/null" onclick="clearCookies()">Édition
                du sous-traitant</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="${ administration }${ travailleurs }?type=idTableContainer">Travailleurs</a></li>
            <li><a
                href="${administration}${editTravailleur}/collaborateur/${ collaborateur.id }/null" onclick="clearCookies()">Édition
                du collaborateur</a></li>
        </c:otherwise>
    </c:choose>

    <li><a
        href="${administration}${editCollaborateur}/${ collaborateur.id }${ editMission }/${ mission.id }">Édition
        de la mission</a></li>
    <li class="active">Édition du forfait</li>
</ol>


<div id="idProfilContainer">
    <form:form class="form-horizontal" method="POST"
               action="${administration}/${ collaborateur.id }/${saveForfait}"
               commandName="linkMissionForfait">
        <fieldset>
            <!-- Form Name -->
            <legend class="text-center"> Création d'un forfait</legend>


            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="listForfaits">
                    Choix du forfait</label>
                <div class="col-md-4">
                    <!--AMNOTE-98 -->
                    <form:select name="listForfaits" class="form-control" id="idListForfaits2" onchange="getSelectedForfait2(this)"
                                 path="forfait.id">

                        <c:forEach items="${listForfaits}" var="tmpForfait">
                            <form:option id="${tmpForfait.code}" name="${tmpForfait.nom}"
                                value="${tmpForfait.id}">${tmpForfait.nom} - ${tmpForfait.frequence.frequence}</form:option>
                        </c:forEach>

                    </form:select>

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group" id="montantForfait2" style="display: block">
                <label class="col-md-4 control-label" for="montant"> Montant (€)
                </label>
                <div class="col-md-4">
                    <form:input id="idMontant" name="montant" path="montant"
                                class="form-control input-md" data-validation="number"
                                data-validation-allowing="float"
                                data-validation-decimal-separator=","
                                onkeyup="remplaceVirgule(this)"/>
                </div>
            </div>

            <div class="form-group" id="libelleForfaitLibre2" style="display: none">
                <label class="col-md-4 control-label" for="libelle"> Libellé
                </label>
                <div class="col-md-4">

                        <%-- <input id="libelle" name="libelle" type="text" class="form-control input-md"/>--%>

                    <form:input id="libelle" path="libelle" type="text"
                                placeholder="libellé du forfait" class="form-control input-md valid"/>
                </div>
            </div>

            <!-- Button (Double) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <a class="btn btn-danger"
                       href="${administration}${editCollaborateur}/${ collaborateur.id }${ editMission }/${ mission.id }"
                       role="button">Retour</a> <input type="submit" id="button1id"
                                                       value="Valider" class="btn btn-success"/>
                </div>
            </div>

        </fieldset>
    </form:form>
</div>
<script>
    $("#idMontant").attr('required', '');
</script>
