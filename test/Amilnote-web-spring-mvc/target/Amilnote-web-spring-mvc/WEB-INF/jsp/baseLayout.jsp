<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  ~ ©Amiltone 2017
  --%>

<c:set var="url">${contextPath}</c:set>
<c:set var="uri" value="${contextPath}"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title><tiles:insertAttribute name="title" ignore="true"/></title>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${contextPath}/">
    <link rel="icon" type="image/x-icon" href="resources/images/favicon.ico"/>

    <!-- JQuery imports -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <!-- <script type="text/javascript" 	src='resources/iframeTracker-jquery-master/jquery.iframetracker.js'></script> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.structure.min.css">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
    <!-- <link rel="stylesheet" 	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css"> -->


    <!-- MomentJs imports -->
    <script type="text/javascript"
            src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data.js"></script>

    <!-- FullCalendar imports -->
    <script type="text/javascript"
            src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.1/fullcalendar.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.1/lang/fr.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.3.1/fullcalendar.min.css">


    <!-- Bootstrap imports -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script> -->
    <script type="text/javascript" src="resources/js/Bootstrap/bootstrap-table.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/locale/bootstrap-table-fr-FR.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script type="text/javascript"
            src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js'></script>
    <script type="text/javascript" src='resources/js/bootstrap-table-editable.js'></script>
    <script type="text/javascript" src='resources/js/Bootstrap/bootstrap-table-cookie.js'></script>


    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" 	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css"> -->
    <link rel="stylesheet" type="text/css" href="resources/css/Bootstrap/bootstrap-table.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <script type="text/javascript" src='resources/js/librairie.js'></script>

    <!-- Local imports -->
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script type="text/javascript" src='resources/js/Administration/administration.js'></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <link rel='stylesheet' type="text/css" href='resources/css/fullCalendar-custom.css'/>
    <link rel="stylesheet" type="text/CSS" href="resources/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"/>

    <!-- Pour faire des gif animés de division sur la page
     <script>
        var anigif_base_url = "https://s3-us-west-2.amazonaws.com/anigif100/anigif/"
    </script>
    <script src="https://s3-us-west-2.amazonaws.com/anigif100/anigif/anigif.min.js"></script>
    -->
</head>


<body>

<div id="testHeader">
    <jsp:include page="template/header.jsp"/>
</div>

<!--Container du HEADER-->
<div id="container">


    <c:if test="${not empty message}">
        <c:choose>
            <c:when test="${fn:containsIgnoreCase(message, 'traitement') || fn:containsIgnoreCase(message, 'succès')}">
                <section class="alert alert-success fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                    </button>
                        ${message}
                </section>
            </c:when>
            <c:otherwise>
                <section class="alert alert-warning fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                    </button>
                        ${message}
                </section>
            </c:otherwise>
        </c:choose>
    </c:if>

    <%-- <c:if test="${disabled == true}">
        <section class="alert alert-warning fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span>
            </button>
            <h4>Vous avez soumis récemment votre rapport d'activités.</h4>
            <p>
                Celui-ci est en cours de traitement. <br> Les
                modifications/ajouts que vous pourriez faire à présent ne seront
                pas pris en compte dans votre rapport d'activités...<br> Pour
                toute réclamation, merci de contacter votre manager.
            </p>
        </section>
    </c:if> --%>
    <tiles:insertAttribute name="body"/>
</div>
<%-- fin div container --%>

<!-- Fenetre modal utilisée pour ajouter un commentaire avant d'effectuer une action
    ex : refus/validation des RA
-->
<div
    id="modalCommentaire"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <form action="">
                <div class="modal-header">
                    <h3></h3>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="comment">Commentaire :</label>
                        <textarea name="commentaire" data-validation="length" data-validation-length="max255"
                                  class="form-control" rows="5" id="comment"></textarea>
                    </div>
                    <input type="hidden" name="monthYearExtract" id="monthYearExtract">

                </div>

                <div class="modal-footer">

                    <button type="submit" value="Submit" class="btn btn-success dismiss">
                        Confirmer
                    </button>

                    <button type="button" class="btn btn-danger dismiss" data-dismiss="modal">Annuler</button>

                </div>
            </form>
        </div>
    </div>

</div>
<div
    id="idModalLoading"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="gif-loader"><img id="idGifLoading" src="resources/gif/load.gif" alt="Chargement"/> Traitement en cours</h3>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>

</div>

<div
    id="modalWarning"
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <form action="">
                <div class="modal-header">
                    <h4>Attention</h4>
                </div>

                <div class="modal-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </form>
        </div>
    </div>

</div>

<div id="testFooter">
    <jsp:include page="template/footer.jsp"/>
</div>

</body>
</html>
