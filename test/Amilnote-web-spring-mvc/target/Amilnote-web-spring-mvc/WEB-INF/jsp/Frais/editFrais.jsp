<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%--
  ~ ©Amiltone 2017
  --%>
<link rel="stylesheet" href="resources/css/editFrais.css">

<script type="text/javascript"
        src='resources/js/NotesDeFrais/modalValidFrais.js'></script>
<script type="text/javascript"
        src='resources/js/NotesDeFrais/editFrais.js'></script>
<script>
    var urlDelate = "mesNotesDeFrais/deleteJustif/";
</script>
<script>
    function remplaceVirgule(input) {
        input.value = input.value.replace(',', '.');
    }
    function valider(form) {
        var date = document.getElementById("dateEvenement").value;
        var idMission = document.getElementById("idMission").value;
        var res = verifPresenceMission(date, idMission);
        var $modalWarning = $("#modalWarning");
        var pattern = /\.(xls|xlsx|pdf|doc|docx|bmp|png|jpe?g)$/i;

        //[JNA][AMNOTE 139] S'il n'y a pas de justificatif et qu'on est dans le cas d'une demande permanente
        if (!pattern.test(document.getElementById("exampleInputFile").value) && document.getElementById("typeAvance").checked == true) {
            setMessageUtilisateur($(document.getElementById("messageUtilisateurWarning")), "Vous devez fournir un justificatif utilisant le format \"pdf, doc, docx, png, jpeg, bmp, xls ou xlsx\"");
            return false;
        }

        if (res == "-1") {
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Erreur dans la sélection de la date. Celle-ci ne correspond à aucun évènement enregistré.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            //alert("Erreur dans la sélection de la date. Celle-ci ne correspond à aucun évènement enregistré.");
            return false;
        }
        else if (res == "0") {
            $modalWarning.find(".modal-body").empty();
            $modalWarning.find(".modal-body").append("Erreur dans la sélection de la date. Celle-ci correspond à une absence.");
            $modalWarning.modal({
                backdrop: 'static',
                keyboard: false
            });
            //alert("Erreur dans la sélection de la date. Celle-ci correspond à une absence.");
            return false;
        }
        else {
            return true;
        }
    }
    function verifJustif() {
        var justif = document.getElementById("exampleInputFile");

        if (justif.value != "" || $(modeCreation)) {
            $(location).attr('href', "${ mesNotesDeFrais }${ frais }/null");
        } else {
            if (!('${typeDeFrais}' == 'VO') || ('${typeDeFrais}' == 'ASF')) {
                setMessageUtilisateur($(document.getElementById("messageUtilisateurWarning")), "Vous devez ajouter un justificatif.");
            } else {
                $(location).attr('href', "${ mesNotesDeFrais }${ frais }/null");
            }
        }
    }

    function checkJustif() {
        var justif = $("#exampleInputFile")[0];
        var messageErrorJustif = $("#errorJustif");

        if (justif.files.length > 0) {
            var justifSize = justif.files[0].size;
            if (justifSize > 20971520) {
                justif.value = "";
                messageErrorJustif.css('display', 'block');
            }else {
                messageErrorJustif.css('display', 'none');
            }
        }
    }

    $('document').ready(function () {
        console.log($('#exampleInputFile'));
        $('#exampleInputFile').change(function () {
            checkJustif();
        })
    });

</script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<section id="messageUtilisateurWarning" class="alert alert-warning" role="alert" hidden=true>
    <button type="button" class="close"><span>×</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>

<ol class="breadcrumb">
    <li><a href="${ welcome }">AmilNote</a></li>
    <li><a href="${ mesNotesDeFrais }${ frais }/null">Mes notes de
        frais</a></li>
    <li class="active">Edition d'un frais</li>
</ol>

<div id="idProfilContainer" class="form-horizontal">
    <fieldset>
        <legend class="text-center">
            <c:choose>
                <c:when test="${!modeCreation }">
                    Edition d'un frais :
                </c:when>
                <c:otherwise>
                    Création d'un frais :
                </c:otherwise>
            </c:choose>
            <c:out value="${typeFrais }"/><br/> Pour la mission : <c:out value="${mission}"/> <c:if
            test="${fraisForfait != ''}"><c:out value="(${fraisForfait})"/></c:if>
        </legend>
        <c:if test="${typeFrais == 'ABT'}">
            Entrez le prix total, Amilnote se charge de faire la division
        </c:if>
        <c:if test="${not empty error}">
            <div class="errorblock" id="errorID">
                    ${error}
            </div>
        </c:if>
        <form:form id="formId" method="post" action="${mesNotesDeFrais}${saveFrais}" enctype="multipart/form-data"
                   class="form-horizontal"
                   commandName="fraisToEdit" name="formulaire" onsubmit="return valider(this)">
            <form:input type="hidden" path="id"/>
            <form:input type="hidden" path="typeFrais"/>
            <form:input type="hidden" path="fraisForfait" name="fraisForfait" value=""/>
            <form:input type="hidden" path="mission" id="mission" name="mission" value="${mission}"/>
            <form:input type="hidden" path="idMission" id="idMission" name="idMission" value="${idMission}"/>
            <div class="form-group">
                <label class="col-md-4 control-label" for="dateEvenement">Date<span style="color:red">*</span></label>
                <div class="col-md-4">
                    <form:input id="dateEvenement" type="text" path="dateEvenement"
                                class="form-control input-md datepicker" data-validation="date"
                                data-validation-format="yyyy-mm-dd" autocomplete="off"/>


                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="enFrance">En
                    France</label>
                <div class="col-md-4">
                    <form:select id="" class="form-control" path="national">
                        <form:option value="true">Oui</form:option>
                        <form:option value="false">Non</form:option>
                    </form:select>
                </div>
            </div>
            <c:choose>
                <c:when test="${typeDeFrais == 'VO'}">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idKm">Nombre entier de km<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idKm" path="nombreKm" type="text"
                                        placeholder="Distance en km" class="form-control input-md"
                                        data-validation="number" data-validation-allowing="int"
                                        data-validation-decimal-separator=","
                                        onkeyup="remplaceVirgule(this)"/>
                            <!--  -->
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="idKm">Montant (€)<span
                            style="color:red">*</span></label>
                        <div class="col-md-4">
                            <form:input id="idKm" path="montant" type="text"
                                        placeholder="Montant total du frais à rembourser" class="form-control input-md"
                                        data-validation="number" data-validation-allowing="float"
                                        data-validation-decimal-separator=","
                                        onkeyup="remplaceVirgule(this)"/>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <c:if test="${typeDeFrais == 'HO'}">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nbNuit">Nombre de nuit<span
                        style="color:red">*</span></label>
                    <div class="col-md-4">
                        <form:input id="idNbNuit" path="nbNuit" type="text" value="${nbNuit}"
                                    placeholder="Nombre de nuit" class="form-control input-md"
                                    data-validation="number" data-validation-allowing="integer"/>
                    </div>
                </div>
            </c:if>
            <div class="form-group">
                <label class="col-md-4 control-label" for="idMotif">Motif</label>
                <div class="col-md-4">
                    <form:textarea id="idMotif" path="commentaire"
                                   placeholder="Commentaire" class="form-control input-md"/>
                </div>
            </div>
            <c:if test="${typeDeFrais == 'ASF'}">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="typeAvance">Avance Permanente ?</label>
                    <div class="col-md-4">
                        <c:if test="${typeAvance == 'PE'}"><input type="checkbox" id="typeAvance" name="typeAvance"
                                                                  checked/></c:if>
                        <c:if test="${typeAvance != 'PE'}"><input type="checkbox" id="typeAvance"
                                                                  name="typeAvance"/></c:if>
                    </div>
                </div>
            </c:if>
            <c:if test="${!modeCreation }">
                <c:forEach items="${listJustifs}" var="justif">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nom du justificatif</label>
                        <div class="col-md-4">
                            <!-- <a onclick = "eventClikDelate(urlDelate + ${justif.id})">
								<span class="glyphicon glyphicon-remove"></span>
							</a> -->
                            <div class="col-md-10">
                                <a href="${mesNotesDeFrais}${download}/${justif.id}" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open" title="Visualiser"
                                          style="font-size:1.3em">&nbsp;${fn:escapeXml(justif.descriptif)}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>

            <div class="form-group">
                <label class="col-md-4 control-label" for="exampleInputEmail1">Nom descriptif du ou des fichiers</label>
                <div class="col-md-4">
                    <input type="text"
                           name="nomFichier" class="form-control" id="exampleInputEmail1"
                           placeholder="Nom du justificatif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="exampleInputFile">Fichier(s) justificatif(s) (format accepté
                    : pdf/doc/docx/png/bmp/jpeg/xls/xlsx)
                    <c:if test="${modeCreation && typeDeFrais!='VO' && typeDeFrais!='ASF'}"><span
                        style="color:red">*</span></c:if></label>
                <div class="col-md-4">

                    <input type="file" id="exampleInputFile" name="fichier" max-size="20971520"
                           accept="image/jpeg,.jpg,image/png,.png,image/bmp,.bmp,application/pdf,.pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.docx,application/msword,.doc,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.xlsx,application/vnd.ms-excel,.xls"
                           <c:if test="${modeCreation && typeDeFrais!='VO' && typeDeFrais!='ASF'}">required</c:if> >
                    <label id="errorJustif" style="display: none; color: red;" class="col-md-8 control-label has-error">Taille de fichier supérieur à 20Mo</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id"></label>
                <div class="col-md-8">
                    <a class="btn btn-danger" href="${ mesNotesDeFrais }${ frais }/null" role="button">Retour</a>
                    <c:if test="${!testFraisForfait}">
                        <button id="idSubmitCollaborateur" type="submit"
                                name="submitCollaborateur" class="btn btn-success">Créer
                        </button>
                    </c:if>
                    <c:if test="${testFraisForfait}">
                        <a type="button" class="btn btn-success" id="btnModalValidFrais">
                            Valider
                        </a>
                    </c:if>
                </div>
            </div>
        </form:form>
    </fieldset>
</div>
<div class="modal fade" id="modalValidFrais" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <H3>Validation du Frais</H3>
            </div>
            <div class="modal-body" style="font-weight: bold;color: red;"></div>
            <div class="modal-footer">
                <span class="label label-danger" id="idMessageUtilisateur"></span>
                <button type="button" class="btn btn-default" aria-label="Left Align" id="idRemplacerButton">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"> </span>
                    Remplacer
                </button>
                <button type="button" class="btn btn-default" aria-label="Left Align" id="idAdditionnerButton">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"> </span>
                    Additionner
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"> </span>
                    Annuler
                </button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="warning-delete-Action" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Suppression d'un justificatif</h3>
            </div>
            <div class="modal-body" id="warning-delete-Collaborateur-body">
                <h4>Êtes-vous sûr de vouloir supprimer ce justificatif ?</h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete-Action"
                        class="btn btn-success dismiss" data-dismiss="modal">Oui
                </button>
                <button type="button" class="btn btn-danger dismiss"
                        data-dismiss="modal">Non
                </button>
            </div>

        </div>
    </div>
</div>
