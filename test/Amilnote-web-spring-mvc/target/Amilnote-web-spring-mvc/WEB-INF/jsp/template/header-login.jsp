<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ ©Amiltone 2017
  --%>

<article id="header">

    <nav class="navbar navbar-default bluck" role="navigation">
        <section class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="${welcome}"><img
                    src="resources/images/amilnote-logo-header.png"
                    alt="Accueil Amilnote"></a>
            </div>
        </section>
    </nav>
</article>
