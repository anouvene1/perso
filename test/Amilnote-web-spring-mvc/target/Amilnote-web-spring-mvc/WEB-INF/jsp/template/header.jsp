<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<script type="text/javascript">

    li = {
        showAdm: function (elem) {
            document.getElementById(elem).style.visibility = 'visible';
        },
        hideAdm: function (elem) {
            document.getElementById(elem).style.visibility = 'hidden';
        },
        showProfil: function (elem) {
            document.getElementById(elem).style.visibility = 'visible';
        },
        hideProfil: function (elem) {
            document.getElementById(elem).style.visibility = 'hidden';
        }
    }

</script>

<article id="header">

    <nav class="navbar navbar-default bluck" role="navigation">
        <section class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="${welcome}"> <img
                    src="resources/images/amilnote-logo-header.png"
                    alt="Accueil Amilnote">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse"
                 id="bs-example-navbar-collapse-1">
                <sec:authorize access="hasAuthority('STT')">
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="${mesTimeSheets}">Mon
                            rapport d'activités</a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                            notes de frais</a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission</a></li>
                    </ul>

                </sec:authorize>
                <sec:authorize access="hasAnyAuthority('CO','DRH', 'AD', 'DIR', 'CP', 'MA', 'RT')">
                <c:choose>


                    <c:when test="${mesTimeSheets == origRequestURL}">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesTimeSheets}">Mon
                                rapport d'activités</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                                notes de frais</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${editDeplacement}">Demande de déplacement</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission</a></li>
                        </ul>
                    </c:when>


                    <c:when test="${mesNotesDeFrais == origRequestURL}">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesTimeSheets}">Mon
                                rapport d'activités</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                                notes de frais</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${editDeplacement}">Demande de déplacement</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission</a></li>
                        </ul>
                    </c:when>


                    <c:when test="${editDeplacement == origRequestURL}">

                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesTimeSheets}">Mon
                                rapport d'activités</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                                notes de frais</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${editDeplacement}">Demande de déplacement</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission </a></li>
                        </ul>

                    </c:when>


                    <c:when test="${ordresDeMission == origRequestURL}">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesTimeSheets}">Mon
                                rapport d'activités</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                                notes de frais</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${editDeplacement}">Demande de déplacement</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission
                                <c:if test="true"><span class="glyphicon glyphicon-alert"></span></c:if></a></li>
                        </ul>
                    </c:when>

                    <c:otherwise>
                        <ul class="nav navbar-nav" id="idUlMesTimesheets">
                            <li class="dropdown"><a href="${mesTimeSheets}">Mon
                                rapport d'activités</a></li>
                        </ul>
                        <ul class="nav navbar-nav" id="idUlMesNotesDeFrais">
                            <li class="dropdown"><a href="${mesNotesDeFrais}${frais}/null">Mes
                                notes de frais</a></li>
                        </ul>
                        <ul class="nav navbar-nav" id="idUlMesDeplacements">
                            <li class="dropdown"><a href="${editDeplacement}">Demande de déplacement</a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="${ordresDeMission}">Mes ordres de mission
                            </a></li>
                        </ul>
                    </c:otherwise>




                </c:choose>

</sec:authorize>

                <ul class="nav navbar-nav navbar-right bluck">
                    <sec:authorize access="hasAnyAuthority('DRH', 'AD', 'DIR', 'CP', 'MA', 'RT')">
                        <li class="dropdown custom open bluck"
                            onMouseOver="li.showAdm('ul-adm')"
                            onMouseOut="li.hideAdm('ul-adm')">

                            <a href="${welcome}" class="dropdown-toggle bluck" id="a-adm">Administration
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right bluck" role="menu" id="ul-adm">
                                <sec:authorize access="hasAnyAuthority('DRH','DIR')">

                                    <li role="presentation" class="dropdown-header">Gestion des consultants</li>
                                    <li><a href="${administration}${travailleurs}">Gestion des consultants</a></li>
                                    <li><a href="${administration}${validationODM}">Validation des ordres de mission</a>
                                    </li>
                                    <li><a href="${administration}${moduleAddvise}">Gestion des modules Addvise</a></li>
                                    <li class="divider"></li>

                                        <%--<li role="presentation" class="dropdown-header">Gestion des clients</li>--%>
                                    <li><a href="${administration}${clients}">Gestion des clients</a></li>
                                    <li><a href="${administration}${gestionCommandes}/CO">Gestion des commandes</a></li>
                                        <%-- AMNOTE-53 Gestion des factures pour les DRH --%>
                                    <!-- [OZE] 24-11-2016 Ajout de la direction 'DIR' pour acceder aux pages factures et commandes -->
                                    <sec:authorize access="hasAnyAuthority('DRH','DIR')">

                                        <li><a href="${administration}${factures}/null">Gestion des factures du mois</a>
                                        </li>
                                    </sec:authorize>
                                    <li class="divider"></li>


                                    <li><a href="${administration}${noteDeFrais}/null/null">Gestion des avances de frais</a></li>
                                    <li class="divider"></li>


                                    <li><a href="${administration}${consultationAbsences}/SO">Gestion des absences</a></li>


                                    <li role="presentation" class="dropdown-header">Gestion des RA</li>
                                    <li><a href="${administration}${consultationRA}/null/null">Consultation des RA</a></li>
                                    <!-- <li><a href="${administration}${validationsRA}">Validation des RA</a></li>  -->
                                    <li><a href="${administration}${listeSoumissionRA}/null">Liste des soumissions</a></li>
                                    <li class="divider"></li>


                                    <li><a href="${administration}${consultationDeplacements}/null">Gestion des
                                        déplacements</a></li>
                                    <li class="divider"></li>
                                </sec:authorize>

                                <sec:authorize access="hasAnyAuthority('MA', 'RT', 'CP', 'DIR', 'AD')">
                                    <li><a href="${administration}${consultationDisponibilites}">Gestion des
                                        disponibilités</a></li>
                                </sec:authorize>

                                <sec:authorize access="hasAnyAuthority('DRH','DIR')">
                                    <li role="presentation" class="dropdown-header">Gestion Application</li>
                                    <li><a href="${administration}${gestionApplication}">Gestion Application</a></li>
                                    <li><a href="${administration}${gestionTableurs}">Gestion Tableurs</a></li>
                                </sec:authorize>
                            </ul>
                        </li>
                    </sec:authorize>

                    <li class="dropdown custom open"
                        onMouseOver="li.showProfil('ul-profil')"
                        onMouseOut="li.hideProfil('ul-profil')"><a href="${welcome}"
                                                                   class="dropdown-toggle bluck" id="a-profil">
                        <sec:authentication
                            property="principal.username"/> <span class="caret"></span>
                    </a>
                        <ul class="dropdown-menu" role="menu" id="ul-profil"
                            style="visibility: hidden; min-width: 160px;">
                            <li><a href="${gestionProfil}">Profil</a></li>
                        </ul>
                    </li>
                    <li><a class="myTooltip" role="menu" data-toggle="tooltip"
                           data-placement="bottom" title="Besoin d'aide ?"
                           href='<c:url value="https://www.youtube.com/watch?v=pDS3dkWfWl0" />' target=_blank> <span
                        class="glyphicon glyphicon-question-sign"></span>
                    </a></li>
                    <li><a class="logout"
                           href='<c:url value="/j_spring_security_logout" />'> <span
                        class="glyphicon glyphicon-off"></span>
                    </a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </section>
        <!-- /.container-fluid -->
    </nav>

    <c:if test="${not empty actionSuccess }">
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <c:choose>
                <c:when test="${actionSuccess == '1'}">
                    Modification effectuée avec succès.
                </c:when>
                <c:when test="${actionSuccess == '2'}">
                    Création effectuée avec succès.
                </c:when>
                <c:otherwise>
                    ${actionSuccess}
                </c:otherwise>
            </c:choose>
        </div>
    </c:if>
    <c:if test="${not empty actionError }">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"
                    aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
                ${actionError}
        </div>
    </c:if>
</article>
