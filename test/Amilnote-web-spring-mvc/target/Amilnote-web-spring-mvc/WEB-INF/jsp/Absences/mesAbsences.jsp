<%--
  ~ ©Amiltone 2017
  --%>

<!-- <script type="text/javascript" src='${contextPath}/resources/js/chargementContenu-Absence.js'></script>-->
<script type="text/javascript" src='${contextPath}/resources/js/Absences/fullCalendar-custom-Absence.js'></script>

<section id="messageUtilisateur" class="alert alert-success" role="alert" hidden=true>
    <button type="button" class="close"><span>�</span><span class="sr-only">Close</span></button>
    <p> -- </p>
</section>
<article class="block-left panel panel-default">

    <div class="panel panel-success" id="listAbsences">

        <div class="panel-heading">
            <h2 class="panelTitle-groupItem">Vos absences :</h2>
        </div>
        <div class="panel-body">
            <ul id="list-group-typeAbsences" class="list-group">
                <li class="list-group-item">
                </li>
            </ul>
        </div>
    </div>
</article>


<article class="block-right panel panel-default">
    <h4 class="fullCalendarTitle">Vous êtes sur la page des absences</h4>
    <section id="calendar">
    </section>
</article>

<!-- Modal -->
<div class="modal fade" id="myModalChoixTypeAbsence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                    class="sr-only">Close</span></button>
                <h3 class="modal-title modalTitleCustom" id="myModalLabelTitle">Enregistrer une absence</h3>
                <h4 class="modal-title modalSubTitleCustom" id="myModalLabelSubTitle">Pour la période :</h4>
            </div>

            <div class="panel panel-default" id="modalPanelContent">
                <div class="input-group input-group-sm gpTypeFrais">
                    <span class="input-group-addon label-new">Choix de l'absence</span>
                    <select name="choixAbsence" class="choixAbsence form-control" id="idlistChoixAbsence">
                    </select>
                </div>
            </div>
            <div class="modal-footer modalFooterCustom">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveModal"> Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Fen�tre modale d'�dition de'absence -->
<div class="modal fade" id="modalEditView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title modalTitleCustom" id="myModalLabelTitle">Propriété de l'absence</h3>
                <h4 class="modal-title modalSubTitleCustom" id="modalEditViewSubTitle">Pour la période :</h4>

                <div class="panel panel-default" id="modalEditViewBody">
                    <div id="modalEditViewTableHead"></div>
                    <div class="modalEditViewTableContent"></div>
                </div>

                <div class="panel panel-info">
                    <span id="modalEditViewSpanWarning" class="label label-warning">La création d'une nouvelle absence supprimera les frais.</span>
                    <div class="panel-heading"><h3 class="panel-title"> Action sur l'absence</h3></div>
                    <!-- Default panel contents -->
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div id="modalEditAbsenceLabelSelect">Modification ou absence :</div>
                                <select id="modalEditAbsenceSelect" name="nameNewTypeAbsence"></select>
                                <button disabled type="button" class="btn btn-default" aria-label="Left Align"
                                        id="sendTypeAbsenceChange">
                                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"> Envoyer</span>
                                </button>
                            </li>
                            <li class="list-group-item">
                                Suppression :
                                <button type="button" class="btn btn-default" id="sendTypeAbsenceDelete"
                                        aria-label="Left Align">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"> Faire une demande de suppression </span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer modalFooterCustom">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>






	
