<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%--
  ~ ©Amiltone 2017
  --%>

<jsp:useBean id="constants" class="com.amilnote.project.metier.domain.utils.Constantes" scope="session"/>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>

<article>
    <script type="text/javascript">
        mapForfaitsMissions = ${mapForfaitsMissionsJSON};
        mapFraisMissions = ${mapFraisMissionsJSON};
        mapHistoFraisMissions = ${mapHistoFraisMissionsJSON};
        mapTypeFrais = ${mapTypeFraisJSON};
        constantAccordion1 = "${constants.ssTitreAccordion1}";
        constantAccordion2 = "${constants.ssTitreAccordion2}";
        listDatesMissions = ${listDatesMissions};
        //mapTotaux = ${mapTotaux};
        dureePeriodeHistoFrais = ${dureePeriodeHistoFrais};
        listEtatFrais = ${listEtatFraisJSON};
        JsonListMissionsEventsFrais = ${JsonListMissionsEventsFrais};
    </script>
    <script type="text/javascript" src='resources/js/NotesDeFrais/notesFrais-custom.js'></script>
    <script type="text/javascript" src='resources/js/NotesDeFrais/ModalEditNotesFrais.js'></script>

    <article class="block-left panel panel-default">

        <div class="panel panel-success" id="list-missionsAndAbsences">
            <!-- Default panel contents -->
            <div class="panel-heading bluck">
                <h2 class="panelTitle-groupItem">Filtre par mission</h2>
            </div>
            <div class="panel-body">
                <table id="idTabFiltreMission"></table>
            </div>
            <div class="panel-heading">
                <h2 class="panelTitle-groupItem"> Paramétrage :<span class="glyphicon glyphicon-wrench"
                                                                     aria-hidden="true"></span></h2>
            </div>
            <div class="panel-body parametrage">

            </div>

        </div>
        <!--  block d'aide et bouton pour masquer la fenêtre de gauche -->
        <aside class="aide-arrow">
            <button type="button" id="aide">
                <img src="resources/images/logo-aide.png"
                     alt="bouton d'acces a l'aide"/>
            </button>
            <div class="block-aide">
                <div class="media">
                    <div class="media-left">


                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Media heading</h4>
                        ...
                    </div>
                </div>
            </div>
        </aside>
    </article>

    <!--  block droit pour les frais et l'historique -->
    <section class="block-right panel panel-default view">
        <!--  block sur les forfaits et la navigation -->
        <div class="block-navig-forfait">
            <div class="navbar-inner">
                <button type="button" id="arrowRight" style="display: none"
                        class="glyphicon glyphicon-large glyphicon-chevron-right"
                        data-toggle="collapse" data-target="#menu"
                        onClick="arrowAction(this);"></button>
            </div>
            <!--  block sur les forfaits -->
            <form class="forfait-mission">
                <fieldset>
                    <legend>Forfaits liés à la mission</legend>
                    <ul></ul>
                    <a class="mailError" href="#" data-toggle="modal" data-target="#error-forfait">Signaler une erreur
                        dans les forfaits</a>
                </fieldset>
            </form>
        </div>
        <div class="block-total-new">
            <p class="total Montant">
                Total dépensé (hors forfaits repas) : <span></span> €
            </p>
            <!-- <p class="total ARembourser">Total à rembourser (hors forfaits repas): <span></span> €</p> -->
            <!-- bouton pr une nouvelle dépense -->
            <a href="" type="button" class="btn btn-default btn-action purpleHaze"
               id="newFrais" data-title="Insérer une nouvelle dépense"
               data-toggle="modal" data-backdrop="static"
               data-target="#form-depense"
            >
                <span class="glyphicon glyphicon-large glyphicon-plus"> </span>
                <span class="glyphicon glyphicon-large glyphicon-euro"> </span>
                Nouvelle dépense
            </a>
        </div>

        <!-- affichage frais -->
        <div id="listeFrais" style="width:60%">
            <table id="tableauFrais">

            </table>
        </div>
    </section>

    <!-- pop-in pour signaler une erreur dans les forfaits -->
    <section class="modal fade" id="error-forfait">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Signaler une erreur dans les forfaits</h4>
                </div>
                <div class="modal-body">
                    <form action="${mailAdministrateur}" method="post">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon label-new">Objet</span> <select
                            name="subject" class="form-control" required>
                            <option value="Erreur montant forfait repas">Erreur
                                montant forfait repas
                            </option>
                            <option value="Erreur total forfait repas">Erreur total
                                forfait repas
                            </option>
                            <option value="Absence forfait repas">Absence forfait
                                repas
                            </option>
                            <option value="Erreur pourcentage forfait transport">Erreur
                                pourcentage forfait transport
                            </option>
                            <option value="Absence forfait transport">Absence
                                forfait transport
                            </option>
                            <option value="Autre">Autre</option>
                        </select>
                        </div>
                        <div class="input-group input-group-sm">
							<span class="input-group-addon label-new ">Message (250
								caractères max)</span>
                            <!--  pattern="[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._\s--]+" -->
							<textArea name="message" class="form-control" required
                                      onkeyup="validateTextArea(this);" maxlength="250"
                                      placeholder="votre message ici" rows="10"></textArea>
                        </div>
                        <div class="input-group input-group-sm">
                            <input type="submit" value="envoyer">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </section>
    <!-- /.modal -->

    <!-- pop-in pour signaler une erreur dans les frais -->
    <section class="modal fade" id="error-frais">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Signaler une erreur dans les frais</h4>
                </div>
                <div class="modal-body">
                    <form action="${mailAdministrateur}" method="post">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon label-new">Objet</span>
                            <select name="subject" class="form-control" required>
                                <option value="Erreur montant frais Restaurant/Hôtel">Erreur montant frais
                                    restaurant/hôtel
                                </option>
                                <option value="Erreur montant frais Voiture">Erreur montant frais voiture</option>
                                <option value="Erreur montant frais Transport/Autres">Erreur montant frais
                                    transport/autres
                                </option>
                                <option value="Erreur total frais (hors forfaits)">Erreur total frais dépensé (hors
                                    forfaits)
                                </option>
                                <option value="Absence frais ajouté récemment">Absence frais ajouté récemment</option>
                                <option value="Erreur à l'insertion d'un frais">Erreur à l'insertion d'un frais</option>
                                <option value="Erreur à la modification d'un frais">Erreur à la modification d'un
                                    frais
                                </option>
                                <option value="Autre">Autre</option>
                            </select>
                        </div>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon label-new ">Message (250 caractères max)</span>
                            <!-- pattern="[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._\s--]+" -->
							<textArea name="message" class="form-control" required
                                      onkeyup="validateTextArea(this);" maxlength="250"

                                      placeholder="votre message ici" rows="10"></textArea>
                        </div>
                        <div class="input-group input-group-sm">
                            <input type="submit" value="envoyer">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </section>
    <!-- /.modal -->


    <!-- pop in pour l'ajout/modification d'une dépense -->
    <section class="modal fade" id="form-depense">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Insérer une nouvelle dépense</h4>
                </div>
                <div class="modal-body">
                    <form id="insertFrais" method="post" enctype="multipart/form-data">
                        <div class="input-group input-group-sm gpMission">
                            <span class="input-group-addon label-new">Référence de la mission</span>
                            <select name="mission" class="form-control" id="idSelectMission" required></select>
                        </div>
                        <div class="input-group input-group-sm gpDate">
                            <span class="input-group-addon label-new">Date</span>
                            <input type="text" class="form-control-date datepicker validate[required]" name="date"
                                   id="idDatePicker" required="required" readonly>
                            <!-- <select name="demiJournee" class="form-control" style="width:20%;" required>
                                <option value="AM">AM</option>
                                <option value="AM">PM</option>
                            </select>-->
                            <span class="obligated">*</span>
                        </div>
                        <div class="input-group input-group-sm gpTypeFrais">
                            <span class="input-group-addon label-new">Type de frais</span>
                            <select name="typeFrais" required="required"
                                    class="typeFrais form-control">

                                <c:forEach var="pair" items="${mapListeTypeFrais}">
                                    <option value="${pair.key}">${pair.value.first}-${pair.value.second}</option>
                                </c:forEach>
                            </select>
                            <p class="obligated">*</p>
                        </div>
                        <div class='input-group input-group-sm gpKm' style="display: none">
                            <span class='input-group-addon label-new'>Nombre de kms</span>
                            <input pattern="\d+(\.\d{2})?" name="km" type="text"
                                   class="form-control" placeholder="ex : 12 ou 12.54"
                            >
                            <p class="obligated">*</p>
                        </div>

                        <div class="input-group input-group-sm gpMontant">
                            <span class="input-group-addon label-new ">Montant (€)</span> <input
                            pattern="\d+(\.\d{2})?" name="montant" type="text"
                            class="form-control" placeholder="ex : 10 ou 10.00"
                            required="required">
                            <p class="obligated">*</p>
                        </div>

                        <div class="input-group input-group-sm gpJustif">
                            <p class="input-group-addon label-new ">
                                Justificatif (pdf/jpeg max 300Ko)<br>
                                <span class="legendText">cliquez sur l'aperçu pour voir	en plus grand</span>
                            </p>
                            <div class="input-group">
                                <input class="justif" type="text" class="form-control" readonly>
								<span class="btn btn-primary btn-file" id="browse">
									Browse <input name="justif" type="file" id="fileLoad"
                                                  accept="image/jpg, image/jpeg, application/pdf"
                                                  onchange="loadfile(this);checkSizeAndFormat(this);">
								</span> <span class="btn btn-default btn-file" id="vider"
                                              onclick="emptyFileInput($('#fileLoad'));"> Vider</span>
                                <img class="preview"
                                     style="width: 15%; margin-top: 10px; display: none" src=""
                                     alt="aperçu justificatif" data-toggle="modal"
                                     data-backdrop="static" data-target="#img-full"/>
                                <!-- frameborder="0" scrolling="no" -->
                                <iframe id="justifAdd" class="viewer" style="display: none" width="100" height="200"
                                        data-toggle="modal" data-backdrop="static"
                                        data-target="#img-full"></iframe>
                            </div>
                        </div>

                        <div class="input-group input-group-sm gpCommentaire">
                            <span class="input-group-addon label-new ">Commentaire</span>
                            <!-- pattern="[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._\s--]+" -->
							<textArea name="commentaire" class="form-control"
                                      onkeyup="validateTextArea(this);" maxlength="250"
                                      placeholder="votre commentaire ici" rows="10"></textArea>
                        </div>
                        <button type="submit" id="validateInsertFrais" class="btn btn-success">Valider</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default dismiss">Annuler</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </section>
    <!-- /.modal -->

    <!-- pop in pour la suppression de dépenses -->
    <section class="modal fade" id="delete-depense">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Supprimer les dépenses suivantes ?</h4>
                    <p>Les dépenses ayant le statut "Validé" ne pourront pas être supprimées.</p>
                </div>
                <div class="modal-body">
                    <div class="tab-content tab-delete" style="display: none">
                        <div class="tab-pane active">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Référence mission</th>
                                    <th>Type de frais</th>
                                    <th>Dépensé</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="div-table-content">
                                <table class="table table-responsive table-striped table-hover">
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="sendRowsToDelete();" type="button"
                            id="validateDeleteFrais" class="btn btn-success">Valider
                    </button>
                    <button type="button" class="btn btn-default dismiss"
                            data-dismiss="modal">Annuler
                    </button>
                </div>
            </div>
        </div>
    </section>


    <!--  pop in pour la vue en grand des justificatifs -->
    <section class="modal fade" id="img-full">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Votre pièce jointe</h4>
                    <p>Pour firefox, merci de cocher "aperçu dans firefox" dans
                        Options>Applications.</p>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </section>

    <!--  pop in pour avertissement avant annulation -->
    <section class="modal fade warning-before" id="warning-before-frais">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Êtes-vous sûr de vouloir annuler cette
                        opération ?</h4>
                    <p>Toutes les données entrées dans le formulaire seront
                        perdues.</p>
                </div>
                <div class="modal-body">
                    <button type="button" class="btn btn-success">Valider</button>
                    <button type="button" class="btn btn-default dismiss"
                            data-dismiss="modal">Annuler
                    </button>
                </div>
            </div>
        </div>
    </section>
</article>
