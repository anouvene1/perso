package com.amilnote.project.metier.domain.services.impl;


import com.amilnote.project.metier.domain.dao.CalculFraisDAO;
import com.amilnote.project.metier.domain.services.CalculFraisService;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by clome on 26/09/2017.
 */
@Transactional
@RunWith(MockitoJUnitRunner.class)

public class CalculFraisServiceImplTest {


    @Autowired
    @Mock
    private CalculFraisService calculFraisService;

    @Autowired
    @Mock
    private CalculFraisDAO calculFraisDAO;


    private List<Object[]> expectedListFrais;
    private List<Object[]> expectedListForfaits;
    private List<Object[]> expectedListAvances;


    private Map<Long,Map<Long,Object[]>> expectedMapMontantFrais;
    private Map<Long,Map<Long,Float>> expectedMapMontantForfaits;
    private Map<Long, Object[]> expectedMapMontantAvances;


    private List<Object[]> expectedCalculedList;

    private List<Object[]> obtainedListFrais;
    private List<Object[]> obtainedListForfaits;
    private List<Object[]> obtainedListAvances;
    private List<Object[]> obtainedListAbsence;

    private Map<Long, Object[]> expectedMapFraisForCollaborator;
    private Map<Long, Object[]> expectedMapFraisForCollaborator_225;
    private Map<Long, Float> expectedMapForfaitsForCollaborator;
    private Map<Long, Float> expectedMapForfaitsForCollaborator_27;
    private Map<Long, Object[]> expectedMapAvancesForCollaborator;
    private Map<Long, Float> expectedMapForfaitsForCollaborator_37;


    private Map<Long,Map<Long,Object[]>> obtainedMapMontantFrais;
    private Map<Long,Map<Long,Float>> obtainedMapMontantForfaits;
    private Map<Long, Object[]> obtainedMapMontantAvances;
    private Map<Long, Integer> mapAbsence;

    private List<Object[]> obtainedCalculedList;

    @Before
    public void setUp() throws Exception {

        calculFraisService = new CalculFraisServiceImpl();

        obtainedMapMontantFrais =new HashMap<>();
        obtainedMapMontantForfaits =new HashMap<>();
        obtainedMapMontantAvances =new HashMap<>();
        obtainedCalculedList=new ArrayList<>();
        obtainedListFrais=new ArrayList<>();
        obtainedListForfaits=new ArrayList<>();
        obtainedListAvances=new ArrayList<>();
        obtainedListAbsence=new ArrayList<>();

        // map/liste de frais/forfait après calcul
        expectedMapFraisForCollaborator =new HashMap<>();
        expectedMapForfaitsForCollaborator =new HashMap<>();
        expectedMapAvancesForCollaborator =new HashMap<>();
        expectedMapForfaitsForCollaborator_27 =new HashMap<>();
        expectedMapFraisForCollaborator_225 =new HashMap<>();
        expectedMapForfaitsForCollaborator_37 =new HashMap<>();

        // ---------------------------------- Valeurs attendues -----------------------------------------
        expectedListFrais = new ArrayList<>();
        expectedListForfaits = new ArrayList<>();
        expectedListAvances = new ArrayList<>();

        expectedMapMontantFrais = new HashMap<>();
        expectedMapMontantForfaits = new HashMap<>();
        expectedMapMontantAvances = new HashMap<>();

        expectedCalculedList = new ArrayList<>();
        mapAbsence=calculAbsence();


    }




    @Test
    public void test_isNull_RequeteCriteria(){
        boolean res;
        DateTime pDate = new DateTime();
        pDate = pDate.withMonthOfYear(pDate.getMonthOfYear());
        List<Object[]>listtemp;
        Date dateDebut = pDate.dayOfMonth().withMinimumValue().toDate();
        Date dateFin = pDate.withMonthOfYear(pDate.getMonthOfYear() + 1)
            .dayOfMonth().withMinimumValue().toDate();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = new Date();
        Date pDateFin = new Date();

        try {
            pDateDebut = simpleDateFormat.parse(dateD);
            pDateFin = simpleDateFormat.parse(dateF);
        } catch (Exception e) {
            //logger.error(e.getMessage());
        }
        listtemp=calculFraisDAO.getDataForfait(pDateDebut,pDateFin,"voiture");
        if (listtemp.size()>0){
            res=false;
        }
        else{
            res=true;
        }

        assertTrue("La requete renvoit une liste vide",res);
    }



    // -------------------------------------- Tests pour getDataForfaits ----------------------------------------------
    @Test
    public void testGetDataFraisNotNull() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        obtainedListFrais.addAll(expectedListFrais);
        assertNotNull("La récupération de données renvoit null", obtainedListFrais);
    }

    @Test
    public void testGetDataFraisGoodFormat() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        obtainedListFrais.addAll(expectedListFrais);
        for (Object[] o : obtainedListFrais) {
            assertEquals("La récupération de données renvoit des objets de mauvaise taille", o.length, 6);
        }
    }

    // -------------------------------------- Tests pour getDataForfaits ----------------------------------------------
    @Test
    public void testGetDataForfaitsNotNull() throws Exception {

        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));

        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));

        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        obtainedListForfaits.addAll(expectedListForfaits);
        assertNotNull("La récupération de données renvoit null", obtainedListForfaits);
    }



    // -------------------------------------- Tests pour getDataAvances ----------------------------------------------
    @Test
    public void testGetDataAvancesNotNull() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        expectedMapFraisForCollaborator.putAll(mapTemp);

        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));

        obtainedListAvances.addAll(expectedListAvances);
        assertNotNull("La récupération de données renvoit null", obtainedListAvances);
    }

    @Test
    public void testGetDataAvancesGoodFormat() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        expectedMapFraisForCollaborator.putAll(mapTemp);

        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));

        obtainedListAvances.addAll(expectedListAvances);
        for (Object[] o : obtainedListAvances) {
            assertEquals("La récupération de données renvoit des objets de mauvaise taille", o.length, 4);
        }
    }

    // -------------------------------------- Tests pour doCalculFrais ----------------------------------------------
    @Test
    public void testDoCalculFraisCorrect() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();

        List<Object[]> listRes;
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        mapTemp=getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        mapTemp= getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        mapTemp=getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        mapTemp=getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        mapTemp=getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator.putAll(mapTemp);
        expectedMapMontantFrais.put(106l, expectedMapFraisForCollaborator);
        //expectedMapFraisForCollaborator=new HashMap<>();


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        mapTemp= getMapFraisDetail(utilFrais);
        expectedMapFraisForCollaborator_225.putAll(mapTemp);
        expectedMapMontantFrais.put(225l, expectedMapFraisForCollaborator_225);
        boolean res;

        Map<Long, Map<Long, Object[]>> result = calculFraisService.doCalculFraisDetail(expectedListFrais);

        assertEquals("Le calcul des frais est incorrect", (expectedMapMontantFrais.get(106L).get(4L))[0], (result.get(106L).get(4L))[0]);

    }


    @Test
    public void testDoCalculFraisNotNull() throws Exception {
        //frais avion

        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        obtainedMapMontantFrais.putAll(calculFraisService.doCalculFraisDetail(expectedListFrais));
        assertNotNull("Le calcul des frais renvoit null", obtainedMapMontantFrais);
    }

    // -------------------------------------- Tests pour doCalculForfaits ---------------------------------------------
    @Test
    public void testDoCalculForfaitsCorrect() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);
        expectedMapMontantFrais.put(106l, expectedMapFraisForCollaborator);
        //expectedMapFraisForCollaborator=new HashMap<>();


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator_225.putAll(mapTemp);
        expectedMapMontantFrais.put(225l, expectedMapFraisForCollaborator_225);


        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator_37.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(37l, expectedMapForfaitsForCollaborator_37);

        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        mapForfaitTemp=getMapForfait(utilForfait);
        expectedMapForfaitsForCollaborator.putAll(mapForfaitTemp);


        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(106l, expectedMapForfaitsForCollaborator);



        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));
        expectedMapForfaitsForCollaborator_27.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(27l, expectedMapForfaitsForCollaborator_27);




        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));
        expectedMapAvancesForCollaborator.putAll(getMapAvance(utilAvance));
        expectedMapMontantAvances.putAll(expectedMapAvancesForCollaborator);



        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        obtainedMapMontantFrais.putAll(calculFraisService.doCalculFraisDetail(expectedListFrais));
        obtainedMapMontantForfaits.putAll(calculFraisService.doCalculForfaitsDetail(expectedListForfaits,expectedMapMontantFrais,mapAbsence));
        boolean res;
        res=IsMapForfaits_Equal(106l,1l);
        assertTrue("Le calcul des forfaits est incorrect",res);
        res=IsMapForfaits_Equal(37l,4l);
        assertTrue("Le calcul des forfaits est incorrect",res);

    }

    private boolean IsMapForfaits_Equal(Long idCollab, Long idForfait){
        boolean res;
        if (expectedMapMontantForfaits.get(idCollab).get(idForfait).equals(obtainedMapMontantForfaits.get(idCollab).get(idForfait))){
            res=true;
        }else{
            res=false;
        }

        return res;
    }

    @Test
    public void testDoCalculForfaitsNotNull() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();

        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);
        expectedMapMontantFrais.put(106l, expectedMapFraisForCollaborator);
        //expectedMapFraisForCollaborator=new HashMap<>();


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator_225.putAll(mapTemp);
        expectedMapMontantFrais.put(225l, expectedMapFraisForCollaborator_225);


        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator_37.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(37l, expectedMapForfaitsForCollaborator_37);

        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        mapForfaitTemp=getMapForfait(utilForfait);
        expectedMapForfaitsForCollaborator.putAll(mapForfaitTemp);


        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(106l, expectedMapForfaitsForCollaborator);



        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));
        expectedMapForfaitsForCollaborator_27.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(27l, expectedMapForfaitsForCollaborator_27);




        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));
        expectedMapAvancesForCollaborator.putAll(getMapAvance(utilAvance));
        expectedMapMontantAvances.putAll(expectedMapAvancesForCollaborator);



        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        obtainedMapMontantFrais.putAll(calculFraisService.doCalculFraisDetail(expectedListFrais));
        obtainedMapMontantForfaits.putAll(calculFraisService.doCalculForfaitsDetail(expectedListForfaits,expectedMapMontantFrais,mapAbsence));
        assertNotNull("Le calcul des forfaits renvoit null", obtainedMapMontantForfaits);
    }

    // -------------------------------------- Tests pour doCalculAvances ----------------------------------------------
    @Test
    public void testDoCalculAvancesCorrect() throws Exception {


        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));
        expectedMapAvancesForCollaborator.putAll(getMapAvance(utilAvance));
        expectedMapMontantAvances.putAll(expectedMapAvancesForCollaborator);



        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        obtainedMapMontantAvances.putAll(calculFraisService.doCalculAvance(expectedListAvances));
        boolean res=(expectedMapMontantAvances.get(new Long(106)))[0].equals((obtainedMapMontantAvances.get(new Long(106)))[0]);
        assertTrue("Le calcul des avances est incorrect", res);

    }

    @Test
    public void testDoCalculAvancesNotNull() throws Exception {

        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));
        expectedMapAvancesForCollaborator.putAll(getMapAvance(utilAvance));
        expectedMapMontantAvances.putAll(expectedMapAvancesForCollaborator);



        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        obtainedMapMontantAvances.putAll(calculFraisService.doCalculAvance(expectedListAvances));
        assertNotNull("Le calcul des avances renvoit null", obtainedMapMontantAvances);
    }



    @Test
    public void testDoCalculCoherent() throws Exception {
        //frais avion

        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));

        obtainedMapMontantFrais.putAll(calculFraisService.doCalculFraisDetail(expectedListFrais));
        obtainedMapMontantForfaits.putAll(calculFraisService.doCalculForfaitsDetail(expectedListForfaits,expectedMapMontantFrais,mapAbsence));
        obtainedMapMontantAvances.putAll(calculFraisService.doCalculAvance(expectedListAvances));
        obtainedCalculedList.addAll(calculFraisService.doCalculDetail("detail", obtainedMapMontantFrais, obtainedMapMontantForfaits, obtainedMapMontantAvances));
        for (Object[] o : obtainedCalculedList) {
            assertEquals("Le calcul final n'est pas cohérent", (Float) o[25],
                (Float) o[22] + (Float) o[23] -(Float) o[16]-(Float) o[17]);
        }
    }

    @Test
    public void testDoCalculNotNull() throws Exception {
        //frais avion

        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));


        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));

        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));


        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));

        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));

        obtainedMapMontantFrais.putAll(calculFraisService.doCalculFraisDetail(expectedListFrais));
        obtainedMapMontantForfaits.putAll(calculFraisService.doCalculForfaitsDetail(expectedListForfaits,expectedMapMontantFrais,mapAbsence));
        obtainedMapMontantAvances.putAll(calculFraisService.doCalculAvance(expectedListAvances));
        obtainedCalculedList.addAll(calculFraisService.doCalculDetail("detail", obtainedMapMontantFrais, obtainedMapMontantForfaits, obtainedMapMontantAvances));
        assertNotNull("Le calcul final renvoit null", obtainedCalculedList);
    }

    // --------------------------------- Tests pour getDataFraisForCollaborator -------------------------------------
    @Test
    public void testGetDataFraisForCollaboratorNotNull() throws Exception {
        //frais avion
        Map<Long, Object[]> mapTemp=new HashMap<>();
        List<Object[]>listRes=new ArrayList<>();
        UtilFrais utilFrais=new UtilFrais(106L,4L,2562.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));

        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Taxi
        utilFrais=new UtilFrais(106L,13L,97F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto soir
        utilFrais=new UtilFrais(106L,11L,15.14F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais resto midi
        utilFrais=new UtilFrais(106L,10L,8.48F,0F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);


        //frais Hotel
        utilFrais=new UtilFrais(106l,9l,500.1f,0f,"FRF",1025l);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator.putAll(mapTemp);
        expectedMapMontantFrais.put(106l, expectedMapFraisForCollaborator);
        //expectedMapFraisForCollaborator=new HashMap<>();


        //Frais Voiture
        utilFrais=new UtilFrais(225L,1L,null,200F,"FRF",1025L);
        expectedListFrais.addAll(initDonneeFraisDetail(utilFrais));
        expectedMapFraisForCollaborator_225.putAll(mapTemp);
        expectedMapMontantFrais.put(225l, expectedMapFraisForCollaborator_225);

        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        assertNotNull("La récupération de données renvoit null", expectedMapFraisForCollaborator);
    }



    // --------------------------------- Tests pour getDataForfaitsForCollaborator ------------------------------------
    @Test
    public void testGetDataForfaitsForCollaboratorNotNull() throws Exception {

        //forfait logement
        Map<Long,Float>mapForfaitTemp;
        UtilForfait utilForfait=new UtilForfait(37l,4l,10f,0f,4l,635l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator_37.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(37l, expectedMapForfaitsForCollaborator_37);

        //forfait repas
        utilForfait=new UtilForfait(106l,2l,8f,19f,1l,1025l,0l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        mapForfaitTemp=getMapForfait(utilForfait);
        expectedMapForfaitsForCollaborator.putAll(mapForfaitTemp);

        //forfait deplacement

        utilForfait=new UtilForfait(106l,2l,2f,19f,2l,1025l,1l);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));
        expectedMapMontantForfaits.put(106l, expectedMapForfaitsForCollaborator);

        //forfait telephone

        utilForfait=new UtilForfait(27l,4l,10f,null,3l,1073l,null);
        expectedListForfaits.addAll(initDonneeForfaitsDetail(utilForfait));
        expectedMapForfaitsForCollaborator.putAll(getMapForfait(utilForfait));


        assertNotNull("La récupération de données renvoit null", expectedMapForfaitsForCollaborator);
    }



    // ---------------------------------- Tests pour getDataAvancesForCollaborator -----------------------------------
    @Test
    public void testGetDataAvancesForCollaboratorNotNull() throws Exception {

        UtilAvance utilAvance=new UtilAvance(106L,12L,2240.72F,"PO");
        expectedListAvances.addAll(initDonneeAvanceDetail(utilAvance));
        expectedMapAvancesForCollaborator.putAll(getMapAvance(utilAvance));
        expectedMapMontantAvances.putAll(expectedMapAvancesForCollaborator);



        expectedCalculedList.addAll(calculFraisService.doCalculDetail("detail",expectedMapMontantFrais,expectedMapMontantForfaits,expectedMapMontantAvances));
        assertNotNull("La récupération de données renvoit null", expectedMapAvancesForCollaborator);
    }



    // -------------------------------------- Tests pour somme -----------------------------------------------------



    /**
     * Calcul des absences
     * @return
     */
    private Map<Long,Integer>calculAbsence(){
        List<Object[]> listAbsence = new ArrayList<>();
        Map<Long, Integer> mapAbsence = new HashMap<>();
        // pour chaque absence un matin, on retire un jour de forfait repas

        mapAbsence.put(0l, 2);

        return mapAbsence;
    }



    /**
     * Genere liste de frais
     * @param utilFrais
     * @return
     */
    private List< Object[]> initDonneeFraisDetail(UtilFrais utilFrais){

        List<Object[]>listRes=new ArrayList<>();
        Object[] tabFrais=new Object[6];
        tabFrais[0]=utilFrais.getIdCollab();
        tabFrais[1]=utilFrais.getIdFrais();
        tabFrais[2]=utilFrais.getMontant();
        tabFrais[3]=utilFrais.getNbKms();
        tabFrais[4]=utilFrais.getFraisForfait();
        tabFrais[5]=utilFrais.getidMission();
        listRes.add(tabFrais);
        return listRes;
    }



    /**
     *
     * Map de frais avec les infos pertinentes
     * @param utilFrais
     * @return
     */
    private Map<Long, Object[]> getMapFraisDetail(UtilFrais utilFrais) {

        Map<Long, Object[]> mapFrais=new HashMap<>() ;
        Object [] tabFraisTemp=new Object[3];
        tabFraisTemp[0]=utilFrais.getMontant();

        if (!utilFrais.getNbKms().equals(0f)){
            tabFraisTemp[0]=utilFrais.getNbKms()*0.38f;
        }
        tabFraisTemp[1]=0;
        tabFraisTemp[2]=utilFrais.getIdMission();
        mapFrais.put(utilFrais.getIdFrais(),tabFraisTemp);
        return mapFrais;
    }



    /**
     * genere la liste des avances
     * @param utilAvance
     * @return
     */
    private List<Object[]> initDonneeAvanceDetail(UtilAvance utilAvance){
        Object[] tabAvance=new Object[4];
        List<Object[]>listRes=new ArrayList<>();

        tabAvance[0]=utilAvance.getIdCollab();
        tabAvance[1]=utilAvance.getIdFrais();
        tabAvance[2]=utilAvance.getMontant();
        tabAvance[3]=utilAvance.getTypeAvance();

        listRes.add(tabAvance);
        return listRes;
    }



    /**
     * genere map d'avance
     * @param utilAvance
     * @return
     */
    private Map<Long, Object[]> getMapAvance(UtilAvance utilAvance) {
        Map<Long, Object[]> mapMontantsavance = new HashMap<>() ;
        Object[] tabTemp=new Object[2];
        if (utilAvance.getIdFrais().equals(12L)){
            tabTemp[0]= utilAvance.getTypeAvance();
            tabTemp[1]= utilAvance.getMontant();

        }
        mapMontantsavance.put(utilAvance.getIdCollab(),tabTemp);

        return mapMontantsavance;
    }




    /**
     * genere une liste de forfait
     * @param utilForfait
     * @return
     */
    private List<Object[]> initDonneeForfaitsDetail(UtilForfait utilForfait){
        List<Object[]>listRes=new ArrayList<>();
        Object tabFrais[] = new Object[7];
        tabFrais[0] = utilForfait.getIdCollab();
        tabFrais[1]=utilForfait.getIdFrequence();
        tabFrais[2]=utilForfait.getMontant();
        tabFrais[3]=utilForfait.getNb();
        tabFrais[4]=utilForfait.getIdForfait();
        tabFrais[5]=utilForfait.getIdMission();
        tabFrais[6]=utilForfait.getMomentJournee();

        listRes.add(tabFrais);
        return listRes;
    }


    /**
     * genere une map de forfait
     * @param utilForfait
     * @return
     */
    private Map<Long, Float> getMapForfait(UtilForfait utilForfait) {
        Map<Long, Float> mapForfait=new HashMap<>();
        Float montantForfait=0F;

        if (!utilForfait.getIdFrequence().equals(2L)){
            montantForfait=utilForfait.getMontant();
        }else{
            montantForfait=utilForfait.getMontant()*utilForfait.getNb();
        }

        mapForfait.put(utilForfait.getIdForfait(),montantForfait);
        return mapForfait;
    }

    /**
     *
     */

    private class UtilAvance{
        private Long idCollab;
        private Long idFrais;
        private Float montant;
        private String typeAvance;


        public UtilAvance(Long idCollab, Long idFrais, Float montant, String typeAvance) {
            this.idCollab = idCollab;
            this.idFrais = idFrais;
            this.montant = montant;
            this.typeAvance = typeAvance;
        }

        public Long getIdCollab() {
            return idCollab;
        }

        public void setIdCollab(Long idCollab) {
            this.idCollab = idCollab;
        }

        public Long getIdFrais() {
            return idFrais;
        }

        public void setIdFrais(Long idFrais) {
            this.idFrais = idFrais;
        }

        public Float getMontant() {
            return montant;
        }

        public void setMontant(Float montant) {
            this.montant = montant;
        }

        public String getTypeAvance() {
            return typeAvance;
        }

        public void setTypeAvance(String typeAvance) {
            this.typeAvance = typeAvance;
        }
    }
    private class UtilForfait{
        private Long idCollab;//id collab
        private Long idFrequence;//frais avion
        private Float montant;
        private Float nb;
        private Long idForfait;
        private Long idMission;
        private Long momentJournee;

        public UtilForfait(Long idCollab, Long idFrequence, Float montant, Float nb, Long idForfait, Long idMission, Long momentJournee) {
            this.idCollab = idCollab;
            this.idFrequence = idFrequence;
            this.montant = montant;
            this.nb = nb;
            this.idForfait = idForfait;
            this.idMission = idMission;
            this.momentJournee = momentJournee;
        }

        public Long getIdCollab() {
            return idCollab;
        }

        public void setIdCollab(Long idCollab) {
            this.idCollab = idCollab;
        }

        public Long getIdFrequence() {
            return idFrequence;
        }

        public void setIdFrequence(Long idFrequence) {
            this.idFrequence = idFrequence;
        }

        public Float getMontant() {
            return montant;
        }

        public void setMontant(Float montant) {
            this.montant = montant;
        }

        public Float getNb() {
            return nb;
        }

        public void setNb(Float nb) {
            this.nb = nb;
        }

        public Long getIdForfait() {
            return idForfait;
        }

        public void setIdForfait(Long idForfait) {
            this.idForfait = idForfait;
        }

        public Long getIdMission() {
            return idMission;
        }

        public void setIdMission(Long idMission) {
            this.idMission = idMission;
        }

        public Long getMomentJournee() {
            return momentJournee;
        }

        public void setMomentJournee(Long momentJournee) {
            this.momentJournee = momentJournee;
        }
    }
    private class UtilFrais {
        private Long idCollab;
        private Long idFrais;
        private Float montant;
        private Float nbKms;
        private String fraisForfait;
        private Long idMission;





        public UtilFrais(Long idCollab, Long idFrais, Float montant, Float nbKms, String fraisForfait, Long idMission) {
            this.idCollab = idCollab;
            this.idFrais = idFrais;
            this.montant = montant;
            this.nbKms = nbKms;
            this.fraisForfait = fraisForfait;
            this.idMission = idMission;
        }


        public Long getIdCollab() {
            return idCollab;
        }

        public void setIdCollab(Long idCollab) {
            this.idCollab = idCollab;
        }

        public Long getIdFrais() {
            return idFrais;
        }

        public void setIdFrais(Long idFrais) {
            this.idFrais = idFrais;
        }

        public Float getMontant() {
            return montant;
        }

        public void setMontant(Float montant) {
            this.montant = montant;
        }

        public Long getidMission() {
            return idMission;
        }

        public void setidMission(Long idMission) {
            this.idMission = idMission;
        }
        public Float getNbKms() {
            return nbKms;
        }

        public void setNbKms(Float nbKms) {
            this.nbKms = nbKms;
        }

        public String getFraisForfait() {
            return fraisForfait;
        }

        public void setFraisForfait(String fraisForfait) {
            this.fraisForfait = fraisForfait;
        }

        public Long getIdMission() {
            return idMission;
        }

        public void setIdMission(Long idMission) {
            this.idMission = idMission;
        }


    }
}
