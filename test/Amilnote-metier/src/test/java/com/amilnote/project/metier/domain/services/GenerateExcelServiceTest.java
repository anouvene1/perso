package com.amilnote.project.metier.domain.services;


import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.services.impl.GenerateExcelServiceImpl;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GenerateExcelServiceTest {

    private GenerateExcelService generateExcelService;


    private CollaboratorService collaboratorService = Mockito.mock(CollaboratorService.class);



    @Before
    public void setUp() {
        generateExcelService = new GenerateExcelServiceImpl(collaboratorService);
    }

    private List<List<Object[]>> getListAnnee() {
        Object[] o;
        List<Object[]> lo;
        List<List<Object[]>> llo = new ArrayList<>(12);

        for (int i=0; i<12; i++) {
            lo = new ArrayList<>(124);
            for (int j=0; j<124; j++) {
                o = new Object[15];
                o[0] = 2l;
                o[1] = "test";
                o[2] = "Test";
                for(int k = 3; k<15; k++){
                    o[k] = 0f;
                }
                lo.add(o);
            }
            llo.add(lo);
        }

        return llo;
    }

    private List<Object[]> getList() {
        Object[] o;
        List<Object[]> lo= new ArrayList<>();

        for (int j=0; j<124; j++) {
            o = new Object[9];
            o[0] = 2L;
            o[1] = "";
            o[2] = "";
            for(int k = 3; k<9; k++){
                o[k] = 0f;
            }
            lo.add(o);
        }

        return lo;
    }



    private Collaborator getCollab() {
        StatutCollaborateur sc = new StatutCollaborateur();
        sc.setStatut("3");
        sc.setCode("1");
        sc.setId(1000l);
        return new Collaborator("","", sc, true, "","","",null,null,null,true,0,Calendar.getInstance().getTime(),null,Calendar.getInstance().getTime(),"","","",null, null, Agency.VILLEURBANNE);
    }




    @Test
    public void finalizeTableListAnnee() throws Exception {
        List<List<Object[]>> listTemps = getListAnnee();
        List<Object[]> lo;
        Collaborator c = getCollab();
        c.setId(2L);

        Mockito.when(collaboratorService.findById(2L)).thenReturn(c);

        lo = generateExcelService.finalizeTableListAnnee(listTemps,2016);
        // on attend 4 car il y a un seul collab + 2 lignes d'entetes et 1 ligne de totaux
        assertEquals(lo.size(), 4);
    }

    @Test
    public void finalizeTableList() throws Exception {
        List<Object[]> listTemps = getList();
        List<Object[]> lo;

        Collaborator c = getCollab();
        c.setId(2l);
        Mockito.when(collaboratorService.findById(2l)).thenReturn(c);


        Calendar fCal = Calendar.getInstance();
        fCal.set(2016, 12, 1);


        lo = generateExcelService.finalizeTableList(listTemps, fCal.getTime());
        // on attend en plus de la liste 2 lignes d'entetes + 1 lignes de totaux
        assertEquals(lo.size(), listTemps.size()+3);


    }

    @Test
    public void finalizeTableListExpensesDetail() throws Exception {
        List<Object[]> listTemps = getList();
        List<Object[]> lo;

        Collaborator c = getCollab();
        c.setId(2l);
        Mockito.when(collaboratorService.findById(2l)).thenReturn(c);


        Calendar fCal = Calendar.getInstance();
        fCal.set(2016, 12, 1);


        lo = generateExcelService.finalizeTableListExpensesDetail(listTemps, fCal.getTime(), false);
        // on attend en plus de la liste 2 lignes d'entetes
        assertEquals(lo.size(), listTemps.size()+2);
    }

    @Test
    public void generateExcel() throws Exception {
        String result = generateExcelService.generateExcel("test", Calendar.getInstance().getTime(),"detail", getList());

        assertNotNull("fichier excel genere ",result);
    }
}
