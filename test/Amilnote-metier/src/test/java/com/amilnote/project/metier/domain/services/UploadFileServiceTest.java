package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.services.impl.FileServiceImpl;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class UploadFileServiceTest {

    private static final String PATH_PDF_FILE_TEST = "src/test/resources/Test-document.pdf";
    private static final String PATH_PNG_FILE_TEST = "src/test/resources/Test-image.png";

    @Mock
    private FileService fileService;

    private UploadFile uploader;

    @Before
    public void setUp() {
        uploader = new UploadFile();
        fileService = new FileServiceImpl();
    }

    @Test
    public void shouldRetrieveFileExtention() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        File file = path.toFile();
        try {
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            System.out.println(mockMultipartFile.getOriginalFilename());
            if(UploadFile.retrieveExtension(mockMultipartFile).isPresent()) {
                assertEquals(UploadFile.retrieveExtension(mockMultipartFile).get(), FileFormatEnum.EXT_PDF.toString());
            } else {
                fail();
            }
        } catch (IOException e) {
            fail("UploadFile Test failed to retrieve file extention");
        }
    }

    @Test
    public void shouldRetrieveMimeType() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        File file = path.toFile();
        try {
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            if(UploadFile.retrievetMIMEType(mockMultipartFile).isPresent()) {
                assertEquals(UploadFile.retrievetMIMEType(mockMultipartFile).get(), FileFormatEnum.PDF.toString());
            } else {
                fail();
            }
        } catch (IOException e) {
            fail("UploadFile Test failed to retrieve file MIME type");
        }
    }

    @Test
    public void shouldValidFileFormat() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        try {
            assertTrue(path.toFile().exists());
            File file = path.toFile();
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            assertTrue(UploadFile.isAcceptedFormat(mockMultipartFile, new FileFormatEnum[] {FileFormatEnum.PDF}));
        } catch (IOException e) {
            fail("UploadFile Test fail to validate file format");
        }
    }

    @Test
    public void shouldValidImageContent() {
        Path path = Paths.get(PATH_PNG_FILE_TEST);
        try {
            File file = path.toFile();
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            assertTrue(UploadFile.isValidImageFileContent(mockMultipartFile));
        } catch (Exception e) {
            fail("UploadFile Test fail to validate file content");
        }
    }

    @Test
    public void shouldRefuseImageContent() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        try {
            File file = path.toFile();
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            assertFalse(UploadFile.isValidImageFileContent(mockMultipartFile));
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void shouldValidFileBeforeUpload() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        try {
            File file = path.toFile();
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            uploader.setAcceptFormat(new FileFormatEnum[] {FileFormatEnum.EXT_PDF});
            uploader.isValidFile(mockMultipartFile);
            assertTrue(true);
        } catch (Exception e) {
            fail("UploadFile Test fail to valid file before a upload");
        }
    }

    @Test
    public void shouldValidFileServiceUpload() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        File file = path.toFile();
        try {
            MockMultipartFile mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
            fileService.uploadFile(mockMultipartFile, file.getAbsolutePath(), uploader.getMaxSize(),
                                                new FileFormatEnum[] {FileFormatEnum.EXT_PDF});
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void shouldThrowExceptionOnBadFileServiceUpload() {
        Path path = Paths.get(PATH_PDF_FILE_TEST);
        File file = path.toFile();
        MockMultipartFile mockMultipartFile = null;
        try {
            mockMultipartFile = new MockMultipartFile(file.getName(), Files.readAllBytes(path));
        } catch (IOException e) {
            fail();
        }
        try {
            fileService.uploadFile(mockMultipartFile, file.getAbsolutePath(), uploader.getMaxSize(),
                                                            new FileFormatEnum[] {FileFormatEnum.EXT_DOCX});
            fail();
        } catch (Exception e) {
            assertTrue(true);
        }
    }

}
