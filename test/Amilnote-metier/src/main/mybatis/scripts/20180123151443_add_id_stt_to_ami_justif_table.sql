--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // add id_stt to ami_justif table
-- Migration SQL that makes the change goes here.
ALTER TABLE ami_justif
  ADD COLUMN id_stt INT(11) NULL AFTER id_collaborateur,
  ADD INDEX id_stt (id_stt ASC);

ALTER TABLE ami_justif
  ADD CONSTRAINT ami_justif_fk_id_stt
FOREIGN KEY (id_stt) REFERENCES `ami_sous_traitant` (id)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;



-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE ami_justif DROP foreign key ami_justif_fk_id_stt, DROP INDEX id_stt, DROP COLUMN id_stt;


