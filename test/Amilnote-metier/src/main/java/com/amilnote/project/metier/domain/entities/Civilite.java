package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "ami_ref_civilite")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Civilite implements Serializable {

    /**
     * The property name
     */
    public static final String PROP_CIVILITE = "civilite";

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * Other civility
     */
    public static final String CIVILITE_INCONNU = "Non précisé";
    public static final Long CIVILITE_INCONNU_ID = 1L;

    /**
     * Mister civility
     */
    public static final String CIVILITE_MONSIEUR = "Monsieur";
    public static final Long CIVILITE_MONSIEUR_ID = 2L;

    /**
     * Miss civility
     */
    public static final String CIVILITE_MADAME = "Madame";
    public static final Long CIVILITE_MADAME_ID = 3L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = PROP_CIVILITE)
    private String civilite;

    public Civilite() {
    }

    public Civilite(String civilite) {
        this.civilite = civilite;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }
}
