/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.Mission;

import java.util.List;

/**
 * The interface Forfait dao.
 */
public interface ForfaitDAO extends LongKeyDAO<Forfait> {

    /**
     * rechercher la liste des forfaits enregistrés pour une mission
     *
     * @param pMission the p mission
     * @return List list
     */
    List<Forfait> findForfaitsForMission(Mission pMission);

    /**
     * rechercher la liste des forfaits lib enregistrés
     * @return the list of {@link Forfait}
     */
    List<Forfait> findForfaitLib();

    /**
     * rechercher la liste des forfaits non lib enregistrés
     * @return the list of {@link Forfait}
     */
    List<Forfait> findForfaitNotLib();
}
