/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Link frais tfc.
 */
@Entity
@Table(name = "ami_link_frais_tfc")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LinkFraisTFC implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_FRAIS.
     */
    public static final String PROP_FRAIS = "frais";
    /**
     * The constant PROP_LINK_TYPE_FRAIS_CARAC.
     */
    public static final String PROP_LINK_TYPE_FRAIS_CARAC = "linkTypeFraisCarac";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    private static final long serialVersionUID = 176780202913716993L;
    private Long id;
    private Frais frais;
    private LinkTypeFraisCarac typeFraisCarac;
    private Boolean val_tiny;
    private DateTime val_datetime;
    private String val_varchar;
    private Float val_decimal;


    /**
     * Constructeur par defaut de la classe LinkMissionForfait
     */
    public LinkFraisTFC() {
    }

    ;

    /**
     * Instantiates a new Link frais tfc.
     *
     * @param pFrais          the p frais
     * @param pTypeFraisCarac the p type frais carac
     * @param pVal_tiny       the p val tiny
     */
    public LinkFraisTFC(Frais pFrais, LinkTypeFraisCarac pTypeFraisCarac, Boolean pVal_tiny) {
        super();
        frais = pFrais;
        typeFraisCarac = pTypeFraisCarac;
        val_tiny = pVal_tiny;
    }

    /**
     * Instantiates a new Link frais tfc.
     *
     * @param pFrais          the p frais
     * @param pTypeFraisCarac the p type frais carac
     * @param pVal_datetime   the p val datetime
     */
    public LinkFraisTFC(Frais pFrais, LinkTypeFraisCarac pTypeFraisCarac, DateTime pVal_datetime) {
        super();
        frais = pFrais;
        typeFraisCarac = pTypeFraisCarac;
        val_datetime = pVal_datetime;
    }

    /**
     * Instantiates a new Link frais tfc.
     *
     * @param pFrais          the p frais
     * @param pTypeFraisCarac the p type frais carac
     * @param pVal_varchar    the p val varchar
     */
    public LinkFraisTFC(Frais pFrais, LinkTypeFraisCarac pTypeFraisCarac, String pVal_varchar) {
        super();
        frais = pFrais;
        typeFraisCarac = pTypeFraisCarac;
        val_varchar = pVal_varchar;
    }

    /**
     * Instantiates a new Link frais tfc.
     *
     * @param pFrais          the p frais
     * @param pTypeFraisCarac the p type frais carac
     * @param pVal_decimal    the p val decimal
     */
    public LinkFraisTFC(Frais pFrais, LinkTypeFraisCarac pTypeFraisCarac, Float pVal_decimal) {
        super();
        frais = pFrais;
        typeFraisCarac = pTypeFraisCarac;
        val_decimal = pVal_decimal;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets type frais carac.
     *
     * @return the type frais carac
     */
    @ManyToOne
    @JoinColumn(name = "id_link_type_frais_carac")
    public LinkTypeFraisCarac getTypeFraisCarac() {
        return typeFraisCarac;
    }

    /**
     * Sets type frais carac.
     *
     * @param pTypeFraisCarac the p type frais carac
     */
    public void setTypeFraisCarac(LinkTypeFraisCarac pTypeFraisCarac) {
        typeFraisCarac = pTypeFraisCarac;
    }

    /**
     * Gets frais.
     *
     * @return the frais
     */
    @ManyToOne
    @JoinColumn(name = "id_frais")
    public Frais getFrais() {
        return frais;
    }

    /**
     * Sets frais.
     *
     * @param pFrais the p frais
     */
    public void setFrais(Frais pFrais) {
        frais = pFrais;
    }

    /**
     * Gets val tiny.
     *
     * @return the val tiny
     */
    @Column(name = "val_tiny", nullable = true)
    public Boolean getVal_tiny() {
        return val_tiny;
    }

    /**
     * Sets val tiny.
     *
     * @param pVal_tiny the p val tiny
     */
    public void setVal_tiny(Boolean pVal_tiny) {
        val_tiny = pVal_tiny;
    }

    /**
     * Gets val datetime.
     *
     * @return the val datetime
     */
    @Column(name = "val_datetime", nullable = true)
    public DateTime getVal_datetime() {
        return val_datetime;
    }

    /**
     * Sets val datetime.
     *
     * @param pVal_datetime the p val datetime
     */
    public void setVal_datetime(DateTime pVal_datetime) {
        val_datetime = pVal_datetime;
    }

    /**
     * Gets val varchar.
     *
     * @return the val varchar
     */
    @Column(name = "val_varchar", nullable = true)
    public String getVal_varchar() {
        return val_varchar;
    }

    /**
     * Sets val varchar.
     *
     * @param pVal_varchar the p val varchar
     */
    public void setVal_varchar(String pVal_varchar) {
        val_varchar = pVal_varchar;
    }

    /**
     * Gets val decimal.
     *
     * @return the val decimal
     */
    @Column(name = "val_decimal", nullable = true)
    public Float getVal_decimal() {
        return val_decimal;
    }

    /**
     * Sets val decimal.
     *
     * @param pVal_decimal the p val decimal
     */
    public void setVal_decimal(Float pVal_decimal) {
        val_decimal = pVal_decimal;
    }

}
