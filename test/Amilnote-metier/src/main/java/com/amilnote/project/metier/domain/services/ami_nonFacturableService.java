package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.ami_nonFacturable;

import java.util.Date;
import java.util.List;

/**
 * Created by clome on 27/04/2017.
 */
public interface ami_nonFacturableService extends AbstractService<ami_nonFacturable> {

    /**
     * Find by date facture archivee.
     *
     * @param pProp the p property
     * @param pDateDebutMois the p date archivage début du mois
     * @param pDateFinMois the p date archivage fin du mois
     * @return the facture archivee list
     */
    List<ami_nonFacturable> findListEntitesByPropBetweenDates(String pProp, Date pDateDebutMois, Date pDateFinMois);
}
