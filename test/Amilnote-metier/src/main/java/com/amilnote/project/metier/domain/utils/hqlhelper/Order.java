/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;

import java.util.List;

/**
 * The type Order.
 */
public class Order {

    private boolean ascending;
    private String propertyName;

    private Order(String propertyName, boolean ascending) {
        this.propertyName = propertyName;
        this.ascending = ascending;
    }

    /**
     * Rendu HQL (fragment)
     *
     * @param listOrder the list order
     * @return the string
     */
    protected static String toSqlString(List<Order> listOrder) {
        String retour = "";
        int i = 0;
        for (Order order : listOrder) {
            retour = retour + order.toString();
            i++;
            if (i < listOrder.size()) {
                retour = retour + ", ";
            }

        }

        return retour;
    }

    /**
     * Orde Ascendant
     *
     * @param propertyName the property name
     * @return Order order
     */
    public static Order asc(Champ propertyName) {
        return new Order(propertyName.getChamp(), true);
    }

    /**
     * Ordre Descendant order
     *
     * @param propertyName the property name
     * @return Order order
     */
    public static Order desc(Champ propertyName) {
        return new Order(propertyName.getChamp(), false);
    }

    /**
     * {@linkplain Object#toString()}
     */
    @Override
    public String toString() {
        return propertyName + ' ' + (ascending ? "ASC" : "DESC");
    }

    /**
     * Is asc boolean.
     *
     * @return the boolean
     */
    public boolean isAsc() {
        return ascending;
    }

}

