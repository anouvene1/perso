/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.AbstractTravailleur;
import com.amilnote.project.metier.domain.entities.json.TravailleurAsJson;

import java.util.Date;
import java.util.List;

/**
 * The interface AbstractTravailleurService service.
 */
public interface AbstractTravailleurService<T extends AbstractTravailleur, JSON extends TravailleurAsJson> extends AbstractService<T> {

    /**
     * retrouver un utilisateur par rapport à son username ici mail
     *
     * @param pMail the p mail
     * @return un collaborateur
     */
    T findByMail(String pMail);

    /**
     * retrouver un utilisateur par rapport à son username ici mail et son
     * mot de passe
     *
     * @param pMail     the p mail
     * @param pPassword the p password
     * @return un collaborateur
     */
    T findByUserNameAndPassword(String pMail, String pPassword);

    /**
     * retrouver tous les utilisateurs actifs et les trier par ordre ascendant
     *
     * @return une liste de collaborateurs
     */
    List<T> findAllOrderByNomAsc();

    /**
     * retrouver tous les utilisateurs et les trier par ordre ascendant
     *
     * @return une liste de travailleurs (collab ou sous traitants, listes
     * pas mélangées) au format json triée par nom
     */
    List<JSON> findAllOrderWithDisabledByNomAscAsJson();

    /**
     * retrouver tous les utilisateurs et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<JSON> findAllOrderByNomAscAsJson();

    /**
     * Supprimer un collaborateur.
     *
     * @param pCollaborateur Collaborateur à supprimer
     * @return Message de retour
     */
    String deleteCollaborator(T pCollaborateur);

    /**
     * Met à jour ou Créer un collaborateur
     *
     * @param pCollaborateur Collaborateur à créer ou à mettre à jour
     * @return Message de retour
     * @throws Exception the exception
     */
    String updateOrCreateCollaborator(JSON pCollaborateur) throws Exception;

    /**
     * Retourne le collaborateur json en fonction de l'id absence
     *
     * @param idAbsence the id absence
     * @return un collaborateur as json
     * @throws Exception the exception
     */
    JSON findByIdAbsence(Long idAbsence) throws Exception;

    /**
     * Retourne le collaborateur json en fonction de l'id RA
     *
     * @param pId the p id
     * @return un collaborateur as json
     * @throws Exception the exception
     */
    JSON findByIdRA(Long pId) throws Exception;

    /**
     * Retourne la liste des collaborateurs par statut
     *
     * @param pStatut the p statut
     * @return la liste des collaborateurs as json
     * @throws Exception the exception
     */
    List<JSON> findAllByStatutOrderByNomAsJson(String pStatut) throws Exception;

    /**
     * Retourne la liste des collaborateurs par statut
     *
     * @param pStatut the p statut
     * @return la liste des collaborateurs
     */
    List<T> findAllByStatutOrderByNom(String pStatut);

    /**
     * Find by id collaborateur.
     *
     * @param idCollaborateur the id collaborateur
     * @return the collaborateur
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    T findById(Long idCollaborateur);

    /**
     * Retrieves a Collaborateur with his first name and his last name
     *
     * @param firstname the first name of the Collaborateur
     * @param lastname the last name of the Collaborateur
     * @return a Collaborateur
     */
    T findCollaboratorByName(String firstname, String lastname);

    /**
     * Retrieves a Collaborateur using his firstname and his last name
     *
     * @param firstname his first name
     * @param lastname his last name
     * @return a Collaborateur in JSON
     */
    JSON findCollaboratorByNameAsJSON(String firstname, String lastname);

    List<JSON> findAllWithMissionOrderByNomAscAsJson();

    /**
     * Find collaborateurs for absence month as json list.
     *
     * @param pDate the p date
     * @return the list
     */
    List<JSON> findCollaboratorsForAbsenceMonthAsJson(Date pDate);

    /**
     * {@inheritDoc}
     */
    List<JSON> findCollaboratorsForFraisAnnuelAsJson(int yearFrais);

}
