package com.amilnote.project.metier.domain.utils.enumerations;

public enum TypeMissionEnum {

    SI_INTERNE ("SI"),
    ACT_COMMERCIALE ("AC"),
    FORMATION ("FO"),
    CLIENT ("MI"),
    WEB_FACTORY ("WF"),
    MOBILE_FACTORY ("MF"),
    DATA_FACTORY ("DF"),
    SOFT_FACTORY ("SF"),
    INTERNE ("MN"),
    DIGITAL_FACTORY ("DGF");

    private final String type;

    TypeMissionEnum(final String value) {
        this.type = value;
    }

    @Override
    public String toString() {
        return this.type;
    }

}
