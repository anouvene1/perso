/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.ami_factureDeCommande;

import java.util.Date;
import java.util.List;

/**
 * The interface Facturede commande dao.
 */
public interface FacturedeCommandeDAO extends LongKeyDAO<ami_factureDeCommande> {

    /**
     * Find all bydate list.
     *
     * @param date the date
     * @return the list
     */
    List<ami_factureDeCommande> findAllBydate(Date date);
}
