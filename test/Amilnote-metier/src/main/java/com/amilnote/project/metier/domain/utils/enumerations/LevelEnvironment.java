package com.amilnote.project.metier.domain.utils.enumerations;

/**
 *
 */
public enum LevelEnvironment {
    DEV("DEV"),
    INT("INT"),
    PROD("PROD");


   private String level;

    private LevelEnvironment(String level) { this.level = level;}


    public static LevelEnvironment getLevel(String level) {

        return LevelEnvironment.valueOf(level);
    }

}
