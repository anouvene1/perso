package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.TypeFacture;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.utils.enumerations.ClientExcelEnum;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.DateTime;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * The interface Document excel service.
 */
public interface DocumentExcelService {

    /**
     * Enumeration of differents cell's alignment type (see : CellStyle class)
     */
    enum CellAlignment {GENERAL, LEFT, CENTER, RIGHT, FILL, JUSTIFY, CENTER_SELECTION}

    /**
     * Create revenues (chiffre d'affaire) excel sheet by month/year
     * param file   the current file (type : excel)
     *
     * @param typeExcel  excel type
     * @param chosenDate the chosen date
     * @param withYear   the chosen year
     * @return path of the file
     * @throws NamingException the  NamingException
     * @throws IOException     the IOException
     */
    String createExcelRevenues(String typeExcel, Date chosenDate, boolean withYear) throws IOException, NamingException;

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * facturations par collaborateur par mission par mois ou par années
     *
     * @param file  le nom du fichier a créer
     * @param pDate the p date
     * @param annee the annee
     * @return String Le chemin d'accès vers le fichier excel créé
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws IOException                the io exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     */
    String createExcelFacturationMois(File file, Date pDate, boolean annee)
            throws InvalidFormatException, IOException, ParseException, NamingException;

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * absences par collaborateur par mois
     *
     * @param fichier        le nom du fichier a créer
     * @param pDate          the p date
     * @param typeConsultant typeConsultant
     * @return String Le chemin d'accès vers le fichier excel créé
     * @throws InvalidFormatException the invalid format exception
     * @throws IOException            the io exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    String createExcelAbsenceMois(File fichier, Date pDate, boolean typeConsultant)
            throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException;

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * frais par collaborateur par mois
     *
     * @param fichier   le nom du fichier a créer
     * @param dateDebut the p date frais
     * @param dateFin   end date for frais
     * @return chemin Le chemin d'accès vers le fichier excel créé
     * @throws ParseException  the parse exception
     * @throws IOException     the io exception
     * @throws NamingException the naming exception
     */
    String createExcelFraisMois(File fichier, Date dateDebut, Date dateFin)
            throws ParseException, IOException, NamingException;

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * frais par collaborateur par mois en version simplifiée
     *
     * @param fichier   le nom du fichier a créer
     * @param dateDebut the p date frais
     * @param dateFin   the p date frais
     * @return String Le chemin d'accès vers le fichier excel créé
     * @throws ParseException  the parse exception
     * @throws IOException     the io exception
     * @throws NamingException the naming exception
     */
    String createExcelFraisMoisSimple(File fichier, Date dateDebut, Date dateFin)
            throws ParseException, IOException, NamingException;

    /**
     * Methode qui permet le lancement du tableau de recap détaillé annuel pour les frais pour l'ensemble des collab
     *
     * @param file      the file which will include the expenses
     * @param yearFrais the expense which year's date equal to this year
     * @return path of file
     */
    String createExcelFraisAnnuel(File file, int yearFrais);

    /**
     * Methode qui permet le lancement du tableau de recap annuel pour les frais pour l'ensemble des collab
     *
     * @param file      target file
     * @param yearFrais year
     * @return path of the file
     */
    String createExcelFraisAnnuelSimple(File file, int yearFrais);

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * présence par collaborateur par mois
     *
     * @param fichier        le nom du fichier a créer
     * @param year           the year
     * @param typeConsultant typeConsultant
     * @return String Le chemin d'accès vers le fichier excel créé
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws ParseException             the parse exception
     * @throws IOException                the io exception
     * @throws NamingException            the naming exception
     */
    //SBE_11/09/16_AMNOTE_170: Modif de la fonction pour qu'on puisse y ajouter l'année voulue
    String createExcelPresenceMois(File fichier, int year, boolean typeConsultant)
            throws InvalidFormatException, ParseException, IOException, NamingException;

    /**
     * Méthode permettant la création d'un fichier excel de récapitulatif des
     * Clients ayant des missions
     *
     * @param fichier    le nom du fichier a créer
     * @param choixExcel {@link ClientExcelEnum}
     * @return String Le chemin d'accès vers le fichier excel créé
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws IOException                the io exception
     * @throws NamingException            the naming exception
     */
    String createExcelClients(File fichier, ClientExcelEnum choixExcel)
            throws EncryptedDocumentException, IOException, NamingException;

    /**
     * Méthode permettant la génération d'un fichier excel a partir d'une liste de collaborateurs
     *
     * @param fichier                     the file
     * @param tmpListCollaborateursAsJson : liste de collaborateurs
     * @param choixExcel                  : quel type de excel on veut générer
     * @return String                     the file path
     * @throws EncryptedDocumentException : the encrypted document exception
     * @throws IOException                the io exception
     * @throws NamingException            the naming exception
     */
    String createExcelClientsFile(File fichier, List<CollaboratorAsJson> tmpListCollaborateursAsJson, ClientExcelEnum choixExcel)
            throws EncryptedDocumentException, IOException, NamingException;

    /**
     * Crée le fichier excel des commandes
     *
     * @param commandes a list of {@link Commande}
     * @param fileName  filename
     * @param date      date
     * @return path of the file
     * @throws NamingException NamingException
     * @throws IOException     IOException
     * @author alosat
     */
    String createExcelCommandes(List<Commande> commandes, String fileName, String date) throws NamingException, IOException;

    /**
     * [AMNOTE-308] Refonte - Méthode qui permet de créer l'excel récapitulatif du tableau des factures pour le
     * mois à l'instant courant
     *
     * @param listeFacture   List de Facture
     * @param date           DateTime
     * @param cheminMonthODF String
     * @param numODF         String
     * @param strTypeFacture type de facture
     * @return chemin du fichier
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws IOException                the io exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     */
    String createExcelRecapFacturesMois(List<Facture> listeFacture, DateTime date,
                                        String cheminMonthODF, String numODF, String strTypeFacture)
            throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException;

    /**
     * [AMNOTE-308] Refonte - Méthode qui permet de créer l'excel récapitulatif du tableau des factures de frais pour
     * le mois à l'instant courant
     *
     * @param listeFactureFrais List de Facture
     * @param date              DateTime
     * @param cheminMonthODF    String
     * @param numODF            String
     * @return String chemin du fichier
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws IOException                the io exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     */
    String createExcelRecapFacturesFraisMois(
            List<Facture> listeFactureFrais,
            DateTime date,
            String cheminMonthODF,
            String numODF)
            throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException;

    /**
     * Excel file creation
     *
     * @param listeFacture the list of all the invoices
     * @param date         the date parameter
     * @param numODF       the numODF parameter
     * @param invoiceType  the invoice type (must correspond to an enum value)
     * @param chemin       the path of the file
     * @throws IOException the IOException
     */
    void createFichierExcel(List<Facture> listeFacture, DateTime date, String numODF, TypeFacture.InvoiceType invoiceType, String chemin) throws IOException;

    /**
     * Interface TableEntry for createExcelFromTableEntries method
     */
    interface TableEntry {
        List<String> getColumnValues();
    }

    /**
     * Fonction qui créee un Tableau Excel (xlsx) à partir d'implems de TableEntry
     *
     * @param titreFeuille : le titre sur la première ligne du Tableau Excel
     * @param columnTitles : la ligne de titres des colonnes
     * @param entries      : liste d'implémentations de TableEntry, représente 1 row
     * @return : les bytes[] du fichier Excel généré
     * @throws IOException : erreur lors de l'écriture dans le ByteArrayStream
     */
    byte[] createExcelFromTableEntries(String titreFeuille, List<String> columnTitles, List<TableEntry> entries)
            throws IOException;

    /**
     * [AMNOTE-308] Refonte - Méthode qui permet de créer l'excel récapitulatif du tableau des RA non Soumis pour le
     * mois à l'instant courant
     *
     * @param listeFacture   List de Facture
     * @param date           DateTime
     * @param cheminMonthODF String
     * @param numODF         String
     * @return String chemin du fichier
     * @throws EncryptedDocumentException the encrypted document exception
     * @throws InvalidFormatException     the invalid format exception
     * @throws IOException                the io exception
     * @throws ParseException             the parse exception
     * @throws NamingException            the naming exception
     */
    String createExcelRecapRANonSoumis(List<Facture> listeFacture, DateTime date,
                                       String cheminMonthODF, String numODF)
            throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException;

}
