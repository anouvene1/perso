/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Justif;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * The interface Justif service.
 */
public interface JustifService extends AbstractService<Justif> {

    /**
     * Retourne toutes les justificatifs existants
     *
     * @return all justif
     */
    List<Justif> getAllJustif();

    /**
     * Retourne un justificatif en fonction de l'id
     *
     * @param pId the p id
     * @return Justif justif by id
     */
    Justif getJustifById(long pId);

    /**
     * Retourne tous les justifications liés au collaborateur
     *
     * @param collaborator collaborateur souhaité
     * @return Liste des jutificatifs
     */
    List<Justif> getAllJustifForCollaborator(Collaborator collaborator);

    /**
     * Méthode permettant l'ajout d'une liste de justificatif à un frais donné.
     *
     * @param pIdFrais  id du frais lié aux justificatifs
     * @param pIdJustif id des justificatifs à lier au frais
     */
    void saveNewAssocFraisJustif(Long pIdFrais, Long pIdJustif);

    /**
     * Ajoute un nouveau Justif et affecte à un frais existant
     *
     * @param path the server storage directory
     * @param fileName file name
     * @param file justificatory file multipart
     * @param idFrais ID Frais
     * @param collaborator a collaborateur
     * @throws Exception the exception
     */
    void saveNewJustifAndApplyToFrais(String path, String fileName, MultipartFile file, Long idFrais, Collaborator collaborator) throws Exception;

    /**
     * Supprimer un justificatif de la base (celui-ci ne doit pas être lié à un frais)
     *
     * @param justif le justificatif à supprimer
     * @return le message de retour (OK si tout s'est bien passé, l'erreur sinon)
     */
    String deleteJustif(Justif justif);

    /**
     * Trouve tous les justificatifs d'un frais
     *
     * @param pIdFrais identifiant du frais
     * @return liste de ses justificatifs
     */
    List<Justif> findJustifByIdFrais(Long pIdFrais);
}
