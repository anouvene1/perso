/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;

import java.util.Date;

/**
 * The type Link evenement court as json.
 */
public class LinkEvenementCourtAsJson {
    private long id;

    private Date date;

    /**
     * Instantiates a new Link evenement court as json.
     *
     * @param pLinkEvenementTimesheet the p link evenement timesheet
     */
    public LinkEvenementCourtAsJson(LinkEvenementTimesheet pLinkEvenementTimesheet) {
        id = pLinkEvenementTimesheet.getId();

        date = pLinkEvenementTimesheet.getDate();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(long pId) {
        id = pId;
    }


    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param pDate the p date
     */
    public void setDate(Date pDate) {
        date = pDate;
    }
}
