/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborateur;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.CollaborateurAsJson;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Date;
import java.util.List;

/**
 * The interface Collaborateur service.
 */
public interface CollaborateurService extends AbstractTravailleurService<Collaborateur, CollaborateurAsJson> {

    /**
     * Find all order by nom asc as json without admin list.
     *
     * @return the list
     */
    List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMIN();

    /**
     * Find all order by nom asc as json without admin list but with disabled collaborators.
     *
     * @return the list
     */
    List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMINWithDisabled();

    /**
     * Find all order by nom asc without admin list.
     *
     * @return the list
     */
    List<Collaborateur> findAllOrderByNomAscWithoutADMIN();


    /**
     * Find all order by nom asc without admin list but with disabled collaborators.
     *
     * @return the list
     */
    List<Collaborateur> findAllOrderByNomAscWithoutADMINWithDisabled();

    /**
     * {@inheritDoc}
     */
    List<CollaborateurAsJson> findCollaborateursForAbsenceMonthAsJson(Date pDate);

    /**
     * {@inheritDoc}
     */
    List<CollaborateurAsJson> findCollaborateursForFraisAnnuelAsJson(int yearFrais);

    /**
     * Retrieves all the Collaborateur who has the same Responsable
     *
     * @param responsable the responsable
     * @return a list of Collaborateur
     */
    List<Collaborateur> findCollaborateursByResponsable(Collaborateur responsable);

    /**
     * Retrieves all the Collaborateur with the same Responsable in JSON
     *
     * @param responsable the responsable
     * @return a list of Collaborateur in JSON
     */
    List<CollaborateurAsJson> findCollaborateursByResponsableAsJSON(Collaborateur responsable);

    List<ModuleAddvise> getAllModuleAddvise();

    boolean isCadre(Collaborateur collaborateur);

    /**
     * Récupère tous les collaborateurs ayant le statut manager ou le poste manager
     *
     * @return liste de managers au format json
     */
    List<CollaborateurAsJson> findAllManagersAsJson();

    /**
     * Récupère tous les collaborateurs ayant le statut responsable technique
     *
     * @return liste de responsables technique au format json
     */
    List<CollaborateurAsJson> findAllResponsablesTechniqueAsJson();

    List<CollaborateurAsJson> findAllDirectionAsJson();

    /**
     * Vérifie si le collaborateur est un ingénieur d'affaires
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isManager(Collaborateur currentUser);

    /**
     * Vérifie si le collaborateur est un responsable technique
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isResponsableTechnique(Collaborateur currentUser);

    /**
     * verifie que le collaborateur est un chef de projet
     *
     * @param currentUser current {@link Collaborateur}
     * @return true if currentUser is chief
     */
    boolean isProjectManager(Collaborateur currentUser);

    /**
     * Retourne la liste des collaborateurs sans les Admins et Directeurs
     *
     * @return a list of {@link CollaborateurAsJson}
     */
    List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMINAndDir();

    /**
     * Vérifie sir le collaborateur a le statut Direction
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isDirector(Collaborateur currentUser);

    /**
     * Vérifie si le collaborateur a le statut DRH
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isDRH(Collaborateur currentUser);

    /**
     * Verifie en base l'existance d un email
     *
     * @param nomEmail email
     * @return true if email exist
     */
    boolean checkEmailExist(String nomEmail);

    /**
     * Verifie que l'id de la mission passée correspond bien à une mission
     * qui appartient au collaborateur passé
     *
     * @param currentUser current {@link Collaborateur}
     * @param missionId   mision id
     * @return true or false
     */
    boolean hasMissionById(Collaborateur currentUser, Long missionId);

    /**
     * réccupere une liste des chefs de projets
     *
     * @return une liste des chefs de projets
     */
    List<CollaborateurAsJson> findAllChefDeProjetAsJson();

    @Override
    String updateOrCreateCollaborateur(CollaborateurAsJson pCollaborateurAsJson) throws Exception;

    /**
     * Retourne la liste des managerz
     *
     * @return liste des managers
     */
    List<CollaborateurAsJson> addListManagers();

    /**
     * Method to retrieve all collaboratos by their {@link StatutCollaborateur} status
     *
     * @param collaboratorsStatus the search will be based on this list of {@link StatutCollaborateur}
     * @return a list of {@link Collaborateur} which status corresponds to the given list of status
     */
    List<Collaborateur> findCollaboratorsByStatus(StatutCollaborateur... collaboratorsStatus);

    /**
     * Returns a list of collaborators by their status code
     *
     * @param collaboratorStatusCode an Array of {@link String}
     * @return list of {@link Collaborateur}
     */
    List<Collaborateur> findAllByStatusCode(String... collaboratorStatusCode);

    /**
     * Method to check if a collaborator is a Subcontractor or not
     *
     * @param currentUser the current collaborator user
     * @return a {@link Boolean} if the collaborator is a subcontractor or not
     */
    boolean isSubcontractor(Collaborateur currentUser);

    /**
     * Reactivate a collaborateur or subcontractor.
     *
     * @param idCollaborateur Collaborator or subcontractor identifier
     * @return Callback message when collaborator or subcontractor are reactivated
     */
    boolean changeCollaboratorActivation(Long idCollaborateur) throws Exception;

    /**
     * Retreive the list of collaborators by their status and activated or not
     * @param enabled Collaborator status activated or not
     * @param status Collaborator status
     * @return The Collaborators list
     */
    List<Collaborateur> retrieveCollaboratorByStatus(boolean enabled, StatutCollaborateur... status);

    List<CollaborateurAsJson> retrieveCollaboratorsByAgency(Agency agency);

    List<CollaborateurAsJson> retrieveCollaboratorsByManagerAndByAgency(Collaborateur manager, Agency agency);

    Collaborateur getCurrentCollaborateur();

    /**
     * Retreive the list of collaborators
     * @return The Collaborators list
     */
    ImmutablePair<List<CollaborateurAsJson>, List<CollaborateurAsJson>> retreiveCollaborators();
}
