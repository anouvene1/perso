/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Poste;

/**
 * The interface Poste dao.
 */
public interface PosteDAO extends LongKeyDAO<Poste> {
}
