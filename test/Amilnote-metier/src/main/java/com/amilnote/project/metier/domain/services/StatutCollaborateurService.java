/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.StatutCollaborateurAsJson;

import java.util.List;

/**
 * The interface Statut collaborateur service.
 */
public interface StatutCollaborateurService extends AbstractService<StatutCollaborateur> {

    /**
     * Gets all statut.
     *
     * @return the all statut
     */
    List<StatutCollaborateur> getAllStatut();

    /**
     * Gets all statut as json.
     *
     * @return the all statut as json
     */
    List<StatutCollaborateurAsJson> getAllStatutAsJson();

    /**
     * Find statut collaborateur by id statut collaborateur.
     *
     * @param id_statut the id statut
     * @return the statut collaborateur
     */
    StatutCollaborateur findStatutCollaborateurById(int id_statut);

    StatutCollaborateur findStatutCollaborateurByCode(String code);

    /**
     * Method to retrieve all collaborators's status which status code corresponds to a given list of status codes
     * @param collaboratorsStatusCodes the search will be based on this collaborators status codes list
     * @return a list of {@link StatutCollaborateur} which status codes correspond to the given status codes list
     */
    List<StatutCollaborateur> findCollaboratorsStatusByCodes(String... collaboratorsStatusCodes);
}
