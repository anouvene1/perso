package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.OutilsInterne;

import java.util.List;

public interface OutilsInterneDAO  extends LongKeyDAO<OutilsInterne>  {
}
