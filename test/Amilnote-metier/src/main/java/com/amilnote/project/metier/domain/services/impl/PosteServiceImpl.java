/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.PosteDAO;
import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Poste;
import com.amilnote.project.metier.domain.entities.json.PosteAsJson;
import com.amilnote.project.metier.domain.services.PosteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Poste.
 */
@Service("posteService")
public class PosteServiceImpl extends AbstractServiceImpl<Poste, PosteDAO> implements PosteService {

    private static final Logger logger = LogManager.getLogger(Absence.class);

    @Autowired
    private PosteDAO posteDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public PosteDAO getDAO() {
        return posteDAO;
    }

    /**
     * {@linkplain PosteService#getAllPoste()}
     */
    @Override
    public List<Poste> getAllPoste() {
        return this.getDAO().loadAll();
    }

    /**
     * {@linkplain PosteService#getAllPosteAsJson()}
     */
    @Override
    public List<PosteAsJson> getAllPosteAsJson() {
        List<Poste> lListPostes = this.getAllPoste();
        List<PosteAsJson> lListPostesAsJson = new ArrayList<>(lListPostes.size());

        for (Poste lPoste : lListPostes) {
            lListPostesAsJson.add(lPoste.toJson());
        }
        return lListPostesAsJson;
    }

    /**
     * {@linkplain PosteService#getPosteByCode(String)}
     */
    @Override
    public Poste getPosteByCode(String pCodePoste) {
        List<Poste> lListPoste = getAllPoste();
        Poste posteRetour = null;
        for (Poste tempPoste : lListPoste) {
            if (tempPoste.getCode().equals(pCodePoste)) {
                posteRetour = tempPoste;
            }
        }
        return posteRetour;
    }
}
