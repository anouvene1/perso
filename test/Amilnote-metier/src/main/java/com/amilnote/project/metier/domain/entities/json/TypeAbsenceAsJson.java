/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TypeAbsence;

/**
 * The type Type absence as json.
 */
public class TypeAbsenceAsJson {
    private long id;
    private String typeAbsence;
    private String code;


    /**
     * Instantiates a new Type absence as json.
     *
     * @param pTypeAbsence the p type absence
     */
    public TypeAbsenceAsJson(TypeAbsence pTypeAbsence) {
        id = pTypeAbsence.getId();
        typeAbsence = pTypeAbsence.getTypeAbsence();
        code = pTypeAbsence.getCode();
    }

    /**
     * Instantiates a new Type absence as json.
     */
    public TypeAbsenceAsJson() {
    }

    ;

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(long pId) {
        id = pId;
    }

    /**
     * Gets type absence.
     *
     * @return the type absence
     */
    public String getTypeAbsence() {
        return typeAbsence;
    }

    /**
     * Sets type absence.
     *
     * @param pTypeAbsence the p type absence
     */
    public void setTypeAbsence(String pTypeAbsence) {
        typeAbsence = pTypeAbsence;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        code = pCode;
    }


}
