/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import java.io.Serializable;


/**
 * The type Full calendar period events.
 */
public class FullCalendarPeriodEvents implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long idPM;

    private Long idAM;
    private String start;

    private String end;

    /**
     * Instantiates a new Full calendar period events.
     */
    public FullCalendarPeriodEvents() {
    }

    ;

    /**
     * Instantiates a new Full calendar period events.
     *
     * @param pIdPM  the p id pm
     * @param pStart the p start
     * @param pEnd   the p end
     * @param pIdAM  the p id am
     */
    public FullCalendarPeriodEvents(Long pIdPM, String pStart, String pEnd, Long pIdAM) {
        setIdPM(pIdPM);
        setIdAM(pIdAM);
        setStart(pStart);
        setEnd(pEnd);

    }

    /**
     * Gets id pm.
     *
     * @return the id pm
     */
    public Long getIdPM() {
        return idPM;
    }

    /**
     * Sets id pm.
     *
     * @param pIdPM the p id pm
     */
    public void setIdPM(Long pIdPM) {
        idPM = pIdPM;
    }

    /**
     * Gets id am.
     *
     * @return the id am
     */
    public Long getIdAM() {
        return idAM;
    }

    /**
     * Sets id am.
     *
     * @param pIdAM the p id am
     */
    public void setIdAM(Long pIdAM) {
        idAM = pIdAM;
    }

    /**
     * Gets start.
     *
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * Sets start.
     *
     * @param pStart the p start
     */
    public void setStart(String pStart) {
        start = pStart;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public String getEnd() {
        return end;
    }

    /**
     * Sets end.
     *
     * @param pEnd the p end
     */
    public void setEnd(String pEnd) {
        end = pEnd;
    }


}
