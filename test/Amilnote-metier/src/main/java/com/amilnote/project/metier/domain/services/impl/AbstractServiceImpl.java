/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.LongKeyDAO;
import com.amilnote.project.metier.domain.services.AbstractService;
import com.amilnote.project.metier.domain.utils.Properties;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The type Abstract service.
 *
 * @param <T>        the type parameter
 * @param <TypedDAO> the type parameter
 */
public abstract class AbstractServiceImpl<T, TypedDAO extends LongKeyDAO<T>> implements AbstractService<T> {

    private static final Logger logger = LogManager.getLogger(AbstractServiceImpl.class);

    /**
     * The Properties.
     */
    @Autowired
    protected Properties properties;

    /**
     * Gets dao.
     *
     * @return the dao
     */
    public abstract TypedDAO getDAO();

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public T load(Long id) {
        return getDAO().load(id);
    }

    /**
     * {@linkplain AbstractService#get(Long)}
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public T get(Long id) {
        return getDAO().get(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void save(T t) {
        getDAO().save(t);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void saveOrUpdate(T t) {
        getDAO().saveOrUpdate(t);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<T> findAll(Order order) {
        return getDAO().findAll(order);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public void delete(T t) {
        getDAO().delete(t);
    }

    /**
     * Write json string.
     *
     * @param pListEntityAsJson the p list entity as json
     * @return the string
     */
    public String writeJson(List<?> pListEntityAsJson) {
        ObjectMapper lMapper = new ObjectMapper();
        lMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        lMapper.setSerializationInclusion(Include.NON_NULL);
        String lJson = "";
        try {
            lJson = lMapper.writeValueAsString(pListEntityAsJson);
            return lJson;
        } catch (JsonProcessingException e) {
            logger.error("[write json] json error", e);
        }
        return lJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findUniqEntiteByProp(String propName, Object propValue) {
        return getDAO().findUniqEntiteByProp(propName, propValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findListEntitesByProp(String propName, Object propValue) {
        return getDAO().findListEntitesByProp(propName, propValue);
    }

    /**
     * {@linkplain AbstractService#findBySearchCriterias(String, Boolean, SearchCriteria[])}
     */
    public List<T> findBySearchCriterias(String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) {
        return getDAO().findBySearchCriterias(orderingProperty, isAscendingOrdering, searchCriterias);
    }

}
