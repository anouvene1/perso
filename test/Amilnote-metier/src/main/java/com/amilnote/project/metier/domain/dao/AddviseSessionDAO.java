/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.AddviseSession;

import java.util.Date;
import java.util.List;

public interface AddviseSessionDAO extends LongKeyDAO<AddviseSession> {
    /**
     * Permet la sauvegarde d'un AddviseSession nouveau ou existant
     *
     * @param addviseSession AddviseSession
     * @return 0
     */
    int createOrUpdateAddviseSession(AddviseSession addviseSession);

    /**
     * retourne une session Addvise à partir de son id
     *
     * @param id Long
     * @return AddviseSession
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    AddviseSession findById(int id);

    /**
     * Retourne toutes les sessions avec un des états de la liste et un intervalle de dates
     *
     * @param etatsSession liste des états de sessions Addvise
     * @param dateDebut Date
     * @param dateFin Date
     * @return liste de sessions Addvise
     */
    List<AddviseSession> findAllByEtatsAndDateOrderByDate(List<AddviseSession.EtatSession> etatsSession, Date dateDebut, Date dateFin);
}
