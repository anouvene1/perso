/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * The type Parametrage.
 */
public class Parametrage {

    /**
     * Durée de la période, en mois, à affichager dans l'écran d'historique de frais
     */
    public final static int DURATION_PERIOD_HISTO_FRAIS = 13;

    /**
     * Nombre de retour maximum autorisés pour l'utilisateur, sur le calendrier, quand il clique sur la flèche "mois précedent"
     * - Est aussi utile pour la récupèration de la liste des missions sur la page des timesheet (leurs date-fin doit être supérieur ou égal au mois courant - ce paramètre)
     */
    public final static int PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR = 6;

    /**
     * Nombre de retour maximum autorisés pour l'utilisateur, sur le selecteur Mission, dans la page "Note de Frais"
     */
    public final static int PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_SELECTOR_FRAIS = 2;

    /**
     * Cette méthode permet de récupérer les variables dans le fichier de contexte externalise amilnote.xml
     *
     * @param CONTEXT_ROOT the context root
     * @return the context
     * @throws NamingException the naming exception
     */
    public static String getContext(String CONTEXT_ROOT) throws NamingException {
        Context envCtx = new InitialContext();
        String value = null;
        if (envCtx != null) {
            try {
                value = (String) envCtx.lookup("java:/comp/env/" + CONTEXT_ROOT);
            } catch (NamingException ne) {
                throw (ne);
            }

        }
        return value;
    }

}


