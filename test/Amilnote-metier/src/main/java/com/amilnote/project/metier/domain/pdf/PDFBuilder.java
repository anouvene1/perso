/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.utils.Properties;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.concatenateObjectsToString;

/**
 * Classe dont héritent toutes le classes de génération de PDF.
 *
 * @author AJakubiak
 */
public class PDFBuilder extends PdfPageEventHelper {

    protected static Logger logger = LogManager.getLogger(PDFBuilder.class);

    /**
     * PDF TEXT HEADER
     */
    protected static final String COLLABORATOR_HEADER = "COLLABORATEUR";
    protected static final String RESPONSIBLE_HEADER = "RESPONSABLE";
    protected static final String TOTAL_LABEL = "TOTAL";

    /**
     * PDF LABELS
     */
    protected static final String PHONE_LABEL = "Téléphone : ";
    protected static final String FIRST_AND_LAST_NAME_LABEL = "Nom, Prénom : ";
    protected static final String MISSION_LABEL = "Mission";

    /**
     * Customized Font
     */
    protected static final Font FONT_TITLE = FontFactory.getFont(FontFactory.HELVETICA, 16);
    protected static final Font FONT_TITLE_BOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18);
    protected static final Font FONT_BOLD = FontFactory.getFont(FontFactory.HELVETICA_BOLD); //Default size is 12
    protected static final Font FONT_HEADER = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12);
    protected static final Font FONT_HEADER_WHITE = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
    protected static final Font FONT_ADDRESS = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 12);
    protected static final Font FONT_FOOTER = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 10);
    protected static final Font FONT_ITALIC_MIN = FontFactory.getFont(FontFactory.HELVETICA_OBLIQUE, 7);

    /**
     * Customized Date formatter
     */
    protected static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat(DATE_FORMAT_dd_MMMM_yyyy_SPACE);
    protected static final SimpleDateFormat DATE_FORMATER_TEXT_MONTH = new SimpleDateFormat(DATE_FORMAT_dd_MM_yyyy_SPACE);

    /**
     * Generic caracter
     */
    protected static final String BLANK_SPACE = " ";

    /**
     * The Properties.
     */
    @Autowired
    protected Properties properties;

    protected Font fontHeaderWhite;

    protected Font fontHeaderGrey;

    protected Font fontPurpleHaze;

    protected Font fontTableHeader;

    public PDFBuilder() {
        this.fontHeaderWhite = FONT_HEADER_WHITE;
        this.fontHeaderWhite.setColor(BaseColor.WHITE);

        this.fontTableHeader = FONT_BOLD;
        this.fontTableHeader.setColor(BaseColor.BLACK);

        this.fontHeaderGrey = FONT_HEADER;
        this.fontHeaderGrey.setColor(NEW_TEXT_HEADER);

        this.fontPurpleHaze = FONT_TITLE_BOLD;
        this.fontPurpleHaze.setColor(NEW_TEXT_MISSION_HEADER);
    }

    /**
     * creation du nouveau document pdf
     *
     * @return Document document
     */
    protected Document newDocument() {
        return new Document(PageSize.A4, 20, 20, 60, 20);
    }

    /**
     * creation du writer
     *
     * @param pDocument         the p document
     * @param pFileOutputStream the p file output stream
     * @return PdfWriter pdf writer
     * @throws DocumentException the document exception
     */
    protected PdfWriter newWriter(Document pDocument, FileOutputStream pFileOutputStream) throws DocumentException {
        return PdfWriter.getInstance(pDocument, pFileOutputStream);
    }


    /**
     * preparation du writer
     *
     * @param pWriter the p writer
     * @throws DocumentException the document exception
     */
    protected void prepareWriter(PdfWriter pWriter) throws DocumentException {

        pWriter.setViewerPreferences(getViewerPreferences());
    }

    /**
     * Gets viewer preferences.
     *
     * @return the viewer preferences
     */
    protected int getViewerPreferences() {
        return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
    }

    /**
     * construction des metadata du pdf
     *
     * @param pDocument the p document
     */
    protected void buildPdfMetadata(Document pDocument) {
        // TODO complete or remove this method
    }


    /**
     * Créer la table contenant la date de création du PDF
     *
     * @param pSujet Titre de la partie date de création dans la doc
     * @param today  the today
     * @return PdfPTable Table de date de création du PDF
     */
    protected PdfPTable createTableDateCreation(String pSujet, Date today) {
        PdfPTable lTableDate = new PdfPTable(1);
        lTableDate.setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableDate.setWidthPercentage(100);
        lTableDate.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableDate.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableDate.addCell(new Phrase(pSujet, FONT_HEADER_WHITE));
        lTableDate.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableDate.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableDate.addCell(new Phrase(DATE_FORMATER.format(today)));
        lTableDate.setSpacingAfter(10);
        return lTableDate;
    }

    /**
     * Créer la table contenant les informations de l'utilisateur courant
     *
     * @param collaborator Collaborateur à traiter
     * @return PdfPTable Table d'informations sur le collaborateur
     */
    public PdfPTable createTableUser(Collaborator collaborator) {
        PdfPTable lTableColRes = new PdfPTable(2);
        lTableColRes.setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableColRes.setWidthPercentage(100);
        lTableColRes.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableColRes.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableColRes.addCell(new Phrase(COLLABORATOR_HEADER, FONT_HEADER_WHITE));
        lTableColRes.addCell(new Phrase(RESPONSIBLE_HEADER, FONT_HEADER_WHITE));
        lTableColRes.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableColRes.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableColRes.addCell(new Phrase(concatenateObjectsToString(
                FIRST_AND_LAST_NAME_LABEL,
                collaborator.getNom(),
                SPACE_SEPARATION,
                collaborator.getPrenom(),
                DOUBLE_RETURN,
                PHONE_LABEL
        )));
        if (collaborator.getManager() != null) {
            lTableColRes.addCell(new Phrase(FIRST_AND_LAST_NAME_LABEL.concat(collaborator.getManager().getNom().concat(" ").concat(collaborator.getManager().getPrenom()) + "\n\n")));
        } else {
            lTableColRes.addCell(new Phrase(FIRST_AND_LAST_NAME_LABEL));
        }

        lTableColRes.setSpacingAfter(10);
        return lTableColRes;
    }

    /**
     * Inner class to add a table as header.
     */
    class TableHeader extends PdfPageEventHelper {
        /**
         * The header text.
         */
        String header;
        /**
         * The template with the total number of pages.
         */
        PdfTemplate total;

        /**
         * Allows us to change the content of the header.
         *
         * @param pHeader the p header
         */
        public void setHeader(String pHeader) {
            this.header = pHeader;
        }

        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        @Override
        public void onOpenDocument(PdfWriter pWriter, Document pDocument) {
            total = pWriter.getDirectContent().createTemplate(10, 15);
            total.setColorFill(BaseColor.WHITE);
        }

        /**
         * Adds a header to every page
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        @Override
        public void onEndPage(PdfWriter pWriter, Document pDocument) {

            URL lUrl = getClass().getResource("/imagesPDF/amilnote-logo-header.png");
            Image lImg = null;
            try {
                lImg = Image.getInstance(lUrl);
            } catch (BadElementException | IOException e) {
                logger.error(e.getMessage());
            }

            PdfPTable lTableHeader = new PdfPTable(4);
            try {
                lTableHeader.setTotalWidth(555);
                lTableHeader.setWidths(new int[]{200, 330, 115, 10});
                lTableHeader.setLockedWidth(true);
                lTableHeader.getDefaultCell().setFixedHeight(40);
                lTableHeader.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                lTableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                lTableHeader.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                lTableHeader.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                lTableHeader.addCell(lImg);

                lTableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                lTableHeader.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                lTableHeader.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                lTableHeader.addCell(new Phrase(header, FONT_HEADER_WHITE));
                lTableHeader.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                lTableHeader.addCell(new Phrase(String.format("Page %d sur", pWriter.getPageNumber()), FONT_HEADER_WHITE));
                PdfPCell lCell = new PdfPCell(Image.getInstance(total));
                lCell.setBackgroundColor(PDF_HEADER_BLUE_COLOR);
                lCell.setBorder(Rectangle.NO_BORDER);
                lCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                lCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                lTableHeader.addCell(lCell);
                lTableHeader.writeSelectedRows(0, -5, 20, 830, pWriter.getDirectContent());
            } catch (DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }

        /**
         * Fills out the total number of pages before the document is closed.
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        @Override
        public void onCloseDocument(PdfWriter pWriter, Document pDocument) {
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(pWriter.getPageNumber() - 1)), 2, 2, 0);
        }
    }

    /**
     * Method to merge/combine multiple existing pdf retrieved by their file paths
     *
     * @param resultPdfFilePath the generated pdf that will include all retrieved pdf will be save at this path
     * @param isSmartCopy       choose if merging with PdfSmartCopy or PdfCopy
     * @param pdfFilesPaths     multiple pdf file paths to combine
     * @throws IOException when no pdf file of archived invoices for the given month found on storage
     */
    public static void mergePdfFilesByFilesPath(String resultPdfFilePath, boolean isSmartCopy, String... pdfFilesPaths) throws IOException {
        Document document = new Document();
        try (OutputStream resultPdf = new FileOutputStream(resultPdfFilePath)) {
            PdfCopy copy;
            if (isSmartCopy)
                copy = new PdfSmartCopy(document, resultPdf);
            else
                copy = new PdfCopy(document, resultPdf);

            document.open();
            copyPdfFilesToPdfCopy(copy, pdfFilesPaths);
            document.close();

        } catch (DocumentException de) {
            logger.error(de.getMessage());
        }
    }

    /**
     * Copy pdf files retrieved by their paths into a {@link PdfCopy} or {@link PdfSmartCopy}
     *
     * @param copy          the {@link PdfCopy} or {@link PdfSmartCopy} which will include the retrieved pdf
     * @param pdfFilesPaths multiple pdf file paths to combine
     * @throws DocumentException pdf file read/write problems
     * @throws IOException       local/network file/directory read/write problems
     */
    private static void copyPdfFilesToPdfCopy(PdfCopy copy, String[] pdfFilesPaths) throws DocumentException, IOException {
        int retrievedPdfFilesNumber = 0;
        for (String pdfFilePath : pdfFilesPaths) {
            PdfReader reader = null;
            try {
                reader = new PdfReader(pdfFilePath);
            } catch (IOException ioe) {
                if (logger != null) logger.error(ioe.getMessage());
            }
            if (reader != null) {
                copy.addDocument(reader);
                copy.freeReader(reader);
                reader.close();
                retrievedPdfFilesNumber++;
            }
        }
        // si aucun pdf n'a été recupéré pour être inclus dans le PdfCopy on lance une exception
        if (retrievedPdfFilesNumber == 0) {
            throw new IOException("Aucun fichier pdf de facture archivée trouvé pour ce mois - ");
        }
    }

}
