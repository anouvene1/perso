/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.DemandeDeplacement;
import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;

import java.io.File;
import java.util.List;

/**
 * The interface Demande deplacement service.
 */
public interface DemandeDeplacementService extends AbstractService<DemandeDeplacement> {

    /**
     * Gets all demande deplacement as json.
     *
     * @return the all demande deplacement as json
     */
    List<DemandeDeplacementAsJson> getAllDemandeDeplacementAsJson();

    /**
     * Gets all demande deplacement.
     *
     * @return the all demande deplacement
     */
    List<DemandeDeplacement> getAllDemandeDeplacement();

    /**
     * Sauvegarde d'une nouvelle demande de déplacement
     * return 1 si la modification s'est bien passé, 2 si la création s'est bien passée, Autre en cas d'erreur
     *
     * @param pDemandeDeplacementAsJson the p demande deplacement as json
     * @return string string
     * @throws Exception the exception
     */
    String saveNewDemandeDeplacement(DemandeDeplacementAsJson pDemandeDeplacementAsJson) throws Exception;

    /**
     * Change l'etat du deplacement en fonction de l'id et du code de l'etat et
     * envoie la mail d'information
     *
     * @param pIdDeplacement the p id deplacement
     * @param pEtat          the p etat
     * @param pCommentaire   the p commentaire
     * @return the string
     * @throws Exception the exception
     */
    String actionValidationDeplacement(Long pIdDeplacement, String pEtat, String pCommentaire) throws Exception;

    /**
     * Retourne le fichier PDF Déplacement lié à l'ID
     *
     * @param pId the p id
     * @return Fichier PDF du RA souhaité
     */
    File getFileDeplacementbyId(Long pId);

    /**
     * Retourne la liste de tous les déplacements à l'état soumis
     *
     * @return all demande deplacement etat so
     */
    List<DemandeDeplacementAsJson> getAllDemandeDeplacementEtatSO();

}
