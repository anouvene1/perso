/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.entities.LinkFraisTFC;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Frais as json.
 */
public class FraisAsJson {

    private Long id;

    private String mission;

    private Long idMission;

    private Date dateEvenement;

    private String typeFrais;

    private BigDecimal montant;

    private Integer nombreKm;

    private Integer nbNuit;

    private String commentaire;

    private Long hasJustif;

    private List<JustifAsJson> justifs;

    private EtatAsJson etat;

    private List<LinkFraisTFCAsJson> listLinkFraisTFCAsJson = new ArrayList<LinkFraisTFCAsJson>();

    private Boolean national;

    private String typeAvance;

    private String fraisForfait;

    private String pdfAvanceFrais;

    private Date dateValidation;

    private Date dateSolde;

    private String collab;

    /**
     * Instantiates a new Frais as json.
     */
    public FraisAsJson() {
        hasJustif = 0L;
    }

    ;

    /**
     * Instantiates a new Frais as json.
     *
     * @param pFrais the p frais
     */
    public FraisAsJson(Object[] pFrais) {
        this.setId((Long) pFrais[0]);
        this.setMission((String) pFrais[1]);
        this.setDateEvenement((Date) pFrais[2]);
        this.setTypeFrais((String) pFrais[3]);
        this.setMontant((BigDecimal) pFrais[4]);
        this.setCommentaire((String) pFrais[5]);
        this.setHasJustif((Long) pFrais[6]);
        this.setNational((Boolean) pFrais[7]);
        this.setNombreKm((Integer) pFrais[8]);
        this.setTypeAvance((String) pFrais[9]);
        this.setFraisForfait((String) pFrais[10]);
    }

    /**
     * Instantiates a new Frais as json.
     *
     * @param pFrais the p frais
     */
    public FraisAsJson(Frais pFrais) {
        this.setId(pFrais.getId());
        this.setCommentaire(pFrais.getCommentaire());
        this.setMontant(pFrais.getMontant());
        this.setNombreKm(pFrais.getNombreKm());
        this.setEtat(new EtatAsJson(pFrais.getEtat()));
        this.setListLinkFraisTFCAsJson(pFrais.getLinkFraisTFC());
        this.setNational(pFrais.getNational());
        this.setDateEvenement(pFrais.getLinkEvenementTimesheet().getDate());
        this.setTypeFrais(pFrais.getTypeFrais().getCode());
        this.setHasJustif(new Long(pFrais.getJustifs().size()));
        this.setTypeAvance(pFrais.getTypeAvance());
        this.setFraisForfait(pFrais.getFraisForfait());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets id mission.
     *
     * @return the idMission
     */
    public Long getIdMission() {
        return idMission;
    }


    /**
     * Sets id mission.
     *
     * @param idMission the id mission
     */
    public void setIdMission(Long idMission) {
        this.idMission = idMission;
    }

    /**
     * Gets date evenement.
     *
     * @return the dateEvenement
     */
    public Date getDateEvenement() {
        return dateEvenement;
    }

    /**
     * Sets date evenement.
     *
     * @param pDateEvenement the dateEvenement to set
     */
    public void setDateEvenement(Date pDateEvenement) {
        dateEvenement = pDateEvenement;
    }

    /**
     * Gets type frais.
     *
     * @return the typeFrais
     */
    public String getTypeFrais() {
        return typeFrais;
    }

    /**
     * Sets type frais.
     *
     * @param pTypeFrais the typeFrais to set
     */
    public void setTypeFrais(String pTypeFrais) {
        typeFrais = pTypeFrais;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the montant to set
     */
    public void setMontant(BigDecimal pMontant) {
        montant = pMontant;
    }

    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param pCommentaire the commentaire to set
     */
    public void setCommentaire(String pCommentaire) {
        commentaire = pCommentaire;
    }

    /**
     * Gets has justif.
     *
     * @return the hasJustif
     */
    public Long getHasJustif() {
        return hasJustif;
    }

    /**
     * Sets has justif.
     *
     * @param pJustif the hasJustif to set
     */
    public void setHasJustif(Long pJustif) {
        hasJustif = pJustif;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the mission to set
     */
    public void setMission(String pMission) {
        mission = pMission;
    }

    /**
     * Gets list link frais tfc as json.
     *
     * @return the listLinkFraisTFCAsJson
     */
    public List<LinkFraisTFCAsJson> getListLinkFraisTFCAsJson() {
        return listLinkFraisTFCAsJson;
    }

    /**
     * Sets list link frais tfc as json.
     *
     * @param pListLinkFraisTFC the p list link frais tfc
     */
    public void setListLinkFraisTFCAsJson(List<LinkFraisTFC> pListLinkFraisTFC) {

        for (LinkFraisTFC linkFraisTFC : pListLinkFraisTFC) {

            LinkFraisTFCAsJson tmpLinkFraisTFC = new LinkFraisTFCAsJson(linkFraisTFC);
            this.getListLinkFraisTFCAsJson().add(tmpLinkFraisTFC);

        }
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public EtatAsJson getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the etat to set
     */
    public void setEtat(EtatAsJson pEtat) {
        etat = pEtat;
    }

    /**
     * Gets justifs.
     *
     * @return the justifs
     */
    public List<JustifAsJson> getJustifs() {
        return justifs;
    }

    /**
     * Sets justifs.
     *
     * @param pJustifs the justifs to set
     */
    public void setJustifs(List<JustifAsJson> pJustifs) {
        justifs = pJustifs;
    }

    /**
     * Gets national.
     *
     * @return the national
     */
    public Boolean getNational() {
        return national;
    }

    /**
     * Sets national.
     *
     * @param pNational the p national
     */
    public void setNational(Boolean pNational) {
        national = pNational;
    }

    /**
     * Gets type avance.
     *
     * @return the type avance
     */
    public String getTypeAvance() {
        return typeAvance;
    }

    /**
     * Sets type avance.
     *
     * @param pTypeAvance the p type avance
     */
    public void setTypeAvance(String pTypeAvance) {
        typeAvance = pTypeAvance;
    }

    /**
     * Gets frais forfait.
     *
     * @return the frais forfait
     */
    public String getFraisForfait() {
        return fraisForfait;
    }

    /**
     * Sets frais forfait.
     *
     * @param pFraisForfait the p frais forfait
     */
    public void setFraisForfait(String pFraisForfait) {
        fraisForfait = pFraisForfait;
    }

    /**
     * Gets nombre km.
     *
     * @return the nombre km
     */
    public Integer getNombreKm() {
        return nombreKm;
    }

    /**
     * Sets nombre km.
     *
     * @param pNombreKm the p nombre km
     */
    public void setNombreKm(Integer pNombreKm) {
        nombreKm = pNombreKm;
    }

    /**
     * Gets nb nuit.
     *
     * @return the nb nuit
     */
    public Integer getNbNuit() {
        return nbNuit;
    }

    /**
     * Sets nb nuit.
     *
     * @param pNbNuit the p nb nuit
     */
    public void setNbNuit(Integer pNbNuit) {
        nbNuit = pNbNuit;
    }

    /**
     * Gets pdf avance frais.
     *
     * @return the pdfAvanceFrais
     */
    public String getPdfAvanceFrais() {
        return pdfAvanceFrais;
    }

    /**
     * Sets pdf avance frais.
     *
     * @param pdfAvanceFrais the pdfAvanceFrais to set
     */
    public void setPdfAvanceFrais(String pdfAvanceFrais) {
        this.pdfAvanceFrais = pdfAvanceFrais;
    }

    /**
     * Gets date validation.
     *
     * @return the date validation
     */
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param dateValidation the dateValidation to set
     */
    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    /**
     * Gets date solde.
     *
     * @return the dateSolde
     */
    public Date getDateSolde() {
        return dateSolde;
    }

    /**
     * Sets date solde.
     *
     * @param dateSolde the dateSolde to set
     */
    public void setDateSolde(Date dateSolde) {
        this.dateSolde = dateSolde;
    }

    /**
     * Gets collab.
     *
     * @return the collab
     */
    public String getCollab() {
        return collab;
    }

    /**
     * Sets collab.
     *
     * @param collab the collab to set
     */
    public void setCollab(String collab) {
        this.collab = collab;
    }
}
