/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.dao.DemandeDeplacementDAO;
import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.impl.DemandeDeplacementDAOImpl;
import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.DemandeDeplacement;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;
import com.amilnote.project.metier.domain.services.DemandeDeplacementService;
import com.amilnote.project.metier.domain.services.FileService;
import com.amilnote.project.metier.domain.services.MailService;
import com.amilnote.project.metier.domain.utils.Parametrage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Demande deplacement service.
 */
@Service("demandeDeplacementService")
public class DemandeDeplacementServiceImpl extends AbstractServiceImpl<DemandeDeplacement, DemandeDeplacementDAO> implements DemandeDeplacementService {

    private static final Logger logger = LogManager.getLogger(DemandeDeplacementServiceImpl.class);

    public static final String MOVEMENT_VALIDATED_WITH_SUCCESS = "Validation du deplacement effectuée avec succès";
    public static final String MOVEMENT_REFUSED_WITH_SUCCESS = "Refus du deplacement effectué avec succès";
    public static final String MOVEMENT_CANCELLED_WITH_SUCCESS = "Annulation du déplacement effectuée avec succès";

    @Autowired
    private EtatDAO etatDAO;

    @Autowired
    private CollaboratorDAO collaboratorDAO;

    @Autowired
    private DemandeDeplacementDAOImpl demandeDeplacementDAO;

    @Autowired
    private MailService mailService;

    @Autowired
    private FileService fileService;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public DemandeDeplacementDAO getDAO() {
        return demandeDeplacementDAO;
    }

    /**
     * {@linkplain DemandeDeplacementService#getAllDemandeDeplacementAsJson()}
     */
    @Override
    public List<DemandeDeplacementAsJson> getAllDemandeDeplacementAsJson() {

        List<DemandeDeplacement> lListDemandeDeplacement = demandeDeplacementDAO.loadAll();
        List<DemandeDeplacementAsJson> lListDemandeDeplacementAsJson = new ArrayList<>();

        for (DemandeDeplacement lDemandeDeplacement : lListDemandeDeplacement) {
            lListDemandeDeplacementAsJson.add(lDemandeDeplacement.toJson());
        }
        return lListDemandeDeplacementAsJson;
    }

    /**
     * {@linkplain DemandeDeplacementService#getAllDemandeDeplacement()}
     */
    @Override
    public List<DemandeDeplacement> getAllDemandeDeplacement() {
        List<DemandeDeplacement> lListDemandeDeplacement = demandeDeplacementDAO.loadAll();
        return lListDemandeDeplacement;
    }

    /**
     * {@linkplain DemandeDeplacementService#saveNewDemandeDeplacement(DemandeDeplacementAsJson)}
     */
    @Override
    public String saveNewDemandeDeplacement(DemandeDeplacementAsJson pDemandeDeplacementAsJson) throws Exception {

        String messageRetour = "";

        if (null == pDemandeDeplacementAsJson) {
            throw new Exception("La demande de déplacement ne doit pas être vide");
        }

        DemandeDeplacement tmpDemandeDeplacement = null;
        Float NbJours;
        long NbSecondes = pDemandeDeplacementAsJson.getDateFin().getTime() - pDemandeDeplacementAsJson.getDateDebut().getTime();
        long jours = NbSecondes / 86400000;
        NbJours = new Float(jours + 1); //+1 pour compter le jour de début

        tmpDemandeDeplacement = new DemandeDeplacement(
                pDemandeDeplacementAsJson.getMission(),
                pDemandeDeplacementAsJson.getCodeProjet(),
                pDemandeDeplacementAsJson.getMotif(),
                pDemandeDeplacementAsJson.getDateDebut(),
                pDemandeDeplacementAsJson.getDateFin(),
                pDemandeDeplacementAsJson.getEtat(),
                pDemandeDeplacementAsJson.getAvance(),
                pDemandeDeplacementAsJson.getDateSoumission(),
                pDemandeDeplacementAsJson.getDateValidation(),
                pDemandeDeplacementAsJson.getLieu(),
                pDemandeDeplacementAsJson.getValidationClient(),
                pDemandeDeplacementAsJson.getCommentaire(),
                NbJours,
                pDemandeDeplacementAsJson.getCollaborateur());
        messageRetour = "2";

        // --- Création de la session et de la transaction
        Session session = demandeDeplacementDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        session.save(tmpDemandeDeplacement);
        session.flush();
        transaction.commit();

        if (pDemandeDeplacementAsJson.getId() == null) {
            pDemandeDeplacementAsJson.setId(tmpDemandeDeplacement.getId());
        }

        return messageRetour;
    }

    /**
     * {@linkplain DemandeDeplacementService#actionValidationDeplacement(Long, String, String)}
     */
    @Override
    public String actionValidationDeplacement(Long pIdDeplacement, String pCodeEtat, String pCommentaire) throws Exception {
        String msgRetour;
        DemandeDeplacement dep = this.getDAO().get(pIdDeplacement);
        Date dateValid = new Date();
        Etat newEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pCodeEtat);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Collaborator collaborateurConnecte = collaboratorDAO.findUniqEntiteByProp(Collaborator.PROP_MAIL, userDetails.getUsername());
        if (null == dep) {
            throw new Exception("L'id du deplacement (" + pIdDeplacement + ") est inconnu");
        }

        if (null == newEtat) {
            throw new Exception("Ce code etat (" + pCodeEtat + ") est inconnu. ");
        }


        // --- Création de la session et de la transaction
        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();


        switch (newEtat.getCode()) {
            //cas deplacement soumis --> validé
            case Etat.ETAT_VALIDE_CODE:

                dep.setEtat(newEtat);
                dep.setDateValidation(dateValid);

                mailService.sendMailMovementValidation(newEtat.getCode(), collaborateurConnecte, dep, pCommentaire);

                msgRetour = MOVEMENT_VALIDATED_WITH_SUCCESS;
                break;

            //cas Deplacement soumis --> refusé
            case Etat.ETAT_REFUSE:


                dep.setEtat(newEtat);

                mailService.sendMailMovementValidation(newEtat.getCode(), collaborateurConnecte, dep, pCommentaire);
                msgRetour = MOVEMENT_REFUSED_WITH_SUCCESS;
                break;

            //cas deplacement validé --> annulé
            case Etat.ETAT_ANNULE:
                session.delete(dep);

                mailService.sendMailMovementValidation(newEtat.getCode(), collaborateurConnecte, dep, pCommentaire);

                msgRetour = MOVEMENT_CANCELLED_WITH_SUCCESS;

                break;
            default:
                msgRetour = "";
        }

        session.flush();
        transaction.commit();
        // --- Fin de la session et commit des changements
        return msgRetour;
    }

    /**
     * {@linkplain DemandeDeplacementService#getFileDeplacementbyId(Long)}
     */
    @Override
    public File getFileDeplacementbyId(Long pId) {
        // --- Récupération du deplacement
        DemandeDeplacement lDeplacement = this.get(pId);
        if (null == lDeplacement) {
            logger.warn("[get travel file] no travel exist for this id : {}", pId);
        }
        String[] paramsNomFichier = {
                Long.toString(pId),
                lDeplacement.getCollaborator().getNom(),
        };
        // --- Récupération du fichier PDF
        String nomFichier = properties.get("pdf.nomFichierDeplacement", paramsNomFichier);
        File tmpFile = null;
        try {
            tmpFile = new File(Parametrage.getContext("dossierPDFDEPLACEMENT") + nomFichier);
        } catch (NamingException e) {
            logger.error("[get travel file] context parameter naming error", e);
        }

        // --- Vérification de l'existance du fichier
        if (!tmpFile.exists() && !fileService.fileWasZipped(tmpFile)) {
            return null;
        }
        return tmpFile;
    }

    /**
     * {@linkplain DemandeDeplacementService#getAllDemandeDeplacementEtatSO()}
     */
    @Override
    public List<DemandeDeplacementAsJson> getAllDemandeDeplacementEtatSO() {

        Etat lEtatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);

        List<DemandeDeplacement> listeDepl = demandeDeplacementDAO.findListEntitesByProp(Absence.PROP_ETAT, lEtatSO);

        List<DemandeDeplacementAsJson> listDeplAsJson = new ArrayList<>();
        for (int i = 0; i < listeDepl.size(); i++) {
            listDeplAsJson.add(listeDepl.get(i).toJson());
        }

        return listDeplAsJson;
    }


}
