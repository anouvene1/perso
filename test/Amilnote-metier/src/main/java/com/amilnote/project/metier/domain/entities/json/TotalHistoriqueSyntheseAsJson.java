/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import java.math.BigDecimal;


/**
 * The type Total historique synthese as json.
 */
public class TotalHistoriqueSyntheseAsJson {

    private int annee;

    private int mois;

    private long idTypeFrais;

    private BigDecimal montant;

    /**
     * Instantiates a new Total historique synthese as json.
     *
     * @param pIdTypeFrais the p id type frais
     * @param pMois        the p mois
     * @param pAnnee       the p annee
     * @param pMontant     the p montant
     */
    public TotalHistoriqueSyntheseAsJson(int pIdTypeFrais, int pMois, int pAnnee, BigDecimal pMontant) {
        super();
        annee = pAnnee;
        mois = pMois;
        idTypeFrais = pIdTypeFrais;
        montant = pMontant;
    }

    /**
     * Instantiates a new Total historique synthese as json.
     *
     * @param pHistoFrais the p histo frais
     */
    public TotalHistoriqueSyntheseAsJson(Object[] pHistoFrais) {
        this.setAnnee((int) pHistoFrais[0]);
        this.setMois((int) pHistoFrais[1]);
        this.setIdTypeFrais((long) pHistoFrais[2]);
        this.setMontant((BigDecimal) pHistoFrais[3]);
    }


    /**
     * Gets mois.
     *
     * @return the mois
     */
    public int getMois() {
        return mois;
    }

    /**
     * Sets mois.
     *
     * @param pMois the p mois
     */
    public void setMois(int pMois) {
        mois = pMois;
    }

    /**
     * Gets annee.
     *
     * @return the annee
     */
    public int getAnnee() {
        return annee;
    }


    /**
     * Sets annee.
     *
     * @param pAnnee the p annee
     */
    public void setAnnee(int pAnnee) {
        annee = pAnnee;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public BigDecimal getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the p montant
     */
    public void setMontant(BigDecimal pMontant) {
        montant = pMontant;
    }

    /**
     * Gets id type frais.
     *
     * @return the id type frais
     */
    public long getIdTypeFrais() {
        return idTypeFrais;
    }

    /**
     * Sets id type frais.
     *
     * @param pIdTypeFrais the p id type frais
     */
    public void setIdTypeFrais(long pIdTypeFrais) {
        idTypeFrais = pIdTypeFrais;
    }

}
