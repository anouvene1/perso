/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.enumerations.ClientExcelEnum;
import org.apache.poi.ss.usermodel.*;
import org.joda.time.DateTime;
import javax.naming.NamingException;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.Date;
import java.util.List;

/**
 * The interface DocumentService.
 */
public interface DocumentService {

    /**
     * Ajoute une nouvelle ligne de données à partir de la feuille et les données passée
     *
     * @param nbJoursParMission Nombre de jours par mission
     * @param tjm               TJM
     * @param rowNumber         Numéro de ligne
     * @param mySheet           Feuille excel
     * @param fullName          Nom et prénom du collaborateur
     * @param missionName       Nom de la mission
     * @param client            Nom du client
     * @return le numéro de la ligne
     */
    int createLine(double nbJoursParMission, Float tjm, int rowNumber, Sheet mySheet,
                           String fullName, String missionName, String client);

    /**
     * Creer une ligne par mission
     * @param collaborator Collaborateur
     * @param year          Année de la mission
     * @param rowNumber     Numéro de ligne
     * @param mySheet       Page
     * @return              Le numéro de ligne
     */
    int createLinesByMissions(Collaborator collaborator, int year, int rowNumber, Sheet mySheet);

    /**
     * Creer une ligne par Rapport d'activité
     * @param collaborator Collaborateur
     * @param month         Mois de la mission
     * @param year          Année de la mission
     * @param rowNumber     Numéro de ligne
     * @param mySheet       page
     * @return              Le numéro de ligne
     */
    int createLinesByRA(Collaborator collaborator, int month, int year, int rowNumber, Sheet mySheet);

    /**
     * Créer une ligne par jour pour facturation
     * @param collaborator Collaborateur
     * @param mission       Mission
     * @param month         Mois de la mission
     * @param year          Année de la mission
     * @param rowNumber     Numéro de la ligne
     * @param mySheet       Page
     * @return              Le numéro de ligne
     */
    int createLineByDaysToInvoice(Collaborator collaborator, Mission mission, int month, int year, int rowNumber, Sheet mySheet);

    /**
     * Met à jour l'affichage de l'absence spécifiée à partir des dates
     * de début et de fin spécifiées
     *
     * @param abs            l'absence
     * @param absenceDateDeb la date de début
     * @param absenceDateFin la date de fin
     * @return l'absence avec la mise à jour des dates
     * <p>
     * SPE_27/01/17_AMNOTE_205: Création de la fonction
     */
    String updateAbsenceDates(String abs, String absenceDateDeb, String absenceDateFin);

    /**
     *Methode qui renvoie la liste des frais des collab
     * @param dateDebut start date
     * @param dateFin end date
     * @param typeDetail detail type
     * @return a list
     */
    List<Object[]>generateListeFrais( Date dateDebut,Date dateFin, String typeDetail);

    /**
     * create one or many rows for the excel file
     * @param choixExcel is the type of excel needed
     * @param collaboratorAsJson the collaborator that is checked
     * @param myRow the row
     * @param mySheet the sheet where it is writter
     * @param index the number of the line
     * @return the index to know in which line was written the last information
     */
    int createClientsFileRows(ClientExcelEnum choixExcel, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index);

    /**
     * prepare the style and first lines of a client excel file
     * @param choixExcel is the type of the excel Client file needed
     * @param font is the font
     * @param mySheet is the sheet where the cells are created
     * @param myRow is the row
     * @param wb is the workbook
     * @param style is the style
     */
    void prepareClientFile(ClientExcelEnum choixExcel, Font font, Sheet mySheet, Row myRow, Workbook wb, CellStyle style);

    /**
     * Créer un Collaborateur sur une cellule de mission client
     * @param collaborator the affected collaborator
     * @param collaboratorAsJson the affected collaborator as json
     * @param myRow the row
     * @param mySheet the sheet
     * @param index index
     * @return row number
     */
    int createCollaboratorsOnClientMissionsCells(Collaborator collaborator, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index);

    /**
     * create a cell for each Client where the Collaborator worked this month
     * @param collaborator the affected collaborator
     * @param collaboratorAsJson the affected collaborator as json
     * @param myRow the row
     * @param mySheet the sheet
     * @param index index
     * @return the index to know where the next line must be writter next
     */
    int createClientsPerCollaboratorCells(Collaborator collaborator, CollaboratorAsJson collaboratorAsJson, Row myRow, Sheet mySheet, int index);

    /**
     * Fonction pour "autoSizer" toutes les colonnes de toutes les sheets d'un workbook
     * Excel gère les colonnes 'gelées' différemment des autres, d'où l'utilisation du
     * 'numRowsTodo' pour aller sizer les 3 premiers rows.
     * @param workbook le workbook (HSSF ou XSSF)
     */
    void autoSizeColumns(@NotNull Workbook workbook);

    /**
     * creation du chemin pour fichier
     * @param date the current date
     * @param cheminMonthODF month path ODF
     * @param numODF ODF number
     * @param typeFacture facture type
     * @throws NamingException name exception
     * @return the file path
     */
    String getCheminFichier(DateTime date,String cheminMonthODF, String numODF, TypeFacture.InvoiceType typeFacture)
            throws NamingException;

    /**
     * Trie la liste de facture
     * @param listeFactures List de facture
     */
    void sortListFactures(List<Facture> listeFactures);

    /**
     * Crée récursivement les dossiers parents d'un fichier dont le chemin est passé en paramètre
     *
     * @param cheminFichier Le chemin du fichier pour lequel on veut créer les dossiers parents
     * @throws FileSystemException L'exception levée si la création récursive échoue
     */
    void creerDossiersPourFichier(String cheminFichier) throws IOException;

    /**
     * Méthode qui calcule le montant total de frais correspondant à une facture
     *
     * @param facture la facture
     * @return le montant total des frais
     */
    float calculFraisTJMElementFacture(Facture facture);

    /**
     * Crée le titre du fichier d'export excel du tableau des commandes
     * @param arrayName array of names
     * @param monthNames array of months
     * @author alosat
     * @see StringBuilder
     * @return title.toString()
     */
    String createTitle(String[] arrayName, String[] monthNames);

    /**
     * Renvoie un booléen si ce sont des commandes en cours ou non
     * @param arrayName array of name
     * @author alosat
     * @return true || false
     */
    boolean isActive(String[] arrayName);

    /**
     * Renvoie une paire d'objet nécessaire à la création du titre
     * @param arrayName array of name
     * @param months month
     * @author alosat
     * @see Pair
     * @return new {@link Pair}
     */
    Pair<Boolean, String> checkMonth(String[] arrayName, String[] months);
}
