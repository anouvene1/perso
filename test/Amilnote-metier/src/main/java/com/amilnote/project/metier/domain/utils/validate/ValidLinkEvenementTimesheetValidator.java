/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.validate;

import com.amilnote.project.metier.domain.dao.LinkEvenementTimesheetDAO;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

/**
 * The type Valid link evenement timesheet validator.
 */
public class ValidLinkEvenementTimesheetValidator implements ConstraintValidator<ValidLinkEvenementTimesheet, LinkEvenementTimesheet> {

    @Autowired
    private LinkEvenementTimesheetDAO linkEvenementTimesheetDAO;

    /**
     * {@linkplain ConstraintValidator#initialize(Annotation)}
     */
    @Override
    public void initialize(ValidLinkEvenementTimesheet constraintAnnotation) {

    }

    /**
     * {@linkplain ConstraintValidator#isValid(Object, ConstraintValidatorContext)}
     */
    @Override
    public boolean isValid(LinkEvenementTimesheet event, ConstraintValidatorContext context) {
        if (null != event.getMission()) {

            DateTime dateDebut = new DateTime(event.getMission().getDateDebut());
            DateTime dateFin = new DateTime(event.getMission().getDateFin());
            DateTime eventDate = new DateTime(event.getDate());

            if (eventDate.isAfter(dateDebut) && eventDate.isBefore(dateFin)) {
                return true;
            }
        }
        return false;
    }

}
