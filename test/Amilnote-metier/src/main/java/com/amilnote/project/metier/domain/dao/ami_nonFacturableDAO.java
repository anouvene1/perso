package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.ami_nonFacturable;

import java.util.Date;
import java.util.List;

/**
 * The interface ami_nonFacturable dao.
 */
public interface ami_nonFacturableDAO extends LongKeyDAO<ami_nonFacturable>  {

    List<ami_nonFacturable> findListEntitesByDateBetweenDates(String pProp, Date pDateDebutMois, Date pDateFinMois);
}
