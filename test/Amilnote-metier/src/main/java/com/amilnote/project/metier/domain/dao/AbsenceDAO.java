/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.TypeAbsence;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;

import java.util.Date;
import java.util.List;

/**
 * The interface Absence dao.
 */
public interface AbsenceDAO extends LongKeyDAO<Absence> {

    /**
     * Retourne tous les Types d'absence
     *
     * @return all type absence
     */
    List<TypeAbsence> getAllTypeAbsence();

    /**
     * Retorune un typeAbsence en fonction de l'id
     *
     * @param id the id
     * @return type absence by id
     */
    TypeAbsence getTypeAbsenceById(long id);

    /**
     * Retourne toutes les absences à l'état brouillon pour un collaborateur
     *
     * @param collaborator the p collaborateur
     * @param pEtat          the p etat
     * @return absences by collaborateur and etat
     */
    List<Absence> getAbsencesByCollaboratorAndEtat(Collaborator collaborator, Etat pEtat);

    /**
     * Retourne toutes les absences triées par date
     *
     * @return all absences order by date
     */
    List<Absence> getAbsencesOrderByDate();

    /**
     * Returns a list of absences by state, month and year ordered by starting date
     *
     * @param state {@link Etat}
     * @param month {@link Integer}
     * @param year  {@link Integer}
     * @return List {@link Absence}
     */
    List<Absence> findAbsencesByStateMonthAndYearOrderByDate(Etat state, Integer month, Integer year);

    /**
     * Nombre d'absences total en attente de validation manager
     *
     * @return the nb absence attente val
     */
    int getNbAbsenceAttenteVal();

    /**
     * Nombre d'absences d'un collborateur en attente de validation manager
     *
     * @param collaborator the p collaborateur
     * @return the nb absence attente val
     */
    int getNbAbsenceAttenteVal(Collaborator collaborator);

    /**
     * Date dernière absence soumise
     *
     * @param collaborator the p collaborateur
     * @return the date dernier abs soumis
     */
    Date getDateDernierAbsSoumis(Collaborator collaborator);

    /**
     * Update absences from br to so string.
     *
     * @param pLListAbsenceAsJson the p l list absence as json
     * @return the string
     */
    String updateAbsencesFromBRToSO(List<AbsenceAsJson> pLListAbsenceAsJson, String comment);

    /**
     * Nombre d'absences par etat et collaborateur
     *
     * @param collaborator the p collaborateur
     * @param pEtat          the p etat
     * @return the nb absence by etat and collaborateur
     */
    int getNbAbsenceByEtatAndCollaborator(Collaborator collaborator, Etat pEtat);

    /**
     * Retourne la liste des absences du collaborateur après la date.
     *
     * @param collaborator the p collaborateur
     * @param pDateDeb       the p date deb
     * @param pDateFin       the p date fin
     * @return by collaborateur between date
     */
    List<Absence> getByCollaboratorBetweenDate(Collaborator collaborator, Date pDateDeb, Date pDateFin);

    /**
     * get absences between two date
     *
     * @param pDateDeb date debut de la periode
     * @param pDateFin date fin de la periode
     * @return liste d'absences soumises pour la période choisie
     */
    List<Absence> getAbsencesSoumisesBetweenDate(Date pDateDeb, Date pDateFin);

    /**
     * return list of accepted absences
     *
     * @param pDateDeb date debut de la periode
     * @param pDateFin date fin de la periode
     * @return liste d'absences validees pour la période choisie
     */
    List<Absence> getAbsencesValideesBetweenDate(Date pDateDeb, Date pDateFin);

    /**
     * supprime une absence
     *
     * @param pAbsence absence à supprimer
     * @return le message de retour
     */
    String deleteAbsence(Absence pAbsence);

    /**
     * Met à jour l'etat de l'absence
     *
     * @param absence absence à mettre à jour
     * @param etat    nouvel etat pour l'absence
     * @return id de l absence
     */
    Long updateEtatAbsence(Absence absence, Etat etat);

    /**
     * Met à jour le commentaire
     *
     * @param absence     absence à mettre à jour
     * @param commentaire nouvel commentaire pour l'absence
     * @return id de l'absence
     */
    Long updateCommentaireAbsence(Absence absence, String commentaire);

    /**
     * Returns a list of Absences with their Type from a list of ids
     *
     * @param ids a list of {@link Long}
     * @return list of {@link Absence}
     */
    List<Absence> findByIds(List<Long> ids);
}
