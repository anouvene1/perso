/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;

import org.apache.commons.lang.StringUtils;

import java.util.Collection;

/**
 * The type Champ.
 */
public class Champ {
    private Table proprietaire;
    private String nom;
    private Object value; //de type string ou collection

    /**
     * Création d'un champ de type champ
     *
     * @param cible Table référant le champ
     * @param nom   Nom du champ
     */
    public Champ(Table cible, String nom) {
        if (cible != null && nom != null) {
            this.proprietaire = cible;
            this.nom = nom;
            this.value = null;
        } else {
            throw new IllegalArgumentException("la table cible et le nom du champ ne peuvent être null!");
        }
    }

    /**
     * Création d'un champ de type valeur
     *
     * @param value valorisation du champ
     */
    public Champ(Object value) {
        this.proprietaire = null;
        this.nom = null;
        this.value = value;
    }

    /**
     * retourne le valorisation du champ, soit le nom du champ soit une value
     *
     * @return Chaine de caractère représentatnt la valorisation du champ
     */
    public String getChamp() {
        if (this.value == null) {
            return getNom();
        } else {
            return getValue();
        }
    }

    private String getNom() {

        String champ = "";
        if (!StringUtils.isEmpty(proprietaire.getAlias())) {
            champ = proprietaire.getAlias() + ".";
        }
        champ = champ + this.nom;

        return champ;
    }

    private String getValue() {
        String value = "";

        if (this.value != null) {
            if (Collection.class.isInstance(this.value)) {
                value = "(";
                for (Object element : (Collection) this.value) {
                    if (element.getClass().equals(String.class)) {
                        value = value + "'" + element.toString() + "',";
                    } else {
                        value = value.concat(element.toString()) + ",";
                    }
                }
                value = value.substring(0, value.length() - 1) + ")";
            } else {
                if (this.value.getClass().equals(String.class)) {
                    value = "'" + this.value.toString() + "'";
                } else {
                    value = this.value.toString();
                }
            }
        }

        return value;
    }
}
