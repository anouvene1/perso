/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.Mission;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * The interface Link evenement timesheet service.
 */
public interface LinkEvenementTimesheetService extends AbstractService<LinkEvenementTimesheet> {
    /**
     * Renvoie l'ensemble des linkEvenementTimesheet pour un collaborateur ET dont la date de fin et supérieur au premier du mois en cours. le tout en Json
     *
     * @param collaborator the p collaborateur
     * @param pStart         the p start
     * @param pEnd           the p end
     * @return string string
     * @throws JsonProcessingException the json processing exception
     */
    String findEventsBetweenDatesAsJson(Collaborator collaborator, String pStart, String pEnd) throws JsonProcessingException;

    /**
     * Renvoie l'ensemble des linkEvenementTimesheet pour un collaborateur ET dont la date de fin et supérieur au premier du mois en cours.
     *
     * @param collaborator the p collaborateur
     * @param pStart         the p start
     * @param pEnd           the p end
     * @return list list
     * @throws JsonProcessingException the json processing exception
     */
    List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, String pStart, String pEnd) throws JsonProcessingException;


    public List<LinkEvenementTimesheet> findEventsBetweenDatesByDateTime(Collaborator collaborator, DateTime pStart, DateTime pEnd) ;
    /**
     * Renvoi la liste des linkEvenementTimesheet entre deux date (pStart et pEnd)
     *
     * @param collaborator the p collaborateur
     * @param pStart         the p start
     * @param pEnd           the p end
     * @return list list
     * @throws JsonProcessingException the json processing exception
     */
    List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, Date pStart, Date pEnd) throws JsonProcessingException;


    /**
     * Renvoie l'ensemble des linkEvenementTimesheet pour un collaborateur ET dont la date de fin et supérieur au premier du mois en cours. Seulement l'id et la date
     *
     * @param pMission the p mission
     * @return string string
     * @throws JsonProcessingException the json processing exception
     */
    String findAllNotFinishEventOnlyDate(Mission pMission) throws JsonProcessingException;

    /**
     * retrouver un LinkEvenementTimesheet pour un id
     *
     * @param pIdLinkEvtTs the p id link evt ts
     * @return LinkEvenementTimesheet link evenement timesheet
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    LinkEvenementTimesheet findById(Long pIdLinkEvtTs);

    /**
     * Renvoie la liste des évènements pour une absence donnée
     *
     * @param pAbsence the p absence
     * @return list list
     */
    List<LinkEvenementTimesheet> findByAbsence(Absence pAbsence);

    /**
     * Update les evenements absences
     *
     * @param pListEvents    the p list events
     * @param collaborator the p collaborateur
     * @return Résultat de l'update
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    String updateAbsenceOnLinkEvenementTimesheet(String pListEvents, Collaborator collaborator) throws JsonProcessingException, IOException;

    /**
     * Update les evenements missions
     *
     * @param pListEvents    the p list events
     * @param collaborator the p collaborateur
     * @return Résultat de l'update
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    String updateMissionOnLinkEvenementTimesheet(String pListEvents, Collaborator collaborator) throws JsonProcessingException, IOException;

    /**
     * Supprime la liste d'evenements
     *
     * @param pListEvents    the p list events
     * @param collaborator the p collaborateur
     * @return Résultat de l'update
     */
    String delLinkEvenementTimesheet(String pListEvents, Collaborator collaborator);


    /**
     * retourne ts les LinkEvenementTimesheet pour la mission passée en paramètre
     *
     * @param pMission the p mission
     * @return list list
     */
    List<LinkEvenementTimesheet> findAllByMission(Mission pMission);

    List<LinkEvenementTimesheet> findAllByMissionWithoutDateRestriction(Mission pMission);

    boolean isRaValideWithMission(Mission pMission, Long idColl);

    /**
     * Récupère la liste des jours fériés et parse en Json
     *
     * @param pStart the p start
     * @param pEnd   the p end
     * @return String listHolidays
     */
    String getHolidaysJson(String pStart, String pEnd);

    /**
     * Renvoie le nombre de jours pour une mission dans un interval de temps
     *
     * @param pMission      the p mission
     * @param pDebutPeriode the p debut periode
     * @param pFinPeriode   the p fin periode
     * @return float float
     */
    float findNbDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * Renvoie la liste des jours pour une mission dans un interval de temps
     *
     * @param pMission      the p mission
     * @param pDebutPeriode the p debut periode
     * @param pFinPeriode   the p fin periode
     * @return float list
     */
    List<LinkEvenementTimesheet> findDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * Retourne le nombre de samedi de linkEvenementTimesheet suprérieur à (now() - histoMaxTimesheet)  (class Parametrage)
     * * @param pCollaborator
     *
     * @param collaborator the p collaborateur
     * @return int count saturdays since histo max timesheet
     */
    int getCountSaturdaysSinceHistoMaxTimesheet(Collaborator collaborator);

    /**
     * Retourne le nombre de matinés pour la mission et le mois courant
     * * @param pMission
     *
     * @param pMission the p mission
     * @param lSart    the l sart
     * @param lEnd     the l end
     * @return int list
     */
    List<LinkEvenementTimesheet> findNbWorkedDaysByMissionAndPeriod(Mission pMission, DateTime lSart, DateTime lEnd);

    /**
     * Find nb days for forfait deplacement float.
     *
     * @param pMission      the p mission
     * @param pDebutPeriode the p debut periode
     * @param pFinPeriode   the p fin periode
     * @return the float
     */
    float findNbDaysForForfaitDeplacement(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * Recupere toutes les linkEvenementTimesheet avec l'id de l'absence
     *
     * @param pAbsenceId the p absence id
     * @return list event with absence id as json
     * @throws Exception the exception
     */
    String getListEventWithAbsenceIdAsJson(long pAbsenceId) throws Exception;

    /**
     * Gets map jour mission.
     *
     * @param listEvent the list event
     * @return the map jour mission
     * @throws JsonProcessingException the json processing exception
     */
    LinkedHashMap<Mission, Double> getMapJourMission(List<LinkEvenementTimesheet> listEvent) throws JsonProcessingException;

    /**
     * Gets holidays.
     *
     * @param year year of search
     * @return a list of {@link Date} of all holidays of a given year
     */
    List<Date> getHolidays(int year) ;

}
