/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.LinkTypeFraisCarac;

import java.util.List;

/**
 * The interface Link type frais carac service.
 */
public interface LinkTypeFraisCaracService extends AbstractService<LinkTypeFraisCarac> {

    /**
     * Find all list.
     *
     * @return the list
     */
    List<LinkTypeFraisCarac> findAll();

    /**
     * retrouver un LinkTypeFraisCarac
     *
     * @param pIdLinkTFraisCarac the p id link t frais carac
     * @return LinkTypeFraisCarac link type frais carac
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    LinkTypeFraisCarac findById(Long pIdLinkTFraisCarac);

    /**
     * Find all as json string.
     *
     * @return the string
     */
    String findAllAsJson();

}
