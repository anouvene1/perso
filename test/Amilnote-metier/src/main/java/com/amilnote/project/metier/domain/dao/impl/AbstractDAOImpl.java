/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.LongKeyDAO;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import java.util.*;

/**
 * The type Abstract dao.
 *
 * @param <T> the type parameter
 */
public abstract class AbstractDAOImpl<T> extends HibernateDaoSupport implements LongKeyDAO<T> {

    /**
     * Gets reference class.
     *
     * @return the reference class
     */
    protected abstract Class<T> getReferenceClass();

    /**
     * @return List
     */
    public List<T> loadAll() {
        Criteria criteria = currentSession().createCriteria(getReferenceClass());
        return criteria.list();
    }

    /**
     * Get un element en fonction de l'id renvoie null si l'objet n'existe pas
     *
     * @param id the id
     * @return un element de la classe T
     */
    public T get(Long id) {
        return getHibernateTemplate().get(getReferenceClass(), id);
    }

    /**
     * Get un element en fonction de l'id lève une objectNotFoundException si l'objet n'existe pas
     *
     * @param id the id
     * @return un element de la classe T
     */
    public T load(Long id) {
        return getHibernateTemplate().load(getReferenceClass(), id);
    }

    /**
     * Get tous les éléments de la classe T
     *
     * @param defaultOrder the default order
     * @return List
     */
    public List<T> findAll(Order defaultOrder) {
        Criteria crit = currentSession().createCriteria(getReferenceClass());
        if (null != defaultOrder) {
            crit.addOrder(defaultOrder);
        }
        return crit.list();
    }

    /**
     * Get un élément de la classe en fonction de la prépriété passé en paramètre
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return un element de la classe T
     */
    public T findUniqEntiteByProp(String propName, Object propValue) {
        Criteria criteria = currentSession().createCriteria(getReferenceClass());
        criteria.add(Restrictions.eq(propName, propValue));

        return (T) criteria.uniqueResult();
    }

    /**
     * Get tous les éléments de la classe en fonction de la prépriété passé en paramètre
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return List
     */
    public List<T> findListEntitesByProp(String propName, Object propValue) {
        return findListEntitesByProp(propName, propValue, null);
    }

    /**
     * Get tous les éléments de la classe en fonction de la prépriété passé en paramètre rengé par ordre croissant
     * sur la propriété passé en paramètre
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @param propOrder the prop order
     * @return List
     */
    public List<T> findListEntitesByProp(String propName, Object propValue, String propOrder) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(propName, propValue));
        if (propOrder != null) {
            criteria.addOrder(Order.asc(propOrder));
        }
        return criteria.list();
    }

    /**
     * retrieve all database entries corresponding to one or multiple {@link SearchCriteria} if not null, and order by ordering property ascending or descending according to isAscendingOrdering boolean
     * @param orderingProperty the resulting entries list will by ordered by this property, default ordering if null
     * @param isAscendingOrdering if true the resluting list will be ordered by orderingProperty ascending, else (false or null) descending
     * @param searchCriterias varargs of {@link SearchCriteria} which will restrict the search
     * @return a list of all database entries corresponding to the searchCriterias, if null retrieve all entries
     */
    @Override
    public List<T> findBySearchCriterias(String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) {
        Criteria criteria = getCriteria();

        // Restriction sur un ou plusieurs search criterias
        if (searchCriterias != null) {
            for (SearchCriteria searchCriteria : searchCriterias) {
                if (searchCriteria != null && searchCriteria.getValues().length > 0) {
                    addSearchCriteriaRestrictions(criteria, searchCriteria);
                }
            }
        }

        // Ordonner la liste des résultats
        addOrder(criteria, orderingProperty, isAscendingOrdering);

        return criteria.list();
    }

    /**
     * Add SQL Restrictions to a given Hibernate Criteria according to a given {@link SearchCriteria}
     * @param criteria the restrictions will be added to this criteria
     * @param searchCriteria the restrictions will be based on this {@link SearchCriteria}
     */
    protected void addSearchCriteriaRestrictions(Criteria criteria, SearchCriteria searchCriteria) {
        switch (searchCriteria.getComparator()){
            case EQUAL:
                criteria.add(Restrictions.eq(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case NOT_EQUAL:
                criteria.add(Restrictions.ne(searchCriteria.getProperty(), searchCriteria.getValues()[0]));
                break;
            case GREATER_THAN:
                criteria.add(Restrictions.gt(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case GREATER_THAN_OR_EQUAL:
                criteria.add(Restrictions.ge(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case LESS_THAN:
                criteria.add(Restrictions.lt(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case LESS_THAN_OR_EQUAL:
                criteria.add(Restrictions.le(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case IN:
                criteria.add(Restrictions.in(searchCriteria.getProperty(), searchCriteria.getValues())) ;
                break;
            case NOT_IN:
                criteria.add(Restrictions.not(Restrictions.in(searchCriteria.getProperty(), searchCriteria.getValues())));
                break;
            case BETWEEN:
                criteria.add(Restrictions.between(searchCriteria.getProperty(), searchCriteria.getValues()[0], searchCriteria.getValues()[1])) ;
                break;
            case IS_NULL:
                criteria.add(Restrictions.isNull(searchCriteria.getProperty())) ;
                break;
            case IS_NOT_NULL:
                criteria.add(Restrictions.isNotNull(searchCriteria.getProperty())) ;
                break;
            case LIKE:
                criteria.add(Restrictions.like(searchCriteria.getProperty(), searchCriteria.getValues()[0])) ;
                break;
            case EXISTS:
                break;
            default:
                break;
        }
    }

    /**
     * Add order to criteria depending on a given ordering property
     * @param criteria this is the criteria that will be ordered
     * @param orderingProperty the ordering will be based on this property
     * @param isAscendingOrdering is ordering ascending or descending
     */
    protected void addOrder(Criteria criteria, String orderingProperty, Boolean isAscendingOrdering) {
        if (orderingProperty != null) {
            if (isAscendingOrdering != null && !isAscendingOrdering) {
                criteria.addOrder(Order.desc(orderingProperty));
            } else {
                criteria.addOrder(Order.asc(orderingProperty));
            }
        }
    }

    /**
     * Ajoute ou mets à jour l'élément passé en paramètre
     *
     * @param t the t
     */
    public void saveOrUpdate(T t) {
        getHibernateTemplate().saveOrUpdate(t);
    }

    /**
     * Ajoute l'élément passé en paramètre
     *
     * @param t the t
     */
    public Long save(T t) {
        return (Long) getHibernateTemplate().save(t);
    }

    /**
     * Mets à jour l'élément passé en paramètre
     *
     * @param t the t
     */
    public T merge(T t) {
        return getHibernateTemplate().merge(t);
    }

    /**
     * Supprime l'élément passé en paramètre
     *
     * @param t the t
     */
    public void delete(T t) {
        getHibernateTemplate().delete(t);
    }

    /**
     * Retourne la Criteria en cours d'utilisation
     *
     * @return Criteria criteria
     */
    public Criteria getCriteria() {
        return currentSession().createCriteria(getReferenceClass(), getReferenceClass().getName());
    }

    /**
     * Renvoie une Query en fonction de la requète passé en paramètre sous forme de String
     *
     * @param query the query
     * @return query hql query
     */
    public Query getHQLQuery(String query) {
        return currentSession().createQuery(query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Session getCurrentSession() {
        return currentSession();
    }
}
