/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.TypeMission;
import com.amilnote.project.metier.domain.entities.json.TypeMissionAsJson;

import java.util.List;

/**
 * The interface Type mission service.
 */
public interface TypeMissionService extends AbstractService<TypeMission> {

    /**
     * Retourne tous les Types de mission
     *
     * @return all type mission
     */
    List<TypeMission> getAllTypeMission();

    /**
     * Retourne un type de Mission en fonction de l'id
     *
     * @param id the id to search
     * @return TypeFrais type mission by id
     */
    TypeMission getTypeMissionById(long id);

    /**
     * Retourne tous les type de frais au format json
     *
     * @return String (Liste de TypeMissionAsJson)
     */
    List<TypeMissionAsJson> getAllTypeMissionAsJson();

    /**
     * Gets type mission by name.
     *
     * @param nameMission the p name mission
     * @return the type mission by name
     */
    TypeMission getTypeMissionByName(String nameMission);

    /**
     * Gets type mission by code.
     *
     * @param codeMission the p code mission
     * @return the type mission by name
     */
    TypeMission getTypeMissionByCode(String codeMission);
}
