/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import java.util.List;

/**
 * The interface Long key dao.
 *
 * @param <T> the type parameter
 */
public interface LongKeyDAO<T> {
    /**
     * Load all list.
     *
     * @return the list
     */
    List<T> loadAll();

    /**
     * Get t.
     *
     * @param id the id
     * @return the t
     */
    T get(Long id);

    /**
     * Load t.
     *
     * @param id the id
     * @return the t
     */
    T load(Long id);

    /**
     * Delete.
     *
     * @param t the t
     */
    void delete(T t);

    /**
     * Find all list.
     *
     * @param defaultOrder the default order
     * @return the list
     */
    List<T> findAll(Order defaultOrder);

    /**
     * Find uniq entite by prop t.
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return the t
     */
    T findUniqEntiteByProp(String propName, Object propValue);

    /**
     * Save or update.
     *
     * @param t the t
     */
    void saveOrUpdate(T t);

    /**
     * Save long.
     *
     * @param t the t
     * @return the long
     */
    Long save(T t);

    /**
     * Find list entites by prop list.
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @param propOrder the prop order
     * @return the list
     */
    List<T> findListEntitesByProp(String propName, Object propValue,
                                  String propOrder);

    /**
     * Find list entites by prop list.
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return the list
     */
    List<T> findListEntitesByProp(String propName, Object propValue);

    /**
     * Merge t.
     *
     * @param t the t
     * @return the t
     */
    T merge(T t);

    /**
     *
     * @return current hibernate session
     */
    Session getCurrentSession();

    /**
     * retrieve all database entries corresponding to one or multiple {@link SearchCriteria} if not null, and order by ordering property ascending or descending according to isAscendingOrdering boolean
     * @param orderingProperty the resulting entries list will by ordered by this property, default ordering if null
     * @param isAscendingOrdering if true the resluting list will be ordered by orderingProperty ascending, else (false or null) descending
     * @param searchCriterias varargs of {@link SearchCriteria} which will restrict the search
     * @return a list of all database entries corresponding to the searchCriterias, if null retrieve all entries
     */
    List <T> findBySearchCriterias(String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) ;

}

