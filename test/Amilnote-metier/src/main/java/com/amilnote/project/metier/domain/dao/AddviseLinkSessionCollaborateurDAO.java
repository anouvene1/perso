/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.AddviseLinkSessionCollaborateur;

import java.util.List;

public interface AddviseLinkSessionCollaborateurDAO extends LongKeyDAO<AddviseLinkSessionCollaborateur> {

    /**
     * Permet la sauvegarde d'un AddviseLinkSessionCollaborateur nouveau ou existant
     *
     * @param addviseLinkSessionCollaborateur AddviseLinkSessionCollaborateur
     * @return 0
     */
    int createOrUpdateAddviseLinkSessionCollaborateur(AddviseLinkSessionCollaborateur addviseLinkSessionCollaborateur);

    /**
     * retourne un AddviseLinkSessionCollaborateur à partir de son id
     *
     * @param id Long
     * @return AddviseLinkSessionCollaborateur
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    AddviseLinkSessionCollaborateur findById(int id);

    /**
     * Retourne les états d'invitation en fonction des id collaborateurs et sessions
     *
     * @param idsCollabSession tableau d'id collaborateurs et sessions (l'un après l'autre)
     * @return List de AddviseLinkSessionCollaborateur
     */
    List<AddviseLinkSessionCollaborateur>
    findAllByIdCollaboratorAndIdSession(String[] idsCollabSession);

    /**
     * Retourne un AddviseLinkSessionCollaborateur étant donné un id collaborateur et un id session
     *
     * @param idCollaborateur id collaborateur
     * @param idSession id session
     * @return AddviseLinkSessionCollaborateur
     */
    AddviseLinkSessionCollaborateur findByIdCollaboratorAndIdSession(long idCollaborateur, int idSession);

}
