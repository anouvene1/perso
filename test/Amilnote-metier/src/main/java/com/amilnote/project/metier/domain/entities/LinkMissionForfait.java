/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Link mission forfait.
 */
@Entity
@Table(name = "ami_link_mission_forfait")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LinkMissionForfait {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";
    /**
     * The constant PROP_FORFAIT.
     */
    public static final String PROP_FORFAIT = "forfait";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    /**
     * The constant PROP_LIBELLE.
     */
    public static final String PROP_LIBELLE = "libelle";


    private Long id;
    private Mission mission;
    private Forfait forfait;
    private float montant;
    private String libelle;


    /**
     * Constructeur par defaut de la classe LinkMissionForfait
     */
    public LinkMissionForfait() {
    }

    /**
     * Instantiates a new Link mission forfait.
     *
     * @param pMission the p mission
     * @param pForfait the p forfait
     * @param pMontant the p montant
     * @param pLibelle libelle
     */
    public LinkMissionForfait(Mission pMission, Forfait pForfait, float pMontant, String pLibelle) {
        super();
        mission = pMission;
        forfait = pForfait;
        montant = pMontant;
        libelle = pLibelle;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    @ManyToOne
    @JoinColumn(name = "id_mission")
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(Mission pMission) {
        mission = pMission;
    }

    /**
     * Gets forfait.
     *
     * @return the forfait
     */
    @ManyToOne
    @JoinColumn(name = "id_forfait")
    public Forfait getForfait() {
        return forfait;
    }

    /**
     * Sets forfait.
     *
     * @param pForfait the p forfait
     */
    public void setForfait(Forfait pForfait) {
        forfait = pForfait;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "montant", nullable = false)
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the p montant
     */
    public void setMontant(float pMontant) {
        montant = pMontant;
    }

    /**
     * Gets libellé
     * @return libellé
     */
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle
     *
     * @param libelle the libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * To json link mission forfait as json.
     *
     * @return the link mission forfait as json
     */
    public LinkMissionForfaitAsJson toJson() {
        return new LinkMissionForfaitAsJson(this);
    }
}
