/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.ElementFacture;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;

import java.util.List;

/**
 * The interface Element facture dao.
 */
public interface ElementFactureDAO extends LongKeyDAO<ElementFacture> {

    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param pElementFactureAsJson the p element facture as json
     * @param pFacture the p facture
     * @return int int
     */
    int createOrUpdateElementFacture(ElementFactureAsJson pElementFactureAsJson, Facture pFacture);

    /**
     * Suppression de la commande
     *
     * @param pElementFacture the p element facture
     * @return string string
     */
    String deleteElementFacture(ElementFacture pElementFacture);

    /**
     * Find elementfacture list.
     *
     * @return La liste de tous les élement de facture
     */
    List<ElementFacture> findElementfacture();

}
