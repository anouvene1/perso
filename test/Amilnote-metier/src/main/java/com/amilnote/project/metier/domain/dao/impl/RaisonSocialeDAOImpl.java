package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.RaisonSocialeDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RaisonSociale;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("raisonSocialeDAO")
public class RaisonSocialeDAOImpl extends AbstractDAOImpl<RaisonSociale> implements RaisonSocialeDAO{


    @Override
    public RaisonSociale findByIdCollab(Long idCollab) {
        Criteria lCritCollaborator = getCriteria();
        Collaborator collabTemp = new Collaborator();
        collabTemp.setId(idCollab);
        lCritCollaborator.add(Restrictions.eq(RaisonSociale.PROP_ID_COLLABORATEUR, collabTemp));
        return (RaisonSociale) lCritCollaborator.uniqueResult();
    }

    @Override
    protected Class<RaisonSociale> getReferenceClass() {
        return RaisonSociale.class;
    }
}
