/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TypeFrais;

/**
 * The type Type frais as json.
 */
public class TypeFraisAsJson {
    private long id;
    private String typeFrais;
    private String code;
    private int etat;
    private String val_interne;
    private TVAAsJson tva;


    /**
     * Instantiates a new Type frais as json.
     */
    public TypeFraisAsJson() {

    }

    /**
     * Instantiates a new Type frais as json.
     *
     * @param pTypeFrais the p type frais
     */
    public TypeFraisAsJson(TypeFrais pTypeFrais) {
        id = pTypeFrais.getId();
        typeFrais = pTypeFrais.getTypeFrais();
        code = pTypeFrais.getCode();
        etat = pTypeFrais.getEtat();
        val_interne = pTypeFrais.getVal_interne();
        tva = (null != pTypeFrais.getTva()) ? pTypeFrais.getTva().toJson() : null;

    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(long pId) {
        id = pId;
    }

    /**
     * Gets type frais.
     *
     * @return the type frais
     */
    public String getTypeFrais() {
        return typeFrais;
    }

    /**
     * Sets type frais.
     *
     * @param pTypeFrais the p type frais
     */
    public void setTypeFrais(String pTypeFrais) {
        typeFrais = pTypeFrais;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        code = pCode;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public int getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(int pEtat) {
        etat = pEtat;
    }

    /**
     * Gets val interne.
     *
     * @return the val interne
     */
    public String getVal_interne() {
        return val_interne;
    }

    /**
     * Sets val interne.
     *
     * @param pVal_interne the p val interne
     */
    public void setVal_interne(String pVal_interne) {
        val_interne = pVal_interne;
    }

    /**
     * Gets tva.
     *
     * @return the tva
     */
    public TVAAsJson getTva() {
        return tva;
    }

    /**
     * Sets tva.
     *
     * @param pTva the tva to set
     */
    public void setTva(TVAAsJson pTva) {
        tva = pTva;
    }
}
