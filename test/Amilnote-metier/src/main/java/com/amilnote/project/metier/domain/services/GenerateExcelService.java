package com.amilnote.project.metier.domain.services;

import java.util.Date;
import java.util.List;

/**
 * The interface GenerateExcelService.
 */
public interface GenerateExcelService {

    /**
     * Allows to lay out the values of the annual collaborators' expenses summary
     *
     * @param list      the content of the file
     * @param yearFrais the year parameter
     * @return the content of the file after the formatting operation
     * @throws Exception the Exception
     */
    List<Object[]> finalizeTableListAnnee(List<List<Object[]>> list, int yearFrais) throws Exception;

    /**
     * Fonction de formatage et tri des données pour generation de fichier excel
     *
     * @param list  a {@link List}
     * @param pDate date
     * @return finalList une liste de ligne de frais et forfait pour chaque collaborateur
     * @throws Exception exception
     */
    List<Object[]> finalizeTableList(List<Object[]> list,Date pDate) throws Exception;

    /**
     * Fonction de formatage et tri des données pour generation de fichier excel pour mois detaillé
     *
     * @param list  a {@link List}
     * @param pDate date
     * @param byYear finalize for full year ?
     * @return a {@link List}
     * @throws Exception exception
     */
    List<Object[]> finalizeTableListExpensesDetail(List<Object[]> list,Date pDate, boolean byYear) throws Exception;

    /**
     * fonction de generation de fichier excel des frais
     *
     * @param fichierExcel     the filepath
     * @param pDateFrais       the date parameter
     * @param typePeriodeFrais the type of the file
     * @param list             a {@link List}
     * @return the filepath
     * @throws Exception the Exception
     */
    String generateExcel(String fichierExcel,Date pDateFrais, String typePeriodeFrais, List<Object[]> list) throws Exception;

}
