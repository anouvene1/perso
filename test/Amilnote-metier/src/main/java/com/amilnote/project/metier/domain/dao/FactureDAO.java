/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.TypeFacture;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * The interface Facture dao.
 */
public interface FactureDAO extends LongKeyDAO<Facture> {

    /**
     * retourne la facture  correspondant à la mission pour le mois voulu
     *
     * @param pMission la mission
     * @param pDate le mois voulu
     * @return la facture correspondante
     * @author clome
     */
    Facture findByMissionByMonth(Mission pMission, DateTime pDate);

    /**
     * Retourne la liste des factures correspondant à la mission entre les dates voulues
     *
     * @param pMission la mission
     * @param pDateDebut la date de début
     * @param pDateFin la date de fin
     * @return la liste des factures correspondante
     * @author clome
     */
    List<Facture> findByMissionBetweenDates(Mission pMission, DateTime pDateDebut, DateTime pDateFin);

    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param facture  the facture json
     * @return int int
     * @throws IOException the IO exception
     */
    int createOrUpdateFacture(Facture facture) throws IOException;

    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param factureJson  the facture json
     * @param pCommande the commande json
     * @param edit         the edit
     * @return int int
     */
    int createOrUpdateFactureAVANTREFONTE(FactureAsJson factureJson, Commande pCommande, Boolean edit);

    /**
     * Suppression de la commande
     *
     * @param pFacture the p facture
     * @return string string
     */
    String deleteFacture(Facture pFacture);

    /**
     * Find invoices by type (Facturables, Main, Frais) and activity report.
     *
     * @param factureType the type of invoices we want to.
     * @param startSearchDate first day of the month
     * @param endSearchDate last day of the month
     * @param activityReportValidate Find if the invoice depends on validated RA or not.
     * @return List
     */
    List<Facture> findFacturesByTypeAndByRA(TypeFacture factureType, Date startSearchDate, Date endSearchDate, Integer ... activityReportValidate);

    /**
     * Find factures list.
     *
     * @return La liste de toutes les facture
     */
    List<Facture> findFactures();

    /**
     * Retourne les factures qui sont complètes et peuvent être facturées
     *
     * @return the list
     * @author clome
     */
    List<Facture> findFacturables();

    /**
     * Trouver les factures pour lesquelles le collaborateur n'a pas soumis son RA
     *
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesRANonSoumis();

    /**
     * Return the list of all additional fees
     * @return the list
     * @author clome
     */
    List<Facture> findFacturesFrais();


    /**
     * Retourne les factures qui sont complètes et peuvent être facturées pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list of {@link Facture}
     * @throws Exception {@link Exception} exception
     * @author clome
     */
    List<Facture> findFacturablesByMonth(DateTime pDateVoulue) throws Exception;

    /**
     * Trouver les factures pour lesquelles le collaborateur n'a pas soumis son RA pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list of {@link Facture}
     * @throws Exception {@link Exception} exception
     * @author clome
     */
    List<Facture> findFacturesRANonSoumisByMonth(DateTime pDateVoulue) throws Exception;

    /**
     * Trouver les factures de frais pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list of {@link Facture}
     * @throws Exception {@link Exception} exception
     * @author clome
     */
    List<Facture> findFacturesFraisByMonth(DateTime pDateVoulue) throws Exception;

    /**
     * Trouver les factures à la main pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list of {@link Facture}
     * @throws Exception {@link Exception} exception
     * @author clome
     */
    List<Facture> findFacturesALaMainByMonth(DateTime pDateVoulue) throws Exception ;

    /**
     * Trouver le max des numéros des factures existantes
     *
     * @return int le max des numéros de facture
     */
    int findMaxNumFacture();

    /**
     * Trouver toutes les factures pour le mois en cours
     *
     * @return La liste des factures du mois
     */
    List<Facture> findAllFactures();

    /**
     * Find by commande list.
     *
     * @param pCommande the p commande
     * @return the list
     */
    List<Facture> findByCommande(Commande pCommande);

    boolean checkExistsNumFacture(int numFacture, int idFacture) throws IOException;

    /**
     * Method for retrieving the list of all invoices whose date is in a given month, and whose state is part of a given set of states
     * @param dateOfSearch the date of the month of search
     * @param propertyNameToOrderBy the result list will be ordered by this property
     * @param codesEtatsFactures list of invoices states codes, "BR" Brouillon, "SO" Soumis, "VA" Validé, etc.
     * @return all invoices whose date is in a given month, and whose state is part of a given set of states
     */
    List<Facture> findFactureByMonthAndState (DateTime dateOfSearch, String propertyNameToOrderBy, String... codesEtatsFactures);

    /**
     * Method for retrieving all invoices by given collaborator status code and one or multiple {@link SearchCriteria}
     * @param collaborateurStatusCode the status code of the collaborator
     * @param orderingProperty the resulting list will be ordered by this property
     * @param isAscendingOrdering is ordering ascending or descending
     * @param searchCriterias one or multiple (varargs) {@link SearchCriteria}
     * @return a list of all invoices corresponding to a given collaborator status code
     */
    List<Facture> findByCollaboratorStatusCodeAndSearchCriterias(String collaborateurStatusCode, String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) ;

    /**
     * Return all mission with bill
     * @param missions list of missions
     * @return list of {@link Facture}
     */
    List<Facture> findBillByMissionList(List<Mission> missions);
}
