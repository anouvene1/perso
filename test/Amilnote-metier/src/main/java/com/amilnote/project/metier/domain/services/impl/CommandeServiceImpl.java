/*
 * ©Amiltone 2017
 */


package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.CommandeDAO;
import com.amilnote.project.metier.domain.entities.Commande;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.CommandeService;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.FactureService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Commande service.
 */
@Service("commandeService")
public class CommandeServiceImpl extends AbstractServiceImpl<Commande, CommandeDAO> implements CommandeService {

    private static final Logger logger = LogManager.getLogger(CommandeServiceImpl.class);

    @Autowired
    private CommandeDAO commandeDAO;

    @Autowired
    private EtatService etatService;

    @Autowired
    private FactureService factureService;

    /**
     * {@inheritDoc}
     */
    @Override
    public CommandeDAO getDAO() {
        return commandeDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Commande findById(Long pIdCommande) {
        return commandeDAO.findUniqEntiteByProp(Commande.PROP_ID, pIdCommande);
    }

    /**
     * {@linkplain CommandeService#createOrUpdateCommande(CommandeAsJson, MissionAsJson)}
     */
    @Override
    public String createOrUpdateCommande(CommandeAsJson pCommandeAsJson, MissionAsJson pMissionAsJson) throws JsonProcessingException, IOException {

        return commandeDAO.createOrUpdateCommande(pCommandeAsJson, pMissionAsJson);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public String createOrUpdateCommande(Commande pCommande, Mission pMission) {
        String msgRetour = "";
        //[JNA][AMNOTE 141] Création d'une facture à partir d'une commande
        FactureAsJson factureJson = new FactureAsJson(); // On crée une nouvelle facture
        //[JNA][AMNOTE 166] On met l'état de la facture à "généré" par défaut
        Etat etat = etatService.getEtatByCode("GN");
        factureJson.setEtat(etat);
        // [JNA][AMNOTE 142] On prérempli la facture avec les élément de la commande
        if (pMission.getClient() != null) {
            factureJson.setIdClient(pMission.getClient().getId().intValue()); // on ajoute à la facture l'id du client
        } else {
            factureJson.setIdClient(0); // on ajoute à la facture l'id du client
        }
        String mode;
        if (pMission != null) {
            // --- Récupération de la mission
            Commande commande = new Commande();
            if (null != pCommande.getId()) { // Si l'ID n'est pas null on récupère la commande
                commande = findUniqEntiteByProp(Commande.PROP_ID, pCommande.getId());
                mode = "update";
            } else {
                mode = "insert";
            }

            // --- Ajout des informations concernant la commande
            commande.setMission(pMission);
            if (pCommande.getDateDebut() != null) {
                commande.setDateDebut(pCommande.getDateDebut());
            } else {
                commande.setDateDebut(null);
            }
            if (pCommande.getDateFin() != null) {
                commande.setDateFin(pCommande.getDateFin());
            } else {
                commande.setDateFin(null);
            }
            if (pCommande.getNbJours() != null) {
                commande.setNbJours(pCommande.getNbJours());
            }
            commande.setNumero(pCommande.getNumero());
            // --- Test sur l'objet pCommande pour remplir l'objet commande qui lui rempli la BD
            if (pCommande.getMontant() != 0) {
                //[JNA][AMNOTE 150] Si le montant a changé
                if (commande.getMontant() != null) {
                    if (!String.valueOf(pCommande.getMontant()).equals(String.valueOf(commande.getMontant()))) {
                        // On met à jour le montant
                        commande.setMontant(pCommande.getMontant());
                        // Et on met à jour le budget avec le nouveau montant
                        commande.setBudget(pCommande.getMontant());
                    }
                } else {
                    commande.setMontant(pCommande.getMontant());
                }

            } else {
                commande.setMontant((float) (0.0));
            }
            // --- Test sur l'objet pCommande pour remplir l'objet commande qui lui rempli la BD
            if (pCommande.getCommentaire() != null) {
                commande.setCommentaire(pCommande.getCommentaire());
            } else {
                commande.setCommentaire(null);
            }
            // --- [JNA][AMNOTE 142] Test si justificatif commande
            if (pCommande.getJustificatif() != null) {
                commande.setJustificatif(pCommande.getJustificatif());
            } else {
                commande.setJustificatif(null);
            }
            // --- [JNA][AMNOTE 160] Sauvegarde moyen de paiement
            if (pCommande.getMoyensPaiement() != null) {
                commande.setMoyensPaiement(pCommande.getMoyensPaiement());
            } else {
                commande.setMoyensPaiement(null);
            }
            if (pCommande.getEtat() != null) {
                commande.setEtat(pCommande.getEtat());
            } else {
                Etat etatCommande = etatService.getEtatByCode("CO");
                commande.setEtat(etatCommande);
            }
            // --- [JNA][AMNOTE 160] Sauvegarde moyen de paiement
            if (pCommande.getConditionPaiement() != null) {
                commande.setConditionPaiement(pCommande.getConditionPaiement());
            } else {
                commande.setConditionPaiement(null);
            }
            if (pCommande.getClient() != null) {
                commande.setClient(pCommande.getClient());
            } else {
                commande.setClient(null);
            }
            // [JNA][AMNOTE 150] Si la buget est vide ou à 0 on l'initialise avec le montant total
            if (commande.getBudget() == null || commande.getBudget() == 0) {
                commande.setBudget(commande.getMontant());
            } else if (pCommande.getBudget() != null && pCommande.getBudget() != 0) {
                commande.setBudget(pCommande.getBudget());
            }

            commandeDAO.saveOrUpdate(commande);

            pCommande.setId(commande.getId());
            if (mode.equals("insert")) {
                FactureAsJson factureAsJson = new FactureAsJson();
                factureService.createOrUpdateFactureAVANTREFONTE(factureAsJson, commande, false);
            }
            msgRetour = "ok";
        } else {
            msgRetour = "error";
        }
        return msgRetour;
    }

    /**
     * {@linkplain CommandeService#deleteCommande(Commande)}
     */
    @Override
    public String deleteCommande(Commande pCommande) {
        return commandeDAO.deleteCommande(pCommande);
    }

    /**
     * {@linkplain CommandeService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Commande> findAllOrderByNomAsc() {
        //with admin, without disabled
        return commandeDAO.findCommandes();
    }

    /**
     * {@linkplain CommandeService#findById(Long)}
     */
    @Override
    public List<CommandeAsJson> findAllOrderByNomAscAsJson() {
        List<Commande> tmpListCommande = this.findAllOrderByNomAsc();
        List<CommandeAsJson> tmpListCommandeAsJson = new ArrayList<>();

        for (Commande tmpCommande : tmpListCommande) {

            tmpListCommandeAsJson.add(tmpCommande.toJson());

        }
        return tmpListCommandeAsJson;
    }

    /**
     * {@linkplain CommandeService#getListCommandeFacture(List)}
     */
    @Override
    public List<Commande> getListCommandeFacture(List<LinkEvenementTimesheet> listLinkEvent) {
        List<Commande> listCommandesFactures = new ArrayList<>();

        //Pour chaque event, on récupère la mission et on la sauvegarde en incrémentant le nbr de demi journées si elle a une commande associée.
        //Si non on sauvegarde la mission dans listMissionSansCommande pour savoir quelle mission n'a pas eu de facture créée
        for (LinkEvenementTimesheet event : listLinkEvent) {

            if (event.getMission() != null) {
                Mission mission = event.getMission();

                List<Commande> listCommandes = mission.getCommandes();

                // Si la liste n'est pas null
                // et qu'elle n'est pas vide
                // et que la mission à un client
                // et que la mission est une mission cliente
                if (listCommandes != null && !listCommandes.isEmpty() && mission.getClient() != null) {
                    if (mission.getTypeMission().getCode().equals("MI")) {
                        listCommandesFactures.addAll(listCommandes);
                    }
                }
            }
        }
        return listCommandesFactures;
    }

    /**
     * {@linkplain CommandeService#findActivesCommandes()}
     */
    @Override
    public List<Commande> findActivesCommandes() {
        return commandeDAO.findActivesCommandes();
    }

    /**
     * {@linkplain CommandeService#findCommandesForMonth(String, String)}
     */
    @Override
    public List<Commande> findCommandesForMonth(String date, String code) {
        Etat etat = etatService.getEtatByCode(code);
        String fullDate = "01/" + date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateTime dateTime = null;
        try {
            dateTime = new DateTime(simpleDateFormat.parse(fullDate));
        } catch (ParseException e) {
            logger.error("[order by month] error parsing date : {}", date, e);
        }

        DateTime dateDebut = dateTime.dayOfMonth().withMinimumValue();
        DateTime dateFin = dateTime.dayOfMonth().withMaximumValue();

        return commandeDAO.findCommandesForMonth(dateDebut, dateFin, etat);
    }

    /**
     * {@linkplain CommandeService#findStartedCommandes()}
     */
    @Override
    public List<Commande> findStartedCommandes() {
        Etat etatCommencee = etatService.getEtatByCode(Etat.ETAT_COMMENCE);
        return commandeDAO.findStartedCommandes(etatCommencee);
    }

    /**
     * {@linkplain CommandeService#findStoppedCommandes()}
     */
    @Override
    public List<Commande> findStoppedCommandes() {
        Etat etatTerminee = etatService.getEtatByCode(Etat.ETAT_TERMINE);
        return commandeDAO.findStoppedCommandes(etatTerminee);
    }

    /**
     * {@linkplain CommandeService#findByIdMissionByMonth(Mission, DateTime)}
     */
    @Override
    public List<Commande> findByIdMissionByMonth(Mission mission, DateTime dateVoulue) {
        return commandeDAO.findByIdMissionByMonth(mission, dateVoulue);
    }


    /**
     * {@linkplain CommandeService#findByIdMission(Mission)}
     */
    @Override
    public List<Commande> findByIdMission(Mission mission) {
        return commandeDAO.findByIdMission(mission);
    }
}


