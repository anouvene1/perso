/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.*;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.joda.time.DateTime;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

import static com.amilnote.project.metier.domain.utils.Constantes.FRENCH_MONTHS_NAMES;


/**
 * The type Utils.
 */
public class Utils {

    private static final Logger logger = LogManager.getLogger(Utils.class);

    private static final String HOSTNAME_CONTEXT = "hostName";
    private static final String AUTHENTIFICATION_MAIL_CONTEXT = "authentificationMail";
    private static final String AUTHENTIFICATION_PASSWORD_CONTEXT = "authentificationPassword";
    private static final String SMTP_PORT = "smtpPort";
    private static final String CHARSET_ENCODING = "encoding";
    private static final String MIMEBODYPART_CONTENT = "text/html; charset=utf-8";
    private static final String ALL_STANDARD_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    public static final int LAST_HOUR_OF_DAY = 23;
    public static final int LAST_MINUTE_OF_HOUR = 59;
    public static final int LAST_SECOND_OF_MINUTE = 59;

    /**
     * Mois int to string string.
     *
     * @param moisInt the mois int
     * @return the string
     */
    public static String moisIntToString(int moisInt) {
        String moisString = "";

        if (moisInt < 0 || moisInt > 11) {
            return moisString;
        }
        return FRENCH_MONTHS_NAMES[moisInt];
    }

    /**
     * Convertit un mois donné en string en format MMMM
     *
     * @param pMois mois au format MM (01-12)
     * @return formatted date
     */
    public static String moisStringToString(String pMois) {
        String lMois;

        if (pMois.startsWith("0")) {
            lMois = pMois.substring(1);
        } else {
            lMois = pMois;
        }
        int mois = Integer.parseInt(lMois);

        return Utils.moisIntToString(--mois);

    }

    /**
     * Sends an email to one or many persons with or without attached files
     * It is possible to send this email with recipients in CC and/or BCC fields
     *
     * @param senderMail    the email address of the sender
     * @param senderName    the name of the sender
     * @param subject       the subject of the mail
     * @param content       the content of the mail
     * @param attachedFiles the attached file(s)
     * @param recipients    the list of recipients
     * @param recipientsCC  the list of recipients in CC
     * @param recipientsBCC the list of recipients in BCC
     */
    public static void sendEmail(String senderMail, String senderName, String subject, String content, List<File> attachedFiles,
                                 List<String> recipients, List<String> recipientsCC, List<String> recipientsBCC) {
        try {
            // Mail server settings
            MultiPartEmail email = new MultiPartEmail();
            email.setHostName(Parametrage.getContext(HOSTNAME_CONTEXT));
            email.setAuthentication(Parametrage.getContext(AUTHENTIFICATION_MAIL_CONTEXT), Parametrage.getContext(AUTHENTIFICATION_PASSWORD_CONTEXT));
            email.setSmtpPort(Integer.parseInt(Parametrage.getContext(SMTP_PORT)));
            email.setSSLCheckServerIdentity(true);
            email.setStartTLSRequired(true);

            // Tests if the context is set on production or development
            if (!Boolean.valueOf(Parametrage.getContext(Constantes.EMAIL_PROD_LIST_CONTEXT))) {
                // Clears the list for testing purposes
                recipients.clear();
                recipientsCC.clear();
                recipientsBCC.clear();
                recipients.add(Parametrage.getContext(Constantes.EMAIL_DEV_LIST_CONTEXT));
                senderName = Parametrage.getContext(Constantes.EMAIL_ADMIN_NAME_CONTEXT);
            }

            // Adds all the recipients to the mail
            for (String recipient : recipients) {
                email.addTo(recipient);
            }
            for (String recipient : recipientsCC) {
                email.addCc(recipient);
            }
            for (String recipient : recipientsBCC) {
                email.addBcc(recipient);
            }

            email.setFrom(senderMail, senderName);
            email.setCharset(Parametrage.getContext(CHARSET_ENCODING));
            email.setSubject(subject);

            MimeMultipart multipart = new MimeMultipart();

            // Creates the message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(content, MIMEBODYPART_CONTENT);
            multipart.addBodyPart(messageBodyPart);

            // Adds any attached files
            if (attachedFiles != null && !attachedFiles.isEmpty()) {
                for (File file : attachedFiles) {
                    MimeBodyPart fileBodyPart = new MimeBodyPart();
                    fileBodyPart.attachFile(file.getAbsolutePath());
                    multipart.addBodyPart(fileBodyPart);
                }
            }
            email.setContent(multipart);
            email.send();
        } catch (MessagingException | EmailException | IOException | NamingException error) {
            logger.error("[send email]", error);
        }
    }

    /**
     * Méthode permettant de générer une chaine de caractères aléatoire.
     *
     * @param length la taille de la chaine voulu
     * @return String la chaine de caractères aléatoire
     */
    public static String _generate(int length) {
        String chars = ALL_STANDARD_CHARACTERS;
        StringBuilder pass = new StringBuilder(length);
        for (int x = 0; x < length; x++) {
            int i = (int) (Math.random() * chars.length());
            pass.append(chars.charAt(i));
        }
        return pass.toString();
    }

    /**
     * Créé la cellule
     *
     * @param myRow          The row
     * @param wb             The WorkBook
     * @param pNbColonne     The column's number
     * @param pCellValue     The Value of the cell
     * @param cellAlignement The alignment (ALIGN_GENERAL, ALIGN_LEFT, ALIGN_CENTER, ALIGN_RIGHT, ALIGN_FILL, ALIGN_JUSTIFY, ALIGN_CENTER_SELECTION)
     * @param pStyle         The style of the cell
     */
    public static void createCellule(Row myRow, Workbook wb, int pNbColonne, String pCellValue, HSSFRichTextString cellAlignement, CellStyle pStyle) {
        myRow.createCell(pNbColonne);

        if (cellAlignement != null) {
            myRow.getCell(pNbColonne).setCellValue(cellAlignement);
        } else {
            myRow.getCell(pNbColonne).setCellValue(pCellValue);
        }

        if (pStyle != null) {
            myRow.getCell(pNbColonne).setCellStyle(pStyle);
        }
    }

    /**
     * Mappe les événements par date
     *
     * @param listEvent     Liste des événements du mois
     * @param mapDateEvents Map des événements par date
     */
// SPE_19/01/17_AMNOTE_205: Création de la fonction
    public static void mapEventsByDate(List<LinkEvenementTimesheet> listEvent, LinkedHashMap<Date, List<LinkEvenementTimesheet>> mapDateEvents) {
        // --- Mapping des événements par date
        listEvent.forEach(event -> {
            if (mapDateEvents.containsKey(event.getDate())) {
                // si la map des dates event contient la clé date de l'event
                // on rajoute l'event dans la MAP
                mapDateEvents.get(event.getDate()).add(event);
            } else {
                // on ajoute les events dans un arrayList
                List<LinkEvenementTimesheet> tmpListEvents = new ArrayList<LinkEvenementTimesheet>();
                tmpListEvents.add(event);
                mapDateEvents.put(event.getDate(), tmpListEvents);
            }
        });
    }

    /**
     * Calcule le nombre de jours total.
     *
     * @param pMapDateEvents      Map des événements par date
     * @param pLinkMissionForfait Lien mission-forfait
     * @return le nombre de jours total calculé
     */
    public static int calculNbJours(LinkedHashMap<Date, List<LinkEvenementTimesheet>> pMapDateEvents, LinkMissionForfait pLinkMissionForfait) {
        int nbJours = 0;

        // --- Pour chaque jour, événements AM & PM
        for (Map.Entry<Date, List<LinkEvenementTimesheet>> entry : pMapDateEvents.entrySet()) {

            // --- Le jour contient des événements
            if (entry.getValue().size() > 0) {

                LinkEvenementTimesheet eventAM = null;
                LinkEvenementTimesheet eventPM = null;
                boolean eventAMContainsForfait = false;
                boolean eventPMContainsForfait = false;

                // --- Pour chaques événement du jour traité
                for (LinkEvenementTimesheet tmpEvent : entry.getValue()) {

                    // --- MATIN
                    if (tmpEvent.getMomentJournee() == 0 && eventAMContainsForfait == false) {

                        eventAM = tmpEvent;
                        Absence tmpAbsenceAM = eventAM.getAbsence();
                        // --- L'événement contient un forfait et n'est pas une
                        // absence Valide ou Soumise
                        eventAMContainsForfait = (null != eventAM.getMission()
                                && eventAM.getMission().getListMissionForfaits().contains(pLinkMissionForfait)
                                && !(isAbsenceVAOrSO(tmpAbsenceAM)));

                        // --- APRES MIDI
                    } else {
                        eventPM = tmpEvent;
                        Absence tmpAbsencePM = eventPM.getAbsence();
                        // --- L'événement contient un forfait et n'est pas une
                        // absence Valide ou Soumise
                        eventPMContainsForfait = (null != eventPM.getMission()
                                && eventPM.getMission().getListMissionForfaits().contains(pLinkMissionForfait)
                                && !isAbsenceVAOrSO(tmpAbsencePM));

                    }
                }

                // --- Présence de forfait pour AM & PM
                if (eventAMContainsForfait && eventPMContainsForfait) {
                    nbJours++;
                    // --- Présence de forfait uniquement pour AM
                } else if (eventAMContainsForfait && (!eventPMContainsForfait)) {
                    nbJours++;
                    // --- Présence de forfait uniquement pour PM
                } else if (!eventAMContainsForfait && eventPMContainsForfait) {
                    // --- un evenement est présent le matin
                    if (null != eventAM) {
                        // cette evenement est une absence va ou so
                        if (isAbsenceVAOrSO(eventAM.getAbsence())) {

                            nbJours++;

                        } else if (null == eventAM.getMission()) {
                            nbJours++;
                        } else {
                            boolean containsForfaitDepForOtherMission = false;
                            for (LinkMissionForfait tmpLinkMissionForfait : eventAM.getMission()
                                    .getListMissionForfaits()) {

                                if (tmpLinkMissionForfait.getForfait().getCode().equals(Forfait.CODE_DEPLACEMENT)) {
                                    containsForfaitDepForOtherMission = true;
                                    break;
                                }

                            }
                            if (!containsForfaitDepForOtherMission) {
                                nbJours++;
                            }
                        }

                    } else {
                        nbJours++;
                    }

                }

            }

        }

        return nbJours;
    }

    /**
     * Si l'absence est valide ou soumise ou pas null retourne vrai
     *
     * @param pAbsence the p absence
     * @return bool boolean
     */
// SPE_19/01/17_AMNOTE_205: Déplacement de la fonction dans Utils
    public static boolean isAbsenceVAOrSO(Absence pAbsence) {
        boolean res = false;

        if (null != pAbsence) {
            res = (pAbsence.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE) || pAbsence.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE));
        }

        return res;
    }

    /**
     * Put the current month on xls files header
     *
     * @param wantedDate to get the invoice's month
     * @return String
     */
    public static String monthString(Date wantedDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(wantedDate);
        String monthString = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        monthString = monthString.substring(0, 1).toUpperCase() + monthString.substring(1) + " " + cal.get(Calendar.YEAR);
        return monthString;
    }

    /**
     * Put the current year on xls files header
     *
     * @return String
     */
    public static String yearString() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        String yearString = String.valueOf(year);
        yearString = yearString.substring(0, 1).toUpperCase() + yearString.substring(1);
        return yearString;
    }

    /**
     * Find collaborator's full name by invoice
     *
     * @param facture current invoice
     * @return String
     */
    public static String fullNameCollabByFacture(Facture facture) {
        return facture.getMission().getCollaborateur().getNom() + " " + facture.getMission().getCollaborateur().getPrenom();
    }

    /**
     * Find manager's full name by invoice
     *
     * @param facture current invoice
     * @return String
     */
    public static String fullNameManagerByFacture(Facture facture) {
        return facture.getMission().getManager().getNom() + " " + facture.getMission().getManager().getPrenom();
    }

    /**
     * Find manager's full name by invoice and collaborator
     *
     * @param facture current invoice
     * @return String
     */
    public static String fullNameManagerByCollabByFacture(Facture facture) {
        return facture.getMission().getCollaborateur().getManager().getNom() + " " + facture.getMission().getCollaborateur().getManager().getPrenom();
    }

    /**
     * Concatenate multiple object with StringBuilder append() method
     *
     * @param objects list of Objects to concatenate together
     * @return the concatenated string result
     */
    public static String concatenateObjectsToString(Object... objects) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object objectTemp : objects) {
            stringBuilder.append(objectTemp);
        }
        return stringBuilder.toString();
    }

    /**
     * Extract the entered Date in the datePicker to DateTime type by a month and year as Strings
     *
     * @param month the month of search
     * @param year  the year of search
     * @return the resulting Date from the Datepicker
     * @author clome
     */
    public static DateTime extractPickedDate(String month, String year) {
        Calendar calendarRefonte = Calendar.getInstance();
        if (year != null && !year.isEmpty() && month != null && !month.isEmpty()) {
            int yearResult = Integer.parseInt(year);
            int monthResult = Integer.valueOf(month) - 1;
            calendarRefonte.set(yearResult, monthResult, 1, 0, 0, 0);
        } else {
            calendarRefonte.set(Calendar.HOUR, 0);
            calendarRefonte.set(Calendar.MINUTE, 0);
            calendarRefonte.set(Calendar.SECOND, 0);
        }
        return new DateTime(calendarRefonte.getTime());
    }

    /**
     * Calculate first and last Dates of a given month and year as Integers
     *
     * @param yearNumber  the year of search
     * @param monthNumber the month of search
     * @return an array of two Dates
     */
    public static DateTime[] firstAndLastDatesOfMonth(Integer yearNumber, Integer monthNumber) {
        DateTime startDate;
        DateTime endDate;
        if (yearNumber != null && monthNumber != null) {
            startDate = new DateTime(yearNumber, monthNumber, 1, 0, 0, 0, 0);
            endDate = startDate.dayOfMonth().withMaximumValue().withHourOfDay(LAST_HOUR_OF_DAY).withMinuteOfHour(LAST_MINUTE_OF_HOUR).withSecondOfMinute(LAST_SECOND_OF_MINUTE);
        } else {
            startDate = DateTime.now().dayOfMonth().withMinimumValue().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
            endDate = DateTime.now().dayOfMonth().withMaximumValue().withHourOfDay(LAST_HOUR_OF_DAY).withMinuteOfHour(LAST_MINUTE_OF_HOUR).withSecondOfMinute(LAST_SECOND_OF_MINUTE);
        }
        return new DateTime[]{startDate, endDate};
    }

    /**
     * Calculate first and last Dates of a given year and month which is the name of the month and which will be compared in a list of months names to get the month number
     *
     * @param yearNumber  the year of search
     * @param monthName   the month name of search
     * @param monthsNames a String array of months names which will be compared to the name of the month of search to get the month as Integer
     * @return an array of two Dates
     */
    public static DateTime[] firstAndLastDatesOfMonth(Integer yearNumber, String monthName, String[] monthsNames) {
        Integer monthNumber = IntStream.range(0, monthsNames.length).filter(index -> monthsNames[index].equals(monthName)).findFirst().getAsInt() + 1;
        return firstAndLastDatesOfMonth(yearNumber, monthNumber);
    }

    /**
     * Method to round a {@link Float} number, according to a number of decimals after the decimal point
     *
     * @param floatNumber the float number to be rounded
     * @param decimals    the number of decimals desired after the decimal point
     * @return a {@link Double} number of the rounded float
     */
    public static Double roundNumber(Float floatNumber, int decimals) {
        return Math.round(floatNumber * (Math.pow(10, decimals))) / (Math.pow(10, decimals));
    }

    /**
     * Method to round a {@link Double} number, according to a number of decimals after the decimal point
     *
     * @param doubleNumber the double number to be rounded
     * @param decimals     the number of decimals desired after the decimal point
     * @return a {@link Double} number of the rounded double
     */
    public static Double roundNumber(Double doubleNumber, int decimals) {
        return Math.round(doubleNumber * (Math.pow(10, decimals))) / (Math.pow(10, decimals));
    }

    /**
     * Get username of logged user from security context
     *
     * @return String logged username
     */
    public static String getloggedUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }


    /**
     * Parse a string to an int or a default value if there is an error
     *
     * @param stringToParse {@link String}
     * @param defaultValue  {@link String}
     * @return {@link Integer}
     */
    public static Integer parseToInt(String stringToParse, Integer defaultValue) {
        Integer ret;
        try {
            ret = Integer.parseInt(stringToParse);
        } catch (NumberFormatException ex) {
            ret = defaultValue;
        }
        return ret;
    }
}
