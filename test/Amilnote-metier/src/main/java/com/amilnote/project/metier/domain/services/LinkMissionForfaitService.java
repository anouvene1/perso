/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.LinkMissionForfait;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import org.joda.time.DateTime;

import java.util.List;

/**
 * The interface Link mission forfait service.
 */
public interface LinkMissionForfaitService extends AbstractService<LinkMissionForfait> {

    /**
     * rechercher la liste des forfaits enregistrés pour une mission
     *
     * @param linkMissionForfait the link mission forfait
     * @param dateDebut          the date debut
     * @param dateFin            the date fin
     * @return float float
     */
    float calculByPeriod(LinkMissionForfait linkMissionForfait, DateTime dateDebut, DateTime dateFin);

    /**
     * Calcul by period and id link mission forfait float.
     *
     * @param id        the id
     * @param dateDebut the date debut
     * @param dateFin   the date fin
     * @return the float
     */
    float calculByPeriodAndIdLinkMissionForfait(Long id, DateTime dateDebut, DateTime dateFin);

    /***
     * Ajoute le LinkMissionForfaitAsJson à la mission passé en parametre
     *
     * @param missionAsJson            the mission as json
     * @param linkMissionForfaitAsJson the link mission forfait as json
     * @return string string
     */
    String addLinkMissionForfait(MissionAsJson missionAsJson, LinkMissionForfaitAsJson linkMissionForfaitAsJson);

    /***
     * Supprime le linkMissionForfait
     *
     * @param pIdForfait the p id forfait
     */
    void deleteLinkMissionForfait(Long pIdForfait);

    /**
     * retourne la liste des linkMissionForfait pour une mission
     *
     * @param pMission the p mission
     * @return list list
     */
    List<LinkMissionForfait> findAllByMission(Mission pMission);

    List<LinkMissionForfait> findLib();

    List<LinkMissionForfait> findNotLib();

    List<LinkMissionForfait> findLibByMission(Mission pMission);

    List<LinkMissionForfait> findNotLibByMission(Mission pMission);
}
