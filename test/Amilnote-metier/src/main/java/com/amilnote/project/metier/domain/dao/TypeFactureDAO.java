/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.TypeFacture;

import java.util.List;

public interface TypeFactureDAO extends LongKeyDAO<TypeFacture> {


}
