/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.FraisDAO;
import com.amilnote.project.metier.domain.dao.MissionDAO;
import com.amilnote.project.metier.domain.dao.RapportActivitesDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * The type Rapport activites dao.
 */
@Repository("rapportActivitesDAO")
public class RapportActivitesDAOImpl extends AbstractDAOImpl<RapportActivites> implements RapportActivitesDAO {

    private static final Logger logger = LogManager.getLogger(RapportActivites.class);

    @Autowired
    private MissionDAO missionDAO;

    @Autowired
    private EtatDAO etatDAO;

    @Autowired
    private FraisDAO fraisDAO;

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<RapportActivites> getReferenceClass() {
        return RapportActivites.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveNewRACollaborator(Date pDateSoumission, String pPdfRapportActivite, Collaborator collaborator, DateTime pDateRAVoulue, boolean pAvecFrais) {

        Session lSession = currentSession();

        //On part du principe que la date dans la base de données sera toujours le premier jour du mois
        Date dateRAVoulueFirstDay = pDateRAVoulue.dayOfMonth().withMinimumValue().toDate();

        Criteria criteria = this.getCriteria();
        criteria.add(Restrictions.eq(RapportActivites.PROP_COLLABORATEUR, collaborator));
        criteria.add(Restrictions.eq(RapportActivites.PROP_MOISRAPPORT, dateRAVoulueFirstDay));

        RapportActivites listeRAToCreateOrUpdate = (RapportActivites) criteria.uniqueResult();

        Transaction transaction = lSession.beginTransaction();

        Etat lEtatSo = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        Etat lEtatBR = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);

        //Sinon si on souhaite une validation avec frais
        if (pAvecFrais) {
            //Aucun RA existe pour le mois donné
            if (null == listeRAToCreateOrUpdate) {
                //On enregistre à l'état Soumis
                listeRAToCreateOrUpdate = new RapportActivites(pDateSoumission, pPdfRapportActivite, lEtatSo, collaborator, pDateRAVoulue.toDate(), null);
            } else {
                //On met à jour à l'état Soumis
                listeRAToCreateOrUpdate.setEtat(lEtatSo);
                listeRAToCreateOrUpdate.setDateSoumission(pDateSoumission);
                //si le RA en train d'être soumis avait été annulé avec un commentaire, il faut le reset
                listeRAToCreateOrUpdate.setCommentaire("");
            }
        }
        //Si on souhaite une validation sans les frais
        else {
            //Aucun RA existe pour le mois donné
            if (null == listeRAToCreateOrUpdate) {
                //On enregistre à l'état Brouillon
                listeRAToCreateOrUpdate = new RapportActivites(pDateSoumission, pPdfRapportActivite, lEtatBR, collaborator, pDateRAVoulue.toDate(), null);
            } else {
                //On met à jour à l'état Brouillon
                listeRAToCreateOrUpdate.setEtat(lEtatBR);
                listeRAToCreateOrUpdate.setPdfRapportActivite(pPdfRapportActivite);
            }
        }

        try {
            lSession.saveOrUpdate(listeRAToCreateOrUpdate);
            lSession.flush();
            transaction.commit();
        } catch (HibernateException e) {
            logger.debug(e.getMessage());
            return e.getMessage();
        }

        if (pAvecFrais) {
            //TODO WTF!? pourquoi faire ça?? mauvaise pratique! soit faire une enum soit faire une requete en base, mais pas ça!!
            Etat etat = new Etat();
            etat.setCode(Etat.ETAT_SOUMIS_CODE);
            etat.setEtat(Etat.ETAT_SOUMIS);
            etat.setId(2);
            listeRAToCreateOrUpdate.setPdfRapportActivite(pPdfRapportActivite);

            if (!fraisDAO.updateFraisEnCours(collaborator, pDateRAVoulue, etat)) {
                logger.debug(Constantes.ERROR_MSG_UPDATE_FRAIS);
                return Constantes.ERROR_MSG_UPDATE_FRAIS;
            }
        }

        return "rapport d'activites enregistré avec succes";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getDateDernierRASoumis(Collaborator collaborator) {

        Criteria lraSoumis = currentSession().createCriteria(RapportActivites.class).setProjection(Projections.max(RapportActivites.PROP_DATESOUMISSION));

        lraSoumis.add(Restrictions.eq(RapportActivites.PROP_COLLABORATEUR, collaborator));

        return (Date) lraSoumis.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RapportActivites> getExistingRAForMonth(Collaborator collaborator, DateTime pDateTime) {

        Criteria criteria = currentSession().createCriteria(RapportActivites.class);

        DateTime lDebutPeriode = pDateTime.dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();
        DateTime lFinPeriode = pDateTime.dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();

        Etat etatAN = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_ANNULE);
        Etat etatRE = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_REFUSE);

        criteria.add(Restrictions.eq(RapportActivites.PROP_COLLABORATEUR, collaborator));

        criteria.add(Restrictions.le(RapportActivites.PROP_MOISRAPPORT, lFinPeriode.toDate()));

        criteria.add(Restrictions.ge(RapportActivites.PROP_MOISRAPPORT, lDebutPeriode.toDate()));

        criteria.add(Restrictions.ne(RapportActivites.PROP_ETAT, etatAN));

        criteria.add(Restrictions.ne(RapportActivites.PROP_ETAT, etatRE));

        List<RapportActivites> listeRa = criteria.list();

        return listeRa;

    }

    public List<RapportActivites> getAllRAForCollaboratorBetweenDate(Collaborator collaborator, DateTime pDateTime) {
        Criteria criteria = currentSession().createCriteria(RapportActivites.class);
        Etat etatVA = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_VALIDE_CODE);
        Etat etatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);

        DateTime lDebutPeriode = pDateTime.dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();

        criteria.add(Restrictions.eq(RapportActivites.PROP_COLLABORATEUR, collaborator));
        criteria.add(Restrictions.ge(RapportActivites.PROP_MOISRAPPORT, lDebutPeriode.toDate()));

        criteria.add(Restrictions.or
                (Restrictions.eq(RapportActivites.PROP_ETAT, etatVA),
                        Restrictions.eq(RapportActivites.PROP_ETAT, etatSO)));

        return (List<RapportActivites>) criteria.list();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<RapportActivites> getBrouillonRAForMonth(Collaborator collaborator, DateTime pDateTime) {

        Criteria criteria = getCriteria();

        DateTime lDebutPeriode = pDateTime.dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();
        DateTime lFinPeriode = pDateTime.dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();


        Etat etatAN = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_ANNULE);
        Etat etatRE = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_REFUSE);

        criteria.add(Restrictions.eq(RapportActivites.PROP_COLLABORATEUR, collaborator));

        criteria.add(Restrictions.le(RapportActivites.PROP_MOISRAPPORT, lFinPeriode.toDate()));

        criteria.add(Restrictions.ge(RapportActivites.PROP_MOISRAPPORT, lDebutPeriode.toDate()));

        criteria.add(Restrictions.ne(RapportActivites.PROP_ETAT, etatAN));

        criteria.add(Restrictions.ne(RapportActivites.PROP_ETAT, etatRE));


        List<RapportActivites> listeRa = criteria.list();
        List<RapportActivites> listeRas = new ArrayList<>();
        listeRas.addAll(listeRa);

        return listeRas;

    }

    @Override
    public List<RapportActivites> getRACollaboratorMissionClienteForMonth(DateTime pDateTime) {

        List<Mission> listeMission;
        List<RapportActivites> listeRaCollab = new ArrayList<RapportActivites>();
        List<RapportActivites> listeRaStt = new ArrayList<RapportActivites>();


        //comparateur pour le set
        class collabComparatorByName implements Comparator<Collaborator> {
            @Override
            public int compare(Collaborator collab1, Collaborator collab2) {
                String nom1 = collab1.getNom();
                String nom2 = collab2.getNom();

                if (nom1.equals(nom2) && !collab1.getPrenom().equals(collab2.getPrenom())) {
                    return 1;
                }

                return nom1.compareToIgnoreCase(nom2);
            }
        }
        Set<Collaborator> listStt = new TreeSet<Collaborator>(new collabComparatorByName());
        Set<Collaborator> listCollab = new TreeSet<Collaborator>(new collabComparatorByName());


        // liste des missions clientes du mois

        listeMission = missionDAO.findMissionClientActuelle(pDateTime);

        // liste des collab (sans doublon) dont le ra doit sortir
        for (Mission mission : listeMission) {
            if (mission.getCollaborateur().getPoste().getCode().equals(Poste.POSTE_SOUS_TRAITANT)) {

                listStt.add(mission.getCollaborateur());
            } else {

                listCollab.add(mission.getCollaborateur());
            }
        }

        // liste des Ra pour chaque collaborateur de la liste pour le mois concerné (trier par Collab ou sous traitant et chacune des parties par ordre alphabétique)
        for (Collaborator collaborateur : listCollab) {

            RapportActivites rap = getExistingRAForMonthForCollab(collaborateur, pDateTime);
            if (rap != null) {
                listeRaCollab.add(rap);
            }
        }
        for (Collaborator stt : listStt) {
            if (getExistingRAForMonthForCollab(stt, pDateTime) != null) {
                listeRaStt.add(getExistingRAForMonthForCollab(stt, pDateTime));
            }
        }
        if (!listeRaStt.isEmpty()) {
            listeRaCollab.addAll(listeRaStt);
        }

        return listeRaCollab;
    }

    public RapportActivites getExistingRAForMonthForCollab(Collaborator pCollaborator, DateTime pDateTime) {
        List<RapportActivites> listRaCollab = pCollaborator.getRapportActivites();
        RapportActivites byDefault = null;
        Date date = pDateTime.toDate();
        Etat etatAN = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_ANNULE);
        Etat etatRE = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_REFUSE);

        DateTime lDebutPeriode = pDateTime.dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();
        DateTime lFinPeriode = pDateTime.dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();

        for (RapportActivites ra : listRaCollab) {
            if (ra.getMoisRapport().getMonth() == date.getMonth() && ra.getMoisRapport().getYear() == date.getYear()) {
                if (ra.getEtat() != etatAN && ra.getEtat() != etatRE)
                    return ra;
            }
        }
        return byDefault;
    }
}
