package com.amilnote.project.metier.domain.utils.enumerations;

public enum ClientExcelEnum {

    CLIENTS_PAR_COLLAB("clientsPerCollaborator"),
    CLIENTS_PAR_STT("clientsPerCollaboratorSTT"),
    SOUSTRAITANTS_EN_MISSION("SST"),
    COLLABORATEUR_EN_MISSION("CO");

    private final String excelAction;

    private static final String ERROR_MESSAGE = "Error : invalid Client Excel Action";

    ClientExcelEnum(String action){
        excelAction = action;
    }

    public static ClientExcelEnum getClientExcelType(String action){
        for (ClientExcelEnum excelType : ClientExcelEnum.values())
        {
            if (action.equals(excelType.excelAction)) return excelType;
        }
        throw new IllegalArgumentException(ERROR_MESSAGE);
    }

    public String toString() {
        return this.excelAction;
    }

}
