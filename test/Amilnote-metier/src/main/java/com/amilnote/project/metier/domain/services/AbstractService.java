/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.hibernate.criterion.Order;
import java.util.List;

/**
 * The interface Abstract service.
 *
 * @param <T> the type parameter
 */
public interface AbstractService<T> {

    /**
     * Save.
     *
     * @param t the t
     */
    void save(T t);

    /**
     * Load t.
     *
     * @param id the id
     * @return the t
     */
    T load(Long id);

    /**
     * Get t.
     *
     * @param id the id
     * @return the t
     */
    T get(Long id);

    /**
     * Save or update.
     *
     * @param t the t
     */
    void saveOrUpdate(T t);

    /**
     * Delete.
     *
     * @param t the t
     */
    void delete(T t);

    /**
     * Find all list.
     *
     * @param order the order
     * @return the list
     */
    List<T> findAll(Order order);

    /**
     * Find uniq entite by prop t.
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return the t
     */
    T findUniqEntiteByProp(String propName, Object propValue);

    /**
     * Find list entites by prop list.
     *
     * @param propName  the prop name
     * @param propValue the prop value
     * @return the list
     */
    List<T> findListEntitesByProp(String propName, Object propValue);

    /**
     * retrieve all database entries corresponding to one or multiple {@link SearchCriteria} if not null, and order by ordering property ascending or descending according to isAscendingOrdering boolean
     * @param orderingProperty the resulting entries list will by ordered by this property, default ordering if null
     * @param isAscendingOrdering if true the resluting list will be ordered by orderingProperty ascending, else (false or null) descending
     * @param searchCriterias varargs of {@link SearchCriteria} which will restrict the search
     * @return a list of all database entries corresponding to the searchCriterias, if null retrieve all entries
     */
    List<T> findBySearchCriterias(String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) ;

}
