/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.TypeFraisDAO;
import com.amilnote.project.metier.domain.entities.TypeFrais;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Type frais dao.
 */
@Repository("typeFraisDAO")
public class TypeFraisDAOImpl extends AbstractDAOImpl<TypeFrais> implements TypeFraisDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<TypeFrais> getReferenceClass() {
        return TypeFrais.class;
    }

    /**
     * {@linkplain TypeFraisDAO#getAllTypeFrais()}
     */
    @Override
    public List<TypeFrais> getAllTypeFrais() {
        return currentSession().createCriteria(TypeFrais.class).list();
    }

    /**
     * {@linkplain TypeFraisDAO#getTypeFraisById(long)}
     */
    @Override
    public TypeFrais getTypeFraisById(long id) {
        TypeFrais lTypeFrais = (TypeFrais) currentSession().get(TypeFrais.class, id);
        return lTypeFrais;
    }

}
