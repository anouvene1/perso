/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.TypeFacture;


public interface TypeFactureService extends AbstractService<TypeFacture> {

    /**
     * Retourne un type de frais en fonction de l'id
     *
     * @param pId the p id
     * @return TypeFacture type frais by id
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    TypeFacture findById(long pId);

    /**
     * Retourne le type de frais correspondant au code renseigné
     *
     * @param code code du type de frais souhaité
     * @return Type de frais correspondant au code
     */
    TypeFacture findByCode(String code);
}
