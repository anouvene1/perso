/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.LinkTypeFraisCaracDAO;
import com.amilnote.project.metier.domain.entities.LinkTypeFraisCarac;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Link type frais carac dao.
 */
@Repository("linkTypeFraisCaracDAO")
public class LinkTypeFraisCaracDAOImpl extends AbstractDAOImpl<LinkTypeFraisCarac> implements LinkTypeFraisCaracDAO {

    /**
     * retrouver la classe modele liée au DAO
     *
     * @return la classe liée
     */
    @Override
    protected Class<LinkTypeFraisCarac> getReferenceClass() {
        return LinkTypeFraisCarac.class;
    }

    public List<LinkTypeFraisCarac> findAll() {
        Criteria lCritFraisCarac = getCriteria();
        return (List<LinkTypeFraisCarac>) lCritFraisCarac.list();

    }

}
