package com.amilnote.project.metier.domain.entities;


import com.amilnote.project.metier.domain.entities.json.RaisonSocialeAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "ami_ref_raison_sociale")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class RaisonSociale implements Serializable{
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_ID_COLLAB.
     */
    public static final String PROP_ID_COLLABORATEUR = "collaborateur";

    /**
     * The constant PROP_RAISON_SOCIALE.
     */
    public static final String PROP_RAISON_SOCIALE = "raison_sociale";

    private static final long serialVersionUID = -1319603069615603284L;
    private Long id;
    private Collaborator collaborator;
    private String raisonSociale;


    public RaisonSociale(Long id, Collaborator collaborator, String raisonSociale) {
        this.id = id;
        this.collaborator = collaborator;
        this.raisonSociale = raisonSociale;
    }

    public RaisonSociale() {
    }

    /**
     *
     * @return le sousTraitant
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne
    @JoinColumn(name = "id_collaborateur", nullable = false)
    public Collaborator getCollaborator() {
        return collaborator;
    }

    public void setCollaborator(Collaborator collaborator) {
        this.collaborator = collaborator;
    }

    @Column(name = "raison_sociale", nullable = false)
    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raison_sociale) {
        this.raisonSociale = raison_sociale;
    }

    /**
     * Convert RaisonSocial entity to JSON
     *
     * @return RaisonSocialeAsJson
     */
    public RaisonSocialeAsJson toJson() {
        return new RaisonSocialeAsJson(this);
    }
}
