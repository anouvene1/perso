/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Justif;

import java.util.List;

/**
 * The interface Justif dao.
 */
public interface JustifDAO extends LongKeyDAO<Justif> {

    /**
     * Supprimer un justificatif
     *
     * @param pJustif le justificatif à supprimer
     * @return le message de résultat (OK si tout s'est bien passé, l'erreur sinon)
     */
    String deleteFrais(Justif pJustif);

    /**
     * Trouve tous les justificatifs d'un frais
     *
     * @param pIdFrais identifiant du frais
     * @return liste de ses justificatifs
     */
    List<Justif> findJustifByIdFrais(Long pIdFrais);


}
