/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Mission.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_mission")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class    Mission implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_COLLABORATEUR = "collaborateur";


    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_SOUSTRAITANT = "sousTraitant";

    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";

    /**
     * The constant PROP_DATEDEBUT.
     */
    public static final String PROP_DATEDEBUT = "dateDebut";
    public static final String SQL_DATE_DEBUT = "date_debut";

    /**
     * The constant PROP_DATEFIN.
     */
    public static final String PROP_DATEFIN = "dateFin";
    public static final String SQL_DATE_FIN = "date_fin";

    /**
     * The constant PROP_LISTE_MISSION_FORFAIT.
     */
    public static final String PROP_LISTE_MISSION_FORFAIT = "listMissionForfaits";

    /**
     * The constant PROP_ISDEFAULT.
     */
    public static final String PROP_ISDEFAULT = "isDefault";

    /**
     * The constant PROP_TYPEMISSION.
     */
    public static final String PROP_TYPEMISSION = "typeMission";

    /**
     * The constant PROP_RESP_CLIENT.
     */
    public static final String PROP_RESP_CLIENT = "resp_client";

    /**
     * The constant PROP_CLIENT.
     */
    public static final String PROP_CLIENT = "client";

    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";

    /**
     * The constant PROP_COMMANDES.
     */
    public static final String PROP_COMMANDES = "commandes";

    /**
     * The constant PROP_LISTE_PERIMETRE_MISSION.
     */
    public static final String PROP_LISTE_PERIMETRE_MISSION = "perimetre_mission";

    /**
     * The constante PROP_LISTE_LINK_EVENEMENT_TIMESHEET
     */
    public static final String PROP_LISTE_LINK_EVENEMENT_TIMESHEET="linkEvenementTimesheet";

    public static final Long MISSION_CLIENT_TYPE_ID = 5L;

    private static final long serialVersionUID = -1319603069615603284L;
    private Long id;
    private Collaborator collaborateur;
    private String mission;
    private Date dateDebut;
    private Date dateFin;
    private List<LinkEvenementTimesheet> linkEvenementTimesheetLines = new ArrayList<>(0);
    private List<LinkMissionForfait> listMissionForfaits = new ArrayList<>(0);
    private List<Commande> commandes = new ArrayList<>(0);
    private int isDefault;
    private TypeMission typeMission;
    private String description;
    private String suivi;
    private Float duree_hebdo;
    private ContactClient resp_client;
    private Client client;
    private Etat etat;
    private Float tjm;
    private OutilsInterne outilsInterne;
    private List<LinkPerimetreMission> listPerimetreMission = new ArrayList<>(0);
    private Agence agence;
    private Collaborator manager;


    /**
     * Constructeur de la classe Mission
     *
     * @param collaborator               the p collaborateur
     * @param pMission                     the p mission
     * @param pDateDebut                   the p date debut
     * @param pDateFin                     the p date fin
     * @param pLinkEvenementTimesheetLines the p link evenement timesheet lines
     * @param pListMissionForfaits         the p list mission forfaits
     * @param pIsDefault                   the p is default
     * @param pTypeMission                 the p type mission
     * @param pDescription                 the p description
     * @param pSuivi                       the p suivi
     * @param pDuree_hebdo                 the p duree hebdo
     * @param pResp_client                 the p resp client
     * @param pResp_facturation            the p resp facturation
     * @param pClient                      the p client
     * @param pCommandes                   the p commandes
     * @param pTjm                         the p tjm
     * @param listPerimetreMission         the list perimetre mission
     * @param pAgence                       the p agence
     * @param manager                     manager
     * @param pOutilsInterne                outils interne
     */
    public Mission(Collaborator collaborator, String pMission, Date pDateDebut, Date pDateFin,
                   List<LinkEvenementTimesheet> pLinkEvenementTimesheetLines, List<LinkMissionForfait> pListMissionForfaits, int pIsDefault, TypeMission pTypeMission,
                   String pDescription, String pSuivi, Float pDuree_hebdo, ContactClient pResp_client, ContactClient pResp_facturation, Client pClient,
                   List<Commande> pCommandes, Float pTjm, OutilsInterne pOutilsInterne, List<LinkPerimetreMission> listPerimetreMission, Agence pAgence, Collaborator manager) {
        this.collaborateur = collaborator;
        this.mission = pMission;
        this.dateDebut = pDateDebut;
        this.dateFin = pDateFin;
        this.linkEvenementTimesheetLines = pLinkEvenementTimesheetLines;
        this.listMissionForfaits = pListMissionForfaits;
        this.isDefault = pIsDefault;
        this.typeMission = pTypeMission;
        this.description = pDescription;
        this.suivi = pSuivi;
        this.duree_hebdo = pDuree_hebdo;
        this.resp_client = pResp_client;
//        this.resp_facturation = pResp_facturation;
        this.client = pClient;
        this.commandes = pCommandes;
        this.tjm = pTjm;
        this.outilsInterne = pOutilsInterne;
        this.listPerimetreMission = listPerimetreMission;
        this.agence = pAgence;
        this.manager = manager;
    }


    /**
     * Constructeur par defaut de la classe Mission
     */
    public Mission() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    @ManyToOne
    @JoinColumn(name = "id_collaborateur", nullable = true)
    public Collaborator getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborator the collaborateur to set
     */
    public void setCollaborateur(Collaborator collaborator) {
        this.collaborateur = collaborator;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    @Column(name = "mission", nullable = false)
    public String getMission() {
        return this.mission;
    }

    /**
     * Sets mission.
     *
     * @param pNom the mission to set
     */
    public void setMission(String pNom) {
        this.mission = pNom;
    }

    /**
     * Gets date debut.
     *
     * @return dateDebut date debut
     */
    @Column(name = "date_debut", nullable = false)
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param pDateDebut the dateDebut to set
     */
    public void setDateDebut(Date pDateDebut) {

        this.dateDebut = pDateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return dateFin date fin
     */
    @Column(name = "date_fin", nullable = false)
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param pDateFin the dateFin to set
     */
    public void setDateFin(Date pDateFin) {
        this.dateFin = pDateFin;
    }


    /**
     * Gets link evenement timesheet line.
     *
     * @return la liste des TimesheetLines avec cette mission
     */
    @OneToMany(mappedBy = "mission")
    public List<LinkEvenementTimesheet> getLinkEvenementTimesheetLine() {
        return linkEvenementTimesheetLines;
    }

    /**
     * Sets link evenement timesheet line.
     *
     * @param pLinkEvenementTimesheetLines the p link evenement timesheet lines
     */
    public void setLinkEvenementTimesheetLine(List<LinkEvenementTimesheet> pLinkEvenementTimesheetLines) {
        this.linkEvenementTimesheetLines = pLinkEvenementTimesheetLines;
    }


    /**
     * Gets is default.
     *
     * @return the isDefault
     */
    @Column(name = "isDefault", nullable = false)
    public int getIsDefault() {
        return isDefault;
    }

    /**
     * Sets is default.
     *
     * @param pIsDefault the isDefault to set
     */
    public void setIsDefault(int pIsDefault) {

        isDefault = pIsDefault;

    }

    /**
     * To json mission as json.
     *
     * @return the mission as json
     */
    public MissionAsJson toJson() {
        return new MissionAsJson(this);
    }

    /**
     * Gets list mission forfaits.
     *
     * @return mission list
     */
    @OneToMany(mappedBy = "mission")
    public List<LinkMissionForfait> getListMissionForfaits() {
        return listMissionForfaits;
    }

    /**
     * Sets list mission forfaits.
     *
     * @param pListMissionForfaits the p list mission forfaits
     */
    public void setListMissionForfaits(List<LinkMissionForfait> pListMissionForfaits) {
        listMissionForfaits = pListMissionForfaits;
    }

    /**
     * Gets type mission.
     *
     * @return the typeMission
     */
    @ManyToOne
    @JoinColumn(name = "id_type_mission", nullable = false)
    public TypeMission getTypeMission() {
        return typeMission;
    }

    /**
     * Sets type mission.
     *
     * @param pTypeMission the p type mission
     */
    public void setTypeMission(TypeMission pTypeMission) {
        typeMission = pTypeMission;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    @Column(name = "description", nullable = true)
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets suivi.
     *
     * @return the suivi
     */
    @Column(name = "suivi", nullable = true)
    public String getSuivi() {
        return suivi;
    }

    /**
     * Sets suivi.
     *
     * @param suivi the suivi
     */
    public void setSuivi(String suivi) {
        this.suivi = suivi;
    }

    /**
     * Gets duree hebdo.
     *
     * @return the duree_hebdo
     */
    @Column(name = "duree_hebdo", nullable = true)
    public Float getDuree_hebdo() {
        return duree_hebdo;
    }

    /**
     * Sets duree hebdo.
     *
     * @param duree_hebdo the duree hebdo
     */
    public void setDuree_hebdo(Float duree_hebdo) {
        this.duree_hebdo = duree_hebdo;
    }

    /**
     * Gets resp client.
     *
     * @return the resp_client
     */
    @ManyToOne
    @JoinColumn(name = "id_resp_client", nullable = true)
    public ContactClient getResp_client() {
        return resp_client;
    }

    /**
     * Sets resp client.
     *
     * @param resp_client the resp client
     */
    public void setResp_client(ContactClient resp_client) {
        this.resp_client = resp_client;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    @ManyToOne
    @JoinColumn(name = "id_client", nullable = true)
    public Client getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(Client client) {
        this.client = client;
    }


    /**
     * Gets etat.
     *
     * @return the etat
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_etat", nullable = false)
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat
     */
    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
     * Gets tjm.
     *
     * @return the tjm
     */
    @Column(name = "tjm")
    public Float getTjm() {
        return tjm;
    }

    /**
     * Sets tjm.
     *
     * @param tjm the tjm
     */
    public void setTjm(Float tjm) {
        this.tjm = tjm;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_outils", nullable = false)
    public OutilsInterne getOutilsInterne() {
        return outilsInterne;
    }

    public void setOutilsInterne(OutilsInterne outilsInterne) {
        this.outilsInterne = outilsInterne;
    }

    /**
     * Gets commandes.
     *
     * @return commande list
     */
    @OneToMany(mappedBy = "mission", fetch = FetchType.LAZY)
    public List<Commande> getCommandes() {
        return commandes;
    }

    /**
     * Sets commandes.
     *
     * @param pCommandes the p commandes
     */
    public void setCommandes(List<Commande> pCommandes) {
        this.commandes = pCommandes;
    }

    /**
     * Gets list perimetre mission.
     *
     * @return perimetre mission list
     */
    @OneToMany(mappedBy = "mission", cascade = CascadeType.ALL)
    public List<LinkPerimetreMission> getListPerimetreMission() {
        return listPerimetreMission;
    }

    /**
     * Sets list perimetre mission.
     *
     * @param pListPerimetreMission the p list perimetre mission
     */
    public void setListPerimetreMission(List<LinkPerimetreMission> pListPerimetreMission) {
        this.listPerimetreMission = pListPerimetreMission;
    }

    /**
     * Gets agence.
     *
     * @return agence
     */
    @ManyToOne
    @JoinColumn(name = "id_agence", nullable = false)
    public Agence getAgence() { return agence; }

    /**
     * Sets agence.
     *
     * @param pAgence the agence
     */
    public void setAgence(Agence pAgence) {
        this.agence = pAgence;
    }


    public boolean equals(Mission obj){
        // ce n'est pas une bonnée idée ce comparer des Long en utilisant '=='
        // https://stackoverflow.com/a/20542511/1761772
        return this.id.equals(obj.id);
    }

    /**
     * get the manager
     * @return manager
     */
    @ManyToOne
    @JoinColumn(name = "id_manager", nullable = true)
    public Collaborator getManager() { return manager; }

    /**
     *  set manager
     * @param manager the manager
     */
    public void setManager(Collaborator manager) {this.manager = manager;}

    public int compareToByMission(Mission mission) {
        String nom_mission1 = this.getMission();
        String nom_mission2 = mission.getMission();
        nom_mission1 = nom_mission1.replace(" ", "");
        nom_mission2 = nom_mission2.replace(" ", "");
        int result = nom_mission1.compareToIgnoreCase(nom_mission2);
        return result;
    }

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public String toString() {
        return "Mission d: " + df.format(dateDebut) + ", f:" + df.format(dateFin);
    }
}
