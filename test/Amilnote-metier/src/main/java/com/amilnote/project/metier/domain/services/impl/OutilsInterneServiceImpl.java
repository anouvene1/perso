package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.OutilsInterneDAO;
import com.amilnote.project.metier.domain.dao.impl.OutilsInterneDAOImpl;
import com.amilnote.project.metier.domain.entities.OutilsInterne;
import com.amilnote.project.metier.domain.entities.json.OutilsInterneAsJson;
import com.amilnote.project.metier.domain.services.OutilsInterneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("outilsInterneService")
public class OutilsInterneServiceImpl extends AbstractServiceImpl<OutilsInterne, OutilsInterneDAOImpl> implements OutilsInterneService {

    private OutilsInterneDAO outilsInterneDAO;

    @Autowired
    public OutilsInterneServiceImpl(OutilsInterneDAO outilsInterneDAO) {
        this.outilsInterneDAO = outilsInterneDAO;
    }

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public OutilsInterneDAOImpl getDAO() {
        return (OutilsInterneDAOImpl) outilsInterneDAO;
    }

    /**
     * {@linkplain OutilsInterneService#getAllOutilsInterne()}
     */
    @Override
    public List<OutilsInterne> getAllOutilsInterne() {
        return outilsInterneDAO.loadAll();
    }


    /**
     * {@linkplain OutilsInterneService#getAllOutilsInterneAsJson()}
     */
    @Override
    public List<OutilsInterneAsJson> getAllOutilsInterneAsJson() {
        List<OutilsInterne> lListOutilsInterne = getAllOutilsInterne();
        List<OutilsInterneAsJson> lOutilsInterneAsJson = new ArrayList<>(lListOutilsInterne.size());

        for (OutilsInterne lOutilsInterne : lListOutilsInterne) {
            lOutilsInterneAsJson.add(lOutilsInterne.toJson());
        }
        return lOutilsInterneAsJson;
    }
}
