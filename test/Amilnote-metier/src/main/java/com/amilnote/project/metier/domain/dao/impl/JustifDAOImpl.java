/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.JustifDAO;
import com.amilnote.project.metier.domain.entities.Justif;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Justif dao.
 */
@Repository("JustifDAO")
public class JustifDAOImpl extends AbstractDAOImpl<Justif> implements JustifDAO {

    private static final Logger logger = LogManager.getLogger(JustifDAOImpl.class);

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Justif> getReferenceClass() {
        return Justif.class;
    }

    /**
     * {@linkplain JustifDAO#deleteFrais(Justif)}
     */
    @Override
    public String deleteFrais(Justif pJustif) {

        // récupération de la session
        Session lSession = getSessionFactory().getCurrentSession();

        // début de la transaction pour supprimer un justificatif
        try {
            Transaction lTransaction = lSession.beginTransaction();
            // suppression
            lSession.delete(pJustif);
            //lSession.flush();
            lTransaction.commit();
        } catch (HibernateException e) {
            logger.error("[delete fees] hibernate commit transaction error", e);
            return e.getMessage();
        }
        return "OK";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Justif> findJustifByIdFrais(Long pIdFrais) {
        Session session = getCurrentSession();
        Query query = session.createQuery("from Justif where id in (select justif from LinkFraisJustif where id_frais = " + pIdFrais.toString() + ")");
        session.flush();
        return query.list();
    }

}
