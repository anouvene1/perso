/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.MoyensPaiement;
import com.amilnote.project.metier.domain.entities.json.MoyensPaiementAsJson;

import java.util.List;

/**
 * The interface Moyens paiement dao.
 */
public interface MoyensPaiementDAO extends LongKeyDAO<MoyensPaiement> {

    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param paiementJson the paiement json
     * @return int int
     */
    int createOrUpdateMoyensPaiement(MoyensPaiementAsJson paiementJson);

    /**
     * Suppression de la commande
     *
     * @param paiement the paiement
     * @return string string
     */
    String deleteMoyensPaiement(MoyensPaiement paiement);

    /**
     * Find moyens paiement list.
     *
     * @return La liste de toutes les facture
     */
    List<MoyensPaiement> findMoyensPaiement();

}
