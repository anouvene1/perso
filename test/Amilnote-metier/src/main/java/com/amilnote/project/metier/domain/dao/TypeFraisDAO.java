/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.TypeFrais;

import java.util.List;

/**
 * The interface Type frais dao.
 */
public interface TypeFraisDAO extends LongKeyDAO<TypeFrais> {


    /**
     * Retourne tous les Types de frais
     *
     * @return List all type frais
     */
    List<TypeFrais> getAllTypeFrais();

    /**
     * Retourne un type de frais en fonction de l'id
     *
     * @param pId the p id
     * @return TypeFrais type frais by id
     */
    TypeFrais getTypeFraisById(long pId);
}
