/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Justif;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Justif as json.
 */
public class JustifAsJson {

    private Long id;
    private String descriptif;
    private String fichier;
    private List<FraisAsJson> frais = new ArrayList<FraisAsJson>();


    /**
     * Instantiates a new Justif as json.
     *
     * @param pJustif the p justif
     */
    public JustifAsJson(Justif pJustif) {
        super();
        id = pJustif.getId();
        descriptif = pJustif.getDescriptif();
        fichier = pJustif.getFichier();
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }


    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }


    /**
     * Gets descriptif.
     *
     * @return the descriptif
     */
    public String getDescriptif() {
        return descriptif;
    }


    /**
     * Sets descriptif.
     *
     * @param pDescriptif the descriptif to set
     */
    public void setDescriptif(String pDescriptif) {
        descriptif = pDescriptif;
    }


    /**
     * Gets fichier.
     *
     * @return the fichier
     */
    public String getFichier() {
        return fichier;
    }


    /**
     * Sets fichier.
     *
     * @param pFichier the fichier to set
     */
    public void setFichier(String pFichier) {
        fichier = pFichier;
    }


    /**
     * Gets frais.
     *
     * @return the frais
     */
    public List<FraisAsJson> getFrais() {
        return frais;
    }


    /**
     * Sets frais.
     *
     * @param pFrais the frais to set
     */
    public void setFrais(List<FraisAsJson> pFrais) {
        frais = pFrais;
    }


}
