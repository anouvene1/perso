/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.impl.EtatDAOImpl;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.json.EtatAsJson;
import com.amilnote.project.metier.domain.services.EtatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Etat service.
 */
@Service("etatService")
public class EtatServiceImpl extends AbstractServiceImpl<Etat, EtatDAOImpl> implements EtatService {

    @Autowired
    private EtatDAO etatDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public EtatDAOImpl getDAO() {
        return (EtatDAOImpl) etatDAO;
    }

    /**
     * {@linkplain EtatService#getAllEtat()}
     */
    @Override
    public List<Etat> getAllEtat() {
        return etatDAO.getAllEtat();
    }

    /**
     * {@linkplain EtatService#getEtatById(int)}
     */
    @Override
    public Etat getEtatById(int pId) {
        return etatDAO.getEtatById(pId);
    }

    /**
     * {@linkplain EtatService#getAllEtatAsJson()}
     */
    @Override
    public String getAllEtatAsJson() {
        List<Etat> lListEtat = etatDAO.getAllEtat();
        List<EtatAsJson> lEtatAsJson = new ArrayList<EtatAsJson>();

        for (Etat lEtat : lListEtat) {
            lEtatAsJson.add(new EtatAsJson(lEtat));
        }
        return this.writeJson(lEtatAsJson);
    }

    /**
     * {@linkplain EtatService#getEtatDomaine(String[])}
     */
    @Override
    public List<Etat> getEtatDomaine(String[] pListeEtat) {
        List<Etat> lEtatEtat = new ArrayList<Etat>();
        for (String idEtat : pListeEtat) {
            lEtatEtat.add(etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, idEtat));
        }
        return lEtatEtat;
    }

    /**
     * {@linkplain EtatService#getEtatDomaineAsJson(String[])}
     */
    @Override
    public String getEtatDomaineAsJson(String[] pListeEtat) {
        List<Etat> lListEtat = getEtatDomaine(pListeEtat);
        List<EtatAsJson> lEtatAsJson = new ArrayList<EtatAsJson>();

        for (Etat lEtat : lListEtat) {
            lEtatAsJson.add(new EtatAsJson(lEtat));
        }
        return this.writeJson(lEtatAsJson);
    }

    /**
     * {@linkplain EtatService#getEtatByCode(String)}
     */
    @Override
    public Etat getEtatByCode(String pEtat) {
        List<Etat> lListEtat = getAllEtat();
        Etat etatRetour = null;
        for (Etat tempEtat : lListEtat) {
            if (tempEtat.getCode().equals(pEtat)) {
                etatRetour = tempEtat;
            }
        }
        return etatRetour;
    }
}
