package com.amilnote.project.metier.domain.utils.enumerations;

/**
 * enumeration of comparison operators, for data search restrictions
 * @author gralandison
 */
public enum ComparisonOperator {
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN,
    LESS_THAN_OR_EQUAL,
    IN,
    NOT_IN,
    NOT,
    BETWEEN,
    IS_NULL,
    IS_NOT_NULL,
    LIKE,
    EXISTS
}
