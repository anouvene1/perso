/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.AbsenceDAO;
import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.LinkEvenementTimesheetDAO;
import com.amilnote.project.metier.domain.dao.MissionDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkEvenementCourtAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkEvenementTimesheetAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.services.CollaboratorService;
import com.amilnote.project.metier.domain.services.LinkEvenementTimesheetService;
import com.amilnote.project.metier.domain.services.RapportActivitesService;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * The type Link evenement timesheet service.
 */
@Service("linkEvenementTimesheetService")
public class LinkEvenementTimesheetServiceImpl extends AbstractServiceImpl<LinkEvenementTimesheet, LinkEvenementTimesheetDAO> implements LinkEvenementTimesheetService {

    private static final Logger logger = LogManager.getLogger(LinkEvenementTimesheetServiceImpl.class);

    private static final String MODIFICATION_ERROR = "Seules les absences à l'état brouillon ou refusé peuvent être modifiées.";
    private static final String CONNECTION_ERROR = "Les évenements doivent appartenir à la personne connectée.";
    private static final String EMPTY_EVENT_DELETE_ERROR = "Les évenements ne peuvent pas être vide.";
    private static final String EMPTY_LIST_EVENT_DELETE_ERROR = "La liste d'évenements ne comporte aucun élement.";
    private static final String DATE_EVENT_ERROR = "La date de l'event est incorrecte.";
    private static final String ID_MISSION_ERROR = "L'id de la mission est inconnu ou n'appartient pas à l'utilisateur connecté.";
    private static final String EMPTY_ABSENCE_TIME_ERROR = "Une absence doit avoir au moins une demi-journée.";
    private static final String COMMIT_ERROR = "erreur lors du commit.";
    private static final String ID_ABSENCE_TYPE_ERROR = "Id de typeAbsence inconnue";
    private static final String SELECTION_ERROR = "Aucune demi-journée sélectionné.";

    @Autowired
    private LinkEvenementTimesheetDAO linkEvenementTimesheetDAO;

    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private MissionDAO missionDAO;

    @Autowired
    private AbsenceDAO absenceDAO;

    @Autowired
    private EtatDAO etatDao;

    /**
     * Méthode de tri des évenementsTimesheets en fonction de la date et du moment de la journée
     *
     * @param pListEvent Liste d'évenements à trier
     */
    public static void sortByDateAndMomentJournee(List<LinkEvenementTimesheet> pListEvent) {
        Collections.sort(pListEvent, new Comparator<LinkEvenementTimesheet>() {
            @Override
            public int compare(LinkEvenementTimesheet event1, LinkEvenementTimesheet event2) {
                if (event1.getDate().before(event2.getDate())) {
                    return -1;
                } else if (event1.getDate().after(event2.getDate())) {
                    return 1;
                } else {
                    if (event1.getMomentJournee() < event2.getMomentJournee()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LinkEvenementTimesheetDAO getDAO() {
        return linkEvenementTimesheetDAO;
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findEventsBetweenDatesAsJson(Collaborator, String, String)}
     */
    @Override
    public String findEventsBetweenDatesAsJson(Collaborator collaborator, String pStart, String pEnd) throws JsonProcessingException {
        List<LinkEvenementTimesheetAsJson> lFullCalendarEventAsJson = null;
        List<LinkEvenementTimesheet> lLinkEvenement = null;
        try {
            lLinkEvenement = this.findEventsBetweenDates(collaborator, pStart, pEnd);

            lFullCalendarEventAsJson = new ArrayList<LinkEvenementTimesheetAsJson>();

            for (LinkEvenementTimesheet lLinkEvenementTimesheet : lLinkEvenement) {
                lLinkEvenementTimesheet.getCollaborator().setRaisonSociale(null);
                lFullCalendarEventAsJson.add(new LinkEvenementTimesheetAsJson(lLinkEvenementTimesheet));
            }

        } catch (Exception e) {
            logger.error("[find events] between dates {} : {}", pStart, pEnd, e);
        }
        return this.writeJson(lFullCalendarEventAsJson);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findEventsBetweenDates(Collaborator, String, String)}
     */
    @Override
    public List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, String pStart, String pEnd) throws JsonProcessingException {

        DateTime lSart = new DateTime(pStart).millisOfDay().withMinimumValue();
        DateTime lEnd = new DateTime(pEnd).millisOfDay().withMaximumValue();

        int differenceMAxAsInt = 60;
        int differenceAsInt = Days.daysBetween(lSart, lEnd).getDays();

        if (differenceAsInt > differenceMAxAsInt || differenceAsInt == 0) {
            lEnd = lSart.plusDays(differenceMAxAsInt);
        }

        List<LinkEvenementTimesheet> lLinkEvenement = linkEvenementTimesheetDAO.findEventsBetweenDates(collaborator, lSart, lEnd);
        return lLinkEvenement;
    }


    // ------------

    /**
     * {@linkplain LinkEvenementTimesheetService#findEventsBetweenDates(Collaborator, String, String)}
     */
    @Override
    public List<LinkEvenementTimesheet> findEventsBetweenDatesByDateTime(Collaborator collaborator, DateTime pStart, DateTime pEnd) {


        List<LinkEvenementTimesheet> lLinkEvenement = linkEvenementTimesheetDAO.findEventsBetweenDates(collaborator, pStart, pEnd);
        return lLinkEvenement;
    }
    // ---------------

    /**
     * {@linkplain LinkEvenementTimesheetService#findEventsBetweenDates(Collaborator, Date, Date)}
     */
    @Override
    public List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, Date pStart, Date pEnd) throws JsonProcessingException {
        return linkEvenementTimesheetDAO.findEventsBetweenDates(collaborator, pStart, pEnd);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findAllNotFinishEventOnlyDate(Mission)}
     */
    @Override
    public String findAllNotFinishEventOnlyDate(Mission pMission) {
        List<LinkEvenementTimesheet> lLinkEvenement = linkEvenementTimesheetDAO.findAllNotFinishEventOnlyDate(pMission);

        List<LinkEvenementCourtAsJson> lListEventAsJson = new ArrayList<>();

        for (LinkEvenementTimesheet lLinkEvenementTimesheet : lLinkEvenement) {
            lListEventAsJson.add(new LinkEvenementCourtAsJson(lLinkEvenementTimesheet));
        }

        return this.writeJson(lListEventAsJson);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findNbWorkedDaysByMissionAndPeriod(Mission, DateTime, DateTime)}
     */
    @Override
    public List<LinkEvenementTimesheet> findNbWorkedDaysByMissionAndPeriod(Mission pMission, DateTime lSart, DateTime lEnd) {
        return linkEvenementTimesheetDAO.findNbWorkedDaysByMissionAndPeriod(pMission, lSart.toDate(), lEnd.toDate());
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findNbDaysMissionPeriod(Mission, DateTime, DateTime)}
     */
    @Override
    public float findNbDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode) {
        List<LinkEvenementTimesheet> lLinkEvenement = linkEvenementTimesheetDAO.findDaysMissionPeriod(pMission, pDebutPeriode, pFinPeriode);
        return lLinkEvenement.size();
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findNbDaysForForfaitDeplacement(Mission, DateTime, DateTime)}
     */
    @Override
    public float findNbDaysForForfaitDeplacement(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode) {
        List<LinkEvenementTimesheet> lLinkEvenement = linkEvenementTimesheetDAO.findNbDaysForForfaitDeplacement(pMission, pDebutPeriode, pFinPeriode);
        return lLinkEvenement.size();
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findDaysMissionPeriod(Mission, DateTime, DateTime)}
     */
    @Override
    public List<LinkEvenementTimesheet> findDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode) {
        return linkEvenementTimesheetDAO.findDaysMissionPeriod(pMission, pDebutPeriode, pFinPeriode);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findById(Long)}
     */
    @Override
    public LinkEvenementTimesheet findById(Long pIdLinkEvtTs) {
        return linkEvenementTimesheetDAO.findUniqEntiteByProp("id", pIdLinkEvtTs);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findByAbsence(Absence)}
     */
    @Override
    public List<LinkEvenementTimesheet> findByAbsence(Absence pAbsence) {
        return linkEvenementTimesheetDAO.findByAbsence(pAbsence);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#updateAbsenceOnLinkEvenementTimesheet(String, Collaborator)}
     */
    @Override
    public String updateAbsenceOnLinkEvenementTimesheet(String listEvents, Collaborator collaborator) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        AbsenceAsJson absAsJson = objectMapper.readValue(listEvents, AbsenceAsJson.class);

        Session session = linkEvenementTimesheetDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        TypeAbsence lTypeAbsence = absenceService.findById(absAsJson.getTypeAbsence().getId());

        if (lTypeAbsence == null) {
            return ID_ABSENCE_TYPE_ERROR;
        }

        if (absAsJson.getListLinkEvenementTimesheetAsJson().size() == 0) {
            return SELECTION_ERROR;
        }

        Absence absence = null;

        if (null == absAsJson.getId()) {

            absence = new Absence();

            absence.setCollaborateur(collaborator);

            absence.setTypeAbsence(lTypeAbsence);

            DateTime lStartAbsence = new DateTime(absAsJson.getDateDebut()).millisOfDay().withMinimumValue();
            absence.setDateDebut(lStartAbsence.toDate());

            DateTime lEndAbsence = new DateTime(absAsJson.getDateFin()).millisOfDay().withMinimumValue();
            absence.setDateFin(lEndAbsence.toDate());

            String codeStatutCollaborator = collaborator.getStatut().getCode();
            Etat lEtat;

            switch (codeStatutCollaborator) {
                case "STT":
                    lEtat = etatDao.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_COMMENCE);
                    break;
                default:
                    lEtat = etatDao.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);
            }

            absence.setEtat(lEtat);

            session.save(absence);

            LinkEvenementTimesheet event;

            for (LinkEvenementTimesheetAsJson eventAsJson : absAsJson.getListLinkEvenementTimesheetAsJson()) {

                if (eventAsJson.getId() != null) {

                    event = linkEvenementTimesheetDAO.get(eventAsJson.getId());
                    Etat lEtatRE = etatDao.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_REFUSE);
                    if (null != event
                            && (null == event.getAbsence()
                            || (null != event.getAbsence() &&
                            event.getAbsence().getEtat().equals(lEtatRE)))) {

                        absence.getLinkEvenementTimesheets().add(event);

                        event.setAbsence(absence);
                        event.setMission(null);

                        for (Frais lFrais : event.getListFrais()) {
                            session.delete(lFrais);
                        }
                    }

                } else {

                    DateTime lStartLinkEvent = new DateTime(eventAsJson.getStart());
                    DateTime lEndLinkEvent = new DateTime(eventAsJson.getEnd());

                    LinkEvenementTimesheet lTmpLinkEvent = new LinkEvenementTimesheet();
                    String momentJourneeIsOk = lTmpLinkEvent.setMomentJournee(lStartLinkEvent, lEndLinkEvent);

                    if (momentJourneeIsOk == "ok") {
                        event = linkEvenementTimesheetDAO.findByDateAndMomentJourneeAndCollaborator(
                                lStartLinkEvent,
                                lTmpLinkEvent.getMomentJournee(),
                                collaborator);

                        if (null == event) {

                            LinkEvenementTimesheet lNewLinkEvent = new LinkEvenementTimesheet();
                            lNewLinkEvent.setMomentJournee(lTmpLinkEvent.getMomentJournee());

                            lNewLinkEvent.setDate(lStartLinkEvent.toDate());

                            lNewLinkEvent.setAbsence(absence);
                            lNewLinkEvent.setCollaborator(collaborator);
                            absence.getLinkEvenementTimesheets().add(lNewLinkEvent);
                            session.save(lNewLinkEvent);

                        } else if (null == event.getAbsence()) {
                            event.setAbsence(absence);
                            event.setMission(null);

                            absence.getLinkEvenementTimesheets().add(event);
                        }
                    }
                }
            }
            session.flush();
            absence.setNbJours(absence.calculNbJours());

        } else {

            absence = (Absence) session.get(Absence.class, absAsJson.getId());
            if (absence == null) {
                transaction.rollback();
                return "absence.id[ " + absAsJson.getId() + "] inconnue ";
            }

            String absCode = absence.getEtat().getCode();

            if ((!absCode.equals(Etat.ETAT_BROUILLON_CODE)) && (!absCode.equals(Etat.ETAT_REFUSE))) {
                transaction.rollback();
                return MODIFICATION_ERROR;
            }

            absence.setTypeAbsence(lTypeAbsence);

            for(LinkEvenementTimesheet event: absence.getLinkEvenementTimesheets()) {
                if (null != event.getId()) {
                    event.setMission(null);
                }
            }
        }

        session.flush();

        if (absence.getNbJours() == 0) {
            transaction.rollback();
            return EMPTY_ABSENCE_TIME_ERROR;
        }

        transaction.commit();

        if (transaction.wasCommitted()) {
            mergeAbsences(absence, collaborator, session);
            return "";
        } else {
            transaction.rollback();
            return COMMIT_ERROR;
        }
    }

    /**
     * Fusion de la nouvelle absence créée avec l'absence précédente ou suivante dans le cas où
     * 1 : On a le même type d'absence
     * 2 : Les absences sont à l'état Brouillon
     *
     * @param absence        La nouvelle absence créée
     * @param collaborator Le collaborateur courant ayant créé son absence
     * @param session        La session courante
     */
    private void mergeAbsences(Absence absence, Collaborator collaborator, Session session) {
        Transaction transaction = session.beginTransaction();

        //Dans le cas d'une fusion, on modifie la date de debut/fin de la nouvelle absence
        //et on modifie l'id de l'absence dans le linkEvenementTimesheet

        //Premier cas où la nouvelle absence est une demi journée.
        //On la fusionne avec l'absence précédente ou suivante.
        if (absence.getNbJours() == 0.5) {
            int momentJourneeAbsence = (this.findByAbsence(absence)).get(0).getMomentJournee();

            //On regarde si on peut la fusionner avec l'autre demi-journée (même date, même type)
            List<Absence> listAbsences = absenceService.getAbsencesByCollaboratorAndEtatBrouillon(collaborator);

            for (Absence oldAbsence : listAbsences) {
                if (oldAbsence.getTypeAbsence() == absence.getTypeAbsence() && oldAbsence.getId() != absence.getId()) {
                    //On a le même type d'absence et pas la même absence
                    //On cherche la date de la veille du début de la oldAbsence
                    Date dateDebOldAbsence = oldAbsence.getDateDebut();
                    Calendar datePreviousOldAbsenceCalendar = Calendar.getInstance();
                    datePreviousOldAbsenceCalendar.setTime(dateDebOldAbsence);
                    datePreviousOldAbsenceCalendar.add(Calendar.DATE, -1);
                    //On cherche la date du lendemande de la fin de la oldAbsence
                    Date dateFinOldAbsence = oldAbsence.getDateFin();
                    Calendar dateAfterOldAbsenceCalendar = Calendar.getInstance();
                    dateAfterOldAbsenceCalendar.setTime(dateFinOldAbsence);
                    dateAfterOldAbsenceCalendar.add(Calendar.DATE, +1);

                    //On récupère la dernière demi-journée de la oldAbsence
                    List<LinkEvenementTimesheet> listOldAbsenceEvent = findByAbsence(oldAbsence);

                    try {
                        Date date = listOldAbsenceEvent.get(0).getDate();

                        LinkEvenementTimesheet saveDernierOldEvent = listOldAbsenceEvent.get(0);    //Evenement correspondant à la fin de la oldAbsence (Jour+AM ou PM)

                        //On récupère la date de fin de la oldAbsenceEvent
                        for (LinkEvenementTimesheet linkOldEvent : listOldAbsenceEvent) {
                            Date dateEvent = linkOldEvent.getDate();
                            //Si la dateEvent est après la plus grande date enregistrée, on l'enregistre
                            if (dateEvent.after(date)) {
                                saveDernierOldEvent = linkOldEvent;
                            }
                            //Si la dateEvent est la même que la date enregistrée, on regarde si on est le AM ou PM
                            else if (!dateEvent.before(date) && !dateEvent.after(date)) {
                                if (linkOldEvent.getMomentJournee() == 1) {
                                    saveDernierOldEvent = linkOldEvent;
                                }
                            }
                        }

                        //On récupère la première demi-journée de la oldAbsence
                        LinkEvenementTimesheet savePremierOldEvent = listOldAbsenceEvent.get(0);    //Evenement correspondant à la fin de la oldAbsence (Jour+AM ou PM)

                        //On récupère la date de fin de la oldAbsenceEvent
                        for (LinkEvenementTimesheet linkOldEvent : listOldAbsenceEvent) {
                            Date dateEvent = linkOldEvent.getDate();
                            //Si la dateEvent est après la plus grande date enregistrée, on l'enregistre
                            if (dateEvent.before(date)) {
                                savePremierOldEvent = linkOldEvent;
                            }
                            //Si la dateEvent est la même que la date enregistrée, on regarde si on est le AM ou PM
                            else if (!dateEvent.before(date) && !dateEvent.after(date)) {
                                if (linkOldEvent.getMomentJournee() == 0) {
                                    savePremierOldEvent = linkOldEvent;
                                }
                            }
                        }

                        //Récap :
                        //saveDernierOldEvent = la dernière demi-journée de l'ancienne absence de même type que la nouvelle absence et au statut brouillon
                        //savePremierOldEvent = la première demi-journée de l'ancienne absence de même type que la nouvelle absence et au statut brouillon

                        //Si la dernière demi-journée est un après midi alors on regarde si la nouvelle absence correspond à la matinée du lendemain
                        //Si c'est le cas, on fusionne
                        if (saveDernierOldEvent.getMomentJournee() == 1) {
                            if (absence.getDateDebut().getTime() == dateAfterOldAbsenceCalendar.getTimeInMillis() && momentJourneeAbsence == 0) {
                                absence.setDateDebut(oldAbsence.getDateDebut());
                                absence.setNbJours(absence.getNbJours() + oldAbsence.getNbJours());

                                //On met ensuite à jour la table link_evenement_timesheet avec le nouvel id de l'absence.
                                List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, oldAbsence);
                                for (LinkEvenementTimesheet event : listEvent) {
                                    event.setAbsence(absence);
                                    session.flush();
                                }
                                session.delete(oldAbsence);
                                session.flush();

                                if(!transaction.wasCommitted()) {
                                    transaction.commit();
                                }
                            }
                        }
                        //Sinon si la dernière demi-journée est un matin alors on regarde si la nouvelle absence correspond à l'apres-midi de la même journée
                        //Si c'est le cas, on fusionne
                        else if (saveDernierOldEvent.getMomentJournee() == 0) {
                            if (oldAbsence.getDateFin().getTime() == absence.getDateDebut().getTime()) {
                                absence.setNbJours(absence.getNbJours() + oldAbsence.getNbJours());

                                //On met ensuite à jour la table link_evenement_timesheet avec le nouvel id de l'absence.
                                List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, oldAbsence);
                                for (LinkEvenementTimesheet event : listEvent) {
                                    event.setAbsence(absence);
                                    session.flush();
                                }
                                session.delete(oldAbsence);
                                session.flush();

                                if(!transaction.wasCommitted()) {
                                    transaction.commit();
                                }
                            }
                        }

                        //Si la première demi-journée est un matin alors on regarde si la nouvelle absence correspond à un après midi la veille
                        //Si c'est le cas, on fusionne
                        if (savePremierOldEvent.getMomentJournee() == 0) {
                            if (absence.getDateFin().getTime() == datePreviousOldAbsenceCalendar.getTimeInMillis() && momentJourneeAbsence == 1) {
                                absence.setDateFin(oldAbsence.getDateFin());
                                absence.setNbJours(absence.getNbJours() + oldAbsence.getNbJours());

                                //On met ensuite à jour la table link_evenement_timesheet avec le nouvel id de l'absence.
                                List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, oldAbsence);
                                for (LinkEvenementTimesheet event : listEvent) {
                                    event.setAbsence(absence);
                                    session.flush();
                                }
                                session.delete(oldAbsence);
                                session.flush();

                                if(!transaction.wasCommitted()) {
                                    transaction.commit();
                                }
                            }
                        }
                        //Sinon si la première demi-journée est un après-midi alors on regarde si la nouvelle absence correspond à un matin du même jour
                        //Si c'est le cas, on fusionne
                        else if (savePremierOldEvent.getMomentJournee() == 1) {
                            if (absence.getDateFin().getTime() == oldAbsence.getDateDebut().getTime() && momentJourneeAbsence == 0) {
                                absence.setNbJours(absence.getNbJours() + oldAbsence.getNbJours());

                                //On met ensuite à jour la table link_evenement_timesheet avec le nouvel id de l'absence.
                                List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, oldAbsence);
                                for (LinkEvenementTimesheet event : listEvent) {
                                    event.setAbsence(absence);
                                    session.flush();
                                }
                                session.delete(oldAbsence);
                                session.flush();

                                if(!transaction.wasCommitted()) {
                                    transaction.commit();
                                }
                            }
                        }

                    } catch(IndexOutOfBoundsException exception) {
                        logger.error("Old absences events empty {listOldAbsenceEvent} ", listOldAbsenceEvent);
                    }

                }
            }
        }

        //Une fois la potentielle demi-journée fusionnée, on fusionne la journée complète ou le groupe de jour.
        if (absence.getNbJours() >= 1) {
            mergeAbsenceWithPreviousAndNext(absence, collaborator, session, transaction);
        }
    }

    private void mergeAbsenceWithPreviousAndNext(Absence absenceToMerge, Collaborator collaborator, Session session, Transaction transaction) {
        // Retrieve start and end date of absence
        Date dateDebAbsence = absenceToMerge.getDateDebut();
        Date dateFinAbsence = absenceToMerge.getDateFin();

        // Calendar treatment
        Calendar calendar = Calendar.getInstance();
        Timestamp dateDebAbsenceTimeStamp = manageCalendar(calendar, dateDebAbsence, true);
        Timestamp dateFinAbsenceTimeStamp = manageCalendar(calendar, dateFinAbsence, false);

        //On compare la date trouvée (après retrait des weekend et jours fériés) avec les autres absences enregistrées
        //Dans le cas où on a une concordance, on "fusionne" les deux absences.
        //(Même type d'absence, état Brouillon et date de fin=date de début)
        List<Absence> listAbsence = absenceDAO.findListEntitesByProp(Absence.PROP_COLLABORATEUR, collaborator);
        for (Absence absence : listAbsence) {
            mergeAbsenceDate(session, transaction, absence, absenceToMerge, dateDebAbsenceTimeStamp, true);
            mergeAbsenceDate(session, transaction, absence, absenceToMerge, dateFinAbsenceTimeStamp, false);
        }
    }

    /**
     * Manage the calendar occording to start or end date of absence and exclude not working days
     * @param calendar The calendar
     * @param dateAbsence Absence date to include
     * @param isStartDateOfAbsenceToMerge Start absence date indicator
     * @return Timestamp The start or end date in timestamp format
     */
    private Timestamp manageCalendar(Calendar calendar, Date dateAbsence, boolean isStartDateOfAbsenceToMerge) {
        calendar.setTime(dateAbsence);

        // Add or remove one day (without sunday/saturday/holidays).
        if(isStartDateOfAbsenceToMerge) {
            calendar.add(Calendar.DATE, -1);
        } else {
            calendar.add(Calendar.DATE, +1);
        }

        Date dateTimeAbsence = new DateTime(calendar).toDate();
        List<Date> listHolidays = getHolidays(calendar.get(Calendar.YEAR));
        for (Date holidays : listHolidays) {
            if (!holidays.after(dateTimeAbsence) && !holidays.before(dateTimeAbsence)) {
                if(isStartDateOfAbsenceToMerge) {
                    calendar.add(Calendar.DATE, -1);
                } else {
                    calendar.add(Calendar.DATE, +1);
                }
            }
        }

        boolean isWorkDay = false;
        while (!isWorkDay) {
            if ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
                isWorkDay = false;
                if(isStartDateOfAbsenceToMerge) {
                    calendar.add(Calendar.DATE, -1);
                } else {
                    calendar.add(Calendar.DATE, +1);
                }
            } else {
                isWorkDay = true;
                dateTimeAbsence = new DateTime(calendar).toDate();
                for (Date holidays : listHolidays) {
                    if (!holidays.after(dateTimeAbsence) && !holidays.before(dateTimeAbsence)) {
                        if(isStartDateOfAbsenceToMerge) {
                            calendar.add(Calendar.DATE, -1);
                        } else {
                            calendar.add(Calendar.DATE, +1);
                        }
                    }
                }
            }
        }

        calendar.set(calendar.HOUR_OF_DAY, 0);
        return new Timestamp(calendar.getTimeInMillis());
    }

    /**
     * Merge absence treatment
     * @param session The current session
     * @param transaction The current transaction
     * @param oldAbsence The absences are already fixed
     * @param absenceToMerge the new absences to merge
     * @param absenceDateToMerge The new start/end absence date to merge with the old star/end absence date
     * @param isStartDateOfAbsenceToMerge The indicator start date of new the absence
     */
    private void mergeAbsenceDate(Session session, Transaction transaction, Absence oldAbsence, Absence absenceToMerge,
                                  Timestamp absenceDateToMerge, boolean isStartDateOfAbsenceToMerge) {
        Date oldAbsenceDate;
        if(isStartDateOfAbsenceToMerge) {
            oldAbsenceDate = oldAbsence.getDateFin();
        } else {
            oldAbsenceDate = oldAbsence.getDateDebut();
        }

        if ((!oldAbsenceDate.before(absenceDateToMerge) && !oldAbsenceDate.after(absenceDateToMerge))
                && oldAbsence.getTypeAbsence().getId() == absenceToMerge.getTypeAbsence().getId()
                && Etat.ETAT_BROUILLON_CODE.equals(oldAbsence.getEtat().getCode())) {

            //On met à jour l'absence avec la date de fin ou de début de la nouvelle et le nb de jours.
            if(isStartDateOfAbsenceToMerge) {
                oldAbsence.setDateFin(absenceToMerge.getDateFin());
            } else {
                oldAbsence.setDateDebut(absenceToMerge.getDateFin());
            }
            oldAbsence.setNbJours(oldAbsence.getNbJours() + absenceToMerge.getNbJours());

            //On met ensuite à jour la table link_evenement_timesheet avec le nouvel id de l'absence.
            List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, absenceToMerge);
            for (LinkEvenementTimesheet event : listEvent) {
                event.setAbsence(oldAbsence);
                session.flush();
            }

            session.delete(absenceToMerge);
            session.flush();
            if(!transaction.wasCommitted()) transaction.commit();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateMissionOnLinkEvenementTimesheet(String listEvents, Collaborator collaborator)
            throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        MissionAsJson missAsJson = objectMapper.readValue(listEvents, MissionAsJson.class);

        Session session = linkEvenementTimesheetDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String errorCheckDateAndSetMission = "La date de la demi-journée n'est pas comprise dans la periode de la mission choisie.";

        Mission mission = missionDAO.get(missAsJson.getId());

        if (null != mission && mission.getCollaborateur().equals(collaborator)) {

            LinkEvenementTimesheet event;

            for (LinkEvenementTimesheetAsJson eventAsJson : missAsJson.getListLinkEvenementTimesheetAsJson()) {

                event = null;
                if (null != eventAsJson.getId()) {
                    event = linkEvenementTimesheetDAO.get(eventAsJson.getId());
                }

                if (null != event) {

                    mission.getLinkEvenementTimesheetLine().add(event);
                    if (!event.checkDateAndSetMission(mission)) {
                        transaction.rollback();
                        return errorCheckDateAndSetMission;
                    }

                    Absence absence =  event.getAbsence();

                    if(absence != null && absence.getCollaborateur().equals(collaborator)) {
                        session.delete(absence);
                    }

                    event.setAbsence(null);
                } else {

                    DateTime eventAsJsonStart = new DateTime(eventAsJson.getStart());
                    DateTime eventAsJsonEnd = new DateTime(eventAsJson.getEnd());

                    LinkEvenementTimesheet lTmpLinkEvent = new LinkEvenementTimesheet();
                    String momentJourneeIsOk = lTmpLinkEvent.setMomentJournee(eventAsJsonStart, eventAsJsonEnd);

                    if (momentJourneeIsOk == "ok") {
                        event = linkEvenementTimesheetDAO.findByDateAndMomentJourneeAndCollaborator(
                                eventAsJsonStart, lTmpLinkEvent.getMomentJournee(), collaborator);

                        if (null != event) {

                            mission.getLinkEvenementTimesheetLine().add(event);
                            event.setMission(mission);

                            event.setAbsence(null);

                        } else {

                            event = new LinkEvenementTimesheet();
                            event.setMomentJournee(lTmpLinkEvent.getMomentJournee());
                            event.setCollaborator(collaborator);
                            event.setDate(eventAsJson.getStart());

                            event.setAbsence(null);

                            if (!event.checkDateAndSetMission(mission)) {
                                transaction.rollback();
                                return errorCheckDateAndSetMission;
                            }

                            mission.getLinkEvenementTimesheetLine().add(event);
                            session.save(event);
                        }
                    } else {
                        return DATE_EVENT_ERROR;
                    }
                }
            }
        } else {
            return ID_MISSION_ERROR;
        }

        session.flush();
        transaction.commit();
        return "";
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#delLinkEvenementTimesheet(String, Collaborator)}
     */
    @Override
    public String delLinkEvenementTimesheet(String pListEvents, Collaborator collaborator) {

        ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory tf = objectMapper.getTypeFactory();
        ArrayList<LinkEvenementTimesheetAsJson> lListeEvenements;

        try {
            lListeEvenements = objectMapper.readValue(pListEvents, tf.constructCollectionType(List.class, LinkEvenementTimesheetAsJson.class));
        } catch (IOException e) {
            logger.error("[delete events] read to json", e);
            return e.getMessage();

        }

        if (lListeEvenements.size() != 0) {

            //Va contenir chaque absences qui a été modifiés
            List<Absence> listAbsences = new ArrayList<Absence>();

            Session session = linkEvenementTimesheetDAO.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            for (LinkEvenementTimesheetAsJson eventAsJson : lListeEvenements) {

                LinkEvenementTimesheet event = (LinkEvenementTimesheet) session.get(LinkEvenementTimesheet.class, eventAsJson.getId());

                if (null == event) {
                    transaction.rollback();
                    return "id inconnu";
                }

                Absence abs = event.getAbsence();
                Mission miss = event.getMission();

                //si l'event n'est pas vide
                if (abs != null || miss != null) {
                    //si il appartient bien au collaborateur connecté
                    if ((abs != null && abs.getCollaborateur().equals(collaborator))
                            || (miss != null && miss.getCollaborateur().equals(collaborator))) {

                        for (Frais frais : event.getListFrais()) {
                            session.delete(frais);
                        }
                        //si l'evenement a une mission et une absence, on supprimme juste le lien de l'absence
                        if (null != miss && null != abs) {
                            event.setAbsence(null);
                            abs.getLinkEvenementTimesheets().remove(event);
                        } else {
                            session.delete(event);
                        }


                        //si une absence etait liée à l'evenement
                        if (null != abs) {

                            if (!listAbsences.contains(abs)) {
                                listAbsences.add(abs);
                            }

                            String absCode = abs.getEtat().getCode();
                            // SI le code de l'absence n'est pas egal à brouillon ou refusé
                            if ((!absCode.equals(Etat.ETAT_BROUILLON_CODE)) && (!absCode.equals(Etat.ETAT_REFUSE)) && (!absCode.equals(Etat.ETAT_ANNULE)) && (!absCode.equals(Etat.ETAT_COMMENCE))) {
                                //On n'est pas censé pouvoir les modifier
                                transaction.rollback();
                                return MODIFICATION_ERROR;
                            }
                        }
                    } else {
                        transaction.rollback();
                        return CONNECTION_ERROR;
                    }
                } else {
                    transaction.rollback();
                    return EMPTY_EVENT_DELETE_ERROR;
                }
            }

            //Flush intermédiaire pour que que les collections entre les entités se mettent à jours
            session.flush();

            //Pour chaque absence dont au moins 1 linkEvenement a été modifiés
            for (Absence abs : listAbsences) {

                int nbLinkEvenement = abs.getLinkEvenementTimesheets().size();

                //si elle n'a plus d'evenement lié
                if (nbLinkEvenement == 0) {
                    session.delete(abs);

                } else {
                    //Mise à jour de la prop nbJours
                    float newNbJours = abs.calculNbJours();
                    abs.setNbJours(newNbJours);

                    //Maj des date de début et date de fin
                    DateTime firstDate = abs.calculFirstOrLastDate("first");
                    abs.setDateDebut(firstDate.toDate());
                    DateTime endDate = abs.calculFirstOrLastDate("last");
                    abs.setDateFin(endDate.toDate());
                }
            }
            session.flush();
            transaction.commit();
        } else {
            return EMPTY_LIST_EVENT_DELETE_ERROR;
        }

        return "";
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#findAllByMission(Mission)}
     */
    @Override
    public List<LinkEvenementTimesheet> findAllByMission(Mission pMission) {
        return linkEvenementTimesheetDAO.findAllByMission(pMission);
    }

    @Override
    public boolean isRaValideWithMission(Mission pMission, Long idColl) {
        boolean raValide = false;
        List<LinkEvenementTimesheet> listLink = this.findAllByMissionWithoutDateRestriction(pMission);
        List<RapportActivites> listRapportSoumis = new ArrayList<RapportActivites>();
        List<RapportActivites> listRapportTemp = rapportActivitesService.getAllRAForCollaboratorBetweenDate(collaboratorService.get(idColl), pMission.getDateDebut());
        for (RapportActivites raTemp : listRapportTemp) {
            if (raTemp.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)
                    || raTemp.getEtat().getCode().equals(Etat.ETAT_VALIDE_CODE)) {
                listRapportSoumis.add(raTemp);
            }
        }

        for (LinkEvenementTimesheet link : listLink) {
            for (RapportActivites raTemp : listRapportSoumis) {
                if (raTemp.getMoisRapport().getMonth() == link.getDate().getMonth()) {
                    raValide = true;
                }
            }
        }
        return raValide;
    }

    @Override
    public List<LinkEvenementTimesheet> findAllByMissionWithoutDateRestriction(Mission pMission) {
        return linkEvenementTimesheetDAO.findAllByMissionWithoutDateRestriction(pMission);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#getCountSaturdaysSinceHistoMaxTimesheet(Collaborator)}
     */
    @Override
    public int getCountSaturdaysSinceHistoMaxTimesheet(Collaborator collaborator) {

        int lPeriodHistoMaxTimeSheetAsMonth = Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR;
        DateTime lFirstDayOnMonth = new DateTime(DateTime.now().withDayOfMonth(1).withMillisOfDay(0))
                .minusMonths(lPeriodHistoMaxTimeSheetAsMonth);

        int lNbSaturday = linkEvenementTimesheetDAO.getCountSaturdaysSinceDate(collaborator, lFirstDayOnMonth);
        return lNbSaturday;
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#getListEventWithAbsenceIdAsJson(long)}
     */
    @Override
    public String getListEventWithAbsenceIdAsJson(long absenceId) throws Exception {

        //Re
        Absence absence = absenceDAO.get(absenceId);
        if (null == absence) {
            throw new Exception("L'id de l'absence est inconnu.");
        }

        List<LinkEvenementTimesheet> listEvents =
                this.getDAO().findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, absence);

        List<LinkEvenementTimesheetAsJson> listEventsAsJson = new ArrayList<LinkEvenementTimesheetAsJson>();

        for (LinkEvenementTimesheet event : listEvents) {
            listEventsAsJson.add(event.toJson());
        }

        return this.writeJson(listEventsAsJson);
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#getMapJourMission(List)}
     */
    @Override
    public LinkedHashMap<Mission, Double> getMapJourMission(List<LinkEvenementTimesheet> listEvent) throws JsonProcessingException {

        LinkedHashMap<Mission, Double> mapJourMission = new LinkedHashMap<>();
        //Pour chaque event, on récupère la mission et on la sauvegarde en incrémentant le nbr de demi journées si elle a une commande associée.
        //Si non on sauvegarde la mission dans listMissionSansCommande pour savoir quelle mission n'a pas eu de facture créée
        for (LinkEvenementTimesheet event : listEvent) {

            if (event.getMission() != null) {
                Mission mission = event.getMission();

                List<Commande> listCommandes = mission.getCommandes();
                // Si la liste des commmandes pour cette mission est pas vide, s'il n'y a pas de client et si la la liste des mission sans commmande ne contient pas cette mission
                if ((listCommandes != null || listCommandes.size() != 0 ||
                        "MI".equals(mission.getTypeMission().getCode()) || mission.getClient() != null)) {
                    //Si il existe au moins une commande associée et on incrémente le nombre de jours par mission
                    //on calcule le nombre de jours
                    if (listCommandes.size() >= 1) {
                        if (!mapJourMission.containsKey(mission)) {
                            mapJourMission.put(mission, 0.5);
                        } else {
                            Double nbcurent = mapJourMission.get(mission);
                            nbcurent += 0.5;
                            mapJourMission.put(mission, nbcurent);
                        }
                    }
                }

            }
        }

        return mapJourMission;
    }

    /**
     * {@linkplain LinkEvenementTimesheetService#getHolidaysJson(String, String)}
     */
    @Override
    public String getHolidaysJson(String pStart, String pEnd) {

        DateTime lStart = new DateTime(pStart);
        DateTime lEnd = new DateTime(pEnd);

        //On récupère les jours férié pour l'année de lStart
        List<Date> lHolidays = getHolidays(lStart.getYear());
        // si Start et End ne sont pas sur la même année
        if (lStart.getYear() != lEnd.getYear()) {
            //On récupère aussi les jours fériés de l'année de End
            List<Date> lHolidaysWithEndYear = getHolidays(lEnd.getYear());
            lHolidays.addAll(lHolidaysWithEndYear);
        }

        List<LinkEvenementTimesheetAsJson> lFullCalendarEventsAsJson = new ArrayList<LinkEvenementTimesheetAsJson>();

        //Pour chacun des jours fériés récupérés
        for (Date lHoliday : lHolidays) {
            //SI (jours férié est > lStart ET < lEnd )
            // BAB 31/05/2016 - [Amilnote 101] modification de la condition pour prise en compte du 1/11 en jour ferie
            if ((lHoliday.after(lStart.toDate()) || lHoliday.equals(lStart.toDate())) && (lHoliday.before(lEnd.toDate()))) {

                LinkEvenementTimesheet lLinkEvenementTimesheet = new LinkEvenementTimesheet();
                lLinkEvenementTimesheet.setMomentJournee(0);
                lLinkEvenementTimesheet.setDate(lHoliday);

                lFullCalendarEventsAsJson.add(new LinkEvenementTimesheetAsJson().ConstructorForHolidays(lLinkEvenementTimesheet));

                lLinkEvenementTimesheet = new LinkEvenementTimesheet();
                lLinkEvenementTimesheet.setMomentJournee(1);
                lLinkEvenementTimesheet.setDate(lHoliday);

                lFullCalendarEventsAsJson.add(new LinkEvenementTimesheetAsJson().ConstructorForHolidays(lLinkEvenementTimesheet));
            }
        }
        return this.writeJson(lFullCalendarEventsAsJson);

    }

    /**
     * Gets holidays.
     *
     * @param annee the annee
     * @return the holidays
     */
    public List<Date> getHolidays(int annee) {
        List<Date> datesFeries = new ArrayList<Date>();

        // Jour de l'an *
        GregorianCalendar jourAn = new GregorianCalendar(annee, 0, 1);
        datesFeries.add(jourAn.getTime());

        // Lundi de pacques *
        GregorianCalendar pacques = calculLundiPacques(annee);
        datesFeries.add(pacques.getTime());

        // Fete du travail *
        GregorianCalendar premierMai = new GregorianCalendar(annee, 4, 1);
        datesFeries.add(premierMai.getTime());

        // 8 mai *
        GregorianCalendar huitMai = new GregorianCalendar(annee, 4, 8);
        datesFeries.add(huitMai.getTime());

        // Ascension (= pâques + 38 jours)
        GregorianCalendar ascension = new GregorianCalendar(annee,
                pacques.get(GregorianCalendar.MONTH),
                pacques.get(GregorianCalendar.DAY_OF_MONTH));
        ascension.add(GregorianCalendar.DAY_OF_MONTH, 38);
        datesFeries.add(ascension.getTime());

        // Fête Nationale
        GregorianCalendar quatorzeJuillet = new GregorianCalendar(annee, 6, 14);
        datesFeries.add(quatorzeJuillet.getTime());

        // Assomption
        GregorianCalendar assomption = new GregorianCalendar(annee, 7, 15);
        datesFeries.add(assomption.getTime());

        // La Toussaint
        GregorianCalendar toussaint = new GregorianCalendar(annee, 10, 1);
        datesFeries.add(toussaint.getTime());

        // L'Armistice
        GregorianCalendar armistice = new GregorianCalendar(annee, 10, 11);
        datesFeries.add(armistice.getTime());

        // Noël
        GregorianCalendar noel = new GregorianCalendar(annee, 11, 25);
        datesFeries.add(noel.getTime());

        return datesFeries;
    }

    /**
     * Calcul lundi pacques gregorian calendar.
     *
     * @param annee the annee
     * @return the gregorian calendar
     */
    private GregorianCalendar calculLundiPacques(int annee) {
        int a = annee / 100;
        int b = annee % 100;
        int c = (3 * (a + 25)) / 4;
        int d = (3 * (a + 25)) % 4;
        int e = (8 * (a + 11)) / 25;
        int f = (5 * a + b) % 19;
        int g = (19 * f + c - e) % 30;
        int h = (f + 11 * g) / 319;
        int j = (60 * (5 - d) + b) / 4;
        int k = (60 * (5 - d) + b) % 4;
        int m = (2 * j - k - g + h) % 7;
        int n = (g - h + m + 114) / 31;
        int p = (g - h + m + 114) % 31;
        int jour = p + 1;
        int mois = n;

        GregorianCalendar date = new GregorianCalendar(annee, mois - 1, jour);
        date.add(GregorianCalendar.DAY_OF_MONTH, 1);
        return date;
    }
}