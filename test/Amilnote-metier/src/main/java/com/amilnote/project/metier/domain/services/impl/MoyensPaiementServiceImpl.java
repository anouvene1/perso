/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.MoyensPaiementDAOImpl;
import com.amilnote.project.metier.domain.entities.MoyensPaiement;
import com.amilnote.project.metier.domain.entities.json.MoyensPaiementAsJson;
import com.amilnote.project.metier.domain.services.MoyensPaiementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Moyens paiement service.
 */
@Service("moyensPaiementService")
public class MoyensPaiementServiceImpl extends AbstractServiceImpl<MoyensPaiement, MoyensPaiementDAOImpl> implements MoyensPaiementService {

    @Autowired
    private MoyensPaiementDAOImpl paiementDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public MoyensPaiementDAOImpl getDAO() {
        return paiementDAO;
    }

    /**
     * {@linkplain MoyensPaiementService#findById(int)}
     */
    @Override
    public MoyensPaiement findById(int pIdPaiement) {
        return paiementDAO.findUniqEntiteByProp(MoyensPaiement.PROP_ID_PAIEMENT, pIdPaiement);
    }

    /**
     * {@linkplain MoyensPaiementService#createOrUpdateMoyensPaiement(MoyensPaiementAsJson)}
     */
    @Override
    public int createOrUpdateMoyensPaiement(MoyensPaiementAsJson paiementJson) throws IOException {
        return paiementDAO.createOrUpdateMoyensPaiement(paiementJson);
    }

    /**
     * {@linkplain MoyensPaiementService#deleteMoyensPaiement(MoyensPaiement)}
     */
    @Override
    public String deleteMoyensPaiement(MoyensPaiement paiement) {
        return paiementDAO.deleteMoyensPaiement(paiement);
    }

    /**
     * {@linkplain MoyensPaiementService#findAllOrderByNomAsc()}
     */
    @Override
    public List<MoyensPaiement> findAllOrderByNomAsc() {
        //with admin, without disabled
        return paiementDAO.findMoyensPaiement();
    }

    /**
     * {@linkplain MoyensPaiementService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<MoyensPaiementAsJson> findAllOrderByNomAscAsJson() {
        List<MoyensPaiement> tmpListPaiement = this.findAllOrderByNomAsc();
        List<MoyensPaiementAsJson> tmpListPaiementAsJson = new ArrayList<>();

        for (MoyensPaiement tmpCommande : tmpListPaiement) {

            tmpListPaiementAsJson.add(tmpCommande.toJson());

        }
        return tmpListPaiementAsJson;
    }

}
