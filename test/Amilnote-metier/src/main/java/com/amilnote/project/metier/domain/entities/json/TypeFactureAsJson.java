/*
 * ©Amiltone 2017
 */
package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TypeFacture;

public class TypeFactureAsJson {

    private Long id;
    private String typeFacture;
    private String code;

    public TypeFactureAsJson(TypeFacture typeFacture) {
        this.id = typeFacture.getId();
        this.typeFacture = typeFacture.getTypeFacture();
        this.code = typeFacture.getCode();
    }

    public TypeFacture toObject(){
        TypeFacture typeFacture = new TypeFacture();
        typeFacture.setId(this.id);
        typeFacture.setCode(this.code);
        typeFacture.setTypeFacture(this.typeFacture);
        return typeFacture;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeFacture() {
        return typeFacture;
    }

    public void setTypeFacture(String typeFacture) {
        this.typeFacture = typeFacture;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
