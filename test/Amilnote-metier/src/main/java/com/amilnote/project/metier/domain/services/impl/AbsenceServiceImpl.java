/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.AbsenceDAO;
import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.LinkEvenementTimesheetDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkEvenementTimesheetAsJson;
import com.amilnote.project.metier.domain.entities.json.TypeAbsenceAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Utils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The type Absence service.
 */
@Service("absenceService")
public class AbsenceServiceImpl extends AbstractServiceImpl<Absence, AbsenceDAO> implements AbsenceService {

    private static final Logger logger = LogManager.getLogger(AbsenceServiceImpl.class);

    private static final String MESSAGE_RETURNED_ABSENCE_REFUSED = "L'absence est refusée";
    private static final String MESSAGE_RETURNED_ABSENCE_CANCELLED = "L'absence est annulée";
    private static final String MESSAGE_RETURNED_ABSENCE_VALIDATED = "Validation de l'absence réussie";


    @Autowired
    private LinkEvenementTimesheetDAO linkEvenementTimesheetDAO;
    @Autowired
    private AbsenceDAO absenceDAO;
    @Autowired
    private CollaboratorDAO collaboratorDAO;
    @Autowired
    private EtatDAO etatDAO;
    @Autowired
    private RapportActivitesService rapportActivitesService;
    @Autowired
    private EtatService etatService;
    @Autowired
    private MissionService missionService;
    @Autowired
    private FactureService factureService;
    @Autowired
    private MailService mailService;
    @Autowired
    private MailListService mailListService;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public AbsenceDAO getDAO() {
        return absenceDAO;
    }

    /**
     * {@linkplain AbsenceService#getAllTypeAbsenceAsJson()}
     */
    @Override
    public String getAllTypeAbsenceAsJson() {

        List<TypeAbsence> lListTypeAbsence = absenceDAO.getAllTypeAbsence();
        List<TypeAbsenceAsJson> lTypeAbsenceAsJson = new ArrayList<>();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Collaborator collaborateurConnecte = collaboratorDAO.findUniqEntiteByProp(Collaborator.PROP_MAIL, userDetails.getUsername());
        String statut = collaborateurConnecte.getStatut().getCode();
        if (statut.equals("STT")) {
            for (TypeAbsence lTypeAbsence : lListTypeAbsence) {
                if (lTypeAbsence.getCode().equals("JNT")) {
                    lTypeAbsenceAsJson.add(new TypeAbsenceAsJson(lTypeAbsence));
                }
            }
        } else {
            for (TypeAbsence lTypeAbsence : lListTypeAbsence) {
                if (lTypeAbsence.getEtat() == 1) {
                    lTypeAbsenceAsJson.add(new TypeAbsenceAsJson(lTypeAbsence));
                }
            }
        }
        return this.writeJson(lTypeAbsenceAsJson);
    }

    /**
     * {@linkplain AbsenceService#findAllAbsencesNotFinishForCollaborator(Collaborator, List)}
     */
    @Override
    public String findAllAbsencesNotFinishForCollaborator(Collaborator collaborator, List<LinkEvenementTimesheet> pListEvents)
            throws JsonProcessingException {

        List<AbsenceAsJson> lListAbsence = new ArrayList<>();
        // Pour chaque evenement passé en paramètre
        for (LinkEvenementTimesheet lEvent : pListEvents) {
            // si il a une absence
            if (lEvent.getAbsence() != null) {
                // On crée un objet Json avec cette absence et on le stock
                lListAbsence.add(new AbsenceAsJson(lEvent.getAbsence()));
            }
        }

        if (lListAbsence.isEmpty()) {
            return null;
            // Si la liste d'absence Json est pas vide
        } else {
            return this.writeJson(lListAbsence);
        }
    }

    /**
     * {@linkplain AbsenceService#findById(long)}
     */
    @Override
    public TypeAbsence findById(long id) {
        return absenceDAO.getTypeAbsenceById(id);
    }

    /**
     * {@linkplain AbsenceService#getNbAbsenceAttenteVal()}
     */
    @Override
    public int getNbAbsenceAttenteVal() {
        return absenceDAO.getNbAbsenceAttenteVal();
    }

    /**
     * {@linkplain AbsenceService#getNbAbsenceAttenteVal(Collaborator)}
     */
    @Override
    public int getNbAbsenceAttenteVal(Collaborator collaborator) {
        return absenceDAO.getNbAbsenceAttenteVal(collaborator);
    }

    /**
     * {@linkplain AbsenceService#getNbAbsenceBrouillonVal(Collaborator)}
     */
    @Override
    public int getNbAbsenceBrouillonVal(Collaborator collaborator) {
        Etat pEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);
        return absenceDAO.getNbAbsenceByEtatAndCollaborator(collaborator, pEtat);
    }

    /**
     * {@linkplain AbsenceService#getDateDernierAbsSoumis(Collaborator)}
     */
    @Override
    public Date getDateDernierAbsSoumis(Collaborator collaborator) {
        return absenceDAO.getDateDernierAbsSoumis(collaborator);
    }

    /**
     * {@linkplain AbsenceService#getAbsencesByCollaboratorAndEtatBrouillon(Collaborator)}
     */
    @Override
    public List<Absence> getAbsencesByCollaboratorAndEtatBrouillon(Collaborator collaborator) {

        Etat lEtatBrouillon = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);

        return absenceDAO.getAbsencesByCollaboratorAndEtat(collaborator, lEtatBrouillon);
    }

    /**
     * {@linkplain AbsenceService#getAbsencesByCollaboratorAndEtatBrouillonAsJson(Collaborator)}
     */
    @Override
    public String getAbsencesByCollaboratorAndEtatBrouillonAsJson(Collaborator collaborator) {

        List<Absence> listeAbs = this.getAbsencesByCollaboratorAndEtatBrouillon(collaborator);

        List<Absence> listAbsences = findDemiJourDebutFinAbsence(listeAbs);

        List<AbsenceAsJson> listeAbsAsJson = this.listToListAsJson(listAbsences);

        return this.writeJson(listeAbsAsJson);
    }

    /**
     * {@linkplain AbsenceService#updateAbsencesFromBRToSO(String, String)}
     */
    @Override
    public String updateAbsencesFromBRToSO(String pListId, String comment) {

            ObjectMapper objectMapper = new ObjectMapper();
            TypeFactory tf = objectMapper.getTypeFactory();
            List<AbsenceAsJson> lListAbsenceAsJson;

            try {
                lListAbsenceAsJson = objectMapper.readValue(
                        pListId,
                        tf.constructCollectionType(List.class, AbsenceAsJson.class)
                );
            } catch (IOException e) {
                logger.error("[update absences]", e);
                return e.getMessage();
            }

            return absenceDAO.updateAbsencesFromBRToSO(lListAbsenceAsJson, comment);

    }

    /**
     * {@linkplain AbsenceService#getAllAbsencesEtatSO()}
     */
    @Override
    public List<AbsenceAsJson> getAllAbsencesEtatSO() {

        Etat lEtatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);

        List<Absence> listeAbs = absenceDAO.findListEntitesByProp(Absence.PROP_ETAT, lEtatSO);

        List<Absence> listAbsences = findDemiJourDebutFinAbsence(listeAbs);

        List<AbsenceAsJson> listeAbsAsJson = this.listToListAsJson(listAbsences);

        return listeAbsAsJson;
    }

    private List<AbsenceAsJson> listToListAsJson(List<Absence> pListAbs) {

        List<AbsenceAsJson> listeAbsAsJson = new ArrayList<>();

        for (Absence lAbsence : pListAbs) {

            AbsenceAsJson tmpAbsenceAsJson = new AbsenceAsJson(lAbsence);

            List<LinkEvenementTimesheet> listEventsSortByDate =
                    linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_ABSENCE, lAbsence);

            LinkEvenementTimesheetServiceImpl.sortByDateAndMomentJournee(listEventsSortByDate);

            for (LinkEvenementTimesheet event : listEventsSortByDate) {
                tmpAbsenceAsJson.getListLinkEvenementTimesheetAsJson().add(new LinkEvenementTimesheetAsJson(event));
            }

            listeAbsAsJson.add(tmpAbsenceAsJson);
        }
        return listeAbsAsJson;
    }


    /**
     * {@linkplain AbsenceService#actionValidationAbsence(Long, String, String, Collaborator)}
     */
    @Override
    public String actionValidationAbsence(Long absenceId, String stateCode, String comment, Collaborator currentCollaborator) throws Exception {
        manageValidationAbsence(absenceId, stateCode, comment);

        // send email to collaborator
        mailService.sendMailAbsenceValidation(stateCode, absenceDAO.get(absenceId), currentCollaborator, comment);

        return messageByAbsenceStateCode(stateCode);
    }

    /**
     * {@linkplain AbsenceService#actionValidationAbsenceList(List, String, String, Collaborator)}
     */
    @Override
    public String actionValidationAbsenceList(List<Long> absenceIds, String stateCode, String comment, Collaborator currentCollaborator)
            throws Exception {
        // Update all absences
        for (Long absenceId : absenceIds) {
            manageValidationAbsence(absenceId, stateCode, comment);
        }

        // Send all emails asynchronously
        mailListService.sendAbsenceListValidationEmail(stateCode, absenceIds, currentCollaborator, comment);

        return messageByAbsenceStateCode(stateCode);
    }

    private String messageByAbsenceStateCode(String stateCode) {
        String returnMessage;
        switch (stateCode) {
            case Etat.ETAT_VALIDE_CODE:
                returnMessage = MESSAGE_RETURNED_ABSENCE_VALIDATED;
                break;
            case Etat.ETAT_REFUSE:
                returnMessage = MESSAGE_RETURNED_ABSENCE_REFUSED;
                break;
            case Etat.ETAT_ANNULE:
                returnMessage = MESSAGE_RETURNED_ABSENCE_CANCELLED;
                break;
            default:
                returnMessage = "";
        }
        return returnMessage;
    }


    /**
     * {@linkplain AbsenceService#manageValidationAbsence(Long, String, String)}
     */
    @Override
    public void manageValidationAbsence(Long absenceId, String stateCode, String comment) throws Exception {
        Etat newState = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, stateCode);
        if (newState == null ||
                !(Etat.ETAT_VALIDE_CODE.equals(newState.getCode()) || Etat.ETAT_REFUSE.equals(newState.getCode()) || Etat.ETAT_ANNULE.equals(newState.getCode()))) {
            throw new Exception("L'état demandé pour l'absence (" + absenceId + ") est inconnu ou non valide");
        }

        Absence absence = this.getDAO().get(absenceId);
        if (null == absence) {
            throw new Exception("L'id de l'absence (" + absenceId + ") est inconnu");
        }

        // update absence state
        updateEtatAbsence(absence, newState);

        // update comment
        if (StringUtils.isNotEmpty(comment))
            updateCommentaireAbsence(absence, comment);

        // Cancel RA for refused and canceled states
        if (Etat.ETAT_REFUSE.equals(newState.getCode()) || Etat.ETAT_ANNULE.equals(newState.getCode())) {
            Collaborator collaborator = absence.getCollaborateur();
            annulerRA(comment, absence.getDateDebut(), collaborator);
        }
    }


    /**
     * {@linkplain AbsenceService#findAllAsJson()}
     */
    @Override
    public List<AbsenceAsJson> findAllAsJson() {

        List<Absence> listAbsences = this.getDAO().loadAll();
        List<AbsenceAsJson> listAbsencesAsJson = new ArrayList<>();

        for (Absence tmpAbsence : listAbsences) {
            listAbsencesAsJson.add(tmpAbsence.toJson());
        }

        return listAbsencesAsJson;
    }

    /**
     * {@linkplain AbsenceService#getAbsencesOrderByDate()} ()} ()}
     */
    @Override
    public List<Absence> getAbsencesOrderByDate() {

        return absenceDAO.getAbsencesOrderByDate();
    }


    /**
     * {@linkplain AbsenceService#findAbsencesByStateMonthAndYearOrderByDate(String, String, String)}
     */
    @Override
    public List<Absence> findAbsencesByStateMonthAndYearOrderByDate(String etatStr, String month, String year) {
        Etat etat = etatService.getEtatByCode(etatStr);
        final Integer monthInt = Utils.parseToInt(month, null);
        final Integer yearInt = Utils.parseToInt(year, null);
        return absenceDAO.findAbsencesByStateMonthAndYearOrderByDate(etat, monthInt, yearInt);
    }

    /**
     * {@linkplain AbsenceService#findDemiJourDebutFinAbsence(List)}
     */
    @Override
    public List<Absence> findDemiJourDebutFinAbsence(List<Absence> listeAbs) {
        //On va ajouter la demi-journée à la date de début et de fin (exemple 2015-01-01 00:00:00 --> 2015-01-01 09:00:00)
        List<Absence> listAbsences = new ArrayList<>();
        for (Absence absence : listeAbs) {
            List<LinkEvenementTimesheet> listEvent = linkEvenementTimesheetDAO.findByAbsence(absence);
            if (!listEvent.isEmpty()) {
                LinkEvenementTimesheet eventDebutAbsence = listEvent.get(0);
                LinkEvenementTimesheet eventFinAbsence = eventDebutAbsence;
                for (LinkEvenementTimesheet event : listEvent) {
                    //On regarde si l'event doit repousser les limites de l'absence (début/fin)
                    if (event.getDate().before(eventDebutAbsence.getDate())) {
                        eventDebutAbsence = event;
                    } else if (event.getDate().after(eventFinAbsence.getDate())) {
                        eventFinAbsence = event;
                    }

                    //Si l'event est le même jour que le début, on regarde le momentJournée
                    if (!event.getDate().after(eventDebutAbsence.getDate()) && !event.getDate().before(eventDebutAbsence.getDate())) {
                        if (event.getMomentJournee() == 0) {
                            eventDebutAbsence = event;
                        }
                    }

                    //Si l'event est le même jour que la fin, on regarde le momentJournée
                    if (!event.getDate().after(eventFinAbsence.getDate()) && !event.getDate().before(eventFinAbsence.getDate())) {
                        if (event.getMomentJournee() == 1) {
                            eventFinAbsence = event;
                        }
                    }
                }

                Date dateDebut = absence.getDateDebut();
                if (eventDebutAbsence.getMomentJournee() == 0) {
                    dateDebut.setHours(8);
                } else {
                    dateDebut.setHours(13);
                }
                absence.setDateDebut(dateDebut);

                Date dateFin = absence.getDateFin();
                if (eventFinAbsence.getMomentJournee() == 0) {
                    dateFin.setHours(12);
                } else {
                    dateFin.setHours(18);
                }
                absence.setDateFin(dateFin);

                listAbsences.add(absence);
            }
        }
        return listAbsences;
    }

    /**
     * {@linkplain AbsenceService#findByCollaboratorBetweenDate(Collaborator, Date, Date)}
     */
    @Override
    public List<Absence> findByCollaboratorBetweenDate(Collaborator collaborator, Date dateDeb, Date dateFin) {
        return absenceDAO.getByCollaboratorBetweenDate(collaborator, dateDeb, dateFin);
    }

    /**
     * {@linkplain AbsenceService#getAbsencesSoumisesBetweenDate(String, String)}
     */
    @Override
    public List<Absence> getAbsencesSoumisesBetweenDate(String start, String end) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);
        Date debut = null;
        Date fin = null;
        try {
            debut = sdf.parse(start);
            fin = sdf.parse(end);
        } catch (ParseException e) {
            logger.error("[find submitted absences] error parsing dates {}, {} ", start, end, e);
        }
        return findDemiJourDebutFinAbsence(absenceDAO.getAbsencesSoumisesBetweenDate(debut, fin));
    }

    /**
     * {@linkplain AbsenceService#getAbsencesValideesBetweenDate(String, String)}
     */
    @Override
    public List<Absence> getAbsencesValideesBetweenDate(String start, String end) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT_SLASH);
        Date debut = null;
        Date fin = null;
        try {
            debut = sdf.parse(start);
            fin = sdf.parse(end);
        } catch (ParseException e) {
            logger.error("[find validated absences] error parsing dates {}, {}", start, end, e);
        }
        return findDemiJourDebutFinAbsence(absenceDAO.getAbsencesValideesBetweenDate(debut, fin));
    }


    @Override
    public void annulerRA(String commentaire, Date dateDebutRA, Collaborator collaborator) throws Exception {
        //declaration des variables
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_MM_YYYY);

        List<RapportActivites> listRA = rapportActivitesService.getExistingRAForMonth(collaborator, new DateTime(dateDebutRA));


        for (RapportActivites rapportActivites : listRA) {
            //change l'etat du RA en annulé
            rapportActivitesService.changeEtatRA(rapportActivites.getId(), Etat.ETAT_ANNULE, commentaire);

            //envoi du mail
            mailService.sendMailActivityReportCanceled(dateFormat.format(rapportActivites.getMoisRapport()), commentaire, collaborator.toJson());
        }
    }

    @Override
    public String deleteAbsence(Absence absence) {
        return absenceDAO.deleteAbsence(absence);
    }

    @Override
    public Long updateEtatAbsence(Absence absence, Etat etat) {
        return absenceDAO.updateEtatAbsence(absence, etat);
    }

    @Override
    public Long updateCommentaireAbsence(Absence absence, String commentaire) {
        return absenceDAO.updateCommentaireAbsence(absence, commentaire);
    }

    @Override
    public List<Absence> findByIds(List<Long> absenceIds) {
        return absenceDAO.findByIds(absenceIds);
    }


    public void majFactureApresMajRA(Collaborator collaborator, DateTime dateRAVoulue) throws IOException {

        Etat etatSoumis = etatService.getEtatByCode(Etat.ETAT_SOUMIS_CODE);
        Etat etatValide = etatService.getEtatByCode(Etat.ETAT_VALIDE_CODE);
        List<Mission> missions = missionService.findMissionsClientesByMonthForCollaborator(collaborator, dateRAVoulue);

        for (Mission mission : missions) {
            Facture facture = factureService.findByMissionByMonth(mission, dateRAVoulue);
            if (facture != null) {
                if (facture.getEtat() != etatSoumis && facture.getEtat() != etatValide) {
                    facture.setRaValide(0);
                    factureService.createOrUpdateFacture(facture);
                }
            }
        }
    }


}
