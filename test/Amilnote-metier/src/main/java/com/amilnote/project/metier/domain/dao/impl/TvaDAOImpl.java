/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.TvaDAO;
import com.amilnote.project.metier.domain.entities.TVA;
import org.springframework.stereotype.Repository;

/**
 * The type Tva dao.
 */
@Repository("TvaDAO")
public class TvaDAOImpl extends AbstractDAOImpl<TVA> implements TvaDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<TVA> getReferenceClass() {
        return TVA.class;
    }
}
