/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.PosteAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Poste.
 */
@Entity
@Table(name = "ami_poste")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Poste implements java.io.Serializable {

    /**
     * The constant POSTE_COLLABORATEUR.
     */
    public static final String POSTE_COLLABORATEUR = "COL";
    /**
     * The constant POSTE_MANAGER.
     */
    public static final String POSTE_MANAGER = "MAN";
    /**
     * The constant POSTE_DIRECTEUR.
     */
    public static final String POSTE_DIRECTEUR = "DIR";
    /**
     * The constant POSTE_INGENIEURAFFAIRES.
     */
    public static final String POSTE_INGENIEURAFFAIRES = "IA";
    /**
     * The constant POSTE_INVITE.
     */
    public static final String POSTE_INVITE = "INV";
    /**
     * The constant POSTE_CHARGE_COMMUNICATION_GRAPHISTE.
     */
    public static final String POSTE_CHARGE_COMMUNICATION_GRAPHISTE = "CCG";
    /**
     * The constant POSTE_SOUS_TRAITANT.
     */
    public static final String POSTE_SOUS_TRAITANT = "STT";
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_POSTE.
     */
    public static final String PROP_POSTE = "poste";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    private static final long serialVersionUID = 2290863870942055022L;
    private Long id;
    private String poste;
    private String code;
    private int etat;

    /**
     * Constructeur par defaut de la classe TypeFrais
     */
    public Poste() {
    }

    /**
     * Constructeur de la classe TypeFrais
     *
     * @param pPoste the p poste
     * @param pCode  the p code
     * @param pEtat  the p etat
     */
    public Poste(String pPoste, String pCode, int pEtat) {
        super();
        poste = pPoste;
        code = pCode;
        etat = pEtat;
    }

	/* 
     * GETTERS AND SETTERS
	 */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets poste.
     *
     * @return the Poste
     */
    @Column(name = "poste", nullable = false)
    public String getPoste() {
        return poste;
    }

    /**
     * Sets poste.
     *
     * @param pPoste the Poste to set
     */
    public void setPoste(String pPoste) {
        this.poste = pPoste;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * 1: actif
     * 0:inactif
     *
     * @return etat etat
     */
    @Column(name = "etat", nullable = false)
    public int getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(int pEtat) {
        etat = pEtat;
    }

    /**
     * To json poste as json.
     *
     * @return the poste as json
     */
    public PosteAsJson toJson() {
        return new PosteAsJson(this);
    }
}
