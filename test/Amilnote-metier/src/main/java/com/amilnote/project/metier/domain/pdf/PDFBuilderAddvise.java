/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.entities.AddviseLinkSessionCollaborateur;
import com.amilnote.project.metier.domain.entities.AddviseSession;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.amilnote.project.metier.domain.utils.Constantes.PDF_HEADER_BLUE_COLOR;

@Service("PDFBuilderAddvise")
public class PDFBuilderAddvise extends PDFBuilder {

    //Constantes
    private static final String KEY_PDF_ADDVISE = "pdf.addvise";
    private static final String NOM_DOSSIER_ADDVISE = "dossierAddvise";
    private static final String HEADER_ADDVISE = "Formation : ADDVISE";
    private static final String ADRESSE = "\n\n76 boulevard du 11 novembre 1918\n" +
        "69100 VILLEURBANNE \nTél. : 04.37.70.89.95\nFax : 04.37.43.63.60\n\n\n";
    private static final String TITRE = "Attestation de Présence\n\n";
    private static final String FOOTER = "AMILTONE Société par actions simplifiée au capital de 30 000 €\n" +
        "Siège social : 76 boulevard du 11 Novembre 1918 – 69100 VILLEURBANNE\n" +
        "538 949 108 RCS LYON – N° TVA intra-communautaire FR 235 389 491 08\n\n" +
        "Ce document est la propriété d'Amiltone SAS et ne peut être reproduit ou communiqué " +
        "sans autorisation écrite.";

    /**
     * Crée le PDF d'émargement pour le programme Addvise à partir de la session donnée
     *
     * @param addviseSession les données de la session Addvise concernée
     * @return le nom du fichier créé
     * @throws NamingException the {@link NamingException}
     * @throws IOException the {@link IOException}
     * @throws DocumentException the {@link DocumentException}
     */
    public byte[] createPDFAddvise(AddviseSession addviseSession) throws NamingException, IOException, DocumentException {

        // Métadonnées du fichier
        String nomFichier;
        String nomDossier;

        // Données intéressantes pour la session Addvise
        DateFormat df = new SimpleDateFormat(Constantes.STANDARD_DATE_FORMAT);
        String date = df.format(addviseSession.getDate());
        String code = addviseSession.getModuleAddvise().getCodeModuleAddvise();
        String nomModule = addviseSession.getModuleAddvise().getNomModuleAddvise();

        // On récupère la liste des collaborateurs confirmés pour cette session Addvise
        List<AddviseLinkSessionCollaborateur> invitations = addviseSession.getListeLinkSessionCollaborateur();
        invitations.removeIf(invit -> invit.getEtatInvitation() != AddviseLinkSessionCollaborateur.EtatInvitation.CONFIRMEE);
        List<Collaborator> collaborateurs = new ArrayList<>();
        for (AddviseLinkSessionCollaborateur invit : invitations) {
            collaborateurs.add(invit.getCollaborateur());
        }
        collaborateurs.sort(Comparator.comparing(Collaborator::getNom));

        // Mise en forme de l'arborescence de fichiers
        String[] paramsNomFichier = {code, date};
        nomFichier = properties.get(KEY_PDF_ADDVISE, paramsNomFichier);
        nomDossier = Parametrage.getContext(NOM_DOSSIER_ADDVISE);
        String chemin = nomDossier + nomFichier;
        File directory = new File(chemin).getParentFile(); //on récupère les dossiers parents
        Files.createDirectories(directory.toPath());

        // On écrit dans le fichier
        FileOutputStream fileOutput;
        fileOutput = new FileOutputStream(nomDossier + nomFichier); // Création du fichier

        Document document = newDocument();
        PdfWriter pdfWriter = newWriter(document, fileOutput);
        TableHeader tableHeader = new TableHeader();
        tableHeader.setHeader(HEADER_ADDVISE + "\n" + nomModule + " (2h) le " + date);

        pdfWriter.setPageEvent(tableHeader);
        prepareWriter(pdfWriter);
        buildPdfMetadata(document);

        document.open();

        // On ajoute le logo et l'adresse
        URL urlLogo = getClass().getResource("/imagesPDF/logo-amiltone.png");
        Image imgLogo = Image.getInstance(urlLogo);
        imgLogo.scalePercent(5);
        document.add(imgLogo);
        document.add(new Phrase(ADRESSE, FONT_ADDRESS));

        // On ajoute le titre
        Paragraph paragraph = new Paragraph(TITRE, FONT_TITLE);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);

        // On ajoute le tableau d'émargement
        document.add(createTableCollaborators(collaborateurs));

        // On ajoute le footer
        ColumnText ct = new ColumnText(pdfWriter.getDirectContent());
        Paragraph footer = new Paragraph(FOOTER, FONT_FOOTER);
        footer.setAlignment(Element.ALIGN_CENTER);
        ct.setSimpleColumn(footer, 80, document.bottom() + 60, 500,document.bottom(), 12, Element.ALIGN_CENTER);
        ct.go();

        document.close();
        fileOutput.close();

        return getFileContent(chemin);
    }

    /**
     * Crée le tableau d'émargement
     *
     * @param collaborateurList liste des collaborateurs
     * @return le tableau à ajouter au document
     */
    private PdfPTable createTableCollaborators(List<Collaborator> collaborateurList) {
        PdfPTable lTableColRes = new PdfPTable(3);
        lTableColRes.setHorizontalAlignment(Element.ALIGN_LEFT);
        lTableColRes.setWidthPercentage(100);
        lTableColRes.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableColRes.getDefaultCell().setBackgroundColor(PDF_HEADER_BLUE_COLOR);
        lTableColRes.addCell(new Phrase("Nom Prénom", fontHeaderWhite));
        lTableColRes.addCell(new Phrase("Date de naissance", fontHeaderWhite));
        lTableColRes.addCell(new Phrase("Signature", fontHeaderWhite));
        lTableColRes.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        lTableColRes.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        lTableColRes.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        lTableColRes.getDefaultCell().setFixedHeight(40);
        for (Collaborator collab : collaborateurList) {
            lTableColRes.addCell(new Phrase(collab.toString()));
            lTableColRes.addCell("");
            lTableColRes.addCell("");
        }
        return lTableColRes;
    }

    private byte[] getFileContent(String cheminFichier) throws IOException {
        File fichier = new File(cheminFichier);
        byte[] content = new byte[]{};

        if (fichier.exists()) {
            Path path = fichier.toPath();
            content = Files.readAllBytes(path);
        }

        return content;
    }
}
