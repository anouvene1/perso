/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.FactureArchiveeDAO;
import com.amilnote.project.metier.domain.entities.FactureArchivee;
import com.amilnote.project.metier.domain.entities.json.FactureArchiveeAsJson;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

/**
 * The type Facture archivee dao.
 */
@Repository("factureArchiveeDAO")
public class FactureArchiveeDAOImpl extends AbstractDAOImpl<FactureArchivee> implements FactureArchiveeDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<FactureArchivee> getReferenceClass() {
        return FactureArchivee.class;
    }

    @Override
    public List<FactureArchivee> findAllFacturesArchivees() {
        Criteria criteria = getCriteria();

        criteria.addOrder(Order.asc(FactureArchivee.PROP_NUMERO_FACTURE_ARCHIVEE));

        return criteria.list();
    }

    /**
     * {@linkplain FactureArchiveeDAO#findMaxNumFacture()}
     */
    @Override
    public int findMaxNumFacture() {
        Criteria criteria = getCriteria();
        int max = 0;

        if (!criteria.list().isEmpty()) {
            ProjectionList projectionList = Projections.projectionList();
            projectionList = projectionList.add((Projections.max(FactureArchivee.PROP_NUMERO_FACTURE_ARCHIVEE)));
            criteria = criteria.setProjection(projectionList);
            max = (int) criteria.uniqueResult();
        }

        return max;
    }

    /**
     * {@linkplain FactureArchiveeDAO#createOrUpdateFactureArchivee(FactureArchiveeAsJson)}
     */
    @Override
    public String createOrUpdateFactureArchivee(FactureArchiveeAsJson factureArchiveeJson) {

        FactureArchivee factureA = new FactureArchivee();

        // On vérifit que l'id soit bien récupéré en cas de modification
        if (factureArchiveeJson.getId() != 0) {
            //On récupère la facture grâce à l'ID
            factureA = findUniqEntiteByProp(FactureArchivee.PROP_ID_FACTURE_ARCHIVEE, factureArchiveeJson.getId());
        } else {

            if (factureArchiveeJson.getClient() != null) {
                factureA.setClient(factureArchiveeJson.getClient());
            } else {
                factureA.setClient(null);
            }

            if (factureArchiveeJson.getContact() != null) {
                factureA.setContact(factureArchiveeJson.getContact());
            } else {
                factureA.setContact(null);
            }

            if (factureArchiveeJson.getCollaborateur() != null) {
                factureA.setCollaborateur(factureArchiveeJson.getCollaborateur());
            } else {
                factureA.setCollaborateur(null);
            }

            if (factureArchiveeJson.getMission() != null) {
                factureA.setMission(factureArchiveeJson.getMission());
            } else {
                factureA.setMission(null);
            }

            if (factureArchiveeJson.getNumCommande() != null) {
                factureA.setNumCommande(factureArchiveeJson.getNumCommande());
            } else {
                factureA.setNumCommande(null);
            }

            if (factureArchiveeJson.getCheminPDF() != null) {
                factureA.setCheminPDF(factureArchiveeJson.getCheminPDF());
            } else {
                factureA.setCheminPDF(null);
            }

            if (factureArchiveeJson.getQuantite() != 0) {
                factureA.setQuantite(factureArchiveeJson.getQuantite());
            } else {
                factureA.setQuantite(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un prix, sinon on le met a 0
            if (factureArchiveeJson.getPrix() != 0) {
                factureA.setPrix(factureArchiveeJson.getPrix());
            } else {
                factureA.setPrix(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un montant, sinon on le met a 0
            if (factureArchiveeJson.getMontant() != 0) {
                factureA.setMontant(factureArchiveeJson.getMontant());
            } else {
                factureA.setMontant(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un montant, sinon on le met a 0
            if (factureArchiveeJson.getNumero() != 0) {
                factureA.setNumero(factureArchiveeJson.getNumero());
            } else {
                factureA.setNumero(0);
            }

            if (factureArchiveeJson.getDate_archivage() != null) {
                factureA.setDate_archivage(factureArchiveeJson.getDate_archivage());
            } else {
                factureA.setDate_archivage(Calendar.getInstance().getTime());
            }

            if (factureArchiveeJson.getTypeFacture() != null) {
                factureA.setTypeFacture(factureArchiveeJson.getTypeFacture());
            } else {
                factureA.setTypeFacture(0);
            }

        }

        // --- Création de la session et de la transaction
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        session.save(factureA);
        session.flush();
        transaction.commit();
        factureArchiveeJson.setId(factureA.getId());

        return "ok";
    }

    /**
     * {@linkplain FactureArchiveeDAO#findFacturesArchivees()}
     */
    @Override
    public List<FactureArchivee> findFacturesArchivees() {
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc(FactureArchivee.PROP_ID_FACTURE_ARCHIVEE));
        return criteria.list();
    }

}
