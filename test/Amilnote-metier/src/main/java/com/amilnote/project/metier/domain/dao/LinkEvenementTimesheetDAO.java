/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.LinkEvenementTimesheet;
import com.amilnote.project.metier.domain.entities.Mission;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

/**
 * The interface Link evenement timesheet dao.
 */
public interface LinkEvenementTimesheetDAO extends LongKeyDAO<LinkEvenementTimesheet> {

    /**
     * retourne la liste des LinkEvenementTimesheet (uniquement la date) pour la mission passée en paramètre
     * qui ne sont pas encore terminés pour la date présente
     *
     * @param pMission the p mission
     * @return List list
     */
    List<LinkEvenementTimesheet> findAllNotFinishEventOnlyDate(Mission pMission);

    /**
     * retourne tous les LinkEvenementTimesheet dont la date de fin est supérieur à pDate ET ( le collaborateur de la mission est pCollaborateur OU le collaborateur de l'absence est pCollaborateur )
     *
     * @param collaborator the p collaborateur
     * @param pStart         the p start
     * @param pEnd           the p end
     * @return List list
     */
    List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, DateTime pStart, DateTime pEnd);

    /**
     * Renvoi une liste d'evenement Timesheet entre pStart et pEnd
     *
     * @param collaborator the p collaborateur
     * @param pStart         the p start
     * @param pEnd           the p end
     * @return list list
     */
    List<LinkEvenementTimesheet> findEventsBetweenDates(Collaborator collaborator, Date pStart, Date pEnd);

    /**
     * retourne le nombre de matiné travaillée pour la periode donnée
     *
     * @param pMission  the p mission
     * @param dateDebut the date debut
     * @param dateFin   the date fin
     * @return int list
     */
    List<LinkEvenementTimesheet> findNbWorkedDaysByMissionAndPeriod(Mission pMission, Date dateDebut, Date dateFin);

    /**
     * retrouve un LinkEvenementTimesheet en fonction de la date et du moment de la journee
     *
     * @param pDate          the p date
     * @param pMomentJournee the p moment journee
     * @param collaborator the p collaborateur
     * @return LinkEvenementTimesheet link evenement timesheet
     */
    LinkEvenementTimesheet findByDateAndMomentJourneeAndCollaborator(DateTime pDate, int pMomentJournee, Collaborator collaborator);

    /**
     * retourne tous les LinkEvenementTimesheet pour la mission passée en paramètre
     *
     * @param pMission the p mission
     * @return List list
     */
    List<LinkEvenementTimesheet> findAllByMission(Mission pMission);

    List<LinkEvenementTimesheet> findAllByMissionWithoutDateRestriction(Mission pMission);

    /**
     * Retourne le nombre de Samedi depuis la date passée en paramètre
     *
     * @param collaborator the p collaborateur
     * @param since          the since
     * @return int count saturdays since date
     */
    int getCountSaturdaysSinceDate(Collaborator collaborator, DateTime since);

    /**
     * Find days mission period list.
     *
     * @param pMission      the p mission
     * @param pDebutPeriode the p debut periode
     * @param pFinPeriode   the p fin periode
     * @return the list
     */
    List<LinkEvenementTimesheet> findDaysMissionPeriod(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode);

    /**
     * Find nb days for forfait deplacement list.
     *
     * @param pMission      the p mission
     * @param pDebutPeriode the p debut periode
     * @param pFinPeriode   the p fin periode
     * @return the list
     */
    List<LinkEvenementTimesheet> findNbDaysForForfaitDeplacement(Mission pMission, DateTime pDebutPeriode, DateTime pFinPeriode);


    /**
     * Renvoie la liste des évènements pour une absence donnée
     *
     * @param pAbsence the p absence
     * @return list list
     */
    List<LinkEvenementTimesheet> findByAbsence(Absence pAbsence);

}
