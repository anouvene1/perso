package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Frais;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface CalculFraisService extends AbstractService<Frais>{
    Map<Long, Object[]> doCalculAvance(List<Object[]> list);
    Map<Long, Map<Long,Object[]>> doCalculFraisDetail(List<Object[]> list);
    List<Object[]>findFraisByDate(Date dateDebut, Date dateFin,String typeCalcul);
    List<Object[]>findForfaitByDate(Date dateDebut, Date dateFin, String typeForfait);
    List<Object[]>findAbsence(Date dateDebut, Date dateFin);
    List<Object[]>doCalculDetail(String typeDetail,Map<Long,Map<Long,Object[]>> montantFrais, Map<Long, Map<Long,Float>> montantForfaits, Map<Long, Object[]> montantAvances);
    Map<Long, Map<Long,Float>> doCalculForfaitsDetail(List<Object[]> list, Map<Long,Map<Long,Object[]>> mapFrais, Map<Long, Integer> mapAbsence);

}
