/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TVA;

/**
 * The type Tva as json.
 */
public class TVAAsJson {

    private long id;
    private String tva;
    private String code;
    private float montant;


    /**
     * Instantiates a new Tva as json.
     *
     * @param pTva the p tva
     */
    public TVAAsJson(TVA pTva) {
        this.setId(pTva.getId());
        this.setCode(pTva.getCode());
        this.setTva(pTva.getTva());
        this.setMontant(pTva.getMontant());
    }

    /**
     * Instantiates a new Tva as json.
     */
    public TVAAsJson() {
    }

    ;

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets tva.
     *
     * @return the tva
     */
    public String getTva() {
        return tva;
    }

    /**
     * Sets tva.
     *
     * @param pTva the tva to set
     */
    public void setTva(String pTva) {
        tva = pTva;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        code = pCode;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param pMontant the montant to set
     */
    public void setMontant(float pMontant) {
        montant = pMontant;
    }
}
