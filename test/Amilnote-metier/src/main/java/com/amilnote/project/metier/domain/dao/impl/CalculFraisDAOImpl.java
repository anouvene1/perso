package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.CalculFraisDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.ForfaitService;
import com.amilnote.project.metier.domain.services.FrequenceService;
import com.amilnote.project.metier.domain.services.TypeFraisService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Repository("CalculFraisDAO")

public class CalculFraisDAOImpl extends AbstractDAOImpl<Frais> implements CalculFraisDAO {

    private static final Logger logger = LogManager.getLogger(CalculFraisDAOImpl.class);

    @Autowired
    private TypeFraisService typeFraisService;

    @Autowired
    private EtatService etatService;

    @Autowired
    @Qualifier("ForfaitService")
    private ForfaitService forfaitService;

    @Autowired
    @Qualifier("FrequenceService")
    private FrequenceService frequenceService;

    @Override
    protected Class<Frais> getReferenceClass() {
        return Frais.class;
    }

    @Override
    public List<Object[]> getDataFrais(Date dateDebut, Date dateFin, String typeCalcul) {
        Criteria criteria = currentSession().createCriteria(Frais.class, Frais.class.getName());

        TypeFrais typeFraisVoiture = typeFraisService.get(new Long(1));
        TypeFrais typeFraisAvance = typeFraisService.get(new Long(12));
        Etat etatValide = etatService.getEtatByCode(Etat.ETAT_VALIDE_CODE);
        Etat etatPaye = etatService.getEtatByCode(Etat.ETAT_SOUMIS_CODE);
        Etat etatBrouillon = etatService.getEtatByCode(Etat.ETAT_BROUILLON_CODE);

        criteria.createCriteria("linkEvenementTimesheet", "e");
        criteria.createCriteria("e.mission", "m");
        criteria.createCriteria("m.collaborateur", "c");
        criteria.createCriteria("typeFrais", "tf");
        switch (typeCalcul) {
            case "Autre":
                criteria.add(Restrictions.ne(Frais.PROP_TYPEFRAIS, typeFraisVoiture));
                criteria.add(Restrictions.ne(Frais.PROP_TYPEFRAIS, typeFraisAvance));
                break;
            case "Voiture":
                criteria.add(Restrictions.eq(Frais.PROP_TYPEFRAIS, typeFraisVoiture));
                criteria.add(Restrictions.ne(Frais.PROP_TYPEFRAIS, typeFraisAvance));
                break;
            case "Avance":
                criteria.add(Restrictions.ne(Frais.PROP_TYPEFRAIS, typeFraisVoiture));
                criteria.add(Restrictions.eq(Frais.PROP_TYPEFRAIS, typeFraisAvance));
                break;
        }

        criteria.add(Restrictions.or(
                Restrictions.eq(Frais.PROP_ETAT, etatValide),
                Restrictions.eq(Frais.PROP_ETAT, etatPaye),
                Restrictions.eq(Frais.PROP_ETAT, etatBrouillon)
                )
        );

        // permet de prendre en compte le 1er du mois
        Calendar c = Calendar.getInstance();
        c.setTime(dateDebut);
        c.add(Calendar.DAY_OF_YEAR, -1);
        dateDebut = new Date(c.getTimeInMillis());

        criteria.add(Restrictions.and(
                Restrictions.ge("e." + LinkEvenementTimesheet.PROP_DATE, dateDebut),
                Restrictions.le("e." + LinkEvenementTimesheet.PROP_DATE, dateFin)
                )
        );
        switch (typeCalcul) {
            case "Avance":
                criteria.setProjection(Projections.projectionList()
                        .add(Projections.property("c.id"))
                        .add(Projections.property("tf." + TypeFrais.PROP_ID))
                        .add(Projections.property(Frais.PROP_MONTANT))
                        .add(Projections.property(Frais.PROP_TYPEAVANCE))
                );
                break;
            default:
                criteria.setProjection(Projections.projectionList()
                        .add(Projections.property("c.id"))
                        .add(Projections.property("tf." + TypeFrais.PROP_ID))
                        .add(Projections.property(Frais.PROP_MONTANT))
                        .add(Projections.property(Frais.PROP_NOMBREKM))
                        .add(Projections.property(Frais.PROP_FRAIS_FORFAIT))
                        .add(Projections.property("m." + Mission.PROP_ID))
                        .add(Projections.property(Frais.PROP_ID))
                );
                break;


        }
        criteria.addOrder(Order.asc("e." + LinkEvenementTimesheet.PROP_DATE));

        return criteria.list();
    }

    @Override
    public List<Object[]> getAbsenceCollab(Date dateDebut, Date dateFin) {
        Criteria criteria = currentSession().createCriteria(LinkEvenementTimesheet.class, LinkEvenementTimesheet.class.getName());
        ;
        try {

            criteria.createCriteria("absence", "a");
            //criteria.createCriteria("mission", "m");
            criteria.createCriteria("a.collaborateur", "c");
            criteria.add(Restrictions.isNotNull(LinkEvenementTimesheet.PROP_ABSENCE));

            Calendar c = Calendar.getInstance();
            c.setTime(dateDebut);
            c.add(Calendar.DAY_OF_YEAR, -1);
            dateDebut = new Date(c.getTimeInMillis());

            criteria.add(Restrictions.ge(LinkEvenementTimesheet.PROP_DATE, dateDebut));
            criteria.add(Restrictions.lt(LinkEvenementTimesheet.PROP_DATE, dateFin));

            criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("c.id"))
                    .add(Projections.property(LinkEvenementTimesheet.PROP_MOMENTJOURNEE))
                    .add(Projections.groupProperty(LinkEvenementTimesheet.PROP_DATE))
                    .add(Projections.rowCount()));

            criteria.addOrder(Order.asc(LinkEvenementTimesheet.PROP_COLLABORATEUR));
            //return criteria.list();
        } catch (Exception e) {
            logger.error("[absence collaborator] between dates {} : {}", dateDebut, dateFin, e);
        }

        return criteria.list();
    }

    @Override
    public List<Object[]> getDataForfait(Date dateDebut, Date dateFin, String typeForfait) {

        Criteria criteria = currentSession().createCriteria(LinkEvenementTimesheet.class, LinkEvenementTimesheet.class.getName());

        criteria.createCriteria("mission", "m");
        criteria.createCriteria("m.collaborateur", "c");
        criteria.createCriteria("m." + Mission.PROP_LISTE_MISSION_FORFAIT, "lmf");
        criteria.createCriteria("lmf." + LinkMissionForfait.PROP_FORFAIT, "f");
        criteria.createCriteria("f." + Forfait.PROP_FREQUENCE, "nu");


// ------------------ Mettre le type de forfait et la fréquence voulus -------------------
        Forfait typeForfaitRepas;
        Frequence frequence;
        Forfait[] colTypeForfait;
        switch (typeForfait) {
            case "repas":
                typeForfaitRepas = forfaitService.get(new Long(1));
                frequence = frequenceService.get(new Long(2));
                //Collaborator idCollab=collaborateurService.get(new Long(95));
                criteria.add(Restrictions.eq("lmf." + LinkMissionForfait.PROP_FORFAIT, typeForfaitRepas));
                criteria.add(Restrictions.eq("f." + Forfait.PROP_FREQUENCE, frequence));
                criteria.add(Restrictions.eq(LinkEvenementTimesheet.PROP_MOMENTJOURNEE, 0));
                //criteria.add(Restrictions.eq("c.id",idCollab));
                break;

            case "mensuel":
                colTypeForfait = new Forfait[]{
                        forfaitService.get(new Long(3)),
                        forfaitService.get(new Long(6))
                };
                criteria.add(Restrictions.in("lmf." + LinkMissionForfait.PROP_FORFAIT, colTypeForfait));
                frequence = frequenceService.get(new Long(4));
                criteria.add(Restrictions.eq("f." + Forfait.PROP_FREQUENCE, frequence));
                criteria.add(Restrictions.eq(LinkEvenementTimesheet.PROP_MOMENTJOURNEE, 0));
                break;

            case "autre":
                //on prend tous les autres type de forfait
                colTypeForfait = new Forfait[]{
                        forfaitService.get(new Long(2)),
                        forfaitService.get(new Long(4)),
                        forfaitService.get(new Long(5))
                };
                criteria.add(Restrictions.in("lmf." + LinkMissionForfait.PROP_FORFAIT, colTypeForfait));
                criteria.add(Restrictions.eq(LinkEvenementTimesheet.PROP_MOMENTJOURNEE, 0));
                break;
        }
        ;

        criteria.add(Restrictions.isNull(LinkEvenementTimesheet.PROP_ABSENCE));
        Calendar c = Calendar.getInstance();
        c.setTime(dateDebut);
        c.add(Calendar.DAY_OF_YEAR, -1);
        dateDebut = new Date(c.getTimeInMillis());

        criteria.add(Restrictions.ge(LinkEvenementTimesheet.PROP_DATE, dateDebut));
        criteria.add(Restrictions.lt(LinkEvenementTimesheet.PROP_DATE, dateFin));
        criteria.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("c.id"))
                .add(Projections.property("nu." + Frequence.PROP_ID))
                .add(Projections.property("lmf." + LinkMissionForfait.PROP_MONTANT))
                .add(Projections.rowCount())
                .add(Projections.groupProperty("f." + Forfait.PROP_ID))
                .add(Projections.groupProperty("m." + Mission.PROP_ID))
                .add(Projections.groupProperty(LinkEvenementTimesheet.PROP_MOMENTJOURNEE)));

        criteria.addOrder(Order.asc("c." + Collaborator.PROP_NOM));


        return criteria.list();
    }


}
