/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.TVAAsJson;

import java.util.List;

/**
 * The type Tva form.
 */
public class TVAForm {

    private List<TVAAsJson> listTVA;

    /**
     * Gets tva.
     *
     * @return the tva
     */
    public List<TVAAsJson> getlistTVA() {
        return listTVA;
    }

    /**
     * Sets tva.
     *
     * @param pListTVA the p list tva
     */
    public void setlistTVA(List<TVAAsJson> pListTVA) {
        this.listTVA = pListTVA;
    }
}
