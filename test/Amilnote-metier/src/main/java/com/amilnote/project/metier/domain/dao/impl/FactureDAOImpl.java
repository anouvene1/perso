/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.FactureDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.services.EtatService;
import com.amilnote.project.metier.domain.services.FactureArchiveeService;
import com.amilnote.project.metier.domain.services.TvaService;
import com.amilnote.project.metier.domain.services.TypeFactureService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.*;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.amilnote.project.metier.domain.entities.Collaborator.PROP_STATUT;
import static com.amilnote.project.metier.domain.entities.Commande.PROP_FACTURE;
import static com.amilnote.project.metier.domain.entities.Mission.PROP_COLLABORATEUR;
import static com.amilnote.project.metier.domain.entities.Mission.PROP_MISSION;
import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.PROP_CODE;

/**
 * The type Facture dao.
 */
@Repository("factureDAO")
public class FactureDAOImpl extends AbstractDAOImpl<Facture> implements FactureDAO {

    private static final Logger logger = LogManager.getLogger(FactureDAOImpl.class);

    @Autowired
    private EtatService etatService;
    @Autowired
    private TypeFactureService typeFactureService;
    @Autowired
    private TvaService tvaService;
    @Autowired
    private FactureArchiveeService factureArchiveeService;


    private static final String ETAT_ARCHIVE = "AR";
    private static final String ETAT_SOUMIS_CODE = "SO";
    private static final String ETAT_GENERE = "GN";
    private static final String ETAT_EDITE = "ED";

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Facture> getReferenceClass() {
        return Facture.class;
    }

    /**
     * {@linkplain FactureDAO#findByMissionByMonth(Mission, DateTime)}
     */
    @Override
    public Facture findByMissionByMonth(Mission pMission, DateTime pDate) {
        Criteria criteria = getCriteria();
        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));
        criteria.add(Restrictions.eq(Facture.PROP_MISSION, pMission));

        // Toute les factures de la mission en cours pendants la période
        int mois = pDate.getMonthOfYear();
        int annee = pDate.getYear();
        criteria.add(Restrictions.sqlRestriction("MONTH(" + Facture.SQL_DATE_FACTURATION + ") = " + mois));
        criteria.add(Restrictions.sqlRestriction("YEAR(" + Facture.SQL_DATE_FACTURATION + ") = " + annee));

        return (Facture) criteria.uniqueResult();
    }

    /**
     * {@linkplain FactureDAO#findByMissionBetweenDates(Mission, DateTime, DateTime)}
     */
    @Override
    public List<Facture> findByMissionBetweenDates(Mission pMission, DateTime pDateDebut, DateTime pDateFin) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Facture.PROP_MISSION, pMission));

        Date dateDebut = pDateDebut.dayOfMonth().withMinimumValue().toDate();
        Date dateFin = pDateFin.dayOfMonth().withMaximumValue().toDate();

        // Toute les factures en cours pendants la période
        criteria.add(Restrictions.ge(Facture.PROP_DATE_FACTURATION, dateDebut));
        criteria.add(Restrictions.le(Facture.PROP_DATE_FACTURATION, dateFin));

        return (List<Facture>) criteria.list();
    }

    /**
     * {@linkplain FactureDAO#createOrUpdateFacture(Facture)}
     */
    @Override
    public int createOrUpdateFacture(Facture facture) {
        int id = 0;
        Session session = null;
        try {
            FactureAsJson factureAsJson = facture.toJson();
            session = getSessionFactory().getCurrentSession();
            session.getTransaction().begin();
            // --- Sauvegarde et Commit de la mise à jour
            session.saveOrUpdate(facture);
            id = facture.getId();
            factureAsJson.setId(facture.getId());
            session.flush();
            session.getTransaction().commit();
        } catch (Exception e) {
            throw e;
        }

        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int createOrUpdateFactureAVANTREFONTE(FactureAsJson factureJson, Commande pCommande, Boolean edit) {
        Facture facture = new Facture();

        // On vérifie que l'id soit bien récupéré en cas de modification
        if (factureJson.getId() != 0) {
            //On récupère la facture grâce à l'ID
            facture = findUniqEntiteByProp(Facture.PROP_ID_FACTURE, factureJson.getId());
            // [JNA][AMNOTE 166] On récupère l'état de la facture
            if (factureJson.getEtat().getCode().equals(ETAT_ARCHIVE)) { // Si on à mis l'état à archivé
                // On récupère l'objet Etat voulu
                Etat etatFacture = etatService.getEtatByCode(ETAT_ARCHIVE);
                facture.setEtat(etatFacture); // On le laisse archivé
            } else { //SBE_AMNOTE_183 Dans le cas où la facture passe à l'état soumis
                if (factureJson.getEtat().getCode().equals(ETAT_SOUMIS_CODE)) {
                    Etat etatFacture = etatService.getEtatByCode(ETAT_SOUMIS_CODE);
                    facture.setEtat(etatFacture); // On le mets à soumis
                } else {
                    if (facture.getAdresseFacturation() == null && factureJson.getAdresseFacturation() == null) { // Si la facture n'a pas d'adresse de facturation s'est qu'elle vient d'être créée
                        //[JNA][AMNOTE 157] On est dans le cas d'une création donc l'état de la facture passe à "Généré"
                        Etat etatFacture = etatService.getEtatByCode(ETAT_GENERE);
                        facture.setEtat(etatFacture);
                    } else { // Si on est pas en archivage ni en création, on est en modification
                        // Si la facture en paramètre est à archiver, on met l'état de la facture à Archivée
                        if (factureJson.getEtat().getCode().equals("AR")) {
                            Etat etatFacture = etatService.getEtatByCode(ETAT_ARCHIVE);
                            facture.setEtat(etatFacture); // On le laisse archivé
                        } else {
                            //SBE_AMNOTE_183 Dans le cas où la facture soumise est refusée on la repasse à l'état généré
                            if (factureJson.getEtat().getCode().equals(ETAT_GENERE)) {
                                Etat etatFacture = etatService.getEtatByCode(ETAT_GENERE);
                                facture.setEtat(etatFacture); // On le mets à soumis
                            } else {
                                // On met la facture à létat "édité"
                                //[JNA][AMNOTE 165] Si c'est une édition manuel
                                if (edit) {
                                    Etat etatFacture = etatService.getEtatByCode(ETAT_EDITE);
                                    facture.setEtat(etatFacture); // On met l'état edit
                                } else { // Sinon est un calcul
                                    Etat etatFacture = etatService.getEtatByCode(ETAT_GENERE);
                                    facture.setEtat(etatFacture); // On met l'état à généré
                                }
                            }
                        }
                    }
                }
            }
        } else { // si on est en mode création

            //[JNA][AMNOTE 166] Si la facture à un état, on lui laisse
            if (factureJson.getEtat() != null) {
                facture.setEtat(factureJson.getEtat());
            } else { // Sinon on lui met "généré"
                Etat etatFacture = etatService.getEtatByCode("GN");
                facture.setEtat(etatFacture);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un id client, sinon on le met a 0
            if (factureJson.getIdClient() != 0) {
                facture.setIdClient(factureJson.getIdClient());
            } else {
                facture.setIdClient(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un contact, sinon on le met a 0
            if (factureJson.getContact() != 0) {
                facture.setContact(factureJson.getContact());
            } else {
                facture.setContact(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a une adresse de facturation, sinon on la met a null
            if (factureJson.getAdresseFacturation() != null) {
                facture.setAdresseFacturation(factureJson.getAdresseFacturation());
            } else {
                facture.setAdresseFacturation(null);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a une quantité, sinon on la met a 0
            if (factureJson.getQuantite() != 0) {
                facture.setQuantite(factureJson.getQuantite());
            } else {
                facture.setQuantite(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un prix, sinon on le met a 0
            if (factureJson.getPrix() != 0) {
                facture.setPrix(factureJson.getPrix());
            } else {
                facture.setPrix(0);
            }

            // [JNA][AMNOTE 142] On regarde s'il y a un montant, sinon on le met a 0
            if (factureJson.getMontant() != 0) {
                facture.setMontant(factureJson.getMontant());
            } else {
                facture.setMontant(0);
            }

            if (factureJson.getTVA() != null) {
                facture.setTVA(factureJson.getTVA());
            } else {
                facture.setTVA(tvaService.getTVAByCode(TVA.TAUX_NORMAL));
            }

        }

        // [JNA][AMNOTE 160] On regarde s'il y a une date de facturation
        if (factureJson.getDateFacture() != null) {
            facture.setDateFacture(factureJson.getDateFacture());
        } else {
            facture.setDateFacture(null);
        }

        // [JNA][AMNOTE 165] Si la facture en BD n'a pas de num mais que le facture Json en a un : on met à jour
        if (facture.getNumFacture() == 0 && factureJson.getNumFacture() != 0) {
            facture.setNumFacture(factureJson.getNumFacture());
        } else if (facture.getNumFacture() == 0 && factureJson.getNumFacture() == 0) {
            facture.setNumFacture(0);
        }

        // [JNA][AMNOTE 165] Si la facture en BD n'a pas de chemin de pdf mais que le facture Json en a un : on met à jour
        if (facture.getPdf() == null && factureJson.getPdf() != null) {
            if (!factureJson.getPdf().equals("")) {
                facture.setPdf(factureJson.getPdf());
            } else {
                facture.setPdf("");
            }
        } else if (factureJson.getPdf() != null) {
            if (!factureJson.getPdf().equals("")) {
                if (facture.getPdf() == null || facture.getPdf().equals("")) {
                    facture.setPdf(factureJson.getPdf());
                }
            } else {
                facture.setPdf("");
            }
        } else {
            facture.setPdf("");
        }

        if (factureJson.getMoisPrestations() != null) { // On récupère le mois des prestations
            facture.setMoisPrestations(factureJson.getMoisPrestations());
        } else {// Sinon on le met le mois en cours
            Date today = new Date();
            facture.setMoisPrestations(today);
        }

        if (pCommande != null && pCommande.getId() != 0) {
            facture.setCommande(pCommande);
        }

        facture.setTypeFacture(factureJson.getTypeFacture());
        facture.setIsAvoir(factureJson.getIsAvoir());
        if (factureJson.getNumFacture() != facture.getNumFacture()) {
            facture.setNumFacture(factureJson.getNumFacture());
        }

        if (factureJson.getCommentaire() == null && pCommande != null) {
            facture.setCommentaire("Frais - " + pCommande.getMission().getCollaborateur().getPrenom() + " " + pCommande.getMission().getCollaborateur().getNom() + " : ");
        }

        // Ajout du try catch pour faire remonter les exceptions
        try {
        } catch (Exception e) {
            throw e;
        }

        // --- Création de la session et de la transaction
        Session session = getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        // --- Sauvegarde et Commit de la mise à jour
        session.saveOrUpdate(facture);
        int id = facture.getId();
        session.flush();
        transaction.commit();
        factureJson.setId(facture.getId());
        return id;
    }

    /**
     * {@linkplain FactureDAO#deleteFacture(Facture)}
     */
    @Override
    public String deleteFacture(Facture pFacture) {
        if (null != pFacture) {

            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();

            Facture facture = (Facture) session.get(Facture.class, pFacture.getId());
            // --- Sauvegarde et Commit de la mise à jour
            //[JNA][AMNOTe 165] session.evict(pFacture) permet de vider l'objet du cache si on vient de le save pour pouvoir le delete
            if (facture != null) {
                session.evict(facture);
                session.delete(facture);
            } else {
                session.evict(pFacture);
                session.delete(pFacture);
            }
            session.flush();
            transaction.commit();
            return "Suppression effectuée avec succès";
        }
        return "Aucune suppression n'a été effectuée";
    }

    /**
     * Find invoices by type (Facturables, Main, Frais) and activity report.
     *
     * @param factureType            the type of invoices we want to.
     * @param startSearchDate        fist day of the month
     * @param endSearchDate          last day of the month
     * @param activityReportValidate Find if the invoice depends on validated RA or not.
     *                               By default, the integer RA for type Main and Frais is 0, because they doesn't depend on RA.
     *                               If raValide = 0 for Facturables the method returns only "RA non valides".
     * @return the list of invoices we want to by type and activity report.
     */
    @Override
    public List<Facture> findFacturesByTypeAndByRA(TypeFacture factureType, Date startSearchDate, Date endSearchDate, Integer... activityReportValidate) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, factureType));
        if (activityReportValidate != null && activityReportValidate.length > 0) {
            criteria.add(Restrictions.in(Facture.PROP_RA_VALIDE, activityReportValidate));
        }
        preparedCriteriaToSearchInvoicesDate(criteria, startSearchDate, endSearchDate);
        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFactures()}
     */
    @Override
    public List<Facture> findFactures() {
        List<Facture> factures = new ArrayList<>();
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc(Facture.PROP_ID_FACTURE));
        try {
            factures.addAll(criteria.list());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return factures;
    }

    /**
     * {@linkplain FactureDAO#findFacturables()}
     */
    @Override
    public List<Facture> findFacturables() {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        Criterion criterion = Restrictions.disjunction()
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, 1))
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, 2));
        criteria.add(criterion);
        criteria.addOrder(Order.asc(Facture.PROP_ID_FACTURE));
        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturesRANonSoumis()}
     */
    @Override
    public List<Facture> findFacturesRANonSoumis() {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        criteria.add(Restrictions.eq(Facture.PROP_RA_VALIDE, 0));
        criteria.addOrder(Order.asc(Facture.PROP_ID_FACTURE));
        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturesFrais()}
     */
    @Override
    public List<Facture> findFacturesFrais() {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURE_FRAIS);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        criteria.addOrder(Order.asc(Facture.PROP_ID_FACTURE));
        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturablesByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturablesByMonth(DateTime pDateVoulue) throws Exception {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        Criterion criterion = Restrictions.disjunction()
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, -1))
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, 1))
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, 2));
        criteria.add(criterion);

        Etat etatArchive = etatService.getEtatByCode("AR");

        criteria.add(Restrictions.ne(Facture.PROP_ETAT, etatArchive));

        Date dateDebut = pDateVoulue.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDateVoulue.getMonthOfYear() == 12) {
            dateFin = pDateVoulue.withMonthOfYear(01).withYear(pDateVoulue.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDateVoulue.withMonthOfYear(pDateVoulue.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = simpleDateFormat.parse(dateD);
        Date pDateFin = simpleDateFormat.parse(dateF);

        criteria.add(Restrictions.ge(Facture.PROP_DATE_FACTURATION, pDateDebut));
        criteria.add(Restrictions.lt(Facture.PROP_DATE_FACTURATION, pDateFin));

        criteria.addOrder(Order.asc(Facture.PROP_NUMFACTURE));

        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturesRANonSoumisByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesRANonSoumisByMonth(DateTime pDateVoulue) throws Exception {
        Criteria criteria = getCriteria();
        // [AMNT-775] Taking all invoices by invoice type and date with an activity report not submitted ( i.e Facture.PROP_RA_VALIDE = 0 )
        // and those having been canceled with an activity report already validated ( i.e Facture.PROP_RA_VALIDE = -1 )
        Criterion criterion = Restrictions.disjunction()
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, -1))
                .add(Restrictions.eq(Facture.PROP_RA_VALIDE, 0));
        criteria.add(criterion);

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        Date dateDebut = pDateVoulue.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDateVoulue.getMonthOfYear() == 12) {
            dateFin = pDateVoulue.withMonthOfYear(01).withYear(pDateVoulue.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDateVoulue.withMonthOfYear(pDateVoulue.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = simpleDateFormat.parse(dateD);
        Date pDateFin = simpleDateFormat.parse(dateF);

        criteria.add(Restrictions.ge(Facture.PROP_DATE_FACTURATION, pDateDebut));
        criteria.add(Restrictions.lt(Facture.PROP_DATE_FACTURATION, pDateFin));

        criteria.addOrder(Order.asc(Facture.PROP_NUMFACTURE));
        return (List<Facture>) criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturesFraisByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesFraisByMonth(DateTime pDateVoulue) throws Exception {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURE_FRAIS);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        Etat etatArchive = etatService.getEtatByCode("AR");

        criteria.add(Restrictions.ne(Facture.PROP_ETAT, etatArchive));

        Date dateDebut = pDateVoulue.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDateVoulue.getMonthOfYear() == 12) {
            dateFin = pDateVoulue.withMonthOfYear(01).withYear(pDateVoulue.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDateVoulue.withMonthOfYear(pDateVoulue.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = simpleDateFormat.parse(dateD);
        Date pDateFin = simpleDateFormat.parse(dateF);

        criteria.add(Restrictions.ge(Facture.PROP_DATE_FACTURATION, pDateDebut));
        criteria.add(Restrictions.lt(Facture.PROP_DATE_FACTURATION, pDateFin));

        criteria.addOrder(Order.asc(Facture.PROP_NUMFACTURE));

        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findFacturesALaMainByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesALaMainByMonth(DateTime pDateVoulue) throws Exception {
        Criteria criteria = getCriteria();

        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN);
        criteria.add(Restrictions.eq(Facture.PROP_TYPE_FACTURE, typeFacture));

        Etat etatArchive = etatService.getEtatByCode("AR");

        criteria.add(Restrictions.ne(Facture.PROP_ETAT, etatArchive));

        Date dateDebut = pDateVoulue.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDateVoulue.getMonthOfYear() == 12) {
            dateFin = pDateVoulue.withMonthOfYear(01).withYear(pDateVoulue.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDateVoulue.withMonthOfYear(pDateVoulue.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.DATE_FORMAT_YYYY_MM_DD);

        String dateD = simpleDateFormat.format(dateDebut);
        String dateF = simpleDateFormat.format(dateFin);

        Date pDateDebut = simpleDateFormat.parse(dateD);
        Date pDateFin = simpleDateFormat.parse(dateF);

        criteria.add(Restrictions.ge(Facture.PROP_DATE_FACTURATION, pDateDebut));
        criteria.add(Restrictions.lt(Facture.PROP_DATE_FACTURATION, pDateFin));

        criteria.addOrder(Order.asc(Facture.PROP_NUMFACTURE));

        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#findMaxNumFacture()}
     */
    @Override
    public int findMaxNumFacture() {
        Criteria criteria = getCriteria();

        ProjectionList projectionList = Projections.projectionList();
        projectionList = projectionList.add(Projections.max(Facture.PROP_NUMFACTURE));
        criteria = criteria.setProjection(projectionList);
        return (int) criteria.uniqueResult();
    }

    /**
     * {@linkplain FactureDAO#findAllFactures()}
     */
    @Override
    public List<Facture> findAllFactures() {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.ne(Facture.PROP_NUMFACTURE, 0));
        criteria.addOrder(Order.asc(Facture.PROP_NUMFACTURE));

        return criteria.list();
    }

    /**
     * {@linkplain FactureDAO#checkExistsNumFacture(int, int)}
     */
    @Override
    public boolean checkExistsNumFacture(int numFacture, int idFacture) throws IOException {
        boolean result = false;

        // On vérifier que le numéro de facture n'est pas déjà donné à une facture du mois couran
        List<Facture> facturesExistantes = findAllFactures();
        for (Facture factureExistante : facturesExistantes) {
            if (idFacture != factureExistante.getId() && numFacture == factureExistante.getNumFacture() && numFacture != 0) {
                result = true;
                throw new IOException("Numéro de facture déjà existant");
            }
        }

        // On vérifie que le numéro de facture n'est pas déjà donné à une facture archivée
        List<FactureArchivee> facturesArchiveesExistantes = factureArchiveeService.findAllFacturesArchivees();
        for (FactureArchivee factureArchiveeExistante : facturesArchiveesExistantes) {
            if (idFacture != factureArchiveeExistante.getId() && numFacture == factureArchiveeExistante.getNumero() && numFacture != 0) {
                result = true;
                throw new IOException("Numéro de facture déjà existant");
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Facture> findByCommande(Commande pCommande) {
        return findListEntitesByProp(Facture.PROP_COMMANDE, pCommande);
    }

    /**
     * Method for retrieving the list of all invoices whose date is in a given month, and whose state is part of a given set of states
     *
     * @param dateOfSearch          the date of the month of search
     * @param propertyNameToOrderBy the result list will be ordered by this property
     * @param codesEtatsFactures    list of invoices states codes, "BR" Brouillon, "SO" Soumis, "VA" Validé, etc.
     * @return all invoices whose date is in a given month, and whose state is part of a given set of states
     */
    @Override
    public List<Facture> findFactureByMonthAndState(DateTime dateOfSearch, String propertyNameToOrderBy, String... codesEtatsFactures) {
        Criteria criteria = getCriteria();

        // Restriction par rapport à l'état de la facture
        if (codesEtatsFactures != null && codesEtatsFactures.length > 0) {
            criteria.createAlias(Facture.PROP_ETAT, "e");
            criteria.add(Restrictions.in("e.code", codesEtatsFactures)); // on vérifie si le code état de la facture fait partie des codes états factures entrés en paramètres
        }

        // Restriction par rapport à la date, ici la date de la facture doit être incluse dans un mois donné
        int mois = dateOfSearch.getMonthOfYear();
        int annee = dateOfSearch.getYear();
        criteria.add(Restrictions.sqlRestriction("MONTH(" + Facture.SQL_DATE_FACTURATION + ") = " + mois));
        criteria.add(Restrictions.sqlRestriction("YEAR(" + Facture.SQL_DATE_FACTURATION + ") = " + annee));


        // Ordonner la liste de résultat selon un critère, un champs de table
        if (propertyNameToOrderBy != null) {
            criteria.addOrder(Order.asc(propertyNameToOrderBy));
        }

        // Après avoir définit les critères on soumets puis retourne les résultat sous forme d'une liste
        return criteria.list();
    }

    /**
     * To prepare criteria to find invoices by first and last days of wanted month
     *
     * @param criteria        the current criteria
     * @param startSearchDate first day of the month
     * @param endSearchDate   last day of the month
     * @return criteria, the wanted date
     */
    private Criteria preparedCriteriaToSearchInvoicesDate(Criteria criteria, Date startSearchDate, Date endSearchDate) {
        criteria.add(Restrictions.between(Facture.PROP_DATE_FACTURATION, startSearchDate, endSearchDate));
        return criteria;
    }

    /**
     * Method for retrieving all invoices by given collaborator status code and one or multiple {@link SearchCriteria}
     *
     * @param collaborateurStatusCode the status code of the collaborator
     * @param orderingProperty        the resulting list will be ordered by this property
     * @param isAscendingOrdering     is ordering ascending or descending
     * @param searchCriterias         one or multiple (varargs) {@link SearchCriteria}
     * @return a list of all invoices corresponding to a given collaborator status code
     */
    @Override
    public List<Facture> findByCollaboratorStatusCodeAndSearchCriterias(String collaborateurStatusCode, String orderingProperty, Boolean isAscendingOrdering, SearchCriteria... searchCriterias) {
        Criteria criteria = getCurrentSession().createCriteria(Facture.class, PROP_FACTURE);
        criteria.createAlias(PROP_FACTURE + "." + PROP_MISSION, PROP_MISSION); // inner join by default
        criteria.createAlias(PROP_MISSION + "." + PROP_COLLABORATEUR, PROP_COLLABORATEUR);
        criteria.createAlias(PROP_COLLABORATEUR + "." + PROP_STATUT, PROP_STATUT);
        criteria.add(Restrictions.eq(PROP_STATUT + "." + PROP_CODE, collaborateurStatusCode));
        if (searchCriterias != null) {
            for (SearchCriteria searchCriteria : searchCriterias) {
                addSearchCriteriaRestrictions(criteria, searchCriteria);
            }
        }
        addOrder(criteria, orderingProperty, isAscendingOrdering);
        return criteria.list();
    }

    /**
     * Method for retrieving all facture from missions list
     * {@linkplain FactureDAO#findBillByMissionList(List)}
     *
     * @param missions list of missions
     * @return list of {@link Facture}
     */
    @Override
    public List<Facture> findBillByMissionList(List<Mission> missions) {
        Criteria criteria = getCurrentSession().createCriteria(Facture.class, PROP_FACTURE)
                .add(Restrictions.in(PROP_FACTURE + "." + PROP_MISSION, missions));
        return criteria.list();
    }

}
