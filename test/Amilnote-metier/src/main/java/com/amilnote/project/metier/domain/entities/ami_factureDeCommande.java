/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ami_factureDeCommandeAsJson;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * The type Ami facture de commande.
 */
@Entity
@Immutable
public class ami_factureDeCommande implements java.io.Serializable {

    /**
     * The constant PROP_ID_FACTURE.
     */
    public static final String PROP_ID_FACTURE = "id_facture";
    /**
     * The constant PROP_NUM_COMMANDE.
     */
    public static final String PROP_NUM_COMMANDE = "num_facture";
    /**
     * The constant PROP_NOM_SOCIETE.
     */
    public static final String PROP_NOM_SOCIETE = "nom_societe";
    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_COLLABORATEUR = "collaborateur";
    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";
    /**
     * The constant PROP_NUMERO.
     */
    public static final String PROP_NUMERO = "numero";
    /**
     * The constant PROP_QUANTITE.
     */
    public static final String PROP_QUANTITE = "quantite";
    /**
     * The constant PROP_PRIX.
     */
    public static final String PROP_PRIX = "prix";
    /**
     * The constant PROP_MONTANT.
     */
    public static final String PROP_MONTANT = "montant";
    /**
     * The constant PROP_MOIS_PRESTATIONS.
     */
    public static final String PROP_MOIS_PRESTATIONS = "mois_prestations";
    /**
     * The constant PROP_Etat.
     */
    public static final String PROP_Etat = "etat";
    /**
     *
     */
    public static final String PROP_MOIS_RAPPORT = "mois_rapport";

    public static final String PROP_LIBELLE = "libelle";

    @Id
    @Column(name = "id_facture")
    private int id_facture;

    @Column(name = "num_facture")
    private int num_facture;

    @Column(name = "nom_societe")
    private String nom_societe;

    @Column(name = "collaborateur")
    private String collaborateur;

    @Column(name = "mission")
    private String mission;

    @Column(name = "numero")
    private String numero;

    @Column(name = "quantite")
    private float quantite;

    @Column(name = "prix")
    private float prix;

    @Column(name = "montant")
    private float montant;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "mois_prestations")
    private Date mois_prestations;

    @Column(name = "etat")
    private int etat;

    @Column(name="mois_rapport")
    private Date mois_rapport;

    @Column(name = "contact")
    private long contact;

    /**
     * Instantiates a new Ami facture de commande.
     */
    public ami_factureDeCommande() {
        super();
    }

    /**
     * Instantiates a new Ami facture de commande.
     *
     * @param mois_rapport     the mois du rapport
     * @param id_facture       the id facture
     * @param num_facture      the num facture
     * @param nom_societe      the nom societe
     * @param collaborateur    the collaborateur
     * @param mission          the mission
     * @param numero           the numero
     * @param quantite         the quantite
     * @param prix             the prix
     * @param montant          the montant
     * @param mois_prestations the mois prestations
     * @param etat             the etat
     * @param libelle          the libelle
     */
    public ami_factureDeCommande(int id_facture,
                                 int num_facture,
                                 String nom_societe,
                                 String collaborateur,
                                 String mission,
                                 String numero,
                                 float quantite,
                                 float prix,
                                 float montant,
                                 String libelle,
                                 Date mois_prestations,
                                 int etat,Date mois_rapport) {

        super();
        this.id_facture = id_facture;
        this.num_facture = num_facture;
        this.nom_societe = nom_societe;
        this.collaborateur = collaborateur;
        this.mission = mission;
        this.numero = numero;
        this.quantite = quantite;
        this.prix = prix;
        this.montant = montant;
        this.libelle = libelle;
        this.mois_prestations = mois_prestations;
        this.etat = etat;
        this.mois_rapport=mois_rapport;
    }

    /**
     * To json ami facture de commande as json.
     *
     * @return the ami facture de commande as json
     */
    public ami_factureDeCommandeAsJson toJson() {
        return new ami_factureDeCommandeAsJson(this);
    }


    /**
     * Gets id facture.
     *
     * @return the id facture
     */
    public int getId_facture() {
        return id_facture;
    }

    /**
     * Sets id facture.
     *
     * @param id_facture the id facture
     */
    public void setId_facture(int id_facture) {
        this.id_facture = id_facture;
    }

    /**
     * Gets num facture.
     *
     * @return the num facture
     */
    public int getNum_facture() {
        return num_facture;
    }

    /**
     * Sets num facture.
     *
     * @param num_facture the num facture
     */
    public void setNum_facture(int num_facture) {
        this.num_facture = num_facture;
    }

    /**
     * Gets nom societe.
     *
     * @return the nom societe
     */
    public String getNom_societe() {
        return nom_societe;
    }

    /**
     * Sets nom societe.
     *
     * @param nom_societe the nom societe
     */
    public void setNom_societe(String nom_societe) {
        this.nom_societe = nom_societe;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public String getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborateur the collaborateur
     */
    public void setCollaborateur(String collaborateur) {
        this.collaborateur = collaborateur;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets numero.
     *
     * @param numero the numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param quantite the quantite
     */
    public void setQuantite(float quantite) {
        this.quantite = quantite;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param prix the prix
     */
    public void setPrix(float prix) {
        this.prix = prix;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param montant the montant
     */
    public void setMontant(float montant) {
        this.montant = montant;
    }

    /**
     * Gets libelle
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets libelle
     * @param libelle le libellé
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets mois prestations.
     *
     * @return the mois prestations
     */
    public Date getMois_prestations() {
        return mois_prestations;
    }

    /**
     * Sets mois prestations.
     *
     * @param mois_prestations the mois prestations
     */
    public void setMois_prestations(Date mois_prestations) {
        this.mois_prestations = mois_prestations;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public int getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param etat the etat
     */
    public void setEtat(int etat) {
        this.etat = etat;
    }

    /**
     *
     * @return le mois du rapport
     */
    public Date getMois_rapport() {
        return mois_rapport;
    }

    /***
     *
     * @param mois_rapport le mois du rapport
     */
    public void setMois_rapport(Date mois_rapport) {
        this.mois_rapport = mois_rapport;
    }

    /**
     *
     * @return la propriété mois du rapport
     */
    public static String getPropMoisRapport() {
        return PROP_MOIS_RAPPORT;
    }

    public long getContact() {
        return contact;
    }

    public void setContact(long contact) {
        this.contact = contact;
    }
}
