/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RaisonSociale;

import java.util.Date;

/**
 * The type Collaborateur as json.
 *
 * @author LSouai
 */
public class CollaboratorAsJson extends TravailleurAsJson {

    private CollaboratorAsJson manager;
    private Date dateNaissance;
    private Date dateEntree;
    private Date dateSortie;
    private String mailPerso;
    private String statutGeneral;
    private boolean exclusAddvise;
    private RaisonSocialeAsJson raisonSociale;

    /**
     * Constructeur par défaut de la classe Collaborateur
     */
    public CollaboratorAsJson() {
    }

    /**
     * Constructor
     *
     * @param collaborator collaborator entity
     */
    public CollaboratorAsJson(Collaborator collaborator) {
        super(collaborator.getId(), collaborator.getNom(),
            collaborator.getPrenom(), collaborator.getMail(),
            collaborator.getPassword(), collaborator.getTelephone(),
            collaborator.isEnabled(),
            collaborator.getPoste()
                .toJson(),
            collaborator.getStatut()
                .toJson(),
            collaborator.getAdressePostale(),
            collaborator.getCivilite(),
            collaborator.getAgency().ordinal());

        this.setDateNaissance(collaborator.getDateNaissance());
        this.setDateEntree(collaborator.getDateEntree());
        this.setDateSortie(collaborator.getDateSortie());

        Collaborator tmpManager = collaborator.getManager();
        if (null != tmpManager){
            this.setManager(tmpManager.toJson());
        }
        this.setMailPerso(collaborator.getMailPerso());
        this.setStatutGeneral(collaborator.getStatutGeneral());
        this.setExclusAddvise(collaborator.getExclusAddvise());

        RaisonSociale resSTemp = collaborator.getRaisonSociale();
        if (null != resSTemp)
            this.setRaisonSociale(resSTemp.toJson());
    }

    /**
     * Gets manager.
     *
     * @return the manager
     */
    public CollaboratorAsJson getManager() {
        return manager;
    }

    /**
     * Sets manager.
     *
     * @param pManager the manager to set
     */
    public void setManager(CollaboratorAsJson pManager) {
        manager = pManager;
    }

    /**
     * Gets date naissance.
     *
     * @return the date of birth
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Sets date naissance.
     *
     * @param dateNaissance the date of birth to set
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     * Gets date entree.
     *
     * @return the hiring date
     */
    public Date getDateEntree() {
        return dateEntree;
    }

    /**
     * Sets date entree.
     *
     * @param dateEntree the hiring date to set
     */
    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    /**
     * Gets date sortie.
     *
     * @return the dismiss date
     */
    public Date getDateSortie() {
        return dateSortie;
    }

    /**
     * Sets date sortie.
     *
     * @param dateSortie the dismiss date to set
     */
    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    /**
     * Gets mail perso.
     *
     * @return mail perso the dismiss date to set
     */
    public String getMailPerso() {
        return mailPerso;
    }

    /**
     * Sets mail perso.
     *
     * @param mailPerso the dismiss date to set
     */
    public void setMailPerso(String mailPerso) {
        this.mailPerso = mailPerso;
    }

    /**
     * Gets statut general.
     *
     * @return statutGeneral statut general
     */
    public String getStatutGeneral() {
        return statutGeneral;
    }

    /**
     * Sets statut general.
     *
     * @param statutGeneral the dismiss date to set
     */
    public void setStatutGeneral(String statutGeneral) {
        this.statutGeneral = statutGeneral;
    }

    public void setExclusAddvise(boolean exclusAddvise) {
        this.exclusAddvise = exclusAddvise;
    }

    public boolean getExclusAddvise() {
        return exclusAddvise;
    }

    public RaisonSocialeAsJson getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(RaisonSocialeAsJson raisonSociale) {
        this.raisonSociale = raisonSociale;
    }
}
