/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Link frais justif.
 */
@Entity
@Table(name = "ami_link_justif_frais")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class LinkFraisJustif implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_JUSTIF.
     */
    public static final String PROP_JUSTIF = "justif";
    /**
     * The constant PROP_FRAIS.
     */
    public static final String PROP_FRAIS = "frais";
    private static final long serialVersionUID = 2290863870942055022L;
    private Long id;
    private Justif justif;
    private Frais frais;

    /**
     * Constructeur par defaut de la classe TypeFrais
     */
    public LinkFraisJustif() {
    }

	/* 
     * GETTERS AND SETTERS
	 */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets justif.
     *
     * @return the justif
     */
    @ManyToOne
    @JoinColumn(name = "id_justif", nullable = false)
    public Justif getJustif() {
        return justif;
    }

    /**
     * Sets justif.
     *
     * @param pJustif the p justif
     */
    public void setJustif(Justif pJustif) {
        justif = pJustif;
    }

    /**
     * Gets frais.
     *
     * @return the frais
     */
    @ManyToOne
    @JoinColumn(name = "id_frais", nullable = false)
    public Frais getFrais() {
        return frais;
    }

    /**
     * Sets frais.
     *
     * @param pFrais the p frais
     */
    public void setFrais(Frais pFrais) {
        frais = pFrais;
    }
}
