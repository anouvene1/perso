/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.AddviseFormateurDAO;
import com.amilnote.project.metier.domain.entities.AddviseFormateur;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("addviseFormateurDAO")
public class AddviseFormateurDAOImpl extends AbstractDAOImpl<AddviseFormateur> implements AddviseFormateurDAO {

    @Override
    protected Class<AddviseFormateur> getReferenceClass() {
        return AddviseFormateur.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int createOrUpdateAddviseFormateur(AddviseFormateur addviseFormateur) {

        saveOrUpdate(addviseFormateur);
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AddviseFormateur findById(int id) {
        return (AddviseFormateur) currentSession().get(AddviseFormateur.class, id);
    }
}
