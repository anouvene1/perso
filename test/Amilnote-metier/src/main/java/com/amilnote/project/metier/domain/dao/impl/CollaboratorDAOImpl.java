/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.CollaboratorDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Poste;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Classe DAO pour la recherche d'un utilisateur en BDD
 */
@Repository("collaboratorDAO")
public class CollaboratorDAOImpl extends TravailleurDAOImpl<Collaborator> implements CollaboratorDAO {

    @Override
    protected Class<Collaborator> getReferenceClass() {
        return Collaborator.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborator> findCollaborators(boolean isWithAdmin, boolean isWithDisabled) {
        List<Collaborator> collaborators = new ArrayList<>();
        Criteria criteria;
        if (isWithAdmin) {
            criteria = reqAll(isWithDisabled);
        } else {
            criteria = reqWithoutAdmin(isWithDisabled);
        }
        addResultToList(collaborators, criteria);
        return collaborators;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborator> findTravailleurs() {
        return findCollaborators(true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborator> findCollaboratorsByResponsable(Collaborator responsable) {
        List<Collaborator> collaborators = new ArrayList<>();
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborator.PROP_MANAGER, responsable));
        criteria.addOrder(Order.asc(Collaborator.PROP_NOM));
        addResultToList(collaborators, criteria);
        return collaborators;
    }

    /**
     * {@linkplain CollaboratorDAO#findAllManagers(StatutCollaborateur, Poste)}
     */
    @Override
    public List<Collaborator> findAllManagers(StatutCollaborateur statutManager, Poste posteManager) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.or(Restrictions.eq(Collaborator.PROP_STATUT, statutManager), Restrictions.eq(Collaborator.PROP_POSTE, posteManager)));
        return criteria.list();
    }

    /**
     * {@linkplain CollaboratorDAO#findAllResponsablesTechnique(StatutCollaborateur)}
     */
    @Override
    public List<Collaborator> findAllResponsablesTechnique(StatutCollaborateur statutResponsableTechnique) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborator.PROP_STATUT, statutResponsableTechnique));
        return criteria.list();
    }

    /**
     * {@linkplain CollaboratorDAO#findAllChefDeProjet(StatutCollaborateur)}
     */
    @Override
    public List<Collaborator> findAllChefDeProjet(StatutCollaborateur statutChefDeProjet) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborator.PROP_STATUT, statutChefDeProjet));
        return criteria.list();
    }

    /**
     * @param collaborateurs a list of {@link Collaborator}
     * @param pDate          date
     */
    @Override
    protected final void removeDisabledLastMonth(List<Collaborator> collaborateurs, Date pDate) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(pDate.getTime());
        List<Collaborator> collabsDisabledInPast = new ArrayList<>();
        // BAB - 21/06/2016  [AMILTONE 100] : on recupere tous les collabs
        for (Collaborator collab : collaborateurs) {
            Calendar dateSortie = GregorianCalendar.getInstance();
            // on verifie que la date de sortie soit non nulle
            if (collab.getDateSortie() != null && !collab.isEnabled()) {
                dateSortie.setTimeInMillis(collab.getDateSortie().getTime());
                // on filtre sur l'annee
                if (dateSortie.get(Calendar.YEAR) < calendar.get(Calendar.YEAR)) {
                    collabsDisabledInPast.add(collab);
                } else if (dateSortie.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
                    // BAB 30/06/2016 AMNOTE-113 si on supprime un collab l'année en cours on compare les mois des 2 dates
                    if (dateSortie.get(Calendar.MONTH) < calendar.get(Calendar.MONTH)) {
                        collabsDisabledInPast.add(collab);
                    }
                }
            }
        }
        // si des collabs ont été supprimés dans le passé ils sont supprimés
        if (!collabsDisabledInPast.isEmpty()) {
            collaborateurs.removeAll(collabsDisabledInPast);
        }
    }

    /**
     * @param collaborateurs a list of {@link Collaborator}
     * @param yearFrais      year
     */
    protected final void removeDisabledLastYear(List<Collaborator> collaborateurs, int yearFrais) {
        List<Collaborator> collabsDisabledInPast = new ArrayList<>();
        // BAB - 21/06/2016  [AMILTONE 100] : on recupere tous les collabs
        for (Collaborator collab : collaborateurs) {
            Date dateEntree = collab.getDateEntree();
            Date dateSortie = collab.getDateSortie();
            Calendar calendarEntree = GregorianCalendar.getInstance();
            Calendar calendarSortie = GregorianCalendar.getInstance();
            // on verifie que la date de sortie soit non nulle
            if (dateSortie != null) {
                calendarSortie.setTimeInMillis(dateSortie.getTime());
                // on filtre sur l'annee
                if (calendarSortie.get(Calendar.YEAR) < yearFrais) {
                    collabsDisabledInPast.add(collab);
                }
            }
            //On supprime les collab ajoutés les années suivant celle recherchée
            if (dateEntree != null) {
                calendarEntree.setTimeInMillis(dateEntree.getTime());
                if (calendarEntree.get(Calendar.YEAR) > yearFrais) {
                    collabsDisabledInPast.add(collab);
                }
            }
        }
        // si des collabs ont été supprimés dans le passé ils sont supprimés
        if (!collabsDisabledInPast.isEmpty()) {
            collaborateurs.removeAll(collabsDisabledInPast);
        }
    }

    /**
     * Get a list collaborators by status and enabled or not
     *
     * @param enabled Enable collaborator status
     * @param status the array of status
     * @return a list of collaborators
     */
    @Override
    public List<Collaborator> findCollaboratorByStatus(boolean enabled, StatutCollaborateur... status){
        Criteria criteria = getCriteria();
        Disjunction or = Restrictions.disjunction();

        for(StatutCollaborateur statutCollaborator : status){
            or.add(Restrictions.eq(Collaborator.PROP_STATUT, statutCollaborator));
        }
        criteria.add(or);

        if(enabled){
            criteria.add(Restrictions.eq(Collaborator.PROP_ENABLED, enabled));
        }

        return criteria.list();
    }

}
