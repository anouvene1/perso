/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.AddviseFormateur;

public interface AddviseFormateurDAO extends LongKeyDAO<AddviseFormateur> {

    /**
     * Permet la sauvegarde d'un AddviseFormateur nouveau ou existant
     *
     * @param addviseFormateur AddviseFormateur
     * @return 0
     */
    int createOrUpdateAddviseFormateur(AddviseFormateur addviseFormateur);

    /**
     * retourne un formateur à partir de son id
     *
     * @param id Long
     * @return AddviseFormateur
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    AddviseFormateur findById(int id);
}
