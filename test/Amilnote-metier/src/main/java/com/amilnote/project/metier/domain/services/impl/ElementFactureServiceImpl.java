/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.ElementFactureDAOImpl;
import com.amilnote.project.metier.domain.entities.ElementFacture;
import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.json.ElementFactureAsJson;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.services.ElementFactureService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Element facture service.
 */
@Service("elementFactureService")
public class ElementFactureServiceImpl extends AbstractServiceImpl<ElementFacture, ElementFactureDAOImpl> implements ElementFactureService {

    /**
     * The Element facture dao.
     */
    @Autowired
    private ElementFactureDAOImpl elementFactureDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public ElementFactureDAOImpl getDAO() {
        return (ElementFactureDAOImpl) elementFactureDAO;
    }

    /**
     * {@linkplain ElementFactureService#findById(int)}
     */
    @Override
    public ElementFacture findById(int pIdElementFacture) {
        return elementFactureDAO.findUniqEntiteByProp(ElementFacture.PROP_ID_ELEMENT_FACTURE, pIdElementFacture);
    }

    /**
     * {@linkplain ElementFactureService#findByFacture(Facture)}
     */
    @Override
    public List<ElementFacture> findByFacture(Facture pFacture) {

        List<ElementFacture> lListElements = elementFactureDAO.findListEntitesByProp(ElementFacture.PROP_FACTURE_ELEMENT, pFacture, Facture.PROP_ID_FACTURE);
        List<ElementFactureAsJson> lListElementsJson = new ArrayList<>();

        for (ElementFacture elem : lListElements) {
            ElementFactureAsJson elemJson = new ElementFactureAsJson(elem);
            lListElementsJson.add(elemJson);

        }

        return lListElements;
    }

    /**
     * {@linkplain ElementFactureService#createOrUpdateElementFacture(ElementFactureAsJson, Facture)}
     */
    @Override
    public int createOrUpdateElementFacture(ElementFactureAsJson pElementFactureAsJson, Facture pFacture) throws JsonProcessingException, IOException {
        return elementFactureDAO.createOrUpdateElementFacture(pElementFactureAsJson, pFacture);
    }

    /**
     * {@linkplain ElementFactureService#deleteElementFacture(ElementFacture)}
     */
    @Override
    public String deleteElementFacture(ElementFacture pElementFacture) {
        return elementFactureDAO.deleteElementFacture(pElementFacture);
    }

    /**
     * {@linkplain ElementFactureService#deleteElementFacture(ElementFacture)}
     */
    @Override
    public List<ElementFacture> findAllOrderByNomAsc() {
        return elementFactureDAO.findElementfacture();
    }

    /**
     * {@linkplain ElementFactureService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<ElementFactureAsJson> findAllOrderByNomAscAsJson() {
        List<ElementFacture> tmpListElement = this.findAllOrderByNomAsc();
        List<ElementFactureAsJson> tmpListElementAsJson = new ArrayList<ElementFactureAsJson>();

        for (ElementFacture tmpElement : tmpListElement) {

            tmpListElementAsJson.add(tmpElement.toJson());

        }
        return tmpListElementAsJson;
    }

    /**
     * Method which called after validating invoices, which consisting of deleting all invoices's fields
     * @param invoiceElementsList the elements to be deleted
     */
    @Override
    public void deleteInvoiceElements(List<ElementFacture> invoiceElementsList) {
        // Si la liste n'est pas vide
        if (!invoiceElementsList.isEmpty()) {
            // On boucle sur le liste des élements de cette facture
            for (ElementFacture invoiceElement : invoiceElementsList) {
                // On supprime les éléments de cette facture
                elementFactureDAO.deleteElementFacture(invoiceElement);
            }
        }
    }

}
