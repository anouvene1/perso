/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.TypeValeur;

/**
 * The type Type valeur as json.
 */
public class TypeValeurAsJson {

    private Long id;
    private String typeValeur;
    private String code;
    private String nomChamp;

    /**
     * Instantiates a new Type valeur as json.
     */
    public TypeValeurAsJson() {
    }

    /**
     * Instantiates a new Type valeur as json.
     *
     * @param pTypeValeur the p type valeur
     */
    public TypeValeurAsJson(TypeValeur pTypeValeur) {
        super();
        typeValeur = pTypeValeur.getTypeValeur();
        code = pTypeValeur.getCode();
        nomChamp = pTypeValeur.getNomChamp();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        id = pId;
    }

    /**
     * Gets type valeur.
     *
     * @return the typeValeur
     */
    public String getTypeValeur() {
        return typeValeur;
    }

    /**
     * Sets type valeur.
     *
     * @param pTypeValeur the typeValeur to set
     */
    public void setTypeValeur(String pTypeValeur) {
        typeValeur = pTypeValeur;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        code = pCode;
    }

    /**
     * Gets nom champ.
     *
     * @return the nomChamp
     */
    public String getNomChamp() {
        return nomChamp;
    }

    /**
     * Sets nom champ.
     *
     * @param pNomChamp the nomChamp to set
     */
    public void setNomChamp(String pNomChamp) {
        nomChamp = pNomChamp;
    }


}
