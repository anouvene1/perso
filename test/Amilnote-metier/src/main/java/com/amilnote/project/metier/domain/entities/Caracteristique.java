/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.CaracteristiqueAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Caracteristique.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_ref_carac")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Caracteristique implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "carac";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PRO_TYPE_VALEUR.
     */
    public static final String PRO_TYPE_VALEUR = "typeValeur";
    /**
     * The constant CARAC_DEPART.
     */
    public static final String CARAC_DEPART = "DEP";
    /**
     * The constant CARAC_ARRIVE.
     */
    public static final String CARAC_ARRIVE = "AR";
    /**
     * The constant CARAC_DEBUT.
     */
    public static final String CARAC_DEBUT = "DEB";
    /**
     * The constant CARAC_FIN.
     */
    public static final String CARAC_FIN = "FIN";
    /**
     * The constant CARAC_NOMBREKM.
     */
    public static final String CARAC_NOMBREKM = "KM";
    /**
     * The constant CARAC_SOIREEETAPE.
     */
    public static final String CARAC_SOIREEETAPE = "SOE";
    private static final long serialVersionUID = -1197857111510107844L;
    private Long id;
    private String carac;
    private String code;
    private TypeValeur typeValeur;

    /**
     * Instantiates a new Caracteristique.
     *
     * @param pCarac      the p carac
     * @param pCode       the p code
     * @param pTypeValeur the p type valeur
     */
    public Caracteristique(String pCarac, String pCode, TypeValeur pTypeValeur) {
        super();
        this.carac = pCarac;
        this.code = pCode;
        this.typeValeur = pTypeValeur;
    }

    /**
     * Instantiates a new Caracteristique.
     */
    public Caracteristique() {
    }

    /**
     * To json caracteristique as json.
     *
     * @return the caracteristique as json
     */
    public CaracteristiqueAsJson toJson() {
        return new CaracteristiqueAsJson(this);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets carac.
     *
     * @return the carac
     */
    @Column(name = "carac", nullable = false)
    public String getCarac() {
        return carac;
    }

    /**
     * Sets carac.
     *
     * @param pCarac the p carac
     */
    public void setCarac(String pCarac) {
        this.carac = pCarac;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets type valeur.
     *
     * @return the type valeur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_type_val", nullable = false)
    public TypeValeur getTypeValeur() {
        return typeValeur;
    }

    /**
     * Sets type valeur.
     *
     * @param pTypeValeur the p type valeur
     */
    public void setTypeValeur(TypeValeur pTypeValeur) {
        typeValeur = pTypeValeur;
    }

}
