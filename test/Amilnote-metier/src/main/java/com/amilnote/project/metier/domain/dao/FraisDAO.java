/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.utils.Pair;
import org.joda.time.DateTime;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

/**
 * The interface Frais dao.
 */
public interface FraisDAO extends LongKeyDAO<Frais> {

    /**
     * trouver l'ensemble des frais enregistrés pour la mission courante
     * et pour la mission sélectionnée
     *
     * @param pMission the p mission
     * @param pMois    the p mois
     * @param pAnnee   the p annee
     * @return List list
     */
    List<Object[]> findByMissionCriteria(Mission pMission, int pMois, int pAnnee);

    List<Object[]> findByMission(Mission pMission, int pMois, int pAnnee);

    /**
     * trouver l'ensemble des frais enregistrés pour la mission courante
     * et pour la mission sélectionnée
     *
     * @param pMission the p mission
     * @param date    the p date
     * @return List list
     */
    List<Object[]> findByMissionCriteria(Mission pMission, DateTime date);

    /**
     * trouver tous les justificatifs attachés à des frais pour la mission passées en parametre
     *
     * @param pMission the p mission
     * @param pDateFin the p date fin
     * @return List list
     */
    List<byte[]> findJustifsByMission(Mission pMission, DateTime pDateFin);

    /**
     * suppression d'un ou de plusieurs frais
     *
     * @param pListIdFrais the p list id frais
     * @return String string
     */
    String deleteFrais(List<Long> pListIdFrais);

    /**
     * modification d'un frais
     *
     * @param pIdFrais      the p id frais
     * @param pMontant      the p montant
     * @param pLinkFraisTFC the p link frais tfc
     * @param pLinkEvtTS    the p link evt ts
     * @param pCommentaire  the p commentaire
     * @param pJustif       the p justif
     * @param Avance        the avance
     * @return String string
     */
    String modifyFrais(Long pIdFrais, BigDecimal pMontant, LinkFraisTFC pLinkFraisTFC, LinkEvenementTimesheet pLinkEvtTS, String pCommentaire, String pJustif, int Avance);


    /**
     * retourne le lien du justificatif d'un frais
     *
     * @param pIdFrais the p id frais
     * @return String picture for frais
     */
    Pair<String, InputStream> getPictureForFrais(Long pIdFrais);

    /**
     * modifie l'etat des frais
     *
     * @param pListFrais the p list frais
     * @return String string
     */
    String modifyEtatFrais(List<Frais> pListFrais);

    /**
     * Trouver les factures de frais pour le mois voulu
     *
     * @param pDateVoulue le mois voulu
     * @return the list
     */
    List<Frais> getAllFraisByMonth(DateTime pDateVoulue);

    /**
     * Retourne la liste des frais pour le collaborateur souhaité
     *
     * @param collaborator Collaborateur souhaité
     * @return Liste des frais du collaborateur
     */
    List<Frais> getFraisByCollaborator(Collaborator collaborator);

    /**
     * Renvoie la valeur de l'id suivant (en fonction de l'AUTO_INCREMENT) d'une table
     *
     * @param nomTable the nom table
     * @return auto increment
     */
    int getAutoIncrement(String nomTable);


    /**
     * Méthode permettant de récupérer les frais pour le RA en cours
     *
     * @param collaborator Collaborateur ayant soumis son RA
     * @param pDateRAVoulue Date du RA soumis
     * @param etat          L'état que l'on souhaite mettre sur les frais
     * @return true si l'update s'est bien fait, false sinon.
     */
    Boolean updateFraisEnCours(Collaborator collaborator, DateTime pDateRAVoulue, Etat etat);

}

