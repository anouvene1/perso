package com.amilnote.project.metier.domain.utils.enumerations;

/**
 *
 */
public enum MonthsOfYear {
    JANVIER(1),
    FEVRIER(2),
    MARS(3),
    AVRIL(4),
    MAI(5),
    JUIN(6),
    JUILLET(7),
    AOUT(8),
    SEPTEMBRE(9),
    OCTOBRE(10),
    NOVEMBRE(11),
    DECEMBRE(12);


   private int monthIndex;

    private MonthsOfYear(int monthIndex) { this.monthIndex = monthIndex;}


    public static MonthsOfYear getMonth(int monthIndex) {

        for (MonthsOfYear month : MonthsOfYear.values())
        {
            if (month.monthIndex == monthIndex) return month;
        }
        throw new IllegalArgumentException("Error : invalid month index");
    }

}
