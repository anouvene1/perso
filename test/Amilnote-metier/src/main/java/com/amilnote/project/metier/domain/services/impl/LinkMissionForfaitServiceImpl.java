/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.LinkMissionForfaitDAO;
import com.amilnote.project.metier.domain.dao.impl.LinkMissionForfaitDAOImpl;
import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.Frequence;
import com.amilnote.project.metier.domain.entities.LinkMissionForfait;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.LinkEvenementTimesheetService;
import com.amilnote.project.metier.domain.services.LinkMissionForfaitService;
import com.amilnote.project.metier.domain.services.MissionService;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The type Link mission forfait service.
 */
@Service("linkMissionForfaitService")
public class LinkMissionForfaitServiceImpl extends AbstractServiceImpl<LinkMissionForfait, LinkMissionForfaitDAOImpl> implements LinkMissionForfaitService {

    @Autowired
    private LinkMissionForfaitDAO linkMissionForfaitDao;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private MissionService missionService;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public LinkMissionForfaitDAOImpl getDAO() {
        return (LinkMissionForfaitDAOImpl) linkMissionForfaitDao;
    }

    /**
     * {@linkplain LinkMissionForfaitService#calculByPeriod(LinkMissionForfait, DateTime, DateTime)}
     */
    @Override
    public float calculByPeriod(LinkMissionForfait linkMissionForfait, DateTime pDateDebut, DateTime pDateFin) {
        Forfait forfait = linkMissionForfait.getForfait();
        Mission mission = linkMissionForfait.getMission();
        if (forfait.getFrequence().getCode() == Frequence.FREQUENCE_JOURNALIER) {
            int nbWorkedDays = linkEvenementTimesheetService.findNbWorkedDaysByMissionAndPeriod(mission, pDateDebut, pDateFin).size();

            return linkMissionForfait.getMontant() * nbWorkedDays;
        } else if (forfait.getFrequence().getCode() == Frequence.FREQUENCE_HEBDOMADAIRE && pDateDebut.isAfter(new DateTime(mission.getDateDebut())) &&
            pDateDebut.isBefore(new DateTime(mission.getDateFin()))) {

            return linkMissionForfait.getMontant();
        }
        return 0;
    }

    /**
     * {@linkplain LinkMissionForfaitService#calculByPeriodAndIdLinkMissionForfait(Long, DateTime, DateTime)}
     */
    @Override
    public float calculByPeriodAndIdLinkMissionForfait(Long id, DateTime pDateDebut, DateTime pDateFin) {

        return this.calculByPeriod(this.get(id), pDateDebut, pDateFin);
    }

    /**
     * {@linkplain LinkMissionForfaitService#addLinkMissionForfait(MissionAsJson, LinkMissionForfaitAsJson)}
     */
    @Override
    public String addLinkMissionForfait(MissionAsJson pMissionAsJson, LinkMissionForfaitAsJson pLinkMissionForfaitAsJson) {

        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        LinkMissionForfait linkMissionForfait = new LinkMissionForfait();

        Mission mission = missionService.findById(pMissionAsJson.getId());
        Forfait forfait = (Forfait) session.get(Forfait.class, pLinkMissionForfaitAsJson.getForfait().getId());

        if (null != mission && null != forfait) {

            linkMissionForfait.setMission(mission);

            if (forfait.getCode().contains(Forfait.CODE_LIBRE)) {
                linkMissionForfait.setMontant(0f); // montant nul
                linkMissionForfait.setForfait(forfait);
                linkMissionForfait.setLibelle(pLinkMissionForfaitAsJson.getLibelle());
            }
            else {
                linkMissionForfait.setMontant(pLinkMissionForfaitAsJson.getMontant());
                linkMissionForfait.setForfait(forfait);
                linkMissionForfait.setLibelle("");
            }

            session.save(linkMissionForfait);
            session.flush();
            transaction.commit();
            return "ok";

        } else {
            return "error";
        }
    }

    /**
     * {@linkplain LinkMissionForfaitService#deleteLinkMissionForfait(Long)}
     */
    @Override
    public void deleteLinkMissionForfait(Long pIdLinkForfait) {

        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        LinkMissionForfait linkMissionForfait = (LinkMissionForfait) session.load(LinkMissionForfait.class, pIdLinkForfait);
        session.delete(linkMissionForfait);

        session.flush();
        transaction.commit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LinkMissionForfait> findAllByMission(Mission pMission) {
        return linkMissionForfaitDao.findByMission(pMission);
    }

    @Override
    public List<LinkMissionForfait> findLib(){
        return linkMissionForfaitDao.findLib();
    }

    @Override
    public List<LinkMissionForfait> findNotLib(){
        return linkMissionForfaitDao.findNotLib();
    }

    @Override
    public List<LinkMissionForfait> findLibByMission(Mission pMission){
        return linkMissionForfaitDao.findLibByMission(pMission);
    }

    @Override
    public List<LinkMissionForfait> findNotLibByMission(Mission pMission){
        return linkMissionForfaitDao.findNotLibByMission(pMission);
    }
}
