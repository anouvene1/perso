/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.DemandeDeplacementAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Demande deplacement.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_dem_deplacement")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class DemandeDeplacement implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_DATEDEBUT.
     */
    public static final String PROP_DATEDEBUT = "dateDebut";
    /**
     * The constant PROP_DATEFIN.
     */
    public static final String PROP_DATEFIN = "dateFin";
    /**
     * The constant PROP_DATESOUMISSION.
     */
    public static final String PROP_DATESOUMISSION = "dateSoumission";
    /**
     * The constant PROP_DATEVALIDATION.
     */
    public static final String PROP_DATEVALIDATION = "dateValidation";
    /**
     * The constant PROP_MISSION.
     */
    public static final String PROP_MISSION = "mission";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";
    /**
     * The constant PROP_CODE_PROJET.
     */
    public static final String PROP_CODE_PROJET = "codeProjet";
    /**
     * The constant PROP_MOTIF.
     */
    public static final String PROP_MOTIF = "motif";
    /**
     * The constant PROP_AVANCE.
     */
    public static final String PROP_AVANCE = "avance";
    /**
     * The constant PROP_LIEU.
     */
    public static final String PROP_LIEU = "lieu";
    /**
     * The constant PROP_COMMENTAIRE.
     */
    public static final String PROP_COMMENTAIRE = "commentaire";
    /**
     * The constant PROP_NB_JOURS.
     */
    public static final String PROP_NB_JOURS = "nbJours";
    /**
     * The constant PROP_COLLABORATEUR.
     */
    public static final String PROP_COLLABORATEUR = "collaborateur";
    private static final long serialVersionUID = 8485827225646929738L;
    private Long id;
    private Mission mission;
    private String codeProjet;
    private String motif;
    private Date dateDebut;
    private Date dateFin;
    private Etat etat;
    private int avance;
    private Date dateSoumission;
    private Date dateValidation;
    private String lieu;
    private String validationClient;
    private String commentaire;
    private Float nbJours;
    private Collaborator collaborator;

    /**
     * Instantiates a new Demande deplacement.
     */
/* CONSTRUCTORS */
    public DemandeDeplacement() {
        super();
    }

    /**
     * Instantiates a new Demande deplacement.
     *
     * @param pMission          the p mission
     * @param pCodeProjet       the p code projet
     * @param pMotif            the p motif
     * @param pDateDebut        the p date debut
     * @param pDateFin          the p date fin
     * @param pEtat             the p etat
     * @param pAvance           the p avance
     * @param pDateSoumission   the p date soumission
     * @param pDateValidation   the p date validation
     * @param pLieu             the p lieu
     * @param pValidationClient the p validation client
     * @param pCommentaire      the p commentaire
     * @param pNbJours          the p nb jours
     * @param collaborator    the p collaborateur
     */
    public DemandeDeplacement(Mission pMission, String pCodeProjet, String pMotif, Date pDateDebut, Date pDateFin, Etat pEtat, int pAvance,
                              Date pDateSoumission, Date pDateValidation, String pLieu, String pValidationClient, String pCommentaire, Float pNbJours, Collaborator collaborator) {
        super();
        mission = pMission;
        codeProjet = pCodeProjet;
        motif = pMotif;
        dateDebut = pDateDebut;
        dateFin = pDateFin;
        etat = pEtat;
        avance = pAvance;
        dateSoumission = pDateSoumission;
        dateValidation = pDateValidation;
        lieu = pLieu;
        validationClient = pValidationClient;
        commentaire = pCommentaire;
        nbJours = pNbJours;
        this.collaborator = collaborator;
    }


	/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Gets date debut.
     *
     * @return dateDebut date debut
     */
    @Column(name = "date_debut", nullable = false)
    public Date getDateDebut() {
        return dateDebut;
    }

    /**
     * Sets date debut.
     *
     * @param dateDebut the date debut
     */
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Gets date fin.
     *
     * @return dateFin date fin
     */
    @Column(name = "date_fin", nullable = false)
    public Date getDateFin() {
        return dateFin;
    }

    /**
     * Sets date fin.
     *
     * @param dateFin the date fin
     */
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * retourne l'état de l'absence
     *
     * @return un objet de type StatutCollaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_etat", nullable = false)
    public Etat getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(Etat pEtat) {
        etat = pEtat;
    }

    /**
     * Gets date soumission.
     *
     * @return the dateSoumission
     */
    @Column(name = "date_soumission", nullable = false)
    public Date getDateSoumission() {
        return dateSoumission;
    }

    /**
     * Sets date soumission.
     *
     * @param pDateSoumission the dateSoumission to set
     */
    public void setDateSoumission(Date pDateSoumission) {
        dateSoumission = pDateSoumission;
    }

    /**
     * Gets date validation.
     *
     * @return the dateValidation
     */
    @Column(name = "date_validation", nullable = true)
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * Sets date validation.
     *
     * @param pDateValidation the dateValidation to set
     */
    public void setDateValidation(Date pDateValidation) {
        dateValidation = pDateValidation;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_mission", nullable = false)
    public Mission getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param pMission the p mission
     */
    public void setMission(Mission pMission) {
        mission = pMission;
    }

    /**
     * Gets code projet.
     *
     * @return the code projet
     */
    @Column(name = "code_projet", nullable = true)
    public String getCodeProjet() {
        return codeProjet;
    }


    /**
     * Sets code projet.
     *
     * @param pCodeProjet the p code projet
     */
    public void setCodeProjet(String pCodeProjet) {
        codeProjet = pCodeProjet;
    }

    /**
     * Gets motif.
     *
     * @return the motif
     */
    @Column(name = "motif", nullable = true)
    public String getMotif() {
        return motif;
    }


    /**
     * Sets motif.
     *
     * @param pMotif the p motif
     */
    public void setMotif(String pMotif) {
        motif = pMotif;
    }

    /**
     * Gets avance.
     *
     * @return the avance
     */
    @Column(name = "avance", nullable = false)
    public int getAvance() {
        return avance;
    }


    /**
     * Sets avance.
     *
     * @param pAvance the p avance
     */
    public void setAvance(int pAvance) {
        avance = pAvance;
    }


    /**
     * Gets lieu.
     *
     * @return the lieu
     */
    @Column(name = "lieu", nullable = false)
    public String getLieu() {
        return lieu;
    }

    /**
     * Sets lieu.
     *
     * @param lieu the lieu to set
     */
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }


    /**
     * Gets commentaire.
     *
     * @return the commentaire
     */
    @Column(name = "commentaire", nullable = true)
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * Sets commentaire.
     *
     * @param commentaire the commentaire to set
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * To json demande deplacement as json.
     *
     * @return the demande deplacement as json
     */
    public DemandeDeplacementAsJson toJson() {
        return new DemandeDeplacementAsJson(this);
    }

    /**
     * Gets validation client.
     *
     * @return the validation client
     */
    @Column(name = "validation_client", nullable = true)
    public String getValidationClient() {
        return validationClient;
    }

    /**
     * Sets validation client.
     *
     * @param validationClient the validation client
     */
    public void setValidationClient(String validationClient) {
        this.validationClient = validationClient;
    }

    /**
     * Gets nb jours.
     *
     * @return the nbJours
     */
    @Column(name = "nb_jours", nullable = true)
    public Float getNbJours() {
        return nbJours;
    }

    /**
     * Sets nb jours.
     *
     * @param nbJours the nbJours to set
     */
    public void setNbJours(Float nbJours) {
        this.nbJours = nbJours;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_collab", nullable = false)
    public Collaborator getCollaborator() {
        return collaborator;
    }

    /**
     * Sets collaborateur.
     *
     * @param collaborateur the collaborateur to set
     */
    public void setCollaborator(Collaborator collaborateur) {
        this.collaborator = collaborateur;
    }
}
