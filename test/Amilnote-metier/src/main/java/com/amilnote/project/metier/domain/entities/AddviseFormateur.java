/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Classe AddviseFormateur pour définir les intervenants des différentes sessions du programme Addvise
 */
@Entity
@Table(name = "ami_addvise_formateur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AddviseFormateur implements Serializable {
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private List<AddviseSession> sessions;

    public AddviseFormateur() {
        sessions = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String eMail) {
        this.email = eMail;
    }

    @OneToMany(mappedBy = "formateur", fetch = FetchType.LAZY)
    public List<AddviseSession> getSessions() {
        return sessions;
    }

    public void setSessions(List<AddviseSession> sessions) {
        this.sessions = sessions;
    }

    public void addSession(AddviseSession session) {
        this.addSession(session);
    }
}
