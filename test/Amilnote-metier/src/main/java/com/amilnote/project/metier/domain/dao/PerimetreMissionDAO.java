/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.PerimetreMission;

import java.util.List;

/**
 * The interface Perimetre mission dao.
 */
//SBE_AMNOTE-197
public interface PerimetreMissionDAO extends LongKeyDAO<PerimetreMission> {

    /**
     * retourne un perimetre de mission à partir de son id
     *
     * @param id the id
     * @return List perimetre mission
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    PerimetreMission findById(Long id);

    /**
     * retourne un perimetre de mission d'un code donné en paramètre
     *
     * @return List list
     */
    List<PerimetreMission> findAll();


}
