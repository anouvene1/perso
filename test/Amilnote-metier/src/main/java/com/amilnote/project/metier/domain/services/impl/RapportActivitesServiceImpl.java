/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.RapportActivitesDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The type Rapport activites service.
 */
@Service("rapportActivitesService")
public class RapportActivitesServiceImpl extends AbstractServiceImpl<RapportActivites, RapportActivitesDAO> implements RapportActivitesService {

    private static final Logger logger = LogManager.getLogger(RapportActivitesServiceImpl.class);

    @Autowired
    private RapportActivitesDAO rapportActivitesDAO;

    @Autowired
    private EtatDAO etatDAO;

    @Autowired
    private LinkEvenementTimesheetService linkEvenementTimesheetService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private FraisService fraisService;

    @Autowired
    private FileService fileService;

    /**
     * {@inheritDoc}
     */
    @Override
    public RapportActivitesDAO getDAO() {
        return rapportActivitesDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveNewRACollaborator(Date pDateSoumission, String pPdfRapportActivite, Collaborator collaborator, DateTime pDateRAVoulue, boolean pAvecFrais) {
        return rapportActivitesDAO.saveNewRACollaborator(pDateSoumission, pPdfRapportActivite, collaborator, pDateRAVoulue, pAvecFrais);
    }

    /**
     * {@linkplain RapportActivitesService#getExistingRAForMonth(Collaborator, DateTime)}
     */
    @Override
    public List<RapportActivites> getExistingRAForMonth(Collaborator collaborator, DateTime pDateTime) {

        return rapportActivitesDAO.getExistingRAForMonth(collaborator, pDateTime);
    }

    /**
     * {@linkplain RapportActivitesService#getBrouillonRAForMonth(Collaborator, DateTime)}
     */
    @Override
    public List<RapportActivites> getBrouillonRAForMonth(Collaborator collaborator, DateTime pDateTime) {

        return rapportActivitesDAO.getBrouillonRAForMonth(collaborator, pDateTime);
    }

    /**
     * {@linkplain RapportActivitesService#getAllRAByFiltre(String)}
     */
    @Override
    public List<RapportActivites> getAllRAByFiltre(String pFiltre) {
        if (!pFiltre.equals("null") && !pFiltre.equals("TO")) {
            Etat etatFiltre;
            etatFiltre = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pFiltre);
            return rapportActivitesDAO.findListEntitesByProp(RapportActivites.PROP_ETAT, etatFiltre);
        } else {
            return getAllRA();
        }
    }

    /**
     * {@linkplain RapportActivitesService#getDateDernierRASoumis(Collaborator)}
     */
    @Override
    public Date getDateDernierRASoumis(Collaborator collaborator) {
        return rapportActivitesDAO.getDateDernierRASoumis(collaborator);
    }

    /**
     * {@linkplain RapportActivitesService#getAllRAEtatSO()}
     */
    @Override
    public List<RapportActivites> getAllRAEtatSO() {
        Etat etatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        return rapportActivitesDAO.findListEntitesByProp(RapportActivites.PROP_ETAT, etatSO);
    }

    /**
     * {@linkplain RapportActivitesService#getRAbyId(Long)}
     */
    @Override
    public RapportActivites getRAbyId(Long pId) {
        return rapportActivitesDAO.get(pId);
    }

    /**
     * {@linkplain RapportActivitesService#changeEtatRA(Long, String, String)}
     */
    @Override
    public void changeEtatRA(Long idRA, String pEtatValide, String pCommentaire) throws Exception {
        Etat lEtat = null;

        // --- Récupération du RA dans la BDD
        RapportActivites lTmpRA = this.get(idRA);

        if (null == lTmpRA) {
            // --- Cas d'erreur : Identifiant du RA inconnu dans la BDD
            throw new Exception("Identifiant RA inconnu");
        }

        // --- Récupération de l'état voulu
        lEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pEtatValide);

        // --- Modification de l'état du RA
        lTmpRA.setEtat(lEtat);

        if (pCommentaire != null) {
            lTmpRA.setCommentaire(pCommentaire);
        }

        // --- Création de la session et de la transaction
        Session session = getDAO().getCurrentSession();
        if (!session.getTransaction().isActive()) {
            session.beginTransaction();
        }

        // --- Sauvegarde et Commit de la mise à jour
        session.saveOrUpdate(lTmpRA);
        session.flush();
        //transaction.commit();
        session.getTransaction().commit();

        //Sauvegarde et commit de la mise à jour de l'état des Frais liés
        DateTime pDateRAVoulue = new DateTime(lTmpRA.getMoisRapport());
        CollaboratorAsJson collaboratorAsJson = collaboratorService.findByIdRA(idRA);
        Collaborator collaborator = new Collaborator();
        collaborator.setId(collaboratorAsJson.getId());

        Etat etat = new Etat();
        if (pEtatValide == Etat.ETAT_VALIDE_CODE) {
            etat.setCode(Etat.ETAT_VALIDE_CODE);
            etat.setEtat(Etat.ETAT_VALIDE);
            etat.setId(3);
        } else if (pEtatValide == Etat.ETAT_REFUSE || pEtatValide == Etat.ETAT_BROUILLON_CODE || pEtatValide == Etat.ETAT_ANNULE) {
            etat.setCode(Etat.ETAT_BROUILLON_CODE);
            etat.setEtat(Etat.ETAT_BROUILLON);
            etat.setId(1);
        }

        if (!fraisService.updateFraisEnCours(collaborator, pDateRAVoulue, etat)) {
            throw new Exception(Constantes.ERROR_MSG_UPDATE_FRAIS);
        }
    }

    /**
     * {@linkplain RapportActivitesService#getFileRAbyId(Long)}
     */
    @Override
    public File getFileRAbyId(Long pIdRA) throws Exception {

        // --- Récupération du RA
        RapportActivites lRA = this.get(pIdRA);
        if (null == lRA) {
            return null;
        }

        String fileName = lRA.getPdfRapportActivite();

        // --- Récupération du fichier PDF lié au RA
        File file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName);
        File fileSocial = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName.substring(0, fileName.lastIndexOf(".")) + Constantes.RA_SOCIAL_PDF_EXTENSION);

        // --- Vérification de l'existance du fichier
        // si le RA n'existe pas en PDF, on verifie s'il est en ZIP
        if (!file.exists() && !fileSocial.exists()) {

            file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName.substring(0, fileName.lastIndexOf(".")) + Constantes.EXTENSION_FILE_ZIP);
            fileSocial = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName.substring(0, fileName.lastIndexOf(".")) + Constantes.RA_SOCIAL_ZIP_EXTENSION);

            // si le fichier est en ZIP, on le décompresse pour le renvoyer a son format d'origine
            if (file.exists()) {
                file = fileService.unZipFile(file);
            } else if (fileSocial.exists()) {
                file = fileService.unZipFile(fileSocial);
            } else //s'il n'existe pas en ZIP, le fichier n'existe pas dans le serveur
            {
                return null;
            }
        } else if (!file.exists()) {
            file = fileSocial;
        }

        return file;
    }

    /**
     * {@linkplain RapportActivitesService#raFileExists(Long)}
     */
    public boolean raFileExists(Long pIdRA) {

        // --- Récupération du RA
        RapportActivites lRA = this.get(pIdRA);
        if (null == lRA) {
            return false;
        }

        try {

            String fileName = lRA.getPdfRapportActivite();

            // --- Récupération du fichier PDF lié au RA (en état "Soumis" ou "Brouillon")
            File file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName);
            File fileSocial = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName + Constantes.SOCIAL_EXTENSION);

            // si le RA n'existe pas en PDF, on verifie s'il est en ZIP (en état "Soumis" ou "Brouillon")
            if (!file.exists() && !fileSocial.exists()) {

                file = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName.substring(0, fileName.lastIndexOf(".")) + Constantes.EXTENSION_FILE_ZIP);
                fileSocial = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_RA) + fileName.substring(0, fileName.lastIndexOf(".")) + Constantes.RA_SOCIAL_ZIP_EXTENSION);

                // si le fichier n'existe pas non plus en zip, alors il n'est pas dans le serveur
                if (!file.exists() && !fileSocial.exists()) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /**
     * {@linkplain RapportActivitesService#getAllRA()}
     */
    @Override
    public List<RapportActivites> getAllRA() {
        return this.getDAO().loadAll();
    }

    /**
     * {@linkplain RapportActivitesService#changeEtatRA(Long, String)}
     */
    @Override
    public void changeEtatRA(Long pIdRA, String pEtatValide) throws Exception {
        this.changeEtatRA(pIdRA, pEtatValide, null);
    }

    /**
     * {@linkplain RapportActivitesService#getAllRAForCollaborator(Collaborator)}
     */
    @Override
    public List<RapportActivites> getAllRAForCollaborator(Collaborator collaborator) {
        List<RapportActivites> listeAllRA = getAllRA();
        List<RapportActivites> listeAllRAForCollaborator = new ArrayList<>();

        if (collaborator != null && collaborator.getId() != null) {
            for (RapportActivites ra : listeAllRA) {
                if (ra.getCollaborateur().getId() == collaborator.getId()) {
                    listeAllRAForCollaborator.add(ra);
                }
            }
        }

        return listeAllRAForCollaborator;
    }


    /**
     * {@linkplain RapportActivitesService#getAllRAForCollaboratorBetweenDate(Collaborator, Date)}
     */
    @Override
    public List<RapportActivites> getAllRAForCollaboratorBetweenDate(Collaborator collaborator, Date dateDebut) {
        return rapportActivitesDAO.getAllRAForCollaboratorBetweenDate(collaborator, new DateTime(dateDebut));
    }


    /**
     * {@linkplain RapportActivitesService#getNbJoursTravailles(Collaborator, Mission, DateTime)}
     */
    @Override
    public Float getNbJoursTravailles(Collaborator collaborator, Mission pMission, DateTime dateVoulue) {
        Float quantite = 0f;

        DateTime lDebutPeriode = dateVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = dateVoulue.dayOfMonth().withMaximumValue();

        List<LinkEvenementTimesheet> listLinkEvent = null;

        try {
            listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDates(collaborator, lDebutPeriode.toString(), lFinPeriode.toString());
        } catch (JsonProcessingException e) {
            logger.error("[find events] collaborator id : {}", collaborator.getId(), e);
        }

        for (LinkEvenementTimesheet event : listLinkEvent) {
            if (event.getMission() != null) {
                if (event.getAbsence() == null) {
                    Mission mission = event.getMission();
                    if (pMission == mission) {
                        quantite = quantite + 0.5f;
                    }
                }
            }
        }

        return quantite;
    }

    public void changePdf(RapportActivites rapportActivites, String pdf) {
        // --- Création de la session et de la transaction
        Session session = getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();

        rapportActivites.setPdfRapportActivite(pdf);
        // --- Sauvegarde et Commit de la mise à jour
        session.saveOrUpdate(rapportActivites);
        session.flush();
        transaction.commit();
    }

    @Override
    public List<RapportActivites> getRACollaboratorMissionClienteForMonth(DateTime pDateTime) throws JsonProcessingException {
        List<RapportActivites> listRA = new ArrayList<>();
        List<RapportActivites> listRATemp = rapportActivitesDAO.getRACollaboratorMissionClienteForMonth(pDateTime);
        for (RapportActivites ra : listRATemp) {
            if (isRAWithMissionCliente(ra)) listRA.add(ra);
        }
        return listRA;
    }

    @Override
    public boolean isRAWithMissionCliente(RapportActivites pRapportActivites) throws JsonProcessingException {
        DateTime pDateRAVoulue = new DateTime(pRapportActivites.getMoisRapport());
        DateTime lDebutPeriode = pDateRAVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = pDateRAVoulue.dayOfMonth().withMaximumValue();
        List<LinkEvenementTimesheet> listLinkEvent = linkEvenementTimesheetService.findEventsBetweenDates(pRapportActivites.getCollaborateur(), lDebutPeriode.toString(), lFinPeriode.toString());
        for (LinkEvenementTimesheet let : listLinkEvent) {
            if (let.getMission() != null && let.getMission().getTypeMission().getCode().equals(TypeMission.MISSION_CLIENTE)) {
                return true;
            }
        }
        return false;
    }

}
