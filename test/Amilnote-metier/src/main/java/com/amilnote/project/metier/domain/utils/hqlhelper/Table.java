/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Table.
 */
public class Table {
    private String nom;
    private String alias;
    private List<Champ> champ = new ArrayList<>();
    private TypeJointure typeJointure = null;
    private HQLHelper query;

    /**
     * Instantiates a new Table.
     *
     * @param table the table
     * @param query the query
     */
    protected Table(Class table, HQLHelper query) {
        this(table.getSimpleName(), null, query);
    }


    /**
     * Instantiates a new Table.
     *
     * @param table the table
     * @param query the query
     */
    protected Table(String table, HQLHelper query) {
        this(table, null, query);
    }

    /**
     * Instantiates a new Table.
     *
     * @param table the table
     * @param alias the alias
     * @param query the query
     */
    protected Table(Class table, String alias, HQLHelper query) {
        this(table.getSimpleName(), alias, query);
    }

    /**
     * Instantiates a new Table.
     *
     * @param table the table
     * @param alias the alias
     * @param query the query
     */
    protected Table(String table, String alias, HQLHelper query) {
        this.nom = table;
        if (alias != null) {
            this.alias = StringUtils.lowerCase(alias);
        } else {
            this.alias = StringUtils.lowerCase(table);
        }
        this.query = query;
    }

    /**
     * Ajoute un champ à la table cible
     *
     * @param nomChamp Nom du champ
     * @return retourn un object Champ représentant le champ ajouté.
     */
    public Champ addChamp(String nomChamp) {
        Champ champ = new Champ(this, nomChamp);
        this.champ.add(champ);
        return champ;
    }

    private String getNom() {
        return this.nom + " AS " + this.alias;
    }

    /**
     * retourne l'alias de la table
     *
     * @return chaine de caractère représentant l'alias de la table
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Gets table.
     *
     * @return the table
     */
    public String getTable() {
        if (this.typeJointure == null) {
            return getNom();
        }

        return getJointure();
    }

    /**
     * Ajoute une Restriction à la requête HQL.
     * si une condition est déjà présente un AND est réalisé entre les deux Condidtions
     *
     * @param condition Condition à ajouter.
     * @return retourne la requête HQL en cours.
     */
    public HQLHelper addRestriction(Condition condition) {
        query.addRestriction(condition);
        return query;
    }

    /**
     * Add jointure table.
     *
     * @param typeJointure the type jointure
     * @return the table
     */
    protected Table addJointure(TypeJointure typeJointure) {
        this.typeJointure = typeJointure;
        return this;
    }

    private String getJointure() {
        String table = "";
        if (this.typeJointure != null) {
            table = this.typeJointure.getLabel();
            if (!StringUtils.isEmpty(this.alias)) {
                table = table + this.query.getTableCible().alias + "." + this.nom + " AS " + this.alias;
            }
        }

        return table;
    }

    /**
     * The enum Type jointure.
     */
    public enum TypeJointure {
        /**
         * The Inner join.
         */
        INNER_JOIN(0, "INNER JOIN "),
        /**
         * The Left join.
         */
        LEFT_JOIN(1, "LEFT JOIN "),
        /**
         * The Right join.
         */
        RIGHT_JOIN(2, "RIGHT JOIN ");

        private final Integer id;
        private final String label;

        TypeJointure(Integer id, String label) {
            this.id = id;
            this.label = label;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public Integer getId() {
            return this.id;
        }

        /**
         * Gets label.
         *
         * @return the label
         */
        public String getLabel() {
            return this.label;
        }
    }
}
