/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Poste;
import com.amilnote.project.metier.domain.entities.json.PosteAsJson;

import java.util.List;

/**
 * The interface Poste service.
 */
public interface PosteService extends AbstractService<Poste> {


    /**
     * Gets all poste.
     *
     * @return the all poste
     */
    List<Poste> getAllPoste();

    /**
     * Gets all poste as json.
     *
     * @return the all poste as json
     */
    List<PosteAsJson> getAllPosteAsJson();

    /**
     * Gets poste by code.
     *
     * @param pCodePoste the p code poste
     * @return the poste by code
     */
    Poste getPosteByCode(String pCodePoste);


}
