/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.CollaborateurDAO;
import com.amilnote.project.metier.domain.entities.Collaborateur;
import com.amilnote.project.metier.domain.entities.Poste;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Classe DAO pour la recherche d'un utilisateur en BDD
 */
@Repository("collaborateurDAO")
public class CollaborateurDAOImpl extends TravailleurDAOImpl<Collaborateur> implements CollaborateurDAO {

    @Override
    protected Class<Collaborateur> getReferenceClass() {
        return Collaborateur.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborateur> findCollaborateurs(boolean isWithAdmin, boolean isWithDisabled) {
        List<Collaborateur> collaborateurs = new ArrayList<>();
        Criteria criteria;
        if (isWithAdmin) {
            criteria = reqAll(isWithDisabled);
        } else {
            criteria = reqWithoutAdmin(isWithDisabled);
        }
        addResultToList(collaborateurs, criteria);
        return collaborateurs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborateur> findTravailleurs() {
        return findCollaborateurs(true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Collaborateur> findCollaborateursByResponsable(Collaborateur responsable) {
        List<Collaborateur> collaborateurs = new ArrayList<>();
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborateur.PROP_MANAGER, responsable));
        criteria.addOrder(Order.asc(Collaborateur.PROP_NOM));
        addResultToList(collaborateurs, criteria);
        return collaborateurs;
    }

    /**
     * {@linkplain CollaborateurDAO#findAllManagers(StatutCollaborateur, Poste)}
     */
    @Override
    public List<Collaborateur> findAllManagers(StatutCollaborateur statutManager, Poste posteManager) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.or(Restrictions.eq(Collaborateur.PROP_STATUT, statutManager), Restrictions.eq(Collaborateur.PROP_POSTE, posteManager)));
        return criteria.list();
    }

    /**
     * {@linkplain CollaborateurDAO#findAllResponsablesTechnique(StatutCollaborateur)}
     */
    @Override
    public List<Collaborateur> findAllResponsablesTechnique(StatutCollaborateur statutResponsableTechnique) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborateur.PROP_STATUT, statutResponsableTechnique));
        return criteria.list();
    }

    /**
     * {@linkplain CollaborateurDAO#findAllChefDeProjet(StatutCollaborateur)}
     */
    @Override
    public List<Collaborateur> findAllChefDeProjet(StatutCollaborateur statutChefDeProjet) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq(Collaborateur.PROP_STATUT, statutChefDeProjet));
        return criteria.list();
    }

    /**
     * @param collaborateurs a list of {@link Collaborateur}
     * @param pDate          date
     */
    @Override
    protected final void removeDisabledLastMonth(List<Collaborateur> collaborateurs, Date pDate) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(pDate.getTime());
        List<Collaborateur> collabsDisabledInPast = new ArrayList<>();
        // BAB - 21/06/2016  [AMILTONE 100] : on recupere tous les collabs
        for (Collaborateur collab : collaborateurs) {
            Calendar dateSortie = GregorianCalendar.getInstance();
            // on verifie que la date de sortie soit non nulle
            if (collab.getDateSortie() != null && !collab.isEnabled()) {
                dateSortie.setTimeInMillis(collab.getDateSortie().getTime());
                // on filtre sur l'annee
                if (dateSortie.get(Calendar.YEAR) < calendar.get(Calendar.YEAR)) {
                    collabsDisabledInPast.add(collab);
                } else if (dateSortie.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)) {
                    // BAB 30/06/2016 AMNOTE-113 si on supprime un collab l'année en cours on compare les mois des 2 dates
                    if (dateSortie.get(Calendar.MONTH) < calendar.get(Calendar.MONTH)) {
                        collabsDisabledInPast.add(collab);
                    }
                }
            }
        }
        // si des collabs ont été supprimés dans le passé ils sont supprimés
        if (!collabsDisabledInPast.isEmpty()) {
            collaborateurs.removeAll(collabsDisabledInPast);
        }
    }

    /**
     * @param collaborateurs a list of {@link Collaborateur}
     * @param yearFrais      year
     */
    protected final void removeDisabledLastYear(List<Collaborateur> collaborateurs, int yearFrais) {
        List<Collaborateur> collabsDisabledInPast = new ArrayList<>();
        // BAB - 21/06/2016  [AMILTONE 100] : on recupere tous les collabs
        for (Collaborateur collab : collaborateurs) {
            Date dateEntree = collab.getDateEntree();
            Date dateSortie = collab.getDateSortie();
            Calendar calendarEntree = GregorianCalendar.getInstance();
            Calendar calendarSortie = GregorianCalendar.getInstance();
            // on verifie que la date de sortie soit non nulle
            if (dateSortie != null) {
                calendarSortie.setTimeInMillis(dateSortie.getTime());
                // on filtre sur l'annee
                if (calendarSortie.get(Calendar.YEAR) < yearFrais) {
                    collabsDisabledInPast.add(collab);
                }
            }
            //On supprime les collab ajoutés les années suivant celle recherchée
            if (dateEntree != null) {
                calendarEntree.setTimeInMillis(dateEntree.getTime());
                if (calendarEntree.get(Calendar.YEAR) > yearFrais) {
                    collabsDisabledInPast.add(collab);
                }
            }
        }
        // si des collabs ont été supprimés dans le passé ils sont supprimés
        if (!collabsDisabledInPast.isEmpty()) {
            collaborateurs.removeAll(collabsDisabledInPast);
        }
    }

    /**
     * Get a list collaborators by status and enabled or not
     *
     * @param enabled Enable collaborator status
     * @param status the array of status
     * @return a list of collaborators
     */
    @Override
    public List<Collaborateur> findCollaboratorByStatus(boolean enabled, StatutCollaborateur... status){
        Criteria criteria = getCriteria();
        Disjunction or = Restrictions.disjunction();

        for(StatutCollaborateur statutCollaborator : status){
            or.add(Restrictions.eq(Collaborateur.PROP_STATUT, statutCollaborator));
        }
        criteria.add(or);

        if(enabled){
            criteria.add(Restrictions.eq(Collaborateur.PROP_ENABLED, enabled));
        }

        return criteria.list();
    }

    @Override
    public List<Collaborateur> findCollaboratorsEnabledByAgency(Agency agency){
        Criteria criteria = getCriteria();

        criteria.add(Restrictions.eq(Collaborateur.PROP_ENABLED, true));
        criteria.add(Restrictions.eq(Collaborateur.AGENCY, agency));

        return criteria.list();
    }

    @Override
    public List<Collaborateur> findCollaboratorsByManagerAndByAgency(Collaborateur manager, Agency agency){
        Criteria criteria = getCriteria();

        criteria.add(Restrictions.eq(Collaborateur.AGENCY, agency));
        criteria.add(Restrictions.eq(Collaborateur.PROP_ENABLED, true));
        criteria.add(Restrictions.eq(Collaborateur.PROP_MANAGER, manager));

        return criteria.list();
    }

}
