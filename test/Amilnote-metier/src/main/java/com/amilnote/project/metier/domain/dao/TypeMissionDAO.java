/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.TypeMission;

/**
 * The interface Type mission dao.
 */
public interface TypeMissionDAO extends LongKeyDAO<TypeMission> {
}
