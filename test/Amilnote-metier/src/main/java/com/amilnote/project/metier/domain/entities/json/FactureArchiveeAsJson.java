/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.FactureArchivee;

import java.util.Date;

/**
 * The type Facture archivee as json.
 */
public class FactureArchiveeAsJson {

    private int id;
    private String client;
    private String collaborateur;
    private String mission;
    private String contact;
    private String numCommande;
    private String cheminPDf;
    private float quantite;
    private float prix;
    private float montant;
    private int num_facture;
    private Date date_archivage;
    private Integer typeFacture;
    private Boolean isAvoir;


    /**
     * Instantiates a new Facture archivee as json.
     *
     * @param factureA the facture a
     */
    public FactureArchiveeAsJson(FactureArchivee factureA) {
        this.setId(factureA.getId());
        this.setClient(factureA.getClient());
        this.setCollaborateur(factureA.getCollaborateur());
        this.setMission(factureA.getMission());
        this.setQuantite(factureA.getQuantite());
        this.setPrix(factureA.getPrix());
        this.setMontant(factureA.getMontant());
        this.setContact(factureA.getContact());
        this.setNumCommande(factureA.getNumCommande());
        this.setCheminPDF(factureA.getCheminPDF());
        this.setNumero(factureA.getNumero());
        this.setDateArchivage(factureA.getDate_archivage());
        this.setTypeFacture(factureA.getTypeFacture());
        this.setAvoir(factureA.getAvoir());
    }

    /**
     * Instantiates a new Facture archivee as json.
     */
    public FactureArchiveeAsJson() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    public String getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collab the collab
     */
    public void setCollaborateur(String collab) {
        this.collaborateur = collab;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     * Gets num commande.
     *
     * @return the num commande
     */
    public String getNumCommande() {
        return numCommande;
    }

    /**
     * Sets num commande.
     *
     * @param commande the commande
     */
    public void setNumCommande(String commande) {
        this.numCommande = commande;
    }

    /**
     * Gets contact.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets contact.
     *
     * @param contact the contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Gets chemin pdf.
     *
     * @return the chemin pdf
     */
    public String getCheminPDF() {
        return cheminPDf;
    }

    /**
     * Sets chemin pdf.
     *
     * @param chemin the chemin
     */
    public void setCheminPDF(String chemin) {
        this.cheminPDf = chemin;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param q the q
     */
    public void setQuantite(float q) {
        this.quantite = q;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param p the p
     */
    public void setPrix(float p) {
        this.prix = p;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param m the m
     */
    public void setMontant(float m) {
        this.montant = m;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    public int getNumero() {
        return num_facture;
    }

    /**
     * Sets numero.
     *
     * @param m the m
     */
    public void setNumero(int m) {
        this.num_facture = m;
    }

    public void setDateArchivage(Date dateArchivage) {
        this.date_archivage = dateArchivage;
    }

    /**
     *
     * @return date_archivage
     */
    public Date getDate_archivage() {
        return date_archivage;
    }

    /**
     * Gets the type facture
     *
     * @return the type facture
     */
    public Integer getTypeFacture() {
        return typeFacture;
    }

    /**
     * Sets the type facture
     *
     * @param typeFacture the type facture
     */
    public void setTypeFacture(Integer typeFacture) {
        this.typeFacture = typeFacture;
    }


    /**
     * Gets the avoir
     *
     * @return the avoir
     */
    public Boolean getAvoir() {
        return isAvoir;
    }

    /**
     * Sets the avoir
     *
     * @param avoir the avoir
     */
    public void setAvoir(Boolean avoir) {
        isAvoir = avoir;
    }

}
