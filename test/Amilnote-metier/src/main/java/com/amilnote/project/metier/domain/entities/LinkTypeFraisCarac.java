/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.LinkTypeFraisCaracAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Link type frais carac.
 *
 * @author LSouai
 */
@Entity
@Table(name = "ami_link_type_frais_carac")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class LinkTypeFraisCarac implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPEFRAIS.
     */
    public static final String PROP_TYPEFRAIS = "typeFrais";
    /**
     * The constant PROP_CARAC.
     */
    public static final String PROP_CARAC = "carac";
    private static final long serialVersionUID = 5982965910248142854L;
    private Long id;
    private TypeFrais typeFrais;
    private Caracteristique carac;


    /**
     * Instantiates a new Link type frais carac.
     *
     * @param pTypeFrais the p type frais
     * @param pCarac     the p carac
     */
    public LinkTypeFraisCarac(TypeFrais pTypeFrais, Caracteristique pCarac) {
        super();
        typeFrais = pTypeFrais;
        carac = pCarac;
    }

    /**
     * Instantiates a new Link type frais carac.
     */
    public LinkTypeFraisCarac() {
    }


    /**
     * To json link type frais carac as json.
     *
     * @return the link type frais carac as json
     */
    public LinkTypeFraisCaracAsJson toJson() {
        return new LinkTypeFraisCaracAsJson(this);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    /**
     * mise a jour de l'id
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets carac.
     *
     * @return the carac
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_carac", nullable = true)
    public Caracteristique getCarac() {
        return carac;
    }

    /**
     * Sets carac.
     *
     * @param pCarac the p carac
     */
    public void setCarac(Caracteristique pCarac) {
        carac = pCarac;
    }

    /**
     * Gets type frais.
     *
     * @return the type frais
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_typeFrais", nullable = false)
    public TypeFrais getTypeFrais() {
        return typeFrais;
    }

    /**
     * Sets type frais.
     *
     * @param pTypeFrais the p type frais
     */
    public void setTypeFrais(TypeFrais pTypeFrais) {
        typeFrais = pTypeFrais;
    }
}
