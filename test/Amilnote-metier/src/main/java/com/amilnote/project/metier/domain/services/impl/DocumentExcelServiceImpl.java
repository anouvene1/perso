package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Pair;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import com.amilnote.project.metier.domain.utils.Utils;
import com.amilnote.project.metier.domain.utils.enumerations.ClientExcelEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFPrintSetup;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.amilnote.project.metier.domain.entities.Facture.PROP_DATE_FACTURATION;
import static com.amilnote.project.metier.domain.entities.Facture.PROP_NUMFACTURE;
import static com.amilnote.project.metier.domain.entities.TypeFacture.TYPEFACTURE_FACTURE_FRAIS;
import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.*;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.BETWEEN;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.NOT_EQUAL;

@Service("DocumentExcelService")
public class DocumentExcelServiceImpl implements DocumentExcelService {
    private static Logger logger = LogManager.getLogger(DocumentExcelServiceImpl.class);

    private static final String DECIMAL_FORMAT = "#,##0.00";
    private static final String TYPE_SIMPLE_LIST_EXPENSES = "simple";
    private static final String TYPE_DETAILED_LIST_EXPENSES = "detail";
    private static final String FILE_CREATION_ERROR_MESSAGE = "Erreur lors de la creation du fichier: ";
    private static final String COLLAB_STATUS_SS_TRAITANT = "NC";

    @Autowired
    private FactureService factureService;

    @Autowired
    private FileService fileService;

    @Autowired
    private GenerateExcelService generateExcelService;

    @Autowired
    private DocumentService documentService;
    /**
     * The Collaborator service.
     */
    @Autowired
    private CollaboratorService collaboratorService;
    /**
     * The Evenement service.
     */
    @Autowired
    private LinkEvenementTimesheetService evenementService;
    /**
     * The Ra service.
     */
    @Autowired
    private RapportActivitesService raService;
    /**
     * The Absence service.
     */
    @Autowired
    private AbsenceService absenceService;

    @Autowired
    private ContactClientService contactClientService;


    /**
     * Create a style for the a chosen group of cells
     *
     * @param style           CellStyle object
     * @param wb              new Workbook ('Workbook' means Microsoft Excel file)
     * @param cellAlignement  an enumeration of differents cell's alignment type (see : CellStyle class)
     * @param foreground      setting to one fills the cell with the foreground color
     * @param foregroundColor background color
     * @param font            specific font for header and title (bigger and bold)
     */
    private void cellStyleParameter(CellStyle style, Workbook wb, CellAlignment cellAlignement, boolean foreground, boolean foregroundColor, boolean font) {

        switch (cellAlignement) {
            case GENERAL:
                style.setAlignment(CellStyle.ALIGN_GENERAL);
                break;
            case LEFT:
                style.setAlignment(CellStyle.ALIGN_LEFT);
                break;
            case RIGHT:
                style.setAlignment(CellStyle.ALIGN_RIGHT);
                break;
            case FILL:
                style.setAlignment(CellStyle.ALIGN_FILL);
                break;
            case CENTER:
                style.setAlignment(CellStyle.ALIGN_CENTER);
                break;
            case JUSTIFY:
                style.setAlignment(CellStyle.ALIGN_JUSTIFY);
                break;
            default:
                style.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
                break;
        }

        if (foreground) {
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        }

        if (foregroundColor) {
            style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        }

        Font myfont = wb.createFont();
        if (font) {
            myfont.setBold(true);
            myfont.setFontHeight((short) (13 * 20));
            myfont.setBold(true);
            style.setFont(myfont);
        }
    }

    /**
     * {@linkplain DocumentExcelService#createExcelRevenues(String, Date, boolean)}
     */
    @Override
    public String createExcelRevenues(String typeExcel, Date chosenDate, boolean withYear) throws IOException, NamingException {

        String path = Parametrage.getContext(TEMP_FOLDER_FILES) + typeExcel + EXTENSION_FILE_XLS;
        fileService.createFolder(path);

        try (Workbook workbook = new HSSFWorkbook()) {
            CellStyle titlesStyle = workbook.createCellStyle();
            cellStyleParameter(titlesStyle, workbook, CellAlignment.CENTER, true, true, true);
            CellStyle defaultStyle = workbook.createCellStyle();
            cellStyleParameter(defaultStyle, workbook, CellAlignment.CENTER, false, false, false);
            CellStyle expenseStyle = getExpenseStyle(workbook); //Style for "Facture de frais" green row

            Sheet mySheet = workbook.createSheet();
            Row currentRow = mySheet.createRow(0);

            if (!withYear) {
                Utils.createCellule(currentRow, workbook, 0, HEADER_EXCEL_MONTHLY_REVENUES + Utils.monthString(chosenDate), null, titlesStyle);
            } else {
                Utils.createCellule(currentRow, workbook, 0, HEADER_EXCEL_YEARLY_REVENUES + Utils.yearString(), null, titlesStyle);
            }

            currentRow = mySheet.createRow(1);
            titlesStyle = workbook.createCellStyle();
            cellStyleParameter(titlesStyle, workbook, CellAlignment.CENTER, false, false, true);

            String[] columnsNames = {
                    COLUMN_EXCEL_ID,
                    COLUMN_EXCEL_FACTURE_NUMBER,
                    COLUMN_EXCEL_CLIENT,
                    COLUMN_EXCEL_COLLABORATOR,
                    COLUMN_EXCEL_MISSION_MANAGER,
                    COLUMN_EXCEL_WORKED_DAYS,
                    COLUMN_EXCEL_TJM,
                    COLUMN_EXCEL_FEES,
                    COLUMN_EXCEL_PRE_TAX_AMOUNT,
                    COLUMN_EXCEL_POST_TAX_AMOUNT,
                    COLUMN_EXCEL_BENEFIT_MONTH,
                    COLUMN_EXCEL_BILLING_MONTH,
                    COLUMN_EXCEL_TYPE};

            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) columnsNames.length - 1));

            // put the column names in the excel sheet
            for (int j = 0; j < columnsNames.length; j++) {
                Cell column = currentRow.createCell(j);
                column.setCellValue(columnsNames[j]);
                column.setCellStyle(titlesStyle);
            }

            DateTime[] startEndDates = new DateTime[2];
            if (withYear) {
                startEndDates[0] = new DateTime(chosenDate).monthOfYear().withMinimumValue().dayOfMonth().withMinimumValue();
                startEndDates[1] = new DateTime(chosenDate).monthOfYear().withMaximumValue().dayOfMonth().withMaximumValue();
            } else {
                DateTime wantedDate = new DateTime(chosenDate);
                startEndDates = Utils.firstAndLastDatesOfMonth(wantedDate.getYear(), wantedDate.getMonthOfYear());
            }

            List<Facture> invoiceList = factureService.findBySearchCriterias(
                    PROP_NUMFACTURE,
                    true,
                    new SearchCriteria<>(PROP_NUMFACTURE, NOT_EQUAL, 0),
                    new SearchCriteria<>(PROP_DATE_FACTURATION, BETWEEN, startEndDates[0].toDate(), startEndDates[1].toDate())
            );

            invoiceList.sort(Comparator.comparing(Facture::getNumFacture));

            int rowNumber = 2;

            double totalRevenues = 0;

            DateFormat monthAndYear = new SimpleDateFormat(DATE_FORMAT_MMMM_YYYY);
            DateFormat formatter = new SimpleDateFormat(STANDARD_DATE_FORMAT_SLASH);

            CellStyle styleCell = workbook.createCellStyle();
            cellStyleParameter(styleCell, workbook, CellAlignment.CENTER, true, true, false);
            CellStyle style = defaultStyle; // Formatting the white cells

            for (Facture invoice : invoiceList) {
                TypeFacture typeFacture = invoice.getTypeFacture();
                style = defaultStyle;

                if ( rowNumber % 2 == 0) {
                    style = styleCell; // Formatting the grey cells
                }
                if (typeFacture.getCode().contains(TYPEFACTURE_FACTURE_FRAIS)) {
                    style = expenseStyle; // Formatting the green cells (facture frais)
                }

                double tempRevenueWithoutVAT = factureService.calculateInvoicesTotalAmount(false, invoice);

                addExcelRow(
                        rowNumber,
                        mySheet,
                        null,
                        style,
                        rowNumber - 1,
                        invoice.getNumFacture(),
                        invoice.getCommande() != null ? invoice.getCommande().getClient().getNom_societe() : invoice.getMission().getClient().getNom_societe(),
                        fullNameCollabByFacture(invoice),
                        invoice.getMission().getManager() != null ? fullNameManagerByFacture(invoice) : fullNameManagerByCollabByFacture(invoice),
                        roundNumber(factureService.calculateWorkedDaysforInvoicesExcel(invoice), 2),
                        roundNumber(invoice.getPrix(), 2),
                        roundNumber(factureService.getMontantFrais(invoice), 2),
                        roundNumber(tempRevenueWithoutVAT, 2),
                        roundNumber(factureService.calculateTotalAmountWithVAT(tempRevenueWithoutVAT, invoice.getTVA().getMontant()), 2),
                        monthAndYear.format(invoice.getMoisPrestations()),
                        formatter.format(invoice.getDateFacture()),
                        collaboratorTypeByFacture(invoice)
                );

                totalRevenues += tempRevenueWithoutVAT;

                rowNumber++;

            }

            Row currentRowTemp = mySheet.createRow(rowNumber);
            Utils.createCellule(currentRowTemp, workbook, 7, TOTAL_MONTANT_CA_HT, null, style);
            Utils.createCellule(currentRowTemp, workbook, 8, String.valueOf(roundNumber(totalRevenues, 2)), null, style);


            for (int k = 0; k <= columnsNames.length; k++) {
                mySheet.autoSizeColumn(k, true);
            }
            fileService.writeFileOutputStream(path, workbook);

        } catch (FileNotFoundException ex) {
            logger.error(FILE_CREATION_ERROR_MESSAGE + ex.getMessage());
            throw ex;
        }
        return path;
    }

    @Override
    public String createExcelFacturationMois(File file, Date pDate, boolean annee) throws InvalidFormatException, IOException, ParseException, NamingException {
        String path = Parametrage.getContext(TEMP_FOLDER_FILES) + file + EXTENSION_FILE_XLS;

        documentService.creerDossiersPourFichier(path);

        Workbook wb = new HSSFWorkbook();
        List<Collaborator> collaborateurs = collaboratorService.findAllOrderByNomAscWithoutADMIN();

        try (FileOutputStream out = new FileOutputStream(path)) { //on peut maintenant créer le fichier voulu
            int rowNumber = 0;
            Sheet mySheet = DocumentServiceImpl.createSheetFacture(wb);

            //Récupération de la date en Calendar pour month
            Calendar cal = Calendar.getInstance();
            cal.setTime(pDate);
            int month = cal.get(Calendar.MONTH);
            int year = cal.get(Calendar.YEAR);
            int nbColumns = 7;

            // first row
            Row firstRow = mySheet.createRow(0);
            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) nbColumns - 1));
            CellStyle stl = wb.createCellStyle();
            stl.setAlignment(CellStyle.ALIGN_CENTER);
            Font fnt = wb.createFont();
            fnt.setBold(true);
            fnt.setFontHeight((short) (16 * 20));
            stl.setFont(fnt);


            //2eme LIGNE
            rowNumber++;
            Row myRow = mySheet.createRow(rowNumber);
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            style.setWrapText(true);
            Font font = wb.createFont();
            font.setBold(true);
            style.setFont(font);
            // ID
            Utils.createCellule(myRow, wb, 0, "Id", null, style);
            // Nom
            Utils.createCellule(myRow, wb, 1, COLUMN_EXCEL_LAST_NAME, null, style);
            // Mission
            Utils.createCellule(myRow, wb, 2, COLUMN_EXCEL_MISSION, null, style);
            // Client
            Utils.createCellule(myRow, wb, 3, COLUMN_EXCEL_CLIENT, null, style);
            // TJM
            Utils.createCellule(myRow, wb, 4, COLUMN_EXCEL_TJM, null, style);
            // Nombre de jours
            Utils.createCellule(myRow, wb, 5, "Nombre de jours à facturer", null, style);
            // Total
            Utils.createCellule(myRow, wb, 6, COLUMN_EXCEL_TOTAL, null, style);

            CellStyle styleDetailAbs = wb.createCellStyle();
            styleDetailAbs.setAlignment(CellStyle.ALIGN_LEFT);
            styleDetailAbs.setVerticalAlignment(CellStyle.ALIGN_CENTER);
            styleDetailAbs.setWrapText(true);

            for (Collaborator collaborateur : collaborateurs) {    // pour chaque collaborateur:
                if (!annee) {
                    rowNumber = documentService.createLinesByRA(collaborateur, month, year, rowNumber, mySheet);
                } else {
                    documentService.createLinesByMissions(collaborateur, year, rowNumber, mySheet);
                }
            }

            //une ligne sur 2 grisée
            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
            styleCell.setWrapText(true);

            for (int j = 2; j < mySheet.getLastRowNum(); j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                }
            }

            // autosize columns
            int itC = 0;
            while (itC <= nbColumns) {
                mySheet.autoSizeColumn(itC, true);
                itC++;
            }

            mySheet.createFreezePane(2, 2);

            // Fermeture du classeur et du fichier.
            wb.write(out);
        } finally {
            wb.close();
        }
        return path;
    }

    @Override
    public String createExcelAbsenceMois(File fichier, Date pDate, boolean typeConsultant) throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException {
        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;

        documentService.creerDossiersPourFichier(chemin);

        //on peut maintenant créer le fichier voulu sans problème
        List<CollaboratorAsJson> tmpListCollaboratorsAsJson = new ArrayList<>();
        Workbook wb = new HSSFWorkbook();
        List<CollaboratorAsJson> tmpListCollaboratorsAsJsonTemp =
                collaboratorService.findCollaboratorsForAbsenceMonthAsJson(pDate);

        //On garde seulement les sous-traitants si on les veut, sinon on garde seulement les collabs
        if (typeConsultant) {
            for (CollaboratorAsJson coll : tmpListCollaboratorsAsJsonTemp) {
                if (coll.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                    tmpListCollaboratorsAsJson.add(coll);
                }
            }
        } else {
            for (CollaboratorAsJson coll : tmpListCollaboratorsAsJsonTemp) {
                if (!coll.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                    tmpListCollaboratorsAsJson.add(coll);
                }
            }
        }

        try (FileOutputStream out = new FileOutputStream(chemin)) {
            Sheet mySheet = wb.createSheet();
            // Sheet mySheet = createSheetAbsPres(wb);
            Row myRow = mySheet.createRow(0);
            int i = 2;

            // Création de la première ligne avec le mois correspondant
            Calendar calendrier = Calendar.getInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(pDate);
            calendrier.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
            String mois = Utils.moisIntToString(calendrier.get(Calendar.MONTH));

            for (int j = 12; j < 20; j++) {
                mySheet.setColumnWidth(j, 15 * 256);
            }

            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) 22));
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short) (16 * 20));
            style.setFont(font);
            Utils.createCellule(myRow, wb, 0, "LISTE COLLABORATEURS / " + mois, null, style);

            // Création de la deuxième ligne avec les titres des colonnes
            myRow = mySheet.createRow(1);
            style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            style.setWrapText(true);
            font = wb.createFont();
            font.setBold(true);
            style.setFont(font);

			/* Numéro, Nom, Prénom, Absence Autorisée, Absence Exceptionnelle, Congé Payé, Congé Sans Solde, RTT,
            Congé Paternité, Maladie, Congé Maternité, Congé Naissance, Nombre de jours de présence, Total,
			Détail Absence Autorisée, Détail Absence Exceptionnelle, Détail Congé Payé, Détail Congé Sans Solde,
			Détail RTT, Détail Congé Paternité, Détail Maladie, Détail Congé Maternité, Détail Congé Naissance
			 */
            String[] columnsNames = {COLUMN_EXCEL_ID, COLUMN_EXCEL_LAST_NAME, COLUMN_EXCEL_FIRST_NAME, "AA", "AE", "CP", "CSS", "RTT", "CPat", "MA", "CMat",
                    "CN", "Nb Jours de présence", COLUMN_EXCEL_TOTAL, "AA", "AE", "CP", "CSS", "RTT", "CPat", "MA", "CMat", "CN"};

            for (int k = 0; k < 23; k++) {
                Utils.createCellule(myRow, wb, k, columnsNames[k], null, style);
            }

            CellStyle styleDetailAbs = wb.createCellStyle();
            styleDetailAbs.setAlignment(CellStyle.ALIGN_LEFT);
            styleDetailAbs.setVerticalAlignment(CellStyle.ALIGN_CENTER);
            styleDetailAbs.setWrapText(true);

            for (CollaboratorAsJson collaborateur : tmpListCollaboratorsAsJson) {
                float absenceAA = 0f;
                String absAA = "";
                float absenceAE = 0f;
                String absAE = "";
                float absenceCP = 0f;
                String absCP = "";
                float absenceCS = 0f;
                String absCS = "";
                float absenceRTT = 0f;
                String absRTT = "";
                float absencePAT = 0f;
                String absPAT = "";
                float absenceMA = 0f;
                String absMA = "";
                float absenceMT = 0f;
                String absMT = "";
                float absenceCN = 0f;
                String absCN = "";

                Date today = calendrier.getTime();
                // On stock la date du premier jour du mois
                SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_01);
                Date dateDeb = today;
                String dateString = formater.format(dateDeb);
                dateDeb = formater.parse(dateString);

                // On stock la date du dernier jour du mois
                Calendar c = Calendar.getInstance();
                Date dateFin = today;
                c.setTime(dateFin);
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                dateFin = c.getTime();
                formater = new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
                dateString = formater.format(dateFin);
                dateFin = formater.parse(dateString);

                // On converti le collaborateurJson en collaborateur.
                Collaborator collab = new Collaborator();
                collab.setId(collaborateur.getId());

                // On récupère tous les évènements du mois en cours
                List<LinkEvenementTimesheet> listEvent = evenementService.findEventsBetweenDates(collab, dateDeb,
                        dateFin);
                float nbJoursPresence = 0f;

                // si liste d'event du collab entre la date de debut et de fin est differente de zero
                if (listEvent != null) {
                    // BOUCLE SUR EVENT
                    for (LinkEvenementTimesheet event : listEvent) {
                        Absence abs = event.getAbsence();
                        if (abs != null) {

                            // etatCodeAbsence nous rend le code de l'etat c'est
                            // a dire VA ou etc
                            String etatCodeAbsence = abs.getEtat().getCode();

                            // Si l'évènement est une absence à l'état VALIDE,
                            // alors on ajoute 0.5 dans le bon type d'absence
                            if (Etat.ETAT_VALIDE_CODE.equals(etatCodeAbsence) || Etat.ETAT_SOUMIS_CODE.equals(etatCodeAbsence)) {

                                // CodeAbsence nous rend le code du type
                                // del'absence c'est a dire AA CP ou RTT etc
                                String codeAbsence = abs.getTypeAbsence().getCode();

                                switch (codeAbsence) {

                                    case TypeAbsence.TYPE_AA:
                                        absenceAA += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_AE:
                                        absenceAE += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_CP:
                                        absenceCP += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_CS:
                                        absenceCS += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_RTT:
                                        absenceRTT += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_PAT:
                                        absencePAT += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_MA:
                                        absenceMA += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_MT:
                                        absenceMT += 0.5;
                                        break;
                                    case TypeAbsence.TYPE_CN:
                                        absenceCN += 0.5;
                                        break;
                                    default:
                                        break;
                                }
                            } else {
                                List<RapportActivites> listRA = raService.getExistingRAForMonth(collab, new DateTime(today));
                                if (!listRA.isEmpty()) {
                                    nbJoursPresence += 0.5;
                                } else {
                                    nbJoursPresence = 0;
                                }
                            }

                        } else {
                            List<RapportActivites> listRA = raService.getExistingRAForMonth(collab, new DateTime(today));
                            if (!listRA.isEmpty()) {
                                if (event.getMission() != null) {
                                    nbJoursPresence += 0.5;
                                }
                            } else {
                                nbJoursPresence = 0;
                            }
                        }
                    }
                } else {
                    System.err.println(" :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
                            + "LISTEVENT IS NULL"
                            + ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");
                }

                // On affiche les colonnes de détails
                List<Absence> listAbs = absenceService.findByCollaboratorBetweenDate(collab, dateDeb, dateFin);
                List<Absence> listAbsences = absenceService.findDemiJourDebutFinAbsence(listAbs);
                for (Absence absence : listAbsences) {
                    String etatCodeAbsence = absence.getEtat().getCode();

                    // Si l'évènement est une absence à l'état VALIDE ou SOUMIS ,on rempli la case de détail
                    if (Etat.ETAT_VALIDE_CODE.equals(etatCodeAbsence) || Etat.ETAT_SOUMIS_CODE.equals(etatCodeAbsence)) {
                        String format = "dd/MM/YY HH:mm";
                        SimpleDateFormat formateur = new SimpleDateFormat(format);
                        String absenceDateDeb = formateur.format(absence.getDateDebut());
                        absenceDateDeb = absenceDateDeb.replace("08:00", "Matin");
                        absenceDateDeb = absenceDateDeb.replace("13:00", "Après Midi");
                        String absenceDateFin = formateur.format(absence.getDateFin());
                        absenceDateFin = absenceDateFin.replace("12:00", "Matin");
                        absenceDateFin = absenceDateFin.replace("18:00", "Après Midi");

                        switch (absence.getTypeAbsence().getCode()) {
                            case TypeAbsence.TYPE_AA:
                                absAA = documentService.updateAbsenceDates(absAA, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_AE:
                                absAE = documentService.updateAbsenceDates(absAE, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_CP:
                                absCP = documentService.updateAbsenceDates(absCP, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_CS:
                                absCS = documentService.updateAbsenceDates(absCS, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_RTT:
                                absRTT = documentService.updateAbsenceDates(absRTT, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_PAT:
                                absPAT = documentService.updateAbsenceDates(absPAT, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_MA:
                                absMA = documentService.updateAbsenceDates(absMA, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_MT:
                                absMT = documentService.updateAbsenceDates(absMT, absenceDateDeb, absenceDateFin);
                                break;
                            case TypeAbsence.TYPE_CN:
                                absCN = documentService.updateAbsenceDates(absCN, absenceDateDeb, absenceDateFin);
                                break;
                            default:
                                break;
                        }
                    }
                }

                // Création de la ligne
                myRow = mySheet.createRow(i);

                // Ajouter des données dans les cellules
                myRow.createCell(0).setCellValue(i - 1);
                myRow.createCell(1).setCellValue(collaborateur.getNom());
                myRow.createCell(2).setCellValue(collaborateur.getPrenom());
                myRow.createCell(3).setCellValue(absenceAA);
                myRow.createCell(4).setCellValue(absenceAE);
                myRow.createCell(5).setCellValue(absenceCP);
                myRow.createCell(6).setCellValue(absenceCS);
                myRow.createCell(7).setCellValue(absenceRTT);
                myRow.createCell(8).setCellValue(absencePAT);
                myRow.createCell(9).setCellValue(absenceMA);
                myRow.createCell(10).setCellValue(absenceMT);
                myRow.createCell(11).setCellValue(absenceCN);
                myRow.createCell(12).setCellValue(nbJoursPresence);
                myRow.createCell(13).setCellValue(nbJoursPresence + absenceAA + absenceAE + absenceCP + absenceCS
                        + absenceRTT + absencePAT + absenceMA + absenceMT + absenceCN);

                // Cellule détail AA
                Utils.createCellule(myRow, wb, 14, absAA, null, styleDetailAbs);
                // Cellule détail AE
                Utils.createCellule(myRow, wb, 15, absAE, null, styleDetailAbs);
                // Cellule détail CP
                Utils.createCellule(myRow, wb, 16, null, new HSSFRichTextString(absCP), styleDetailAbs);
                // Cellule détail CS
                Utils.createCellule(myRow, wb, 17, null, new HSSFRichTextString(absCS), styleDetailAbs);
                // Cellule détail RTT
                Utils.createCellule(myRow, wb, 18, null, new HSSFRichTextString(absRTT), styleDetailAbs);
                // Cellule détail PAT
                Utils.createCellule(myRow, wb, 19, null, new HSSFRichTextString(absPAT), styleDetailAbs);
                // Cellule détail MA
                Utils.createCellule(myRow, wb, 20, null, new HSSFRichTextString(absMA), styleDetailAbs);
                // Cellule détail MT
                Utils.createCellule(myRow, wb, 21, null, new HSSFRichTextString(absMT), styleDetailAbs);
                // Cellule détail CN
                Utils.createCellule(myRow, wb, 22, null, new HSSFRichTextString(absCN), styleDetailAbs);
                i++;
            }

            //une ligne sur 2 grisée
            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
            styleCell.setWrapText(true);

            for (int j = 2; j < mySheet.getLastRowNum(); j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                }
            }

            int nbColumn = 0;
            while (nbColumn < 22) {
                mySheet.autoSizeColumn(nbColumn, true);
                nbColumn++;
            }

            mySheet.createFreezePane(3, 2); // this will freeze first 2 rows(titres) and 3 columns (id,nom,prénom)
            // Fermeture du classeur et du fichier.
            wb.write(out);
        } finally {
            wb.close();
        }

        return chemin;
    }

    @Override
    public String createExcelFraisMois(File fichier, Date dateDebut, Date dateFin) throws ParseException, IOException, NamingException {
        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;
        try {
            documentService.creerDossiersPourFichier(chemin);

            List<Object[]> listFinal;
            List<Object[]> listTotal = documentService.generateListeFrais(dateDebut, dateFin, TYPE_DETAILED_LIST_EXPENSES);

            //Classement des données et remplacement des id par nom et prenom
            listFinal = generateExcelService.finalizeTableListExpensesDetail(listTotal, dateDebut, false);
            chemin = generateExcelService.generateExcel(chemin, dateDebut, TYPE_DETAILED_LIST_EXPENSES, listFinal);

        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        } catch (Exception e) {
            logger.error(DocumentServiceImpl.class + ": Error => " + e.getMessage());
        }
        return chemin;
    }

    @Override
    public String createExcelFraisMoisSimple(File fichier, Date dateDebut, Date dateFin) throws ParseException, IOException, NamingException {
        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;
        try {
            documentService.creerDossiersPourFichier(chemin);

            List<Object[]> listFinal;
            List<Object[]> listTotal = documentService.generateListeFrais(dateDebut, dateFin, TYPE_SIMPLE_LIST_EXPENSES);

            //Classement des données et remplacement des id par nom et prenom
            listFinal = generateExcelService.finalizeTableList(listTotal, dateDebut);
            chemin = generateExcelService.generateExcel(chemin, dateDebut, TYPE_SIMPLE_LIST_EXPENSES, listFinal);

        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        } catch (Exception e) {
            logger.error(DocumentServiceImpl.class + ": Error => " + e.getMessage());
        }
        return chemin;
    }

    @Override
    public String createExcelFraisAnnuel(File file, int yearFrais) {
        String chemin = null;
        try {
            chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + file + EXTENSION_FILE_XLS;
        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        }

        /* Setting Start date & End Date */
        Calendar calDeb;
        Calendar calFin;
        Date date;
        Integer nbJours;

        // On stock la date du premier jour du mois
        Date dateDeb, dateFin;
        calDeb = Calendar.getInstance();
        calFin = Calendar.getInstance();
        date = calDeb.getTime();
        calDeb.set(Calendar.DATE, 1);
        calDeb.set(Calendar.MONTH, 0);
        calDeb.set(Calendar.YEAR, yearFrais);
        nbJours = calDeb.getActualMaximum(Calendar.DAY_OF_MONTH);
        calFin.set(Calendar.DATE, nbJours);
        calFin.set(Calendar.MONTH, 11);
        calFin.set(Calendar.YEAR, yearFrais);

        dateDeb = calDeb.getTime();

        dateFin = calFin.getTime();
        try {
            documentService.creerDossiersPourFichier(chemin);

            List<Object[]> listFinal;
            List<Object[]> listTotal = documentService.generateListeFrais(dateDeb, dateFin, TYPE_DETAILED_LIST_EXPENSES);

            //Classement des données et remplacement des id par nom et prenom
            listFinal = generateExcelService.finalizeTableListExpensesDetail(listTotal, dateDeb, true);
            chemin = generateExcelService.generateExcel(chemin, date, TYPE_DETAILED_LIST_EXPENSES, listFinal);
        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        } catch (Exception e) {
            logger.error(DocumentServiceImpl.class + ": Error => " + e.getMessage());
        }

        return chemin;
    }

    /**
     * {@linkplain DocumentExcelService#createExcelFraisAnnuelSimple(File, int)}
     */
    @Override
    public String createExcelFraisAnnuelSimple(File file, int yearFrais) {

        String chemin = null;
        try {
            chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + file + EXTENSION_FILE_XLS;
        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        }
        List<List<Object[]>> listTotal;
        List<Object[]> listFinal;
        Calendar calDeb;
        Calendar calFin;
        Date date;
        Integer nbJours;

        // On stock la date du premier jour du mois
        Date dateDeb, dateFin;

        try {
            listTotal = new ArrayList<>(12);
            calDeb = Calendar.getInstance();
            calFin = Calendar.getInstance();
            date = calDeb.getTime();
            for (Integer m = 0; m <= 11; m++) {
                calDeb.set(Calendar.DATE, 1);
                calDeb.set(Calendar.MONTH, m);
                calDeb.set(Calendar.YEAR, yearFrais);
                nbJours = calDeb.getActualMaximum(Calendar.DAY_OF_MONTH);
                calFin.set(Calendar.DATE, nbJours);
                calFin.set(Calendar.MONTH, m);
                calFin.set(Calendar.YEAR, yearFrais);

                dateDeb = calDeb.getTime();

                dateFin = calFin.getTime();

                List<Object[]> listMois = documentService.generateListeFrais(dateDeb, dateFin, TYPE_SIMPLE_LIST_EXPENSES);
                /**
                 * l'objet renvoyé par getListeFrais contient les champs suivant :
                 * ID
                 * Avance sur frais
                 * Avance sur frais permanente (non implemente, a ignorer)
                 * Frais
                 * Forfait
                 * Frais + Forfait + Avance sur frais
                 * Frais + Forfait
                 */
                listTotal.add(listMois);

            }
            listFinal = generateExcelService.finalizeTableListAnnee(listTotal, yearFrais);
            chemin = generateExcelService.generateExcel(chemin, date, "annee", listFinal);
        } catch (NamingException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
        } catch (Exception e) {
            logger.error(DocumentServiceImpl.class + ": error => " + e.getMessage());
        }

        return chemin;
    }

    @Override
    public String createExcelPresenceMois(File fichier, int year, boolean typeConsultant) throws InvalidFormatException, ParseException, IOException, NamingException {

        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;

        documentService.creerDossiersPourFichier(chemin);

        Workbook wb = new HSSFWorkbook();
        List<CollaboratorAsJson> tmpListCollaboratorsAsJson = new ArrayList<>();
        List<CollaboratorAsJson> tmpListCollaboratorsAsJsonTemp =
                collaboratorService.findAllOrderByNomAscAsJsonWithoutADMIN();

        //On garde seulement les sous-traitants si on les veut, sinon on garde seulement les collabs
        if (typeConsultant) {
            for (CollaboratorAsJson coll : tmpListCollaboratorsAsJsonTemp) {
                if (coll.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                    tmpListCollaboratorsAsJson.add(coll);
                }
            }
        } else {
            for (CollaboratorAsJson coll : tmpListCollaboratorsAsJsonTemp) {
                if (!coll.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
                    tmpListCollaboratorsAsJson.add(coll);
                }
            }
        }

        try (FileOutputStream out = new FileOutputStream(chemin)) { //on peut maintenant créer le fichier voulu
            Sheet mySheet = wb.createSheet();
            Row myRow = mySheet.createRow(0);

            // Création du titre contenu dans la premiere ligne
            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) 24));
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short) (16 * 20));
            style.setFont(font);
            Utils.createCellule(myRow, wb, 0, "Jours de présence / collaborateur / mois / " + year, null, style);

            // Création de la deuxième ligne avec les titres des colonnes
            myRow = mySheet.createRow(1);
            // Numéro
            style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            font = wb.createFont();
            font.setBold(true);
            style.setFont(font);

            String[] columnsNames = {COLUMN_EXCEL_ID, COLUMN_EXCEL_LAST_NAME, COLUMN_EXCEL_FIRST_NAME, "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
                    "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre", COLUMN_EXCEL_TOTAL};

            for (int i = 0; i < 16; i++) {
                Utils.createCellule(myRow, wb, i, columnsNames[i], null, style);
            }

            // debut des lignes a ecrire
            int i = 2;

            // liste qui regroupe le nombre de jour dans tout les mois de
            // l'année
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, Calendar.FEBRUARY);

            String formatDateDeb = DATE_FORMAT_YYYY_MM_01;
            String formatDateFin = DATE_FORMAT_YYYY_MM_DD;
            Date dateDeb;
            Date dateFin;

            for (CollaboratorAsJson collaborateur : tmpListCollaboratorsAsJson) {
                // liste des presence de l'année par collab
                List<Float> nbreJoursPresenceByMonth = new ArrayList<>(13);
                for (int j = 0; j < 13; j++) {
                    nbreJoursPresenceByMonth.add(0.0f);
                }

                for (int m = 1; m <= 12; m++) {

                    float nbJoursPresence = 0;
                    cal = Calendar.getInstance();
                    cal.set(Calendar.YEAR, year);
                    cal.set(Calendar.MONTH, m - 1);
                    Date date = cal.getTime();

                    String dateDeDebutDuMois = new SimpleDateFormat(formatDateDeb).format(date);

                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                    date = cal.getTime();
                    String dateDeFinDuMois = new SimpleDateFormat(formatDateFin).format(date);

                    // transfo des date de debut et fin de mois en format "Date"
                    dateDeb = new SimpleDateFormat(formatDateDeb).parse(dateDeDebutDuMois);
                    dateFin = new SimpleDateFormat(formatDateFin).parse(dateDeFinDuMois);

                    Collaborator collab = new Collaborator();
                    collab.setId(collaborateur.getId());

                    // On récupère tous les évènements du mois en cours
                    List<LinkEvenementTimesheet> listEvent = evenementService.findEventsBetweenDates(collab, dateDeb,
                            dateFin);

                    if (listEvent != null) {

                        for (LinkEvenementTimesheet event : listEvent) {
                            Absence abs = event.getAbsence();
                            if (abs != null) {
                                String etatCodeAbsence = event.getAbsence().getEtat().getCode();
                                if (!Etat.ETAT_VALIDE_CODE.equals(etatCodeAbsence) &&
                                        !Etat.ETAT_BROUILLON_CODE.equals(etatCodeAbsence)) {
                                    List<RapportActivites> listRA =
                                            raService.getExistingRAForMonth(collab, new DateTime(date));
                                    if (!listRA.isEmpty()) {
                                        nbJoursPresence += 0.5;
                                    } else {
                                        nbJoursPresence = 0;
                                    }
                                }
                            } else {
                                List<RapportActivites> listRA =
                                        raService.getExistingRAForMonth(collab, new DateTime(date));
                                if (!listRA.isEmpty()) {
                                    nbJoursPresence += 0.5;
                                } else {
                                    nbJoursPresence = 0;
                                }
                            }
                        }
                    }

                    nbreJoursPresenceByMonth.set(m - 1, nbJoursPresence);
                    nbreJoursPresenceByMonth.set(12,
                            nbreJoursPresenceByMonth.get(m - 1) + nbreJoursPresenceByMonth.get(12));

                } // boucle des mois m

                // Création de la ligne
                myRow = mySheet.createRow(i);

                // Ajouter des données dans les cellules
                myRow.createCell(0).setCellValue(i - 1);
                myRow.createCell(1).setCellValue(collaborateur.getNom());
                myRow.createCell(2).setCellValue(collaborateur.getPrenom());

                // presence par mois
                for (int h = 3; h <= 15; h++) {
                    myRow.createCell(h).setCellValue(nbreJoursPresenceByMonth.get(h - 3));
                }

                int column = 0;
                while (column < 15) {
                    mySheet.autoSizeColumn(column, true);
                    column++;
                }

                mySheet.createFreezePane(3, 2); // this will freeze first 2 rows(titres) and 3 columns (id,nom,prénom)

                i++;

            } // boucle fin collab

            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);

            for (int j = 2; j < mySheet.getLastRowNum(); j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                }
            }

            wb.write(out);
        } catch (FileNotFoundException ex) {
            logger.error(DocumentServiceImpl.class + ": error FileNotFoundException => " + ex.getMessage());
            throw ex;
        } finally {
            wb.close();
        }
        return chemin;
    }

    @Override
    public String createExcelClients(File fichier, ClientExcelEnum choixExcel) throws EncryptedDocumentException, IOException, NamingException {

        List<CollaboratorAsJson> tmpListCollaboratorsAsJsonTemp =
                collaboratorService.findAllOrderByNomAscAsJsonWithoutADMIN();

        List<CollaboratorAsJson> tmpListCollaboratorsAsJson = new ArrayList<>();

        // vérifie si le consultant est un sous-traitant, et rempli la liste en fonction
        for (CollaboratorAsJson collaborateur : tmpListCollaboratorsAsJsonTemp) {

            String statutCollaborator = collaborateur.getStatut().getCode();

            switch (choixExcel) {
                case CLIENTS_PAR_STT:
                case SOUSTRAITANTS_EN_MISSION:
                    if (StatutCollaborateur.STATUT_SOUS_TRAITANT.equals(statutCollaborator)) {
                        tmpListCollaboratorsAsJson.add(collaborateur);
                    }
                    break;
                case COLLABORATEUR_EN_MISSION:
                case CLIENTS_PAR_COLLAB:
                    if (!StatutCollaborateur.STATUT_SOUS_TRAITANT.equals(statutCollaborator)) {
                        tmpListCollaboratorsAsJson.add(collaborateur);
                    }
                    break;
            }
        }

        String chemin = createExcelClientsFile(fichier, tmpListCollaboratorsAsJson, choixExcel);
        return chemin;
    }

    @Override
    public String createExcelClientsFile(File fichier, List<CollaboratorAsJson> tmpListCollaboratorsAsJson, ClientExcelEnum choixExcel) throws EncryptedDocumentException, IOException, NamingException {
        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;

        documentService.creerDossiersPourFichier(chemin);

        Workbook wb = new HSSFWorkbook();

        // creation de la feuille excel
        try (FileOutputStream out = new FileOutputStream(chemin)) { //on peut maintenant créer le fichier voulu
            // créer une feuille
            Sheet mySheet = wb.createSheet();
            // creer la premier ligne >>> row(zero)
            Row myRow = mySheet.createRow(0);

            CellStyle style = wb.createCellStyle();
            Font font = wb.createFont();

            // prépare l'entête du fichier excel en fonction du Excel Client voulu ainsi que le style de la page
            documentService.prepareClientFile(choixExcel, font, mySheet, myRow, wb, style);

            // debut des lignes a ecrire

            int index = 2;

            for (CollaboratorAsJson collaboratorAsJson : tmpListCollaboratorsAsJson) {

                // la méthode renvoie a chaque fois l'index afin de savoir à quelle ligne en est l'écriture du Excel
                index = documentService.createClientsFileRows(choixExcel, collaboratorAsJson, myRow, mySheet, index);
            } // fin de la boucle des collab

            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);

            for (int j = 2; j < mySheet.getLastRowNum(); j += 2) {
                if (mySheet.getRow(j) != null) {
                    for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                        mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                    }
                }
            }
            // Fermeture du classeur et du fichier
            wb.write(out);
        } catch (FileNotFoundException ex) {
            logger.error(DocumentServiceImpl.class + ": error FileNotFoundException => " + ex.getMessage());
            throw ex;
        } finally {
            wb.close();
        }

        return chemin;
    }

    @Override
    public String createExcelCommandes(List<Commande> commandes, String fileName, String date) throws NamingException, IOException {

        String fichier = fileName;
        String chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;
        documentService.creerDossiersPourFichier(chemin);

        String arrayName[] = fichier.split(UNDERSCORE);

        Workbook workbook = new HSSFWorkbook();

        List<Commande> listeCommandes = commandes;
        try (FileOutputStream fileOutputStream = new FileOutputStream(chemin)) {

            int rowNumber = 0;
            Sheet sheet = workbook.createSheet();

            Row row = sheet.createRow(rowNumber++);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));
            CellStyle titleStyle = workbook.createCellStyle();
            titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
            Font fontTitle = workbook.createFont();
            fontTitle.setBold(true);
            fontTitle.setFontHeight((short) (13 * 20));
            titleStyle.setFont(fontTitle);

            String dateTitle = documentService.createTitle(arrayName, FRENCH_MONTHS_NAMES);

            Utils.createCellule(row, workbook, 0, dateTitle + date.substring(6), null, titleStyle);

            row = sheet.createRow(rowNumber++);
            titleStyle = workbook.createCellStyle();
            titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
            titleStyle.setWrapText(true);
            fontTitle = workbook.createFont();
            fontTitle.setBold(true);
            titleStyle.setFont(fontTitle);

            String[] columnsNames = {
                    COLUMN_EXCEL_ID,
                    COLUMN_EXCEL_NUMBER,
                    COLUMN_EXCEL_CLIENT,
                    COLUMN_EXCEL_COLLABORATOR,
                    COLUMN_EXCEL_MISSION,
                    COLUMN_EXCEL_BUSINESS_ENGINEER,
                    COLUMN_EXCEL_BUDGET,
                    COLUMN_EXCEL_STARTING_DATE,
                    COLUMN_EXCEL_ENDING_DATE
            };

            for (int i = 0; i < columnsNames.length; i++) {
                Utils.createCellule(row, workbook, i, columnsNames[i], null, titleStyle);
            }

            DateFormat formatter = new SimpleDateFormat(STANDARD_DATE_FORMAT_SLASH);

            CellStyle styleDecimal = workbook.createCellStyle();
            styleDecimal.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));

            CellStyle styleDecimalGrey = workbook.createCellStyle();
            styleDecimalGrey.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));
            styleDecimalGrey.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleDecimalGrey.setFillPattern(CellStyle.SOLID_FOREGROUND);

            CellStyle dateStyle = workbook.createCellStyle();
            dateStyle.setAlignment(CellStyle.ALIGN_RIGHT);

            CellStyle dateStyleGrey = workbook.createCellStyle();
            dateStyleGrey.setAlignment(CellStyle.ALIGN_RIGHT);
            dateStyleGrey.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            dateStyleGrey.setFillPattern(CellStyle.SOLID_FOREGROUND);

            for (Commande commande : listeCommandes) {

                row = sheet.createRow(rowNumber);

                // Assigning values for each column headers
                for (int columnNumber = 0; columnNumber < columnsNames.length; columnNumber++) {
                    switch (columnNumber) {

                        case 0:
                            row.createCell(columnNumber).setCellValue(rowNumber - 1);
                            break;
                        case 1:
                            row.createCell(columnNumber).setCellValue(commande.getNumero());
                            break;
                        case 2:
                            if (commande.getClient() != null) {
                                row.createCell(columnNumber).setCellValue(commande.getClient().getNom_societe());
                            } else {
                                row.createCell(columnNumber).setCellValue(NOT_ASSIGNED);
                            }
                            break;
                        case 3:
                            row.createCell(columnNumber).setCellValue(commande.getMission().getCollaborateur().toString());
                            break;
                        case 4:
                            row.createCell(columnNumber).setCellValue(commande.getMission().getMission());
                            break;
                        case 5:
                            Cell cellBusinessEngineer = row.createCell(columnNumber);
                            if (commande.getMission().getManager() != null) {
                                cellBusinessEngineer.setCellValue(commande.getMission().getManager().toString());
                            } else {
                                cellBusinessEngineer.setCellValue(NOT_ASSIGNED);
                            }
                            break;
                        case 6:
                            Cell cellBudget = row.createCell(columnNumber);
                            cellBudget.setCellValue(commande.getBudget());
                            cellBudget.setCellStyle(styleDecimal);
                            break;
                        case 7:
                            Cell cellDateDebut = row.createCell(columnNumber);
                            if (commande.getDateDebut() != null) {
                                cellDateDebut.setCellValue(formatter.format(commande.getDateDebut()));
                                cellDateDebut.setCellStyle(dateStyle);
                            } else {
                                cellDateDebut.setCellValue(NOT_ASSIGNED);
                            }
                            break;
                        case 8:
                            Cell cellDateFin = row.createCell(columnNumber);
                            if (commande.getDateFin() != null) {
                                cellDateFin.setCellValue(formatter.format(commande.getDateFin()));
                                cellDateFin.setCellStyle(dateStyle);
                            } else {
                                cellDateFin.setCellValue(NOT_ASSIGNED);
                            }
                            break;
                        default:
                            break;
                    }
                }
                rowNumber++;
            }

            CellStyle greyStyle = workbook.createCellStyle();
            greyStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            greyStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

            for (int i = 2; i <= sheet.getLastRowNum(); i += 2) {
                for (int j = 0; j < sheet.getRow(i).getLastCellNum(); j++) {
                    if (j < 5) {
                        sheet.getRow(i).getCell(j).setCellStyle(greyStyle);
                    } else if (j == 5) {
                        sheet.getRow(i).getCell(j).setCellStyle(styleDecimalGrey);
                    } else {
                        sheet.getRow(i).getCell(j).setCellStyle(dateStyleGrey);
                    }
                }
            }

            for (int i = 0; i < sheet.getRow(2).getPhysicalNumberOfCells(); i++) {
                sheet.autoSizeColumn(i, true);
            }

            workbook.write(fileOutputStream);

        } catch (FileNotFoundException e) {
            logger.error(FILE_CREATION_ERROR_MESSAGE + e.getMessage());
            throw e;
        } finally {
            workbook.close();
        }
        return chemin;
    }

    @Override
    public String createExcelRecapFacturesMois(List<Facture> listeFacture, DateTime date, String cheminMonthODF, String numODF, String strTypeFacture) throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException {
        String chemin;
        TypeFacture.InvoiceType invoiceType = TypeFacture.InvoiceType.valueOf(strTypeFacture);

        chemin = documentService.getCheminFichier(date, cheminMonthODF, numODF, invoiceType);
        documentService.creerDossiersPourFichier(chemin);
        createFichierExcel(listeFacture, date, numODF, invoiceType, chemin);

        return chemin;
    }

    @Override
    public String createExcelRecapFacturesFraisMois(List<Facture> listeFactureFrais, DateTime date, String cheminMonthODF, String numODF) throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException {
        String chemin;
        // [CLO AMNOTE-281 02/05/2017] nouvelle méthode pour créer un excel des factures de frais.
        String odf = null;
        String fichier;

        if (numODF == null) {
            fichier = concatenateObjectsToString("Recapitulatif_Factures_De_Frais_", Utils.moisIntToString(date.getMonthOfYear() - 1), UNDERSCORE, date.getYear());
            if (cheminMonthODF == null) {
                chemin = concatenateObjectsToString(Parametrage.getContext(TEMP_FOLDER_FILES), fichier, EXTENSION_FILE_XLS);
            } else {
                chemin = concatenateObjectsToString(Parametrage.getContext(TEMP_FOLDER_FILES), SLASH, cheminMonthODF, SLASH, fichier, EXTENSION_FILE_XLS);
            }
        } else {
            odf = numODF;
            fichier = concatenateObjectsToString("Factures_soumises_", Utils.moisIntToString(date.getMonthOfYear() - 1), UNDERSCORE, date.getYear(), UNDERSCORE, numODF);
            chemin = concatenateObjectsToString(Parametrage.getContext(TEMP_FOLDER_FILES), cheminMonthODF, SLASH, fichier, EXTENSION_FILE_XLS);
        }

        documentService.creerDossiersPourFichier(chemin);

        //on peut maintenant créer le fichier voulu sans problème
        try (
                FileOutputStream out = new FileOutputStream(chemin);
                Workbook wb = new HSSFWorkbook()
        ) {

            Sheet mySheet = DocumentServiceImpl.createSheetFacture(wb);
            Row myRow = mySheet.createRow(0);

            int i = 2;

            // styles
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short) (13 * 20));
            style.setFont(font);

            CellStyle dateStyle = wb.createCellStyle();
            dateStyle.setAlignment(CellStyle.ALIGN_RIGHT);

            DateFormat dateFormat = new SimpleDateFormat(STANDARD_DATE_FORMAT_SLASH);

            CellStyle decimalFormatBis = wb.createCellStyle();
            decimalFormatBis.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            decimalFormatBis.setFillPattern(CellStyle.SOLID_FOREGROUND);
            decimalFormatBis.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));
            decimalFormatBis.setAlignment(CellStyle.ALIGN_RIGHT);

            CellStyle decimalFormat = wb.createCellStyle();
            decimalFormat.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));

            /// ID, Numéro de facture, Client, Contact, Collaborator, Manager, Nombre de jours, TJM, Montant HT
            String[] columnsNames = {
                    COLUMN_EXCEL_ID,
                    COLUMN_EXCEL_INVOICE_NUMBER,
                    COLUMN_EXCEL_CLIENT,
                    COLUMN_EXCEL_COLLABORATOR,
                    COLUMN_EXCEL_MISSON_MANAGER,
                    COLUMN_EXCEL_PRE_TAX_AMOUNT,
                    COLUMN_EXCEL_POST_TAX_AMOUNT,
                    COLUMN_EXCEL_BILLING_DATE
            };

            // première ligne "titre"
            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) columnsNames.length - 1));

            if (odf != null) {
                Utils.createCellule(myRow, wb, 0, "FACTURES DE FRAIS DU MOIS / " + Utils.moisIntToString(date.getMonthOfYear() - 1) + " / " + odf, null, style);
            } else {
                Utils.createCellule(myRow, wb, 0, "FACTURES DE FRAIS DU MOIS / " + Utils.moisIntToString(date.getMonthOfYear() - 1), null, style);
            }

            // deuxième ligne avec les titres des colonnes
            myRow = mySheet.createRow(1);
            // Numéro
            style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            font = wb.createFont();
            font.setBold(true);
            style.setFont(font);

            documentService.sortListFactures(listeFactureFrais);

            for (int j = 0; j < columnsNames.length; j++) {
                Utils.createCellule(myRow, wb, j, columnsNames[j], null, style);
            }

            //ajout de la TVA du montant HT et TTC
            double montantTva = 0d;
            double sommeMontant = 0;

            for (Facture facture : listeFactureFrais) {

                Pair<Float, Float> montants = factureService.getMontantHT(facture);
                double montantHT = montants.getFirst();
                double montantTTC = montants.getSecond();
                BigDecimal bd = new BigDecimal(montantHT);
                bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
                montantHT = bd.doubleValue();
                montantTva = facture.getTVA().getMontant() / 100d;

                // Création de la ligne
                myRow = mySheet.createRow(i);

                // Ajouter des données dans les cellules
                // num ligne
                myRow.createCell(0).setCellValue(i - 1);
                // Num Facture
                myRow.createCell(1).setCellValue(facture.getNumFacture());
                // Nom Societe
                if (facture.getCommande() != null) {
                    myRow.createCell(2).setCellValue(facture.getCommande().getClient().getNom_societe());
                } else {
                    myRow.createCell(2).setCellValue(facture.getMission().getClient().getNom_societe());
                }
                // Nom / Prénom Collaborator
                myRow.createCell(3).setCellValue(Utils.fullNameCollabByFacture(facture));
                // Nom / Prénom Manager
                if (facture.getMission().getManager() != null) {
                    myRow.createCell(4).setCellValue(Utils.fullNameManagerByFacture(facture));
                } else {
                    myRow.createCell(4).setCellValue(Utils.fullNameManagerByCollabByFacture(facture));
                }
                // montant HT
                Cell cellHT = myRow.createCell(5);
                cellHT.setCellValue(montantHT);
                cellHT.setCellStyle(decimalFormat);
                // montant TTC
                Cell cellTTC = myRow.createCell(6);
                cellTTC.setCellValue(montantTTC);
                cellTTC.setCellStyle(decimalFormat);
                // Date Facture
                Cell dateCell = myRow.createCell(7);
                dateCell.setCellValue(
                        dateFormat.format(facture.getDateFacture())
                );
                dateCell.setCellStyle(dateStyle);

                sommeMontant = sommeMontant + montantHT;
                i++;
            }

            //On ajoute les lignes totaux
            myRow = mySheet.createRow(i);
            myRow.createCell(4).setCellValue(TOTAL_MONTANT_HT);
            Cell cellHT = myRow.createCell(5);
            cellHT.setCellValue(sommeMontant);
            cellHT.setCellStyle(decimalFormat);

            i++;
            myRow = mySheet.createRow(i);
            myRow.createCell(4).setCellValue(MONTANT_TVA);
            Cell cellTotalTva = myRow.createCell(5);
            cellTotalTva.setCellValue(sommeMontant * montantTva);
            cellTotalTva.setCellStyle(decimalFormat);

            i++;
            myRow = mySheet.createRow(i);
            myRow.createCell(4).setCellValue(MONTANT_TTC);
            Cell cellTotalTTC = myRow.createCell(5);
            cellTotalTTC.setCellValue(sommeMontant * (1 + montantTva));
            cellTotalTTC.setCellStyle(decimalFormat);

            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);

            // [ALO] [Modification de la boucle pour ajouter le format décimal aux colones numériques paires
            for (int j = 2; j < mySheet.getLastRowNum() - 2; j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    // on récupère l'index de la dernière colonne
                    int lastCell = mySheet.getRow(2).getPhysicalNumberOfCells() - 1;
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                    mySheet.getRow(j).getCell(lastCell).setCellStyle(decimalFormatBis);
                    mySheet.getRow(j).getCell(lastCell - 1).setCellStyle(decimalFormatBis);
                }
            }

            for (int k = 0; k < 9; k++) {
                mySheet.autoSizeColumn(k, true);
            }

            // Fermeture du classeur et du fichier.
            wb.write(out);
        } catch (FileNotFoundException ex) {
            logger.error(ex.getMessage());
            throw ex;
        }

        return chemin;
    }

    @Override
    public void createFichierExcel(List<Facture> listeFacture, DateTime date, String numODF, TypeFacture.InvoiceType invoiceType, String chemin) throws IOException {

        try (
                FileOutputStream out = new FileOutputStream(chemin);
                Workbook wb = new HSSFWorkbook()
        ) {

            Sheet mySheet = DocumentServiceImpl.createSheetFacture(wb);
            Row myRow = mySheet.createRow(0);

            //[ALO] Pour arrondir les cellules au centième près, on déclare un style ensuite il faut l'ajouter aux cellules concernées
            CellStyle decimalFormat = wb.createCellStyle();
            decimalFormat.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));

            CellStyle decimalFormatBis = wb.createCellStyle();
            decimalFormatBis.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            decimalFormatBis.setFillPattern(CellStyle.SOLID_FOREGROUND);
            decimalFormatBis.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));
            decimalFormatBis.setAlignment(CellStyle.ALIGN_RIGHT);

            CellStyle rightAligned = wb.createCellStyle();
            rightAligned.setAlignment(CellStyle.ALIGN_RIGHT);

            int i = 2;

            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) 10));
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short) (13 * 20));
            style.setFont(font);

            if (numODF != null) {
                switch (invoiceType) {
                    case FRABLE:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_MONTHLY_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1) + " / " + numODF, null, style);
                        break;
                    case FRABLESST:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_SST_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1) + " / " + numODF, null, style);
                        break;
                    case FMAIN:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_HANDWRITTEN_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1) + " / " + numODF, null, style);
                        break;
                    default:
                        break;
                }
            } else {
                switch (invoiceType) {
                    case FRABLE:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_YEARLY_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1), null, style);
                        break;
                    case FRABLESST:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_SST_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1), null, style);
                        break;
                    case FMAIN:
                        Utils.createCellule(myRow, wb, 0, HEADER_EXCEL_HANDWRITTEN_INVOICES + Utils.moisIntToString(date.getMonthOfYear() - 1), null, style);
                        break;
                    default:
                        break;
                }
            }

            // Création de la deuxième ligne avec les titres des colonnes
            myRow = mySheet.createRow(1);
            // Numéro
            style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            font = wb.createFont();
            font.setBold(true);
            style.setFont(font);

            /// ID, Numéro de facture, Client, Contact, Collaborator, Nombre de jours, TJM, Montant HT
            List<String> columnNames = Arrays.asList(
                    COLUMN_EXCEL_ID,
                    COLUMN_EXCEL_INVOICE_NUMBER,
                    COLUMN_EXCEL_CLIENT,
                    COLUMN_EXCEL_COLLABORATOR,
                    COLUMN_EXCEL_MISSON_MANAGER,
                    COLUMN_EXCEL_CONTACT,
                    COLUMN_EXCEL_DAYS_NUMBER,
                    COLUMN_EXCEL_TJM,
                    COLUMN_EXCEL_OTHER_REVENUE,
                    COLUMN_EXCEL_PRE_TAX_AMOUNT,
                    COLUMN_EXCEL_POST_TAX_AMOUNT,
                    COLUMN_EXCEL_BILLING_DATE
            );

            for (int j = 0; j < columnNames.size(); j++) {
                Utils.createCellule(myRow, wb, j, columnNames.get(j), null, style);
            }

            //SBE_AMNOTE_176 24/11/2016 Modif de l'excel ajout de la TVA du montant HT et TTC
            double rateVAT = 0d;     // VAT Rate ( i.e 0.2 for 20% )
            double sumAmount = 0;    // Excluding taxes
            double sumVATAmount = 0; // Amount of VAT
            double sumInclTaxes = 0; // Inclusive taxes
            double roundSumMontant = 0;

            documentService.sortListFactures(listeFacture);

            for (Facture facture : listeFacture) {
                Pair<Float, Float> quantiteEtMontant = factureService.getQuantiteMontantAvecFrais(facture);
                float quantite = quantiteEtMontant.getFirst();
                float montant = quantiteEtMontant.getSecond();
                float fraisTJM = documentService.calculFraisTJMElementFacture(facture);

                rateVAT = facture.getTVA().getMontant() / 100d;
                // Création de la ligne
                myRow = mySheet.createRow(i);

                // Ajouter des données dans les cellules
                myRow.createCell(0).setCellValue(i - 1);
                myRow.createCell(1).setCellValue(facture.getNumFacture());
                if (facture.getCommande() != null) {
                    myRow.createCell(2).setCellValue(facture.getCommande().getClient().getNom_societe());
                } else {
                    myRow.createCell(2).setCellValue(facture.getMission().getClient().getNom_societe());
                }

                myRow.createCell(3).setCellValue(Utils.fullNameCollabByFacture(facture));
                if (facture.getMission().getManager() != null) {
                    myRow.createCell(4).setCellValue(Utils.fullNameManagerByFacture(facture));
                } else {
                    myRow.createCell(4).setCellValue(Utils.fullNameManagerByCollabByFacture(facture));
                }

                if (facture.getContact() != 0) {
                    ContactClient contactClient = contactClientService.findById((long) facture.getContact());
                    myRow.createCell(5).setCellValue(contactClient.getNom() + " " + contactClient.getPrenom());
                } else {
                    myRow.createCell(5).setCellValue("");
                }
                myRow.createCell(6).setCellValue(quantite);
                Cell prix = myRow.createCell(7);

                // convert float to Double without rounding error
                Double price = new Double(Float.toString(facture.getPrix()));

                prix.setCellValue(price);
                prix.setCellStyle(decimalFormat);

                Cell cellFrais = myRow.createCell(8);
                cellFrais.setCellValue(fraisTJM);
                cellFrais.setCellStyle(decimalFormat);

                Cell cellMontant = myRow.createCell(9);
                cellMontant.setCellValue(montant);
                cellMontant.setCellStyle(decimalFormat);

                Cell cellTTC = myRow.createCell(10);
                cellTTC.setCellValue(montant + Math.round(montant * rateVAT * 100.00) / 100.00);
                cellTTC.setCellStyle(decimalFormat);

                DateFormat formatter = new SimpleDateFormat(STANDARD_DATE_FORMAT_SLASH);
                Cell cellDate = myRow.createCell(11);
                cellDate.setCellValue(formatter.format(facture.getDateFacture()));
                cellDate.setCellStyle(rightAligned);

                roundSumMontant = roundSumMontant + (montant * 100);
                sumInclTaxes += (double)Math.round(montant * ((1+rateVAT)) * 100d);
                i++;
            }

            sumAmount = roundSumMontant / 100.00;
            sumInclTaxes /= 100.00;
            sumVATAmount += sumInclTaxes - sumAmount ;

            //On ajoute les lignes totaux
            myRow = mySheet.createRow(i);
            myRow.createCell(8).setCellValue(TOTAL_MONTANT_HT);
            Cell cellHT = myRow.createCell(9);
            cellHT.setCellValue(sumAmount);
            cellHT.setCellStyle(decimalFormat);

            i++;
            myRow = mySheet.createRow(i);
            myRow.createCell(8).setCellValue(MONTANT_TVA);
            Cell cellTva = myRow.createCell(9);
            cellTva.setCellValue(sumVATAmount);
            cellTva.setCellStyle(decimalFormat);

            i++;
            myRow = mySheet.createRow(i);
            myRow.createCell(8).setCellValue(MONTANT_TTC);
            Cell cellTTC = myRow.createCell(9);
            cellTTC.setCellValue(sumInclTaxes);
            cellTTC.setCellStyle(decimalFormat);

            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);

            style = wb.createCellStyle();
            style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            style.setAlignment(CellStyle.ALIGN_RIGHT);

            // on récupère l'index de la dernière colonne
            int lastCell = mySheet.getRow(2).getPhysicalNumberOfCells() - 1;
            for (int j = 2; j < mySheet.getLastRowNum() - 2; j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                    mySheet.getRow(j).getCell(lastCell).setCellStyle(style);
                    mySheet.getRow(j).getCell(lastCell - 1).setCellStyle(decimalFormatBis);
                    mySheet.getRow(j).getCell(lastCell - 2).setCellStyle(decimalFormatBis);
                    mySheet.getRow(j).getCell(lastCell - 3).setCellStyle(decimalFormatBis);
                    mySheet.getRow(j).getCell(lastCell - 4).setCellStyle(decimalFormatBis);
                }
            }

            for (int k = 0; k <= 11; k++) {
                mySheet.autoSizeColumn(k, true);
            }
            // Fermeture du classeur et du fichier.
            wb.write(out);
        } catch (FileNotFoundException ex) {
            logger.error(FILE_CREATION_ERROR_MESSAGE + ex.getMessage());
            throw ex;
        }
    }

    @Override
    public byte[] createExcelFromTableEntries(String titreFeuille, List<String> columnTitles, List<TableEntry> entries) throws IOException {

        if (entries.size() < 1) {
            return new byte[]{0};
        }

        if (columnTitles.size() != entries.get(0).getColumnValues().size()) {
            throw new IllegalArgumentException(
                    "Les titres et les données n'ont pas le même nombre de colonnes"
            );
        }

        Workbook wb = new XSSFWorkbook();
        // feuille et paramètres d'impression
        Sheet sheet = wb.createSheet();
        PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setLandscape(false);
        sheet.setFitToPage(true);
        printSetup.setPaperSize(XSSFPrintSetup.A4_PAPERSIZE);

        // creer la premier ligne >>> row(zero)
        Row row = sheet.createRow(0);
        // int numColonnes = columnTitles.size();

        // Création du titre contenu dans la premiere ligne
        sheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) columnTitles.size()));
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        Font font = wb.createFont();
        font.setBold(true);
        font.setFontHeight((short) (16 * 20));
        style.setFont(font);
        Utils.createCellule(row, wb, 0, titreFeuille, null, style);

        // Création de la deuxième ligne avec les titres des colonnes
        row = sheet.createRow(1);
        // Numéro
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        font = wb.createFont();
        font.setBold(true);
        style.setFont(font);

        // titre de colonne pour l'ID
        Utils.createCellule(row, wb, 0, COLUMN_EXCEL_ID, null, style);

        int ti = 1;
        for (String title : columnTitles) {
            Utils.createCellule(row, wb, ti, title, null, style);
            ti++;
        }

        int rindex = 2;
        int cindex;
        for (TableEntry entry : entries) {
            row = sheet.createRow(rindex);

            row.createCell(0).setCellValue(rindex - 1);
            cindex = 1;
            for (String colVal : entry.getColumnValues()) {
                row.createCell(cindex).setCellValue(colVal);
                cindex++;
            }
            rindex++;
        }

        documentService.autoSizeColumns(wb);

        // geler les deux premiers rows
        sheet.createFreezePane(0, 2);

        ByteArrayOutputStream baout = new ByteArrayOutputStream();

        try {
            wb.write(baout);
        } catch (IOException e) {
            logger.error(DocumentServiceImpl.class + ": file naming error => " + e.getMessage());
            throw e;
        }

        return baout.toByteArray();
    }

    @Override
    public String createExcelRecapRANonSoumis(List<Facture> listeFacture, DateTime date, String cheminMonthODF, String numODF) throws EncryptedDocumentException, InvalidFormatException, IOException, ParseException, NamingException {

        String chemin;
        String odf = null;
        String fichier;
        if (numODF == null) {
            fichier = "Recapitulatif_RANonSoumis_" + date.getDayOfMonth() + UNDERSCORE + Utils.moisIntToString(date.getMonthOfYear() - 1) + UNDERSCORE + date.getYear();
            if (cheminMonthODF == null) {
                chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + fichier + EXTENSION_FILE_XLS;
            } else {
                chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + SLASH + cheminMonthODF + SLASH + fichier + EXTENSION_FILE_XLS;
            }
        } else {
            odf = numODF;
            fichier = "Recapitulatif_RANonSoumis_" + date.getDayOfMonth() + UNDERSCORE + Utils.moisIntToString(date.getMonthOfYear() - 1) + UNDERSCORE + date.getYear();
            chemin = Parametrage.getContext(TEMP_FOLDER_FILES) + cheminMonthODF + SLASH + fichier + EXTENSION_FILE_XLS;
        }

        documentService.creerDossiersPourFichier(chemin);

        //on peut maintenant créer le fichier voulu sans problème
        try (
                FileOutputStream out = new FileOutputStream(chemin);
                Workbook wb = new HSSFWorkbook()
        ) {
            Sheet mySheet = DocumentServiceImpl.createSheetFacture(wb);
            Row myRow = mySheet.createRow(0);

            //[ALO] Pour arrondir les cellules au centième près, on déclare un style ensuite il faut l'ajouter aux cellules concernées
            CellStyle decimalFormat = wb.createCellStyle();
            decimalFormat.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));

            CellStyle decimalFormatBis = wb.createCellStyle();
            decimalFormatBis.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            decimalFormatBis.setFillPattern(CellStyle.SOLID_FOREGROUND);
            decimalFormatBis.setDataFormat(HSSFDataFormat.getBuiltinFormat(DECIMAL_FORMAT));
            decimalFormatBis.setAlignment(CellStyle.ALIGN_RIGHT);

            CellStyle rightAligned = wb.createCellStyle();
            rightAligned.setAlignment(CellStyle.ALIGN_RIGHT);

            int i = 2;

            mySheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) 4));
            CellStyle style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            Font font = wb.createFont();
            font.setBold(true);
            font.setFontHeight((short) (13 * 20));
            style.setFont(font);

            if (odf != null) {
                Utils.createCellule(myRow, wb, 0, "RA NON SOUMIS / " + date.getDayOfMonth() + " " + Utils.moisIntToString(date.getMonthOfYear() - 1) + " / " + odf, null, style);
            } else {
                Utils.createCellule(myRow, wb, 0, "RA NON SOUMIS / " + date.getDayOfMonth() + " " + Utils.moisIntToString(date.getMonthOfYear() - 1), null, style);
            }

            // Création de la deuxième ligne avec les titres des colonnes
            myRow = mySheet.createRow(1);
            // Numéro
            style = wb.createCellStyle();
            style.setAlignment(CellStyle.ALIGN_CENTER);
            font = wb.createFont();
            font.setBold(true);
            style.setFont(font);

            /// ID, Numéro de facture, Client, Contact, Collaborator, Nombre de jours, TJM, Montant HT
            String[] columnsNames = {
                    COLUMN_EXCEL_ID,
                    COLUMN_EXCEL_COLLABORATOR,
                    COLUMN_EXCEL_CLIENT,
                    COLUMN_EXCEL_MISSION,
                    COLUMN_EXCEL_ORDER
            };

            for (int j = 0; j < columnsNames.length; j++) {
                Utils.createCellule(myRow, wb, j, columnsNames[j], null, style);
            }

            for (Facture facture : listeFacture) {

                // Création de la ligne
                myRow = mySheet.createRow(i);

                // Ajouter des données dans les cellules
                myRow.createCell(0).setCellValue(i - 1);
                myRow.createCell(1).setCellValue(Utils.fullNameCollabByFacture(facture));
                if (facture.getCommande() != null) {
                    myRow.createCell(2).setCellValue(facture.getCommande().getClient().getNom_societe());
                } else {
                    myRow.createCell(2).setCellValue(facture.getMission().getClient().getNom_societe());
                }
                myRow.createCell(3).setCellValue(facture.getMission().getMission());
                if (facture.getCommande() != null) {
                    myRow.createCell(4).setCellValue(facture.getCommande().getNumero());
                } else {
                    myRow.createCell(4).setCellValue("Suivant proposition commerciale");

                }
                i++;
            }

            CellStyle styleCell = wb.createCellStyle();
            styleCell.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            styleCell.setFillPattern(CellStyle.SOLID_FOREGROUND);

            style = wb.createCellStyle();
            style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);

            // on récupère l'index de la dernière colonne
            int lastCell = mySheet.getRow(2).getPhysicalNumberOfCells() - 1;
            for (int j = 2; j < mySheet.getLastRowNum() + 1; j += 2) {
                for (int h = 0; h < mySheet.getRow(j).getLastCellNum(); h++) {
                    mySheet.getRow(j).getCell(h).setCellStyle(styleCell);
                    mySheet.getRow(j).getCell(lastCell).setCellStyle(style);
                }
            }

            for (int k = 0; k <= 10; k++) {
                mySheet.autoSizeColumn(k, true);
            }

            // Fermeture du classeur et du fichier.
            wb.write(out);
        } catch (FileNotFoundException ex) {
            logger.error(FILE_CREATION_ERROR_MESSAGE + ex.getMessage());
            throw ex;
        }
        return chemin;
    }

    /**
     * Add a row to an excel {@link Sheet}
     *
     * @param rowNumber            the number of the row where the row will be added
     * @param mySheet              the row will be added in this sheet
     * @param cellAlignement       the alignement of the cell
     * @param cellStyle            the style of the cell
     * @param singleRowCellsValues the cells of the row will have values from this varargs
     */
    private void addExcelRow(int rowNumber, Sheet mySheet, HSSFRichTextString cellAlignement, CellStyle cellStyle, Object... singleRowCellsValues) {

        Row currentRow = mySheet.createRow(rowNumber);
        int tempColumnNumber = 0;
        for (Object cellValue : singleRowCellsValues) {
            setExcelRowCell(currentRow, tempColumnNumber++, cellValue, cellAlignement, cellStyle);
        }
    }

    /**
     * Set the value of a cell
     *
     * @param currentRow     the row where the cell is located
     * @param columnNumber   the column of the row where the cell is located
     * @param cellValue      the value of the cell, must be String, Double, Integer, Float, Date, or Boolean
     * @param cellAlignement the alignment of the cell
     * @param cellStyle      the style of the cell
     */
    private static void setExcelRowCell(Row currentRow, int columnNumber, Object cellValue, HSSFRichTextString cellAlignement, CellStyle cellStyle) {
        Cell currentCell = currentRow.createCell(columnNumber);
        if (cellAlignement != null) {
            currentCell.setCellValue(cellAlignement);
        } else {
            if (cellValue instanceof String) {
                currentCell.setCellValue(((String) cellValue));
            } else if (cellValue instanceof Integer) {
                currentCell.setCellValue(((Integer) cellValue));
            } else if (cellValue instanceof Float) {
                currentCell.setCellValue(((Float) cellValue));
            } else if (cellValue instanceof Double) {
                currentCell.setCellValue(((Double) cellValue));
            } else if (cellValue instanceof Date) {
                currentCell.setCellValue(((Date) cellValue));
            } else if (cellValue instanceof Boolean) {
                currentCell.setCellValue(((Boolean) cellValue));
            } else {
                throw new IllegalArgumentException("Valeur de cellule \"" + cellValue + "\" non supporté, veuillez utiliser un type String, Double, Integer, Float, Date, ou Boolean");
            }
        }
        if (cellStyle != null) {
            currentCell.setCellStyle(cellStyle);
        }
    }

    /**
     * Create and return green cell style for "Facture de frais"
     *
     * @param workbook
     * @return CellStyle
     */
    private CellStyle getExpenseStyle(Workbook workbook) {
        CellStyle expenseStyle = workbook.createCellStyle();
        expenseStyle.setAlignment(CellStyle.ALIGN_CENTER);
        expenseStyle.setFillForegroundColor(HSSFColor.GREEN.index);
        expenseStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        expenseStyle.setBorderBottom(CellStyle.BORDER_THIN);
        expenseStyle.setBottomBorderColor(HSSFColor.GREY_25_PERCENT.index);
        expenseStyle.setBorderLeft(CellStyle.BORDER_THIN);
        expenseStyle.setLeftBorderColor(HSSFColor.GREY_25_PERCENT.index);
        expenseStyle.setBorderRight(CellStyle.BORDER_THIN);
        expenseStyle.setRightBorderColor(HSSFColor.GREY_25_PERCENT.index);
        expenseStyle.setBorderTop(CellStyle.BORDER_THIN);
        expenseStyle.setTopBorderColor(HSSFColor.GREY_25_PERCENT.index);

        return expenseStyle;
    }

    /**
     * Find collaborator general status
     *
     * @param invoice current invoice
     * @return String
     */
    private String collaboratorTypeByFacture(Facture invoice) {

        if (invoice.getTypeFacture().getCode().equals(TYPEFACTURE_FACTURE_FRAIS))
            return TYPE_VALUE_EXCEL_COLLABORATOR_EXPENSES;
        else if (invoice.getMission().getCollaborateur().getStatutGeneral().equals(COLLAB_STATUS_SS_TRAITANT))
            return TYPE_VALUE_EXCEL_SOUS_TRAITANT;
        else
            return TYPE_VALUE_EXCEL_COLLABORATOR;

    }
}

