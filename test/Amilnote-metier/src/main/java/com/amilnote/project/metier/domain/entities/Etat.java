/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.EtatAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Etat.
 *
 * @author GCornet
 */
@Entity
@Table(name = "ami_ref_etat")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Etat implements java.io.Serializable {

    //TODO remplacer les noms des constantes d'état : ajouter "_CODE" pour les codes, et créer une constante pour le nom entier de l'état

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";

    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";

    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";

    /**
     * The constant ETAT_BROUILLON_CODE.
     */
    public static final String ETAT_BROUILLON_CODE = "BR";

    public static final String ETAT_BROUILLON = "Brouillon";

    /**
     * The constant ETAT_SOUMIS_CODE.
     */
    public static final String ETAT_SOUMIS_CODE = "SO";

    public static final String ETAT_SOUMIS = "Soumis";

    /**
     * The constant ETAT_VALIDE_CODE.
     */
    public static final String ETAT_VALIDE_CODE = "VA";

    public static final String ETAT_VALIDE = "Valide";
    /**
     * The constant ETAT_REFUSE.
     */
    public static final String ETAT_REFUSE = "RE";

    /**
     * The constant ETAT_ANNULE.
     */
    public static final String ETAT_ANNULE = "AN";

    /**
     * The constant ETAT_PAYE.
     */
    public static final String ETAT_PAYE = "PA";

    /**
     * The constant ETAT_SOLDE.
     */
    public static final String ETAT_SOLDE = "SD";

    /**
     * The constant ETAT_TERMINE.
     */
    public static final String ETAT_TERMINE = "TE";

    /**
     * The constant ETAT_COMMENCE.
     */
    public static final String ETAT_COMMENCE = "CO";

    /**
     * The constant ETAT_EDITE.
     */
    public static final String ETAT_EDITE = "ED";

    /**
     * The constant ETAT_GENERE.
     */
    public static final String ETAT_GENERE = "GN";

    public static final String ETAT_ARCHIVEE = "AR";

    /**
     * The constant ETAT_LISTE_FRAIS.
     */
    public static final String[] ETAT_LISTE_FRAIS = {Etat.ETAT_BROUILLON_CODE, Etat.ETAT_SOUMIS_CODE, Etat.ETAT_VALIDE_CODE, Etat.ETAT_PAYE};

    /**
     * The constant ETAT_LISTE_RA.
     */
    public static final String[] ETAT_LISTE_RA = {Etat.ETAT_BROUILLON_CODE, Etat.ETAT_SOUMIS_CODE, Etat.ETAT_VALIDE_CODE, Etat.ETAT_REFUSE};

    /**
     * The constant ETAT_LISTE_DEMANDE_DEPLACEMENT.
     */
    public static final String[] ETAT_LISTE_DEMANDE_DEPLACEMENT = {Etat.ETAT_BROUILLON_CODE, Etat.ETAT_SOUMIS_CODE, Etat.ETAT_VALIDE_CODE, Etat.ETAT_REFUSE, Etat.ETAT_ANNULE};

    /**
     * The constant ETAT_LISTE_ABENCE.
     */
    public static final String[] ETAT_LISTE_ABENCE = {Etat.ETAT_BROUILLON_CODE, Etat.ETAT_SOUMIS_CODE, Etat.ETAT_VALIDE_CODE, Etat.ETAT_REFUSE, Etat.ETAT_ANNULE};

    private static final long serialVersionUID = 4412240278282002064L;

    private int id;

    private String etat;

    private String code;

    /**
     * Instantiates a new Etat.
     *
     * @param pEtat the p etat
     * @param pCode the p code
     */
    public Etat(String pEtat, String pCode) {
        super();
        this.etat = pEtat;
        this.code = pCode;
    }

    /**
     * Instantiates a new Etat.
     */
    public Etat() {
    }


    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(int pId) {
        this.id = pId;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    @Column(name = "etat", nullable = false)
    public String getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the p etat
     */
    public void setEtat(String pEtat) {
        this.etat = pEtat;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    @Column(name = "code", nullable = true)
    public String getCode() {
        return code;
    }


    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * To j son frais as json.
     *
     * @return the frais as json
     */
    public EtatAsJson toJSon() {
        return new EtatAsJson(this);
    }
}
