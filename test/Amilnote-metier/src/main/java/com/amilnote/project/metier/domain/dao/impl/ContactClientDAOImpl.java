/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.ContactClientDAO;
import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.Mission;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * The type Contact client dao.
 */
@Repository("contactClientDAO")
public class ContactClientDAOImpl extends AbstractDAOImpl<ContactClient> implements ContactClientDAO {

    @Autowired
    private MissionDAOImpl missionDAO;

    @Override
    protected Class<ContactClient> getReferenceClass() {
        return ContactClient.class;
    }

    /**
     * {@linkplain ContactClientDAO#findAllOrderByNomAsc()}
     */
    @Override
    public List<ContactClient> findAllOrderByNomAsc() {
        Criteria lCritClient = getCriteria();
        lCritClient.addOrder(Order.asc(Client.PROP_NOM));
        return lCritClient.list();
    }

    /**
     * {@linkplain ContactClientDAO#deleteContactClient(ContactClient)}
     */
    @Override
    public String deleteContactClient(ContactClient pContact) {
        Date today = Calendar.getInstance().getTime();
        if (null != pContact) {
            List<Mission> listMissionsContactClient = missionDAO.findListEntitesByProp(Mission.PROP_RESP_CLIENT, pContact);
            List<Mission> missionsToUpdate = new ArrayList<>();
            boolean missionEnCours = false;
            StringBuilder listCollab = new StringBuilder();
            for (Mission mission : listMissionsContactClient) {
                if (mission.getDateFin().after(today)) {
                    missionEnCours = true;
                    listCollab.append("<p>" + mission.getCollaborateur().getMail() + ": " + mission.getMission() + "</p>");
                } else {
                    mission.setResp_client(null);
                    missionsToUpdate.add(mission);
                }
            }
            if (missionEnCours) {
                return "<p>Erreur: Ce contact est attribué en tant que responsable client sur une ou plusieurs mission en cours: " + listCollab
                    + "<p>Veuillez vérifier qu'il ne soit plus assigné à  aucune mission en cours avant de le supprimer.</p>";
            }
            // --- Création de la session et de la transaction
            Session session = getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            // --- Sauvegarde et Commit de la mise à jour
            for (Mission mission : missionsToUpdate) {
                session.save(mission);
            }
            session.delete(pContact);
            session.flush();
            transaction.commit();
            return "Suppression effectuée avec succès";
        }
        return "Aucune suppression n'a été effectuée";
    }
}
