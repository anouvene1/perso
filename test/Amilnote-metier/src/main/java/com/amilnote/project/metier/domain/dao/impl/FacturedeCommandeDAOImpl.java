/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.FacturedeCommandeDAO;
import com.amilnote.project.metier.domain.entities.ami_factureDeCommande;
import org.hibernate.Query;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The type Facturede commande dao.
 */
@Repository("FacturedeCommandeDAO")
public class FacturedeCommandeDAOImpl extends AbstractDAOImpl<ami_factureDeCommande> implements FacturedeCommandeDAO {

    /**
     * {@linkplain FacturedeCommandeDAO#findAllBydate(Date)}
     */

    @Override
    public List<ami_factureDeCommande> findAllBydate(Date date) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        int month = cal.get(Calendar.MONTH) +1;
        int year = cal.get(Calendar.YEAR);

        DateTime dateVoulue = new DateTime(date);
        DateTime lDebutPeriode = dateVoulue.dayOfMonth().withMinimumValue();
        DateTime lFinPeriode = dateVoulue.dayOfMonth().withMaximumValue().plusDays(1);

        List<ami_factureDeCommande> results;

        Query requete=currentSession().createSQLQuery("SELECT DISTINCT\n" +
                "        `fac`.`id` AS `id_facture`,\n" +
                "        `fac`.`num_facture` AS `num_facture`,\n" +
                "        `cli`.`nom_societe` AS `nom_societe`,\n" +
                "        CONCAT(`collab`.`nom`, ' ', `collab`.`prenom`) AS `collaborateur`,\n" +
                "        `m`.`mission` AS `mission`,\n" +
                "        `com`.`numero` AS `numero`,\n" +
                "        `com`.`commentaire` AS `libelle`,\n" +
                "        `fac`.`quantite` AS `quantite`,\n" +
                "        `fac`.`prix` AS `prix`,\n" +
                "        `fac`.`montant` AS `montant`,\n" +
                "        `fac`.`mois_prestations` AS `mois_prestations`,\n" +
                "        `fac`.`etat` AS `etat`,\n" +
                "        `ra`.`mois_rapport` AS `mois_rapport`,\n" +
                "        `ra`.`id_etat` AS `ra_etat`, \n" +
                "        `ct`.`id` AS `contact`" +
                "    FROM\n" +
                "        ((((((`ami_mission` `m`\n" +
                "        JOIN `ami_collaborateur` `collab` ON ((`m`.`id_collaborateur` = `collab`.`id`)))\n" +
                "        JOIN `ami_rapport_activite` `ra` ON ((`ra`.`id_collaborateur` = `collab`.`id`)))\n" +
                "        JOIN `ami_client` `cli` ON ((`m`.`id_client` = `cli`.`id`)))\n" +
                "        LEFT JOIN `ami_contact_client` `ct` ON ((`ct`.`id_client` = `cli`.`id`)))\n" +
                "        LEFT JOIN `ami_commande` `com` ON ((`com`.`id_mission` = `m`.`id`)))\n" +
                "        LEFT JOIN `ami_facture` `fac` ON ((`fac`.`id_commande` = `com`.`id`)))\n" +
                "    WHERE\n" +
                "        ((`m`.`id_type_mission` = 5)\n" +
                "            AND (`com`.`etat` = 12)\n" +
                "            AND ((`ra`.`id_etat` = 1) OR (`ra`.`id_etat` = 2))\n" +
                "            AND ((`fac`.`etat` = 8) OR (`fac`.`etat` = 9) OR (`fac`.`etat` = 2))\n" +
                "            AND (MONTH(`ra`.`mois_rapport`) = :month)\n" +
                "            AND (YEAR(`ra`.`mois_rapport`) = :year))\n" +
                "            AND (m.date_fin >= :firstDay and m.date_debut < :lastDay)\n"+
                "            AND `ct`.`id` is not null\n" +
                "            AND (com.date_fin >= :firstDay and com.date_debut < :lastDay)\n" +
                "            AND (`fac`.`quantite` <> 0)\n" +
                "    GROUP BY `com`.`id` , `ra`.`mois_rapport`").addEntity(ami_factureDeCommande.class);

            requete.setInteger("month",month);
            requete.setInteger("year",year);
            requete.setDate("firstDay",lDebutPeriode.toDate());
            requete.setDate("lastDay",lFinPeriode.toDate());

        results = requete.list();

        return results;
    }

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<ami_factureDeCommande> getReferenceClass() {
        return ami_factureDeCommande.class;
    }
}
