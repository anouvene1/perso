/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;

/**
 * The type Operator.
 */
public class Operator {
    private static final int AND = 0;
    private static final int OR = 1;
    private static final int IN = 2;
    private static final int LIKE = 3;
    private static final int EQ = 4;
    private static final int SUP = 5;
    private static final int INF = 6;
    private static final int GE = 7;
    private static final int LE = 8;
    private static final int NOT = 9;
    private static final int NULL = 10;

    private Integer operator;
    private Boolean inverse;

    private Operator(Integer operator, Boolean inverse) {
        this.operator = operator;
        this.inverse = inverse;
    }

    /**
     * And operator.
     *
     * @return the operator
     */
    public static Operator and() {
        return new Operator(Operator.AND, false);
    }

    /**
     * Or operator.
     *
     * @return the operator
     */
    public static Operator or() {
        return new Operator(Operator.OR, false);
    }

    /**
     * Not operator.
     *
     * @return the operator
     */
    public static Operator not() {
        return new Operator(Operator.NOT, false);
    }

    /**
     * In operator.
     *
     * @return the operator
     */
    public static Operator in() {
        return new Operator(Operator.IN, false);
    }

    /**
     * Like operator.
     *
     * @return the operator
     */
    public static Operator like() {
        return new Operator(Operator.LIKE, false);
    }

    /**
     * Nlike operator.
     *
     * @return the operator
     */
    public static Operator nlike() {
        return new Operator(Operator.LIKE, true);
    }

    /**
     * Eq operator.
     *
     * @return the operator
     */
    public static Operator eq() {
        return new Operator(Operator.EQ, false);
    }

    /**
     * Neq operator.
     *
     * @return the operator
     */
    public static Operator neq() {
        return new Operator(Operator.EQ, true);
    }

    /**
     * Sup operator.
     *
     * @return the operator
     */
    public static Operator sup() {
        return new Operator(Operator.SUP, false);
    }

    /**
     * Inf operator.
     *
     * @return the operator
     */
    public static Operator inf() {
        return new Operator(Operator.INF, false);
    }

    /**
     * Ge operator.
     *
     * @return the operator
     */
    public static Operator ge() {
        return new Operator(Operator.GE, false);
    }

    /**
     * Le operator.
     *
     * @return the operator
     */
    public static Operator le() {
        return new Operator(Operator.LE, false);
    }

    /**
     * Is null operator.
     *
     * @return the operator
     */
    public static Operator isNull() {
        return new Operator(Operator.NULL, false);
    }

    /**
     * Not operator.
     *
     * @param operateur the operateur
     * @return the operator
     */
    public static Operator not(Operator operateur) {
        switch (operateur.operator) {
            case 2:
            case 3:
            case 4:
            case 10:
                operateur.inverse = true;
            default:
                return operateur;
        }
    }

    /**
     * Is or boolean.
     *
     * @return the boolean
     */
    public boolean isOr() {
        if (operator.equals(Operator.OR)) {
            return true;
        }
        return false;
    }

    /**
     * Gets operator.
     *
     * @return the operator
     */
    public String getOperator() {
        String lien = "";

        switch (operator) {
            case 0:
                lien = "AND";
                break;
            case 1:
                lien = "OR";
                break;
            case 2:
                if (inverse)
                    lien = "NOT";

                lien = lien + " IN";
                break;
            case 3:
                if (inverse)
                    lien = "NOT";
                lien = lien + " LIKE";
                break;
            case 4:
                if (inverse) {
                    lien = "<>";
                }
                else {
                    lien = "=";
                }
                break;
            case 5:
                lien = ">";
                break;
            case 6:
                lien = "<";
                break;
            case 7:
                lien = ">=";
                break;
            case 8:
                lien = "<=";
                break;
            case 9:
                lien = "NOT";
                break;
            case 10:
                if (inverse) {
                    lien = "IS NOT NULL";
                }
                else {
                    lien = "IS NULL";
                }
                break;
            default:
                break;
        }

        return lien;
    }
}
