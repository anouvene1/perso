package com.amilnote.project.metier.domain.dao;
import com.amilnote.project.metier.domain.entities.*;

import java.util.Date;
import java.util.List;


public interface CalculFraisDAO extends LongKeyDAO<Frais>{

    List<Object[]> getDataFrais(Date dateDebut, Date dateFin, String typeCalcul);

    List<Object[]> getDataForfait(Date dateDebut, Date dateFin, String typeForfait);

    List<Object[]> getAbsenceCollab (Date dateDebut, Date dateFin);

}
