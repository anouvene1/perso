/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.FactureArchiveeAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Facture archivee.
 */
@Entity
@Table(name = "ami_facture_archivee")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class FactureArchivee implements java.io.Serializable {

    /**
     * The constant PROP_ID_FACTURE_ARCHIVEE.
     */
    public static final String PROP_ID_FACTURE_ARCHIVEE = "id";
    /**
     * The constant PROP_DATE_ARCHIVAGE.
     */
    public static final String PROP_DATE_ARCHIVAGE = "date_archivage";
    /**
     * The constant PROP_CLIENT_FACTURE_ARCHIVEE.
     */
    public static final String PROP_CLIENT_FACTURE_ARCHIVEE = "client";
    /**
     * The constant PROP_COLLABORATEUR_FACTURE_ARCHIVEE.
     */
    public static final String PROP_COLLABORATEUR_FACTURE_ARCHIVEE = "collaborateur";
    /**
     * The constant PROP_MISSION_FACTURE_ARCHIVEE.
     */
    public static final String PROP_MISSION_FACTURE_ARCHIVEE = "mission";
    /**
     * The constant PROP_CONTACT_FACTURE_ARCHIVEE.
     */
    public static final String PROP_CONTACT_FACTURE_ARCHIVEE = "contact";
    /**
     * The constant PROP_NUMERO_COMMANDE_FACTURE_ARCHIVEE.
     */
    public static final String PROP_NUMERO_COMMANDE_FACTURE_ARCHIVEE = "numCommande";
    /**
     * The constant PROP_QUANTITE_FACTURE_ARCHIVEE.
     */
    public static final String PROP_QUANTITE_FACTURE_ARCHIVEE = "quantite";
    /**
     * The constant PROP_PRIX_FACTURE_ARCHIVEE.
     */
    public static final String PROP_PRIX_FACTURE_ARCHIVEE = "prix";
    /**
     * The constant PROP_MONTANT_FACTURE_ARCHIVEE.
     */
    public static final String PROP_MONTANT_FACTURE_ARCHIVEE = "montant";
    /**
     * The constant PROP_CHEMIN_PDF_FACTURE_ARCHIVEE.
     */
    public static final String PROP_CHEMIN_PDF_FACTURE_ARCHIVEE = "cheminPDF";
    /**
     * The constant PROP_NUMERO_FACTURE_ARCHIVEE.
     */
    public static final String PROP_NUMERO_FACTURE_ARCHIVEE = "numero";

    /**
     * The constant PROP_TYPE_FACTURE.
     */
    public static final String PROP_TYPE_FACTURE = "typeFacture";

    /**
     * The constant PROP_IS_AVOIR.
     */
    public static final String PROP_IS_AVOIR = "isAvoir";



    private static final long serialVersionUID = -329072377657745936L;
    private int id;
    private String client;
    private String collaborateur;
    private String mission;
    private String contact;
    private String numCommande;
    private String cheminPDf;
    private float quantite;
    private float prix;
    private float montant;
    private int numero;
    private Date date_archivage;
    private Integer typeFacture;
    private Boolean isAvoir;

	/* CONSTRUCTORS */

    /**
     * Instantiates a new Facture archivee.
     */
    public FactureArchivee() {
        super();
    }

    /**
     * Instantiates a new Facture archivee.
     *
     * @param pClient           the p client
     * @param pCollaborateur    the p collaborateur
     * @param pMission          the p mission
     * @param pContact          the p contact
     * @param pNumCommande      the p num commande
     * @param pCheminPDF        the p chemin pdf
     * @param pQuantite         the p quantite
     * @param pPrix             the p prix
     * @param pMontant          the p montant
     * @param pNumero           the p numero
     * @param pDate_archivage   the p date d'archivage
     * @param pTypeFacture      the p type facture
     * @param pIsAvoir           the p avoir
     */

    public FactureArchivee(String pClient, String pCollaborateur, String pMission, String pContact, String pNumCommande,
                           String pCheminPDF, float pQuantite, float pPrix, float pMontant, int pNumero,
                           Date pDate_archivage, Integer pTypeFacture, Boolean pIsAvoir) {
        super();
        client = pClient;
        collaborateur = pCollaborateur;
        mission = pMission;
        contact = pContact;
        numCommande = pNumCommande;
        cheminPDf = pCheminPDF;
        quantite = pQuantite;
        prix = pPrix;
        montant = pMontant;
        numero = pNumero;
        date_archivage = pDate_archivage;
        typeFacture = pTypeFacture;
        isAvoir = pIsAvoir;
    }

    /**
     * To json facture archivee as json.
     *
     * @return the facture archivee as json
     */
    public FactureArchiveeAsJson toJson() {
        return new FactureArchiveeAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idFactureArchivee", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
//@OneToOne(mappedBy = "id_commande")
    @Column(name = "client_facture", nullable = false)
    public String getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * Gets collaborateur.
     *
     * @return the collaborateur
     */
    @Column(name = "collaborateur_facture")
    public String getCollaborateur() {
        return collaborateur;
    }

    /**
     * Sets collaborateur.
     *
     * @param collab the collab
     */
    public void setCollaborateur(String collab) {
        this.collaborateur = collab;
    }

    /**
     * Gets mission.
     *
     * @return the mission
     */
    @Column(name = "mission_facture")
    public String getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the mission
     */
    public void setMission(String mission) {
        this.mission = mission;
    }

    /**
     * Gets num commande.
     *
     * @return the num commande
     */
    @Column(name = "numero_commande_facture")
    public String getNumCommande() {
        return numCommande;
    }

    /**
     * Sets num commande.
     *
     * @param commande the commande
     */
    public void setNumCommande(String commande) {
        this.numCommande = commande;
    }

    /**
     * Gets contact.
     *
     * @return the contact
     */
    @Column(name = "contact_facture")
    public String getContact() {
        return contact;
    }

    /**
     * Sets contact.
     *
     * @param contact the contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Gets chemin pdf.
     *
     * @return the chemin pdf
     */
    @Column(name = "chemin_pdf")
    public String getCheminPDF() {
        return cheminPDf;
    }

    /**
     * Sets chemin pdf.
     *
     * @param chemin the chemin
     */
    public void setCheminPDF(String chemin) {
        this.cheminPDf = chemin;
    }

    /**
     * Gets quantite.
     *
     * @return the quantite
     */
    @Column(name = "quantite_facture")
    public float getQuantite() {
        return quantite;
    }

    /**
     * Sets quantite.
     *
     * @param q the q
     */
    public void setQuantite(float q) {
        this.quantite = q;
    }

    /**
     * Gets prix.
     *
     * @return the prix
     */
    @Column(name = "prix_facture")
    public float getPrix() {
        return prix;
    }

    /**
     * Sets prix.
     *
     * @param p the p
     */
    public void setPrix(float p) {
        this.prix = p;
    }

    /**
     * Gets montant.
     *
     * @return the montant
     */
    @Column(name = "total_facture")
    public float getMontant() {
        return montant;
    }

    /**
     * Sets montant.
     *
     * @param m the m
     */
    public void setMontant(float m) {
        this.montant = m;
    }

    /**
     * Gets numero.
     *
     * @return the numero
     */
    @Column(name = "num_facture")
    public int getNumero() {
        return numero;
    }

    /**
     * Sets numero.
     *
     * @param m the m
     */
    public void setNumero(int m) {
        this.numero = m;
    }

    /**
     * Gets date d'archivage.
     *
     * @return the date_archivage
     */
    @Column(name = "date_archivage")
    public Date getDate_archivage() {
        return date_archivage;
    }

    /**
     * Sets date d'archivage.
     *
     * @param date_archivage the date_archivage
     */
    public void setDate_archivage(Date date_archivage) {
        this.date_archivage = date_archivage;
    }

    /**
     * Gets the type facture
     *
     * @return the type facture
     */
    @Column(name = "type_facture")
    public Integer getTypeFacture() {
        return typeFacture;
    }

    /**
     * Sets the type facture
     *
     * @param typeFacture the type facture
     */
    public void setTypeFacture(Integer typeFacture) {
        this.typeFacture = typeFacture;
    }


    /**
     * Gets the avoir
     *
     * @return the avoir
     */
    @Column(name = "is_avoir")
    public Boolean getAvoir() {
        return isAvoir;
    }

    /**
     * Sets the avoir
     *
     * @param avoir the avoir
     */
    public void setAvoir(Boolean avoir) {
        isAvoir = avoir;
    }

}
