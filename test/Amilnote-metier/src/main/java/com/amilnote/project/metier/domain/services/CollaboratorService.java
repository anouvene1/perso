/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.ModuleAddvise;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;

import java.util.Date;
import java.util.List;

/**
 * The Collaborator service interface.
 */
public interface CollaboratorService extends AbstractTravailleurService<Collaborator, CollaboratorAsJson> {

    /**
     * Find all order by nom asc as json without admin list.
     *
     * @return the list
     */
    List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMIN();

    /**
     * Find all order by nom asc as json without admin list but with disabled collaborators.
     *
     * @return the list
     */
    List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMINWithDisabled();

    /**
     * Find all order by nom asc without admin list.
     *
     * @return the list
     */
    List<Collaborator> findAllOrderByNomAscWithoutADMIN();


    /**
     * Find all order by nom asc without admin list but with disabled collaborators.
     *
     * @return the list
     */
    List<Collaborator> findAllOrderByNomAscWithoutADMINWithDisabled();

    /**
     * {@inheritDoc}
     */
    List<CollaboratorAsJson> findCollaboratorsForAbsenceMonthAsJson(Date pDate);

    /**
     * {@inheritDoc}
     */
    List<CollaboratorAsJson> findCollaboratorsForFraisAnnuelAsJson(int yearFrais);

    /**
     * Retrieves all the Collaborateur who has the same Responsable
     *
     * @param responsable the responsable
     * @return a list of Collaborateur
     */
    List<Collaborator> findCollaboratorsByResponsable(Collaborator responsable);

    /**
     * Retrieves all the Collaborateur with the same Responsable in JSON
     *
     * @param responsable the responsable
     * @return a list of Collaborateur in JSON
     */
    List<CollaboratorAsJson> findCollaboratorsByResponsableAsJSON(Collaborator responsable);

    List<ModuleAddvise> getAllModuleAddvise();

    boolean isCadre(Collaborator collaborator);

    /**
     * Récupère tous les collaborateurs ayant le statut manager ou le poste manager
     *
     * @return liste de managers au format json
     */
    List<CollaboratorAsJson> findAllManagersAsJson();

    /**
     * Récupère tous les collaborateurs ayant le statut responsable technique
     *
     * @return liste de responsables technique au format json
     */
    List<CollaboratorAsJson> findAllResponsablesTechniqueAsJson();

    List<CollaboratorAsJson> findAllDirectionAsJson();

    /**
     * Vérifie si le collaborateur est un ingénieur d'affaires
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isManager(Collaborator currentUser);

    /**
     * Vérifie si le collaborateur est un responsable technique
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isResponsableTechnique(Collaborator currentUser);

    /**
     * verifie que le collaborateur est un chef de projet
     *
     * @param currentUser current {@link Collaborator}
     * @return true if currentUser is chief
     */
    boolean isChefDeProjet(Collaborator currentUser);

    /**
     * Retourne la liste des collaborateurs sans les Admins et Directeurs
     *
     * @return a list of {@link CollaboratorAsJson}
     */
    List<CollaboratorAsJson> findAllOrderByNomAscAsJsonWithoutADMINAndDir();

    /**
     * Vérifie sir le collaborateur a le statut Direction
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isDirecteur(Collaborator currentUser);

    /**
     * Vérifie si le collaborateur a le statut DRH
     *
     * @param currentUser le collaborateur actuel
     * @return true or false
     */
    boolean isDRH(Collaborator currentUser);

    /**
     * Verifie en base l'existance d un email
     *
     * @param nomEmail email
     * @return true if email exist
     */
    boolean checkEmailExist(String nomEmail);

    /**
     * Verifie que l'id de la mission passée correspond bien à une mission
     * qui appartient au collaborateur passé
     *
     * @param currentUser current {@link Collaborator}
     * @param missionId   mision id
     * @return true or false
     */
    boolean hasMissionById(Collaborator currentUser, Long missionId);

    /**
     * réccupere une liste des chefs de projets
     *
     * @return une liste des chefs de projets
     */
    List<CollaboratorAsJson> findAllChefDeProjetAsJson();

    @Override
    String updateOrCreateCollaborator(CollaboratorAsJson pCollaboratorAsJson) throws Exception;

    /**
     * Retourne la liste des managerz
     *
     * @return liste des managers
     */
    List<CollaboratorAsJson> addListManagers();

    /**
     * Method to retrieve all collaboratos by their {@link StatutCollaborateur} status
     *
     * @param collaboratorsStatus the search will be based on this list of {@link StatutCollaborateur}
     * @return a list of {@link Collaborator} which status corresponds to the given list of status
     */
    List<Collaborator> findCollaboratorsByStatus(StatutCollaborateur... collaboratorsStatus);

    /**
     * Returns a list of collaborators by their status code
     *
     * @param collaboratorStatusCode an Array of {@link String}
     * @return list of {@link Collaborator}
     */
    List<Collaborator> findAllByStatusCode(String... collaboratorStatusCode);

    /**
     * Method to check if a collaborator is a Subcontractor or not
     *
     * @param currentUser the current collaborator user
     * @return a {@link Boolean} if the collaborator is a subcontractor or not
     */
    boolean isSubcontractor(Collaborator currentUser);

    /**
     * Reactivate a collaborateur or subcontractor.
     *
     * @param idCollaborateur Collaborator or subcontractor identifier
     * @return Callback message when collaborator or subcontractor are reactivated
     */
    boolean changeCollaboratorActivation(Long idCollaborateur) throws Exception;

    /**
     * Retreive the list of collaborators by their status and activated or not
     * @param enabled Collaborator status activated or not
     * @param status Collaborator status
     * @return The Collaborators list
     */
    List<Collaborator> retrieveCollaboratorByStatus(boolean enabled, StatutCollaborateur... status);
}
