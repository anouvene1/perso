/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.task;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.services.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Module permettant de gérer les tâches périodiques
 *
 * @author kesprit
 */
@Component("scheduledTasks")
@Transactional
public class ScheduledTasks {

    private MailService mailService;

    private CollaboratorService collaboratorService;

    private CommandeService commandeService;

    private FactureService factureService;

    private MissionService missionService;

    private ParametreService parametreService;

    private EtatService etatService;

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @Autowired
    public ScheduledTasks(MailService mailService, CollaboratorService collaboratorService,
                          CommandeService commandeService, FactureService factureService,
                          MissionService missionService, ParametreService parametreService,
                          EtatService etatService) {
        this.mailService = mailService;
        this.collaboratorService = collaboratorService;
        this.commandeService = commandeService;
        this.factureService = factureService;
        this.missionService = missionService;
        this.parametreService = parametreService;
        this.etatService = etatService;
    }

    /**
     * Check for mail rappel ra.
     *
     * @throws NamingException the naming exception
     */
    @Transactional
    public void checkForMailRappelRA() throws NamingException {
        mailService.envoiMailRappelRA();
    }

    /**
     * Tache automatique (3min) permettant de délock les comptes lock suite à un trop grand nombre d'erreur de mdp.
     */
    @Transactional
    public void resetLocked() {
        List<Collaborator> collaborateurs = collaboratorService.findAll(null);
        for (Collaborator collab : collaborateurs) {
            if (!collab.getAccountNonLocked()) {
                collab.setAccountNonLocked(true);
                collab.setNbAttempts(0);
                collaboratorService.saveOrUpdate(collab);
            }
        }
    }

    // [ALO] [05/06/2017]
    /**
     * Tache automatique (tout les jours) permettant d'envoyer un mail si c'est l'anniversaire d'un collaborateur
     *
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    @Transactional
    public void checkDateBirthdayCollab() throws IOException, InvalidFormatException, ParseException, NamingException {

        SimpleDateFormat formatJourMois = new SimpleDateFormat("MM-dd");

        List<Collaborator> collaborateurs = collaboratorService.findAllOrderByNomAsc();

        //date aujourd'hui
        Calendar today = Calendar.getInstance();
        String todayMoisAnnee = formatJourMois.format(today.getTime());

        for (Collaborator collaborateur : collaborateurs) {

            if(collaborateur.getStatut().getId() != 7) {
                //SBE_AMNOTE-195 Date de naissance du collab
                Date dateNaissance = collaborateur.getDateNaissance();
                String dateNaissMoisAnnee = formatJourMois.format(dateNaissance);

                //SBE_AMNOTE-195 Verification si c'est l'anniversaire du collab
                if (todayMoisAnnee.equals(dateNaissMoisAnnee)) {
                    mailService.envoiMailRappelDate(collaborateur, 0);
                }
            }
        }
    }

    // [ALO] [05/06/2017]
    /**
     * Tache automatique (tous les jours) permettant d'envoyer un mail si c'est bientôt la date d'entretien annuel d'un collaobrateur.
     *
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    @Transactional
    public void checkDateEntretienAnnuel() throws IOException, InvalidFormatException, ParseException, NamingException {

        SimpleDateFormat formatJourMoisAnnee = new SimpleDateFormat(DATE_FORMAT);

        List<Collaborator> collaborateurs = collaboratorService.findAllOrderByNomAsc();

        for (Collaborator collaborateur : collaborateurs) {
            if(collaborateur.getStatut().getId() != 7) {
                Date dateEmbauche = collaborateur.getDateEntree();
                String dateEmbaucheMoisAnnee = formatJourMoisAnnee.format(dateEmbauche);

                Calendar dayYearLaterTemp = Calendar.getInstance();
                dayYearLaterTemp.add(Calendar.YEAR, -1);
                dayYearLaterTemp.add(Calendar.DATE, 14);
                String dayYearLaterTempJourMoisAnnee = formatJourMoisAnnee.format(dayYearLaterTemp.getTime());

                if (dayYearLaterTempJourMoisAnnee.equals(dateEmbaucheMoisAnnee)) {
                    mailService.envoiMailRappelDate(collaborateur, 1);
                }
            }
        }
    }

    // [ALO] [06/06/2017]
    /**
     * Tache automatique (tout les jours) permettant d'envoyer un mail si c'est la fin de période d'essai d'un collaborateur
     *
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    @Transactional
    public void checkDateFinPeriodeEssai() throws IOException, InvalidFormatException, ParseException, NamingException {

        SimpleDateFormat formatDate = new SimpleDateFormat(DATE_FORMAT);

        List<Collaborator> collaborateurs = collaboratorService.findAllOrderByNomAsc();

        for (Collaborator collaborateur : collaborateurs) {
            if(collaborateur.getStatut().getId() != 7) {
                checkDateFinPeriodeEssaiForOneUser(formatDate, collaborateur);
            }

        }
    }

    /**
     * Vérifie la fin de la période d'essai pour un utilisateur
     * @param formatDate le formatDate
     * @param collaborateur le collaborateur
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    private void checkDateFinPeriodeEssaiForOneUser(SimpleDateFormat formatDate, Collaborator collaborateur) throws IOException, InvalidFormatException, ParseException, NamingException {
        // Récupération de la date d'embauche du collaborateur et conversion en chaine de caractères
        Date dateEntreeCollaborator = collaborateur.getDateEntree();
        String dateEmbauche = formatDate.format(dateEntreeCollaborator);

        // Récupération de la date du jour et récupération du jour en question
        Calendar finPeriodeEssai = Calendar.getInstance();
        int dayOfWeek = finPeriodeEssai.get(Calendar.DAY_OF_WEEK);

        // On vérifie si c'est un jour ouvré
        switch (dayOfWeek) {
            case Calendar.MONDAY :
            case Calendar.TUESDAY :
            case Calendar.WEDNESDAY :
            case Calendar.THURSDAY :
                sendRappel(dateEmbauche, collaborateur, finPeriodeEssai);
                break;
            case Calendar.FRIDAY :
                if (!sendRappel(dateEmbauche, collaborateur, finPeriodeEssai)) {
                    sendRappelForWeekEnd(collaborateur, dateEmbauche);
                }
                break;
            default :
                break;
        }
    }

    /**
     * Envoie le mail de rappel de fin de période d'essai le vendredi pour les jours non-ouvrés
     * @param collaborateur le collaborateur
     * @param dateEmbauche la date d'embauche
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    private void sendRappelForWeekEnd(Collaborator collaborateur, String dateEmbauche) throws IOException, InvalidFormatException, ParseException, NamingException {
        // Si on est vendredi on vérifie le lendemain
        Calendar finPeriodeEssaiLendemain = Calendar.getInstance();
        finPeriodeEssaiLendemain.add(Calendar.DATE, 1);

        if (!sendRappel(dateEmbauche, collaborateur, finPeriodeEssaiLendemain)) {
            // Si on est en vendredi on vérifie également le surlendemain
            Calendar finPeriodeEssaiSurlendemain = Calendar.getInstance();
            finPeriodeEssaiSurlendemain.add(Calendar.DATE, 2);
            sendRappel(dateEmbauche, collaborateur, finPeriodeEssaiSurlendemain);
        }
    }

    /**
     * SHA AMNOTE-198 10/01/2017
     * Tache automatique (tous les 10 et 17 du mois) permettant de verifier les factures sans bon de commande et prévenir les commerciaux.
     */
    @Transactional
    public void factureSansBonDeCmd() {
        List<Commande> lCommandes = commandeService.findAll(null);
        for (Commande cmd : lCommandes) {
            if ("0".equals(cmd.getNumero())) {
                //envoi du mail pour chaques commande d'on le numero = 0 (0 = pas bon de commande)
                mailService.envoiMailFactureSansBonDeCmd(cmd.toJson());
            }
        }
    }

    /**
     * Vérifie 7 et/ou 14 jours avant si c'est la fin de la période d'essai
     *
     * @param dateEmbauche la date d'embauche
     * @param collaborateur le collaborateur
     * @param calendar le calendar
     * @return true si un mail de rappel est envoyé sinon false
     * @throws IOException            the io exception
     * @throws InvalidFormatException the invalid format exception
     * @throws ParseException         the parse exception
     * @throws NamingException        the naming exception
     */
    private boolean sendRappel(String dateEmbauche, Collaborator collaborateur, Calendar calendar) throws IOException, InvalidFormatException, ParseException, NamingException {

        SimpleDateFormat formatDate = new SimpleDateFormat(DATE_FORMAT);
        int typeMail = 2;
        int daysBefore = 7;

        // on ajoute 7 jours à la date d'aujourd'hui
        calendar.add(Calendar.DATE, daysBefore);

        //Si c'est un cadre, la periode d'essai est de 4 mois, sinon c'est 2 mois
        // on enlève 4 ou 2 mois à la date
        if (collaboratorService.isCadre(collaborateur)) {
            calendar.add(Calendar.MONTH, -4);
        } else {
            calendar.add(Calendar.MONTH, -2);
        }

        // On convertit la date en chaîne de caractères
        String finPeriodeEssai = formatDate.format(calendar.getTime());

        // Première vérification 7 jours avant la date de fin de période d'essai
        if (checkDates(finPeriodeEssai, dateEmbauche)) {
            mailService.envoiMailRappelDate(collaborateur, typeMail);
            return true;
        } else {
            calendar.add(Calendar.DATE, daysBefore);
            finPeriodeEssai = formatDate.format(calendar.getTime());

            // Seconde vérification 14 avant la date de fin de période d'essai
            if (checkDates(finPeriodeEssai, dateEmbauche)) {
                mailService.envoiMailRappelDate(collaborateur, ++typeMail);
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param firstDate
     * @param secondDate
     * @return true si les dates sont égales sinon false
     */
    private boolean checkDates(String firstDate, String secondDate) {
        return firstDate.equals(secondDate);
    }

    /**
     * Tache automatique (tous les 16 du mois à 8h) permettant de générer les factures des commandes en cours si elles ne sont pas finies ou sinon envoi de mails
     *
     * @throws IOException            the io exception
     * @throws NamingException        the naming exception
     */
    @Transactional
    public void genererFactures() throws NamingException, IOException {
        //[CLO AMNOTE-308] Refonte
        DateTime dateVoulue = new DateTime();
        List<Mission> missions = missionService.findMissionsClientesByMonth(dateVoulue);

        for (Mission mission : missions) {
            Facture facture = factureService.createFactureFromMission(mission,null);
            if (facture != null) {
                factureService.saveOrUpdate(facture);
            }
        }
        // [CLO] Fin

        //[JNA][AMNOTE-150] on récupère la liste de toutes les commandes
        List<Commande> listeCommandes = commandeService.findActivesCommandes();
        Date today = new Date();
        Float initMontant = Float.valueOf(0);

        // On récupère le seuil du budget qui est stocker dans l'objet parametre
        List<Parametre> parametre = parametreService.findAll(null);
        // Le budget est le premier élément de la table params
        Float seuilBudget = Float.valueOf(parametre.get(0).getValeur());
        //On initialise un Etat à "généré" pour les nouvelles factures
        Etat etatNouvelleFacture = etatService.getEtatByCode("GN");
        Etat etatCommande = etatService.getEtatByCode("TE");


        //envoi de 3 mails separe contenant toutes les commandes selon le type de mail
        ArrayList<Commande> listeCommandesType1 = new ArrayList<Commande>();
        ArrayList<Commande> listeCommandesType2 = new ArrayList<Commande>();
        ArrayList<Commande> listeCommandesType3 = new ArrayList<Commande>();

        for (Commande commande : listeCommandes) {
            if (commande.getEtat().getCode().equals("CO")) {
                if (commande.getMontant() > initMontant && commande.getDateFin() != null) { // S'il y a un montant à la commande ET une date de fin
                    if (commande.getBudget() < seuilBudget || commande.getDateFin().getTime() < today.getTime()) { // Si le montant est en dessous de 1000 € OU la date est passée
                       listeCommandesType1.add(commande);
                } else if (commande.getMontant() > initMontant) { // Si la commande n'a qu'un montant
                    if (commande.getBudget() < seuilBudget) { // Si ce montant est inférieur au seuil de 1 000 €
                        listeCommandesType2.add(commande); }
                    }
                } else if (commande.getDateFin() != null) { // SI la commande n'a qu'une date de fin
                    if (commande.getDateFin().getTime() < today.getTime()) { // Si la date de fin est inférieur à today
                        listeCommandesType3.add(commande); }
                }
            }
        }

        if(!listeCommandesType1.isEmpty()) mailService.envoiMailLimiteDesCommandes(1,listeCommandesType1);
        if(!listeCommandesType2.isEmpty()) mailService.envoiMailLimiteDesCommandes(2,listeCommandesType2);
        if(!listeCommandesType3.isEmpty()) mailService.envoiMailLimiteDesCommandes(3,listeCommandesType3);

    }

}
