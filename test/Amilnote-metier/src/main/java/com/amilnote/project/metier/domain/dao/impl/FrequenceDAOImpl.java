package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.FrequenceDAO;

import com.amilnote.project.metier.domain.entities.Frequence;

import org.springframework.stereotype.Repository;

@Repository("FrequenceDAO")
public class FrequenceDAOImpl extends AbstractDAOImpl<Frequence> implements FrequenceDAO {

    @Override
    protected Class<Frequence> getReferenceClass() {
        return Frequence.class;
    }


}
