/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.utils.Pair;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Classe AddviseSession pour utiliser les sessions du programme Addvise, c'est-à-dire une séance datée d'un module
 */
@Entity
@Table(name = "ami_addvise_session")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AddviseSession implements Serializable {
    /**
     * EN_COURS_DE_SAISIE : La session est modifiable et n'est pas encore passée
     * CONFIRMEE : La session est confirmée pour avoir lieu, elle reste modifiable après
     * VALIDEE : La session, après modifications, est considérée comme passée et définitive
     * SUPPRIMEE : La session est annulée, elle n'aura ou elle n'a pas eu lieu
     */
    public enum EtatSession {
        EN_COURS_DE_SAISIE, CONFIRMEE, VALIDEE, SUPPRIMEE
    }

    private int id;
    private ModuleAddvise moduleAddvise;
    private Date date;
    private AddviseFormateur formateur;
    private EtatSession etat;
    private List<Collaborator> collaborateurs;
    private List<AddviseLinkSessionCollaborateur> listeLinkSessionCollaborateur;

    public AddviseSession() {
        collaborateurs = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_module_addvise")
    public ModuleAddvise getModuleAddvise() {
        return moduleAddvise;
    }

    public void setModuleAddvise(ModuleAddvise moduleAddvise) {
        this.moduleAddvise = moduleAddvise;
    }

    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formateur")
    public AddviseFormateur getFormateur() {
        return formateur;
    }

    public void setFormateur(AddviseFormateur formateur) {
        this.formateur = formateur;
    }

    @Column(name = "etat")
    public EtatSession getEtat() {
        return etat;
    }

    public void setEtat(EtatSession etat) {
        this.etat = etat;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ami_addvise_link_session_collaborateur",
        joinColumns = {@JoinColumn(name = "id_addvise_session", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "id_collaborateur", referencedColumnName = "id")})
    public List<Collaborator> getCollaborateurs() {
        return this.collaborateurs;
    }

    public void setCollaborateurs(List<Collaborator> collaborators) {
        this.collaborateurs = collaborators;
    }

    public void addCollaborateur(Collaborator collaborator) {
        this.collaborateurs.add(collaborator);
    }

    @OneToMany(mappedBy = "session", fetch = FetchType.LAZY)
    public List<AddviseLinkSessionCollaborateur> getListeLinkSessionCollaborateur() {
        return listeLinkSessionCollaborateur;
    }

    public void setListeLinkSessionCollaborateur(List<AddviseLinkSessionCollaborateur> listeLinkSessionCollaborateur) {
        this.listeLinkSessionCollaborateur = listeLinkSessionCollaborateur;
    }

    /**
     * Constante sur les sessions passés non stockés en base
     *
     * @return liste contenant des paires {année, nombre par modules de M1 à M6}
     */
    @Transient
    public static List<Pair<Integer, int[]>> getNombreSessionsPassees() {
        List<Pair<Integer, int[]>> sessionsPassees = new ArrayList<>();
        sessionsPassees.add(new Pair<>(2012, new int[]{1, 0, 0, 0, 0, 0}));
        sessionsPassees.add(new Pair<>(2013, new int[]{5, 0, 0, 0, 0, 0}));
        sessionsPassees.add(new Pair<>(2014, new int[]{5, 5, 2, 0, 0, 0}));
        sessionsPassees.add(new Pair<>(2015, new int[]{1, 3, 3, 3, 2, 0}));
        sessionsPassees.add(new Pair<>(2016, new int[]{3, 0, 0, 0, 0, 0}));
        return sessionsPassees;
    }
}
