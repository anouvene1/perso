/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.amilnote.project.metier.domain.entities.json.ContactClientAsJson;
import org.springframework.http.ResponseEntity;
import java.util.List;

/**
 * The interface Contact client service.
 */
public interface ContactClientService extends AbstractService<ContactClient> {

    /**
     * retrouver tous les clients et les trier par ordre ascendant
     *
     * @return List de clients
     */
    List<ContactClient> findAllOrderByNomAsc();

    /**
     * retrouver tous les clients et les trier par ordre ascendant au format Json
     *
     * @return List de clients
     */
    List<ContactClientAsJson> findAllOrderByNomAscAsJson();

    /**
     * Gets list contacts to add.
     *
     * @return the list contacts to add
     */
    List<ContactClientAsJson> getListContactsToAdd();

    /**
     * Sets list forfaits to add.
     *
     * @param listContactsToAdd the list contacts to add
     */
    void setListForfaitsToAdd(List<ContactClientAsJson> listContactsToAdd);

    /***
     * retourne la liste des Contacts chez le client passé en paramètre
     *
     * @param pClient Client voulu
     * @return Liste des contatcs chez le client
     */
    List<ContactClientAsJson> findAllContactsForClient(Client pClient);

    /**
     * retrouver un contact par son Id
     *
     * @param pIdContactClient the p id contact client
     * @return ContactClient contact client
     */
    ContactClient findById(Long pIdContactClient);

    /**
     * Find by mail for client contact client.
     *
     * @param pMail   the p mail
     * @param pClient the p client
     * @return the contact client
     */
    ContactClient findByMailForClient(String pMail, Client pClient);

    /**
     * Create or updtate client contact.
     * and return The ClientContact object
     *
     * @param contactClientAsJson the client contact as json
     * @param clientAsJson        the client as json
     * @return the string
     */
    ResponseEntity<ContactClientAsJson> createOrUpdtateContact(ContactClientAsJson contactClientAsJson, ClientAsJson clientAsJson);

    /**
     * Delete contact client string.
     *
     * @param pContact the p contact
     * @return the string
     */
    String deleteContactClient(ContactClient pContact);

    /**
     * Get list of billing managers for a customer
     * @param customer
     * @return the list
     */
    List<ContactClient> findAllBillingManagersByCustomer(Client customer);
}
