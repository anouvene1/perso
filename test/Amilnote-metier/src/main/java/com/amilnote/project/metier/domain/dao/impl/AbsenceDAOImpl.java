/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.AbsenceDAO;
import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Etat;
import com.amilnote.project.metier.domain.entities.TypeAbsence;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * The type Absence dao.
 */
@Repository("absenceDAO")
public class AbsenceDAOImpl extends AbstractDAOImpl<Absence> implements AbsenceDAO {

    private static Logger logger = LogManager.getLogger(AbsenceDAOImpl.class);

    private static final String ABSENCE_SUPPRESSION_SUCCESS = "Suppression effectuée avec succès";
    private static final String ABSENCE_SUPPRESSION_FAILURE = "Aucune suppression n'a été effectuée";

    @Autowired
    private EtatDAO etatDAO;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Absence> getReferenceClass() {
        return Absence.class;
    }

    /**
     * {@linkplain AbsenceDAO#getAllTypeAbsence()}
     */
    @Override
    public List<TypeAbsence> getAllTypeAbsence() {
        return currentSession().createCriteria(TypeAbsence.class).list();
    }

    /**
     * {@linkplain AbsenceDAO#getTypeAbsenceById(long)}
     */
    @Override
    public TypeAbsence getTypeAbsenceById(long id) {
        return (TypeAbsence) currentSession().get(TypeAbsence.class, id);
    }

    /**
     * {@linkplain AbsenceDAO#getNbAbsenceAttenteVal()}
     */
    @Override
    public int getNbAbsenceAttenteVal() {
        Criteria lnbAbs = currentSession().createCriteria(Absence.class);
        Etat etatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        lnbAbs.add(Restrictions.eq(Absence.PROP_ETAT, etatSO));
        List<Absence> listeAbs = lnbAbs.list();
        return listeAbs.size();
    }

    /**
     * {@linkplain AbsenceDAO#getNbAbsenceAttenteVal(Collaborator)}
     */
    @Override
    public int getNbAbsenceAttenteVal(Collaborator collaborator) {
        Criteria lnbAbs = currentSession().createCriteria(Absence.class);
        Etat etatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        lnbAbs.add(Restrictions.eq(Absence.PROP_ETAT, etatSO));
        lnbAbs.add(Restrictions.eq(Absence.PROP_COLLABORATEUR, collaborator));
        List<Absence> listeAbs = lnbAbs.list();
        return listeAbs.size();
    }

    /**
     * {@linkplain AbsenceDAO#getNbAbsenceByEtatAndCollaborator(Collaborator, Etat)}
     */
    @Override
    public int getNbAbsenceByEtatAndCollaborator(Collaborator collaborator, Etat pEtat) {
        Criteria lnbAbs = currentSession().createCriteria(Absence.class);
        lnbAbs.add(Restrictions.eq(Absence.PROP_COLLABORATEUR, collaborator));
        lnbAbs.add(Restrictions.eq(Absence.PROP_ETAT, pEtat));
        List<Absence> listeAbs = lnbAbs.list();
        return listeAbs.size();
    }

    /**
     * {@linkplain AbsenceDAO#getDateDernierAbsSoumis(Collaborator)}
     */
    @Override
    public Date getDateDernierAbsSoumis(Collaborator collaborator) {
        Criteria lraSoumis = currentSession().createCriteria(Absence.class).setProjection(Projections.max(Absence.PROP_DATEDEBUT));
        Etat lEtatSoumis = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);
        lraSoumis.add(Restrictions.eq(Absence.PROP_COLLABORATEUR, collaborator));
        lraSoumis.add(Restrictions.eq(Absence.PROP_ETAT, lEtatSoumis));
        lraSoumis.addOrder(Order.desc(Absence.PROP_DATESOUMISSION));
        return (Date) lraSoumis.uniqueResult();
    }

    /**
     * {@linkplain AbsenceDAO#getAbsencesOrderByDate()} ()}
     */
    @Override
    public List<Absence> getAbsencesOrderByDate() {
        Criteria lListAbsences = getCriteria();
        lListAbsences.addOrder(Order.asc(Absence.PROP_DATEDEBUT));
        return lListAbsences.list();
    }

    /**
     * {@linkplain AbsenceDAO#findAbsencesByStateMonthAndYearOrderByDate(Etat, Integer, Integer)}
     */
    @Override
    public List<Absence> findAbsencesByStateMonthAndYearOrderByDate(Etat state, Integer month, Integer year) {
        Criteria absenceCriteria = getCriteria();
        if (state != null)
            absenceCriteria.add(Restrictions.eq(Absence.PROP_ETAT, state));
        if (month != null)
            absenceCriteria.add(Restrictions.sqlRestriction("YEAR(date_debut) = ?", year, IntegerType.INSTANCE));
        if (year != null)
            absenceCriteria.add(Restrictions.sqlRestriction("MONTH(date_debut) = ?", month, IntegerType.INSTANCE));
        absenceCriteria.addOrder(Order.asc(Absence.PROP_DATEDEBUT));
        return absenceCriteria.list();
    }

    /**
     * {@linkplain AbsenceDAO#getAbsencesByCollaboratorAndEtat(Collaborator, Etat)}
     */
    @Override
    public List<Absence> getAbsencesByCollaboratorAndEtat(Collaborator collaborator, Etat pEtat) {
        Criteria lListAbsences = getCriteria();
        lListAbsences.add(Restrictions.eq(Absence.PROP_COLLABORATEUR, collaborator));
        lListAbsences.add(Restrictions.eq(Absence.PROP_ETAT, pEtat));
        return lListAbsences.list();
    }

    /**
     * {@linkplain AbsenceDAO#updateAbsencesFromBRToSO(List, String)}
     */
    @Override
    public String updateAbsencesFromBRToSO(List<AbsenceAsJson> lListAbsenceAsJson, String comment) {
        Transaction tx = currentSession().beginTransaction();
        try {
            Etat lEtatSoumis = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, "SO");
            for (AbsenceAsJson abs : lListAbsenceAsJson) {
                Absence lAbsence = this.get(abs.getId());
                lAbsence.setEtat(lEtatSoumis);
                lAbsence.setDateSoumission(new Date());
                lAbsence.setCommentaire(comment);
            }
            currentSession().flush();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            logger.error("[hibernate error] update absences", e);
            return e.getMessage();
        }
        return "";
    }

    /**
     * {@linkplain AbsenceDAO#getByCollaboratorBetweenDate(Collaborator, Date, Date)}
     */
    @Override
    public List<Absence> getByCollaboratorBetweenDate(Collaborator collaborator, Date pDateDeb, Date pDateFin) {
        Criteria lListAbsences = getCriteria();
        lListAbsences.add(Restrictions.eq(Absence.PROP_COLLABORATEUR, collaborator));
        lListAbsences.add(Restrictions.ge(Absence.PROP_DATEDEBUT, pDateDeb));
        lListAbsences.add(Restrictions.le(Absence.PROP_DATEDEBUT, pDateFin));
        lListAbsences.add(Restrictions.ge(Absence.PROP_DATEFIN, pDateDeb));
        return lListAbsences.list();
    }

    /**
     * {@linkplain AbsenceDAO#getAbsencesSoumisesBetweenDate(Date, Date)}
     */
    @Override
    public List<Absence> getAbsencesSoumisesBetweenDate(Date pDateDeb, Date pDateFin) {
        Criteria listeAbsencesSoumises = getCriteria();
        Criterion debutAbsSupDebutPeriode = Restrictions.ge(Absence.PROP_DATEDEBUT, pDateDeb);
        Criterion debutAbsInferieurDebutPeriode = Restrictions.le(Absence.PROP_DATEDEBUT, pDateDeb);
        Criterion finAbsSupDebutPeriode = Restrictions.ge(Absence.PROP_DATEFIN, pDateDeb);
        Criterion etatSoumis = Restrictions.like(Absence.PROP_ETAT, etatDAO.getAllEtat().get(1));
        // pour que l'absence soit considérée comme étant dans l'intervale il faut que :
        // sa date de debut doit etre supperieur au debut de la periode étudiée ET sa date debut inferieur à la date de fin de la periode étudiée
        // OU sa date de debut doit être inférieur à la date de debut de la periode etudiée ET la date de fin doit être supperieur à la date de debut de la periode étudiée
        // il faut egalement que la mission soit dans l'état soumis
        listeAbsencesSoumises.add(Restrictions.and(etatSoumis, Restrictions.or(debutAbsSupDebutPeriode, Restrictions.and(debutAbsInferieurDebutPeriode, finAbsSupDebutPeriode))));

        return listeAbsencesSoumises.list();
    }

    /**
     * {@linkplain AbsenceDAO#getAbsencesValideesBetweenDate(Date, Date)}
     */
    @Override
    public List<Absence> getAbsencesValideesBetweenDate(Date pDateDeb, Date pDateFin) {
        Criteria listeAutresAbsences = getCriteria();
        Criterion debutAbsSupDebutPeriode = Restrictions.ge(Absence.PROP_DATEDEBUT, pDateDeb);
        Criterion debutAbsInferieurDebutPeriode = Restrictions.le(Absence.PROP_DATEDEBUT, pDateDeb);
        Criterion finAbsSupDebutPeriode = Restrictions.ge(Absence.PROP_DATEFIN, pDateDeb);
        Criterion etatValidee = Restrictions.like(Absence.PROP_ETAT, etatDAO.getAllEtat().get(2));
        // pour que l'absence soit considérée comme étant dans l'intervale il faut que :
        // sa date de debut doit etre supperieur au debut de la periode étudiée ET sa date debut inferieur à la date de fin de la periode étudiée
        // OU sa date de debut doit être inférieur à la date de debut de la periode etudiée ET la date de fin doit être supperieur à la date de debut de la periode étudiée
        // il faut egalement que la mission soit dans l'etat Validee
        listeAutresAbsences.add(Restrictions.and(etatValidee, Restrictions.or(debutAbsSupDebutPeriode, Restrictions.and(debutAbsInferieurDebutPeriode, finAbsSupDebutPeriode))));
        return listeAutresAbsences.list();
    }


    @Override
    @Transactional
    public Long updateEtatAbsence(Absence pAbsence, Etat etat) {
        pAbsence.setEtat(etat);
        return pAbsence.getId();
    }

    @Override
    @Transactional
    public Long updateCommentaireAbsence(Absence pAbsence, String commentaire) {
        pAbsence.setCommentaire(commentaire);
        return pAbsence.getId();
    }

    @Override
    @Transactional
    public String deleteAbsence(Absence pAbsence) {
        if (null != pAbsence) {
            try {
                delete(pAbsence);
                return ABSENCE_SUPPRESSION_SUCCESS;
            } catch (HibernateException e) {
                logger.error("[hibernate error] deleteAbsence", e);
            }
        }

        return ABSENCE_SUPPRESSION_FAILURE;
    }

    /**
     * {@linkplain AbsenceDAO#findByIds(List)}
     */
    @Override
    public List<Absence> findByIds(List<Long> ids) {
        return getCriteria()
                .add(Restrictions.in(Absence.PROP_ID, ids))
                .list();
    }
}