package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.OutilsInterneDAO;
import com.amilnote.project.metier.domain.entities.OutilsInterne;
import org.springframework.stereotype.Repository;

@Repository("outilsInterneDAO")
public class OutilsInterneDAOImpl extends AbstractDAOImpl<OutilsInterne> implements OutilsInterneDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<OutilsInterne> getReferenceClass() {
        return OutilsInterne.class;
    }

}
