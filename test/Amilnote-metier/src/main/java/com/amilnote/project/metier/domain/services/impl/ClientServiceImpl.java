/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.ClientDAOImpl;
import com.amilnote.project.metier.domain.entities.Client;
import com.amilnote.project.metier.domain.entities.ContactClient;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.amilnote.project.metier.domain.services.ClientService;
import com.amilnote.project.metier.domain.utils.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Client service.
 */
@Service("clientService")
public class ClientServiceImpl extends AbstractServiceImpl<Client, ClientDAOImpl> implements ClientService {

    @Autowired
    private ClientDAOImpl clientDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public ClientDAOImpl getDAO() {
        return (ClientDAOImpl) clientDAO;
    }

    /**
     * {@linkplain ClientService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Client> findAllOrderByNomAsc() {
        return clientDAO.findAllOrderByNomAsc();
    }

    /**
     * {@linkplain ClientService#findByNom(String)}
     */
    @Override
    public Client findByNom(String pNom) {
        return clientDAO.findUniqEntiteByProp(Client.PROP_NOM, pNom);
    }

    /**
     * {@linkplain ClientService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<ClientAsJson> findAllOrderByNomAscAsJson() {

        List<Client> tmpListClient = this.findAllOrderByNomAsc();
        List<ClientAsJson> tmpListClientAsJson = new ArrayList<ClientAsJson>();

        for (Client tmpClient : tmpListClient) {

            tmpListClientAsJson.add(tmpClient.toJson());

        }
        return tmpListClientAsJson;
    }

    /**
     * {@linkplain ClientService#deleteClient(Client)}
     */
    @Override
    public String deleteClient(Client pClient) {
        return clientDAO.deleteClient(pClient);
    }

    /**
     * {@linkplain ClientService#updateOrCreateClient(ClientAsJson)}
     */
    @Override
    public String updateOrCreateClient(ClientAsJson pClient) throws Exception {
        return clientDAO.updateOrCreateClient(pClient);
    }

    /**
     * {@linkplain ClientService#getResponsableFacturation(Client)}
     */
    @Override
    public int getResponsableFacturation(Client client){
        int responsableFacturation = 0;
        List<ContactClient> list = client.getContacts();
        for (ContactClient contact : list) {
            if (contact.isRespFacturation()) {
                responsableFacturation = contact.getId().intValue();
            }
        }

        return responsableFacturation;
    }

    /**
     * {@linkplain ClientService#getResponsableClient(Client)}
     */
    @Override
    public int getResponsableClient(Client client){
        int responsableFacturation = 0;
        List<ContactClient> list = client.getContacts();
        for (ContactClient contact : list) {
            if (contact.isRespClient()) {
                responsableFacturation = contact.getId().intValue();
            }
        }

        return responsableFacturation;
    }

    /**
     * {@linkplain ClientService#getResponsableFacturation(Long)}
     */
    @Override
    public int getResponsableFacturation(Long idClient){
        int responsableFacturation = 0;
        Client client = this.get(idClient);
        List<ContactClient> list = client.getContacts();
        for (ContactClient contact : list) {
            if (contact.isRespFacturation()) {
                responsableFacturation = contact.getId().intValue();
            }
        }

        return responsableFacturation;
    }

    /**
     * {@linkplain ClientService#getResponsableFacturation(Long)}
     */
    @Override
    public int getResponsableClient(Long idClient){
        int responsableFacturation = 0;
        Client client = this.get(idClient);
        List<ContactClient> list = client.getContacts();
        for (ContactClient contact : list) {
            if (contact.isRespClient()) {
                responsableFacturation = contact.getId().intValue();
            }
        }

        return responsableFacturation;
    }

    /**
     * {@linkplain ClientService#isClientIntracNumberFR(Client)}
     */
    @Override
    public boolean isClientIntracNumberFR(Client customer){
        String clientIntrac = customer.getTva();

        if(clientIntrac.isEmpty() || clientIntrac.contains(Constantes.FRENCH_INTRA_COMMUNITY_ACCOUNT_NUMBER_START)){
            return true;
        }

        return false;
    }
}
