/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.ContactClientAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * The type Contact client.
 */
@Entity
@Table(name = "ami_contact_client")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ContactClient implements java.io.Serializable {

    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_NOM.
     */
    public static final String PROP_NOM = "nom";
    /**
     * The constant PROP_PRENOM.
     */
    public static final String PROP_PRENOM = "prenom";
    /**
     * The constant PROP_TEL.
     */
    public static final String PROP_TEL = "telephone";
    /**
     * The constant PROP_MAIL.
     */
    public static final String PROP_MAIL = "mail";
    /**
     * The constant PROP_CLIENT.
     */
    public static final String PROP_CLIENT = "client";
    /**
     * The constant PROP_RESP_CLIENT.
     */
    public static final String PROP_RESP_CLIENT = "resp_client";
    /**
     * The constant PROP_RESP_FACTURATION.
     */
    public static final String PROP_RESP_FACTURATION = "resp_facturation";
    /**
     * The constant PROP_ID_CLIENT.
     */
    public static final String PROP_ID_CLIENT= "id_client";

    private static final long serialVersionUID = -260585274471076262L;

    /**
     * The constant PROP_ENABLED.
     */
    public static final String PROP_ENABLED = "enabled";

    @Column(name = PROP_ID)
    private Long id;

    @Column(name = PROP_NOM)
    private String nom;

    @Column(name = PROP_PRENOM)
    private String prenom;

    @Column(name = PROP_MAIL)
    private String mail;

    @Column(name = PROP_TEL)
    private String telephone;

    @Column(name = PROP_CLIENT)
    private Client client;

    @Column(name = PROP_RESP_CLIENT)
    private boolean respClient;

    @Column(name = PROP_RESP_FACTURATION)
    private boolean respFacturation;

    @Column(name = PROP_ENABLED)
    private boolean enabled;

    /**
     * Instantiates a new Contact client.
     */
    public ContactClient() {
        super();
    }

    /**
     * Instantiates a new Contact client.
     *
     * @param nom               the lastname
     * @param prenom            the firstname
     * @param mail              the e-mail
     * @param telephone         the telephone
     * @param client            the client
     * @param respClient        the client manager
     * @param respFacturation   the billing manager
     * @param enabled           the enabled status
     */
    public ContactClient(String nom, String prenom, String mail, String telephone, Client client, boolean respClient, boolean respFacturation, boolean enabled) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.telephone = telephone;
        this.client = client;
        this.respClient = respClient;
        this.respFacturation = respFacturation;
        this.enabled = enabled;
    }

    /**
     * Parse ce ContactClient au format json
     *
     * @return un ContactClient json
     */
    public ContactClientAsJson toJson() {
        return new ContactClientAsJson(this);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = PROP_ID, unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    @Column(name = PROP_NOM, nullable = false)
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    @Column(name = PROP_PRENOM, nullable = false)
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets mail.
     *
     * @return the mail
     */
    @Column(name = PROP_MAIL, nullable = false)
    public String getMail() {
        return mail;
    }

    /**
     * Sets mail.
     *
     * @param mail the mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Gets telephone.
     *
     * @return the telephone
     */
    @Column(name = PROP_TEL, nullable = true)
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets telephone.
     *
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Gets client.
     *
     * @return the client
     */
    @ManyToOne
    @JoinColumn(name = PROP_ID_CLIENT, nullable = false)
    public Client getClient() {
        return client;
    }

    /**
     * Sets client.
     *
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * permet de savoir si le contact est responsable client ou non
     *
     * @return boolean boolean
     */
    @Column(name = PROP_RESP_CLIENT, nullable = false)
    public boolean isRespClient() {
        return this.respClient;
    }

    /**
     * mise a jour du statut du contact
     *
     * @param respClient the resp client
     */
    public void setRespClient(Boolean respClient) {
        this.respClient = respClient;
    }

    /**
     * permet de savoir si le contact est responsable facturation ou non
     *
     * @return boolean boolean
     */
    @Column(name = PROP_RESP_FACTURATION, nullable = false)
    public boolean isRespFacturation() {
        return this.respFacturation;
    }

    /**
     * mise a jour du statut du contact
     *
     * @param respFacturation the resp facturation
     */
    public void setRespFacturation(Boolean respFacturation) {
        this.respFacturation = respFacturation;
    }

    /**
     * Get the enabled status of client contact
     * @return boolean Enabled status of client contact
     */
    @Column(name = PROP_ENABLED)
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set status of client contact to enabled
     * @param enabled Enabled status of client contact
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
