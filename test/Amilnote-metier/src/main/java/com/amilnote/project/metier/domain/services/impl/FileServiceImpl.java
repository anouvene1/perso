package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.services.FileService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.Utils;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.NamingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static com.amilnote.project.metier.domain.utils.Constantes.SLASH;
import static com.amilnote.project.metier.domain.utils.Utils.concatenateObjectsToString;

@Service("FileService")
public class FileServiceImpl implements FileService {

    private static Logger logger = LogManager.getLogger(FileServiceImpl.class);

    /**
     * Convert a file (with its extension) into a .ZIP file
     *
     * @param fileName is the name of the file that must be converted
     * @param folder   is the location of the file (folder context)
     */
    @Override
    public void convertFileToZip(String fileName, String folder) {
        try {
            // recupere le chemin d'acces du fichier
            String sourceFile = Parametrage.getContext(folder) + fileName;

            // garde seulement le nom du fichier pour que le .ZIP ait le meme nom
            String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));

            try (FileOutputStream fileOutputStream = new FileOutputStream(Parametrage.getContext(folder) + File.separator + fileNameWithoutExtension + Constantes.EXTENSION_FILE_ZIP)) {
                try (ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
                    addFileToExistingZip(zipOutputStream, sourceFile);
                }
            }
        } catch (IOException | NamingException error) {
            logger.error(error.getMessage());
        }

    }

    /**
     * Add file retrieved by file path to a given zip stream
     *
     * @param zipOutputStream the zip stream
     * @param sourceFile      the path to the file which will be include in the zip
     * @throws IOException local/network file/folder read/write problems
     */
    private void addFileToExistingZip(ZipOutputStream zipOutputStream, String sourceFile) throws IOException {
        File fileToZip = new File(sourceFile);
        try (FileInputStream fileInputStream = new FileInputStream(fileToZip)) {
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOutputStream.putNextEntry(zipEntry);
            final byte[] bytes = new byte[2048];
            int amountRead;
            while ((amountRead = fileInputStream.read(bytes)) > 0) {
                zipOutputStream.write(bytes, 0, amountRead);
            }
        }
    }

    /**
     * Unzip a file
     *
     * @param file is the file that must be unzipped (must be in a ZIP format)
     * @return the file that is contained into the .ZIP file
     */
    public File unZipFile(File file) {
        File unzippedFile = null;
        try {

            // recupere le chemin d'acces du dossier pour que le fichier extrait soit au meme endroit
            String filePath = file.getParent();

            // recupere le chemin d'acces du fichier a dezipper
            String zipFileName = file.getPath();

            byte[] buffer = new byte[1024];

            try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFileName))) {
                ZipEntry zipEntry = zipInputStream.getNextEntry();
                while (zipEntry != null) {
                    String fileName = zipEntry.getName();
                    unzippedFile = new File(filePath + File.separator + fileName);
                    try (FileOutputStream fileOutputStream = new FileOutputStream(unzippedFile)) {
                        int length;
                        while ((length = zipInputStream.read(buffer)) > 0) {
                            fileOutputStream.write(buffer, 0, length);
                        }
                        zipEntry = zipInputStream.getNextEntry();
                    }
                }
                zipInputStream.closeEntry();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return unzippedFile;
    }

    /**
     * deletes a file from the system
     *
     * @param fileName is the name of the file that must be deleted
     * @param folder   is the location of the file (folder context)
     */
    public void deleteFile(String fileName, String folder) {
        try {
            File oldFile = new File(Parametrage.getContext(folder) + fileName);

            deleteFile(oldFile);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * deletes a file from the system
     *
     * @param file is the file that must be deleted
     */
    public void deleteFile(File file) {
        try {
            if (file.exists()) {
                Files.delete(file.toPath());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Check if the file was zipped
     *
     * @param fullPathFile File path to check
     * @return file was zipped or not
     */
    public boolean fileWasZipped(String fullPathFile) {
        File file = new File(fullPathFile.substring(0, fullPathFile.lastIndexOf('.')) + Constantes.EXTENSION_FILE_ZIP);

        return file.exists();

    }

    /**
     * Check if the file was zipped
     *
     * @param file file to check
     * @return file was zipped or not
     */
    public boolean fileWasZipped(File file) {
        return fileWasZipped(file.getPath());
    }


    /**
     * Read all bytes of a file retrieved by a path and return a byte array
     *
     * @param filePath the path to the file to convert
     * @return a byte array of the converted file
     * @throws IOException network/local file/folder read/write problem
     */
    public byte[] readFileByPath(String filePath) throws IOException {

        byte[] resultByteArray = new byte[]{0};
        File file = new File(filePath);

        if (file.exists() && file.isFile()) {
            Path path = file.toPath();
            resultByteArray = Files.readAllBytes(path);
        }

        return resultByteArray;
    }

    /**
     * Creates a folder according to the filePath passed as parameter
     *
     * @param filePath the path of the file to be created
     */
    public void createFolder(String filePath) {
        // Getting the directory to create the new folder
        Path directoryPath = (new File(filePath).getParentFile()).toPath();

        // Creating the directory
        try {
            Files.createDirectories(directoryPath);
        } catch (IOException ioException) {
            logger.error("Erreur lors de la création du dossier : " + ioException.getMessage());
        }
    }

    /**
     * {@linkplain FileService#writeFile(String, byte[])}
     */
    public void writeFile(String filePath, byte[] fileAsBytes) {
        try (
                // Opening the stream
                FileOutputStream fileOutputStream = new FileOutputStream(new File(filePath));
        ) {

            // Writing the file's content
            fileOutputStream.write(fileAsBytes);

        } catch (Exception exception) {

            logger.error(exception.getMessage());

        }
    }

    /**
     * Add a folder to a given zip
     *
     * @param path      path to the folder to include into the zip
     * @param srcFolder the zip file path
     * @param zip       the zip file who will include the folder
     * @throws IOException network/local file/folder read/write problem
     */
    public void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws IOException {
        File folder = new File(srcFolder);
        for (String fileName : folder.list()) {
            if (path.equals("")) {
                addFilesToExistingZip(
                        folder.getName(),
                        zip,
                        concatenateObjectsToString(srcFolder, SLASH, fileName)
                );
            } else {
                addFilesToExistingZip(
                        concatenateObjectsToString(path, SLASH, folder.getName()),
                        zip,
                        concatenateObjectsToString(srcFolder, SLASH, fileName)
                );
            }
        }
    }

    /**
     * Add files retrieved by their file path to a generated zip
     *
     * @param path                 the folder path to put the zip
     * @param generatedZipFilePath the zip file path where will be placed the generated result zip
     * @param filesPathsAsString   files paths for retrieving the files to add to the zip
     */
    public void addFilesToGeneratedZip(String path, String generatedZipFilePath, String... filesPathsAsString) {
        try (FileOutputStream fileWriter = new FileOutputStream(generatedZipFilePath)) {
            try (ZipOutputStream resultZip = new ZipOutputStream(fileWriter)) {
                addFilesToExistingZip(path, resultZip, filesPathsAsString);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Add files retrieved by their file path to a given zip
     *
     * @param path               the folder path to put the zip
     * @param resultZip          the zip file path where will be placed the generated result zip
     * @param filesPathsAsString files paths for retrieving the files to add to the zip
     * @throws IOException network/local file/folder read/write problem
     */
    public void addFilesToExistingZip(String path, ZipOutputStream resultZip, String... filesPathsAsString) throws IOException {

        for (String filePathAsString : filesPathsAsString) {
            File file = new File(filePathAsString);
            if (file.isDirectory()) {
                addFolderToZip(path, filePathAsString, resultZip);
            } else {
                addFileToExistingZip(resultZip, filePathAsString);
            }
        }
        resultZip.flush();
        resultZip.close();
    }

    /**
     * {@linkplain FileService#uploadFile(MultipartFile, String, Long, FileFormatEnum[])}
     */
    @Override
    public void uploadFile(MultipartFile file, String fullPath, Long fileSizeMax, FileFormatEnum[] acceptedFormat) throws Exception {

        UploadFile uploader = new UploadFile(fileSizeMax, acceptedFormat);
        try {
            uploader.storeFile(file, fullPath); //throw exception if not valid
        } catch (Exception e) {
            logger.error("[UPLOAD FILE] : " + Utils.getloggedUsername() + " " + e.getMessage()); //log error with collab email
            throw new Exception(e.getMessage());
        }
    }

    /**
     * {@linkplain FileService#isValidToUpload(MultipartFile, Long, FileFormatEnum[])}
     */
    @Override
    public void isValidToUpload(MultipartFile file, Long fileSizeMax, FileFormatEnum[] acceptedFormat) throws Exception {

        UploadFile uploader = new UploadFile(fileSizeMax, acceptedFormat);
        try {
            uploader.isValidFile(file);
        } catch (UploadFile.SizeException | UploadFile.FormatException | UploadFile.FileEmptyException e) {
            logger.error("[UPLOAD FILE] : " + Utils.getloggedUsername() + " " + e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    /**
     * {@linkplain FileService#isValidImageContent(MultipartFile)}
     */
    @Override
    public void isValidImageContent(MultipartFile file) throws IOException {
        UploadFile.isValidImageFileContent(file);
    }

    /**
     * {@linkplain FileService#writeFileOutputStream(String, Workbook)}
     */
    @Override
    public void writeFileOutputStream(String filepath, Workbook wb) throws IOException {
        FileOutputStream out = new FileOutputStream(filepath);
        wb.write(out);
    }
}
