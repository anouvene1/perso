package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.AbstractTravailleur;

import java.util.Date;
import java.util.List;

public interface TravailleurDAO<T extends AbstractTravailleur> extends LongKeyDAO<T> {

    /**
     * To find all the {@link AbstractTravailleur} in the Database ordered by Nom ASC.
     *
     * @return a list containing {@link AbstractTravailleur}.
     */
    List<T> findTravailleurs();

    /**
     * Retourne un utilisateur par rapport à son username, ici mail
     *
     * @param pMail : mail de l'utilisateur
     * @return un collaborateur
     */
    T findByMail(String pMail);

    /**
     * retrouver un utilisateur par mail et mot de passe
     *
     * @param pUsername : mail de l'utilisateur
     * @param pPassword : mot de passe encodé en md5
     * @return un collaborateur
     */
    T findByUsernameAndPassword(String pUsername, String pPassword);

    /**
     * Retrieve the Travailleur that were enabled this year.
     *
     * @param yearFrais the year frais
     * @return the list
     */
    List<T> findCollaboratorsForFraisAnnuel(int yearFrais);

    /**
     * Retrieve the Travailleur that were enabled this month.
     *
     * @param pDate the p date
     * @return the list
     */
    List<T> findCollaboratorsForAbsenceMonth(Date pDate);

    /**
     * Retrieve a Travailleur with his firstname and his lastnamee
     *
     * @param firstname : the firstname of the person
     * @param lastname : the lastname of the person
     * @return a Collaborateur
     */
    T findCollaboratorByName(String firstname, String lastname);




}
