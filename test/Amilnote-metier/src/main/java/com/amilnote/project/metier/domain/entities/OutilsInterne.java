/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.OutilsInterneAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Outils Interne
 *
 * @author vavenein
 */
@Entity
@Table(name = "ami_ref_outils_interne")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class OutilsInterne implements java.io.Serializable {
    /**
     * The constant PROP_ID.
     */
    public static final String PROP_ID = "id";
    /**
     * The constant PROP_TYPE_FRAIS.
     */
    public static final String PROP_NOM = "nom";
    /**
     * The constant PROP_CODE.
     */
    public static final String PROP_CODE = "code";
    /**
     * The constant PROP_ETAT.
     */
    public static final String PROP_ETAT = "etat";

    private static final long serialVersionUID = -3551222991204906031L;

    private Long id;
    private String nom;
    private String code;
    private int etat;

    public OutilsInterne() {
    }

    public OutilsInterne(Long pId, String pNom, String pCode, int pEtat){
        super();
        this.id = pId;
        this.nom = pNom;
        this.code = pCode;
        this.etat = pEtat;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    /**
     * Sets ID
     *
     * @param pId the id to set
     */
    public void setId(Long pId) {
        this.id = pId;
    }


    /**
     * Gets nom
     *
     * @return the nom
     */
    @Column(name = "nom", nullable = false)
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom
     *
     * @param pNom the nom to set
     */
    public void setNom(String pNom) {
        this.nom = pNom;
    }


    /**
     * Gets code
     * @return the code
     */
    @Column(name = "code", nullable = false)
    public String getCode() {
        return code;
    }

    /**
     * Sets code
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }


    /**
     * Gets etat
     *
     * @return the etat
     */
    @Column(name = "etat", nullable = false)
    public int getEtat() {
        return etat;
    }

    /**
     * Sets etat
     *
      * @param pEtat the etat to set
     */
    public void setEtat(int pEtat) {
        this.etat = pEtat;
    }


    /**
     * To json type mission as json.
     *
     * @return the type mission as json
     */
    public OutilsInterneAsJson toJson() {
        return new OutilsInterneAsJson(this);
    }
}
