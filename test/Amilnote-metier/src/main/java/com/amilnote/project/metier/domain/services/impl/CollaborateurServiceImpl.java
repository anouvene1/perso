package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.*;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaborateurAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.*;
import com.amilnote.project.metier.domain.utils.enumerations.Agency;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.STATUT_SOUS_TRAITANT;
import static com.amilnote.project.metier.domain.utils.Utils._generate;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.EQUAL;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.IN;


/**
 * The type Collaborateur service.
 */
@Service("collaborateurService")
public class CollaborateurServiceImpl extends AbstractServiceImpl<Collaborateur, CollaborateurDAO> implements CollaborateurService {

    private static final Logger logger = LogManager.getLogger(CollaborateurServiceImpl.class);

    @Autowired
    private CollaborateurDAO collaborateurDAO;

    @Autowired
    private AbsenceDAO absenceDAO;

    @Autowired
    private RapportActivitesDAO rapportActivitesDAO;

    @Autowired
    private PosteService postService;

    @Autowired
    private StatutCollaborateurDAO statutCollaborateurDAO;

    @Autowired
    private ModuleAddviseDAO moduleAddviseDAO;

    @Autowired
    private RaisonSocialeDAO raisonSocialeDAO;

    @Autowired
    private MissionService missionService;

    @Autowired
    private StatutCollaborateurService statutCollaborateurService;

    @Autowired
    private CiviliteService civiliteService;

    @Autowired
    private MailService mailService;

    @Override
    public CollaborateurDAO getDAO() {
        return collaborateurDAO;
    }

    /**
     * {@linkplain CollaborateurService#findByMail(String)}
     */
    @Override
    public Collaborateur findByMail(String pMail) {
        return collaborateurDAO.findUniqEntiteByProp(Collaborateur.PROP_MAIL, pMail);
    }

    /**
     * {@linkplain CollaborateurService#findByUserNameAndPassword(String, String)}
     */
    @Override
    public Collaborateur findByUserNameAndPassword(String pMail, String pPassword) {
        return collaborateurDAO.findByUsernameAndPassword(pMail, pPassword);
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Collaborateur> findAllOrderByNomAsc() {
        //with admin, without disabled
        return collaborateurDAO.findTravailleurs();
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscWithoutADMIN()}
     */
    @Override
    public List<Collaborateur> findAllOrderByNomAscWithoutADMIN() {
        //without admin, without disabled
        return collaborateurDAO.findCollaborateurs(false, false);
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscWithoutADMINWithDisabled()}
     */
    @Override
    public List<Collaborateur> findAllOrderByNomAscWithoutADMINWithDisabled() {
        //without admin, with disabled
        return collaborateurDAO.findCollaborateurs(false, true);
    }

    /**
     * {@linkplain CollaborateurService#findCollaborateursForAbsenceMonthAsJson(Date)}
     */
    @Override
    public List<CollaborateurAsJson> findCollaborateursForAbsenceMonthAsJson(Date pDate) {
        List<Collaborateur> tmpListCollaborateur = collaborateurDAO.findCollaborateursForAbsenceMonth(pDate);
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>();

        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {
            tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());
        }
        return tmpListCollaborateurAsJson;
    }

    /**
     * {@linkplain CollaborateurService#findCollaborateursForFraisAnnuelAsJson(int)}
     */
    @Override
    public List<CollaborateurAsJson> findCollaborateursForFraisAnnuelAsJson(int yearFrais) {
        List<Collaborateur> tmpListCollaborateur = collaborateurDAO.findCollaborateursForFraisAnnuel(yearFrais);
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>();

        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {
            tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());
        }
        return tmpListCollaborateurAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collaborateur findCollaborateurByName(String firstname, String lastname) {
        return collaborateurDAO.findCollaborateurByName(firstname, lastname);
    }

    /**
     * {@linkplain CollaborateurService#findCollaborateursByResponsable(Collaborateur)}
     */
    @Override
    public List<Collaborateur> findCollaborateursByResponsable(Collaborateur responsable) {
        return collaborateurDAO.findCollaborateursByResponsable(responsable);
    }

    /**
     * {@linkplain CollaborateurService#findCollaborateurByNameAsJSON(String, String)}
     */
    @Override
    public CollaborateurAsJson findCollaborateurByNameAsJSON(String firstname, String lastname) {
        return collaborateurDAO.findCollaborateurByName(firstname, lastname).toJson();
    }

    /**
     * {@linkplain CollaborateurService#findCollaborateursByResponsableAsJSON(Collaborateur)}
     */
    @Override
    public List<CollaborateurAsJson> findCollaborateursByResponsableAsJSON(Collaborateur responsable) {
        List<Collaborateur> collaborateurs = collaborateurDAO.findCollaborateursByResponsable(responsable);
        List<CollaborateurAsJson> collaborateursJSON = new ArrayList<>();

        for (Collaborateur tmpCollab : collaborateurs) {
            if (tmpCollab.isEnabled() && !isAdminManager(tmpCollab)) {
                collaborateursJSON.add(tmpCollab.toJson());
            }
        }

        return collaborateursJSON;
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscAsJsonWithoutADMIN()}
     */
    @Override
    public List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMIN() {

        return convertCollaboratorListToAsJsonList(this.findAllOrderByNomAscWithoutADMIN());
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscAsJsonWithoutADMINWithDisabled()}
     */
    @Override
    public List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMINWithDisabled() {

        return convertCollaboratorListToAsJsonList(this.findAllOrderByNomAscWithoutADMINWithDisabled());
    }


    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllOrderByNomAscAsJson() {

        List<Collaborateur> tmpListCollaborateur = this.findAllOrderByNomAsc();
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>();
        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {
            tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());
        }
        return tmpListCollaborateurAsJson;
    }

    @Override
    public List<CollaborateurAsJson> findAllWithMissionOrderByNomAscAsJson() {
        List<Collaborateur> tmpListCollaborateur = collaborateurDAO.findTravailleurs();
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>();
        List<Mission> listMission;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date result = cal.getTime();
        DateTime date = new DateTime(result);

        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {

            listMission = missionService.findMissionsClientesByMonthForCollaborateur(tmpCollaborateur, date);
            if (!listMission.isEmpty())
                tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());
        }
        return tmpListCollaborateurAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String deleteCollaborateur(Collaborateur pCollaborateur) {

        // --- Récupération du collaborateur
        Collaborateur collaborateur = this.get(pCollaborateur.getId());

        // --- Test de l'existance du collaborateur
        if (null != collaborateur) {

            // BAB 15/06/2016 - [AMILNOTE-100] : on ne supprime plus un collaborateur lorsque on le desactive dans l'appli
            // on lui bloque juste l'acces à l'application je commente donc tout ce qui a ete fait ci-dessous :(

            //On vérifie que le collab ne soit pas manger d'un autre collab
            List<Collaborateur> allCollab = collaborateurDAO.findTravailleurs();
            StringBuilder collabManager = new StringBuilder();
            Boolean isManager = false;
            for (Collaborateur collab : allCollab) {
                if (collab.getManager() != null) {
                    if (collab.getManager().getId() == collaborateur.getId()) {
                        collabManager.append(collab.getMail() + " ");
                        isManager = true;
                    }
                }
            }

            if (isManager) {
                return "Attention: ce collaborateur ne peut pas être désactivé tant qu'il est associé à  un autre collaborateur en tant que manager (cf liste ci-dessous):"
                        + "<p>" + collabManager + "</p>";
            }

            // --- Création de la session et de la transaction
            Session session = collaborateurDAO.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // BAB 15/06/2016 - [AMILNOTE-100] : on set juste le flag en bdd enabled=false pour couper l'accès
            // on set la date de sortie a la date du jour
            collaborateur.setEnabled(false);
            collaborateur.setDateSortie(pCollaborateur.getDateSortie());
            // on sauvegarde la modification en bdd
            session.save(collaborateur);

            // --- Sauvegarde et Commit de la mise à jour
            session.flush();
            transaction.commit();

            List<Mission> missions = collaborateur.getMissions();
            for (Mission mission : missions) {
                try {
                    missionService.updateEtatMission(mission.getId(), Etat.ETAT_ANNULE);
                } catch (Exception e) {
                    logger.error("[update mission state] mission id : {}", mission.getId(), e);
                }
            }

            return "Désactivation effectuée avec succès";
        } else {
            return "Erreur collaborateur null";
        }

    }


    /**
     * @param nomEmail email
     * @return true or false
     */
    @Override
    public boolean checkEmailExist(String nomEmail) {

        Boolean res;

        Collaborateur collaborateur = collaborateurDAO.findByMail(nomEmail);
        if (collaborateur != null) {
            res = true;
        } else {
            res = false;
        }
        return res;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String updateOrCreateCollaborateur(CollaborateurAsJson pCollaborateurAsJson) throws Exception {

        String messageRetour = "";

        if (null == pCollaborateurAsJson) {
            throw new Exception("Le collaborateur ne doit pas être vide");
        }

        Collaborateur tmpCollaborateur = null;
        String clearTmpPassword = "";

        // Modification d'un collaborateur existant
        if (null != pCollaborateurAsJson.getId()) {

            tmpCollaborateur = this.get(pCollaborateurAsJson.getId());

            if (null == tmpCollaborateur) {
                throw new Exception("L'identifiant du collaborateur est inconnu");
            }

            if (null != pCollaborateurAsJson.getEnabled()) {
                tmpCollaborateur.setEnabled(pCollaborateurAsJson.getEnabled());
            }
            if (null != pCollaborateurAsJson.getMail()) {
                if (!checkEmailExist(pCollaborateurAsJson.getMail()) || null != pCollaborateurAsJson.getId()) {
                    tmpCollaborateur.setMail(pCollaborateurAsJson.getMail());
                } else {
                    throw new Exception("Erreur dans l'adresse email: Adresse vide ou déjà existante en base.");
                }
            }
            if (null != pCollaborateurAsJson.getNom()) {
                tmpCollaborateur.setNom(pCollaborateurAsJson.getNom());
            }
            if (null != pCollaborateurAsJson.getPrenom()) {
                tmpCollaborateur.setPrenom(pCollaborateurAsJson.getPrenom());
            }
            if (null != pCollaborateurAsJson.getPassword()) {
                try {
                    tmpCollaborateur.setPasswordAndEncode(pCollaborateurAsJson.getPassword());
                } catch (NoSuchAlgorithmException e) {
                    logger.error("[password encoding] not found algorithm", e);
                }
            }
            if (null != pCollaborateurAsJson.getPoste()) {
                tmpCollaborateur.setPoste(postService.get(pCollaborateurAsJson.getPoste().getId()));
            }
            if (null != pCollaborateurAsJson.getTelephone()) {
                tmpCollaborateur.setTelephone(pCollaborateurAsJson.getTelephone());
            }
            if (null != pCollaborateurAsJson.getManager()) {
                Collaborateur tmpManager = this.get(pCollaborateurAsJson.getManager().getId());

                if (null == tmpManager) {
                    throw new Exception("L'identifiant de ce manager est inconnu");
                }

                tmpCollaborateur.setManager(tmpManager);
            }
            if (null != pCollaborateurAsJson.getStatut()) {
                StatutCollaborateur newStatut = statutCollaborateurDAO.get(pCollaborateurAsJson.getStatut().getId());
                checkAndChangeStatut(tmpCollaborateur, newStatut);
            }
            if (null != pCollaborateurAsJson.getDateEntree()) {
                tmpCollaborateur.setDateEntree(pCollaborateurAsJson.getDateEntree());
            }
            if (null != pCollaborateurAsJson.getDateNaissance()) {
                tmpCollaborateur.setDateNaissance(pCollaborateurAsJson.getDateNaissance());
            }

            if (null != pCollaborateurAsJson.getAdressePostale()) {
                tmpCollaborateur.setAdressePostale(pCollaborateurAsJson.getAdressePostale());
            }

            if (null != pCollaborateurAsJson.getMailPerso()) {
                tmpCollaborateur.setMailPerso(pCollaborateurAsJson.getMailPerso());
            }

            if (null != pCollaborateurAsJson.getRaisonSociale()) {
                tmpCollaborateur.setRaisonSociale(raisonSocialeDAO.findByIdCollab(pCollaborateurAsJson.getId()));
                tmpCollaborateur.getRaisonSociale().setRaison_sociale(pCollaborateurAsJson.getRaisonSociale().getRaison_sociale());
            }

            if (null != pCollaborateurAsJson.getStatutGeneral()) {
                tmpCollaborateur.setStatutGeneral(pCollaborateurAsJson.getStatutGeneral());
            }
            tmpCollaborateur.setExclusAddvise(pCollaborateurAsJson.getExclusAddvise());

            if (null != pCollaborateurAsJson.getCivilite()) {
                tmpCollaborateur.setCivilite(civiliteService.get(pCollaborateurAsJson.getCivilite().getId()));
            } else {
                tmpCollaborateur.setCivilite(civiliteService.get(Civilite.CIVILITE_INCONNU_ID));
            }

            tmpCollaborateur.setAgency(Agency.getFromId(pCollaborateurAsJson.getAgency()));

            messageRetour = "1";
        } else {

            if (checkEmailExist(pCollaborateurAsJson.getMail())) {
                throw new Exception("Erreur dans l'adresse email: Adresse vide ou déjà existante en base.");
            }

            Collaborateur manager = null;
            if (null != pCollaborateurAsJson.getManager()) {
                manager = this.getDAO().get(pCollaborateurAsJson.getManager().getId());
            }

            Date dateEmbauche = new Date();
            if (null != pCollaborateurAsJson.getDateEntree()) {
                dateEmbauche = pCollaborateurAsJson.getDateEntree();
            }

            Date dateSortie = null;
            tmpCollaborateur = new Collaborateur(
                    pCollaborateurAsJson.getNom(),
                    pCollaborateurAsJson.getPrenom(),
                    statutCollaborateurDAO.get(pCollaborateurAsJson.getStatut().getId()),
                    pCollaborateurAsJson.getEnabled(),
                    pCollaborateurAsJson.getTelephone(),
                    pCollaborateurAsJson.getMail(),
                    "",
                    null,
                    manager,
                    postService.get(pCollaborateurAsJson.getPoste().getId()),
                    true,
                    // BAB 16/06/2016 [AMILNOTE 100] : on ajoute la date d'embauche, de sortie=null,
                    // et la date d'anniversaire du collab a sa creation.
                    0, dateEmbauche, dateSortie, pCollaborateurAsJson.getDateNaissance(),
                    pCollaborateurAsJson.getAdressePostale(),
                    pCollaborateurAsJson.getMailPerso(),
                    pCollaborateurAsJson.getStatutGeneral(),
                    null,
                    civiliteService.get(pCollaborateurAsJson.getCivilite().getId()),
                    Agency.getFromId(pCollaborateurAsJson.getAgency())
            );

            if (pCollaborateurAsJson.getRaisonSociale() != null) {
                RaisonSociale raisTemp = new RaisonSociale();
                raisTemp.setRaison_sociale(pCollaborateurAsJson.getRaisonSociale().getRaison_sociale());
                raisTemp.setCollaborateur(tmpCollaborateur);
                tmpCollaborateur.setRaisonSociale(raisTemp);
            }

            //String tmpPassword = pCollaborateurAsJson.getMail().split("@")[0];
            clearTmpPassword = _generate(11);
            String hashedTmpPassword = "";
            try {
                hashedTmpPassword = Encryption.encryptSHA(clearTmpPassword);
            } catch (NoSuchAlgorithmException e) {
                logger.error("[password encryption] algorithm not found", e);
            }

            try {
                //--- Encodage du mot de passe
                tmpCollaborateur.setPasswordAndEncode(hashedTmpPassword);
            } catch (NoSuchAlgorithmException e) {
                logger.error("[password encode] algorithm not found", e);
            }
            messageRetour = "2";
        }

        // --- Création de la session et de la transaction
        Session session = collaborateurDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        session.saveOrUpdate(tmpCollaborateur);

        if (pCollaborateurAsJson.getId() == null) {
            pCollaborateurAsJson.setId(getDAO().findUniqEntiteByProp(Collaborateur.PROP_MAIL, tmpCollaborateur.getMail()).getId());
        }
        session.flush();
        transaction.commit();

        //sauvegarde de la maj et commit raison sociale
        if (pCollaborateurAsJson.getRaisonSociale() != null) {
            tmpCollaborateur.getRaisonSociale().setCollaborateur(tmpCollaborateur);
            Session session2 = raisonSocialeDAO.getCurrentSession();
            Transaction transaction2 = session.beginTransaction();
            session2.saveOrUpdate(tmpCollaborateur.getRaisonSociale());

            if (pCollaborateurAsJson.getRaisonSociale() == null) {
                pCollaborateurAsJson.setRaisonSociale(tmpCollaborateur.getRaisonSociale().toJson());
            }

            session2.flush();
            transaction2.commit();
        }

        // dans le cas d'une création, envoi du mail avec le mot de passe
        if (messageRetour.equals("2")) {
            String env = "";
            if (!Boolean.valueOf(Parametrage.getContext("env.prod"))) env = "[DEV]";
            mailService.sendMailCreationCollab(pCollaborateurAsJson, clearTmpPassword, env);
        }

        return messageRetour;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollaborateurAsJson findByIdAbsence(Long pIdAbsence) throws Exception {

        Absence absence = absenceDAO.get(pIdAbsence);

        if (null == absence) {
            throw new Exception("Identifiant n°" + pIdAbsence + " inconnu pour les absences");
        }

        return absence.getCollaborateur().toJson();
    }

    /**
     * {@linkplain CollaborateurService#findByIdRA(Long)}
     */
    @Override
    public CollaborateurAsJson findByIdRA(Long pId) throws Exception {

        RapportActivites rapportActivites = rapportActivitesDAO.get(pId);

        if (null == rapportActivites) {
            throw new Exception("Identifiant n°" + pId + " inconnu pour les rapport d'activités");
        }

        return rapportActivites.getCollaborateur().toJson();
    }

    @Override
    public List<Collaborateur> findAllByStatutOrderByNom(String pStatut) {

        Objects.requireNonNull(pStatut);
        StatutCollaborateur statut = statutCollaborateurDAO.findUniqEntiteByProp(
                StatutCollaborateur.PROP_CODE, pStatut
        );

        Objects.requireNonNull(statut);
        List<Collaborateur> listCollabs = getDAO().findListEntitesByProp(
                Collaborateur.PROP_STATUT,
                statut,
                Collaborateur.PROP_NOM
        );

        if (listCollabs.isEmpty()) {
            return listCollabs;
        }

        return listCollabs.stream().filter(Collaborateur::isEnabled).collect(Collectors.toList());
    }

    /**
     * {@linkplain CollaborateurService#findAllByStatutOrderByNomAsJson(String)}
     */
    @Override
    public List<CollaborateurAsJson> findAllByStatutOrderByNomAsJson(String pStatut) throws Exception {
        //Recuperation d'un statut
        StatutCollaborateur statut =
                statutCollaborateurDAO.findUniqEntiteByProp(
                        StatutCollaborateur.PROP_CODE,
                        pStatut
                );

        if (null == statut) {
            throw new Exception("Statut (" + pStatut + ") inconnu.");
        }

        //Recuperation de la liste des managers triés par nom
        List<Collaborateur> listManagers =
                this.getDAO().findListEntitesByProp(
                        Collaborateur.PROP_STATUT,
                        statut,
                        Collaborateur.PROP_NOM
                );

        List<CollaborateurAsJson> tmpListManagerAsJson = new ArrayList<>();

        for (Collaborateur tmpManager : listManagers) {
            if (tmpManager.isEnabled()) {
                tmpListManagerAsJson.add(tmpManager.toJson());
            }
        }

        return tmpListManagerAsJson;
    }

    /**
     * Verifie si le changement de statut est possible et, si oui, effectue l'operation
     *
     * @param pCollaborateur le collaborateur à tester
     * @param pNewStatut     le statut
     * @return bool
     * @throws Exception exception
     */
    private Boolean checkAndChangeStatut(Collaborateur pCollaborateur, StatutCollaborateur pNewStatut) throws Exception {
        //Si il n'y a pas de changement de statut on fais rien
        if (null == pNewStatut || null == pCollaborateur) {
            throw new Exception("Le statut ou le collaborateur ne peuvent pas être vide");
        }

        if (pCollaborateur.getStatut() == pNewStatut) {
            return false;
        }

        //Statut actuel du collaborateur
        String oldStatut = pCollaborateur.getStatut().getCode();
        //Nouveau statut qu'on doit lui attribuer
        String newStatut = pNewStatut.getCode();

        //cas manager/admin --> collaborateur/DRH
        if (oldStatut.equals(StatutCollaborateur.STATUT_MANAGER) ||
                oldStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

            if (newStatut.equals(StatutCollaborateur.STATUT_COLLABORATEUR) ||
                    newStatut.equals(StatutCollaborateur.STATUT_DRH)) {

                if (pCollaborateur.getCollaborateurs().size() > 0) {
                    throw new Exception("Changement statut impossible: ce manager (" + pCollaborateur.getMail() + ") est toujours lié à des collaborateurs. ");
                }

            }

        }
        //cas collaborateur/DRH --> Manager/administrateur
        if (oldStatut.equals(StatutCollaborateur.STATUT_COLLABORATEUR) ||
                oldStatut.equals(StatutCollaborateur.STATUT_DRH)) {

            if (newStatut.equals(StatutCollaborateur.STATUT_MANAGER) ||
                    newStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

                //On récupère et supprime le collaborateur dans la liste de collaborateurs managés
                Collaborateur manager = pCollaborateur.getManager();

                //On annule le lien collaborateur --> manager
                if (null != manager) {
                    manager.getCollaborateurs().remove(pCollaborateur);
                }

                pCollaborateur.setManager(null);

            }

        }
        //cas admin --> Autre
        if (oldStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

            if (!newStatut.equals(StatutCollaborateur.STATUT_ADMIN)) {

                StatutCollaborateur statutAdmin =
                        statutCollaborateurDAO.findUniqEntiteByProp(
                                StatutCollaborateur.PROP_CODE,
                                StatutCollaborateur.STATUT_ADMIN);

                List<Collaborateur> listAdmin =
                        this.getDAO().findListEntitesByProp(
                                Collaborateur.PROP_STATUT,
                                statutAdmin);

                if (listAdmin.size() < 2) {
                    throw new Exception(
                            "Changement statut impossible : il doit y avoir au minimum un adiminstrateur dans la base de données"
                    );
                }

            }
        }

        //Le changement de statut est possible
        pCollaborateur.setStatut(pNewStatut);

        return true;
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderWithDisabledByNomAscAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllOrderWithDisabledByNomAscAsJson() {

        List<Collaborateur> tmpListCollaborateur = collaborateurDAO.findCollaborateurs(true, true);
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>(tmpListCollaborateur.size());

        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {

            tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());

        }
        return tmpListCollaborateurAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collaborateur findById(Long aIdCollaborateur) {
        return collaborateurDAO.get(aIdCollaborateur);
    }

    @Override
    public List<ModuleAddvise> getAllModuleAddvise() {
        List<ModuleAddvise> listeModule = moduleAddviseDAO.findAll();
        listeModule.sort((module1, module2) -> {
            String[] codeM1 = module1.getCodeModuleAddvise().split("M");
            String[] codeM2 = module2.getCodeModuleAddvise().split("M");
            if (codeM1.length > 1 && codeM2.length > 1) {
                if (Integer.parseInt(codeM1[1]) > Integer.parseInt(codeM2[1])) {
                    return 5;
                } else if (Integer.parseInt(codeM1[1]) < Integer.parseInt(codeM2[1])) {
                    return -5;
                } else {
                    return 0;
                }
            } else {
                return module1.getCodeModuleAddvise().compareTo(module2.getCodeModuleAddvise());
            }
        });
        return listeModule;
    }

    @Override
    public boolean isCadre(Collaborateur collaborateur) {
        return "C".equals(collaborateur.getStatutGeneral());
    }

    /**
     * {@linkplain CollaborateurService#findAllManagersAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllManagersAsJson() {
        List<CollaborateurAsJson> managersAsJsons = new ArrayList<>();
        StatutCollaborateur statutManager = statutCollaborateurDAO.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_MANAGER);
        Poste posteManager = postService.getPosteByCode(Poste.POSTE_MANAGER);
        List<Collaborateur> managers = collaborateurDAO.findAllManagers(statutManager, posteManager);

        for (Collaborateur collaborateur : managers) {
            if (collaborateur.isEnabled()) {
                managersAsJsons.add(collaborateur.toJson());
            }
        }

        return managersAsJsons;
    }

    /**
     * {@linkplain CollaborateurService#findAllDirectionAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllDirectionAsJson() {
        List<CollaborateurAsJson> directorsAsJsons = new ArrayList<>();
        StatutCollaborateur statutManager = statutCollaborateurDAO.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        Poste posteManager = postService.getPosteByCode(Poste.POSTE_DIRECTEUR);
        List<Collaborateur> managers = collaborateurDAO.findAllManagers(statutManager, posteManager);

        for (Collaborateur collaborateur : managers) {
            if (collaborateur.isEnabled()) {
                directorsAsJsons.add(collaborateur.toJson());
            }
        }

        return directorsAsJsons;
    }

    /**
     * {@linkplain CollaborateurService#findAllResponsablesTechniqueAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllResponsablesTechniqueAsJson() {
        List<CollaborateurAsJson> responsablesTechniqueAsJsons = new ArrayList<>();
        StatutCollaborateur statutResponsableTechnique = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_RESPONSABLE_TECHNIQUE);
        List<Collaborateur> responsablesTechnique = collaborateurDAO.findAllResponsablesTechnique(statutResponsableTechnique);

        for (Collaborateur collaborateur : responsablesTechnique) {
            if (collaborateur.isEnabled()) {
                responsablesTechniqueAsJsons.add(collaborateur.toJson());
            }
        }

        return responsablesTechniqueAsJsons;
    }

    /**
     * {@linkplain CollaborateurService#findAllChefDeProjetAsJson()}
     */
    @Override
    public List<CollaborateurAsJson> findAllChefDeProjetAsJson() {
        List<CollaborateurAsJson> chefDeProjetAsJson = new ArrayList<>();
        StatutCollaborateur statutChefDeProjet = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_CHEF_DE_PROJET);
        List<Collaborateur> chefDeProjet = collaborateurDAO.findAllChefDeProjet(statutChefDeProjet);

        for (Collaborateur collaborateur : chefDeProjet) {
            if (collaborateur.isEnabled()) {
                chefDeProjetAsJson.add(collaborateur.toJson());
            }
        }
        return chefDeProjetAsJson;
    }


    /**
     * {@linkplain CollaborateurService#isResponsableTechnique(Collaborateur)}
     */
    @Override
    public boolean isResponsableTechnique(Collaborateur currentUser) {
        StatutCollaborateur statutResponsableTechnique = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_RESPONSABLE_TECHNIQUE);
        return statutResponsableTechnique.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaborateurService#isProjectManager(Collaborateur)}
     */
    @Override
    public boolean isProjectManager(Collaborateur currentUser) {
        StatutCollaborateur statutChefDeProjet = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_CHEF_DE_PROJET);
        return statutChefDeProjet.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaborateurService#findAllOrderByNomAscAsJsonWithoutADMINAndDir()}
     */
    @Override
    public List<CollaborateurAsJson> findAllOrderByNomAscAsJsonWithoutADMINAndDir() {

        List<Collaborateur> tmpListCollaborateur = findAllOrderByNomAscWithoutADMIN();
        List<CollaborateurAsJson> tmpListCollaborateurAsJson = new ArrayList<>();

        for (Collaborateur tmpCollaborateur : tmpListCollaborateur) {
            if (!isDirector(tmpCollaborateur) && !isAdminManager(tmpCollaborateur))
                tmpListCollaborateurAsJson.add(tmpCollaborateur.toJson());
        }
        return tmpListCollaborateurAsJson;
    }

    /**
     * {@linkplain CollaborateurService#isDirector(Collaborateur)}
     */
    @Override
    public boolean isDirector(Collaborateur currentUser) {
        StatutCollaborateur statutDirecteur = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DIRECTION);
        return statutDirecteur.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaborateurService#isDRH(Collaborateur)}
     */
    @Override
    public boolean isDRH(Collaborateur currentUser) {
        StatutCollaborateur statutDRH = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_DRH);
        return statutDRH.equals(currentUser.getStatut());
    }

    /**
     * Vérifie si le collaborateur est l'Admin Manager en fonction de son nom et prénom
     *
     * @param collaborateur collaborateur à tester
     * @return true or false
     */
    private boolean isAdminManager(Collaborateur collaborateur) {
        return "MANAGER".equalsIgnoreCase(collaborateur.getNom()) && "ADMIN".equalsIgnoreCase(collaborateur.getPrenom());
    }

    @Override
    public boolean hasMissionById(Collaborateur currentUser, Long missionId) {

        List<Long> missionIds = currentUser
                .getMissions().stream().map(Mission::getId).collect(Collectors.toList());

        return missionIds.contains(missionId);
    }

    private List<CollaborateurAsJson> convertCollaboratorListToAsJsonList(List<Collaborateur> listCollaborators) {
        List<CollaborateurAsJson> listCollaboratorsAsJson = new ArrayList<>();
        for (Collaborateur collaborator : listCollaborators) {
            listCollaboratorsAsJson.add(collaborator.toJson());
        }

        return listCollaboratorsAsJson;
    }


    /**
     * creation de la liste des manager
     *
     * @return a list of {@link CollaborateurAsJson}
     */
    @Override
    public List<CollaborateurAsJson> addListManagers() {
        List<CollaborateurAsJson> listeManagers = new ArrayList<>();
        try {
            listeManagers.addAll(findAllManagersAsJson());
            listeManagers.addAll(findAllDirectionAsJson());
            listeManagers.addAll(findAllResponsablesTechniqueAsJson());
            listeManagers.addAll(findAllChefDeProjetAsJson());
        } catch (Exception e) {
            logger.error("[add managers]", e);
        }

        return listeManagers;
    }

    /**
     * {@linkplain CollaborateurService#findCollaboratorsByStatus(StatutCollaborateur...)}
     */
    @Override
    public List<Collaborateur> findCollaboratorsByStatus(StatutCollaborateur... collaboratorsStatus) {
        return findBySearchCriterias(
                Collaborateur.PROP_NOM,
                true,
                new SearchCriteria<>(Collaborateur.PROP_STATUT, IN, collaboratorsStatus),
                new SearchCriteria<>(Collaborateur.PROP_ENABLED, EQUAL, true)
        );
    }

    /**
     * {@linkplain CollaborateurService#findAllByStatusCode(String...)}
     */
    @Override
    public List<Collaborateur> findAllByStatusCode(String... collaboratorsStatusCode) {
        List<StatutCollaborateur> statuses = statutCollaborateurService
                .findCollaboratorsStatusByCodes(collaboratorsStatusCode);
        return findCollaboratorsByStatus(statuses.toArray(new StatutCollaborateur[0]));
    }

    /**
     * {@linkplain CollaborateurService#isSubcontractor(Collaborateur)}
     */
    @Override
    public boolean isSubcontractor(Collaborateur currentUser) {
        return currentUser.getStatut().getCode().equals(STATUT_SOUS_TRAITANT);
    }

    /**
     * {@linkplain CollaborateurService#changeCollaboratorActivation(Long)}
     */
    @Override
    public boolean changeCollaboratorActivation(Long collaboratorId) {

        if (null == collaboratorId) {
            return false;
        }

        Session session = collaborateurDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Collaborateur collaborator = collaborateurDAO.get(collaboratorId);
        collaborator.setEnabled(!collaborator.isEnabled());
        session.saveOrUpdate(collaborator);

        session.flush();
        transaction.commit();

        return true;
    }

    /**
     * Retreive the list of collaborators by their status and activated or not
     * {@linkplain CollaborateurService#retrieveCollaboratorByStatus(boolean, StatutCollaborateur...)}
     */
    @Override
    public List<Collaborateur> retrieveCollaboratorByStatus(boolean enabled, StatutCollaborateur... status) {
        return collaborateurDAO.findCollaboratorByStatus(enabled, status);
    }

    @Override
    public List<CollaborateurAsJson> retrieveCollaboratorsByAgency(Agency agency){
        return convertCollaboratorListToAsJsonList(collaborateurDAO.findCollaboratorsEnabledByAgency(agency));
    }

    @Override
    public List<CollaborateurAsJson> retrieveCollaboratorsByManagerAndByAgency(Collaborateur manager, Agency agency){
        return convertCollaboratorListToAsJsonList(collaborateurDAO.findCollaboratorsByManagerAndByAgency(manager, agency));
    }

    /**
     * Gets current collaborateur.
     *
     * @return the current collaborateur
     */
    @Override
    public Collaborateur getCurrentCollaborateur() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String mail = userDetails.getUsername();
        return findByMail(mail);
    }

    /**
     * {@linkplain CollaborateurService#isManager(Collaborateur)}
     */
    @Override
    public boolean isManager(Collaborateur currentUser) {
        StatutCollaborateur statutManager = statutCollaborateurService.findStatutCollaborateurByCode(StatutCollaborateur.STATUT_MANAGER);
        return statutManager.equals(currentUser.getStatut());
    }

    /**
     * {@linkplain CollaborateurService#retreiveCollaborators()}
     */
    @Override
    public ImmutablePair<List<CollaborateurAsJson>, List<CollaborateurAsJson>> retreiveCollaborators() {

        Collaborateur currentCollaborator = getCurrentCollaborateur();

        List<CollaborateurAsJson> listCollaboratorsAsJsons;// All collaborators
        List<CollaborateurAsJson> listCollaboratorsAsJsonsForSelect;// Collaborator dependent on a manager

        if (isManager(getCurrentCollaborateur())) {
            listCollaboratorsAsJsons = retrieveCollaboratorsByManagerAndByAgency(currentCollaborator, currentCollaborator.getAgency());
        } else if (isDirector(getCurrentCollaborateur())
                || isProjectManager(getCurrentCollaborateur())
                || isDRH(getCurrentCollaborateur())) {
            listCollaboratorsAsJsons = findAllOrderByNomAscAsJsonWithoutADMINAndDir();
        } else {
            listCollaboratorsAsJsons = retrieveCollaboratorsByAgency(currentCollaborator.getAgency());
        }
        listCollaboratorsAsJsonsForSelect = listCollaboratorsAsJsons;

        return new ImmutablePair<>(listCollaboratorsAsJsons, listCollaboratorsAsJsonsForSelect);
    }
}
