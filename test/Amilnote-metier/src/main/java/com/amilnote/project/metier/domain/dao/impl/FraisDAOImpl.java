/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.dao.FraisDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Pair;
import eu.medsea.mimeutil.MimeUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * The type Frais dao.
 */
@Repository("fraisDAO")
@Component
public class FraisDAOImpl extends AbstractDAOImpl<Frais> implements FraisDAO {

    private static final Logger logger = LogManager.getLogger(Frais.class);

    @Autowired
    private EtatDAO etatDAO;

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Frais> getReferenceClass() {
        return Frais.class;
    }

    /**
     * {@linkplain FraisDAO#findByMissionCriteria(Mission, int, int)}
     */
    @Override
    public List<Object[]> findByMissionCriteria(Mission pMission, int pMois, int pAnnee) {
        DateTime date = new DateTime();

        DateTime dateDebut = new DateTime(date.toDate());
        DateTime dateFin = new DateTime(date.toDate());
        dateDebut = dateDebut.dayOfMonth().withMinimumValue()
                .withHourOfDay(0)
                .withMinuteOfHour(0);
        dateFin = dateFin.dayOfMonth().withMaximumValue()
                .withHourOfDay(23)
                .withMinuteOfHour(59);

        Criteria criteria = getCriteria();
        criteria.createCriteria("linkEvenementTimesheet", "let");
        criteria.createCriteria("let.mission", "m");
        criteria.createCriteria("typeFrais", "tf");

        criteria.add(Restrictions.eq("let." + LinkEvenementTimesheet.PROP_MISSION, pMission));
        criteria.add(Restrictions.sqlRestriction("MONTH(" + LinkEvenementTimesheet.PROP_DATE + ")=" + pMois));
        criteria.add(Restrictions.sqlRestriction("YEAR(" + LinkEvenementTimesheet.PROP_DATE + ")=" + pAnnee));

        criteria.setProjection(Projections.projectionList()
                .add(Projections.property(Frais.PROP_ID))
                .add(Projections.property("m." + Mission.PROP_MISSION))
                .add(Projections.property("let." + LinkEvenementTimesheet.PROP_DATE))
                .add(Projections.property("tf." + TypeFrais.PROP_TYPE_FRAIS))
                .add(Projections.property(Frais.PROP_MONTANT))
                .add(Projections.property(Frais.PROP_COMMENTAIRE))
                .add(Projections.property(Frais.PROP_NOMBREKM))
                .add(Projections.property(Frais.PROP_TYPEAVANCE)));

        criteria.addOrder(Order.desc("let." + LinkEvenementTimesheet.PROP_DATE));

        return (List<Object[]>) criteria.list();
    }

    /**
     * {@linkplain FraisDAO#findByMissionCriteria(Mission, int, int)}
     */
    @Override
    public List<Object[]> findByMission(Mission pMission, int pMois, int pAnnee) {
//        Session lSession = currentSession();
//        Query lListResults = lSession.createQuery(MessageFormat.format(sqlAllFraisMonthMission, pMission.getId().toString().replace(" ",""), pMois,
//            Integer.toString(pAnnee, 10)));
//        return lListResults.list();
        DateTime date = new DateTime();

        DateTime dateDebut = new DateTime(date.toDate());
        DateTime dateFin = new DateTime(date.toDate());
        dateDebut = dateDebut.dayOfMonth().withMinimumValue()
                .withHourOfDay(0)
                .withMinuteOfHour(0);
        dateFin = dateFin.dayOfMonth().withMaximumValue()
                .withHourOfDay(23)
                .withMinuteOfHour(59);

        Criteria criteria = getCriteria();
        criteria.createCriteria("linkEvenementTimesheet", "let");
        criteria.createCriteria("let.mission", "m");
        criteria.createCriteria("typeFrais", "tf");

        criteria.add(Restrictions.eq("let." + LinkEvenementTimesheet.PROP_MISSION, pMission));
        criteria.add(Restrictions.sqlRestriction("MONTH(" + LinkEvenementTimesheet.PROP_DATE + ")=" + pMois));
        criteria.add(Restrictions.sqlRestriction("YEAR(" + LinkEvenementTimesheet.PROP_DATE + ")=" + pAnnee));

        criteria.setProjection(Projections.projectionList()
                .add(Projections.property(Frais.PROP_ID))
                .add(Projections.property("m." + Mission.PROP_MISSION))
                .add(Projections.property("let." + LinkEvenementTimesheet.PROP_DATE))
                .add(Projections.property("tf." + TypeFrais.PROP_TYPE_FRAIS))
                .add(Projections.property(Frais.PROP_MONTANT))
                .add(Projections.property(Frais.PROP_COMMENTAIRE))
                .add(Projections.min(Frais.PROP_NATIONAL)) // vaut 0
                .add(Projections.property(Frais.PROP_NATIONAL))
                .add(Projections.property(Frais.PROP_NOMBREKM))
                .add(Projections.property(Frais.PROP_TYPEAVANCE))
                .add(Projections.property(Frais.PROP_FRAIS_FORFAIT))
        );

        criteria.addOrder(Order.desc("let." + LinkEvenementTimesheet.PROP_DATE));

        return (List<Object[]>) criteria.list();
    }

    /**
     * {@linkplain FraisDAO#findByMissionCriteria(Mission, int, int)}
     */
    @Override
    public List<Object[]> findByMissionCriteria(Mission pMission, DateTime date) {
        DateTime dateDebut = new DateTime(date.toDate());
        DateTime dateFin = new DateTime(date.toDate());
        dateDebut = dateDebut.dayOfMonth().withMinimumValue()
                .withHourOfDay(0)
                .withMinuteOfHour(0);
        dateFin = dateFin.dayOfMonth().withMaximumValue()
                .withHourOfDay(23)
                .withMinuteOfHour(59);

        Criteria criteria = getCriteria();
        criteria.createCriteria("linkEvenementTimesheet", "let");
        criteria.createCriteria("let.mission", "m");
        criteria.createCriteria("typeFrais", "tf");


        criteria.add(Restrictions.eq("let." + LinkEvenementTimesheet.PROP_MISSION, pMission));
        criteria.add(Restrictions.isNull("let." + LinkEvenementTimesheet.PROP_ABSENCE));
        criteria.add(Restrictions.between("let." + LinkEvenementTimesheet.PROP_DATE, dateDebut.toDate(), dateFin.toDate()));

        criteria.setProjection(Projections.projectionList()
                .add(Projections.property(Frais.PROP_ID))
                .add(Projections.property("m." + Mission.PROP_MISSION))
                .add(Projections.property("let." + LinkEvenementTimesheet.PROP_DATE))
                .add(Projections.property("tf." + TypeFrais.PROP_TYPE_FRAIS))
                .add(Projections.property(Frais.PROP_MONTANT))
                .add(Projections.property(Frais.PROP_COMMENTAIRE))
                .add(Projections.property(Frais.PROP_NOMBREKM))
                .add(Projections.property(Frais.PROP_TYPEAVANCE)));

        criteria.addOrder(Order.desc("let." + LinkEvenementTimesheet.PROP_DATE));

        return (List<Object[]>) criteria.list();
    }

    /**
     * {@linkplain FraisDAO#deleteFrais(List)}
     */
    @Override
    public String deleteFrais(List<Long> pListIdFrais) {
        // récupération de la session
        Session lSession = getSessionFactory().getCurrentSession();

        // début de la transaction pour supprimer un/plusieurs frais
        try {
            Transaction lTransaction = lSession.beginTransaction();
            // récupération de chaque frais et suppression
            for (Long lId : pListIdFrais) {
                Frais lFraisToDelete = (Frais) lSession.get(Frais.class, lId);
                lSession.delete(lFraisToDelete);
                lSession.flush();
            }
            lTransaction.commit();
        } catch (HibernateException e) {
            logger.debug(e.getMessage());
            return e.getMessage();
        }

        //FIXME WTF!!??! retourner du html dans un dao??!!
        return "<h4>Suppression(s) effectuée(s) !</h4> Le(s) frais a/ont été supprimé(s) avec succès.</p>";
    }

    /**
     * {@linkplain FraisDAO#modifyFrais(Long, BigDecimal, LinkFraisTFC, LinkEvenementTimesheet, String, String, int)}
     */
    @Override
    public String modifyFrais(Long pIdFrais, BigDecimal pMontant, LinkFraisTFC pLinkFraisTFC,
                              LinkEvenementTimesheet pLinkEvtTS, String pCommentaire, String pJustif, int pAvance) {

        //FIXME methode qui ne fait absolument rien
        //FIXME WTF!!??! retourner du html dans un dao??!!
        return "<h4>Modification effectuée !</h4><p>Le frais a été modifié avec succès.</p>";
    }

    /**
     * {@linkplain FraisDAO#getPictureForFrais(Long)}
     */
    @Override
    public Pair<String, InputStream> getPictureForFrais(Long pIdFrais) {
        // récupération de la session
        Session lSession = currentSession();

        // liste des éléments à renvoyer
        ProjectionList lProjectionlist = Projections.projectionList();
        lProjectionlist.add(Projections.property(Frais.PROP_JUSTIF), "justif");

        // récupération du frais
        Criteria lCritImg = lSession.createCriteria(Frais.class);
        lCritImg.add(Restrictions.eq(Frais.PROP_ID, pIdFrais));
        lCritImg.setProjection(lProjectionlist);
        InputStream lIs = new BufferedInputStream(new ByteArrayInputStream((byte[]) lCritImg.list().get(0)));
        MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
        Collection<?> lMimeTypes = MimeUtil.getMimeTypes(lIs);
        String lMime = lMimeTypes.iterator().next().toString();

        return new Pair<>(lMime, lIs);
    }

    /**
     * {@linkplain FraisDAO#findJustifsByMission(Mission, DateTime)}
     */
    @Override
    public List<byte[]> findJustifsByMission(Mission pMission, DateTime pDate) {

        // liste des éléments à renvoyer
        ProjectionList lProjectionlist = Projections.projectionList();
        lProjectionlist.add(Projections.property("frais.justif"), "justif");

        Session lSession = currentSession();
        Criteria lCritJustifs = lSession.createCriteria(Frais.class, "frais");
        lCritJustifs.createAlias("frais.linkEvenementTimesheet", "evenement");
        lCritJustifs.add(Restrictions.eq("evenement.mission", pMission));
        lCritJustifs.add(Restrictions.isNotNull("frais.justif"));
        lCritJustifs.add(Restrictions.ge("evenement.date", pDate.toDate()));

        lCritJustifs.setProjection(lProjectionlist);
        return (List<byte[]>) lCritJustifs.list();
    }

    /**
     * {@linkplain FraisDAO#modifyEtatFrais(List)}
     */
    @Override
    public String modifyEtatFrais(List<Frais> pListFrais) {

        Session lSession = currentSession();
        // récupération de l'objet etat correspondant à l'id
        Etat lEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_VALIDE_CODE);

        // début de la transaction pour supprimer un/plusieurs frais
        try {
            Transaction lTransaction = lSession.beginTransaction();
            // récupération de chaque frais et suppression
            for (Frais lFrais : pListFrais) {
                Frais lFraisToModify = (Frais) lSession.get(Frais.class, lFrais.getId());
                lFraisToModify.setEtat(lEtat);
                lSession.flush();
            }
            lTransaction.commit();
        } catch (HibernateException e) {
            logger.debug(e.getMessage());
            return e.getMessage();
        }

        return "état des frais modifié : soumis";
    }

    /**
     * {@linkplain FraisDAO#getAllFraisByMonth(DateTime)}
     */
    @Override
    public List<Frais> getAllFraisByMonth(DateTime pDateVoulue) {
        // --- Création du criteria sur la table Frais
        Criteria criteria = currentSession().createCriteria(Frais.class, Constantes.ALIAS_FRAIS);

        // --- Création d'alias pointant sur les missions et les collaborateurs
        criteria.createAlias(Constantes.ALIAS_FRAIS + "." + Frais.PROP_LINKEVTTS, Constantes.ALIAS_EVENT);
        criteria.createAlias(Constantes.ALIAS_FRAIS + "." + Frais.PROP_ETAT, Constantes.ALIAS_ETAT);
        criteria.createAlias(Constantes.ALIAS_EVENT + "." + LinkEvenementTimesheet.PROP_COLLABORATEUR, Constantes.ALIAS_COLLAB);

        // --- Restriction sur la date voulue

        Date dateDebut = pDateVoulue.dayOfMonth().withMinimumValue().toDate();

        Date dateFin;

        if (pDateVoulue.getMonthOfYear() == 12) {
            dateFin = pDateVoulue.withMonthOfYear(01).withYear(pDateVoulue.getYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();

        } else {
            dateFin = pDateVoulue.withMonthOfYear(pDateVoulue.getMonthOfYear() + 1)
                    .dayOfMonth().withMinimumValue().toDate();
        }

        criteria.add(Restrictions.ge(Constantes.ALIAS_EVENT + "." + LinkEvenementTimesheet.PROP_DATE, dateDebut));
        criteria.add(Restrictions.lt(Constantes.ALIAS_EVENT + "." + LinkEvenementTimesheet.PROP_DATE, dateFin));

        criteria.add(Restrictions.or(Restrictions.eq(Constantes.ALIAS_ETAT + "." + Etat.PROP_ID, 2), Restrictions.eq(Constantes.ALIAS_ETAT + "." + Etat.PROP_ID, 3), Restrictions.eq(Constantes.ALIAS_ETAT + "." + Etat.PROP_ID, 7)));

        criteria.add(Restrictions.isNotNull(Constantes.ALIAS_FRAIS + "." + Frais.PROP_MONTANT));

        return criteria.list();
    }

    /**
     * {@linkplain FraisDAO#getFraisByCollaborator(Collaborator)}
     */
    @Override
    public List<Frais> getFraisByCollaborator(Collaborator collaborator) {
        // --- Récupération de l'état Brouillon
        Etat etatBR = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);

        // --- Création du criteria sur la table Frais
        Criteria lListFrais = currentSession().createCriteria(Frais.class, "frais");

        // --- Création d'alias pointant sur les missions et les absences
        lListFrais.createAlias("frais." + Frais.PROP_LINKEVTTS, "event");
        lListFrais.createAlias("frais." + Frais.PROP_LINKEVTTS + "."
                + LinkEvenementTimesheet.PROP_MISSION, "aliasMission");
        lListFrais.createAlias("frais." + Frais.PROP_LINKEVTTS + "."
                + LinkEvenementTimesheet.PROP_ABSENCE, "aliasAbsence", JoinType.LEFT_OUTER_JOIN);

        // --- Restriction sur le collaborateur voulu
        lListFrais.add(Restrictions.eq("aliasMission." + Mission.PROP_COLLABORATEUR, collaborator));

        // --- Restriction sur l'absence nulle ou à l'état brouillon
        Disjunction absenceBrOrNull = Restrictions.disjunction();
        absenceBrOrNull.add(Restrictions.eq("aliasAbsence." + Absence.PROP_ETAT, etatBR));
        absenceBrOrNull.add(Restrictions.isNull("aliasAbsence." + Absence.PROP_ETAT));
        lListFrais.add(absenceBrOrNull);

        return lListFrais.list();

    }

    /**
     * {@inheritDoc}
     */
    public int getAutoIncrement(String nomTable) {
        Query query = currentSession().createSQLQuery("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE table_name = :nomTable");
        query.setString("nomTable", nomTable);
        BigInteger id = (BigInteger) query.uniqueResult();
        return id.intValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean updateFraisEnCours(Collaborator collaborator, DateTime pDateRAVoulue, Etat etat) {
        DateTime dateFirstDay = pDateRAVoulue.dayOfMonth().withMinimumValue().toDateTime();
        DateTime dateLastDay = pDateRAVoulue.dayOfMonth().withMaximumValue().toDateTime();

        Session session = getCurrentSession();
//        Transaction transaction = session.beginTransaction();

        String requete = "select id from Frais " +
                "where id_link_evenement_timesheet in ( " +
                "select id from LinkEvenementTimesheet " +
                "where id_mission in ( " +
                "select id from Mission " +
                "where id_collaborateur in (	 " +
                "select id from Collaborator " +
                "where id=" + collaborator.getId() + ")) " +
                "and date<='" + dateLastDay + "' " +
                "and date>='" + dateFirstDay + "') " +
                "and id_type_frais in ( " +
                "select id from TypeFrais " +
                "where code!='ASF')";
        Query query = session.createQuery(requete);
//        transaction.commit();
        try {
            List result = query.list();
            int size = result.size();
            int i = 0;
            while (i < size) {
//                transaction = session.beginTransaction();
                Frais tmpFrais = this.findUniqEntiteByProp(Frais.PROP_ID, result.get(i));
                tmpFrais.setEtat(etat);

                // --- Sauvegarde et Commit de la mise à jour
                session.saveOrUpdate(tmpFrais);
//                transaction.commit();
                session.flush();
                i++;
            }
        } catch (HibernateException e) {
            return false;
        }

        return true;
    }


}
