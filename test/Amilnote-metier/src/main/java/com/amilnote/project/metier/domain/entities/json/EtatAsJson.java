/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Etat;

/**
 * The type Etat as json.
 */
public class EtatAsJson {

    private int id;
    private String etat;
    private String code;

    /**
     * Instantiates a new Etat as json.
     *
     * @param pEtat the p etat
     */
    public EtatAsJson(Etat pEtat) {
        this.setId(pEtat.getId());
        this.setEtat(pEtat.getEtat());
        this.setCode(pEtat.getCode());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the id to set
     */
    public void setId(int pId) {
        id = pId;
    }

    /**
     * Gets etat.
     *
     * @return the etat
     */
    public String getEtat() {
        return etat;
    }

    /**
     * Sets etat.
     *
     * @param pEtat the etat to set
     */
    public void setEtat(String pEtat) {
        etat = pEtat;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the code to set
     */
    public void setCode(String pCode) {
        code = pCode;
    }


}
