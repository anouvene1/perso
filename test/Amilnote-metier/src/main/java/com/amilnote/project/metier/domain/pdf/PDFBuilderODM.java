/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.pdf;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.ClientAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkPerimetreMissionAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.concatenateObjectsToString;

/**
 * The type Pdf builder odm.
 */
@Service("PDFBuilderODM")
public class PDFBuilderODM extends PDFBuilder {

    private static final Logger logger = LogManager.getLogger(PDFBuilderRA.class);

    // PDF HEADERS
    private static final String PLANIFICATION_HEADER = "PLANIFICATION";
    private static final String MISSION_COSTS_HEADER = "FRAIS DE MISSION";
    private static final String SUBCONTRACTOR_HEADER = "SOUS-TRAITANT";
    private static final String PRESCRIPTION_HEADER = "PRESCRIPTION";
    private static final String MISSION_ORDER_HEADER = "ORDRE DE MISSION";
    private static final String AMILTONE_CUSTOMER_HEADER = "CLIENT AMILTONE";
    private static final String CUSTOMER_HEADER = "CLIENT";
    private static final String MISSION_DETAILS_HEADER = "DESCRIPTIF MISSION";
    private static final String FORMATION_HEADER = "FORMATION";
    private static final String VISA_HEADER = "VISA";
    private static final String DIRECTION_HEADER = "DIRECTION";

    // PDF LABELS
    private static final String COMPANY_LABEL = "Société: ";
    private static final String ADDRESS_LABEL = "Adresse: ";
    private static final String CUSTOMER_MANAGER_LABEL = "Resp. Client: ";
    private static final String NAME_LABEL = "Nom: ";
    private static final String FIRST_NAME_LABEL = "Prénom: ";
    private static final String FIRST_LAST_NAMES_LABEL = "Nom, Prénom : ";
    private static final String PHONE_NUMBER_LABEL = "Téléphone: ";
    private static final String MISSION_NUMBER_LABEL = "Mission N°: ";
    private static final String COLLABORATEUR_FOLLOWUP_LABEL = "Suivi collaborateur: ";
    private static final String WEEKLY_DURATION_LABEL = "Durée hebdomadaire: ";
    private static final String START_DATE_LABEL = "Date début: ";
    private static final String DOCUMENT_VALIDITY_END_DATE_LABEL = "Date fin de validité du document: ";

    // PDF DETAILS
    private static final String BUSINESS_TRIP_REIMBURSEMENT_DETAILS = "- Remboursés sur justificatif si déplacement hors du site du client";
    private static final String DIRECTION_VALIDATION_DETAILS = "Validé par la direction";
    private static final String MEAL_PLAN_LABEL = "Forfait repas : ";
    private static final String PHONE_PLAN_LABEL = "Forfait téléphone : ";
    private static final String HOUSING_PLAN_LABEL = "Forfait logement : ";
    private static final String BUSINESS_TRIP_PLAN_LABEL = "Forfait déplacement : ";
    private static final String TIME_PLANIFICATION_INTERNAL_MISSIONS = "* Vous devrez indiquer dans l’outil JIRA, 8h/jour travaillé, qui correspondent à un travail effectif de 7h18/jour travaillé.";

    // TIME UNITS
    private static final String DATE_LABEL = "Date : ";
    private static final String HOURS_LOWERCASE_UNIT = "heures";

    private static final String AMILTONE_NAME = "Amiltone";

    // SECURITY RULES
    private static final String PERIMETRE_COM = "Lors des déplacements professionnels, vous devrez respecter les " +
            "dispositions du code de la route.\n" +
            "Ces dispositions impliquent notamment le respect " +
            "des impératifs de limitation de vitesse.\n" +
            "Au-delà des prescriptions légales et règlementaires relatives à la législation " +
            "routière, il est rappelé à tous les salariés la nécessité de prendre une pause toutes " +
            "les deux heures et plus si nécessaire, dès les premiers signes de fatigue.\n" +
            "L'entreprise vous laisse la possibilité, si vous le jugez nécessaire, de dormir à " +
            "l'hôtel en cas de fatigue et de longues heures de trajet.";


    private static final String PERIMETRE_INDUS = "Dans le cadre d’intervention sur site industriel " +
            "(usine, laboratoire, …), vous avez :\n" +
            "- L’obligation de porter vos Equipements de Protection Individuelle, adaptés au site et " +
            "aux travaux : chaussures de sécurité (à plein temps), gants anti-coupure " +
            "(lors d’utilisation et manipulation d’objets ou outils coupants), protections auditives " +
            "(environnement bruyant), lunettes de protection (si risque de projection), casque de " +
            "protection (si risque de chute d’objet) etc.\n" +
            "- Et l’interdiction de manipuler des engins ou d’intervenir sur des systèmes " +
            "électriques sauf habilitations (électrique, cariste etc.).";

    private static final String PERIMETRE_INFO_ADMIN = "Dans le cadre de votre métier, le travail sur écran ne présente pas " +
            "de risque immédiat mais il nécessite d’avoir des bons réflexes :\n" +
            "- Adapter l’ergonomie de votre poste de travail : réglage de l’écran, du siège etc.\n" +
            "- Prévenir la fatigue visuelle en réglant la luminosité de votre écran et en éliminant " +
            "les reflets (orientation de l’écran par rapport aux sources lumineuses)\n" +
            "- Organiser votre temps de travail en tenant compte de la variabilité de votre " +
            "environnement et en faisant des pauses régulières\n" +
            "- Interdiction d’intervenir sur des systèmes électriques sauf habilitations";

    private static final String SECURITY_RULES = "Règles de sécurité : ";

    private static final String LEGAL_NOTES = "Ce document est la propriété d'Amiltone SAS et ne peut être reproduit ou communiqué sans autorisation écrite.";

    @Autowired
    private ForfaitService forfaitService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private TypeMissionService typeMissionService;

    @Autowired
    private FileService fileService;

    /**
     * Create the mission order pdf
     *
     * @param collaborator  the employee who is related to the mission
     * @param missionAsJson the mission related
     * @return the file name of the generated pdf file
     * @throws IOException       the io exception
     * @throws DocumentException the document exception
     */
    public String createPDFODM(Collaborator collaborator, MissionAsJson missionAsJson) throws IOException, DocumentException {

        logger.debug("-------------------------------------------------------------- creation pdf ---------------------------------------------------------");

        String fileName;
        String[] fileNameParams = {
                collaborator.getNom(),
                missionAsJson.getMission(),
                Long.toString(missionAsJson.getId()),
        };

        fileName = properties.get(PROPERTY_ODM_PDF_FILENAME, fileNameParams);

        FileOutputStream fileOutputStream;

        try {
            String missionOrderDirectory = Parametrage.getContext(CONTEXT_FOLDER_ODM);
            String filePath = missionOrderDirectory + fileName;
            fileService.createFolder(filePath);
            fileOutputStream = new FileOutputStream(missionOrderDirectory + fileName);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "Erreur fichier";
        }

        missionAsJson.setpdfODM(fileName);

        Document document = newDocument();
        document.setMargins(20,20,30,0);
        PdfWriter writer;
        writer = newWriter(document, fileOutputStream);

        TableHeaderFooter event = new TableHeaderFooter();
        writer.setPageEvent(event);
        prepareWriter(writer);

        // ------------------------------------------------------//
        // CONSTITUTION DU CONTENU DU pdf D'ORDRE DE MISSION    //
        // ----------------------------------------------------//
        document.open();

        document.add(createHeader(missionAsJson));

        document.add(createTableUserODM(missionAsJson, collaborator));
        document.add(createClientTable(missionAsJson, collaborator));
        document.add(createPlanificationTable(missionAsJson, collaborator));
        document.add(createVisaTable(missionAsJson, collaborator));

        document.newPage();
        document.add(addSecurityRules(missionAsJson));

        document.close();
        fileOutputStream.close();

        logger.debug("-------------------------------------------------------------- fin creation pdf ---------------------------------------------------------");

        return fileName;
    }


    /**
     * Insert the title, name of the mission and date of creation of the mission
     *
     * @param mission the mission for which we create the ODM
     * @return Paragraph with all the infos about the header
     */
    private Paragraph createHeader(MissionAsJson mission){

        Date today = new Date();

        Paragraph header = new Paragraph(MISSION_ORDER_HEADER, fontPurpleHaze);
        Paragraph missionHeader = new Paragraph(
                concatenateObjectsToString(
                    SIMPLE_RETURN, MISSION_NUMBER_LABEL, mission.getMission(), SIMPLE_RETURN,
                    DATE_LABEL, DATE_FORMATER.format(today), SIMPLE_RETURN
                )
        );

        header.add(missionHeader);

        header.setIndentationLeft(30);
        header.setSpacingAfter(20);

        return header;
    }

    /**
     * Create a table about the current user informations
     *
     * @param missionAsJson the mission related
     * @param collaborator the employee related
     * @return PdfPTable table with the informations of the given employee
     */
    public PdfPTable createTableUserODM(MissionAsJson missionAsJson, Collaborator collaborator) {

        PdfPTable userTable = tableFactory(2);

        if (collaborator.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            userTable.addCell(titleCellFactory(SUBCONTRACTOR_HEADER));
            userTable.addCell(titleCellFactory(PRESCRIPTION_HEADER));
        } else {
            userTable.addCell(titleCellFactory(COLLABORATOR_HEADER));
            userTable.addCell(titleCellFactory(RESPONSIBLE_HEADER));
        }

        String collaboratorInfos = concatenateObjectsToString(
                NAME_LABEL, collaborator.getNom(),
                DOUBLE_RETURN,
                FIRST_NAME_LABEL, collaborator.getPrenom(),
                DOUBLE_RETURN,
                PHONE_NUMBER_LABEL, collaborator.getTelephone() != null ? collaborator.getTelephone() : "",
                DOUBLE_RETURN
        );

        userTable.addCell(new Phrase(collaboratorInfos));

        if (missionAsJson.getManager() != null) {
            String managerInfos = concatenateObjectsToString(
                    NAME_LABEL, missionAsJson.getManager().getNom(),
                    DOUBLE_RETURN,
                    FIRST_NAME_LABEL, missionAsJson.getManager().getPrenom(),
                    DOUBLE_RETURN,
                    PHONE_NUMBER_LABEL, missionAsJson.getManager().getTelephone() != null ? missionAsJson.getManager().getTelephone() : "",
                    DOUBLE_RETURN
            );
            userTable.addCell(new Phrase(managerInfos));

        } else {
            userTable.addCell(new Phrase(FIRST_LAST_NAMES_LABEL));
        }

        return userTable;
    }

    /**
     * Create the table about customer informations and a given mission details
     *
     * @param missionAsJson The mission to be treates
     * @param collaborator  the employee who is related to the mission
     * @return a PdfPTable about the client related to the mission
     */
    private PdfPTable createClientTable(MissionAsJson missionAsJson, Collaborator collaborator) {

        PdfPTable clientTable = tableFactory(2);

        if (collaborator.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            clientTable.addCell(titleCellFactory(AMILTONE_CUSTOMER_HEADER));
        } else {
            clientTable.addCell(titleCellFactory(CUSTOMER_HEADER));
        }
        clientTable.addCell(titleCellFactory(MISSION_DETAILS_HEADER));

        PdfPCell clientInfos;

        if (missionAsJson.getTypeMission().getCode().equals(TypeMission.FORMATION)) {
            clientInfos = contentCellFactory(
                    SIMPLE_RETURN +
                    FORMATION_HEADER +
                    DOUBLE_RETURN +
                    SIMPLE_RETURN
            );
        } else {
            clientInfos = contentCellFactory(prepareClientInfos(missionAsJson));
        }
        clientTable.addCell(clientInfos);

        PdfPCell missionDescription;

        if (missionAsJson.getDescription() != null) {
            missionDescription = contentCellFactory(missionAsJson.getDescription().replaceAll("\r\n", " "));
        } else {
            missionDescription = contentCellFactory("");
        }
        clientTable.addCell(missionDescription);

        return clientTable;
    }

    /**
     * retrieve client informations related to a given mission
     *
     * @param missionAsJson the related mission
     * @return a String with all of the client informations
     */
    private String prepareClientInfos(MissionAsJson missionAsJson) {

        ClientAsJson client = missionAsJson.getClient();

        String clientInfos = "";

        if (client != null) {
            clientInfos = concatenateObjectsToString(
                COMPANY_LABEL, client.getNom_societe(),
                DOUBLE_RETURN,
                ADDRESS_LABEL, client.getAdresse(),
                DOUBLE_RETURN
            );

            if (missionAsJson.getResponsableClient() != null) {
                clientInfos = concatenateObjectsToString(
                    clientInfos,
                    CUSTOMER_MANAGER_LABEL, missionAsJson.getResponsableClient().getNom(),
                    SPACE_SEPARATION, missionAsJson.getResponsableClient().getPrenom(),
                    DOUBLE_RETURN
                );
            }
        } else if (typeMissionService.findUniqEntiteByProp(TypeMission.PROP_CODE, missionAsJson.getTypeMission().getCode()) != null) {
            clientInfos = concatenateObjectsToString(
                clientInfos,
                missionAsJson.getTypeMission().getTypeMission(),
                DOUBLE_RETURN
            );
        } else {
            clientInfos = concatenateObjectsToString(
                    clientInfos,
                    SIMPLE_RETURN,
                    COMPANY_LABEL, AMILTONE_NAME,
                    DOUBLE_RETURN,
                    ADDRESS_LABEL, missionAsJson.getAgence().getAdresse(), COMMA_SEPARATION,
                    missionAsJson.getAgence().getCodePostal(), SPACE_SEPARATION, missionAsJson.getAgence().getVille(),
                    DOUBLE_RETURN
            );
        }
        return clientInfos;
    }

    /**
     * Create the mission planning table
     *
     * @param missionAsJson the mission related
     * @return the pdf table about the mission planning
     */
    private PdfPTable createPlanificationTable(MissionAsJson missionAsJson, Collaborator collaborator) {

        String collaboratorStatusCode = collaborator.getStatut().getCode();
        PdfPTable planificationTable;

        if (collaboratorStatusCode.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
            planificationTable = tableFactory(1);
        else
            planificationTable = tableFactory(2);

        planificationTable.addCell(titleCellFactory(PLANIFICATION_HEADER));

        if (!collaboratorStatusCode.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT))
            planificationTable.addCell(titleCellFactory(MISSION_COSTS_HEADER));

        String planificationInfos = concatenateObjectsToString(
                START_DATE_LABEL, DATE_FORMATER.format(missionAsJson.getDateDebut()),
                DOUBLE_RETURN,
                DOCUMENT_VALIDITY_END_DATE_LABEL, DATE_FORMATER.format(missionAsJson.getDateFin()),
                DOUBLE_RETURN,
                WEEKLY_DURATION_LABEL, missionAsJson.getHeuresHebdo(), SPACE_SEPARATION, HOURS_LOWERCASE_UNIT, SPACE_SEPARATION,
                DOUBLE_RETURN
        );

        if (missionAsJson.getClient() != null && missionAsJson.getSuivi() != null) {
            planificationInfos = concatenateObjectsToString(
                    planificationInfos,
                    COLLABORATEUR_FOLLOWUP_LABEL, missionAsJson.getSuivi(),
                    DOUBLE_RETURN) ;
        }

        PdfPCell planificationCell = contentCellFactory(planificationInfos);

        String missionCode = missionAsJson.getTypeMission().getCode();

        if (missionCode.equals(TypeMission.WEB_FACTORY)
                || missionCode.equals(TypeMission.MOBILE_FACTORY)
                || missionCode.equals(TypeMission.DATA_FACTORY)
                || missionCode.equals(TypeMission.SOFT_FACTORY)
                || collaborator.getPoste().getCode().equals(Poste.POSTE_CHARGE_COMMUNICATION_GRAPHISTE)) {
            planificationCell.addElement(new Phrase(TIME_PLANIFICATION_INTERNAL_MISSIONS, FONT_ITALIC_MIN));
        }

        planificationTable.addCell(planificationCell);

        if (!collaboratorStatusCode.equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            planificationTable.addCell(createChargesCell(missionAsJson));
        }

        return planificationTable;
    }

    /**
     * Add mission costs(frais) to pdf table cell
     *
     * @param missionAsJson the mission related to the costs
     * @return the pdf table with costs included in a cell
     */
    private PdfPCell createChargesCell(MissionAsJson missionAsJson) {
        String charges = "";

        PdfPCell cellCharges;

        Mission mission = missionService.get(missionAsJson.getId());
        if ((TypeMission.MISSION_CLIENTE.equals(missionAsJson.getTypeMission().getCode()))) {
            charges = BUSINESS_TRIP_REIMBURSEMENT_DETAILS + DOUBLE_RETURN;
        }

        List<LinkMissionForfaitAsJson> listForfaitJson = missionAsJson.getListMissionForfait();
        List<LinkMissionForfait> listForfait = mission.getListMissionForfaits();

        if (!listForfaitJson.isEmpty() || !listForfait.isEmpty()) {
            int listSize = !listForfaitJson.isEmpty() ? listForfaitJson.size() : listForfait.size();
            boolean asJson = !listForfaitJson.isEmpty();
            Forfait planTemp;
            float planAmount;
            String planLabel;

            for (int i = 0; i < listSize; i++) {
                if (asJson) {
                    planTemp = forfaitService.get(listForfaitJson.get(i).getForfait().getId());
                    planAmount = listForfaitJson.get(i).getMontant();
                    planLabel = listForfaitJson.get(i).getLibelle();
                } else {
                    planTemp = forfaitService.get(listForfait.get(i).getForfait().getId());
                    planAmount = listForfait.get(i).getMontant();
                    planLabel = listForfait.get(i).getLibelle();
                }

                switch (planTemp.getCode()) {
                    case Forfait.CODE_REPAS:
                        charges = concatenateObjectsToString(
                            charges,
                            DASH, MEAL_PLAN_LABEL, planAmount, EURO_PER_WORKINGDAY_UNIT +
                            DOUBLE_RETURN
                        );
                        break;
                    case Forfait.CODE_TELEPHONE:
                        charges = concatenateObjectsToString(
                            charges,
                            DASH, PHONE_PLAN_LABEL, planAmount, EURO_PER_MONTH_UNIT,
                            DOUBLE_RETURN
                        );
                        break;
                    case Forfait.CODE_LOGEMENT:
                        charges = concatenateObjectsToString(
                            charges,
                            DASH, HOUSING_PLAN_LABEL, planAmount, EURO_PER_WORKINGDAY_UNIT,
                            DOUBLE_RETURN
                        );
                        break;
                    case Forfait.CODE_DEPLACEMENT:
                        charges = concatenateObjectsToString(
                            charges,
                            DASH, BUSINESS_TRIP_PLAN_LABEL, planAmount, EURO_PER_WORKINGDAY_UNIT,
                            DOUBLE_RETURN
                        );
                        break;
                    default:
                        charges = concatenateObjectsToString(
                            charges,
                            DASH, planLabel,
                            DOUBLE_RETURN
                        );
                        break;
                }
            }


            cellCharges = contentCellFactory(charges);
        } else {
            cellCharges = new PdfPCell(new Phrase(""));
        }
        return cellCharges;
    }

    /**
     * Create table visa pdf p table.
     *
     * @param missionAsJson the p mission
     * @return the pdf p table
     */
    private PdfPTable createVisaTable(MissionAsJson missionAsJson, Collaborator collaborator) {
        PdfPTable visaTable = tableFactory(2);

        if (collaborator.getStatut().getCode().equals(StatutCollaborateur.STATUT_SOUS_TRAITANT)) {
            visaTable.addCell(titleCellFactory(VISA_HEADER + SPACE_SEPARATION + SUBCONTRACTOR_HEADER));
        } else {
            visaTable.addCell(titleCellFactory(VISA_HEADER + SPACE_SEPARATION + COLLABORATOR_HEADER));
        }

        visaTable.addCell(titleCellFactory(VISA_HEADER + SPACE_SEPARATION + DIRECTION_HEADER));

        visaTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        visaTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        visaTable.getDefaultCell().setMinimumHeight(60);

        String missionCode = missionAsJson.getEtat().getCode();

        switch (missionCode) {
            case Etat.ETAT_BROUILLON_CODE:
                visaTable.addCell(new Phrase(""));
                visaTable.addCell(new Phrase(""));
                break;
            case Etat.ETAT_SOUMIS_CODE:
                visaTable.addCell(new Phrase(""));
                visaTable.addCell(new Phrase(DIRECTION_VALIDATION_DETAILS));
                break;
            case Etat.ETAT_VALIDE_CODE:
                visaTable.addCell(new Phrase("Validé par " + collaborator.getNom() + " " + collaborator.getPrenom()));
                visaTable.addCell(new Phrase(DIRECTION_VALIDATION_DETAILS));
                break;
        }
        return visaTable;
    }

    private Paragraph addSecurityRules(MissionAsJson missionAsJson) {
        if (missionAsJson.getPerimetreMissionAsJson().isEmpty()) {
            Mission mission = missionService.get(missionAsJson.getId());

            List<LinkPerimetreMission> perimeters = missionService.getListPerimetreMission(mission);
            List<LinkPerimetreMissionAsJson> perimetersAsJson = new ArrayList<>();

            perimeters.forEach(perimeter -> perimetersAsJson.add(new LinkPerimetreMissionAsJson(perimeter)));
            missionAsJson.setPerimetreMissionAsJson(perimetersAsJson);
        }

        if (missionAsJson.getPerimetreMissionAsJson().isEmpty()) {
            return new Paragraph("");
        }

        Paragraph securityRulesLabel = new Paragraph(SECURITY_RULES + DOUBLE_RETURN, FONT_BOLD);
        Paragraph securityRules = new Paragraph("");

        for (LinkPerimetreMissionAsJson linkPerimetreMissionAsJson : missionAsJson.getPerimetreMissionAsJson()) {
            switch (linkPerimetreMissionAsJson.getPerimetreMission().getId().intValue()) {
                case 1:
                    securityRules.add(PERIMETRE_INFO_ADMIN);
                    break;
                case 2:
                    securityRules.add(PERIMETRE_INDUS);
                    break;
                case 3:
                    securityRules.add(PERIMETRE_COM);
                    break;
                default:
                    break;

            }
            securityRules.add(DOUBLE_RETURN);
        }
        securityRulesLabel.setIndentationLeft(30);
        securityRulesLabel.setIndentationRight(30);
        securityRules.setLeading(0, 1.5f);

        securityRulesLabel.add(securityRules);

        return securityRulesLabel;
    }

    private PdfPTable tableFactory(int nbColumn){

        PdfPTable table = new PdfPTable(nbColumn);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setWidthPercentage(90);

        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);
        table.getDefaultCell().setPadding(10);
        table.setSpacingAfter(20);

        return table;
    }

    private PdfPCell titleCellFactory(String titleCellOne){

        PdfPCell cell = new PdfPCell(new Phrase(titleCellOne, fontHeaderGrey));

        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(NEW_BACKGROUND_HEADER);
        cell.setMinimumHeight(30);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        return cell;
    }

    private PdfPCell contentCellFactory(String content){

        PdfPCell cell = new PdfPCell();
        Paragraph paragraph = new Paragraph(content);

        cell.setPadding(10);
        cell.setBackgroundColor(BaseColor.WHITE);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        paragraph.setLeading(0, 1.2f);

        cell.addElement(paragraph);

        return cell;
    }

    /**
     * Inner class to add a table as header.
     */
    private static class TableHeaderFooter extends PdfPageEventHelper {

        /**
         * Adds a footer to every page
         * Called automatically when adding a TableHeaderFooter to the document
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
         * com.itextpdf.text.Document)
         */
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                        new Phrase(LEGAL_NOTES, FONT_FOOTER),
                        (document.right() - document.left()) / 2 + document.leftMargin(),
                        document.bottom() + 30, 0);
        }
    }
}
