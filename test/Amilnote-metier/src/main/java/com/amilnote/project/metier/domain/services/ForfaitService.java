/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Forfait;
import com.amilnote.project.metier.domain.entities.Mission;
import com.amilnote.project.metier.domain.entities.json.ForfaitAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkMissionForfaitAsJson;

import java.util.List;

/**
 * The interface Forfait service.
 */
public interface ForfaitService extends AbstractService<Forfait> {

    /**
     * rechercher la liste des forfaits enregistrés pour une mission
     *
     * @param pMission the p mission
     * @return List list
     */
    List<Forfait> findForfaitsForMission(Mission pMission);

    /**
     * Recherche un forfait en fonction de son code
     *
     * @param code the code
     * @return the forfait
     */
    Forfait findForfaitByCode(String code);

    /***
     * Retourne tous les forfaits au format as json
     *
     * @return List all forfaits as json
     */
    List<ForfaitAsJson> getAllForfaitsAsJson();

    /**
     * Gets list forfaits to add.
     *
     * @return the list forfaits to add
     */
    List<LinkMissionForfaitAsJson> getListForfaitsLIBToAdd();

    /**
     * Sets list forfaits LIBRE to add.
     *
     * @param listForfaitsToAdd the list forfaits to add
     */
    void setListForfaitsLIBToAdd(List<LinkMissionForfaitAsJson> listForfaitsToAdd);

    /**
     * Gets list forfaits NON LIBRE to add.
     *
     * @return the list forfaits to add
     */
    List<LinkMissionForfaitAsJson> getListForfaitsNonLIBToAdd();

    /**
     * Sets list forfaits NON LIBRE to add.
     *
     * @param listForfaitsToAdd the list forfaits to add
     */
    void setListForfaitsNonLIBToAdd(List<LinkMissionForfaitAsJson> listForfaitsToAdd);

    /**
     * Get a plan list ({@link LinkMissionForfaitAsJson}) in this service's implementation instance (in-memory) related to a collaborator
     * @param collaboratorId the collaborator's id on which the search will be based on
     * @return a list of ({@link LinkMissionForfaitAsJson}) of free plans ({@link Forfait})
     */
    List<LinkMissionForfaitAsJson> getListForfaitsLIBToAdd(Long collaboratorId);

    /**
     * Set a plan list ({@link LinkMissionForfaitAsJson}) in this service's implementation instance (in-memory) related to a collaborator
     * @param collaboratorId the collaborator's id on which the search will be based on
     * @param listForfaitsToAdd the (in-memory) list will be set by this list
     */
    void setListForfaitsLIBToAdd(Long collaboratorId, List<LinkMissionForfaitAsJson> listForfaitsToAdd);

    /**
     * Get a non free plan list ({@link LinkMissionForfaitAsJson}) in this service's implementation instance (in-memory) related to a collaborator
     * @param collaboratorId the collaborator's id on which the search will be based on
     * @return a list of ({@link LinkMissionForfaitAsJson}) of non free plans ({@link Forfait})
     */
    List<LinkMissionForfaitAsJson> getListForfaitsNonLIBToAdd(Long collaboratorId);

    /**
     * Set a non free plan list ({@link LinkMissionForfaitAsJson}) in this service's implementation instance (in-memory) related to a collaborator
     * @param collaboratorId the collaborator's id on which the search will be based on
     * @param listForfaitsToAdd the (in-memory) list will be set by this list
     */
    void setListForfaitsNonLIBToAdd(Long collaboratorId, List<LinkMissionForfaitAsJson> listForfaitsToAdd);

    /**
     * Clear/remove a list of {@link LinkMissionForfaitAsJson} related to a collaborator, in the mapForfaitsLIBToAdd and mapForfaitsNonLIBToAdd of this service implementation
     * @param collaboratorId the list tht will be cleared/removed from the hashmap is related to this collaborator id
     */
    void clearForfaitsMaps(Long collaboratorId) ;

    /**
     * Clear/remove one forfait from a list of {@link LinkMissionForfaitAsJson} related to a collaborator, a code and an amount in the mapForfaitsNonLIBToAdd of this service implementation
     * @param listForfaits the forfaitList that will be iterated in order to remove a forfait from it.
     * @param code the code of the forfait that will be removed from the hashmap.
     * @param amount the amount of the forfait that will be removed from the hashmap.
     */
    void removeNonLibForfait(List<LinkMissionForfaitAsJson> listForfaits, String code, float amount);

    /**
     * Clear/remove one forfait from a list of {@link LinkMissionForfaitAsJson} related to a collaborator, a code and an amount in the mapForfaitsLIBToAdd of this service implementation
     * @param listForfaits the forfaitList that will be iterated in order to remove a forfait from it.
     * @param code the code of the forfait that will be removed from the hashmap.
     */
    void removeLibForfait(List<LinkMissionForfaitAsJson> listForfaits, String code);
}
