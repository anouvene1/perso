package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.ami_nonFacturableDAO;
import com.amilnote.project.metier.domain.entities.ami_nonFacturable;
import com.amilnote.project.metier.domain.services.ami_nonFacturableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by clome on 27/04/2017.
 */
@Service("ami_nonFacturableService")
public class ami_nonFacturableServiceImpl extends AbstractServiceImpl<ami_nonFacturable, ami_nonFacturableDAO> implements ami_nonFacturableService {
    /**
     * The ami_nonFacturable dao.
     */
    @Autowired
    private ami_nonFacturableDAO ami_non_FacturableDAO;


    /**
     * {@inheritDoc}
     */
    @Override
    public List<ami_nonFacturable> findListEntitesByPropBetweenDates(String pProp, Date pDateDebutMois, Date pDateFinMois) {
        return ami_non_FacturableDAO.findListEntitesByDateBetweenDates(pProp, pDateDebutMois,pDateFinMois);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ami_nonFacturableDAO getDAO() { return ami_non_FacturableDAO; }

}
