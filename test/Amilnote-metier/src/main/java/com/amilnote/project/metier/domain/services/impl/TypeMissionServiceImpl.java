/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.TypeMissionDAO;
import com.amilnote.project.metier.domain.dao.impl.TypeMissionDAOImpl;
import com.amilnote.project.metier.domain.entities.TypeMission;
import com.amilnote.project.metier.domain.entities.json.TypeMissionAsJson;
import com.amilnote.project.metier.domain.services.TypeMissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Type mission service.
 */
@Service("typeMissionService")
public class TypeMissionServiceImpl extends AbstractServiceImpl<TypeMission, TypeMissionDAOImpl> implements TypeMissionService {

    @Autowired
    private TypeMissionDAO typeMissionDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public TypeMissionDAOImpl getDAO() {
        return (TypeMissionDAOImpl) typeMissionDAO;
    }

    /**
     * {@linkplain TypeMissionService#getAllTypeMission()}
     */
    @Override
    public List<TypeMission> getAllTypeMission() {
        return typeMissionDAO.loadAll();
    }

    /**
     * {@linkplain TypeMissionService#getTypeMissionById(long)}
     */
    @Override
    public TypeMission getTypeMissionById(long pId) {
        return typeMissionDAO.get(pId);
    }

    /**
     * {@linkplain TypeMissionService#getAllTypeMissionAsJson()}
     */
    @Override
    public List<TypeMissionAsJson> getAllTypeMissionAsJson() {
        List<TypeMission> lListTypeMission = typeMissionDAO.loadAll();
        List<TypeMissionAsJson> lTypeMissionAsJson = new ArrayList<>();

        for (TypeMission lTypeMission : lListTypeMission) {
            lTypeMissionAsJson.add(lTypeMission.toJson());
        }
        return lTypeMissionAsJson;
    }

    /**
     * {@linkplain TypeMissionService#getTypeMissionByName(String)}
     */
    @Override
    public TypeMission getTypeMissionByName(String pNameMission) {
        return typeMissionDAO.findUniqEntiteByProp(TypeMission.PROP_TYPE_FRAIS, pNameMission);

    }

    @Override
    public TypeMission getTypeMissionByCode(String pCodeMission) {
        return typeMissionDAO.findUniqEntiteByProp(TypeMission.PROP_CODE, pCodeMission);
    }
}
