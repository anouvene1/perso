/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils.hqlhelper;

import com.amilnote.project.metier.domain.utils.hqlhelper.Table.TypeJointure;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Hql helper.
 */
public class HQLHelper {
    private Table fromTable;
    private List<Table> joinTables = new ArrayList<>();
    private Condition restriction = null;
    private List<Order> tri = new ArrayList<>();


    /**
     * Instantiates a new Hql helper.
     *
     * @param table the table
     */
    public HQLHelper(Class table) {
        this.fromTable = new Table(table, this);
    }

    /**
     * Instantiates a new Hql helper.
     *
     * @param table the table
     * @param alias the alias
     */
    public HQLHelper(Class table, String alias) {
        this.fromTable = new Table(table, alias, this);
    }

    /**
     * Ajoute une table à la requête avec une association de type INNER JOIN
     *
     * @param newTable Class représentant la table
     * @return table table
     */
    public Table addTable(Class newTable) {
        Table table = new Table(newTable, this);
        table.addJointure(TypeJointure.INNER_JOIN);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute une table à la requête avec une association de type INNER JOIN
     *
     * @param newTable Chaine de caractère représentant le nom de la table
     * @return retourn un object Table représentant la table ajouté.
     */
    public Table addTable(String newTable) {
        Table table = new Table(newTable, this);
        table.addJointure(TypeJointure.INNER_JOIN);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute une table avec un alias à la requête avec une association de type INNER JOIN
     *
     * @param newTable Class représentant la table
     * @param alias    Nom de l'alias
     * @return retourn un object Table représentant la table ajouté.
     */
    public Table addTable(Class newTable, String alias) {
        Table table = new Table(newTable, alias, this);
        table.addJointure(TypeJointure.INNER_JOIN);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute une table avec un alias à la requête avec une association de type INNER JOIN
     *
     * @param newTable Chaine de caractère représentant le non de la table
     * @param alias    Nom de l'alias
     * @return retourn un object Table représentant la table ajouté.
     */
    public Table addTable(String newTable, String alias) {
        Table table = new Table(newTable, alias, this);
        table.addJointure(TypeJointure.INNER_JOIN);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute une table avec un alias à la requête
     *
     * @param newTable     Class représentant la table
     * @param alias        Nom de l'alias
     * @param typeJointure type de joinTables (INNER JOIN, LEFT JOIN ou RIGHT JOIN)
     * @return retourn un object Table représentant la table ajouté.
     */
    public Table addTable(Class newTable, String alias, TypeJointure typeJointure) {
        Table table = new Table(newTable, alias, this);
        table.addJointure(typeJointure);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute une table avec un alias à la requête
     *
     * @param newTable     Chaine de caractère représentant le non de la table
     * @param alias        Nom de l'alias
     * @param typeJointure type de joinTables (INNER JOIN, LEFT JOIN ou RIGHT JOIN)
     * @return retourn un object Table représentant la table ajouté.
     */
    public Table addTable(String newTable, String alias, TypeJointure typeJointure) {
        Table table = new Table(newTable, alias, this);
        table.addJointure(typeJointure);
        this.joinTables.add(table);

        return table;
    }

    /**
     * Ajoute un champ à la table pricipale
     *
     * @param nomChamp Nom du champ
     * @return retourn un object Champ représentant le champ ajouté.
     */
    public Champ addChamp(String nomChamp) {
        return this.fromTable.addChamp(nomChamp);
    }

    /**
     * Ajoute une Restriction à la requête HQL.
     * si une condition est déjà présente un AND est réalisé entre les deux Condidtions
     *
     * @param condition Condition à ajouter.
     * @return retourne la requête HQL en cours.
     */
    public HQLHelper addRestriction(Condition condition) {
        if (this.restriction == null) {
            this.restriction = condition;
        } else {
            this.restriction = Condition.and(this.restriction, condition);
        }
        return this;
    }

    /**
     * Ajout un champ à la clause ORDER BY de la requête.
     *
     * @param order Champ fromTable et ascendance de celui-ci
     * @return retourne la requête HQL en cours.
     */
    public HQLHelper addTri(Order order) {
        this.tri.add(order);
        return this;
    }

    /**
     * Gets table cible.
     *
     * @return the table cible
     */
    protected Table getTableCible() {
        return this.fromTable;
    }

    /**
     * Génération de la requête HQL dans une chaine de caractère
     *
     * @return Retourne la requête HQL
     */
    public String getHQLQuery() {
        String requete = "SELECT " + this.fromTable.getAlias() + " FROM " + this.fromTable.getTable();

        for (Table table : joinTables) {
            requete = requete + " " + table.getTable();
        }

        requete = requete + " WHERE " + this.restriction.getRestriction();

        requete = requete + " ORDER BY " + Order.toSqlString(tri);

        return requete;
    }
}
