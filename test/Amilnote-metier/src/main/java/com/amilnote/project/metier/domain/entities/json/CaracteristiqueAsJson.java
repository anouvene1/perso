/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Caracteristique;
import com.amilnote.project.metier.domain.entities.TypeValeur;

/**
 * The type Caracteristique as json.
 *
 * @author LSouai
 */
public class CaracteristiqueAsJson {

    private Long id;
    private String carac;
    private String code;
    private TypeValeurAsJson typeValeur;

    /**
     * Instantiates a new Caracteristique as json.
     *
     * @param pCarac      the p carac
     * @param pCode       the p code
     * @param pTypeValeur the p type valeur
     */
    public CaracteristiqueAsJson(String pCarac, String pCode, TypeValeur pTypeValeur) {
        super();
        this.carac = pCarac;
        this.code = pCode;
        this.typeValeur = pTypeValeur.toJson();
    }

    /**
     * Instantiates a new Caracteristique as json.
     *
     * @param pId the p id
     */
    public CaracteristiqueAsJson(Long pId) {
        super();
        this.id = pId;
    }

    /**
     * Instantiates a new Caracteristique as json.
     *
     * @param pCaracteristique the p caracteristique
     */
    public CaracteristiqueAsJson(Caracteristique pCaracteristique) {
        super();
        id = pCaracteristique.getId();
        carac = pCaracteristique.getCarac();
        code = pCaracteristique.getCode();
        typeValeur = pCaracteristique.getTypeValeur().toJson();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param pId the p id
     */
    public void setId(Long pId) {
        this.id = pId;
    }

    /**
     * Gets carac.
     *
     * @return the carac
     */
    public String getCarac() {
        return carac;
    }

    /**
     * Sets carac.
     *
     * @param pCarac the p carac
     */
    public void setCarac(String pCarac) {
        this.carac = pCarac;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param pCode the p code
     */
    public void setCode(String pCode) {
        this.code = pCode;
    }

    /**
     * Gets type valeur as json.
     *
     * @return the type valeur as json
     */
    public TypeValeurAsJson getTypeValeurAsJson() {
        return typeValeur;
    }

    /**
     * Sets type valeur as json.
     *
     * @param pTypeValeur the p type valeur
     */
    public void setTypeValeurAsJson(TypeValeurAsJson pTypeValeur) {
        typeValeur = pTypeValeur;
    }
}
