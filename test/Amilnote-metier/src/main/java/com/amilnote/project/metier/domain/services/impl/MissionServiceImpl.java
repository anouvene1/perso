/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.*;
import com.amilnote.project.metier.domain.dao.impl.MissionDAOImpl;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FraisAsJson;
import com.amilnote.project.metier.domain.entities.json.LinkPerimetreMissionAsJson;
import com.amilnote.project.metier.domain.entities.json.MissionAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderFacture;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.Parametrage;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Mission service.
 */
@Service("missionService")
public class MissionServiceImpl extends AbstractServiceImpl<Mission, MissionDAOImpl> implements MissionService {

    private static Logger logger = LogManager.getLogger(MissionServiceImpl.class);

    @Autowired
    private MissionDAO missionDAO;

    @Autowired
    private ContactClientDAO contactClientDAO;

    @Autowired
    private ClientDAO clientDAO;

    @Autowired
    private MissionDAOImpl missionDAOImpl;

    @Autowired
    private TypeMissionService typeMissionService;

    @Autowired
    private OutilsInterneDAO outilsInterneDAO;

    @Autowired
    private FraisService fraisService;

    @Autowired
    private LinkEvenementTimesheetDAO linkEvenementTimesheetDAO;

    @Autowired
    private EtatDAO etatDAO;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private PerimetreMissionDAO perimetreMissionDAO;

    @Autowired
    private LinkPerimetreMissionDAO linkPerimetreMissionDAO;

    @Autowired
    private AgenceService agenceService;

    @Autowired
    private CollaboratorService collaboratorService;

    @Autowired
    private FactureService factureService;

    @Autowired
    private StatutCollaborateurService statutCollaboratorService;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public MissionDAOImpl getDAO() {
        return (MissionDAOImpl) missionDAO;
    }

    /**
     * {@linkplain MissionService#findById(Long)}
     */
    @Override
    public Mission findById(Long pIdMission) {
        return missionDAO.findUniqEntiteByProp("id", pIdMission);
    }

    /**
     * {@linkplain MissionService#findAllMissionsNotFinishForCollaborator(Collaborator, int)}
     */
    @Override
    public List<Mission> findAllMissionsNotFinishForCollaborator(Collaborator collaborator, int nbMonthBeforeCurrent) {

        // Récupère une date au premier jour du mois de ( now() -
        // (PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR * mois) )
        DateTime lFirstDayOnMonth = new DateTime(DateTime.now().withDayOfMonth(1)).minusMonths(nbMonthBeforeCurrent);

        return missionDAO.findAllMissionsNotFinishForCollaborator(collaborator, lFirstDayOnMonth);
    }

    /**
     * {@linkplain MissionService#findAllMissionByCollaboratorForFraisAsJson(Collaborator, int, int)}
     */
    @Override
    public String findAllMissionByCollaboratorForFraisAsJson(Collaborator pCollaborator, int pMois, int pAnnee) {
        List<Mission> lListMission = this.findAllMissionsNotFinishForCollaborator(pCollaborator,
                Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR);

        if (pMois < 1 && pMois > 12) {
            pMois = Calendar.getInstance().get(Calendar.MONTH) + 1;
        }

        if (pAnnee < 1 && pAnnee > 9999) {
            pMois = Calendar.getInstance().get(Calendar.YEAR);
        }

        List<MissionAsJson> lListMissionAsJson = new ArrayList<MissionAsJson>();
        for (Mission lMission : lListMission) {

            MissionAsJson tmp = lMission.toJson();
            tmp.setListLinkEvenementTimesheet(lMission.getLinkEvenementTimesheetLine());
            // recherche de tous les frais de la mission pour le mois courant
            tmp.setListFraisAsObject(fraisService.findAllByMissionOrderByDateCriteria(lMission, pMois, pAnnee));
            lListMissionAsJson.add(tmp);

        }
        return this.writeJson(lListMissionAsJson);
    }

    /**
     * {@linkplain MissionService#findAllMissionsNotFinishForCollaboratorAsJson(Collaborator, int, int)}
     */
    @Override
    public String findAllMissionsNotFinishForCollaboratorAsJson(Collaborator pCollaborator, int pMois, int pAnnee) {

        List<Mission> lListMission = this.findAllMissionsNotFinishForCollaborator(pCollaborator,
                Parametrage.PERIOD_HISTO_MAX_TIMESHEET_AS_MONTH_PAGINATOR);

        if (pMois < 1 && pMois > 12) {
            pMois = Calendar.getInstance().get(Calendar.MONTH) + 1;
        }

        if (pAnnee < 1 && pAnnee > 9999) {
            pMois = Calendar.getInstance().get(Calendar.YEAR);
        }

        List<MissionAsJson> lListMissionAsJson = new ArrayList<>();
        MissionAsJson lMissionAsJson;
        for (Mission lMission : lListMission) {

            lMissionAsJson = lMission.toJson();

            for (LinkMissionForfait linkMissionForfait : lMission.getListMissionForfaits()) {
                lMissionAsJson.getListMissionForfait().add(linkMissionForfait.toJson());
            }

            // recherche de tous les frais de la mission pour le mois courant
            DateTime date = new DateTime();
            date = date.withDate(pAnnee, pMois, 15);

            List<Object[]> listObject = fraisService.findAllByMissionOrderByDateCriteria(lMission, date);
            List<FraisAsJson> listFraisAsJson = getListFraisAsJsonFromListObject(listObject);
            lMissionAsJson.setListFraisAsJson(listFraisAsJson);

            lListMissionAsJson.add(lMissionAsJson);
        }
        return this.writeJson(lListMissionAsJson);
    }

    /**
     * {@linkplain MissionService#findByName(String)}
     */
    @Override
    public Mission findByName(String pNameMission) {
        return missionDAO.findUniqEntiteByProp(Mission.PROP_MISSION, pNameMission);

    }

    /**
     * {@linkplain MissionService#findDefaultMissionForCollaborator(Collaborator)}
     */
    @Override
    public Mission findDefaultMissionForCollaborator(Collaborator pCollaborator) {
        return missionDAO.findDefaultMissionForCollaborator(pCollaborator);
    }

    /**
     * {@linkplain MissionService#findMissionCollabPourRA(Collaborator, DateTime, DateTime)}
     */
    @Override
    public List<Mission> findMissionCollabPourRA(Collaborator pCollaborator, DateTime pDebutPeriode, DateTime pFinPeriode) {
        return missionDAO.findMissionEncoursPeriode(pCollaborator, pDebutPeriode, pFinPeriode);
    }

    /**
     * {@linkplain MissionService#findMissionsClientesByMonth(DateTime)}
     */
    @Override
    public List<Mission> findMissionsClientesByMonth(DateTime pDate) {
        List<Mission> missions = new ArrayList<>();
        try {
            missions = missionDAO.findMissionsClientesByMonth(pDate);
        } catch (Exception e) {
            logger.error("[Customer mission] find for date:" + pDate.toString(), e);
        }
        Collections.sort(missions, (m1, m2) -> m1.compareToByMission(m2));

        return missions;
    }

    /**
     * {@linkplain MissionService#findMissionsClientesByMonthForCollaborator(Collaborator, DateTime)}
     */
    @Override
    public List<Mission> findMissionsClientesByMonthForCollaborator(Collaborator pCollaborator, DateTime pDate) {
        return missionDAO.findMissionsClientesByMonthForCollaborator(pCollaborator, pDate);
    }

    /**
     * {@linkplain MissionService#findMissionCollabPourRAAsMissionAsJson(Collaborator, DateTime, DateTime)}
     */
    @Override
    public List<MissionAsJson> findMissionCollabPourRAAsMissionAsJson(Collaborator pCollaborator, DateTime pDebutPeriode, DateTime pFinPeriode) {

        List<Mission> lListMission = this.findMissionCollabPourRA(pCollaborator, pDebutPeriode, pFinPeriode);

        List<MissionAsJson> lListMissionAsJson = new ArrayList<>();
        for (Mission lMission : lListMission) {
            MissionAsJson missionAsJson = new MissionAsJson(lMission);
            missionAsJson.setListLinkEvenementTimesheet(lMission.getLinkEvenementTimesheetLine(), pDebutPeriode, pFinPeriode);
            missionAsJson.setListMissionForfait(lMission.getListMissionForfaits());
            missionAsJson.setCommandes(lMission.getCommandes());
            lListMissionAsJson.add(missionAsJson);

        }

        return lListMissionAsJson;
    }

    /**
     * {@linkplain MissionService#findAllMissionsCollaborator(Collaborator)}
     */
    @Override
    public List<MissionAsJson> findAllMissionsCollaborator(Collaborator pCollaborator) {
        List<Mission> lListMission = missionDAO.findListEntitesByProp(Mission.PROP_COLLABORATEUR, pCollaborator, Mission.PROP_MISSION);
        List<MissionAsJson> lListMissionAsJson = new ArrayList<>();
        for (Mission lMission : lListMission) {
            MissionAsJson missionAsJson = new MissionAsJson(lMission);
            missionAsJson.setListMissionForfait(lMission.getListMissionForfaits());
            missionAsJson.setCommandes(lMission.getCommandes());
            lListMissionAsJson.add(missionAsJson);
        }

        return lListMissionAsJson;
    }

    /**
     * {@linkplain MissionService#findAllMissionsCollaborator(Collaborator, String)}
     */
    @Override
    public List<MissionAsJson> findAllMissionsCollaborator(Collaborator pCollaborator, String orderBy) {
        List<Mission> lListMission = missionDAO.findListEntitesByProp(Mission.PROP_COLLABORATEUR, pCollaborator, orderBy);
        List<MissionAsJson> lListMissionAsJson = new ArrayList<>(lListMission.size());
        for (Mission lMission : lListMission) {
            MissionAsJson missionAsJson = new MissionAsJson(lMission);
            missionAsJson.setListMissionForfait(lMission.getListMissionForfaits());
            missionAsJson.setCommandes(lMission.getCommandes());
            lListMissionAsJson.add(missionAsJson);
        }

        return lListMissionAsJson;
    }

    /**
     * {@linkplain MissionService#findAllMissionsCollaborator(Collaborator)}
     */
    @Override
    public List<MissionAsJson> findAllMissionsSubcontractor(Collaborator pSousTraitant) {
        List<Mission> lListMission = missionDAO.findListEntitesByProp(Mission.PROP_SOUSTRAITANT, pSousTraitant, Mission.PROP_MISSION);
        List<MissionAsJson> lListMissionAsJson = new ArrayList<>();
        for (Mission lMission : lListMission) {
            MissionAsJson missionAsJson = new MissionAsJson(lMission);
            missionAsJson.setListMissionForfait(lMission.getListMissionForfaits());
            missionAsJson.setCommandes(lMission.getCommandes());
            lListMissionAsJson.add(missionAsJson);
        }

        return lListMissionAsJson;
    }

    /**
     * {@linkplain MissionService#findAllMissionsCollaborator(Collaborator, String)}
     */
    @Override
    public List<MissionAsJson> findAllMissionsSubcontractor(Collaborator pSousTraitant, String orderBy) {
        List<Mission> lListMission = missionDAO.findListEntitesByProp(Mission.PROP_SOUSTRAITANT, pSousTraitant, orderBy);
        List<MissionAsJson> lListMissionAsJson = new ArrayList<>(lListMission.size());
        for (Mission lMission : lListMission) {
            MissionAsJson missionAsJson = new MissionAsJson(lMission);
            missionAsJson.setListMissionForfait(lMission.getListMissionForfaits());
            missionAsJson.setCommandes(lMission.getCommandes());
            lListMissionAsJson.add(missionAsJson);
        }

        return lListMissionAsJson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String updateMission(MissionAsJson pMissionAsJson, Collaborator pCollaborator) {

        String msgRetour = "";
        if (pCollaborator != null) {
            // --- Récupération du collaborateur
            Mission mission = new Mission();
            // --- Si mission existante, chargement de cette mission
            if (null != pMissionAsJson.getId()) {
                mission = get(pMissionAsJson.getId());
            }

            // --- Création de la session et de la transaction
            Session session = missionDAOImpl.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            // --- Ajout des informations concernant la mission
            mission.setCollaborateur(pCollaborator);
            mission.setDateDebut(pMissionAsJson.getDateDebut());
            mission.setDateFin(pMissionAsJson.getDateFin());
            mission.setMission(pMissionAsJson.getMission());

            TypeMission typeMission = typeMissionService.getTypeMissionById(pMissionAsJson.getTypeMission().getId());
            mission.setTypeMission(typeMission);

            mission.setDuree_hebdo(pMissionAsJson.getHeuresHebdo());

            // --- Ajout des informations concernant le client
            if (pMissionAsJson.getDescription() != null) {
                mission.setDescription(pMissionAsJson.getDescription());
            } else {
                mission.setDescription(null);
            }

            if (pMissionAsJson.getSuivi() != null) {
                mission.setSuivi(pMissionAsJson.getSuivi());
            } else {
                mission.setSuivi(null);
            }

            if (pMissionAsJson.getResponsableClient() != null) {
                ContactClient contact = contactClientDAO.get(pMissionAsJson.getResponsableClient().getId());
                mission.setResp_client((contact));
            } else {
                mission.setResp_client(null);
            }

            if (pMissionAsJson.getClient() != null) {
                Client client = clientDAO.get(pMissionAsJson.getClient().getId());
                mission.setClient(client);
            } else {
                mission.setClient(null);
            }

            if (pMissionAsJson.getEtat() != null) {
                Etat etat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pMissionAsJson.getEtat().getCode());
                mission.setEtat(etat);
            }

            mission.setTjm(pMissionAsJson.getTjm());

            if (pMissionAsJson.getOutilsInterne() != null) {
                OutilsInterne outilsInterne = outilsInterneDAO.findUniqEntiteByProp(OutilsInterne.PROP_ID, pMissionAsJson.getOutilsInterne().getId());
                mission.setOutilsInterne(outilsInterne);
            } else {
                mission.setOutilsInterne(null);
            }

            if (pMissionAsJson.getAgence() != null && typeMission.getCode().equals(TypeMission.ACTIVITE_COMMERCIALE)) {
                Agence agence = agenceService.getAgenceById(pMissionAsJson.getAgence().getId());
                mission.setAgence(agence);
            } else {
                mission.setAgence(agenceService.getAgenceSiegeSocial());
            }

            if (pMissionAsJson.getManager() != null && typeMission.getCode().equals(TypeMission.MISSION_CLIENTE)) {
                Collaborator collaborateur = collaboratorService.findById(pMissionAsJson.getManager().getId());
                mission.setManager(collaborateur);
            } else {
                mission.setManager(pCollaborator.getManager());
            }

            // --- Sauvegarde et Commit de la mise à jour
            session.save(mission);
            session.flush();
            transaction.commit();
            pMissionAsJson.setId(mission.getId());
            msgRetour = "ok";
        } else {
            msgRetour = "error";
        }

        return msgRetour;
    }

    /**
     * {@linkplain MissionService#createOrUpdateLinkPerimetreMission(MissionAsJson)}
     */
    @Override
    public int createOrUpdateLinkPerimetreMission(MissionAsJson mission) {

        //SBE_AMNOTE-197
        if (mission.getPerimetreMissionAsJson() != null) {
            Mission miss = findById(mission.getId());

            //on créee une mission à partir du missionAsJson avec seulement ce dont on à besoin
            Mission missionAEnvoyer = new Mission();
            missionAEnvoyer.setId(mission.getId());

            List<LinkPerimetreMission> listeLink = new ArrayList<LinkPerimetreMission>();

            //on créee une liste qui recupere les linkperimetremission de mission
            LinkPerimetreMission linkperiTemp;
            PerimetreMission periTemp;
            for (LinkPerimetreMissionAsJson linkTemp : mission.getPerimetreMissionAsJson()) {
                linkperiTemp = new LinkPerimetreMission();
                linkperiTemp.setId(linkTemp.getId());
                linkperiTemp.setMission(miss);

                //on crée le nouveau perimetre mission
                periTemp = new PerimetreMission();
                periTemp.setId(linkTemp.getPerimetreMission().getId());
                periTemp.setNomPerimetre(linkTemp.getPerimetreMission().getNomPerimetre());
                periTemp.setCodePerimetre(linkTemp.getPerimetreMission().getCodePerimetre());
                linkperiTemp.setPerimetreMission(periTemp);

                listeLink.add(linkperiTemp);
            }
            missionAEnvoyer.setListPerimetreMission(listeLink);
            linkPerimetreMissionDAO.createOrUpdateLinkPerimetreMissionByMission(missionAEnvoyer);
            miss.setListPerimetreMission(listeLink);
        }
        return 0;
    }

    /**
     * {@linkplain MissionService#deleteMission(Mission)}
     */
    @Override
    public String deleteMission(Mission pMission) {

        // --- Si mission existante, chargement de cette mission
        if (null != pMission) {

            // --- Création de la session et de la transaction
            Session session = missionDAOImpl.getCurrentSession();
            Transaction transaction = session.beginTransaction();

            List<LinkEvenementTimesheet> list = linkEvenementTimesheetDAO.findListEntitesByProp(LinkEvenementTimesheet.PROP_MISSION, pMission);
            for (int i = 0; i < list.size(); i++) {
                session.delete(list.get(i));
            }

            String nomFichier = "";
            String[] paramsNomFichier = {
                    pMission.getCollaborateur().getNom(),
                    pMission.getMission(),
                    Long.toString(pMission.getId()),
            };

            //Mise en forme du nom du fichier
            nomFichier = properties.get("pdf.nomFichierODM", paramsNomFichier);

            File odm = null;
            try {
                odm = new File(Parametrage.getContext("dossierPDFODM") + nomFichier);
            } catch (NamingException e) {
                logger.error("[delete mission] mission : " + pMission.getId(), e);
            }
            if (odm.exists()) {
                odm.delete();
            }

            //SBE_AMNOTE-197 delete les perimetres missions
            List<LinkPerimetreMission> listLink = pMission.getListPerimetreMission();
            for (int i = 0; i < listLink.size(); i++) {
                session.delete(listLink.get(i));
            }
            pMission.setListPerimetreMission(null);
            /* for (LinkPerimetreMission linkPerimetreMiss : listLink) {
                linkPerimetreMissionDAO.deleteLinkPerimetreMissionByMission(linkPerimetreMiss);
            }*/

            session.flush();
            transaction.commit();

            List<Commande> listCommandes = commandeService.findListEntitesByProp(Commande.PROP_MISSION, pMission);
            listCommandes.forEach(commandeService::deleteCommande);


            Session session2 = missionDAOImpl.getCurrentSession();
            Transaction transaction2 = session.beginTransaction();

            // --- Sauvegarde et Commit de la mise à jour
            session2.delete(pMission);
            session2.flush();
            transaction2.commit();
            return "Suppression effectuée avec succès";
        }
        return "Mission nulle. Aucune suppression n'a été effectuée";
    }

    /**
     * {@linkplain MissionService#findByNameForCollab(String, Collaborator)}
     */
    @Override
    public Mission findByNameForCollab(String pNameMission, Collaborator pCollaborator) {

        //--- Récupération de la liste des missions portant le même nom que celle en paramètre et possédé par le collaborateur en paramètre
        List<Mission> lListMission = missionDAO.findListEntitesByProp(Mission.PROP_COLLABORATEUR, pCollaborator, Mission.PROP_MISSION);

        //--- Vérification supplémentaire : si une mission d'une même nom est présente dans la liste, on retourne cette mission
        for (Mission mission : lListMission) {
            //--- Contrôle d'égalité sans les espaces
            if (StringUtils.equals(mission.getMission().trim(), pNameMission.trim())) {
                return mission;
            }
        }

        return null;
    }

    /**
     * {@linkplain MissionService#getFileODMId(Long)}
     */
    @Override
    public File getFileODMId(Long pId) {
        // --- Récupération de la mission
        Mission mission = this.get(pId);
        if (null == mission) {
            logger.warn("[get file mission] mission is null");
            return null;
        }
        String nom = mission.getCollaborateur().getNom();
        String[] paramsNomFichier = {
                nom,
                mission.getMission(),
                Long.toString(mission.getId()),
        };
        // --- Récupération du fichier PDF
        String nomFichier = properties.get("pdf.nomFichierODM", paramsNomFichier);
        File tmpFile = null;
        try {
            tmpFile = new File(Parametrage.getContext("dossierPDFODM") + nomFichier);
        } catch (NamingException e) {
            logger.error("[get file mission] file naming error", e);
        }

        // --- Vérification de l'existance du fichier
        if (!tmpFile.exists()) {
            logger.warn("[get file mission] file does not exist");
            return null;
        }
        return tmpFile;
    }

    /**
     * {@linkplain MissionService#getAllMissionEtatSO()}
     */
    @Override
    public List<MissionAsJson> getAllMissionEtatSO() {

        Etat lEtatSO = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_SOUMIS_CODE);

        List<Mission> listeMission = missionDAO.findListEntitesByProp(Absence.PROP_ETAT, lEtatSO);

        List<MissionAsJson> listMissionAsJson = new ArrayList<MissionAsJson>();
        for (int i = 0; i < listeMission.size(); i++) {
            listMissionAsJson.add(listeMission.get(i).toJson());
        }

        return listMissionAsJson;
    }

    /**
     * {@linkplain MissionService#getAllMissionEtatBR()}
     */
    @Override
    public List<MissionAsJson> getAllMissionEtatBR() {

        Etat lEtatBR = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_BROUILLON_CODE);

        List<Mission> listeMission = missionDAO.findListEntitesByProp(Absence.PROP_ETAT, lEtatBR);

        List<MissionAsJson> listMissionAsJson = new ArrayList<MissionAsJson>();
        for (int i = 0; i < listeMission.size(); i++) {
            listMissionAsJson.add(listeMission.get(i).toJson());
        }

        return listMissionAsJson;
    }

    /**
     * {@linkplain MissionService#getAllMissionEtatVA()}
     */
    @Override
    public List<MissionAsJson> getAllMissionEtatVA() {

        Etat lEtatBR = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, Etat.ETAT_VALIDE_CODE);

        List<Mission> listeMission = missionDAO.findListEntitesByProp(Absence.PROP_ETAT, lEtatBR);

        List<MissionAsJson> listMissionAsJson = new ArrayList<MissionAsJson>();
        for (int i = 0; i < listeMission.size(); i++) {
            listMissionAsJson.add(listeMission.get(i).toJson());
        }

        return listMissionAsJson;
    }

    /**
     * {@linkplain MissionService#updateEtatMission(Long, String)}
     */
    @Override
    public String updateEtatMission(Long pIdMission, String pCodeEtat) throws Exception {
        String msgRetour;
        Mission mission = this.getDAO().get(pIdMission);
        Etat newEtat = etatDAO.findUniqEntiteByProp(Etat.PROP_CODE, pCodeEtat);

        if (null == mission) {
            throw new Exception("L'id de la mission (" + pIdMission + ") est inconnu");
        }

        if (null == newEtat) {
            throw new Exception("Ce code etat (" + pCodeEtat + ") est inconnu. ");
        }

        // --- Création de la session et de la transaction
        Session session = this.getDAO().getCurrentSession();
        Transaction transaction = session.beginTransaction();


        switch (newEtat.getCode()) {
            //cas deplacement Soumis=>valid : validation coté collab
            case Etat.ETAT_VALIDE_CODE:
                mission.setEtat(newEtat);
                msgRetour = "Validation effectuée avec succès";
                break;

            //cas ODM soumis --> refusé // BR -->refusé
            case Etat.ETAT_REFUSE:

                mission.setEtat(newEtat);
                msgRetour = "Refus effectué avec succès";

                break;

            //cas validation coté admin => passage à l'état soumis
            case Etat.ETAT_SOUMIS_CODE:
                mission.setEtat(newEtat);

                msgRetour = "Soumission effectuée avec succès";
                break;
            case Etat.ETAT_ANNULE:
                mission.setEtat(newEtat);

                // msgRetour = "Soumission effectuée avec succès";

            default:
                msgRetour = "";
        }
        session.save(mission);
        session.flush();
        transaction.commit();
        // --- Fin de la session et commit des changements
        return msgRetour;
    }

    /**
     * {@linkplain MissionService#getNbMissionAttenteVal(Collaborator)}
     */
    @Override
    public int getNbMissionAttenteVal(Collaborator pCollaborator) {
        return missionDAO.getNbMissionAttenteVal(pCollaborator);
    }

    /**
     * {@linkplain MissionService#getNbMissionAttenteVal()}
     */
    @Override
    public int getNbMissionAttenteVal() {
        return missionDAO.getNbMissionAttenteVal();
    }

    /**
     * {@linkplain MissionService#getFileFacture(Mission, DateTime)}
     */
    @Override
    public File getFileFacture(Mission pMission, DateTime date) {

        String dateFormatted = DateTimeFormat.forPattern("MMyyyy").print(date);
        if (null == pMission) {
            logger.warn("[get file invoice] mission is null");
            return null;
        }

        String nomFichier = "Fac_" + pMission.getClient().getNom_societe() + "_" + pMission.getCollaborateur().getNom() + "_" + pMission.getMission() + "_" + dateFormatted + ".xls";
        File tmpFile = null;
        try {
            tmpFile = new File(Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT) + nomFichier);
        } catch (NamingException e) {
            logger.error("[get file invoice] file naming error", e);
        }

        // --- Vérification de l'existance du fichier
        if (!tmpFile.exists()) {
            logger.warn("[get file invoice] file does not exist");
            return null;
        }
        return tmpFile;
    }

//SBE_AMNOTE-197

    //----------Link Perimetre & Mission-----------//

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LinkPerimetreMission> getListPerimetreMission(Mission mission) {
        List<LinkPerimetreMission> listLinkTemp = linkPerimetreMissionDAO.findAll();
        List<LinkPerimetreMission> listeARetourner = new ArrayList<>();
        for (LinkPerimetreMission linkTemp : listLinkTemp) {
            if (linkTemp.getMission().getId() == mission.getId()) {
                listeARetourner.add(linkTemp);
            }
        }
        return listeARetourner;
    }

    //----------Link Perimetre & Mission-----------//

    /**
     * {@linkplain MissionService#getAllPerimetreMission()}
     */
    @Override
    public List<PerimetreMission> getAllPerimetreMission() {
        return perimetreMissionDAO.findAll();
    }

    /**
     * {@linkplain MissionService#findPerimetreMissionByCode(String)}
     */
    @Override
    public PerimetreMission findPerimetreMissionByCode(String code) {
        List<PerimetreMission> lListePerimetre = getAllPerimetreMission();
        PerimetreMission perimetre = null;
        for (PerimetreMission tempPerimetre : lListePerimetre) {
            if (tempPerimetre.getCodePerimetre().equals(code)) {
                perimetre = tempPerimetre;
            }
        }
        return perimetre;
    }

    private List<FraisAsJson> getListFraisAsJsonFromListObject(List<Object[]> listObject) {
        List<FraisAsJson> listFraisAsJson = new ArrayList<>();
        for (Object[] o : listObject) {
            FraisAsJson frais = new FraisAsJson();
            frais.setId((Long) o[0]);
            frais.setMission((String) o[1]);
            frais.setDateEvenement((Date) o[2]);
            frais.setTypeFrais((String) o[3]);
            frais.setMontant((BigDecimal) o[4]);
            frais.setCommentaire((String) o[5]);
            frais.setNombreKm((Integer) o[6]);
            frais.setTypeAvance((String) o[7]);

            listFraisAsJson.add(frais);
        }
        return listFraisAsJson;
    }

    /**
     * {@linkplain MissionService#findAllMissionEnCours()}
     */
    @Override
    public List<Mission> findAllMissionEnCours() {
        Date today = new Date();
        List<Mission> listeMissionEnCours = missionDAO.findAllMissionEnCours(today);

        return listeMissionEnCours;
    }

    /**
     * {@linkplain MissionService#findAllMissionClientEnCours()}
     */
    @Override
    public List<Mission> findAllMissionClientEnCours() {
        Date today = new Date();
        List<Mission> listeMissionClientEnCours = missionDAO.findMissionClientEnCours(today);
        return listeMissionClientEnCours;
    }

    /**
     * {@linkplain MissionService#findAllMissionEnCoursNonClient()}
     */
    @Override
    public List<Mission> findAllMissionEnCoursNonClient() {
        Date today = new Date();
        List<Mission> listeMissionEnCoursNonClient = missionDAO.findMissionEnCoursNonClient(today);
        return listeMissionEnCoursNonClient;
    }

    /**
     * {@linkplain MissionService#findAllMissionForCollaborator(DateTime, Collaborator)}
     */
    @Override
    public List<Mission> findAllMissionForCollaborator(DateTime date, Collaborator collaborateur) {
        List<Mission> listeMissionEnCoursForCollab = missionDAO.findAllMissionForCollaborator(date, collaborateur);
        return listeMissionEnCoursForCollab;
    }

    /**
     * {@linkplain MissionService#findMissionForCollaboratorBetweenTwoDates(Date, Date, Collaborator)}
     */
    @Override
    public List<Mission> findMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborateur) {
        return missionDAO.findMissionForCollaboratorBetweenTwoDates(debut, fin, collaborateur);
    }

    /**
     * {@linkplain MissionService#findClientMissionForCollaboratorBetweenTwoDates(Date, Date, Collaborator)}
     */
    @Override
    public List<Mission> findClientMissionForCollaboratorBetweenTwoDates(Date debut, Date fin, Collaborator collaborator) {
        return missionDAO.findClientMissionForCollaboratorBetweenTwoDates(debut, fin, collaborator);
    }

    /**
     * {@linkplain MissionService#findMissionBetweenTwoDates(String, String)}
     */
    @Override
    public List<Mission> findMissionBetweenTwoDates(String start, String end) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date debut = null;
        Date fin = null;
        try {
            debut = sdf.parse(start);
            fin = sdf.parse(end);
        } catch (ParseException e) {
            logger.error("[find mission] error parsing dates", e);
        }
        return missionDAO.findMissionBetweenTwoDates(debut, fin);
    }

    /**
     * {@linkplain MissionService#findMissionNonClientBetweenTwoDates(String, String)}
     */
    @Override
    public List<Mission> findMissionNonClientBetweenTwoDates(String start, String end) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date debut = null;
        Date fin = null;
        try {
            debut = sdf.parse(start);
            fin = sdf.parse(end);
        } catch (ParseException e) {
            logger.error("[find mission] non customer mission, error parsing dates", e);
        }
        return missionDAO.findMissionNonClientBetweenTwoDates(debut, fin);
    }

    /**
     * {@linkplain MissionService#findMissionNonClientBetweenTwoDatesForCollab(String, String, Collaborator)}
     */
    @Override
    public List<Mission> findMissionNonClientBetweenTwoDatesForCollab(String start, String end, Collaborator collaborateur) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date debut = null;
        Date fin = null;
        try {
            debut = sdf.parse(start);
            fin = sdf.parse(end);
        } catch (ParseException e) {
            logger.error("[find mission] non customer collab mission, error parsing dates", e);
        }
        return missionDAO.findMissionNonClientBetweenTwoDatesForCollab(debut, fin, collaborateur);
    }

    /**
     * {@linkplain MissionService#findMissionWithoutInvoice(String, String, Date)}
     */
    @Override
    public List<Mission> findMissionWithoutInvoice(String missionTypeCode, String collaboratorStatusCode, Date end) {
        TypeMission missionType = typeMissionService.getTypeMissionByCode(missionTypeCode);
        StatutCollaborateur collaboratorStatus = statutCollaboratorService.findStatutCollaborateurByCode(collaboratorStatusCode);
        List<Mission> currentMissions = missionDAO.findMissionByTypeCollaboratorStatusAndCurrentPeriod(missionType, collaboratorStatus, end);

        List<Facture> invoices = factureService.getFactureByMissionsList(currentMissions);
        List<Mission> missionsWithInvoice = invoices.stream().map(Facture::getMission).collect(Collectors.toList());

        currentMissions.removeAll(missionsWithInvoice);
        return currentMissions;
    }
}
