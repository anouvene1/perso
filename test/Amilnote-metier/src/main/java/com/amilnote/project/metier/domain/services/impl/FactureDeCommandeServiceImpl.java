/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.FacturedeCommandeDAO;
import com.amilnote.project.metier.domain.dao.impl.FacturedeCommandeDAOImpl;
import com.amilnote.project.metier.domain.entities.ami_factureDeCommande;
import com.amilnote.project.metier.domain.services.FacturedeCommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * The type Facturede commande service.
 */
@Service("FactureDeCommandeService")
public class FactureDeCommandeServiceImpl extends AbstractServiceImpl<ami_factureDeCommande, FacturedeCommandeDAOImpl> implements FacturedeCommandeService {

    @Autowired
    private FacturedeCommandeDAO facturedeCommandeDAO;

    /**
     * {@linkplain FacturedeCommandeService#findAllByDate(Date)}
     */
    @Override
    public List<ami_factureDeCommande> findAllByDate(Date date) {
        return facturedeCommandeDAO.findAllBydate(date);
    }

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public FacturedeCommandeDAOImpl getDAO() {
        return null;
    }
}
