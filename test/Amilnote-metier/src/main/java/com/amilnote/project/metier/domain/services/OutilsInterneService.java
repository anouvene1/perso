package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.OutilsInterne;
import com.amilnote.project.metier.domain.entities.json.OutilsInterneAsJson;

import java.util.List;

public interface OutilsInterneService extends AbstractService<OutilsInterne> {

    List<OutilsInterne> getAllOutilsInterne();

    List<OutilsInterneAsJson> getAllOutilsInterneAsJson();
}
