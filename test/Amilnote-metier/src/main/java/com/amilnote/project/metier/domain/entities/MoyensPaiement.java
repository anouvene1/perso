/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities;

import com.amilnote.project.metier.domain.entities.json.MoyensPaiementAsJson;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * The type Moyens paiement.
 */
@Entity
@Table(name = "ami_moyens_paiement")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idPaiement")
public class MoyensPaiement implements java.io.Serializable {

    /**
     * The constant PROP_ID_PAIEMENT.
     */
    public static final String PROP_ID_PAIEMENT = "idPaiement";
    /**
     * The constant PROP_LIBELLE_PAIEMENT.
     */
    public static final String PROP_LIBELLE_PAIEMENT = "libelle_paiement";
    private static final long serialVersionUID = -329072377657745936L;
    private int idPaiement;
    private String libelle_paiement;

	/* CONSTRUCTORS */

    /**
     * Instantiates a new Moyens paiement.
     */
    public MoyensPaiement() {
        super();
    }

    /**
     * Instantiates a new Moyens paiement.
     *
     * @param pId      the p id
     * @param pLibelle the p libelle
     */
    public MoyensPaiement(int pId, String pLibelle) {
        super();
        idPaiement = pId;
        libelle_paiement = pLibelle;
    }

    /**
     * To json moyens paiement as json.
     *
     * @return the moyens paiement as json
     */
    public MoyensPaiementAsJson toJson() {
        return new MoyensPaiementAsJson(this);
    }

	/* GETTERS AND SETTERS */

    /**
     * Gets id paiement.
     *
     * @return the id paiement
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_paiement", unique = true, nullable = false)
    public int getIdPaiement() {
        return idPaiement;
    }

    /**
     * Sets id paiement.
     *
     * @param id the id
     */
    public void setIdPaiement(int id) {
        this.idPaiement = id;
    }


    /**
     * Gets libelle paiement.
     *
     * @return the libelle paiement
     */
    @Column(name = "libelle_paiement")
    public String getLibellePaiement() {
        return libelle_paiement;
    }

    /**
     * Sets libelle paiement.
     *
     * @param libelle the libelle
     */
    public void setLibellePaiement(String libelle) {
        this.libelle_paiement = libelle;
    }

}
