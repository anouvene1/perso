/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.FactureArchivee;
import com.amilnote.project.metier.domain.entities.json.FactureArchiveeAsJson;
import java.util.List;

/**
 * The interface Facture archivee dao.
 */
public interface FactureArchiveeDAO extends LongKeyDAO<FactureArchivee> {

    List<FactureArchivee> findAllFacturesArchivees();

    /**
     * Trouver le max des numéros des factures existantes
     *
     * @return int le max des numéros de facture
     */
    int findMaxNumFacture();
    /**
     * Création ou mise à jour de la commande commandeJson associée à la mission missionJson
     *
     * @param factureArchiveeJson the facture archivee json
     * @return string string
     */
    String createOrUpdateFactureArchivee(FactureArchiveeAsJson factureArchiveeJson);


    /**
     * Find factures archivees list.
     *
     * @return La liste de toutes les facture
     */
    List<FactureArchivee> findFacturesArchivees();

}
