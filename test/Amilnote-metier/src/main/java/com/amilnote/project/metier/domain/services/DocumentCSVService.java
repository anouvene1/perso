package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Facture;
import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.entities.TVA;
import org.joda.time.DateTime;

import javax.naming.NamingException;
import java.io.IOException;
import java.util.List;

/**
 * Interface handling CSV editing
 */
public interface DocumentCSVService {

    String createCSVFilePath(DateTime dateTime, String monthODFPath, String typeFacture);

    String createCSVFileFacture(List<Facture> listFactures, DateTime dateTime, String typeFacture, String monthODFPath);

    String createCSVFileFrais(String dateToParse, String typeFacture, String monthODFPath);

}
