/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.MissionAsJson;

import java.util.List;

/**
 * The type Odm form.
 */
public class ODMForm {

    private List<MissionAsJson> listMissions;

    /**
     * Instantiates a new Odm form.
     */
    public ODMForm() {
    }

    /**
     * Gets list missions.
     *
     * @return the list missions
     */
    public List<MissionAsJson> getListMissions() {
        return listMissions;
    }

    /**
     * Sets list missions.
     *
     * @param pListMissions the p list missions
     */
    public void setListMissions(List<MissionAsJson> pListMissions) {
        listMissions = pListMissions;
    }
}
