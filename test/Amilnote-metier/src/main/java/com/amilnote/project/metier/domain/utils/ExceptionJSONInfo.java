/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

/**
 * The type Exception json info.
 */
public class ExceptionJSONInfo {

    private String url;
    private String message;

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
