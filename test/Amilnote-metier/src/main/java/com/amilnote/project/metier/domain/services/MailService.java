/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.CollaboratorAsJson;
import com.amilnote.project.metier.domain.entities.json.CommandeAsJson;
import com.amilnote.project.metier.domain.utils.Properties;
import org.joda.time.DateTime;

import javax.naming.NamingException;
import java.io.IOException;
import java.util.List;

/**
 * The type Mail service.
 */
public interface MailService {

    /**
     * Envoi mail factures soumises.
     *
     * @param cheminComplet the piece jointe
     * @param typeFacture the type facture
     */
    void envoiMailFacturesSoumises(String cheminComplet, int typeFacture);

    /**
     * Envoi mail factures confirmees ou refusees.
     *
     * @param typeFacture      the type facture
     * @param choix            the choix
     * @param commentaireRefus the commentaire refus
     */
    void envoiMailFacturesConfirmeesOuRefusees(int typeFacture, boolean choix, String commentaireRefus);

    /**
     * Envoi mail rappel date.
     *
     * @param collaborator the collaborateur
     * @param type          the type
     */
    void envoiMailRappelDate(Collaborator collaborator, int type);

    /**
     * Envoi mail rappel ra.
     *
     * @throws NamingException the naming exception
     */
    void envoiMailRappelRA() throws NamingException;

    /**
     * Envoi d'un seul mail limite commande.
     *
     * @param typeMail the type mail
     * @param listeCommandes les commandes
     * @throws NamingException the NamingException
     */
    void envoiMailLimiteDesCommandes(int typeMail, List<Commande> listeCommandes) throws NamingException;

    /**
     * SHA AMNOTE-198 10/01/2017
     * Envoi du mail aux commerciaux pour les facture sans bon de commande.
     *
     * @param pCommandeSansBonCmd the p commande sans bon cmd
     */
    void envoiMailFactureSansBonDeCmd(CommandeAsJson pCommandeSansBonCmd);

    /**
     * Méthode destinée à un envoi de mail asynchrone aux responsables après demande de validation des absences par le collaborateur
     *
     * @param collaborator celui qui demande la validation des absences
     * @param pListId       la liste des id des absences concernées
     * @param pComment      le commentaire entré lors de la soumission des absences
     * @throws IOException  the io exception
     */
    void envoiMailDemandeAbsences(Collaborator collaborator, String pListId, String pComment) throws IOException;

    /**
     * Send a mail for mission prolongation
     *
     * @param mission : modified mission
     */
    void sendMailMissionExtension(Mission mission);

    /**
     * Send a mail for inform a disable collaborator
     * @param collaborator collaborator to disable
     * @throws NamingException NamingException
     */
    void sendMailDesactivationCollab(Collaborator collaborator) throws NamingException;

    /**
     * Méthode permettant d'envoyer la demande de déplacement au collaborateur et aux statuts DRH
     *
     * @param collaborator the collaborateur
     * @param cheminFichier the chemin fichier
     * @param nomFichier    the nom fichier
     */
    void sendMailMovement(Collaborator collaborator, String cheminFichier, String nomFichier);

    /**
     * Send a mail for inform creation of ODM
     * @param collaborator collaborator to be inform
     */
    void sendMailODMCreation(Collaborator collaborator);

    /**
     * Send a mail depending on mission order status (validate / refused)
     *
     * @param collaborator collab concerned by mission order
     * @param mission mission concerned by mission order
     * @param action mission order status (validate or refused)
     * @param commentaire comment for mail content
     * @throws Exception exception
     */
    void sendMailMissionOrder(Collaborator collaborator, Mission mission, String action, String commentaire) throws Exception;

    /**
     * Envoyer mail du rapport d'activités
     *
     * @param pApresSoumissionFacture   the p après soumission ou non boolean
     * @param collaborator Collaborateur à traiter
     * @param cheminFichier Chemin du fichier PDF à envoyer
     * @param nomFichier    Nom du fichier PDF
     * @param pDateVoulue   Date du traitement
     * @param pAvecFrais    the p avec frais
     * @throws IOException        the io exception
     * @throws NamingException    the naming exception
     */
    void sendMailActivityReportSubmit(Collaborator collaborator, String cheminFichier, String nomFichier, DateTime pDateVoulue, boolean pAvecFrais, String pApresSoumissionFacture) throws IOException, NamingException;

    /**
     * Send a mail depending on absence request reply
     *
     * @param state replay status
     * @param absence absence concerned
     * @param connectedCollaborator collab concerned
     * @param pCommentaire comment
     */
    void sendMailAbsenceValidation (String state,Absence absence, Collaborator connectedCollaborator, String pCommentaire);

    /**
     * Send a mail for activity report cancellation
     *
     * @param date date
     * @param commentaire commentary
     * @param collaborateur a {@link CollaboratorAsJson}
     */
    void sendMailActivityReportCanceled(String date, String commentaire, CollaboratorAsJson collaborateur);

    /**
     * Send a mail for collab creation with login and password
     *
     * @param pCollaboratorAsJson a {@link CollaboratorAsJson}
     * @param clearTmpPassword a string
     * @param env env
     */
    void sendMailCreationCollab(CollaboratorAsJson pCollaboratorAsJson, String clearTmpPassword, String env);

    /**
     * Send a mail for mission order validation
     *
     * @param mission {@link Mission}
     */
    void sendMailAdminMissionOrder(Mission mission);

    /**
     * Send a mail for advance submit
     *
     * @param frais frais
     */
    void sendMailAdvanceSubmit(Frais frais);

    /**
     * Send a mail depending on advance request reply
     *
     * @param state reply status
     * @param frais cost
     * @param pCommentaire comment
     */
    void sendMailAdvanceValidation(String state, Frais frais, String pCommentaire);

    /**
     * Send a mail for password reset
     *
     * @param collaborator collab concerned
     * @param env environment (dev or prod)
     * @param newMdp new password
     */
    void sendMailPasswordReset(Collaborator collaborator, String env, String newMdp);

    /**
     * Send a mail depending on move request reply
     *
     * @param state reply status
     * @param connectedCollaborator collab concerned
     * @param deplacement move request
     * @param pCommentaire comment for reply
     */
    void sendMailMovementValidation (String state, Collaborator connectedCollaborator, DemandeDeplacement deplacement, String pCommentaire);

    /**
     * Send a mail after a modification of an addvise session
     *
     * @param properties the properties
     * @param recipients the recipients
     * @param keyObject  the mail title
     * @param keyMessage the mail body
     * @param params     the parameters used to setup the mail
     */
    void sendMailAddviseCollaborator(Properties properties, List<Collaborator> recipients, String keyObject, String keyMessage, String[] params);

}
