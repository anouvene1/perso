/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.MoyensPaiement;
import com.amilnote.project.metier.domain.entities.json.MoyensPaiementAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;

/**
 * The interface Moyens paiement service.
 */
public interface MoyensPaiementService extends AbstractService<MoyensPaiement> {

    /**
     * Création ou mise à jour d'une commande (liée à la mission pMissionJson)
     *
     * @param paiementJson the paiement json
     * @return int int
     * @throws JsonProcessingException the json processing exception
     * @throws IOException             the io exception
     */
    int createOrUpdateMoyensPaiement(MoyensPaiementAsJson paiementJson) throws JsonProcessingException, IOException;

    /**
     * Suppression de la commande
     *
     * @param paiement the paiement
     * @return string string
     */
    String deleteMoyensPaiement(MoyensPaiement paiement);

    /**
     * Find all order by nom asc list.
     *
     * @return the list
     */
    List<MoyensPaiement> findAllOrderByNomAsc();

    /**
     * Find by id moyens paiement.
     *
     * @param pIdFacture the p id facture
     * @return the moyens paiement
     * @deprecated remplacer l'appel a cette méthode par la méthode standard get(Long id) héritée de com.amilnote.project.metier.domain.dao.LongKeyDAO&lt;T&gt;
     */
    @Deprecated
    MoyensPaiement findById(int pIdFacture);

    /**
     * retrouver toutes les commandes et les trier par ordre ascendant au
     * format json
     *
     * @return the list
     */
    List<MoyensPaiementAsJson> findAllOrderByNomAscAsJson();

}
