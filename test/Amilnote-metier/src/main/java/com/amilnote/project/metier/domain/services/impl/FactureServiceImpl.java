/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.FactureDAO;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.FactureAsJson;
import com.amilnote.project.metier.domain.pdf.PDFBuilderFacture;
import com.amilnote.project.metier.domain.services.*;
import com.amilnote.project.metier.domain.utils.*;
import com.itextpdf.text.DocumentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.amilnote.project.metier.domain.entities.Collaborator.*;
import static com.amilnote.project.metier.domain.entities.Etat.ETAT_ARCHIVEE;
import static com.amilnote.project.metier.domain.entities.Facture.*;
import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.STATUT_COLLABORATEUR;
import static com.amilnote.project.metier.domain.entities.StatutCollaborateur.STATUT_SOUS_TRAITANT;
import static com.amilnote.project.metier.domain.entities.TypeFacture.PROP_TYPE_FACTURE;
import static com.amilnote.project.metier.domain.entities.TypeFacture.*;
import static com.amilnote.project.metier.domain.utils.Constantes.*;
import static com.amilnote.project.metier.domain.utils.Utils.*;
import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.*;
import static java.lang.Math.toIntExact;

/**
 * The type Facture service.
 */
@Service("factureService")
public class FactureServiceImpl extends AbstractServiceImpl<Facture, FactureDAO> implements FactureService {

    /**
     * The Facture dao.
     */
    @Autowired
    private FactureDAO factureDAO;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private EtatService etatService;

    @Autowired
    private TypeFactureService typeFactureService;

    @Autowired
    private RapportActivitesService rapportActivitesService;

    @Autowired
    private LinkEvenementTimesheetService linkEvenenementTimesheetService;

    @Autowired
    private ElementFactureService elementFactureService;

    @Autowired
    private TvaService tvaService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private MissionService missionService;

    @Autowired
    private FactureArchiveeService factureArchiveeService;

    @Autowired
    private DocumentExcelService documentExcelService;

    @Autowired
    private MailService mailService;

    @Autowired
    private FileService fileService;

    private static Logger logger = LogManager.getLogger(FactureServiceImpl.class);

    private static final String ETAT_GENERE = "GN";

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public FactureDAO getDAO() {
        return factureDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Facture findById(int pIdFacture) {
        return factureDAO.findUniqEntiteByProp(Facture.PROP_ID_FACTURE, pIdFacture);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Facture> findByCommande(Commande pCommande) {
        return factureDAO.findByCommande(pCommande);
    }

    /**
     * {@linkplain FactureService#findByMissionByMonth(Mission, DateTime)}
     */
    @Override
    public Facture findByMissionByMonth(Mission pMission, DateTime pDate) {
        return factureDAO.findByMissionByMonth(pMission, pDate);
    }

    /**
     * {@linkplain FactureService#findByMissionBetweenDates(Mission, DateTime, DateTime)}
     */
    @Override
    public List<Facture> findByMissionBetweenDates(Mission pMission, DateTime pDateDebut, DateTime pDateFin) {
        return factureDAO.findByMissionBetweenDates(pMission, pDateDebut, pDateFin);
    }

    /**
     * {@linkplain FactureService#findByCommandeByMonth(Commande)}
     */
    @Override
    public Facture findByCommandeByMonth(Commande pCommande) {
        SimpleDateFormat mois = new SimpleDateFormat("MMMM");
        Date today = new Date();
        if (pCommande != null) {
            List<Facture> lListFactures = factureDAO.findListEntitesByProp(Facture.PROP_COMMANDE, pCommande);

            for (Facture facture : lListFactures) {
                //[SHA][AMNOTE-211] si la facture vient juste d'être créé, la date de création n'est pas remonté de la base,
                // on la set donc après la remonté de l'information.
                if (facture.getDateCreation() == null) {
                    facture.setDateCreation(new Date());
                }
                if (mois.format(today).equals(mois.format(facture.getDateCreation()))) {
                    return facture;
                }
            }
        }
        return null;
    }

    /**
     * {@linkplain FactureService#createOrUpdateFacture(Facture)}
     */
    @Override
    public int createOrUpdateFacture(Facture pFacture) throws IOException {
        return factureDAO.createOrUpdateFacture(pFacture);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int createOrUpdateFactureAVANTREFONTE(FactureAsJson pFactureAsJson, Commande pCommande, Boolean edit) {
        return factureDAO.createOrUpdateFactureAVANTREFONTE(pFactureAsJson, pCommande, edit);
    }

    /**
     * {@linkplain FactureService#deleteFacture(Facture)}
     */
    @Override
    public String deleteFacture(Facture pFacture) {
        return factureDAO.deleteFacture(pFacture);
    }

    /**
     * {@linkplain FactureService#findFacturesByTypeAndByRA(TypeFacture, DateTime, boolean, Integer...)}
     */
    @Override
    public List<Facture> findFacturesByTypeAndByRA(TypeFacture factureType, DateTime wantedDate, boolean withYear, Integer... activityReportValidate) {
        DateTime[] startEndDates = new DateTime[2];
        if (withYear) {
            startEndDates[0] = new DateTime(wantedDate.getYear(), 1, 1, 0, 0, 0);
            startEndDates[1] = new DateTime(wantedDate.getYear(), 12, 31, 23, 59, 59);
        } else {
            startEndDates = Utils.firstAndLastDatesOfMonth(wantedDate.getYear(), wantedDate.getMonthOfYear());
        }
        return factureDAO.findFacturesByTypeAndByRA(factureType, startEndDates[0].toDate(), startEndDates[1].toDate(), activityReportValidate);
    }

    /**
     * {@linkplain FactureService#findAllOrderByNomAsc()}
     */
    @Override
    public List<Facture> findAllOrderByNomAsc() {
        return factureDAO.findFactures();
    }

    /**
     * {@linkplain FactureService#findFacturables()}
     */
    @Override
    public List<Facture> findFacturables() {
        return factureDAO.findFacturables();
    }

    /**
     * {@linkplain FactureService#findFacturesRANonSoumis()}
     */
    @Override
    public List<Facture> findFacturesRANonSoumis() {
        return factureDAO.findFacturesRANonSoumis();
    }

    /**
     * {@linkplain FactureService#findFacturesFrais()}
     */
    @Override
    public List<Facture> findFacturesFrais() {
        return factureDAO.findFacturesFrais();
    }

    /**
     * {@linkplain FactureService#findFacturablesByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturablesByMonth(DateTime pDateVoulue) {
        List<Facture> factures = new ArrayList<>();
        try {
            factures = factureDAO.findFacturablesByMonth(pDateVoulue);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        Collections.sort(factures, (f1, f2) -> {
            return f1.compareToByClient(f2);
        });
        return factures;
    }

    /**
     * {@linkplain FactureService#findFacturablesAboveZeroByMonth}
     */
    @Override
    public List<Facture> findFacturablesAboveZeroByMonth(DateTime dateTime) {
        List<Facture> factures = findFacturablesByMonth(dateTime);
        List<Facture> facturesAboveZero = new ArrayList<>();
        for (Facture facture : factures) {
            if (facture.getMontant() > 0)
                facturesAboveZero.add(facture);
        }
        return facturesAboveZero;
    }

    /**
     * {@linkplain FactureService#findFacturesRANonSoumisByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesRANonSoumisByMonth(DateTime pDateVoulue) {
        List<Facture> factures = new ArrayList<>();
        try {
            factures = factureDAO.findFacturesRANonSoumisByMonth(pDateVoulue);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return factures;
    }

    /**
     * {@linkplain FactureService#findFacturesFraisByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesFraisByMonth(DateTime pDateVoulue) {
        List<Facture> factures = new ArrayList<>();
        try {
            factures = factureDAO.findFacturesFraisByMonth(pDateVoulue);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        Collections.sort(factures, (f1, f2) -> {
            return f1.compareToByClient(f2);
        });
        return factures;
    }

    /**
     * {@linkplain FactureService#findFacturesALaMainByMonth(DateTime)}
     */
    @Override
    public List<Facture> findFacturesALaMainByMonth(DateTime pDateVoulue) {
        List<Facture> factures = new ArrayList<>();
        try {
            factures = factureDAO.findFacturesALaMainByMonth(pDateVoulue);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        Collections.sort(factures, (f1, f2) -> {
            return f1.compareToByClient(f2);
        });
        return factures;
    }

    /**
     * {@linkplain FactureService#findMaxNumFacture()}
     */
    @Override
    public int findMaxNumFacture() {
        return factureDAO.findMaxNumFacture();
    }

    /**
     * {@linkplain FactureService#findAllOrderByNomAscAsJson()}
     */
    @Override
    public List<FactureAsJson> findAllOrderByNomAscAsJson() {
        List<Facture> tmpListFacture = this.findAllOrderByNomAsc();
        List<FactureAsJson> tmpListFactureAsJson = new ArrayList<>();
        for (Facture tmpCommande : tmpListFacture) {
            tmpListFactureAsJson.add(tmpCommande.toJson());
        }
        return tmpListFactureAsJson;
    }

    /**
     * {@linkplain FactureService#updateFactures(DateTime, Collaborator)}
     */
    @Override
    public void updateFactures(DateTime dateRAVoulue, Collaborator collaborateur) throws IOException {
        List<LinkEvenementTimesheet> listEvent = linkEvenenementTimesheetService.findEventsBetweenDates(collaborateur,
                dateRAVoulue.dayOfMonth().withMinimumValue().toString(),
                dateRAVoulue.dayOfMonth().withMaximumValue().toString());

        LinkedHashMap<Mission, Double> mapJourMission = linkEvenenementTimesheetService.getMapJourMission(listEvent);

        List<Commande> listCommandesFactures = commandeService.getListCommandeFacture(listEvent);

        for (Mission mission : mapJourMission.keySet()) {
            for (Commande commande : listCommandesFactures) {
                //On vérifit que l'on met bien à jour la bonne commande
                if (commande.getMission() == mission) {
                    //On récupère la facture de cette commande
                    Facture facture = this.findByCommandeByMonth(commande);
                    // Si la facture n'est pas null,
                    // si ce n'est pas un facture vierge
                    // et que le montant de la facture est à 0
                    // ou la quantité est à 0
                    // ou le prix est à 0
                    // et que le TJM de la mission n'est pas null
                    if (facture != null
                            && facture.getTypeFacture().getId() == 0
                            && (facture.getMontant() == Float.valueOf("0.0")
                            || facture.getQuantite() == Float.valueOf("0.0")
                            || facture.getPrix() == Float.valueOf("0.0"))
                            && mission.getTjm() != 0) {
                        if (!facture.getEtat().getCode().equals(Etat.ETAT_EDITE) && !facture.getEtat().getCode().equals(Etat.ETAT_SOUMIS_CODE)) {
                            // Si la commande a un nombre de jours
                            if (commande.getNbJours() != null) {
                                // la quantité de la facture est le NB jours de la commande
                                facture.setQuantite(commande.getNbJours());
                                // Le montant de la facture est la multiplication du nombre de jours et du TJM
                                facture.setMontant(commande.getNbJours() * mission.getTjm());
                            } else { // Si la commande n'a pas de nombre de jours
                                // la quantité de la facture est le NB jours de la mission
                                facture.setQuantite(mapJourMission.get(mission).floatValue());
                                // Le montant de la facture est la multiplication ddu nombre de jours et du TJM
                                facture.setMontant(mapJourMission.get(mission).floatValue() * mission.getTjm());
                            }
                            // Le prix unitaire de la facture est le TJM
                            facture.setPrix(mission.getTjm());

                            if (facture.getCommentaire() == null) {
                                facture.setCommentaire("Frais - " + collaborateur.getPrenom() + " " + collaborateur.getNom() + " : ");
                            }
                            // On met à jour la base
                            this.createOrUpdateFactureAVANTREFONTE(facture.toJson(), commande, false);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Pair<Float, Float> getMontantHT(Facture facture) {
        // [CLO AMNOTE 281 02/05/2017] on va calculer le montant HT total de la facture de frais en parcourant les frais de la facture
        List<ElementFacture> elements = elementFactureService.findByFacture(facture);
        Float montantHT = 0f;
        for (ElementFacture elem : elements) {
            montantHT += elem.getMontantElement();
        }
        Float montantTTC;
        if (facture.getTVA() == null) {
            montantTTC = montantHT + (montantHT * (tvaService.getTVAByCode(TVA.TAUX_NORMAL).getMontant() / 100));
        } else {
            montantTTC = montantHT + (montantHT * (facture.getTVA().getMontant() / 100));
        }
        return new Pair<>(montantHT, montantTTC);
    }

    /**
     * {@linkplain FactureService#createFactureFromMission(Mission, TVA)}
     */
    @Override
    public Facture createFactureFromMission(Mission mission, TVA tva) {
        String adresseFacturation;
        int responsableFacturation = 0;
        String descriptif;
        Float tjm;
        Float quantite;
        Collaborator collaborateur;
        int raValide;
        Client client;
        String rib = "";
        Boolean avoir = Boolean.FALSE;

        Etat etatCommence = etatService.getEtatByCode(ETAT_GENERE);
        TypeFacture typeFacture = typeFactureService.findByCode(TypeFacture.TYPEFACTURE_FACTURABLE);
        DateTime dateVoulue = new DateTime(Calendar.getInstance().getTime()); //date courante pour le moment


        List<Commande> commandes = commandeService.findByIdMissionByMonth(mission, dateVoulue);
        Commande commande = null;

        DateTime dateFacturation = new DateTime();
        dateFacturation = dateFacturation.dayOfMonth().withMaximumValue();
        dateFacturation = getMostRecentWorkingDay(dateFacturation);


        int indice = 0;
        if (!commandes.isEmpty()) {
            commande = commandes.get(indice);

            client = commande.getClient();

            // Remplissage du descriptif
            if (commande.getCommentaire() != null && !commande.getCommentaire().equals("")) {
                descriptif = commande.getCommentaire();
            } else {
                descriptif = mission.getDescription();
            }

            tjm = mission.getTjm();
        } else {
            descriptif = "Suivant proposition commerciale";
            client = mission.getClient();

            tjm = mission.getTjm();
        }

        adresseFacturation = client.getAdresseFacturation();

        if (commande != null && commande.getIdResponsableFacturation() > 0) {
            responsableFacturation = toIntExact(commande.getIdResponsableFacturation());
        } else {
            responsableFacturation = clientService.getResponsableFacturation(client);
        }

        collaborateur = mission.getCollaborateur();
        if (client.getRibClient() != null) {
            rib = client.getRibClient();
        }

        Facture facture;

        TVA appliedTvaRate;


        if(mission.getClient().getTva().contains(Constantes.FRENCH_INTRA_COMMUNITY_ACCOUNT_NUMBER_START)){

            appliedTvaRate = (tva == null) ? tvaService.getTVAByCode(TVA.TAUX_NORMAL) : tva;
        }
        else{

            appliedTvaRate = tvaService.getTVAByCode(TVA.TAUX_ZERO);
        }

        List<RapportActivites> listRA = rapportActivitesService.getExistingRAForMonth(collaborateur, dateVoulue);
        if (!listRA.isEmpty()) {
            quantite = rapportActivitesService.getNbJoursTravailles(collaborateur, mission, dateVoulue);
            raValide = 1;
            facture = new Facture(mission, commande, adresseFacturation, mission.getClient().getId().intValue(), quantite, tjm, quantite * tjm,
                    responsableFacturation, etatCommence, "", dateFacturation.toDate(), Calendar.getInstance().getTime(), rib, descriptif, 0,
                    Calendar.getInstance().getTime(), typeFacture, raValide, avoir, appliedTvaRate);
        } else {
            quantite = 0f;
            raValide = 0;
            facture = new Facture(mission, commande, adresseFacturation, mission.getClient().getId().intValue(), quantite, tjm, quantite * tjm,
                    responsableFacturation, etatCommence, "", dateFacturation.toDate(), Calendar.getInstance().getTime(), rib, descriptif, 0,
                    Calendar.getInstance().getTime(), typeFacture, raValide, avoir, appliedTvaRate);
        }
        return facture;
    }

    @Override
    public Pair<Float, Float> getQuantiteMontantAvecFrais(Facture facture) {
        Float quantite = facture.getQuantite();
        Float montant = facture.getMontant();

        List<ElementFacture> listeElementFacture = elementFactureService.findByFacture(facture);

        if (!listeElementFacture.isEmpty()) {
            for (ElementFacture elementFacture : listeElementFacture) {
                // Si le libellé de l'element ne contient pas "Frais :", on peut modifier la quantité
                if (elementFacture.getQuantiteElement() != 0 && !(elementFacture.getLibelleElement().toLowerCase()).contains("frais")) {
                    quantite += elementFacture.getQuantiteElement();
                }
                if (elementFacture.getMontantElement() != 0) {
                    montant += elementFacture.getMontantElement();
                }
            }
        }
        return new Pair<>(quantite, montant);
    }

    @Override
    public Float getMontantFrais(Facture facture) {

        Float montant = 0.0f;

        List<ElementFacture> listeElementFacture = elementFactureService.findByFacture(facture);

        if (!listeElementFacture.isEmpty()) {
            for (ElementFacture elementFacture : listeElementFacture) {
                montant += elementFacture.getMontantElement();
            }
        }
        return montant;
    }

    @Override
    public Boolean checkExistsNumFacture(int numFacture, int idFacture) throws IOException {
        return factureDAO.checkExistsNumFacture(numFacture, idFacture);
    }

    // [CLO AMNOTE-285]

    /**
     * Dans les factures, on veut afficher comme date, le dernier jour ouvrable du mois
     * Cette méthode sert à renvoyer le plus récent jour ouvrable depuis le jour donnée en argument (si c'est un jour
     * ouvrable, pas de changement, et si c'est un samedi ou un dimanche, on renvoit le dernier vendredi)
     *
     * @param dateOuvree le jour à convertir
     * @return le plus récent jour ouvrable
     */
    @Override
    public DateTime getMostRecentWorkingDay(DateTime dateOuvree) {
        DateTime dateOuvrable = dateOuvree;
        switch (dateOuvree.getDayOfWeek()) {
            case DateTimeConstants.SUNDAY:
                dateOuvrable = dateOuvrable.withDayOfMonth(dateOuvree.getDayOfMonth() - 2);
                break;
            case DateTimeConstants.SATURDAY:
                dateOuvrable = dateOuvrable.withDayOfMonth(dateOuvree.getDayOfMonth() - 1);
                break;
        }
        return dateOuvrable;
    }

    @Override
    public String findTypeFactureById(Integer idTypeFacture, Long idMission) {

        String redir = "";

        if (idTypeFacture != null && idMission != null) {
            TypeFacture typeFacture = typeFactureService.findById(new Long(idTypeFacture));
            Mission mission = missionService.get(idMission);
            Collaborator collaborateur = mission.getCollaborateur();
            String codeFacture = typeFacture.getCode();
            String codeStatutCollab = mission.getCollaborateur().getStatut().getCode();

            // vérifie quelle facture a été traitée afin de renvoyer le bon #hash pour la redirection
            if ((TYPEFACTURE_FACTURE_FRAIS).equals(codeFacture)) {
                redir = TypeFacture.TYPEFACTURE_FACTURE_FRAIS_HASH;         // si c'est une facture de frais
            } else if ((TypeFacture.TYPEFACTURE_FACTURABLE).equals(codeFacture) && (STATUT_COLLABORATEUR).equals(codeStatutCollab)) {
                redir = TypeFacture.TYPEFACTURE_FACTURABLE_HASH;    // si c'est une facture collaborateur
            } else if ((TYPEFACTURE_FACTURE_A_LA_MAIN).equals(codeFacture) && (STATUT_COLLABORATEUR).equals(codeStatutCollab)) {
                redir = TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_HASH;     // si c'est une facture collaborateur faite à la main
            } else if ((TypeFacture.TYPEFACTURE_FACTURABLE).equals(codeFacture) && (STATUT_SOUS_TRAITANT).equals(codeStatutCollab)) {
                redir = TypeFacture.TYPEFACTURE_FACTURABLE_STT_HASH;    // si c'est une facture sous-traitant
            } else if ((TYPEFACTURE_FACTURE_A_LA_MAIN).equals(codeFacture) && (STATUT_SOUS_TRAITANT).equals(codeStatutCollab)) {
                redir = TypeFacture.TYPEFACTURE_FACTURE_A_LA_MAIN_STT_HASH;     // si c'est une facture sous-traitant faite à la main
            }
        }
        return redir;
    }

    public void createFactureAfterCron(Mission missionFacture) throws IOException {
        // [SBE AMNT-590]Création d'une facture à la validation du nouvel ordre de mission
        if (missionFacture.getTypeMission().getCode().equals("MI")) {
            Boolean deuxiemePartieDuMois = false;
            Boolean missionEnCours = false;

            DateTime dateLimite = new DateTime();
            DateTime dateDebutMois = new DateTime();
            DateTime dateFinMois = new DateTime();
            dateLimite = dateLimite.withDayOfMonth(16).withHourOfDay(8).withMinuteOfHour(0).withSecondOfMinute(0);
            dateFinMois = dateFinMois.dayOfMonth().withMaximumValue().hourOfDay().withMaximumValue()
                    .minuteOfHour().withMaximumValue().secondOfMinute().withMaximumValue();
            dateDebutMois = dateDebutMois.dayOfMonth().withMinimumValue().hourOfDay().withMinimumValue()
                    .minuteOfHour().withMinimumValue().secondOfMinute().withMinimumValue();

            DateTime dateCourante = new DateTime();

            if (dateLimite.isBeforeNow()) {
                deuxiemePartieDuMois = true;
            }

            // LA mission est en cours
            if (dateDebutMois.isBefore(missionFacture.getDateFin().getTime()) && dateFinMois.isAfter(missionFacture.getDateDebut().getTime())) {
                missionEnCours = true;
            }

            if (deuxiemePartieDuMois && missionEnCours) {
                Facture facture = this.findByMissionByMonth(missionFacture, dateCourante);
                if (facture == null) {
                    facture = this.createFactureFromMission(missionFacture, null);
                }
                this.createOrUpdateFacture(facture);
            }
        }
    }

    /**
     * Method for retrieving the list of all invoices whose date is in a given month, and whose state is part of a given set of states
     *
     * @param dateOfSearch          the date of the month of search
     * @param propertyNameToOrderBy the result list will be ordered by this property
     * @param codesEtatsFactures    list of invoices states codes, "BR" Brouillon, "SO" Soumis, "VA" Validé, etc.
     * @return all invoices whose date is in a given month, and whose state is part of a given set of states
     */
    @Override
    public List<Facture> findFactureByMonthAndState(DateTime dateOfSearch, String propertyNameToOrderBy, String... codesEtatsFactures) {
        return factureDAO.findFactureByMonthAndState(dateOfSearch, propertyNameToOrderBy, codesEtatsFactures);
    }

    @Override
    public float calculateTotalFactureAndElementFactureHt(Facture facture) {
        float totalInvoiceElements = getMontantFrais(facture);
        float totalInvoice = facture.getMontant();
        return totalInvoiceElements + totalInvoice;
    }

    @Override
    public float calculateTotalFactureAndElementFactureTva(Facture facture) {
        float totalInvoice = calculateTotalFactureAndElementFactureHt(facture);
        float amountTva = facture.getTVA().getMontant();
        return totalInvoice * (amountTva / 100);
    }

    @Override
    public float calculateTotalFactureAndElementFactureTtc(Facture facture) {
        float totalFactureHT = calculateTotalFactureAndElementFactureHt(facture);
        float tvaFacture = calculateTotalFactureAndElementFactureTva(facture);
        return totalFactureHT + tvaFacture;
    }

    /**
     * Order the invoices by number
     *
     * @param listeFactures list of the invoices
     */
    @Override
    public void orderFacturesByNumber(List<Facture> listeFactures) {
        Collections.sort(listeFactures, (Facture o1, Facture o2) -> {
            if (o1.getNumFacture() < o2.getNumFacture()) {
                return -1;
            } else if (o1.getNumFacture() == o2.getNumFacture()) {
                return 0;
            } else return 1;
        });
    }

    /**
     * To find worked days (quantite)
     *
     * @param facture current invoice from the chosen list
     * @return float with the number of worked days
     */
    @Override
    public float calculateWorkedDaysforInvoicesExcel(Facture facture) {
        Pair<Float, Float> workedDaysOrAmount = getQuantiteMontantAvecFrais(facture);
        float varWorkedDays = workedDaysOrAmount.getFirst();
        return varWorkedDays;
    }

    /**
     * To find amount (montant HT)
     *
     * @param facture current invoice from the chosen list
     * @return float with the amount element
     */
    @Override
    public float calculateAmountforInvoicesExcel(Facture facture) {
        Pair<Float, Float> workedDaysOrAmount = getQuantiteMontantAvecFrais(facture);
        float varAmount = workedDaysOrAmount.getSecond();
        return varAmount;
    }

    /**
     * Method for retrieving all invoices by given collaborator status code and one or multiple {@link SearchCriteria}
     *
     * @param collaborateurStatusCode the status code of the collaborator
     * @param orderingProperty        the resulting list will be ordered by this property
     * @param isAscendingOrdering     is ordering ascending or descending
     * @param searchCriterias         one or multiple (varargs) {@link SearchCriteria}
     * @return a list of all invoices corresponding to a given collaborator status code
     */
    @Override
    public List<Facture> findByCollaboratorStatusCodeAndSearchCriterias(
            String collaborateurStatusCode,
            String orderingProperty,
            Boolean isAscendingOrdering,
            SearchCriteria... searchCriterias
    ) {
        return factureDAO.findByCollaboratorStatusCodeAndSearchCriterias(collaborateurStatusCode, orderingProperty, isAscendingOrdering, searchCriterias);
    }

    /**
     * Validate all invoices af a given month and year which consist of setting invoices states to 10 (archived), generate and save corresponding pdf, persist corresponding archived invoices
     *
     * @param monthOfSearch      all invoices which {@link Facture#getDateFacture()} 's month correspond to this month of search
     * @param yearOfSearch       all invoices which {@link Facture#getDateFacture()} 's year correspond to this year of search
     * @param invoiceTypeId      the type of the invoice
     * @param collaboratorStatus the status of the collaborator , 'collaborator' or 'subcontractor
     * @return true if there is at least one validated invoices, else return false if no validated invoice
     * @throws IOException            Local/Network File/Folder read/write issues
     * @throws DocumentException      issues related to pdf/excel read/write
     * @throws NamingException        naming exception
     * @throws InvalidFormatException invalid format exception for method arguments
     * @throws ParseException         parsing exception
     */
    public boolean validateInvoices(String monthOfSearch, String yearOfSearch, long invoiceTypeId, String collaboratorStatus) throws IOException, DocumentException, NamingException, InvalidFormatException, ParseException {

        // On définit le chemin et si les dossiers n'existent pas, on les crée
        String resultDirectoryPath = concatenateObjectsToString(
                Parametrage.getContext(PDFBuilderFacture.PDF_FACTURES_CONTEXT_ROOT),
                yearOfSearch,
                SLASH,
                Utils.moisIntToString(Integer.valueOf(monthOfSearch) - 1)
        );

        fileService.createFolder(resultDirectoryPath);

        File existingResultDirectory = new File(resultDirectoryPath);
        String[] existingODFDirectories = existingResultDirectory.list(); // Liste des ODF déjà crée

        //Si aucun ODF n'existent on cree ODF1 sinon on incremente le dernier
        String odfFolderName = concatenateObjectsToString(
                "ODF",
                existingODFDirectories != null ? existingODFDirectories.length + 1 : 1
        );

        resultDirectoryPath = concatenateObjectsToString(
                resultDirectoryPath,
                SLASH,
                odfFolderName
        );

        TypeFacture typeFactureOfSearch = typeFactureService.get(invoiceTypeId);
        String collaboratorStatusCode = null;

        switch (collaboratorStatus) {
            case COLLABORATOR_LABEL:
                collaboratorStatusCode = STATUT_COLLABORATEUR;
                break;
            case SUBCONTRACTOR_LABEL:
                collaboratorStatusCode = STATUT_SOUS_TRAITANT;
                break;
            default:
                break;
        }

        DateTime[] startEndSearchDates = Utils.firstAndLastDatesOfMonth(Integer.parseInt(yearOfSearch), Integer.parseInt(monthOfSearch));
        List<Facture> invoices;

        if (typeFactureOfSearch.getCode().equals(TYPEFACTURE_FACTURE_FRAIS)) {
            invoices = findBySearchCriterias(
                    Facture.PROP_NUMFACTURE,
                    true,
                    new SearchCriteria<>(Facture.PROP_DATE_FACTURATION, BETWEEN, startEndSearchDates[0].toDate(), startEndSearchDates[1].toDate()),
                    new SearchCriteria<>(Facture.PROP_TYPE_FACTURE, EQUAL, typeFactureOfSearch),
                    new SearchCriteria<>(Facture.PROP_ETAT, NOT_EQUAL, etatService.getEtatByCode(Etat.ETAT_ARCHIVEE))
            );
        } else {
            invoices = findByCollaboratorStatusCodeAndSearchCriterias(
                    collaboratorStatusCode,
                    Facture.PROP_NUMFACTURE,
                    true,
                    new SearchCriteria<>(Facture.PROP_DATE_FACTURATION, BETWEEN, startEndSearchDates[0].toDate(), startEndSearchDates[1].toDate()),
                    new SearchCriteria<>(Facture.PROP_TYPE_FACTURE, EQUAL, typeFactureOfSearch),
                    new SearchCriteria<>(Facture.PROP_ETAT, NOT_EQUAL, etatService.getEtatByCode(Etat.ETAT_ARCHIVEE))
            );
        }

        // La fonction ArchivageFactures permet de passer la facture à l'état archivée, en enregistrant le chemin de son PDF et en créant ce dernier
        for (Facture facture : invoices) {
            factureArchiveeService.archiveInvoice(resultDirectoryPath, facture, startEndSearchDates[0], odfFolderName);
        }

        mailService.envoiMailFacturesConfirmeesOuRefusees(typeFactureOfSearch.getId().intValue(), true, null);

        if (invoices.isEmpty()) {
            Files.delete(new File(resultDirectoryPath).toPath()); // On supprime le dossier ODF vide
            return false;
        } else {
            //création de l'excel de récap des factures de l'ODF
            String summuryWorkBookFilePath = concatenateObjectsToString(
                    yearOfSearch,
                    SLASH,
                    Utils.moisIntToString(Integer.valueOf(monthOfSearch) - 1),
                    SLASH,
                    odfFolderName
            );

            // on créé l'Excel de l'ODF avec la liste des factures générées en PDF
            if (!typeFactureOfSearch.getCode().equals(TYPEFACTURE_FACTURE_FRAIS))
                documentExcelService.createExcelRecapFacturesMois(invoices, startEndSearchDates[0], summuryWorkBookFilePath, null, typeFactureOfSearch.getCode());
            else
                documentExcelService.createExcelRecapFacturesFraisMois(invoices, startEndSearchDates[0], summuryWorkBookFilePath, null);

            // Si on est en prod on supprime les factures
            // TODO Ici si on est dans l'environnement prod 'env.prod' une fois que les factures editables sont archivées on supprime ses elements puis les factures, es ce pertinent de faire cela? car la table des factures archivées ne contient pas autant d'informations que dans facture, on perd des informations
            if (Boolean.valueOf(Parametrage.getContext("env.prod"))) {
                invoices.forEach(invoice -> {
                    elementFactureService.deleteInvoiceElements(elementFactureService.findByFacture(invoice));
                    deleteFacture(invoice);
                });
            }
            return true;
        }
    }

    /**
     * {@linkplain FactureService#calculateInvoicesTotalAmount(boolean, Facture...)}
     */
    @Override
    public double calculateInvoicesTotalAmount(boolean withVAT, Facture... invoices) {
        double result = 0;
        for (Facture invoice : invoices) {
            if (withVAT) {
                result += calculateTotalAmountWithVAT(
                        calculateInvoicesTotalAmount(false, invoice),
                        invoice.getTVA().getMontant()
                );
            } else {
                Pair<Float, Float> workedDaysOrAmount = getQuantiteMontantAvecFrais(invoice);
                result += workedDaysOrAmount.getSecond();
            }
        }
        return result;
    }

    /**
     * {@linkplain FactureService#getFactureByMissionsList(List)}
     */
    @Override
    public List<Facture> getFactureByMissionsList(List<Mission> missions) {
        return factureDAO.findBillByMissionList(missions);
    }

    /**
     * {@linkplain FactureService#calculateTotalAmountWithVAT(double, double)}
     */
    @Override
    public double calculateTotalAmountWithVAT(double preTaxAmount, double valueAddedTax) {
        return preTaxAmount * (1 + (valueAddedTax / 100));
    }

    /**
     * {@linkplain FactureService#assignNumbersToInvoices(int, int, String, String, String)}
     */
    public boolean assignNumbersToInvoices(int invoiceTypeId, int invoiceNumber, String collaboratorStatus, String yearOfSearch, String monthOfSearch) throws IOException {

        String invoiceTypeCode = "";
        List<Facture> invoicesListTemp = new ArrayList<>();
        DateTime dateOfSearch = extractPickedDate(monthOfSearch, yearOfSearch);

        TypeFacture invoiceType = typeFactureService.findById(invoiceTypeId);

        if (collaboratorStatus.equals(COLLABORATORS_FR_LOWER_CASE)) {
            invoiceTypeCode = STATUT_COLLABORATEUR;
        } else if (collaboratorStatus.equals(SUBCONTRACTORS_FR_SNAKE_CASE)) {
            invoiceTypeCode = STATUT_SOUS_TRAITANT;
        }

        DateTime[] startEndSearchDateTimes = firstAndLastDatesOfMonth(
                Integer.parseInt(yearOfSearch),
                Integer.parseInt(monthOfSearch)
        );

        switch (invoiceType.getCode()) {
            case TYPEFACTURE_FACTURABLE:
                invoicesListTemp = findUnarchivedInvoicesByTypeAndValidRa(
                        startEndSearchDateTimes[0].toDate(),
                        startEndSearchDateTimes[1].toDate(),
                        typeFactureService.findByCode(TYPEFACTURE_FACTURABLE),
                        1, 2, -1
                );
                break;
            case TYPEFACTURE_FACTURE_FRAIS:
                invoicesListTemp = findFacturesFraisByMonth(dateOfSearch);
                break;
            case TYPEFACTURE_FACTURE_A_LA_MAIN:
                invoicesListTemp = findFacturesALaMainByMonth(dateOfSearch);
                break;
            default:
                break;
        }

        List<Facture> invoicesList = filterInvoicesListsByInvoiceType(invoiceTypeCode, invoiceType, invoicesListTemp);

        int maxNumFacture = 0;

        //Sort the bills list by client.
        invoicesList.sort(Facture::compareToByClient);

        for (Facture facture : invoicesList) {
            if (maxNumFacture < facture.getNumFacture()) {
                maxNumFacture = facture.getNumFacture();
            }
        }

        int maxNumeroFacture = findMaximumInvoiceNumber();

        if (maxNumeroFacture < invoiceNumber) {
            for (Facture facture : invoicesList) {
                if (facture.getNumFacture() == 0) {
                    facture.setNumFacture(invoiceNumber);
                    createOrUpdateFacture(facture);
                    invoiceNumber++;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * filter invoices lists by a given invoice type
     *
     * @param invoiceTypeCode  the lists will be compared to this invoice type code
     * @param invoiceType      the lists filtering will be based on this invoice type
     * @param invoicesListTemp the filtering will be applied to this list
     * @return a list of invoices resulting from the filtering of a given invoice list
     */
    private List<Facture> filterInvoicesListsByInvoiceType(String invoiceTypeCode, TypeFacture invoiceType, List<Facture> invoicesListTemp) {

        List<Facture> invoicesList = new ArrayList<>();

        if (invoiceTypeCode.equals("")) {
            return invoicesList;
        }

        if (invoiceType.getCode().equals(TYPEFACTURE_FACTURE_FRAIS)) {
            return invoicesListTemp;
        }

        final boolean isSubcontractor = invoiceTypeCode.equals(STATUT_SOUS_TRAITANT);
        for (Facture fact : invoicesListTemp) {
            final String statusCode = fact.getMission().getCollaborateur().getStatut().getCode();
            if ((isSubcontractor && statusCode.equals(invoiceTypeCode)) || (!isSubcontractor && !statusCode.equals(STATUT_SOUS_TRAITANT))
            ) {
                invoicesList.add(fact);
            }
        }

        return invoicesList;
    }

    /**
     * Method to find the maximum invoice number in database
     *
     * @return an int value of the maximum invoice number in database
     */
    private int findMaximumInvoiceNumber() {
        int max1 = findMaxNumFacture();
        int max2 = factureArchiveeService.findMaxNumFacture();
        return max1 > max2 ? max1 : max2;
    }

    /**
     * {@linkplain FactureService#findUnarchivedInvoicesByTypeAndValidRa(Date, Date, TypeFacture, Integer...)}
     */
    public List<Facture> findUnarchivedInvoicesByTypeAndValidRa(Date startSearchDate, Date endSearchDate, TypeFacture invoiceType, Integer... raValid) {
        return findBySearchCriterias(
                PROP_NUMFACTURE,
                true,
                new SearchCriteria<>(PROP_DATE_FACTURATION, BETWEEN, startSearchDate, endSearchDate),
                new SearchCriteria<>(PROP_TYPE_FACTURE, EQUAL, invoiceType),
                new SearchCriteria<>(PROP_RA_VALIDE, IN, raValid),
                new SearchCriteria<>(PROP_ETAT, NOT_EQUAL, etatService.getEtatByCode(ETAT_ARCHIVEE))
        );
    }

    /**
     * {@linkplain FactureService#sortInvoicesByClient(List)}
     */
    @Override
    public List<Facture> sortInvoicesByClient(List<Facture> invoiceList){

        invoiceList.sort(Facture::compareToByClient);
        return invoiceList;

    }

}
