/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.dao.impl.CollaboratorDAOImpl;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Poste;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;

import java.util.List;

/**
 * The interface Collaborateur dao.
 */
public interface CollaboratorDAO extends TravailleurDAO<Collaborator> {

    /**
     * To find all the @{@link Collaborator} in the Database ordered by Nom ASC.
     *
     * @param isWithAdmin    should include admins in the request.
     * @param isWithDisabled should include disabled users in the request.
     * @return the List containing the wanted Collaborateurs.
     */
    List<Collaborator> findCollaborators(boolean isWithAdmin, boolean isWithDisabled);


    /**
     * To find all the @{@link Collaborator} in the Database ordered by Nom ASC.
     *
     * @return the List containing Collaborateurs that can be admin and that are NOT disabled.<br>
     *     This is a syntaxic suggar for findCollaborateurs(true, false) taht is used in
     *     Abstraction related to inheritance with AbstractTravailleur
     */
    @Override
    List<Collaborator> findTravailleurs();

    /**
     * Retrieve all the Travailleur who have the same Responsable
     *
     * @param responsable the responsable
     * @return a list of {@link Collaborator}
     */
    List<Collaborator> findCollaboratorsByResponsable(Collaborator responsable);


    /**
     * Retourne la liste de tous les ingénieurs d'affaires
     * @param statutManager {@link StatutCollaborateur} manager
     * @param posteManager {@link Poste} manager
     * @return une liste de {@link Collaborator}
     */
    List<Collaborator> findAllManagers(StatutCollaborateur statutManager, Poste posteManager);

    /**
     * Retourne la liste de tous les responsables technique
     * @param statutResponsableTechnique {@link StatutCollaborateur} responsable technique
     * @return une liste de {@link Collaborator}
     */
    List<Collaborator> findAllResponsablesTechnique(StatutCollaborateur statutResponsableTechnique);

    /**
     * Retourne la liste de tous les chefs de projets
     * @param statutChefDeProjet {@link StatutCollaborateur} statut de chef de projet
     * @return une liste de {@link Collaborator}
     */
    List<Collaborator> findAllChefDeProjet(StatutCollaborateur statutChefDeProjet);

    /**
     * Get a list collaborators by status and enabled or not
     * {@linkplain CollaboratorDAOImpl#findCollaboratorByStatus(boolean, StatutCollaborateur...)}
     */
    List<Collaborator> findCollaboratorByStatus(boolean enabled, StatutCollaborateur... status);
}
