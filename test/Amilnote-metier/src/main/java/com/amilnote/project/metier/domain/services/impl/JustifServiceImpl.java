/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.JustifDAO;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.Frais;
import com.amilnote.project.metier.domain.entities.Justif;
import com.amilnote.project.metier.domain.services.FileService;
import com.amilnote.project.metier.domain.services.FraisService;
import com.amilnote.project.metier.domain.services.JustifService;
import com.amilnote.project.metier.domain.utils.Constantes;
import com.amilnote.project.metier.domain.utils.Parametrage;
import com.amilnote.project.metier.domain.utils.UploadFile;
import com.amilnote.project.metier.domain.utils.enumerations.FileFormatEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.NamingException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The type Justif service.
 */
@Repository("justifService")
public class JustifServiceImpl extends AbstractServiceImpl<Justif, JustifDAO> implements JustifService {

    private static final Logger logger = LogManager.getLogger(JustifServiceImpl.class);

    @Autowired
    private JustifDAO justifDAO;

    @Autowired
    private FraisService fraisService;

    @Autowired
    private FileService fileService;

    /**
     * {@linkplain JustifService#getAllJustif()}
     */
    @Override
    public List<Justif> getAllJustif() {
        return justifDAO.loadAll();
    }

    /**
     * {@linkplain JustifService#getJustifById(long)}
     */
    @Override
    public Justif getJustifById(long pId) {
        return justifDAO.get(pId);
    }

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public JustifDAO getDAO() {
        return justifDAO;
    }

    /**
     * {@linkplain JustifService#getAllJustifForCollaborator(Collaborator)}
     */
    @Override
    public List<Justif> getAllJustifForCollaborator(Collaborator collaborateur) {
        return justifDAO.findListEntitesByProp("collaborateur", collaborateur);
    }

    /**
     * {@linkplain JustifService#saveNewAssocFraisJustif(Long, Long)}
     */
    @Override
    public void saveNewAssocFraisJustif(Long pIdFrais, Long pIdJustif) {
        // --- Création du justificatif
        Justif newJustif = justifDAO.findUniqEntiteByProp("id", pIdJustif);

        //Récupération du frais
        Frais frais = fraisService.get(pIdFrais);

        // --- Création d'une liste de frais
        List<Frais> listFrais = new ArrayList<>();
        listFrais.add(frais);

        // --- Sauvegarde du justificatif dans le frais
        List<Justif> listJustif = frais.getJustifs();
        listJustif.add(newJustif);
        frais.setJustifs(listJustif);
        fraisService.saveOrUpdate(frais);
    }

    /**
     * {@linkplain JustifService#saveNewJustifAndApplyToFrais(String, String, MultipartFile, Long, Collaborator)}
     */
    @Override
    public void saveNewJustifAndApplyToFrais(String path, String fileName, MultipartFile file, Long idFrais, Collaborator collaborateur) throws Exception {

        if (null == fileName || null == file || null == collaborateur) {
            throw new Exception("Une erreur est survenue lors de la création d'un nouveau justificatif.");
        }

        Optional<String> fileExtention = UploadFile.retrieveExtension(file);
        if (!fileExtention.isPresent()) {
            fileExtention.orElse(" ");
        }

        final String fileNameToBackup = collaborateur.getId() + "_" + DateTime.now().getMillis() + fileExtention.get();
        Frais frais;
        if (idFrais == null) {
            frais = null;
        } else {
            frais = fraisService.get(idFrais);
        }

        if (null != frais && null != collaborateur) {
            List<Frais> listFrais = new ArrayList<>();
            listFrais.add(frais);
            Justif newJustif = new Justif(fileName, fileNameToBackup, listFrais);
            newJustif.setCollaborator(collaborateur);
            List<Justif> listJustif = frais.getJustifs();
            if (listJustif == null) {
                listJustif = new ArrayList<>();
            }
            listJustif.add(newJustif);
            writeFileOnServer(file, path, fileNameToBackup);
            frais.setJustifs(listJustif);
            fraisService.saveOrUpdate(frais);
        } else if (frais == null) {
            Justif proof = new Justif();
            proof.setCollaborator(collaborateur);
            proof.setFichier(fileNameToBackup);
            proof.setDescriptif(fileName);
            Session session = justifDAO.getCurrentSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(proof);
            transaction.commit();
            session.flush();
            writeFileOnServer(file, path, fileNameToBackup);
        }
    }

    /**
     * {@linkplain JustifService#deleteJustif(Justif)}
     */
    @Override
    public String deleteJustif(Justif pJustif) {

        Boolean suppressionFichier = false;

        File fichierASupprimer = null;
        try {
            fichierASupprimer = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + pJustif.getFichier());
        } catch (NamingException e) {
            logger.error("[delete supporting doc] parameter context name error", e);
        }
        if (fichierASupprimer.exists()) {
            suppressionFichier = fichierASupprimer.delete();
        } else {
            try {
                fichierASupprimer = new File(Parametrage.getContext(Constantes.CONTEXT_FOLDER_FRAIS) + pJustif.getFichier().substring(0, (pJustif.getFichier()).lastIndexOf(".")) + Constantes.EXTENSION_FILE_ZIP);

            } catch (NamingException e) {
                logger.error("[delete supporting doc] parameter context name error", e);
            }
            if (fichierASupprimer.exists()) {
                suppressionFichier = fichierASupprimer.delete();
            }
        }
        if (suppressionFichier) {


            return justifDAO.deleteFrais(pJustif);
        } else {
            return "Le justificatif " + pJustif.getFichier() + " n'a pas pu être supprimer du serveur. Veuillez contacter l'administrateur avec le nom du fichier ci-avant.";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Justif> findJustifByIdFrais(Long pIdFrais) {
        return justifDAO.findJustifByIdFrais(pIdFrais);
    }

    /**
     * @param file
     * @param filenameToSave
     * @throws Exception
     */
    private void writeFileOnServer(MultipartFile file, String path, String filenameToSave) throws Exception {
        fileService.uploadFile(file, path + filenameToSave, UploadFile.MAX_SIZE_JUSTIF_FRAIS, FileFormatEnum.values());
        fileService.convertFileToZip(filenameToSave, Constantes.CONTEXT_FOLDER_FRAIS);
        fileService.deleteFile(filenameToSave, Constantes.CONTEXT_FOLDER_FRAIS);
    }
}
