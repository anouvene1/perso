package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Frequence;

public interface FrequenceService extends AbstractService<Frequence> {
}
