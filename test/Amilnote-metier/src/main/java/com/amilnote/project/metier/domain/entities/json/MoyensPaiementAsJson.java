/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.MoyensPaiement;


/**
 * The type Moyens paiement as json.
 */
public class MoyensPaiementAsJson {

    private int idPaiement;
    private String libelle_paiement;


    /**
     * Instantiates a new Moyens paiement as json.
     *
     * @param paiement the paiement
     */
    public MoyensPaiementAsJson(MoyensPaiement paiement) {
        this.setIdPaimenent(paiement.getIdPaiement());
        this.setLibellePaiement(paiement.getLibellePaiement());
    }

    /**
     * Instantiates a new Moyens paiement as json.
     */
    public MoyensPaiementAsJson() {
    }

    /**
     * Gets id paiement.
     *
     * @return the id paiement
     */
    public int getIdPaiement() {
        return idPaiement;
    }

    /**
     * Sets id paimenent.
     *
     * @param id the id
     */
    public void setIdPaimenent(int id) {
        this.idPaiement = id;
    }

    /**
     * Gets libelle paiement.
     *
     * @return the libelle paiement
     */
    public String getLibellePaiement() {
        return libelle_paiement;
    }

    /**
     * Sets libelle paiement.
     *
     * @param libelle the libelle
     */
    public void setLibellePaiement(String libelle) {
        this.libelle_paiement = libelle;
    }
}
