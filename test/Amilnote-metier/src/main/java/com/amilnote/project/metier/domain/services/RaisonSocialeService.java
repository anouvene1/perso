package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.entities.RaisonSociale;
import com.amilnote.project.metier.domain.entities.json.RaisonSocialeAsJson;

import java.util.List;

public interface RaisonSocialeService {

    /**
     * Gets all raison sociale.
     *
     * @return the all raison sociale
     */
    List<RaisonSociale> getAllRaisonSociale();

    /**
     * Gets all raison sociale as json.
     *
     * @return the all poste as json
     */
    List<RaisonSocialeAsJson> getAllRaisonSocialeAsJson();

    /**
     * Gets poste by code.
     *
     * @param collaborator collab
     * @return the raison poste by collab
     */
    RaisonSociale getRaisonSocialeByCollab(Collaborator collaborator);
}
