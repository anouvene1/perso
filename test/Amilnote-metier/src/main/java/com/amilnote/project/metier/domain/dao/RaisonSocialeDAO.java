package com.amilnote.project.metier.domain.dao;

import com.amilnote.project.metier.domain.entities.RaisonSociale;

public interface RaisonSocialeDAO extends LongKeyDAO<RaisonSociale>{

    RaisonSociale findByIdCollab(Long idCollab);

}
