/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services;

import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.AbsenceAsJson;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Date;
import java.util.List;

/**
 * The interface Absence service.
 *
 * @author AmiltoneDev_DT006
 */
public interface AbsenceService extends AbstractService<Absence> {

    /**
     * Retourne tous les type d'absences
     *
     * @return String (Liste de TypeAbsenceAsJson)
     */
    String getAllTypeAbsenceAsJson();

    /**
     * Retourne toutes les absences qui ne se sont pas terminées après le 1er du mois courant.
     *
     * @param collaborator the p collaborateur
     * @param pListEvents    the p list events
     * @return String (liste de AbsenceAsJson)
     * @throws JsonProcessingException the json processing exception
     */
    String findAllAbsencesNotFinishForCollaborator(Collaborator collaborator, List<LinkEvenementTimesheet> pListEvents) throws JsonProcessingException;

    /**
     * retourne toutes les types d'absences correspondant à l'id passé en paramètre
     *
     * @param id the id
     * @return TypeAbsence type absence
     * FIXME creer TypeAbsenceDAO, impl et TypeAbsenceService et impl (separation des roles!)
     */
    TypeAbsence findById(long id);

    /**
     * retourne toutes les types d'absences correspondant au collaborateur passé en paramètre entre les dates données
     *
     * @param collaborator the collaborateur
     * @param dateDeb       the date deb
     * @param dateFin       the date fin
     * @return list list
     */
    List<Absence> findByCollaboratorBetweenDate(Collaborator collaborator, Date dateDeb, Date dateFin);

    /**
     * Nombre d'absences en attente de validation manager
     *
     * @return int nb absence attente val
     */
    int getNbAbsenceAttenteVal();

    /**
     * Nombre d'absences d'un collaborateur en attente de validation manager
     *
     * @param collaborator the p collaborateur
     * @return int nb absence attente val
     */
    int getNbAbsenceAttenteVal(Collaborator collaborator);

    /**
     * Nombre d'absences d'un collaborateur à l'état brouillon
     *
     * @param collaborator the p collaborateur
     * @return int nb absence brouillon val
     */
    int getNbAbsenceBrouillonVal(Collaborator collaborator);

    /**
     * Date dernière absence soumise :
     *
     * @param collaborator the p collaborateur
     * @return Date date dernier abs soumis
     */
    Date getDateDernierAbsSoumis(Collaborator collaborator);

    /**
     * Retourne Toutes les absences pour pCollaborateur et à l'état Brouillon
     *
     * @param collaborator the p collaborateur
     * @return list absences by collaborateur and etat brouillon
     */
    List<Absence> getAbsencesByCollaboratorAndEtatBrouillon(Collaborator collaborator);

    /**
     * Retourne Toutes les absences pour pCollaborateur et à l'état Brouillon en Json
     *
     * @param collaborator the p collaborateur
     * @return String absences by collaborateur and etat brouillon as json
     */
    String getAbsencesByCollaboratorAndEtatBrouillonAsJson(Collaborator collaborator);

    /**
     * Update absences from br to so string.
     *
     * @param pListId the p list id
     * @return the string
     */
    String updateAbsencesFromBRToSO(String pListId, String comment);

    /**
     * Retourne toutes les absences soumises
     *
     * @return la liste des absences json
     */
    List<AbsenceAsJson> getAllAbsencesEtatSO();

    /**
     * Update the state and comment of the absence and send an information email
     *
     * @param absenceId           {@link Long}
     * @param stateCode           {@link String}
     * @param comment             {@link String}
     * @param currentCollaborator {@link Collaborator}
     * @return a message regarding the updated state
     * @throws Exception the exception
     */
    String actionValidationAbsence(Long absenceId, String stateCode, String comment, Collaborator currentCollaborator)
            throws Exception;

    /**
     * Update the state and comment of a List of absences and send asynchronous information emails
     *
     * @param absenceIds          a list of {@link Long}
     * @param stateCode           {@link String}
     * @param comment             {@link String}
     * @param currentCollaborator {@link Collaborator}
     * @return a message regarding the updated state
     * @throws Exception exception from manageValidationAbsence
     */
    String actionValidationAbsenceList(List<Long> absenceIds, String stateCode, String comment, Collaborator currentCollaborator)
            throws Exception;

    /**
     * Update the state and comment of the absence
     *
     * @param absenceId the absence id
     * @param stateCode the state code
     * @param comment   the comment
     * @throws Exception the exception
     */
    void manageValidationAbsence(Long absenceId, String stateCode, String comment) throws Exception;

    /**
     * Retourne toutes les absences au format json
     *
     * @return la liste des absences asJson
     */
    List<AbsenceAsJson> findAllAsJson();

    /**
     * Permet d'ajouter à la date de début et de fin d'une absence ses horaires afin de différencier les matin des après-midi
     *
     * @param listeAbs the liste abs
     * @return la liste des absences avec l'heure de début/fin de l'absence (AM ou PM)
     */
    List<Absence> findDemiJourDebutFinAbsence(List<Absence> listeAbs);

    /**
     * Permet de reccuperer les absences soumises comprises entre deux dates
     *
     * @param start start date String format
     * @param end   end date String format
     * @return une liste d'{@link Absence}
     */
    List<Absence> getAbsencesSoumisesBetweenDate(String start, String end);

    /**
     * Permet de reccuperer toutes les absences triées par dates
     *
     * @return une liste d'{@link Absence}
     */
    List<Absence> getAbsencesOrderByDate();


    /**
     * Returns a list of absences by state, month and year, ordered by starting date
     *
     * @param state {@link Etat}
     * @param month {@link String}
     * @param year  {@link String}
     * @return List {@link Absence}
     */
    List<Absence> findAbsencesByStateMonthAndYearOrderByDate(String state, String month, String year);

    /**
     * Permet de reccuperer les absences validées comprises entre deux dates
     *
     * @param start start date String format
     * @param end   end date String format
     * @return une liste d'{@link Absence}
     */
    List<Absence> getAbsencesValideesBetweenDate(String start, String end);


    /**
     * Annule le RA correspondant au mois d'une absence d'un collaborateur (suite à annulation ou refus d'une absence)
     *
     * @param commentaire commentary
     * @param dateDebutRA start date
     * @param collaborator      a {@link Collaborator}
     * @throws Exception exception
     */
    void annulerRA(String commentaire, Date dateDebutRA, Collaborator collaborator) throws Exception;

    /**
     * Supprime une absence
     *
     * @param absence {@link Absence} to delete
     * @return Message de retour
     */
    String deleteAbsence(Absence absence);

    /**
     * Met à jour l'etat de l'absence
     *
     * @param absence {@link Absence} to update
     * @param etat    new {@link Etat} for absence
     * @return id absence
     */
    Long updateEtatAbsence(Absence absence, Etat etat);

    Long updateCommentaireAbsence(Absence absence, String commentaire);


    /**
     * Returns a list of Absences from a list of ids
     *
     * @param absenceIds a list of {@link Long}
     * @return a list of {@link Absence}
     */
    List<Absence> findByIds(List<Long> absenceIds);
}
