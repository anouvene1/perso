package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.Collaborator;
import com.amilnote.project.metier.domain.services.AbsenceService;
import com.amilnote.project.metier.domain.services.MailListService;
import com.amilnote.project.metier.domain.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MailListServiceImpl implements MailListService {

    @Autowired
    MailService mailService;

    @Autowired
    AbsenceService absenceService;


    /**
     * {@linkplain MailListService#sendAbsenceListValidationEmail(String, List, Collaborator, String)}
     */
    @Override
    @Async
    @Transactional(readOnly = true)
    public void sendAbsenceListValidationEmail(String state, List<Long> absenceIds, Collaborator connectedCollaborator, String comment) {
        List<Absence> absences = absenceService.findByIds(absenceIds);
        absences.forEach(absence -> mailService.sendMailAbsenceValidation(state, absence, connectedCollaborator, comment));
    }
}
