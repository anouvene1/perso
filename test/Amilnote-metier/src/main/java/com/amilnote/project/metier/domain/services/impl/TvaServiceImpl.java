/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.impl.TvaDAOImpl;
import com.amilnote.project.metier.domain.entities.*;
import com.amilnote.project.metier.domain.entities.json.TVAAsJson;
import com.amilnote.project.metier.domain.services.ClientService;
import com.amilnote.project.metier.domain.services.TvaService;
import com.amilnote.project.metier.domain.utils.TVAForm;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Tva service.
 */
@Repository("tvaService")
public class TvaServiceImpl extends AbstractServiceImpl<TVA, TvaDAOImpl> implements TvaService {

    @Autowired
    private TvaDAOImpl tvaDAO;

    @Autowired
    private ClientService clientService;

    /**
     * {@linkplain TvaService#getAllTVA()}
     */
    @Override
    public List<TVA> getAllTVA() {
        return tvaDAO.loadAll();
    }

    /**
     * {@linkplain TvaService#getTVAById(long)}
     */
    @Override
    public TVA getTVAById(long pId) {
        return tvaDAO.get(pId);
    }

    /**
     * {@linkplain TvaService#getAllTVAAsJson()}
     */
    @Override
    public List<TVAAsJson> getAllTVAAsJson() {
        List<TVA> lListTVA = tvaDAO.loadAll();
        List<TVAAsJson> lTypeTVAAsJson = new ArrayList<>();

        for (TVA lTva : lListTVA) {
            lTypeTVAAsJson.add(lTva.toJson());
        }
        return lTypeTVAAsJson;
    }

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public TvaDAOImpl getDAO() {
        return tvaDAO;
    }

    /**
     * {@linkplain TvaService#getTVAByCode(String)}
     */
    @Override
    public TVA getTVAByCode(String pCode) {
        return tvaDAO.findUniqEntiteByProp(TVA.PROP_CODE, pCode);
    }

    /**
     * {@linkplain TvaService#updateAllTVA(TVAForm)}
     */
    @Override
    public void updateAllTVA(TVAForm pTvaForm) {
        TVA tmpTVA = null;

        // --- Création de la session et de la transaction
        Session session = tvaDAO.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        // --- Sauvegarde et Commit de la mise à jour
        for (TVAAsJson tmpTVAAsJson : pTvaForm.getlistTVA()) {
            tmpTVA = this.getTVAByCode(tmpTVAAsJson.getCode());
            tmpTVA.setMontant(tmpTVAAsJson.getMontant());
            session.saveOrUpdate(tmpTVA);
        }
        session.flush();
        transaction.commit();
    }

    /**
     * {@linkplain TvaService#updateTVAIntracRate(Commande, Facture)}
     */
    @Override
    public long updateTVAIntracRate(Commande commande, Facture bill) {
        Client customer = commande.getClient();
        if (customer == null) {
            return 1;
        }

        if(clientService.isClientIntracNumberFR(customer)){
            bill.setTVA(getTVAByCode(TVA.TAUX_NORMAL));
        }
        else{
            bill.setTVA(getTVAByCode(TVA.TAUX_ZERO));
        }
        return bill.getTVA().getId();
    }

}
