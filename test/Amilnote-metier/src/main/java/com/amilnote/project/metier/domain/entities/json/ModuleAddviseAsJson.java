/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.ModuleAddvise;

/**
 * The type Module addvise as json.
 */
/*
 * SHA AMNOTE-203 12/01/2017.
 */
public class ModuleAddviseAsJson {

    private Long id;
    private String codeModuleAddvise;
    private String nomModuleAddvise;

    /**
     * Constructeur
     */
    public ModuleAddviseAsJson() {
    }

    /**
     * Instantiates a new Module addvise as json.
     *
     * @param moduleAddvise the module addvise
     */
    public ModuleAddviseAsJson(ModuleAddvise moduleAddvise) {
        this.id = moduleAddvise.getId();
        this.codeModuleAddvise = moduleAddvise.getCodeModuleAddvise();
        this.nomModuleAddvise = moduleAddvise.getNomModuleAddvise();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets code module addvise.
     *
     * @return codeModuleAddvise code module addvise
     */
    public String getCodeModuleAddvise() {
        return codeModuleAddvise;
    }

    /**
     * Sets code module addvise.
     *
     * @param codeModuleAddvise the dateDebut to set
     */
    public void setCodeModuleAddvise(String codeModuleAddvise) {
        this.codeModuleAddvise = codeModuleAddvise;
    }

    /**
     * Gets nom module addvise.
     *
     * @return nomModuleAddvise nom module addvise
     */
    public String getNomModuleAddvise() {
        return nomModuleAddvise;
    }

    /**
     * Sets nom module addvise.
     *
     * @param nomModuleAddvise the dateDebut to set
     */
    public void setNomModuleAddvise(String nomModuleAddvise) {
        this.nomModuleAddvise = nomModuleAddvise;
    }
}
