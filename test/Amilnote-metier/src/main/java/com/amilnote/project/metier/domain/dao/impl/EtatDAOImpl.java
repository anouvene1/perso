/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.dao.impl;

import com.amilnote.project.metier.domain.dao.EtatDAO;
import com.amilnote.project.metier.domain.entities.Etat;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Etat dao.
 */
@Repository("etatDAO")
public class EtatDAOImpl extends AbstractDAOImpl<Etat> implements EtatDAO {

    /**
     * {@linkplain AbstractDAOImpl#getReferenceClass()}
     */
    @Override
    protected Class<Etat> getReferenceClass() {
        return Etat.class;
    }

    /**
     * {@linkplain EtatDAO#getAllEtat()}
     */
    @Override
    public List<Etat> getAllEtat() {
        return currentSession().createCriteria(Etat.class).list();
    }

    /**
     * {@linkplain EtatDAO#getEtatById(int)}
     */
    @Override
    public Etat getEtatById(int id) {
        Etat lEtat = (Etat) currentSession().get(Etat.class, id);
        return lEtat;
    }

}


