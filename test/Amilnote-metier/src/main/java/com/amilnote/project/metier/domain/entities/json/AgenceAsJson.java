package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.Agence;

/**
 * The type Agence as json.
 */
public class AgenceAsJson {

    private Long id;
    private String adresse;
    private Long codePostal;
    private String ville;
    private String code;
    private boolean siege_social;

    /**
     * Instantiates a new Agence as json.
     */
    public AgenceAsJson() {
    }

    public AgenceAsJson(Agence agence) {
        id = agence.getId();
        adresse = agence.getAdresse();
        codePostal = agence.getCodePostal();
        ville = agence.getVille();
        code = agence.getCode();
        siege_social = agence.isSiege_social();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets adresse.
     *
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Sets adresse.
     *
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Gets codePostal.
     *
     * @return the codePostal
     */
    public Long getCodePostal() {
        return codePostal;
    }

    /**
     * Sets codePostal.
     *
     * @param codePostal the codePostal to set
     */
    public void setCodePostal(Long codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Gets ville.
     *
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * Sets ville.
     *
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets siege_social.
     *
     * @return the siege_social
     */
    public boolean isSiege_social() {
        return siege_social;
    }

    /**
     * Sets siege_social.
     *
     * @param siege_social the siege_social to set
     */
    public void setSiege_social(boolean siege_social) {
        this.siege_social = siege_social;
    }


}
