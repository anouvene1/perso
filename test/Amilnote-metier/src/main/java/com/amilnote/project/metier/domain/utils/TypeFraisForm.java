/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.utils;

import com.amilnote.project.metier.domain.entities.json.TypeFraisAsJson;

import java.util.List;

/**
 * The type Type frais form.
 */
public class TypeFraisForm {

    private List<TypeFraisAsJson> listTypeFrais;

    /**
     * Gets list type frais.
     *
     * @return the list type frais
     */
    public List<TypeFraisAsJson> getListTypeFrais() {
        return listTypeFrais;
    }

    /**
     * Sets list type frais.
     *
     * @param pListTypeFrais the p list type frais
     */
    public void setListTypeFrais(List<TypeFraisAsJson> pListTypeFrais) {
        this.listTypeFrais = pListTypeFrais;
    }
}
