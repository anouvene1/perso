/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.services.impl;

import com.amilnote.project.metier.domain.dao.StatutCollaborateurDAO;
import com.amilnote.project.metier.domain.entities.Absence;
import com.amilnote.project.metier.domain.entities.StatutCollaborateur;
import com.amilnote.project.metier.domain.entities.json.StatutCollaborateurAsJson;
import com.amilnote.project.metier.domain.services.StatutCollaborateurService;
import com.amilnote.project.metier.domain.utils.SearchCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.amilnote.project.metier.domain.utils.enumerations.ComparisonOperator.IN;

/**
 * The type Statut collaborateur service.
 */
@Service("statutCollaborateurService")
public class StatutCollaborateurServiceImpl extends AbstractServiceImpl<StatutCollaborateur, StatutCollaborateurDAO> implements StatutCollaborateurService {

    private static final Logger logger = LogManager.getLogger(Absence.class);

    @Autowired
    private StatutCollaborateurDAO statutDAO;

    /**
     * {@linkplain AbstractServiceImpl#getDAO()}
     */
    @Override
    public StatutCollaborateurDAO getDAO() {
        return statutDAO;
    }

    /**
     * {@linkplain StatutCollaborateurService#getAllStatut()}
     */
    @Override
    public List<StatutCollaborateur> getAllStatut() {
        return this.getDAO().loadAll();
    }

    /**
     * {@linkplain StatutCollaborateurService#getAllStatutAsJson()}
     */
    @Override
    public List<StatutCollaborateurAsJson> getAllStatutAsJson() {
        List<StatutCollaborateur> lListStatuts = this.getAllStatut();
        List<StatutCollaborateurAsJson> lListStatutsAsJson = new ArrayList<>();

        for (StatutCollaborateur lStatut : lListStatuts) {
            lListStatutsAsJson.add(lStatut.toJson());
        }
        return lListStatutsAsJson;
    }

    /**
     * {@linkplain StatutCollaborateurService#findStatutCollaborateurById(int)}
     */
    @Override
    public StatutCollaborateur findStatutCollaborateurById(int aId_Statut) {
        return statutDAO.findUniqEntiteByProp(StatutCollaborateur.PROP_ID, aId_Statut);
    }

    @Override
    public StatutCollaborateur findStatutCollaborateurByCode(String code) {
        return statutDAO.findStatutCollaborateurByCode(code);
    }

    /**
     * {@linkplain StatutCollaborateurService#findCollaboratorsStatusByCodes(String...)}
     */
    @Override
    public List<StatutCollaborateur> findCollaboratorsStatusByCodes(String... collaboratorsStatusCodes) {
        return findBySearchCriterias(
                StatutCollaborateur.PROP_CODE,
                true,
                new SearchCriteria<>(StatutCollaborateur.PROP_CODE, IN, collaboratorsStatusCodes)
        );
    }
}
