/*
 * ©Amiltone 2017
 */

package com.amilnote.project.metier.domain.entities.json;

import com.amilnote.project.metier.domain.entities.LinkPerimetreMission;

/**
 * Created by sbenslimane on 22/12/2016.
 */
public class LinkPerimetreMissionAsJson {

    private Long id;
    private PerimetreMissionAsJson perimetreMission;
    private MissionAsJson mission;

    /**
     * Constructeur
     */
    public LinkPerimetreMissionAsJson() {
    }

    /**
     * Instantiates a new Link perimetre mission as json.
     *
     * @param pLinkPerimetreMission the p link perimetre mission
     */
    public LinkPerimetreMissionAsJson(LinkPerimetreMission pLinkPerimetreMission) {
        this.setId(pLinkPerimetreMission.getId());
        this.setPerimetreMission(new PerimetreMissionAsJson(pLinkPerimetreMission.getPerimetreMission()));
        this.setMission(new MissionAsJson(pLinkPerimetreMission.getMission()));
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets perimetre mission.
     *
     * @return idPerimetreMission perimetre mission
     */
    public PerimetreMissionAsJson getPerimetreMission() {
        return perimetreMission;
    }

    /**
     * Sets perimetre mission.
     *
     * @param perimetreMission the perimetre mission
     */
    public void setPerimetreMission(PerimetreMissionAsJson perimetreMission) {
        this.perimetreMission = perimetreMission;
    }

    /**
     * Gets mission.
     *
     * @return Mission mission
     */
    public MissionAsJson getMission() {
        return mission;
    }

    /**
     * Sets mission.
     *
     * @param mission the id to set
     */
    public void setMission(MissionAsJson mission) {
        this.mission = mission;
    }


}
