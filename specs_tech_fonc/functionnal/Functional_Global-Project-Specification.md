# <!-- omit in toc --> Spécifications fonctionnelles globales du projet - Amilnote collaborateur

- [1. Contexte Projet](#1-contexte-projet)
- [2. Glossaire](#2-glossaire)
- [3. Fiches d'identité des applications](#3-fiches-didentit%c3%a9-des-applications)
- [4. Schéma fonctionnel de l'architecture](#4-sch%c3%a9ma-fonctionnel-de-larchitecture)
- [5. Les principes généraux](#5-les-principes-g%c3%a9n%c3%a9raux)
  - [5.1. Gestion d'utilisateurs de rôles et de permissions](#51-gestion-dutilisateurs-de-r%c3%b4les-et-de-permissions)
  - [5.2. Remplissage du rapport d'activités](#52-remplissage-du-rapport-dactivit%c3%a9s)
- [6. Les fonctionnalités et règles métier](#6-les-fonctionnalit%c3%a9s-et-r%c3%a8gles-m%c3%a9tier)
  - [6.1. Rapport d'activité](#61-rapport-dactivit%c3%a9)
    - [6.1.1. Description](#611-description)
    - [6.1.2. Règles métier](#612-r%c3%a8gles-m%c3%a9tier)
    - [6.1.3. Absences](#613-absences)
  - [6.2. Notes de frais](#62-notes-de-frais)
    - [6.2.1. Description](#621-description)
    - [6.2.2. Règles métier](#622-r%c3%a8gles-m%c3%a9tier)
  - [6.3. Demande de déplacement](#63-demande-de-d%c3%a9placement)
    - [6.3.1. Description](#631-description)
    - [6.3.2. Règles métier](#632-r%c3%a8gles-m%c3%a9tier)
  - [6.4. Ordre de mission](#64-ordre-de-mission)
    - [6.4.1. Description](#641-description)
    - [6.4.2. Règles métier](#642-r%c3%a8gles-m%c3%a9tier)

## 1. Contexte Projet

Amilnote API se veut être l'application des collaborateurs pour la gestion de leurs rapports d'activités. Dans cette version, le collaborateur pourra notifier ses jours de mission/absence, remplir ses frais pour le mois, créer une demande de déplacement et valider/invalider un ordre de mission lui étant proposé. 

Un front sera connecté pour pouvoir afficher et traiter les informations. 

Les fonctionnalités de l'API seront évidemment vouées à évoluer au fil du temps pour supplanter l'utilisation de l'application Amilnote que nous connaissons actuellement. Il est donc crucial de réfléchir dès maintenant à la meilleure manière d'implémenter les briques techniques nécessaires à l'avenir.

## 2. Glossaire

- AD : Active Directory
- ODM : Ordre De Mission
- RA : Rapport d'Activités
- DRH : Direction des Ressources Humaines
- DG : Directeur Général
- CDP : Chef de Projet

## 3. Fiches d'identité des applications

- Spring boot
- JAVA 11
- MySQL / *PostgreSQL*

## 4. Schéma fonctionnel de l'architecture

_To do_

## 5. Les principes généraux

### 5.1. Gestion d'utilisateurs de rôles et de permissions

Tous les utilisateurs se connectent à l'application via leurs identifiants AD. Chaque utilisateur a un rôle associé qui lui octroie certaines permissions dans l'application.

À terme, plusieurs rôles seront créés sur l'API :
- Sous-traitant
- Collaborateur (représente 90% des utilisateurs)
- Ingénieur d'affaires
- CDP
- RH
- DRH
- DG

Dans un premier temps, seul le rôle collaborateur sera implémenté. Ce rôle dispose des fonctionnalités communes à tous les utilisateurs et représente, par conséquent, le point de départ de cette API.

### 5.2. Remplissage du rapport d'activités

Chaque collaborateur remplit un rapport d'activités sur une base mensuelle. Le rapport est la partie centrale d'Amilnote, sans cela, la facturation serait impossible, l'inscription des absences également ainsi que de nombreuses autres fonctionnalités.

## 6. Les fonctionnalités et règles métier

### 6.1. Rapport d'activité

#### 6.1.1. Description

La partie rapport d'activités permet au collaborateur de saisir ses jours de présence et d'absence en rapport avec la/les mission(s) affectée(s) à ce dernier.

#### 6.1.2. Règles métier

- Chaque journée du rapport d'activités est scindée en deux parties : matin et après-midi. La granularité minimale lors de la sélection d'une plage horaire est **d'une demi-journée**. Il est par exemple impossible d'inscrire uniquement deux heures, ainsi, une journée de travail/absence correspond à deux demi-journées.
- Lors de l'inscription d'une période travaillée, le collaborateur doit pouvoir choisir une mission dans la liste des missions qui sont **actuellement** en cours pour ce collaborateur.
- Une demande de validation d'absence est nécessaire pour valider les jours d'absence du mois courant. (_NB_: Les jours d'absence doivent être validés par la direction au moins un mois à l'avance)
- Si la sélection concerne un jour d'absence, (voir [Absences](#61-absences)) la liste des types d'absences doit être récupérée pour ensuite inscrire dans le rapport d'activités le type d'absence qui convient à la situation.
- le collaborateur aura aussi la possibilité de notifier des jours d'astreinte (heures travaillées avec les pauses) suivi d'un commentaire.
- L'utilisateur doit avoir la possibilité de récupérer les rapports d'activités des 6 derniers mois. Il est aussi possible de prévoir ses jours d'absence sur les mois futurs (nombre de mois à définir). Pour chaque mois, il est possible de permettre l'édition des jours fériés et des samedis.

- Deux validations sont possible pour le RA : **validation du rapport d'activités** et **validation des frais**

  - Lorsque l'on valide son RA sans frais, les jours travaillés du mois en cours seront validés. Cela permet à la comptabilité de gérer la paie du mois.
  - Lorsque l'on valide son RA final, tous les frais que le collaborateur a eus sur le mois seront transmis à la comptabilité (voir [Notes de frais](#62-notes-de-frais))
  - _NB2_ : S'il y a des jours sans mission/absence entre deux contrats (stage/alternance/CDD -> CDI), le mois actuel devra considérer uniquement **les jours travaillés**.


- Pour chaque jour travaillé, il est obligatoire de spécifier la mission concernée.
- Pour chaque jour d’absence, il est obligatoire de spécifier un type d’absence.
- Pour chaque jour d’astreinte, il est possible de spécifier les heures travaillées/pauses ainsi qu’un commentaire.

L'API doit retourner la liste de toutes les missions **en cours** d'un collaborateur. elle doit par la même occasion retourner la liste des frais associés à une mission en cours.

Il est possible pour le collaborateur de valider son RA sans les frais du mois pour valider les absences/jours travaillés.

Il est possible pour le collaborateur de valider le RA final pour valider les absences/jours travaillés ainsi que les frais du mois.

#### 6.1.3. Absences

Plusieurs types d'absences sont disponibles pour remplir son RA :

- Absence autorisée
- Absence exceptionnelle
- Congés payés
- Congés sans solde
- Congé naissance
- Congé maternité
- Congé paternité
- Jour de RTT
- Jour non travaillé
- Maladie
- Fin de contrat

Plusieurs statuts d'absences sont également disponibles :

- Brouillon
- Soumis
- Validé
- Refusé
- Annulé

### 6.2. Notes de frais

#### 6.2.1. Description

En tant que collaborateur, je peux insérer des frais pour une mission particulière ainsi que visualiser mes anciens frais.

#### 6.2.2. Règles métier

Chaque frais est lié à **une mission** et à **une date**. Le frais en question doit **obligatoirement** être saisi avant la validation finale du RA.

Lors de la création du frais, la liste des missions actives du collaborateur est disponible. Le frais doit nécessairement être rattaché à une mission toujours valide à la date du frais. La mission du collaborateur se termine au minimum à la date du frais.

Les types de frais sont les suivants :

- Abonnement transport en commun
- Avance sur frais
- Avion
- Divers (autre frais)
- Essence
- Hôtel
- Location meublée
- Location véhicule
- Parking
- Petit déjeuner
- Péage
- Restaurant midi
- Restaurant soir
- Taxi
- Train
- Transport (bus, navette)
- Téléphone
- Voiture personnelle

Pour chaque frais, certaines informations sont demandées :

- Date du frais (Obligatoire)
- Frais en France (oui/non) (Obligatoire)
- Montant du frais (en Euro) (Obligatoire)
- Motif du frais
- Descriptif/nom du fichier généré automatiquement et unique
- Fichier justificatif du frais (Obligatoire)

Tant que le RA n'est pas finalisé, le frais peut être éditable. A tout moment, le fichier justificatif doit pouvoir être récupéré pour être visualisé par le front.

### 6.3. Demande de déplacement

#### 6.3.1. Description

En tant que collaborateur, je peux créer une demande de déplacement lorsqu'un client m'envoie sur un site extérieur (autre ville, autre pays,...)

#### 6.3.2. Règles métier

Pour la demande de déplacement, le collaborateur devra renseigner les informations suivantes :

- La mission en cours (Obligatoire)
- La date de début du déplacement (Obligatoire)
- La date de fin du déplacement (Obligatoire)
- Le motif du déplacement
- Le code projet du client
- Le lieu du déplacement (Obligatoire)
- Un commentaire

Comme pour les frais, la date de début et la date de fin doivent **obligatoirement** correspondre à une mission toujours en cours pour le collaborateur. La mission du collaborateur se termine au minimum à la date de fin du déplacement.

### 6.4. Ordre de mission

#### 6.4.1. Description

En tant que collaborateur, je peux valider mes Ordres De Mission (ODM).

#### 6.4.2. Règles métier

Lors de la création d'un nouvel ODM pour un collaborateur, ce dernier doit être en mesure d'accepter ou refuser l'ordre de mission.