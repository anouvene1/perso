 Spécifications techniques 

- [1. Architecture BDD](#1-architecture-bdd)
- [2. Architecture application (MVC)](#2-architecture-application-mvc)
- [3. Listing des dépendances du projet](#3-listing-des-dépendances-du-projet)
- [4. Listing des différentes routes de l API](#4-listing-des-différentes-routes-de-l-api)
- [5. Mise en place des tests unitaires et des logs](#5-mise-en-place-des-tests-unitaires-et-des-logs)
- [6. Étude de la mise en place d une configuration externe à l API](#6-étude-de-la-mise-en-place-d-une-configuration-externe-à-l-api)
    - [6.1 Configuration](#61-configuration)
- [7. Étude des possibilités de l AD](#7-étude-des-possibilités-de-l-ad)
    - [7.1 Introduction](#71-introduction)
    - [7.2 Mise en place et configuration](#72-mise-en-place-et-configuration)
    - [7.3 Authentification](#73-authentification)
    - [7.4 Recherche d'utilisateurs](#74-recherche-dutilisateurs)
    - [7.5 Création de tokens d'authentification](#75-creation-de-tokens-dauthentification)
- [8. Étude de la mise en place de l intégration continue](#8-étude-de-la-mise-en-place-de-l-intégration-continue)
    - [8.2 SonarLint SonarQube](#81-sonarlint-sonarqube)
    - [8.1 GitLab CI](#82-gitlab-ci)
    - [8.3 Jenkins](#83-jenkins)

<br/><br/>

## 1. Architecture BDD 
![Alt text](img/mcd_collaborator.png "model conceptuel de donnees collaborateur")

![Alt text](img/new_amilnote_collab_diagram.png "model logique de donnees collaborateur")
<br/><br/>

## 2. Architecture application (MVC)

![Alt text](img/amilnote-api-architecture-1.png "couches de l'architecture n-tierce")

![Alt text](img/amilnote-api-architecture-2.png "Organisation des packages")
<br/><br/>

## 3. Listing des dépendances du projet

Spring Boot

| Starter                       | Dépendances      | Description                 |
| :---------------              |:-------------    | :--------------           |
| spring-boot-starter           | spring-core      | *description* |
| spring-boot-starter-web       | spring-webmvc <br/> hibernate-validator  |*description*| |
| spring-boot-starter-logging   | log4j-to-slf4j    | *description*            |
| spring-boot-starter-data-jpa  |  hibernate-core   | *description*  |
| -                             | postgresql | version 9.1-901-1.jdbc4 |
|spring-boot-starter-test | junit <br/> mockito-core <br/> hamcrest-core | *description* |
|spring-boot-starter-security | stater-security <br/> spring-ldap-core <br/> spring-data-ldap <br/> pring-security-ldap <br/> spring-security-oauth2 <br/> jsonwebtoken <br/> spring-security-jwt |  *à modifier* |
|-              |spring-cloud-starter-config <br/> spring-boot-starter-actuator <br/> jasypt | *description*  | 
| spring-boot-starter-batch | - | *description* |
|-| modelmapper | en cas de type de variable différent entre entité et DTO (utilisé dans Amilnote API) |
|- | Apache pdfbox |version 2.0.17 |
|- | swagger | *description* |


<br/><br/>


## 4. Listing des différentes routes de l API


## RA : rapport d'activité

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /activity-report/{date} | date: date demandée | Récupérer le RA selon la date demandée | Objet contenant les événements du mois demandé ainsi que l'état du RA |
| POST: /activity-report | date: date voulue <br> cost: frais | Soumettre le RA, avec OU sans les frais| - |
| GET: /activity-report/collaborator/{id}/{date} | id du collaborateur et date souhaitée | Récupérer un TA pour un collaborateur et une date donnée| Objet contenant les événements du collaborateur/mois demandé ainsi que l'état du RA |
| GET: /activity-report/{pdf-file} | - | Récupérer le pdf du RA généré lors de la validation de celui-ci | fichier .pdf|

## Evénements

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /events | - | récupère les events du collaborateur authentifié pour le dernier mois terminé | tableau d'objets events |
| GET: /events/{monthYear} | monthYear: mois et année au format MM-YYYY | Récupérer tous les événements du collaborateur identifié pour un mois donné | Tableau contenant des objets Event |
| GET: /events/{id} | identifiant collaborateur | récupère les evenements d'un collaborateur pour le dernier mois terminé | tableau contenant des objet Event |
| GET: /events/{id}/{monthYear} | id: collaborateur, monthYear: mois et année au format MM-YYYY | récupère les evenements d'un collaborateur sur le mois voulu | tableau contenant des objet Event |
| POST: /events | Tableau contenant des objets Event | Sauvegarder les évènements | String (ou ResponseEntity) |

## Mission

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /missions | - | Récupérer toutes les missions du collaborateur authentifié | Tableau contenant des objets Mission |
| GET: /missions/{monthYear} | monthYear: mois et année au format MM-YYYY | Récupérer toutes les missions du collaborateur authentifié pour un mois donné | Tableau contenant des objets Mission |
| GET: /missions/{year} | Year: année au format YYYY | Récupérer toutes les missions du collaborateur authentifié pour une année donnée | Tableau contenant des objets Mission |
| GET: /missions/{id} | idMission | Récupérer une mission spécifique | String (ou ResponseEntity) |
| GET: /missions/{id}/status | idMission | Récupérer le statut d'une mission spécifique | String (ou ResponseEntity) |
| GET: /missions/collaborator/{id} | id : collaborator | Récupère la liste des missions d'un collaborateur | tableau de missions | 
| GET: /missions/collaborator/{id}/{monthYear} | id : collaborator, monthYear: mois et année au format MM-YYYY | Récupère la liste des missions d'un collaborateur pour un mois donné | tableau de missions | 
| GET: /missions/collaborator/{id}/{year} | id : collaborator, Year: année au format YYYY | Récupère la liste des missions d'un collaborateur | tableau de missions | 
| GET: /missions/{id}/mission-order | idMission | Récupérer le pdf de l'ODM | ResponseEntity |
| POST: /missions | Objet mission | Créer une mission | String (ou ResponseEntity) |
| PUT: /missions/{id}/status | idMission et le statut "validée" ou "refusée" | Modifier le statut d'une mission( "validée" / "refusée") spécifique | String (ou ResponseEntity) |
| PUT: /missions | Objet mission | Modifier une mission | String (ou ResponseEntity) |
| DELETE: /missions | Objet mission | Supprimer une mission | String (ou ResponseEntity) |

## Absence
### Avec une table absence

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /absences | - | récupère toutes les absences du dernier mois terminé | tableau contenant des objets absence |
| GET: /absences/{id} | id de l'absence | Récupère l'absence voulue | objet de type absence |
| GET: /absences/collaborator/{id} | id: identifiant du collaborateur | Récupérer toutes les absences d'un collaborateur | Tableau contenant des objets Absence |
| GET: /absences/collaborator/{id}/{monthYear} | id: identifiant du collaborateur, monthYear: mois-année au format MM-YYYY  | Récupérer toutes les absences d'un collaborateur pour le mois donné | Tableau contenant des objets Absence |
| GET: /absences/{monthYear} | monthYear: mois-année au format MM-YYYY | Récupérer toutes les absences de tous les collaborateurs pour le mois donné | Tableau contenant des objets Absence |
| PUT: /absences | listId: liste d'identifiants des absences concernées <br> comment: commentaire | Demander la validation d'une période d'absence | String (ou ResponseEntity) |
| PUT: /absences/{id} | id de l'objet absence | Modifier une absence | String (ou ResponseEntity) |
| PUT: /absences/validation | liste d'id des absences a valider | Valider des absences | String (ou ResponseEntity) |
| POST: /absences | Objet absence | Créer une absence | String (ou ResponseEntity) |
| DELETE: /absences/{id}/ | id de l'objet absence | Supprimer une absence | String (ou ResponseEntity) |

### Sans table absence

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /events/{absence-type} | - | récupère toutes les absences du dernier mois terminé | tableau contenant des objets absence |
| GET: /events/{absence-type}/{id} | id de l'absence | Récupère l'absence voulue | objet de type absence |
| GET: /events/{absence-type}/collaborator/{id} | id: identifiant du collaborateur | Récupérer toutes les absences d'un collaborateur | Tableau contenant des objets Absence |
| GET: /events/{absence-type}/collaborator/{id}/{monthYear} | id: identifiant du collaborateur, monthYear: mois-année au format MM-YYYY  | Récupérer toutes les absences d'un collaborateur pour le mois donné | Tableau contenant des objets Absence |
| GET: /events/{absence-type}/{monthYear} | monthYear: mois-année au format MM-YYYY | Récupérer toutes les absences de tous les collaborateurs pour le mois donné | Tableau contenant des objets Absence |
| PUT: /events/{absence-type} | listId: liste d'identifiants des absences concernées <br> comment: commentaire | Demander la validation d'une période d'absence | String (ou ResponseEntity) |
| PUT: /events/{absence-type}/{id} | id de l'objet absence | Modifier une absence | String (ou ResponseEntity) |
| PUT: /events/{absence-type}/validation | liste d'id des absences a valider | Valider des absences | String (ou ResponseEntity) |
| POST: /events/{absence-type} | Objet absence | Créer une absence | String (ou ResponseEntity) |
| DELETE: /events/{absence-type}/{id}/ | id de l'objet absence | Supprimer une absence | String (ou ResponseEntity) |

## Notes de frais

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /expenses/{id} | id: identifiant de l'expense | récupère un frais de mission | objet expense |
| GET: /expenses/collaborator/{filter} | filter: ALL, BR, VA, SO, SD | Récupérer tous les frais de mission d'un collaborateur selon filtre | Tableau contenant des objets expenses |
| GET: /expenses/{id}/receipt | idExpense | Récupérer le justificatif de frais en pdf | ResponseEntity |
| POST: /expenses | Objet mission | Créer un frais de mission | String (ou ResponseEntity) |
| PUT: /expenses/{id} | id du frais de mission | Modifier un frais de mission | String (ou ResponseEntity) |
| DELETE: /expenses/{id} | id du frais de mission | Supprimer un frais de mission | String (ou ResponseEntity) |

## Déplacement

|     Route     |     Paramètres     |    Description     |    Retour     |
| :------------ | :------------- | :------------- | :------------- |
| GET: /transport-requests | - | Récupérer toutes les demandes de déplacement pour le dernier mois terminé | Tableau contenant les objets TransportRequests |
| GET: /transport-requests/collaborator/{id} | id: collaborator | Récupérer toutes les demandes de déplacement pour un collaborateur | Tableau contenant les objets TransportRequests |
| GET: /transport-requests/{monthYear} | monthYear: mois-année au format MM-YYYY | Récupérer toutes les demandes de déplacement pour un mois donné | Tableau contenant les objets TransportRequests |
| GET: /transport-requests/collaborator/{id}/{monthYear} | monthYear: mois-année au format MM-YYYY | Récupérer toutes les demandes de déplacement d'un collaborateur pour un mois donné | Tableau contenant les objets TransportRequests |
| POST: /transport-requests/ | Objet RequestTransport | Créer une demande de déplacement | String (ou ResponseEntity |
| PUT: /transport-requests/{id}/status | id de la demande de déplacement et le statut validé/refusé et annulé | Modifier le statut de la demande( validé / refusée / annulé) | String (ou ResponseEntity) |
| DELETE: /transport_requests/{id} | id de la demande | supprime une demande de déplacement | String (ou ResponseEntity) |

<br/><br/>

## 5. Mise en place des tests unitaires et des logs

*en cours de rédaction*

<br/><br/>

## 6. Étude de la mise en place d une configuration externe à l API

Pour l'API, nous avons décidé d'utiliser Spring. Ce framework permet d'utiliser des fichiers contenant un certain nombre de propriétés, telles que, par exemple, le port sur lequel va s'exécuter l'application ou l'adresse de la base de données par exemple. Ces fichiers sont généralement stockés dans le dossier resources. Néanmoins, on peut souhaiter externaliser ces différents fichiers dans un dépot Git. De cette manière, il sera possible d'effectuer des modifications dans ces différentes propriétés sans devoir redémarrer l'application.

### 6.1. Configuration

Pour effectuer cette externalisation, on aura plusieurs modules dans notre projet. Tout d'abord, on aura un projet où nous commiterons les différentes propriétés dans un fichier de type properties. 
Ce projet sera séparé de l'application principale et ne comportera que le fichier de type properties.<br/>
Ensuite, on va ajouter un module dans l'application. Ce module sera notre serveur d'externalisation de la configuration. Pour créer ce module, il faut utiliser Spring Initializr et créer un projet Spring Boot avec les bibliothèques suivantes : <br/>
- spring-cloud-config-server : bibliothèque pour créer le serveur d'externalisation de la configuration
- spring-boot-starter-actuator : bibliothèque pour actualiser les différentes propriétés à la volée dans notre application principale
- jasypt : bibliothèque pour encrypter diverses propriétés dans nos fichiers dre properties ( utilisation à développer ) <br/>

Pour créer notre serveur d'externalisation, il suffit de faire deux opérations. Tout d'abord, il faut ajouter l'annotation <b>@EnableConfigServer</b> dans la classe de lancement de l'application. Ensuite, il faut ajouter diverses propriétés dans le fichier application.properties de notre module : 
- server.port=<numéro de port> ( si vous souhaitez lancer le serveur sur un port différent de 8080 )
- spring.cloud.config.server.git.uri=<url du dépot git ou seront hébergées les properties>
- spring.cloud.config.server.git.username=<nom d'utilisateur du dépôt git>
- spring.cloud.config.server.git.password=<mot de passe de l'utilisateur du dépôt git>
- spring.cloud.config.server.encrypt.enabled=true ou false. Permet d'autoriser l'encryption et la décryption du coté du client des propriétes<br/>

Au lancement, ce projet va se connecter au dépôt Git et récupérer les différentes propriétés stockées dans le fichier application.properties du dépôt Git.<br />

Enfin, pour faire en sorte que notre application principale puisse utiliser les différentes propriétés du dépôt Git, il y a plusieurs étapes à respecter : 
-  Tout d'abord, il faut renommer le fichier application.properties de notre projet en bootstrap.properties. De cette manière, ce sera le fichier hébergé dans le dépôt Git qui sera pris en compte au démarrage de l'application
- Ensuite, il faut ajouter les propriétés suivantes dans notre bootstrap.properties : 
    - spring.application.name=<nom de l'application>. Ce nom doit correspondre au nom du fichier hébergé sur le dépôt Git. Par exemple, si le fichier s'appelle "app-test.properties", le nom à renseigner sera "app-test"
    - spring.cloud.config.uri=<adresse du serveur d'externalisation de la configuration>
    - management.endpoints.web.exposure.include=*. Cette propriété est nécessaire pour mettre à jour les différentes valeurs des propriétés via Spring Actuator
    - spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true

Grâce à cette installation, il est possible d'accéder aux propriétés du dépot Git en utilisant l'annotation @Value. Pour la mise à jour des différentes propriétés, il suffit de modifier le fichier dans le projet du dépôt Git, de commiter les modifications et de redémarrer le projet. Ensuite, pour que les modifications soient répercutées dans le projet principal, il faut utiliser Spring Actuator. Au préalable, il faut ajouter l'annotation <b>@RefreshScope</b> sur les classes où on souhaite récupérer nos propriétes mises à jour. Ensuite, il faut lancer la requête POST suivante : http://<adresse de l'application>/actuator/refresh. Ainsi, les valeurs seront mises à jour dans l'application

<br/><br/>

## 7. Étude des possibilités de l AD

### 7.1. Introduction

Le protocole LDAP ( pour Lightweight Directory Access Protocol ) est un protocole standard permettant de gérer des annuaires, c'est-à-dire d'accéder à des bases d'informations sur les utilisateurs d'un réseau par l'intermédiaire de protocoles TCP/IP. Il a été développé en 1993 par l'université du Michigan et avait pour but de supplanter le protocole DAP en l'intégrant à la suite TCP/IP. A partir de 1995, LDAP est également devenu un annuaire natif (standalone LDAP). <br/>
LDAP fournit à l'utilisateur des méthodes lui permettant de :
- se connecter
- se déconnecter
- rechercher des informations
- comparer des informations
- insérer des entrées
- modifier des entrées
- supprimer des entrées

D'autre part le protocole LDAP ( dans sa version 3 ) propose des mécanismes de chiffrement ( SSL, ... ) et d'authentification ( SASL ) permettant de sécuriser l'accès aux informations stockées dans la base.

LDAP présente les informations sous forme d'une arborescence d'informations hiérarchiques appelée DIT ( Directory Information Tree ), dans laquelle les informations, appelées entrées ( ou encore DSE, Directory Service Entry ), sont représentées sous forme de branches.
Une branche située à la racine d'une ramification est appelée racine ou suffixe ( en anglais root entry ).

Chaque entrée de l'annuaire LDAP correspond à un objet abstrait ou réel (par exemple une personne, un objet matériel, des paramètres, ...). Chaque noeud de l'arbre correspond à un objet pouvant appartenir à n'importe quelle classe d'objets à tout niveau. Cela signifie qu'il est possible d'utiliser n'importe quelle classe comme racine de l'arbre ou encore qu'une classe d'objets peut être utilisée à n'importe quel niveau de la hiérarchie.

![Alt text](img/architecture-ldap.png "Architecturte LDAP")

Chaque entrée est constituée d'un ensemble d'attributs ( paires clé/valeur ) permettant de caractériser l'objet que l'entrée définit. Il existe deux types d'attributs :
- Les attributs normaux: ceux-ci sont les attributs habituels ( nom, prénom, ... ) caractérisant l'objet.
- Les attributs opérationnels: ceux-ci sont des attributs auxquels seul le serveur peut accéder afin de manipuler les données de l'annuaire ( dates de modification, ... )

Ci-dessous un exemple d'utilisateur dans le LDAP d'Amiltone : 

![Alt text](img/exemple-utilisateur-ldap.png "Exemple d'utilisateur LDAP")

Une entrée est indexée par un nom distinct ( DN, distinguished name ) permettant d'identifier de manière unique un élément de l'arborescence.

Un DN se construit en prenant le nom de l'élément, appelé Relative Distinguished Name ( RDN, c'est-à-dire le chemin de l'entrée par rapport à un de ses parents ), et en lui ajoutant l'ensemble des nom des entrées parentes. Il s'agit d'utiliser une série de paires clé/valeur permettant de repérer une entrée de manière unique. Voici une série de clés généralement utilisées :
- uid ( userid ) : il s'agit d'un identifiant unique obligatoire
- cn ( common name ) : il s'agit du nom de la personne
- givenname : il s'agit du prénom de la personne
- sn ( surname ) : il s'agit du surnom de la personne
- o ( organization ) : il s'agit de l'entreprise de la personne
- u ( organizational unit ) : il s'agit du service de l'entreprise dans laquelle la personne travaille
mail : il s'agit de l'adresse de courrier électronique de la personne

Quant à Active Directory, il s'agit de la mise en oeuvre par Microsoft des services d'annuaire LDAP pour les systèmes d'exploitation Windows.

Tout le code relatif à l'authentification et à la recherche d'utilisateurs sur Active Directory est disponible dans le projet amilnote-api ( l'ancien projet ) sur la branche security/ldap.

Spring nous propose un ensemble de bibliothèques qui permettent de créer une application permettant à un utilisateur de s'authentifier mais aussi d'effectuer des opérations sur le serveur AD ( CRUD ).

### 7.2. Mise en place et configuration

Tout d'abord, il faut ajouter un certain nombre de dépendances dans le pom.xml pour mettre en place les mécanismes d'authetification et de recherche d'utilisateurs dans notre serveur AD. Elles sont les suivantes : 
- spring-boot-starter-security : starter Spring Security, qui permet de créer des modules d'authentification et d'autorisation pour les applications JAVA
- spring-ldap-core : bibliothèque de base pour implémenter une application basée sur le protocole LDAP
- spring-security-ldap : implémentation de Spring Security pour les applications LDAP
- spring-data-ldap : implémentation de Spring Data pour les applications LDAP

Après avoir ajouté ces dépendances, il faut ajouter un certain nombre de propriétés pour pouvoir établir une connection avec le serveur AD. Ces propriétés sont : 
- ldap.domain: <domaine du serveur AD>
- spring.ldap.urls: <adresse du serveur AD>
- spring.ldap.base: <base du serveur AD>
- spring.ldap.username: <nom d'utilisateur du compte de jonction au serveur AD>
- spring.ldap.password: <mot de passe du compte de jonction au serveur AD>

Les deux premières serviront à établir une connexion vers le serveur AD, les quatre autres seront utiles pour établir un template qui nous permettra d'interagir avec le serveur lors des opérations de persistance vers le serveur.

### 7.3. Authentification

Pour permettre à un utilisateur de s'identifier auprès du serveur AD en renseignant son nom d'utilisateur et son mot de passe, il va falloir créer deux classes dans notre projet : 
- une classe de configuration ( WebSecurityConfig dans le package config )
- un controller ( AuthenticationController dans le package controller )

Dans la classe de configuration, nous allons configurer la connexion vers notre serveur AD. Elle étend la classe WebSecurityConfigurerAdapter et comporte plusieurs annotations Spring : 
- @Configuration : permet de créer une classe de configuration
- @EnableWebSecurity : permet de remplacer les paramètres de sécurité par défaut par ceux de notre classe de configuration

Dans cette classe, nous allons créer un bean qui nous permettra d'établir une connexion avec notre serveur AD. Ce bean sera de type AuthenticationManager. Dans la création de l'objet AuthenticationManager, il faudra renseigner le domaine du serveur AD ainsi que son adresse. 

Ensuite, il faut écrire deux méthodes qui s'appellent "configure". dans la première, on va configurer un objet de type AuthenticationManagerBuilder, qui utilisera notre AuthenticationManager pour permettre à un utilisateur de se connecter en renseignant son nom d'utilisateur et son mot de passe dans un formulaire. La seconde méthode permet de configurer le formualire de connexion de l'utilisateur. C'est ici qu'on va lier le formulaire à notre controller pour, qu'à  la soumission du formulaire, on soit redirigé vers la méthode "authenticate" du controller. On peut également indiquer une route vers un formulaire personnalisé ou laisser le formulaire par défaut.

Pour s'authentifier, il suffit de démarrer l'application. Dès que vous lancez l'application dans un navigateur, un formulaire de connexion apparaitra. Si vous renseignez un identifiant et un mot de passe corrects, vous serez redirigé vers l'AuthenticationController.

### 7.4. Recherche d'utilisateurs

Pour rechercher un utilisateur dans l'annuaire LDAP, il existe deux méthodes.

Après la connexion, il est possible de récupérer les données de l'utilisateur en accédant au contexte de connexion. Pour cela il faut utiliser la ligne de code suivante : Authentication auth = SecurityContextHolder.getContext().getAuthentication(). Cette  ligne retourne un objet de type Authentication qui contient toutes les données de l'utilisateur connecté. 

![Alt text](img/authentication.png "Exemple d'objet Authentication").

Il existe aussi une deuxième méthode : l'utilisation de Spring Data LDAP. Ce framework est en tous points similaire à Spring Data JPA. Il faut tout d'abord créer un objet User et mapper ses attributs avec ceux d'un utilisateur dans l'annuaire LDAP ( cf classe User dans le package Model ). Cette classe doit avoir l'annotation @Entry pour faire le lien entre notre objet et un utilisateur de l'annuaire.

Ensuite, il faut créer une interface avec nos différentes méthodes DAO, à la différence que notre interface étendra LdapRepository au lieu de JpaReository ( cf. l'interface UserRepository dans le package repository ). Cela permet de créer des méthodes personnalisées pour rechercher des utilisateurs à partir d'un attribut particulier de l'objet User. Après, comme pour un CRUD classique, il suffit de créer un service qui utilisera nos différentes méthodes DAO, puis on pourra utiliser notre service dans notre AuthenticationController.

A l'heure actuelle, seules les données suivantes sont présentes dans l'annuaire : 
- nom
- prénom
- adresse mail
- groupes auxquels l'utilisateur appartient

Il est possible d'ajouter des données utilisateur dans le LDAP. Mais il faut bien noter qu'à la abse un serveur AD n'est pas conçu pour se substituer à une base de données mais pour de l'authentification.

### 7.5. Création de tokens d'authentification

TO DO 


## 8. Étude de la mise en place de l intégration continue
### 8.1. SonarLint SonarQube
<b> <u> SonarQube / SonarLintSonarQube :</u> </b> <br/> 
<b>SonarQube</b> <br/>
<u> -Fonctionnalités :</u> <br/>
Espace d'analyse dédié consultable sur http://localhost:9000/sonar/Classement des défauts logiciels en 3 catégories : 
<ul>
    <li> bugs </li>
    <li> vulnérabilités </li> 
    <li>"code smells" : anti-patterns qui impactent la maintenabilité de l'application.</li>
</ul>
<i>Ecran d'accueil :</i></br>

![Alt text](img/sonarqube_accueil.png "Ecran d'accueil de SonarQube")

<b>"Passed" = </b> Le projet satisfait les exigeances définies dans l'espace "Barrière qualité" <br/>
<b>"Défauts" =</b> bugtracking, liste des défauts détaillés de l'application.<br/>
<b>"Mesures" =</b> Informations supplémentaires générales.<br/>
<b>"Code" =</b> Permet de visualiser le code fichier par fichier <br/>
<b>"Tableau de bord" =</b> métrique qui communique la qualité du code <br/>

<u>Règles de SonarQube :</u><br/>
 Liste des règles : http://localhost:9000/sonar/coding_rules <br/><br/>
<u> Lancer une analyse SonarQube depuis Jenkins : </u> <br/>
    <ol>
        <li> Installer le plugin SonarQube : https://wiki.jenkins.io/display/JENKINS/SonarQube+plugin
        </li>
        <li> Générer un token dans l'interface SonarQube. Losque connecté : cliquer sur son profil, puis "my account" et "Generate tokens".
        </li>
        <li> Cliquer sur "administrer Jenkins" puis sur "Configurer le système". Dans "SonarQube servers", entrer le token généré.
        </li>
        <li> Créer un job et le configurer. Créer une étape de build "Lancer une analyse avec SonarQube Scanner". (si on ne remplit pas les champs, ce sont les infos du fichier "sonar-project.properties" qui sont prises en compte)
        </li>
    </ol> 
    <u>SonarQube et Jenkins Pipeline :</u> <br/>
    <i> exemple de pipeline réalisant une analyse SonarQube : </i><br/>

 ![Alt text](img/pipeline_code_exemple.png "pipeline code exemple")

<br/>
<u>Points faibles de SonarQube :</u><br/>
 <ul>
    <li> Exactitude discutable </li>
    <li> Ce n'est pas un substitut de revue de code</li>
 </ul>
    *************************************************************************** <br/><br/>
    <u><b>SonarLint :</b></u> <br/>
    <u>Présentation :</u> <br/>
     SonarLint est un plugin qui permet de réaliser des analyses de code SonarQube directement dans l'IDE, au fur et à mesure des développements. <br/> 
    <u> Installation :</u> 
    <ol>
    <li>Ouvrir IntelliJ</li>
    <li>Aller dans "File" > "Settings" > "Plugins" </li>
    <li>Cliquer sur "Browse repositories"</li>
    <li>Rechercher "SonarLint", installer et redémarrer IntelliJ</li>
    </lo>
    <br/>
    <u>Lancer une analyse :</u><br/>
     Cliquer sur l'onglet "Analyze" d'intelliJ. Choisir les options SonarLint dansla liste déroulante.<br/><br/>
     <u>Intégration de SonarLint avec un serveur SonarQube :</u><br/>
      <i> Partager les règles SonarQube sur tous les postes : </i> 
      <ol>
      <li>Aller dans "File"> "Settings" > "Other settings" > "SonarLint General Settings"
      </li>
      <li>Ajouter un serveur SonarQube à l'aide d'un token ou d'un couple login/password
      </li>
      <li>Dans l'onglet "SonarLint Project Settings", associer le projet courant au projet correspondant dans SonarQube. <b><i>! Le nom du projet doit être indentique dans les 2 outils ! </i></b>
      </li>
      <li>Retour à l'onglet "SonarLint General Settings", cliquer sur le bouton "Update binding".
      </li>
      </ol>
      <br/>
      <i>Tester :  </i>  Introduire un bug dans le code, et changer la priorité de ce bug dans SonarCube. Cliquer de nouveau sur "Update Binding" dans les setting d'IntelliJ, et la priorité du bug doit changer dans l'IDE lors de la prochaine analyse.<br/><br/>
      <u>Résoudre les problèmes de version :</u><br/>
      <i> message d'erreur pouvant survenir : </i><br/>
      <i><b>The following plugins do not meet the required minimum versions, please upgrade them: *** (installed: x.x, minimum: x.x).</b></i> <br/>
      <i>solution :</i><br/>
      <ol>
      <li>Aller sur l'interface SonarQube</li>
      <li>Se connecter avec un compte administrateur</li>
      <li>Aller dans "Configuration" > "Système" > "Mises à jour"</li>
      <li>Mettre à jour le module mentionné dans le message d'erreur.</li>
      </ol>

### 8.2. GitLab CI
<b><u>Intégration continue avec GitLab CI:</u></b><br/><br/>
<u>Introduction :</u><br/>
GitLab est une plateforme permettant d'héberger et gérer des projets. Grâce à son tableau de bord, il fournit une vue complète des issues et tâches en cours.
Outre cela, GitLab permet également de gérer l'intégration et le déploiement continue des projets grâce à l'ajout d'un fichier de configuration .gitlab-ci.yml au répertoire racine du dépôt Git.
Nous allons voir les concepts clés de l'intégration continue avec GitLab, la configuration du fichier .gitlab-ci.yml, les jobs, les pipelines, et les Runners.
<br/><br/>
<u>Configuration de .gitlab-ci.yml : </u><br/>
Ce fichier est écrit en **YAML**.
Il configure le projet GitLab de sorte à utiliser un Runner. Cela signifie que chaque commit ou push déclenche l'éxécution des pipelines d'intégration continue. Les données concernant l'IC (jobs, environements, pipelines, etc..) sont affichées dans le menu CI/CD du menu. 
<br/><br/>
<b>La notion de jobs </b><br/>
Le fichier .gitlab-ci.yml définit un ensembles de jobs (ou tasks) qui auront une contrainte obligatoire : être exécutés dans un stage (une étape), et de toujours contenir l'instruction 'script'<br/>
<i>exemple d'un fichier .gitlab-ci.yml :</i>

![Alt text](img/gitlab-ci-yml_exemple.png "exemple d'un fichier gitlab-ci.yml")
<br/><br/>
Les jobs sont exécutés indépendamment les uns des autres, dans l'ordre des stages  (et non de déclaration). 
Le nombre de jobs est illimité, mais chacun d'entre eux doit avoir un nom unique pour l'identifier. L'odre d'exécution des jobs est défini dans stages : <br/>

![Alt text](img/stages_exemple.png "exemple d'un fichier gitlab-ci.yml")
<br/>
On peut nommer chaque étape comme on le souhaite.
<br/><br/>
Mots-clés :<br/>
<ul>
    <li><b>script :</b> cette clause contient les commandes à exécuter par le runner.
    </li>
    <li><b>before-script :</b> commandes à exécuter avant de lancer les jobs 
    </li>
    <li><b>environment :</b> défini les environnements spécifiques de déploiement des jobs(exemple : review, test, qa, production, staging, etc..)
            <ul>
                <li> On peut nommer l'environnement comme on le souhaite
                </li>
                <li>Avec les termes spécifiques "review", "staging", "production" ou "test", les stats du temps passé à la création d'une issue au déploiement dans l'environnement seront visibles dans le menu "Overview/ Cycle Analytics"
                </li>
            </ul>
    </li>
    <li><b>when :</b> défini quand le jobdoit être lancé (manual, on_failure, on_success, always)
    </li>
    <li><b>only :</b> ne déclenche le job qu'aux push ou commit sur certaines branches ou tags.
    </li>
    <li><b>except :</b> ne déclenche pas les jobs aux push ou commit sur certaines branches ou tags.
    </li>
    <li><b>artifacts :</b> une liste de fichiers ou répertoires joints aux jobs et téléchargeables quand ceux-ci sont exécutés avec succès.
    </li>
</ul>
<br/>
<u>Pipeline :</u>
<br/>
Un pipeline est un ensemble de jobs exécutés par étapes.
Tous les jobs d'une même étape sont éxécutés en parallèle si suffisamment de Runners simultanés sont disponibles. 
Le pipeline passe à l'étape suivante lorsque tous les jobs de l'étape en cours ont terminer leurs exécutions avec succès. Si ne serait-ce qu'un job fail, le pipeline arrête son éxécution.
<br/>
<i>pipeline dans GitLab :</i>

![Alt text](img/schema_pipeline.png "exemple d'un fichier gitlab-ci.yml")

Cette vue est disponible dans l'onglet "Pipelines" du menu CI/CD.
<br/><br/>
<u> GitLab CI Runner : </u>
<br/>
Un Runner est une machine virtuelle, un container Docker ou un cluster de containers, un VPS (Virtual Private Server) qui permet de personnaliser l'environnement d'éxécution du job.
Le Runner gitlab est écrit en Go; supporte les scripts Bash, les Batchs Windows, Windows Powershell et fonctionne sur les plate-formes GNU/Linux, OS X, Windows.
GitLab communique avec le Runner au travers d'API pour éxécuter les scripts écrits dans la configuration gitlab-ci. 
Le Runner doit avoir une connexion à internet et pouvoir communiquer via SSH.
Un Runner est personnalisable facilement  grâce à une configuration prise en charge par des environnements Docker, Docker-ssh, parallèle ou SSH. 
<br/>
<i>Exemple d'un Runner pour l'éxécution d'un job :</i>

![Alt text](img/execution_runner_exemple.png "exemple d'un fichier gitlab-ci.yml")

Pour configurer un Runner, il faut récupérer un token sur gitlab et lancer la commande suivante :

![Alt text](img/config_runner_commande.png "exemple d'un fichier gitlab-ci.yml")

Puis :
<br/>
<b><i> $ gitlab-runner run </i></b>
<br/>
pour lancer le run
<br/><br/>
<u>Container Docker :</u>
<br/>
Un container Docker peut être utilisé en tant que Runner dans la configuration gitlab-ci (disponible à partir de la version 1.5.0). Gitlab et Gitlab Runner utilisent le moteur Docker pour l'exécution et les tests d'applications.
<br/>
Gitlab permet d'utiliser une image disponible sur un serveur privé ou sur le hub Docker.<br/>
Docker dans Gitlab exécute chacun des jobs dans un environnement isolé et contenant l'image à utiliser.<br/>
Cela permet donc de mettre en place une configuration reproductible simplement, de tester des commandes ou tâches sur le container et non sur les serveurs d'intégration dédié.<br/>
Pour utiliser une image et des services docker dans .gitlab-ci.yml, on utilise ce code : 

![Alt text](img/code_image_docker_service.png "exemple d'un fichier gitlab-ci.yml")

<br/>

### 8.3. Jenkins
*rédaction en cours (doc Mathilde à venir)*